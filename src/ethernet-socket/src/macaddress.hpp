#ifndef MACADDRESS_HPP_INCLUDED
#define MACADDRESS_HPP_INCLUDED

#include <string>
#include <ostream>

class macAddress
{
  unsigned char octet[6];
public:
    macAddress ( int a, int b, int c, int d, int e, int f );
    macAddress ( const macAddress & add );
    virtual ~ macAddress (  )
  {
  };
  const macAddress & operator= ( const macAddress & add );
  const unsigned char *getMac (  ) const
  {
    return octet;
  }

  const std::string toString (  ) const;

  static const size_t size = 6;
  static const macAddress BROADCAST;

  bool operator== ( const macAddress & add ) const;
  bool operator!= ( const macAddress & add ) const;
};

std::ostream & operator<< ( std::ostream & stream, const macAddress & add );

#endif // MACADDRESS_HPP_INCLUDED
