#include <linux/init.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
//#include <linux/cdev.h>
#include "main.h"
#include "ethernet.h"

#include "char.h"

MODULE_AUTHOR ("Tobias Nielsen");
MODULE_LICENSE ("Dual BSD/GPL");

int buffersize = 100;

module_param (buffersize, int, S_IRUGO);

static void ur_module_exit (void);
static int ur_module_init (void);

struct ur_interface *ur_main;

unsigned char *
select_rbuffer (struct ur_interface *iff)
{
  return iff->rbuffer;
}

unsigned char *
select_wbuffer (struct ur_interface *iff)
{
  /* if (iff->holding == 0)
     {
     init_completion (&iff->complete_send);
     iff->holding = 1;
     wait_for_completion (&iff->complete_send);
     } */
  return iff->wbuffer;
}

unsigned char *
pick_rbuffer (struct ur_interface *iff)
{
  return iff->rbuffer;
}

unsigned char *
pick_wbuffer (struct ur_interface *iff)
{
  return iff->wbuffer;
}

void
unpick_rbuffer (struct ur_interface *iff)
{
  /* if (iff->holding == 1)
     {
     complete_all (&iff->complete_send);
     iff->holding = 0;
     } */
}

void
unpick_wbuffer (struct ur_interface *iff)
{

}


void
release_rbuffer (struct ur_interface *iff)
{

}



void
release_wbuffer (struct ur_interface *iff)
{

}

size_t
buffer_size (struct ur_interface *ur)
{
  return ur->buffer_size;
}

/**
 * Uses no locks!!!
 */
int
ur_create_buffers (struct ur_interface *main)
{
  /*
   *         Check to see if the buffers are already aranged
   *         We dont have do it twice
   */

  if (main->buffers_init == 1)
    return 0;

  main->rbuffer = kmalloc (buffersize, GFP_KERNEL);
  if (main->rbuffer == 0)
    goto out;

  main->wbuffer = kmalloc (buffersize, GFP_KERNEL);
  if (main->wbuffer == 0)
    goto out;

  memset (main->rbuffer, 0, buffersize);
  memset (main->wbuffer, 0, buffersize);

  main->buffer_size = buffersize;
  main->buffers_init = 1;

  return 0;
out:
  ur_free_buffers (main);
  return -ENOMEM;
}


/**
 * Uses no locks!!!
 */
void
ur_free_buffers (struct ur_interface *main)
{
  if (main->rbuffer == 0)
    kfree (main->rbuffer);

  if (main->wbuffer == 0)
    kfree (main->wbuffer);

  main->buffers_init = 0;
}



/***************************************
 * Initialisation / shutdown
 ***************************************/
static int __init
ur_module_init (void)
{
  int result = 0;

  if (!(ur_main = kmalloc (sizeof (struct ur_interface), GFP_KERNEL)))
    goto err;

  if ((result = setup_ur_ethernet ()) < 0)
    goto err;

  if ((result = ur_char_init ()) < 0)
    goto err;

  return result;

err:
  ur_module_exit ();
  return result;
}

static void __exit
ur_module_exit (void)
{
  int result = 0;

  if ((result = ur_char_destroy ()) != 0)
    {
      PERROR ("char err - %d\n", result);
    }

  if ((result = shutdown_ur_ethernet ()) != 0)
    {
      PERROR ("Net error - %d\n", result);
    }

  if (ur_main)
    {
      ur_free_buffers (ur_main);
      kfree (ur_main);
    }

  return;
}

module_init (ur_module_init);
module_exit (ur_module_exit);
