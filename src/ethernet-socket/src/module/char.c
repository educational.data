/********************************************************
 ** \file char.c
 **
 ** Implementation of a simple char device.
 **
 ** This is made to make sure a very simplistic control
 ** interface for writing to coldfire device exists.
 **
 ** A further controll interface can be used in order
 ** to manage the robot.
 **
 ** $Author: tobibobi $
 ** $Revision: 1.4 $
 **
 ********************************************************/
#include <linux/config.h>
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>

#include <linux/kernel.h>	/* printk() */
#include <linux/slab.h>		/* kmalloc() */
#include <linux/fs.h>		/* everything... */
#include <linux/errno.h>	/* error codes */
#include <linux/types.h>	/* size_t */
#include <linux/proc_fs.h>
#include <linux/fcntl.h>	/* O_ACCMODE */
#include <linux/seq_file.h>
#include <linux/cdev.h>

#include <asm/system.h>		/* cli(), *_flags */
#include <asm/uaccess.h>	/* copy_*_user */

#include "char.h"
#include "main.h"
#include "ethernet.h"

/**
 * Our parameters which can be set at load time.
 */
int ur_major = 0; /// < Indicates major device.
int ur_minor = 0; /// < Indicated minor device.

module_param (ur_major, int, S_IRUGO);
module_param (ur_minor, int, S_IRUGO);



/****************************************************
 * Open functionality.
 *
 * Just makes sure that the buffers is set up 
 * correctly
 *
 * prior it would have changes the ethernet block 
 * to a running state.
 ****************************************************/
static int
ur_char_open (struct inode *inode, struct file *filp)
{
  int err = 0;
  struct ur_cdev *dev;		/* device information */

  dev = container_of (inode->i_cdev, struct ur_cdev, cdev);

  filp->private_data = dev;	/* for other methods */
  if (down_interruptible (&dev->sem))
    return -ERESTARTSYS;

  err = ur_create_buffers (ur_main);

  up (&dev->sem);

  if (!err)
    return 0;			//ethernet_start (ur_main->ether);

  return err;
}



/*****************************************************
 * Release the device ressources
 *
 * This is mainly a notifier from the kernel that the
 * user space program has released all connections to
 * the char device
 *
 * We could use it to release buffers, but its hardly
 * neccesarry.
 *****************************************************/
static int
ur_char_release (struct inode *inode, struct file *filp)
{
  PDEBUG ("char released\n");
  return 0;			//ethernet_stop (ur_main->ether);
}





static loff_t
ur_char_llseek (struct file *filp, loff_t off, int whence)
{
  struct ur_cdev *dev = filp->private_data;

  loff_t newpos;

  PDEBUG ("llseek called\n");

  switch (whence)
    {
    case 0:			/* SEEK_SET */
      newpos = off;
      break;

    case 1:			/* SEEK_CUR */
      newpos = filp->f_pos + off;
      break;

    case 2:			/* SEEK_END */
      newpos = dev->size + off;
      break;

    default:			/* can't happen */
      return -EINVAL;
    }
  if (newpos < 0)
    return -EINVAL;
  filp->f_pos = newpos;
  return newpos;
}





/*******************************************************************
 ** char dev write functionality
 *******************************************************************/
static ssize_t
ur_char_write (struct file *filp, const char __user * buf, size_t count,
	       loff_t * f_pos)
{
  struct ur_cdev *dev = filp->private_data;
  unsigned char *ibuf;
  int retval = 0;

  ibuf = select_wbuffer (dev->main);
  if (ibuf == NULL)
    return -EPERM;

  if (down_interruptible (&dev->sem))
    return -ERESTARTSYS;


  /*
   *         Trim input size
   */
  if (count > dev->main->buffer_size)
    {
      //retval = -EFAULT;
      count = dev->main->buffer_size;
      //goto out;
    }


  /*
   *         Move across boundaries
   */
  if (copy_from_user (ibuf, buf, count))
    {
      retval = -EFAULT;
      goto out;
    }

  /* 
   *         Finally, ship the data to the output buffer.
   */
  ethernet_send (dev->main->ether, ibuf, count);

  *f_pos = 0;
  retval = count;
out:
  release_wbuffer (dev->main);
  up (&dev->sem);
  PDEBUG ("recieved %d bytes\n", count);

  return retval;
}





/*******************************************************************
 ** char dev read functionality
 **
 ** It will not indicate end of file, but it should probally 
 ** start of by saying how much data is available in some way.
 ** But this is still to be considered.
 *******************************************************************/
static ssize_t
ur_char_read (struct file *filp, char __user * buf, size_t count,
	      loff_t * f_pos)
{
  struct ur_cdev *dev = filp->private_data;
  int retval = 0;
  unsigned char *ptr;
  //char rabuf[] = "ACK";
  int size_of_packet = 0;

  if (down_interruptible (&dev->sem))
    return -ERESTARTSYS;

  ptr = select_rbuffer (dev->main);

  if (count > dev->main->buffer_size)
    {
      count = dev->main->buffer_size;
    }

  if ((size_of_packet = ethernet_recv (dev->main->ether, ptr, count)) == 0)
    {
      goto out;
    }

  if (size_of_packet == -ERESTARTSYS)
    {
      count = 0;
      goto out;
    }

  /* Check size of user buffer - and shrink the result
   * FIXME: Make this perform better - checking is a dud
   */
  if ( size_of_packet > count ) 
    {
      size_of_packet = count;
    }

  if (copy_to_user (buf, ptr, size_of_packet))
    {
      retval = -EFAULT;
      goto out;
    }
  //mdelay(150);
  /*
   * Transmit acknowledge package
   */
  ethernet_send (dev->main->ether, (void *) ptr, 1);


  *f_pos = 0;
  retval = size_of_packet;
out:
  up (&dev->sem);
  release_rbuffer (ur_main);
  return retval;
}


static struct file_operations ur_char_fops = {
  .owner = THIS_MODULE,
  .llseek = ur_char_llseek,
  .read = ur_char_read,
  .write = ur_char_write,
  .open = ur_char_open,
  .release = ur_char_release,
};

int __init
ur_char_init ()
{
  dev_t dev = 0;
  int result = 0;
  struct ur_cdev *cdev;

  result = alloc_chrdev_region (&dev, ur_minor, 1, CHAR_DEVICE);
  ur_major = MAJOR (dev);
  if (result < 0)
    {
      printk (KERN_WARNING MODULE_NAME ":Cannot get major device id\n");
      return result;
    }

  if (!(ur_main->cdev = kmalloc (sizeof (struct ur_cdev), GFP_KERNEL)))
    {
      printk (KERN_WARNING MODULE_NAME ":no memory for char device\n");
      return -ENOMEM;
    }
  memset (ur_main->cdev, 0, sizeof (struct ur_cdev));

  cdev = ur_main->cdev;
  cdev->main = ur_main;
  init_MUTEX (&cdev->sem);
  cdev_init (&cdev->cdev, &ur_char_fops);
  cdev->cdev_begin = dev;
  cdev->cdev.owner = THIS_MODULE;
  cdev->cdev.ops = &ur_char_fops;
  result = cdev_add (&cdev->cdev, dev, 1);

  if (result)
    printk (KERN_WARNING MODULE_NAME ":error %d adding " CHAR_DEVICE "0",
	    result);
  else
    cdev->init = 1;

  return result;
}

int __exit
ur_char_destroy ()
{
  struct ur_cdev *dev;

  if (ur_main)
    if (ur_main->cdev)
      {
	dev = ur_main->cdev;

	if (dev->init)
	  {
	    unregister_chrdev_region (dev->cdev_begin, 1);
	    cdev_del (&dev->cdev);
	  }

	ur_main->cdev = NULL;
	dev = NULL;
      }
  return 0;
}
