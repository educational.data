#ifndef __ETHERNET_H__INCLUDED__
#define __ETHERNET_H__INCLUDED__

#include <linux/in.h>
#include <linux/socket.h>
#include <linux/timer.h>
#include <linux/delay.h>

struct ur_main;
struct ur_device_socket_t;

struct ether_ops
{
  void (*stop) (struct ur_device_socket_t *);
  void (*start) (struct ur_device_socket_t *);
};


struct ur_interface;

/**
 * This is the actual device blok
 */
struct ur_device_socket_t
{
  struct socket *ethersock;
  struct sockaddr_in addr;
  struct net_device *net_device;
  int device_control;
  int interface_index;
  unsigned char interface_addr[6];
  int ethersock_setup;
  struct timer_list timer;
  struct timer_list recv_timer;
  struct completion done_ethernet;
  int setup_recv;
  int running;
  int errmarker;
  int counter;

  struct ether_ops *ops;

  struct ur_interface *main;
};

int setup_ur_ethernet (void);
int shutdown_ur_ethernet (void);
int ethernet_start (struct ur_device_socket_t *ether);
int ethernet_stop (struct ur_device_socket_t *ether);
int ethernet_send (struct ur_device_socket_t *ether, unsigned char *buffer,
		   size_t len);
int ethernet_recv (struct ur_device_socket_t *ether, unsigned char *buffer,
		   size_t len);


#endif
