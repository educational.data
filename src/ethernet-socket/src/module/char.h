#ifndef MODULE_UR_CHAR_INCLUDED
#define MODULE_UR_CHAR_INCLUDED

#include <linux/fs.h>		/* everything... */
#include <linux/cdev.h>

struct ur_interface;

#define CHAR_DEVICE "urobot"

struct ur_cdev
{
  unsigned char *mem_block;
  int size;
  int init;
  dev_t cdev_begin;
  struct ur_interface *main;
  struct cdev cdev;
  struct semaphore sem;
};

int ur_char_init (void);
int ur_char_destroy (void);

#endif
