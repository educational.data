/** \file ethernet.c
 * This file include all ethernet communication.
 *
 * This ethernet communication works by letting the pc send all the
 * needed data on the port, whenever it needs it to. But the
 * syncronization is purely on the recieve side of the system.\n
 * At the time a recieve of data is arrived, it must continue running
 * ASAP.\n
 * The kernel functions still needs some locking functionality in
 * order to made safe - but it seems alright for now.
 *
 * \author Tobias Nielsen \email tobibobi@gmail.com
 * \version $Revision: 1.8 $
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/kthread.h>

#include <linux/in.h>
#include <linux/if_ether.h>
#include <linux/if.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>
#include <linux/sockios.h>
#include <linux/socket.h>
#include <linux/rtnetlink.h>

#include <linux/delay.h>
#include "main.h"

#include "ethernet.h"
#include <linux/netdevice.h>

#define FALSE 0
#define TRUE 1

#define PROTOCOL    0x5255

char *net_device_name = "eth0";

module_param (net_device_name, charp, S_IRUGO);



static void ur_timer_loop (unsigned long);
static void ur_timer_recv (unsigned long);
static int __ur_send_data (struct ur_device_socket_t *ur_socket,
			   void *data, size_t size);
static int __ur_rcv_data (struct ur_device_socket_t *ur_socket, void *data,
			  size_t len, int bwait);


/**
 * Exported send function for the char device.
 *
 * This function will return from the very moment 
 * that the data has been shipped right off the device.
 * The function will try to do a best effort to send the data, but it
 * will send a minimum amount of data (64 bytes - ethernet header size
 * which is 14 bytes)
 *
 * \param ether The device that is responsible for sending data
 * \param buffer Pointer to data to be transmitted
 * \param len The length of the data to be send.
 *
 * \see ethernet_recv
 * \see ethernet_async_recv
 * \warning The function is not syncronised
 */
int
ethernet_send (struct ur_device_socket_t *ether, unsigned char *buffer,
	       size_t len)
{
  return __ur_send_data (ether, buffer, len);
}




/**
 * Exported recieve function for the char device.
 *
 * This function is a blocking function that will try to wait for a
 * reply from the robot device (ColdFIRE device). According to kernel
 * rules, the function is interruptible from the user space by
 * pressing CTRL-C, but it is not preemptible. But im still missing
 * some functionality to prevent preemption on kernel types that
 * supports preemption in kernel modes.
 *
 * \param ether The device responsible for sending the data.
 * \param buffer Pointer to the buffer that will have the data when
 * done.
 * \param len Length of buffer data available.
 * \see ethernet_send
 */
int
ethernet_recv (struct ur_device_socket_t *ether, unsigned char *buffer,
	       size_t len)
{
  return __ur_rcv_data (ether, buffer, len, FALSE);
}




/**
 * Exported recieve function for the char device.
 *
 * This function is a _non_ blocking function that will poll for a 
 * reply from the robot device (ColdFIRE device).
 *
 * \param ether The device responsible for sending the data.
 * \param buffer Pointer to the buffer that will have the data when
 * done.
 * \param len Length of buffer data available.
 *
 * \see ethernet_send
 * \warning Still missing locking
 */
int
ethernet_async_recv (struct ur_device_socket_t *ether, unsigned char *buffer,
		     size_t len)
{
  return __ur_rcv_data (ether, buffer, len, TRUE);
}



/**
 * Exported command to signal start of ehternet running.
 *
 * \deprecated This is not used by any upper layer anymore
 */
int
ethernet_start (struct ur_device_socket_t *ether)
{
  if (ether)
    if (ether->ops)
      if (ether->ops->start)
	{
	  ether->ops->start (ur_main->ether);
	  return 0;
	}

  return -EFAULT;
}



/**
 * Exported command to signal start of ehternet running
 */
int
ethernet_stop (struct ur_device_socket_t *ether)
{
  if (ether)
    if (ether->ops)
      if (ether->ops->stop)
	{
	  ether->ops->stop (ether);
	  return 0;
	}

  return -EFAULT;
}



/**
 * Mainloop of ethernet package dispatcher
 */
static void
ur_timer_loop (unsigned long arg)
{
  int err = 0;

  struct ur_device_socket_t *ur_socket = (struct ur_device_socket_t *) arg;
  unsigned char *buffer = pick_wbuffer (ur_socket->main);




  /*
   *         Send the data that we have in our buffer out on the wire
   */
  if ((err =
       __ur_send_data (ur_socket, buffer, buffer_size (ur_socket->main))) < 0)
    {
      ur_socket->errmarker = err;
      ur_socket->running = FALSE;
    }

  if (err != buffer_size (ur_socket->main))
    {
      ur_socket->errmarker = err;
      ur_socket->running = FALSE;
      err = 0;
    }

  ur_socket->counter++;




  /*
   *         By now all timers from last loop should be done.
   *         But if they dont it means we have a kernel that 
   *         is running waaay to slow...
   */
  if (ur_socket->setup_recv > 0)
    {
      ur_socket->errmarker = -ETIME;
      ur_socket->running = FALSE;
    }

  /*
   *         Check if we are still alowed to run
   */
  if (ur_socket->running == TRUE)	// this will be marked from the rmmod utils
    {

      /*
       *     Okay, now data has been sent out.
       *     This means that each device now will respond in 1 msec
       *     We start the timers so we will be ready for it.
       */
      ur_socket->setup_recv = 6;	// We expect 6 joints to reply
      ur_socket->recv_timer.expires = jiffies += 2 * (HZ / 1000);
      add_timer (&ur_socket->recv_timer);


      ur_socket->timer.expires += HZ / 100;	// runs a hundred times a second
      add_timer (&ur_socket->timer);
    }
  else
    {
      complete_all (&ur_socket->done_ethernet);
    }
}




/**************************************************************************
 * Major time loop.
 * 
 * It initiates a send message.
 * runs 100 times a second
 **************************************************************************/
static void
ur_timer_recv (unsigned long arg)
{
  int err = 0;
  unsigned char *ptr;
  struct ur_device_socket_t *ur_socket = (struct ur_device_socket_t *) arg;

  ptr = pick_rbuffer (ur_socket->main);
  ptr += buffer_size (ur_socket->main) * (ur_socket->setup_recv - 1);
  if ((err =
       __ur_rcv_data (ur_socket, &ptr, buffer_size (ur_socket->main) / 6,
		      FALSE)) < 0)
    {
      ur_socket->errmarker = err;
      ur_socket->running = FALSE;
      return;
    }

  if (err == 0)
    {
      memset (ptr, 0, buffer_size (ur_socket->main) / 6);
      // the joint didn't answer properly
      // but we ignore it

      /* ur_socket->errmarker = -ENOMEM;
         ur_socket->running = 0; */
    }


  if ((--ur_socket->setup_recv) > 0)
    {
      ur_socket->recv_timer.expires += HZ / 1000;
      add_timer (&ur_socket->recv_timer);
    }
  else
    {
      unpick_rbuffer (ur_socket->main);
    }

}


/*******************************************************************
 * reads the reply from each device, and runs 3 times.
 *******************************************************************/
static void
set_brdcast (unsigned char *b)
{
  int c;

  for (c = 0; c < ETH_ALEN; ++c)
    {
      b[c] = 0xff;
    }
}

/*******************************************************************
 * Master function to send data in to the socket.
 *
 * Makes sure that data is send directly to the device with the
 * right package hdr on.
 *******************************************************************/
static int
__ur_send_data (struct ur_device_socket_t *ur_socket, void *data, size_t len)
{
  struct msghdr msg;
  struct ethhdr ehdr;

  //ur_socket->ethersock->ops->sendmsg(NULL,
  //unsigned char daddr[6] = { 0x00, 0x13, 0x8f, 0x23, 0x72, 0x6f };
  struct iovec iov[2];
  mm_segment_t oldfs;
  int size = 0;


  struct sockaddr_ll saddr;

  /* Is this really needed?
   */
  if (ur_socket->ethersock->sk == NULL)
    return 0;


  /*
   *         Fill out the headers of the ethernet frame.
   */
  set_brdcast (ehdr.h_dest);
  //memcpy (ehdr.h_dest, daddr, ETH_ALEN);
  memcpy (ehdr.h_source, ur_socket->interface_addr, ETH_ALEN);
  ehdr.h_proto = htons (PROTOCOL);



  /*
   *         We use the excellent functionality to make sure that 
   *         all data can be shipped in multiple arrays.
   *         in that way we prepend our ether headers.
   */
  iov[0].iov_base = &ehdr;
  iov[0].iov_len = ETH_HLEN;

  iov[1].iov_base = data;
  iov[1].iov_len = len;

  saddr.sll_ifindex = ur_socket->interface_index;;
  saddr.sll_protocol = PROTOCOL;
  memcpy (saddr.sll_addr, ur_socket->interface_addr, ETH_ALEN);

  msg.msg_flags = MSG_DONTWAIT;
  msg.msg_name = &saddr;
  msg.msg_namelen = sizeof (struct sockaddr_ll);
  msg.msg_control = NULL;
  msg.msg_controllen = 0;
  msg.msg_iov = iov;
  msg.msg_iovlen = 2;
  msg.msg_control = NULL;

  oldfs = get_fs ();
  set_fs (KERNEL_DS);
  size = sock_sendmsg (ur_socket->ethersock, &msg, len + ETH_HLEN);
  set_fs (oldfs);
  if (size < 0)
    {
      PERROR ("Error in sock_sendmsg: err(%d)\n", -size);
      return 0;
    }

  return size - ETH_HLEN;
}






/***********************************************************************
 * Master function to send data in to the socket.
 *
 * @param ur_socket
 * @param data pointer to data that needs sending
 * @param len length of data.
 * @param bwait Indicates if the function has to wait for new data, or
 *              it's just polling.
 * @todo Make it reentrent.
 ***********************************************************************/
static int
__ur_rcv_data (struct ur_device_socket_t *ur_socket, void *data,
	       size_t len, int bwait)
{
  struct msghdr msg;
  struct ethhdr ehdr;



  /*
   *         This should be replaced with the correct device code.
   */
  struct iovec iov[2];
  mm_segment_t oldfs;
  int size = 0;




  struct sockaddr_ll saddr;

  if (ur_socket->ethersock->sk == NULL)
    return 0;

  iov[0].iov_base = &ehdr;
  iov[0].iov_len = ETH_HLEN;

  iov[1].iov_base = data;
  iov[1].iov_len = len;


  /*
   *         Make sure we are listening in on the right interface
   *         And everything
   */
  saddr.sll_ifindex = ur_socket->interface_index;;
  saddr.sll_protocol = PROTOCOL;
  memcpy (saddr.sll_addr, ur_socket->interface_addr, ETH_ALEN);




  /*
   *         Im not sure how much of this is actually needed.
   *         Maybe we should try to use SOCKET_DGRAM
   */
  msg.msg_flags = (bwait == TRUE ? MSG_DONTWAIT : 0);
  msg.msg_name = &saddr;
  msg.msg_namelen = sizeof (struct sockaddr_ll);
  msg.msg_control = NULL;
  msg.msg_controllen = 0;
  msg.msg_iov = iov;
  msg.msg_iovlen = 2;
  msg.msg_control = NULL;



  /*
   *         Request a packet from the kernel
   */
  oldfs = get_fs ();
  set_fs (KERNEL_DS);
  size =
    sock_recvmsg (ur_socket->ethersock, &msg, len + ETH_HLEN, msg.msg_flags);
  set_fs (oldfs);

  /**
   * \todo Handle the ethernet hdr so that we can send data directly
   *       instead of sending to broadcast.
   */


  /*
   *         If the data in the buffer was not available 
   *         at this moment, just skip it.
   */
  if (size == -EAGAIN)
    return 0;

  if (size < 0)
    {
      return size;
    }

  PDEBUG("Size is %d\n",size);

  /*
   *         Return the data size WITHOUT the header size.
   *         The data is just stored in another buffer (iov[0])
   */
  return size - ETH_HLEN;
}


/**
 * @deprecated
 */
void
ur_ether_start (struct ur_device_socket_t *sock)
{
  if (sock->running == TRUE)
    return;			// allready running

  sock->timer.expires = jiffies + HZ / 100;
  add_timer (&sock->timer);
  sock->running = TRUE;
}

/**
 * @deprecated
 */
void
ur_ether_stop (struct ur_device_socket_t *sock)
{
  if (sock->running == 0)
    return;

  sock->running = 0;
  wait_for_completion (&sock->done_ethernet);
}


static struct ether_ops eops = {
  .start = ur_ether_start,
  .stop = ur_ether_stop,
};


/******************************************************************
 * Initialises ethernet stack
 *
 * It runs (so far) without locks
 ******************************************************************/
int __init
setup_ur_ethernet (void)
{
  struct sockaddr_ll local_addr;
  int err;
  struct ur_device_socket_t *sock;




  /*
   *         Start by allocating the needed memory for our use
   */
  sock = kmalloc (sizeof (struct ur_device_socket_t), GFP_KERNEL);
  if (IS_ERR (sock))
    {
      err = -ENOMEM;
      sock = NULL;
      goto setup_out;
    }
  ur_main->ether = sock;

  memset (sock, 0, sizeof (struct ur_device_socket_t));

  sock->ops = &eops;
  sock->main = ur_main;



  /*
   *         We create a completion entry point for the 
   *         system to signal when its done
   */
  init_completion (&sock->done_ethernet);



  /*
   *         Lets find out wich device index we need
   */
  sock->net_device = dev_get_by_name (net_device_name);
  if (sock->net_device == NULL)
    {
      PERROR ("Could not obtain device %s.\n", net_device_name);
      err = -ENODEV;
      sock->net_device = NULL;
      goto setup_out;
    }

  sock->interface_index = sock->net_device->ifindex;
  memcpy (sock->interface_addr, sock->net_device->dev_addr,
	  sock->net_device->addr_len);

  /*
   *         Start the device if its not up
   */
  if (!(sock->net_device->flags & IFF_UP))
    {
      // indicate that the device is ours!
      rtnl_lock ();
      if (dev_open (sock->net_device) != 0)
	{
	  PERROR ("could not start device %s.\n", net_device_name);
	  err = -ENODEV;
	  goto setup_out;
	}
      rtnl_unlock ();
      sock->device_control = TRUE;
    }

#if 0
  PDEBUG
    ("got interface address: %02X:%02X:%02X:%02X:%02X:%02X for device %s\n",
     sock->interface_addr[0], sock->interface_addr[1],
     sock->interface_addr[2], sock->interface_addr[3],
     sock->interface_addr[4], sock->interface_addr[5], net_device_name);
#endif




  /*
   *         We try to create a kernel BSD socket
   */
  err =
    sock_create_kern (PF_PACKET, SOCK_RAW, htons (PROTOCOL),
		      &(sock->ethersock));
  if (err)
    {
      PERROR ("could not create a packet socket\n");
      goto setup_out;
    }


  sock->ethersock_setup = TRUE;	// state we have a running socket config
  sock->counter = 0;		// the number of transmitted packets is 0


  /*
   *         We mark the interface we wants to use
   */
  memset (&local_addr, 0, sizeof (struct sockaddr_ll));
  local_addr.sll_family = PF_PACKET;
  local_addr.sll_protocol = htons (PROTOCOL);
  local_addr.sll_ifindex = sock->interface_index;

  err =
    sock->ethersock->ops->bind (sock->ethersock,
				(struct sockaddr *) &local_addr,
				sizeof (struct sockaddr_ll));
  if (err)
    {
      PERROR ("cannot bind the socket to the right interface\n");
      goto setup_out;
    }



  /*
   *         Initialize the timer that is going to drive our
   *         reading process.
   */
  init_timer (&sock->recv_timer);
  sock->recv_timer.data = (unsigned long) sock;
  sock->recv_timer.function = ur_timer_recv;



  /* 
   *         We are ready to start the actual timer 
   *         that will ship messages out on the wire
   */
  init_timer (&sock->timer);
  sock->running = 0;		// used in the timer block to indikate termination
  sock->timer.data = (unsigned long) sock;
  sock->timer.function = ur_timer_loop;


  /*
   *         When we have gotten this far, we are successfull!!!
   */
  return 0;

setup_out:

  if (err < 0)
    PERROR ("aborted with error code %d\n", err);


  if (sock == NULL)
    return err;

  if (sock->net_device)
    {
      if (sock->device_control)
	{
	  rtnl_lock ();
	  dev_close (sock->net_device);
	  rtnl_unlock ();
	  sock->device_control = 0;
	}

      dev_put (sock->net_device);
      sock->net_device = NULL;
    }

  if (sock->ethersock_setup)
    sock_release (sock->ethersock);

  sock->ethersock_setup = 0;

  sock->ethersock = NULL;

  kfree (sock);
  sock = NULL;

  if (ur_main)
    ur_main->ether = NULL;


  return err;
}



/***********************************************************
 * Termination function for UR protocol.
 *
 * Releases all resources
 **********************************************************/
int __exit
shutdown_ur_ethernet (void)
{
  int err = 0;
  int cnt;
  struct ur_device_socket_t *ur_socket;

  /*
   *         We should at least have something to work on
   */
  if (ur_main == NULL)
    return 0;

  ur_socket = ur_main->ether;


  /*
   *         If the bad thing happens, and we got no mem what so ever
   *         Just abort and say we are fine..
   */
  if (ur_socket == NULL)
    return 0;


  /*
   *         Mark the timer so it will know it should terminate
   */
  if (ur_socket->running == TRUE)
    {
      ur_socket->running = FALSE;
      wait_for_completion (&ur_socket->done_ethernet);
    }
  else if (ur_socket->errmarker)
    {
      PDEBUG ("the timer was already dead - errorcode: %d\n",
	      ur_socket->errmarker);
      err = ur_socket->errmarker;
    }

  if (ur_socket->net_device)
    {
      dev_put (ur_socket->net_device);
      if (ur_socket->device_control)
	{
	  rtnl_lock ();
	  dev_close (ur_socket->net_device);
	  rtnl_unlock ();
	  ur_socket->device_control = 0;
	}

      ur_socket->net_device = 0;
    }


  /*
   *         free allocated socket before exit
   */
  if (ur_socket->ethersock != NULL)
    {
      sock_release (ur_socket->ethersock);
      ur_socket->ethersock = NULL;
    }
  cnt = ur_socket->counter;
  kfree (ur_main->ether);
  ur_main->ether = NULL;

  printk (KERN_INFO MODULE_NAME ":ur_send has been run %d times\n", cnt);
  return err;
}
