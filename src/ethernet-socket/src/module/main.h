#ifndef MAIN_H_MODULE_INCLUDED
#define MAIN_H_MODULE_INCLUDED

struct ur_device_socket_t;

#define MODULE_NAME "ur_module"

#define PDEBUG(fmt, args...) \
  printk(KERN_DEBUG MODULE_NAME ":%s()::" fmt, \
	 __func__, ## args)

#define PERROR(fmt, args...) \
  printk(KERN_ERR MODULE_NAME ":%s()::" fmt, \
	 __func__, ## args)

struct ur_interface
{
  struct ur_device_socket_t *ether;
  struct ur_cdev *cdev;

  int buffer_size;

  int buffers_init;
  unsigned char *rbuffer;
  unsigned char *wbuffer;
  int holding;

  struct completion complete_send;
};

extern struct ur_interface *ur_main;

// buffer functions
int ur_create_buffers (struct ur_interface *);
void ur_free_buffers (struct ur_interface *);
unsigned char *select_rbuffer (struct ur_interface *);
unsigned char *select_wbuffer (struct ur_interface *);
void release_rbuffer (struct ur_interface *);
void release_wbuffer (struct ur_interface *);

unsigned char *pick_wbuffer (struct ur_interface *);
unsigned char *pick_rbuffer (struct ur_interface *);
void unpick_wbuffer (struct ur_interface *iff);
void unpick_rbuffer (struct ur_interface *iff);

size_t buffer_size (struct ur_interface *ur);

#endif
