#ifndef __EHTERNET_HPP_INCLUDED__
#define __EHTERNET_HPP_INCLUDED__

#include <string>

#include "macaddress.hpp"

using namespace std;

class etherframe;
class URPacket;

class EthernetIF
{
  bool initialised;
  int ethsock;
  int interfaceIndex;
  macAddress ownMac;
  void send_frame ( const etherframe & frame );
  const etherframe read_frame (  );
public:
    EthernetIF ( const string & interfaceName );
   ~EthernetIF (  );

  void send_package ( const URPacket & packet );

};

#endif
