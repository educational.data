#ifndef __FRAME_HPP_INCLUDED__
#define __FRAME_HPP_INCLUDED__


#include "macaddress.hpp"
#include "rwproperty.hpp"
#include <string>

class etherframe
{
  macAddress h_dest;		/* destination eth addr */
  macAddress h_source;		/* source ether addr    */
  unsigned short h_proto;	/* packet type ID field */
  int *accCount;
  size_t hdrSize;
  size_t dataSize;
  size_t frSize;
public:
    etherframe ( const etherframe & frame );
    etherframe ( const int size );
    etherframe ( const int size, const macAddress & dest,
		 const macAddress & source, unsigned short proto );
   ~etherframe (  );

  const macAddress & destination_mac (  ) const;
  const macAddress & source_mac (  ) const;
  void destination_mac ( const macAddress & m );
  void source_mac ( const macAddress & m );

  void type ( const int type );
  const int type (  ) const;
  const std::string string_type (  ) const;

  void insert ( const void *prt, const size_t size );

  unsigned char *Data;
  unsigned char *RawData;
  unsigned int size;

  const static int TYPE_IPv4 = 0x800;
  const static int TYPE_ARP = 0x206;
  const static int TYPE_URPACKET = 0x108;
};

#endif // __FRAME_HPP_INCLUDED__
