#ifndef IP_HPP_INCLUDED
#define IP_HPP_INCLUDED

#include <string>
#include <iostream>

class IP
{
  uint32_t ip;
public:
    IP ( const uint32_t ipRaw );
    IP ( const IP & ip );
    IP ( const uint8_t A, const uint8_t B, const uint8_t C, const uint8_t D );
   ~IP (  );
  const std::string toString (  ) const;
  const uint8_t getA (  ) const;
  const uint8_t getB (  ) const;
  const uint8_t getC (  ) const;
  const uint8_t getD (  ) const;
};

std::ostream & operator<< ( std::ostream & stream, const IP & ip );

#endif // IP_HPP_INCLUDED
