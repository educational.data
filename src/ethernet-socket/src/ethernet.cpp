#ifdef HAVE_CONFIG_H
# include "config.h"
#else
# error Must be compiled with a config.h file available
#endif

#include <errno.h>

#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <net/ethernet.h>
#include <netpacket/packet.h>
#include <signal.h>

#include "ethernet.hpp"
#include "etherframe.hpp"
#include "urpacket.hpp"

#include <iostream>


using namespace std;

EthernetIF::EthernetIF ( const string & interfaceName ):
initialised ( false ),
ownMac ( 0, 0, 0, 0, 0, 0 )
{
  int errnumber;

  ethsock = socket ( PF_PACKET, SOCK_RAW, htons ( ETH_P_ALL ) );
  errnumber = errno;
  if ( ethsock == -1 )
    throw strerror ( errnumber );

  // fra nu af skal vi lukke ned hver gang vi bliver lukket ned
  initialised = true;

  struct ifreq ifreq;

  strcpy ( ifreq.ifr_name, interfaceName.c_str (  ) );
  /* sprg om interface index, det skal bruges vi vil binde os til et
   * bestemt netkort, det kunne jo vre vi havde lyst til at route
   * mellem to net
   */
  int rc = ioctl ( ethsock, SIOCGIFINDEX, &ifreq );

  errnumber = errno;
  if ( rc == -1 )
    throw strerror ( errnumber );

  cout << "Bound to " << ifreq.ifr_name << endl;

  interfaceIndex = ifreq.ifr_ifindex;


  /* Undersøg om hvilken macadresse vi befinder os på. 
   * Vi vil gerne identificere os rigtigt.
   */
  rc = ioctl ( ethsock, SIOCGIFHWADDR, &ifreq );
  if ( rc < 0 )
    throw strerror ( errnumber );

  memcpy ( const_cast < unsigned char *>( ownMac.getMac (  ) ),
	   ifreq.ifr_hwaddr.sa_data, ETH_ALEN );

  /* Vi registrerer os på det pågældende interface med fuld 
   * adgang.
   */
  struct sockaddr_ll ethaddr;
  memset ( &ethaddr, 0x0, sizeof ( struct sockaddr_ll ) );
  ethaddr.sll_family = AF_PACKET;
  ethaddr.sll_halen = ETH_ALEN;
  ethaddr.sll_ifindex = interfaceIndex;
  ethaddr.sll_protocol = htons ( ETH_P_ALL );
  rc = bind ( ethsock, ( struct sockaddr * ) &ethaddr,
	      sizeof ( struct sockaddr_ll ) );

  if ( rc == -1 )
    throw strerror ( errnumber );

  // rc = fcntl(ethsock,F_SETFL,O_NONBLOCK|O_ASYNC|O_RDWR);
  errnumber = errno;
  if ( rc < 0 )
    throw strerror ( errnumber );

  rc = fcntl ( ethsock, F_SETSIG, 0 );
  errnumber = errno;
  if ( rc < 0 )
    throw strerror ( errnumber );

  rc = fcntl ( ethsock, F_SETOWN, getpid (  ) );
  errnumber = errno;
  if ( rc < 0 )
    throw strerror ( errnumber );

}

EthernetIF::~EthernetIF (  )
{
  if ( initialised )
    close ( ethsock );
}


const etherframe
EthernetIF::read_frame (  )
{
  /* nu er ethsock klar!
   * Vi kan f.eks. modtage en ethernet frame
   */
  int rc;
  int received;
  struct ethhdr *ethhdr;
  struct sockaddr_ll ethaddr;

  etherframe frame ( 1500 );

  socklen_t socksize = sizeof ( struct sockaddr_ll );
  bool noGood = true;

  while ( noGood == true ) {
    memset ( &ethaddr, 0x0, sizeof ( struct sockaddr_ll ) );
    /*
       ethaddr.sll_family = AF_PACKET;
       ethaddr.sll_halen = ETH_ALEN;
       ethaddr.sll_ifindex = interfaceIndex;
       ethaddr.sll_protocol = htons ( ETH_P_ECONET );
     */
    /* vi antager vi har char * frame der er ETH_FRAME_LEN lang */
    received = recvfrom ( ethsock, frame.RawData, frame.size, 0,
			  ( struct sockaddr * ) &ethaddr, &socksize );

    int errnumber = errno;

    if ( received == EAGAIN ) {
      cout << "nothing was available at time of execution" << endl;
    } else if ( received < 0 )
      throw strerror ( errnumber );

    macAddress dest = frame.destination_mac (  );
    macAddress src = frame.source_mac (  );

    // if data is sent directly to us
    if ( dest == ownMac )
      noGood = false;

    // if data is sent to broadcast, but not from us.
    if ( dest == macAddress::BROADCAST && src != ownMac )
      noGood = false;

  }

  return frame;
}

void
EthernetIF::send_frame ( const etherframe & frame )
{
  int rc;

  struct sockaddr_ll ethaddr;
  struct sockaddr_ll ethddr;

  memset ( &ethaddr, 0x0, sizeof ( struct sockaddr_ll ) );
  ethaddr.sll_family = AF_PACKET;
  ethaddr.sll_halen = ETH_ALEN;
  ethaddr.sll_ifindex = interfaceIndex;
  ethaddr.sll_protocol = htons ( ETH_P_ALL );

  /* vi antager vi har en char * frame af lngden frame_length */
  rc = sendto ( ethsock, frame.RawData, 14, MSG_DONTWAIT,
		( struct sockaddr * ) &ethaddr,
		sizeof ( struct sockaddr_ll ) );
  int errnumber = errno;

  if ( rc < 0 )
    throw strerror ( errnumber );
}


void
EthernetIF::send_package ( const URPacket & packet )
{
  etherframe frame ( packet.size (  ) );

  frame.insert ( packet.rawData, packet.size (  ) );
  frame.source_mac ( ownMac );
  frame.destination_mac ( macAddress::BROADCAST );
  frame.type ( etherframe::TYPE_URPACKET );
  send_frame ( frame );
}
