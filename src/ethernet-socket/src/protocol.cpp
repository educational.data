#ifdef HAVE_CONFIG_H
# include "config.h"
#else
# error Must be compiled with a config.h file available
#endif

#include <unistd.h>
#include <fcntl.h>

#include "ethernet.hpp"
// #include "ippacket.hpp"
// #include "ip.hpp"
#include "urpacket.hpp"
#include <iostream>

#include <signal.h>
#include <errno.h>

#ifdef HAVE_LIBCAP
# undef _POSIX_SOURCE
# include <sys/capability.h>
#else
# warning We are compiling without libcap wich is dangerous
# warning The program is open to exploits
#endif



using namespace std;

void
checkRights (  )
{
  /* this program should not be made setuid-0 */
  if ( getuid (  ) && !geteuid (  ) ) {
    cerr << "Cannot run setuid-0" << endl;
    exit ( -1 );
  }
  if ( getuid (  ) != 0 ) {
    cerr << "Error, this program must be run as root" << endl;
    exit ( -1 );
  }
}

#ifdef HAVE_LIBCAP
void
setupCapabilities (  )
{
  cap_t cap = cap_from_text ( "= cap_net_raw=ep" );

  if ( capsetp ( 0, cap ) == -1 ) {
    int errNum = errno;

    cout << "Capabilities not set: " << strerror ( errNum ) << endl;
    // exit ( -1 );
  }
  cap_free ( cap );
}
#endif /* HAVE_LIBCAP */


void
io_sig_arrv ( int ignored )
{
  // performs nothing - yet
  cout << "called io_sig_argv" << endl;
}

void
io_sig_hup ( int ignored )
{
  // performs nothing - yet
  cout << "called io_sig_hup" << endl;
  exit ( 0 );
}

int
main ( char *argv[], int argc )
{
  checkRights (  );
#ifdef HAVE_LIBCAP
  setupCapabilities (  );
#endif

  if ( signal ( SIGIO, io_sig_arrv ) == SIG_ERR ) {
    cerr << "Cannot set signal" << endl;
    exit ( -1 );
  }


  if ( signal ( SIGTERM, io_sig_hup ) == SIG_ERR ) {
    cerr << "Cannot set signal" << endl;
    exit ( -1 );
  }

  try {
    EthernetIF eth0 ( "eth0" );

    //eth0.SendFrame ( out );

    /* for ( ;; ) {
       //pause();
       etherframe in = eth0.ReadFrame (  );
       cout << in.string_type();
       cout << " frame from: " << in.source_mac (  );
       if(in.type()==etherframe::TYPE_IPv4) {
       ippacket ip(in.Data);
       cout << " " << ip;
       }
       cout << endl;
       } */
    for ( ;; ) {
      URPacket packet ( 10, 0, 1 );

      eth0.send_package ( packet );
      usleep ( 100 );
    }
  }
  catch ( const char *errmsg )
  {
    cerr << "Exception thrown: " << errmsg << endl;
  }

  return 0;
}
