
/*****************************************************
 ** IP protocol layer package
 ** $Log: ippacket.hpp,v $
 ** Revision 1.2  2006-08-09 20:20:02  tobibobi
 ** Added a bit of documentation, but in all, the system is mainly running fine. Its still needing some locking, it will come at a later time.
 **
 ** Revision 1.1  2006/07/20 20:57:42  tobibobi
 ** *** empty log message ***
 **
 *****************************************************/
#ifndef IPPACKET_HPP_INCLUDED
#define IPPACKET_HPP_INCLUDED

#include <iostream>
#include <string>

class IP;			// include ip.hpp in order to use in your cpp file.

class ippacket
{
  unsigned char *buffer;
  struct ipheader;
  ipheader *hdr;
  void set ( const void *data );
public:
    ippacket ( const void *data );
    ippacket ( const ippacket & packet );

  const IP destination_ip (  ) const;
  const IP source_ip (  ) const;
  const uint8_t hdr_len (  ) const;
  const uint16_t datagram_len (  ) const;
  const uint8_t type_of_service (  ) const;
  const bool fragmented (  ) const;

  void toStream ( std::ostream & stream ) const;
  uint16_t calculate_checksum (  );
  bool validate (  ) const;
};
std::ostream & operator<< ( std::ostream & stream,
			    const ippacket & ippacket );

#endif // IPPACKET_HPP_INCLUDED
