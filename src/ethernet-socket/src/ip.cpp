#ifdef HAVE_CONFIG_H
# include "config.h"
#else
#warn this source is without config.h
#endif

#include "ip.hpp"

IP::IP ( const uint32_t rawIP )
{
  ip = rawIP;
}

IP::IP ( const IP & oldIP )
{
  ip = oldIP.ip;
}

IP::IP ( const uint8_t A, const uint8_t B, const uint8_t C, const uint8_t D )
{
  ip = A | B << 8 | C << 16 | D << 24;
}

const uint8_t
IP::getA (  ) const const
{
  return ip & 0xff;
}

const uint8_t
IP::getB (  ) const const
{
  return ip >> 8 & 0xff;
}

const uint8_t
IP::getC (  ) const const
{
  return ip >> 16 & 0xff;
}

const uint8_t
IP::getD (  ) const const
{
  return ip >> 24 & 0xff;
}

const
  std::string
IP::toString (  ) const const
{
  char ipt[20];

  sprintf ( ipt, "%d.%d.%d.%d", getA (  ), getB (  ), getC (  ), getD (  ) );
  return std::string ( ipt );
}

IP::~IP (  )
{

}

using namespace std;
bool
testIP (  )
{
  IP ip ( 192, 168, 0, 1 );

  if ( ip.getA (  ) != 192 ||
       ip.getB (  ) != 168 || ip.getC (  ) != 0 || ip.getD (  ) != 1 ) {
    cerr << "IP failed! - expected 192.168.0.1 got " << ip.
      toString (  ) << endl;
    return false;
  }
  return true;
}

ostream & operator<< ( ostream & stream, const IP & ip )
{
  stream << ip.toString (  );
  return stream;
}
