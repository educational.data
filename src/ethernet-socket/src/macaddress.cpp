
/***********************************************************
 ** Author: Tobias G. Nielsen
 **
 ** $Log: macaddress.cpp,v $
 ** Revision 1.5  2006-08-09 20:20:02  tobibobi
 ** Added a bit of documentation, but in all, the system is mainly running fine. Its still needing some locking, it will come at a later time.
 **
 ** Revision 1.4  2006/07/20 20:57:42  tobibobi
 ** *** empty log message ***
 **
 ** Revision 1.3  2006/07/19 14:17:37  tobibobi
 ** Cleaned up data
 **
 ** Revision 1.2  2006/07/18 16:55:44  tobibobi
 ** *** empty log message ***
 **
 **********************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#else
# error Must be compiled with a config.h file available
#endif

#include "macaddress.hpp"

using namespace std;

const macAddress
macAddress::BROADCAST ( 0xff, 0xff, 0xff, 0xff, 0xff, 0xff );

/***********************************************************
 * Direct constructor.
 ***********************************************************/
macAddress::macAddress ( int a, int b, int c, int d, int e, int f )
{
  octet[0] = a;
  octet[1] = b;
  octet[2] = c;
  octet[3] = d;
  octet[4] = e;
  octet[5] = f;
}


/*************************************************************
 * This is the constructor abreviated function
 *************************************************************/
const macAddress &
macAddress::operator= ( const macAddress & add )
{
  octet[0] = add.octet[0];
  octet[1] = add.octet[1];
  octet[2] = add.octet[2];
  octet[3] = add.octet[3];
  octet[4] = add.octet[4];
  octet[5] = add.octet[5];
  return *this;
}


/************************************************************
 * Copy constructor
 ************************************************************/
macAddress::macAddress ( const macAddress & add )
{
  octet[0] = add.octet[0];
  octet[1] = add.octet[1];
  octet[2] = add.octet[2];
  octet[3] = add.octet[3];
  octet[4] = add.octet[4];
  octet[5] = add.octet[5];
}

bool macAddress::operator== ( const macAddress & add ) const const
{
  return
    octet[0] == add.octet[0] &&
    octet[1] == add.octet[1] &&
    octet[2] == add.octet[2] &&
    octet[3] == add.octet[3] &&
    octet[4] == add.octet[4] && octet[5] == add.octet[5];
}

bool macAddress::operator!= ( const macAddress & add ) const const
{
  return !operator== ( add );
}


const
  string
macAddress::toString (  ) const const
{
  char str[20];

  sprintf ( str, "%02x:%02x:%02x:%02x:%02x:%02x",
	    octet[0], octet[1], octet[2], octet[3], octet[4], octet[5] );
  return str;
}

ostream & operator<< ( ostream & stream, const macAddress & add )
{
  stream << add.toString (  );
  return stream;
}
