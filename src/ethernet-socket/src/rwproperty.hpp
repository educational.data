#ifndef __RWPROPERTY_H__
#define __RWPROPERTY_H__

template < class base, typename t > class rwproperty {
  typedef t ( base::*getFunc ) (  );
  typedef void ( base::*setFunc ) ( const t & valuse );
  setFunc sf;
  getFunc gf;
  base *obj;

public:
  rwproperty ( base * pthis, getFunc get_f, setFunc set_f ) {
    sf = set_f;
    gf = get_f;
    obj = pthis;
  }
  t & operator-> (  )const
  {
    return ( obj->*gf ) (  );
  }
  const t & operator= ( const t & value )
  {
    ( obj->*sf ) ( value );
    return obj->*gf (  );
  }
};

#endif // __RWPROPERTY_H__
