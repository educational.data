
/***********************************************************
 ** $Author: tobibobi $
 ** $Log: ippacket.cpp,v $
 ** Revision 1.2  2006-08-09 20:20:02  tobibobi
 ** Added a bit of documentation, but in all, the system is mainly running fine. Its still needing some locking, it will come at a later time.
 **
 ** Revision 1.1  2006/07/20 20:57:42  tobibobi
 ** *** empty log message ***
 **
 ***********************************************************/
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "ippacket.hpp"
#include "ip.hpp"
#include <cstring>

using namespace std;

struct ippacket::ipheader
{
  uint8_t vlen;
  uint8_t tos;
  uint16_t dlen;

  uint16_t id;
  uint16_t fl_offset;
  uint8_t ttl;
  uint8_t upper_layer_protocol;
  uint16_t checksum;
  uint32_t source_ip;
  uint32_t destination_ip;
  uint32_t options;
};

ippacket::ippacket ( const void *packet )
{
  set ( packet );
}
ippacket::ippacket ( const ippacket & packet )
{
  set ( packet.hdr );
}

void
ippacket::set ( const void *data )
{

  hdr = reinterpret_cast < ipheader * >( const_cast < void *>( data ) );
}

const IP
ippacket::destination_ip (  ) const const
{
  return IP ( hdr->destination_ip );
}

const IP
ippacket::source_ip (  ) const const
{
  return IP ( hdr->source_ip );
}

const uint8_t
ippacket::hdr_len (  ) const const
{
  return hdr->vlen & 0xF * 4;
}

const uint8_t
ippacket::type_of_service (  ) const const
{
  return hdr->tos;
}

const uint16_t
ippacket::datagram_len (  ) const const
{
  return hdr->dlen;
}

const bool
ippacket::fragmented (  ) const const
{
  return hdr->fl_offset >> 9 & 1 == 1;
}

uint16_t ippacket::calculate_checksum (  )
{
  hdr->checksum = 0;
  uint16_t *
    value = reinterpret_cast < uint16_t * >( hdr );
  uint16_t
    res = 0;

  for ( int i = 0; i < hdr->dlen * 2; i++ ) {
    res += value[i];
  }
  hdr->checksum = !res;
  return res;
}

bool ippacket::validate (  ) constconst
{
  uint16_t
    checksum = hdr->checksum;

  hdr->checksum = 0;
  uint16_t *
    value = reinterpret_cast < uint16_t * >( hdr );
  uint16_t
    res = 0;

  for ( int i = 0; i < hdr->dlen * 2; i++ ) {
    res += value[i];
  }
  bool
    passed = false;

  if ( ( !( res ^ checksum ) ) == 0 )
    passed = true;
  hdr->checksum = checksum;
  return passed;
}

void
ippacket::toStream ( ostream & stream ) const const
{
  if ( validate (  ) ) {
    stream << "IPv4 { hdr_len:" << static_cast < int >( hdr_len (  ) );
    stream << " tos: " << static_cast < int >( type_of_service (  ) );

    if ( fragmented (  ) )
      stream << " fragmented";
    stream << " s_ip:" << source_ip (  );
    stream << " d_ip:" << destination_ip (  );
    stream << " }";
  } else {
    stream << "IPv4 corrupt package";
  }
}

ostream & operator<< ( ostream & stream, const ippacket & ippacket )
{
  ippacket.toStream ( stream );
  return stream;
}
