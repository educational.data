#ifndef URPACKET_HPP_IINCLUDED
#define URPACKET_HPP_IINCLUDED



class URPacket
{
public:
  const static uint8_t pt_ping = 1L;
  const static uint8_t pt_start = 1L >> 1;
  const static uint8_t pt_reset = 1L >> 2;
  const static uint8_t pt_data = 1L >> 3;

  struct pheader
  {
    uint8_t type;
    uint8_t src_id;
    uint8_t to_id;
    uint8_t size;
  } *hdr;

  void *rawData;
  void *data;

    URPacket ( uint8_t size, uint8_t fromid, uint8_t toid );

  const size_t size (  ) const;


};
#endif
