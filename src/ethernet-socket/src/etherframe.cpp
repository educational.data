#ifdef HAVE_CONFIG_H
# include "config.h"
#else
# error Must be compiled with a config.h file available
#endif

#include "etherframe.hpp"
#include <arpa/inet.h>
#include <exception>

class no_mem_exception:public
  std::exception
{
  const char *
  what (  ) const
  throw (  )
  {
    return "No memory available";
  }
};

etherframe::etherframe ( const int size ):
h_source ( 0, 0, 0, 0, 0, 0 ), h_dest ( 0, 0, 0, 0, 0, 0 )
{
  RawData =
    static_cast <
    unsigned char *>( malloc ( size + macAddress::size * 2 + 2 ) );
  Data = RawData + macAddress::size * 2 + 2;
  accCount = new int;

  *accCount = 1;
  frSize = size + macAddress::size * 2 + 2;
  hdrSize = macAddress::size * 2 + 2;
  dataSize = size;
}

etherframe::etherframe ( const etherframe & rhs ):
h_source ( rhs.h_source ),
h_dest ( rhs.h_dest )
{
  RawData = rhs.RawData;
  Data = rhs.Data;
  ++( *accCount );
  dataSize = rhs.dataSize;
  hdrSize = rhs.hdrSize;
  frSize = rhs.frSize;
}

etherframe::~etherframe (  )
{
  if ( --( *accCount ) == 0 ) {
    delete accCount;

    delete[]RawData;
  }
}

const macAddress &
etherframe::destination_mac (  ) const const
{
  memcpy ( const_cast < unsigned char *>( h_dest.getMac (  ) ), RawData,
	   macAddress::size );
  return h_dest;
}

const macAddress &
etherframe::source_mac (  ) const const
{
  memcpy ( const_cast < unsigned char *>( h_source.getMac (  ) ),
	   RawData + macAddress::size, macAddress::size );
  return h_source;
}

void
etherframe::type ( int type )
{

  RawData[macAddress::size * 2] = type / 255 & 0xff;
  RawData[macAddress::size * 2 + 1] = type & 0xff;
}

const int
etherframe::type (  ) const const
{
  return ntohs ( RawData[macAddress::size * 2 + 1] * 0xff +
		 RawData[macAddress::size * 2] );
}

const
  std::string
etherframe::string_type (  ) const const
{
  switch ( type (  ) ) {
  case 0x800:
    return "IPv4";
  case 0x206:
    return "ARP";
  default:
    return "unknown";
  }
}

void
etherframe::destination_mac ( const macAddress & m )
{
  h_dest = m;

  memcpy ( RawData, m.getMac (  ), macAddress::size );
}

void
etherframe::source_mac ( const macAddress & m )
{
  h_source = m;
  memcpy ( RawData + macAddress::size, m.getMac (  ), macAddress::size );
}

void
etherframe::insert ( const void *ptr, const size_t size )
{
  if ( size > dataSize )
    throw no_mem_exception (  );

  memcpy ( Data, ptr, size );
}
