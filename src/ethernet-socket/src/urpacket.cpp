#ifdef HAVE_CONFIG_H
# include "config.h"
#else
#endif
#include <iostream>
#include "urpacket.hpp"

#ifdef HAVE_MALLOC
#include <malloc.h>
#endif

URPacket::URPacket ( uint8_t size, uint8_t fromid, uint8_t toid )
{
  hdr =
    reinterpret_cast < pheader * >( malloc ( size + sizeof ( pheader ) ) );
  rawData = hdr;
  data = hdr + sizeof ( pheader );
  hdr->size = size;
  hdr->src_id = fromid;
  hdr->to_id = toid;
}

const size_t
URPacket::size (  ) const const
{
  return hdr->size + sizeof ( pheader );
}
