package drno;

import javax.swing.UIManager;

import drno.support.FileLoader;
import drno.swingui.guiController.LoginController;

public class GUIRunner {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String path = GUIRunner.class.getProtectionDomain().getCodeSource().getLocation().getFile();
        FileLoader.setPath(path);
        try {
        	String cn = UIManager.getSystemLookAndFeelClassName();
            //String cn = UIManager.getCrossPlatformLookAndFeelClassName();
            UIManager.setLookAndFeel(cn);
        } catch (Exception cnf) {
        	System.err.println("Could not set look and feel");
        }
        
		(new LoginController()).run();

	}

}
