/**
 * This is an attempt to document the package
 */
package drno.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Timestamp;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;


/**
 * Interface for a locked Tidsreservation object.
 * <p>
 * In order to use an object from this, one must 
 * lock it prior to using it.
 * <p>
 * This is only if the system is external
 * 
 * @author  silex@mip.sdu.dk
 * @author  tobibobi@mip.sdu.dk
 * @version $LastChangeRevision:$
 * @see     ITidsreservation
 */
public interface ITidsreservationEditable extends ITidsreservation, Remote {
	abstract public void setPatient(IPatient a) throws ConsistanceException, ObjectLockException, RemoteException;
	abstract public void setEmployee(IEmployee a) throws ConsistanceException, ObjectLockException, RemoteException;
	abstract public void setTidStart(Timestamp a) throws ConsistanceException, ObjectLockException, RemoteException;
	abstract public void setTidSlut(Timestamp a) throws ConsistanceException, ObjectLockException, RemoteException;
	abstract public void setReelTidStart(Timestamp a) throws ConsistanceException, ObjectLockException, RemoteException;
	abstract public void setReelTidSlut(Timestamp a) throws ConsistanceException, ObjectLockException, RemoteException;
	abstract public void setEmne(String a) throws ConsistanceException, ObjectLockException, RemoteException;
	abstract public void setType(int a) throws ConsistanceException, ObjectLockException,RemoteException;
}
