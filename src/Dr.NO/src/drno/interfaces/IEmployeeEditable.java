/*
 * Created on 2005-03-22
 * This file is created 2005-03-22 and descripes IEmployeeEditable
 * 
 * $Log: IEmployeeEditable.java,v $
 * Revision 1.1  2006-12-25 14:55:30  tobibobi
 * Initial checkin
 *
 */
package drno.interfaces;


import java.io.Serializable;
import java.rmi.Remote;
import java.rmi.RemoteException;

import drno.exception.ConsistanceException;


/**
 * The writeable interface to employee.
 * <p>
 * The way to obtain this interface one must use the locking mechanism of the object.
 * Trying to cast to this interface and directly use the set functions will throw 
 * an ObjectLockException and by that render it impossible to use. 
 * 
 * @author tobibobi
 */

public interface IEmployeeEditable extends IEmployee,Remote,Serializable {
    abstract public void setLogin(String login) throws ConsistanceException, RemoteException;
	abstract public void setPassword(String password) throws ConsistanceException, RemoteException;
	abstract public void setEfternavn(String efternavn) throws ConsistanceException, RemoteException;
	abstract public void setFornavn(String fornavn) throws ConsistanceException, RemoteException;
	abstract public void setTidsOpdeling(int tidsopdeling) throws ConsistanceException, RemoteException;
	abstract public void setType(int type) throws ConsistanceException, RemoteException;
    abstract public void setTelefonNummer(String nr) throws ConsistanceException, RemoteException;
}
