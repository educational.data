package drno.interfaces;
import java.rmi.Remote;
import java.rmi.RemoteException;

import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.server.event.Signal;


public interface LoginHandler extends Remote
{	
	//public IEmployee getUser()throws RemoteException;
	public PatientHandler getPatientHandler() throws RemoteException, SecurityDisallowedException, SecurityDisallowedException;
	public EmployeeHandler getEmployeeHandler() throws RemoteException, SecurityDisallowedException;
	public TidsreservationsHandler getTidsreservationsHandler() throws RemoteException, SecurityDisallowedException;
	public Signal getOnUpdate() throws RemoteException;
	public IEmployee getUser() throws RemoteException;
	/**
	 * @param username
	 * @param password
	 * @throws RemoteException
	 * @throws UnkErrException
	 * @throws SecurityDisallowedException
	 */
	public LoginHandler login(String username, String password) throws SecurityDisallowedException, UnkErrException, RemoteException;
	/**
	 * 
	 */
	public void fireUpdate() throws RemoteException;
	
}