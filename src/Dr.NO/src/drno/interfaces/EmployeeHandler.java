/*
 * Created on 2005-04-26
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.server.event.Signal;



/**
 * @author Sloggi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface EmployeeHandler  extends Remote{
	/**
	 * Creates an empty IPatient object.
	 * <p>
	 * The object can be edited or dropped. But remember that it will not be
	 * persisted before it has been called with save.
	 * 
	 * @return e a new editable IPatientEditable object
	 * @throws SecurityDisallowedException
	 * @throws Exception
     * @deprecated 
	 */
	public IEmployeeEditable createNewEmployee()throws RemoteException, SecurityDisallowedException;

	/**
	 * Deletes the patient from the database.
	 * 
	 * @param e
	 *            {@link IPatientEditable} object former marked as editable
	 * @throws UnkErrException
	 * @throws SecurityDisallowedException
	 * @throws Exception
	 */
	public void deleteEmployee(IEmployee e) throws ObjectLockException, RemoteException, SecurityDisallowedException, UnkErrException;

	/**
	 * Locks a patient object object.
	 * <p>
	 * By locking an object, it means that the <code>set..()</code>
	 * operations can be called on the returned object. If the operation is not called,
	 * and the class is simply typecast to {@link drno.interfaces.IPatientEditable}
	 * 
	 * @param employee the readonly employee object (IEmployee) that needs to be edited.
	 * @return An IPatient object ready to be edited. The object must be
	 *         released by a call to either
	 *         {@link #deleteEmployee(IEmployeeEditable)},
	 *         {@link #saveEmployee(IEmployeeEditable)}
	 * @throws SecurityDisallowedException
	 * 
	 * @throws Exception
	 * @see IPatientEditable
     * @deprecated
	 */
	public IEmployeeEditable editEmployee(IEmployee employee)
			throws ObjectLockException,RemoteException, SecurityDisallowedException;
    /**
     * 
     * @param employee
     * @return
     * @throws RemoteException
     * @throws SecurityDisallowedException
     * @throws UnkErrException
     * @throws ObjectLockException
     * 
     * @deprecated 
     */
	public IEmployee freeLockedEmployee(IEmployeeEditable employee)throws RemoteException, SecurityDisallowedException, UnkErrException, ObjectLockException;
	public Vector getAllEmployees() throws UnkErrException, RemoteException;
	public Vector getAllDoctors() throws UnkErrException, RemoteException;

	/**
	 * Persist objects if they are correct.
	 * <p>
	 * The object must be locked prior to calling this function. If the object
	 * is not, an ObjectLockException will be thrown.
	 * <p>
	 * The object can be locked by calling either {@link #createNewEmployee()} or
	 * {@link #editEmployee(IEmployee)}
	 * 
	 * @param employee
	 *            The employee to be saved. If the object is not locked, the
	 *            function will fail.
	 * @throws Exception
	 *             If the object is not locked, it will throw one of two
	 *             possible drno.exception.
	 * @see drno.exception.ObjectLockException
	 * @see drno.exception.DatabaseDisconnectedException
	 * @see #createNewEmployee()
	 * @see #editEmployee(IEmployee)
     * 
     * @deprecated
	 */
	public IEmployee saveEmployee(IEmployeeEditable employee) throws SecurityDisallowedException, 
		UnkErrException, RemoteException, ObjectLockException, ConsistanceException;
    
    /**
     * Gem en medarbejder ved at specificere hans nye egenskaber.
     * <p>
     * @link drno.interfaces.IEmployee skal v�re l�st inden dette kald. Hvis ikke at det er sket, vil 
     * en drno.exception blive kastet.
     *  
     * @param employee (@see drno.interfaces.IEmployee)
     * @param fornavn
     * @param efternavn
     * @param login
     * @param password
     * @param tlf
     * @param tidsopdeling
     * @param type (@see drno.interfaces.IEmployee)
     * @return the same employee object as passed in.
     * @throws SecurityDisallowedException
     * @throws UnkErrException
     * @throws RemoteException
     * @throws ObjectLockException
     * @throws ConsistanceException
     */
    public IEmployee saveEmployee(IEmployee employee,
            String fornavn, 
            String efternavn,
            String login,
            String password,
            String tlf,
            int tidsopdeling,
            int type) 
    throws SecurityDisallowedException, UnkErrException, 
    RemoteException, ObjectLockException, ConsistanceException; 
    

	/**
	 * searches for all employee fitting with name resolvement.
	 * <p>
	 * The name can contain both first and last name. Both will be tested.
	 * 
	 * @param name
	 *            An name or names of the person that is to be searched for.
	 * @return a Vector with objects fitting the description of the search
	 *         parameters.
	 * @throws UnkErrException
	 */
	public Vector searchEmployeesByName(String name)throws RemoteException, UnkErrException;
	
	public Signal getOnUpdate() throws RemoteException;
}