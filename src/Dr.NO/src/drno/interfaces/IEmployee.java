/* 
 * Created on 2005-03-17
 *
 * $Log: IEmployee.java,v $
 * Revision 1.1  2006-12-25 14:55:30  tobibobi
 * Initial checkin
 *
 */
package drno.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * ReadOnly interface for all Employees.
 * <p>
 * This interface is handed out of this package area, and thereby all users sees.
 * In order to write to this interface, one must use a Editable interface
 * 
 * @author Leo
 */
public interface IEmployee extends Remote{
	static final int TYPE_ADMIN = 0;
	/** Used by get/setType to indicate a doctor. */
	static final int TYPE_DOCTOR = 1;
	/** Used by get/setType to indicate a secretary. */
	static final int TYPE_SECRETARY = 2;
	/** Used by get/setType to indicate a nurse. */
	static final int TYPE_NURSE = 3;
	


	abstract public String getLogin()throws RemoteException;
	abstract public String getPassword()throws RemoteException;
	abstract public String getEfternavn()throws RemoteException;
	abstract public String getFornavn()throws RemoteException;
	abstract public int getTidsOpdeling()throws RemoteException;
	abstract public int getType()throws RemoteException;
	abstract public String getTelefonNummer()throws RemoteException;
	abstract public String asString()throws RemoteException;
	
}
