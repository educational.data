/*
 * IPatientEditable.java
 * 
 * $Log: IPatientEditable.java,v $
 * Revision 1.1  2006-12-25 14:55:29  tobibobi
 * Initial checkin
 *
 */
package drno.interfaces;

import java.rmi.RemoteException;

import drno.exception.ConsistanceException;


/**
 * The writeable interface to Patient.
 * <p>
 * The way to obtain this interface one must use the locking mechanism of the object.
 * Trying to cast to this interface and directly use the set functions will throw 
 * an ObjectLockException and by that render it impossible to use. 
 * 
 * @author tobibobi
 */

public interface IPatientEditable extends IPatient {
	abstract public void setCpr(String cpr) throws ConsistanceException, RemoteException;
	abstract public void setEfternavn(String efternavn) throws ConsistanceException, RemoteException;
	abstract public void setFornavn(String fornavn) throws ConsistanceException, RemoteException;
	abstract public void setSex(int sex) throws ConsistanceException, RemoteException;
	abstract public void setTlf(String tlf) throws ConsistanceException, RemoteException;	
	abstract public void setDoctor(IEmployee doctor) throws ConsistanceException, RemoteException;
	abstract public void setPostnummer(Integer postnummer) throws ConsistanceException, RemoteException;
	abstract public void setAdresse(String adresse) throws ConsistanceException, RemoteException;
}
