/*
 * Created on 2005-03-21
 * 
 * $Log: ITidsreservation.java,v $
 * Revision 1.1  2006-12-25 14:55:31  tobibobi
 * Initial checkin
 *
 */
package drno.interfaces;
import java.rmi.RemoteException;
import java.sql.Timestamp;

/**
 * ReadOnly interface for all Tidsreservation.
 * <p>
 * This interface is handed out of this package area, and thereby all users sees.
 * In order to write to this interface, one must use a Editable interface
 * 
 * @author Leo
 */
public interface ITidsreservation {
	static final int TYPE_CONSULTATION = 1;
	static final int TYPE_PHONECONSULTATION = 2;
	static final int TYPE_HOLIDAY = 3;
	
	abstract public IPatient getPatient() throws RemoteException;
	abstract public IEmployee getEmployee() throws RemoteException;
	abstract public Timestamp getTidStart() throws RemoteException;
	abstract public Timestamp getTidSlut() throws RemoteException;
	abstract public Timestamp getReelTidStart() throws RemoteException;
	abstract public Timestamp getReelTidSlut() throws RemoteException;
	abstract public String getEmne() throws RemoteException;
	abstract public String asString() throws RemoteException;
	abstract public int getType() throws RemoteException;
}
