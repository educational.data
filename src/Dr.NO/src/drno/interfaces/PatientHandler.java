package drno.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.server.event.Signal;


public interface PatientHandler extends Remote {
    public IPatientEditable createNewPatient() throws RemoteException;

    public void deletePatient(IPatientEditable e) throws ObjectLockException,
            UnkErrException,
            RemoteException;

    public IPatientEditable editPatient(IPatient patient) throws ObjectLockException,
            UnkErrException,
            RemoteException;

    public IPatient freeLockedPatient(IPatientEditable patient) throws RemoteException,
            UnkErrException,
            ObjectLockException;

    public Vector getAllPatients() throws RemoteException, UnkErrException;

    public IPatient savePatient(IPatientEditable patient) throws ObjectLockException,
            ConsistanceException,
            UnkErrException,
            RemoteException;

    public Vector searchPatientsByName(String name) throws ObjectLockException,
            ConsistanceException,
            UnkErrException,
            RemoteException;

    public Vector searchPatientsByCPR(String cpr) throws RemoteException,
            UnkErrException;

    public Signal getOnUpdate() throws RemoteException;
}
