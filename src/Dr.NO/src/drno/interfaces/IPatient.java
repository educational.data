/*
 * Created on 16-03-2005
 * 
 * $Log: IPatient.java,v $
 * Revision 1.1  2006-12-25 14:55:29  tobibobi
 * Initial checkin
 *
 */
package drno.interfaces;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Kun l�sbart interface til Patient objektet.
 * <p>
 * Dette interface er en repr�sentation af modellagets Patienter, 
 * men indeholder kun funktioner til at l�se fra objektet.
 * <p>
 * Hvis man �nsker at skrive til objektet, skal man anvende 
 * {@link drno.server.PatientHandlerImpl#editPatient(IPatient)} for at f� skriveadgang.  
 * 
 * @author tobibobi
 */
public interface IPatient extends Remote{
	/** repr�senterer mandlig k�n */
	static public final int SEX_MALE=0;
	/** repr�senterer kvindelige k�n */
	static public final int SEX_FEMALE=1;
	/** @return Adressen p� Patienten */
	abstract public String getAdresse() throws RemoteException;
	/**
	 * Alder - dette beregnes ud fra patientens cpr og kan derfor ikke 
	 * s�ttes direkte.
	 *
	 * @return Alder
	 */
	abstract public int getAlder() throws RemoteException;
	/** @return valid Cpr-nummer */
	abstract public String getCpr() throws RemoteException;
	/** @return Efternavn p� patienten */
	abstract public String getEfternavn() throws RemoteException;
	/** @return Fornavn p� patienten */
	abstract public String getFornavn() throws RemoteException;
	/** @return Postnummer p� patienten */
	abstract public Integer getPostnummer() throws RemoteException;
	/** Nummerisk v�rdi svarende til patientens k�n.
	 * <p>
	 * Mulige returv�rdier er:
	 * <ul>
	 * <li>{@link IPatient.SEX_MALE}</li>
	 * <li>{@link IPatient.SEX_FEMALE}</li>
	 * </ul>
	 * 
	 * @return k�nnet.
	 */
	abstract public int getSex() throws RemoteException;
	/** @return Telefon nummeret med 8 cifre til patienten */
	abstract public String getTlf() throws RemoteException;
	/** 
	 * En RMI venlig version af <i>toString()</i>.
	 * @return streng version af objektet
	 */
	abstract public String asString() throws RemoteException;
	/**
	 * Patientens s�dvanlige l�ge som b�r benyttes n�r man opretter en tidsreservation.
	 * @return IEmployee som repr�senterer patientens typiske l�ge.
	 */
	abstract public IEmployee getDoctor() throws RemoteException;

}
