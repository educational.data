/*
 * Created on 2005-04-25
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.swing.JOptionPane;

import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.IPatientEditable;
import drno.interfaces.PatientHandler;
import drno.server.*;
import drno.swingui.guiView.AddPatient;
import drno.swingui.guiView.MainCalendarView;
import drno.swingui.guiView.SearchView;
import drno.swingui.guiView.SelectView;





/**
 * PatientController binder GUI-views omkring tilf�j, ret, slet og s�g patient 
 * sammen med den underliggende database.
 * 
 * @author Bo
 *
 */
public class PatientController {

	private AddPatient viewAdd;
	private SearchView viewSearch;
	private SelectView viewSelect;
	private AddPatient viewEdit;
	
	private PatientHandler p_handler;
	private EmployeeHandler e_handler;
	private MainCalendarController control;
	
	public PatientController(PatientHandler ph, EmployeeHandler eh, final MainCalendarController control){
		this.control = control;
		p_handler = ph;
		e_handler = eh;
		viewAdd = new AddPatient();
		viewSearch = new SearchView("Hele eller dele af patientens navn:");
		viewEdit = new AddPatient();
		viewSelect = new SelectView(false);
		
		
	// ADD PATIENT EVENTS
		viewAdd.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewAdd.setVisible(false);
		      }
		    });
		viewAdd.setOKListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	addPatient();
		                return null;
		            }
		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });
	// EDIT PATIENT EVENTS
		viewEdit.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewEdit.setVisible(false);
		      }
		    });
		viewEdit.setOKListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	savePatient(viewEdit.getPatient());
						control.calendar_change();
		                return null;
		            }
		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });		
	// DELETE PATIENT EVENTS
		
	// SEARCH PATIENT EVENTS
		viewSearch.setSearchListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	searchPatient();
		                return null;
		            }
		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });
		viewSearch.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewSearch.setVisible(false);
		      }
		    });
		viewSelect.setEditListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Ret patient
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run(){
		            	viewSelect.setVisible(false);
		            	/*System.out.println((IPatient)viewSelect.getSelected());
		            	System.out.println("Ret Patient");*/
		            	showViewEdit((IPatient)viewSelect.getSelected());
		            		
		            }
		        });
		      }
		    });
		viewSelect.setDeleteListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Slet patient
		      	
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run(){
				        final SwingWorker worker = new SwingWorker() {
				            public Object construct() {
				            	viewSelect.setVisible(false);
				            	deletePatient((IPatient)viewSelect.getSelected());

				                return null;
				            }
				        };
				        worker.start();  //required for SwingWorker 3		      	

		            	
		            }
		        });
		      }
		    });
		viewSelect.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
				  viewSelect.setVisible(false);
		      }
		});
	}

// Funktioner til visning af GUI
	public void showViewAdd(){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
            	try{
            			viewAdd.clear();
	            		Vector e_doc = e_handler.getAllDoctors();
	            		for(int i=0; i<e_doc.size(); i++) {
	            			viewAdd.addDoctor((IEmployee)e_doc.elementAt(i));
	            		}
	            		viewAdd.setVisible(true);
            	}
            	catch (Exception e){
            		System.out.println(e.getMessage());
            		e.printStackTrace();
            	}
                
            }
        });
		
	}
	public void showViewSearch(){
		viewSearch.setVisible(true);
	}
	public void showViewEdit(final IPatient p){
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
            	try{
            			viewEdit.clear();
						viewEdit.setTitel("Ret Patient");
	            		Vector e_doc = e_handler.getAllDoctors();
            			IPatientEditable pe = p_handler.editPatient(p);
            			viewEdit.setPatient(pe,e_doc);
            			viewEdit.setVisible(true);
            	}
            	catch (Exception e){
            		System.out.println(e.getMessage());
            		e.printStackTrace();
            	}
                
            }
        });
		
	}

// Swingworker-funktioner	
	public void addPatient(){
        try{
			IPatientEditable p = p_handler.createNewPatient();

			p.setFornavn(viewAdd.getFornavn());
			p.setEfternavn(viewAdd.getEfternavn());
			p.setAdresse(viewAdd.getAdresse());
			p.setCpr(viewAdd.getCPR());
			p.setSex(viewAdd.getGender());
			p.setTlf(viewAdd.getTlf());
			p.setDoctor(viewAdd.getDoctor());
			p_handler.savePatient(p);
			JOptionPane.showMessageDialog(viewAdd, "Patient oprettet og gemt i database.");
			viewAdd.setVisible(false);
		}catch(Exception e){
			e.printStackTrace();
			JOptionPane.showMessageDialog(viewAdd, "Patient kunne ikke oprettes: "+e.toString());
			
		}					
			
	}
	public void searchPatient(){
		viewSearch.setVisible(false);
		//System.out.println(viewSearch.getSearchText());
		// S�g efter kriterier og vis patienter
		//Object [][] patienter = getPatientData(viewSearch.getSearchText());	
		try{
			viewSelect.setPatientData(p_handler.searchPatientsByName(viewSearch.getSearchText()));
			//viewSelect.setData(patienter);
			viewSelect.setVisible(true);			
		}
		catch (Exception e){
			e.printStackTrace();
		}

		
	}
	public void savePatient(IPatientEditable p){
		try {
			//JOptionPane.showMessageDialog(viewEdit, "Gemmer nu...");
			p_handler.savePatient(p);
			JOptionPane.showMessageDialog(viewEdit, "Patient �ndret og gemt i database.");
			viewEdit.setVisible(false);
		}
		catch (Exception e){
			e.printStackTrace();
			//System.out.println("Ikke muligt at gemme patient...");
		}
	}
	
	public void deletePatient(IPatient p){
		try {
			JOptionPane.showMessageDialog(viewEdit, "Sletter nu...");
			p_handler.deletePatient(p_handler.editPatient(p));
			JOptionPane.showMessageDialog(viewSelect, "Patient slettet fra databasen.");
			viewSelect.setVisible(false);
		}
		catch (ObjectLockException e){
			JOptionPane.showMessageDialog(viewSelect, "Patient kan ikke slettes fra databasen, da patient-objektet er l�st af anden bruger.");
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnkErrException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
		

	
}
