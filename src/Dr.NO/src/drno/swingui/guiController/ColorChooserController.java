package drno.swingui.guiController;


import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComponent;

import drno.swingui.guiView.ColorChooser;
import drno.swingui.guiView.MainCalendarView;

public class ColorChooserController {
	
	private MainCalendarView view;
	private ColorChooser CCView;
	
	
	public ColorChooserController(final MainCalendarView view){

		CCView = new ColorChooser();
		this.view = view;
		
		CCView.setAnnullerListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
						// System.out.println("Color Annuller");
		            	CCView.setVisible(false);
		            }	
		        });
				
			}
		});		
		CCView.setOKListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	// System.out.println("Set color");
						CCView.setVisible(false);
		            }	
		        });
				
			}
		});	
		CCView.setAnvendListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
						anvend();
						view.getContentPane().repaint();
		            }	
		        });
				
			}
		});	
		
	}
	
	public void showCCView(){
		// System.out.println("Indstille udseende...");
		CCView.setVisible(true);
	}
	
	public void anvend(){
		Color selectedColor = CCView.getChoosedColor();
		Component[] c = view.getAllComponents();
		/*
		for(int i=0; i<c.length;i++){
			System.out.println(c[i].getName());
		}*/
		String backgroundElement = CCView.getList1Element();
		String foregroundElement = CCView.getList2Element();
		//System.out.println(backgroundElement+" "+foregroundElement);
		if(!foregroundElement.equals("")){
			if(foregroundElement.equals("Menu")){
				view.setMenuForegroundColor(selectedColor);
			}
			if(foregroundElement.equals("Tabel")){
				view.setTabelForegroundColor(selectedColor);
			}
			if(foregroundElement.equals("Ansat")){
				view.setList1ForegroundColor(selectedColor);
			}
			if(foregroundElement.equals("Tidsreservation")){
				view.setList2ForegroundColor(selectedColor);
			}
			else{
				for(int i=0; i<c.length;i++){
					JComponent jcomp = (JComponent)c[i];
					if(jcomp.getName().equals(foregroundElement)){
						view.setForegroundColor(jcomp,selectedColor);
						
					}
				}
			}
			/*
			
			if(c[i].getName().contains("ScrollPane")){
				System.out.println(c[i].getComponentAt(0,0).getName());
			}*/
			
		}
		if(!backgroundElement.equals("")){
			if(backgroundElement.equals("Menu")){
				view.setMenuBackgroundColor(selectedColor);
			}
			if(backgroundElement.equals("Tabel")){
				view.setTabelBackgroundColor(selectedColor);
			}
			if(backgroundElement.equals("Ansat")){
				view.setList1BackgroundColor(selectedColor);
			}
			if(backgroundElement.equals("Tidsreservation")){
				view.setList2BackgroundColor(selectedColor);
			}
			else{
				for(int i=0; i<c.length;i++){
					JComponent jcomp = (JComponent)c[i];
					if(jcomp.getName().equals(backgroundElement)){
						view.setBackgroundColor(jcomp,selectedColor);
						
					}
				}
			}
		}
	}
	
}
