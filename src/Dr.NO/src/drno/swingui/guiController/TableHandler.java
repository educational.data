/*
 * TableHandler.java
 *
 * Created on 10. april 2005, 13:50
 */

package drno.swingui.guiController;

import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Vector;
import java.util.*;

import java.text.DateFormat;
import java.util.GregorianCalendar;

import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservation;
import drno.interfaces.LoginHandler;
import drno.interfaces.PatientHandler;
import drno.interfaces.TidsreservationsHandler;
import drno.server.*;


/**
 *
 * @author  Leo
 */
public class TableHandler {
    LoginHandler lh;
    PatientHandler p_handler;
    EmployeeHandler e_handler;
    TidsreservationsHandler t_handler;
    Hashtable reservations;
    //Object[][] data;
    DateFormat df2; 
     
    /** Creates a new instance of TableHandler 
     * @param lh
     * @param t_handler
     * @param e_handler
     * @param p_handler*/
    public TableHandler(PatientHandler p_handler, EmployeeHandler e_handler, TidsreservationsHandler t_handler, LoginHandler lh){//throws SecurityDisallowedException {
        try{
            this.lh = lh;
            this.p_handler = p_handler;
            this.e_handler = e_handler;
            this.t_handler = t_handler;
            reservations = new Hashtable();
            //data = new Object[28][2];
            df2 = DateFormat.getTimeInstance(DateFormat.SHORT);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public Object[] getDoctors(){
        try{
            Vector docs = e_handler.getAllDoctors();
            Object[] docData = new Object[docs.size()];
            docs.copyInto(docData);
            return docData;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
   
    public Object[][] getTableData(){    
        try{
            Vector ip = p_handler.getAllPatients();
            Object[][] Pdata = new Object[ip.size()][6];
            
                for(int i=0; i<ip.size(); i++) {
                    Pdata[i][0] = ((IPatient)ip.elementAt(i)).getFornavn()+" "+((IPatient)ip.elementAt(i)).getEfternavn();
                    Pdata[i][1] = ((IPatient)ip.elementAt(i)).getCpr();
                    Pdata[i][2] = ((IPatient)ip.elementAt(i)).getAdresse();
                    Pdata[i][3] = ((IPatient)ip.elementAt(i)).getTlf();
                    Pdata[i][4] = new Integer(((IPatient)ip.elementAt(i)).getSex());
                    Pdata[i][5] = ((IPatient)ip.elementAt(i)).getDoctor();
                }
             return Pdata;
        }
        catch(Exception e){
            e.printStackTrace();
        }
       return null;
    }
    
    public Object[][] makeCalendar(int kolloner, int tidsopdeling) {
        Object[][] data = new Object[33][kolloner];
        GregorianCalendar c = new GregorianCalendar();
        DateFormat df2 = DateFormat.getTimeInstance(DateFormat.SHORT);
        //Timestamp t = new Timestamp(c.getTimeInMillis());
        Timestamp t = new Timestamp(c.getTimeInMillis());
        t.setHours(07);
        t.setMinutes(00);
        String s = df2.format(t);
        for(int i=0; i<data.length;i++){
            data[i][0]= s;
            t.setMinutes(t.getMinutes()+tidsopdeling);
            s = df2.format(t);
        }
        return data;
    }
   
    public Object[][] makeCalendarFromLogin() {
        try{
            IEmployee e = lh.getUser();
            int tidsopdeling = e.getTidsOpdeling();
            Object[][] data = new Object[60/tidsopdeling*8][2];
            GregorianCalendar c = new GregorianCalendar();
            //Timestamp t = new Timestamp(c.getTimeInMillis());
            Timestamp t = new Timestamp(c.getTimeInMillis());
            t.setHours(8);
            t.setMinutes(00);
            String s = df2.format(t);
            //set tids kolloner:
            for(int i=0; i<data.length;i++){
                data[i][0]= s;
                t.setMinutes(t.getMinutes()+tidsopdeling);
                s = df2.format(t);
            }
            return data;
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
        return null;
    }
    public Object[][] makeCalendarFromEmployee(IEmployee e) {
        try{
            int tidsopdeling = e.getTidsOpdeling();
            Object[][] data = new Object[60/tidsopdeling*8][2];
            GregorianCalendar c = new GregorianCalendar();
            //Timestamp t = new Timestamp(c.getTimeInMillis());
            Timestamp t = new Timestamp(c.getTimeInMillis());
            t.setHours(8);
            t.setMinutes(00);
            String s = df2.format(t);
            //set tids kolloner:
            for(int i=0; i<data.length;i++){
                data[i][0]= s;
                t.setMinutes(t.getMinutes()+tidsopdeling);
                s = df2.format(t);
            }
            return data;
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
        return null;
    }
    //Laver en Util date objekt til et SQL timestamp objekt
    public java.sql.Timestamp DateToTimestamp(java.util.Date d){
        Calendar c = new GregorianCalendar();
        c.setTime(d);
        Timestamp h = new Timestamp(c.getTimeInMillis());
        //Dump tidsreservations databasen i tabellen     
        return h;  
    }
    
    public Object[][] getReservationFromDateAndDoc(Object[][] data, Timestamp t, IEmployee e){
        try{
            reservations.clear();
            Vector reservation = t_handler.getFromDate(t,e);
            if(reservation.size()==0){
                return data;
            }
            else{
                for(int i=0; i<reservation.size(); i++){
                    ITidsreservation rv = (ITidsreservation)reservation.elementAt(i);
                    String tbase =  df2.format(rv.getTidStart()); //tid fra databasen
      
                    for(int j=0; j<data.length;j++){
                        if(data[j][0].equals(tbase)){
                            reservations.put(data[j][0],rv);
                            IPatient p = rv.getPatient();
                            String navn = p.getFornavn()+" "+p.getEfternavn();
                            data[j][1] = navn;
                        }
                    }
                }
                return data;
            }
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
        return null;
    }
        
    public Object[][] getReservationFromDate(Object[][] data, Timestamp t){
        try{
            reservations.clear();
            Vector reservation = t_handler.getFromDate(t); //henter alle reservationer for idag
            if(reservation.size()==0){
                return null;
            }
            else{
                for(int i = 0; i<reservation.size(); i++){
                    ITidsreservation rv = (ITidsreservation)reservation.elementAt(i);
                    String tbase =  df2.format(rv.getTidStart()); //tid fra databasen
      
                    for(int j=0; j<data.length;j++){
                        if(data[j][0].equals(tbase)){
                            reservations.put(data[j][0],rv);
                            IPatient p = rv.getPatient();
                            String navn = p.getFornavn()+" "+p.getEfternavn();
                            data[j][1] = navn;
                        }
                    }
                }
                return data;
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }
    
    public Hashtable getHashtable(){
        if(reservations.isEmpty()){
			return null;
        }
        else{
			return reservations;
        }
    }
    
    public int getTidsopdeling(){
    	
        try {
			IEmployee e = lh.getUser();
			int tidsopdeling = e.getTidsOpdeling();
			return tidsopdeling;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return -1;
		}
    }
	
    
    public Vector getReservationFromPatient(IPatient p){
        try{
            Vector v = t_handler.getFromPatient(p);
            if(v.size()==0){
                return null;
            }
            else{
                return v;
            }
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
        return null;
    }
    public String PInfo_toString(IPatient p){
        try {
			String output = "PATIENT INFO:\n"+
			                "Fornavn:\t"+p.getFornavn()+"\n"+
			                "Efternavn:\t"+p.getEfternavn()+"\n"+
			                "CPR:\t"+p.getCpr()+"\n"+
			                "Adresse:\t"+p.getAdresse()+"\n"+
			                "TLF:\t"+p.getTlf()+"\n"+
			                "Alder:\t"+p.getAlder()+"\n\n";
			return output;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    
    public String TInfo_toString(ITidsreservation t){
        try {
			String output = "RESERVATION INFO:\n"+
			                "Tidstart:\t"+t.getTidStart()+"\n"+
			                "Emne:\t"+t.getEmne()+"\n";
			return output;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    }
    
    public void print(String str) {
        //System.out.print(str);
    }
    public void println(String str) {
        //System.out.println(str);
    }
    
}
