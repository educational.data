/*
 * Created on 2005-04-24
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiController;


import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.security.Permission;

import javax.swing.JOptionPane;
import javax.swing.UIManager;

import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.LoginHandler;
import drno.interfaces.PatientHandler;
import drno.interfaces.TidsreservationsHandler;
import drno.support.FileLoader;
import drno.support.Logger;
import drno.swingui.guiView.LoginView;


/**
 * @author Bo
 *
 */
public class LoginController {
	private LoginView loginView;
	private LoginHandler loginModel;
	
	private MainCalendarController m_controller;
	private PatientController ap_controller;
	
	private PatientHandler p_handler;
	private TidsreservationsHandler t_handler;
	private EmployeeHandler e_handler;
	
	public void run() {
		loginView = new LoginView();
		loginView.setVisible(true);
		
		loginView.setOKListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e) {
		        handleOnOK();
		      	
		      }
		    });
        
		loginView.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut programmet
		      	System.exit(0);
		      }
		    });
	
	}
	
	public void startApplication() throws SecurityDisallowedException
	{
		// Opret handler-klasser
		try {
            if(loginModel != null) {
    			p_handler = loginModel.getPatientHandler();
    			e_handler = loginModel.getEmployeeHandler();
    			t_handler = loginModel.getTidsreservationsHandler();
            } else return;
		} catch(RemoteException ex) {
			ex.printStackTrace();
			System.exit(0);
		} catch(Exception ex) {
            JOptionPane.showMessageDialog(loginView, "Der er sket en fejl. Beskeden er: " + ex.getMessage());
        }
		
		//ap_controller = new AddPatientController(p_handler,e_handler);
		//ap_controller.showView();
		m_controller = new MainCalendarController(loginModel);
		m_controller.showView();
		
	}

	private void handleOnOK() {
		// Tjek login - og start system hvis korrekt
		
		final String baseName = loginView.getHost();

		SecurityManager man = new SecurityManager() {
			public void checkPermission(Permission perm) { }
			public void checkPermission(Permission perm, Object context) { }
		};
		System.setSecurityManager(man);
		loginView.disable();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
		
		try {
			try {	
			    String conn = "//" + baseName + "/DrNOServer";
			    LoginHandler th =((LoginHandler)Naming.lookup(conn));
			    
			    loginModel = th.login(loginView.getUserName(),loginView.getPassword());
			    
			    startApplication();
				loginView.setVisible(false);
			} catch(RemoteException ex) {
		        JOptionPane.showMessageDialog(loginView,"Kan ikke forbinde til serveren p�: " + baseName + "\n" +  
		                "Indtast en ny host");
			    Logger.log(ex);
			    //System.exit(1);
			}
			// loginModel = new LoginHandlerImpl(loginView.getUserName(),loginView.getPassword()); 
			catch (MalformedURLException ex) {
				JOptionPane.showMessageDialog(loginView,"Du skal indtaste servernavn");
				Logger.log(ex);
		        return;
			}
			//startApplication();
			
		}

		catch (SecurityDisallowedException ex){
			JOptionPane.showMessageDialog(loginView, "Forkert brugernavn eller password - pr�v igen");
		} catch(NotBoundException ex) {
			JOptionPane.showMessageDialog(loginView, "Kunne ikke forbinde til serveren");
		} catch(UnkErrException ex) {
			JOptionPane.showMessageDialog(loginView, "Ukendt fejl, beskeden er: " + ex.getMessage());
		}
		loginView.enable();
			}
		});
		
	}
}
