/*
 * Created on 2005-04-25
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiController;


import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.Timer;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservation;
import drno.interfaces.ITidsreservationEditable;
import drno.interfaces.LoginHandler;
import drno.interfaces.PatientHandler;
import drno.interfaces.TidsreservationsHandler;
import drno.server.event.Event;
import drno.server.event.Signal;
import drno.swingui.guiView.MainCalendarView;
import drno.swingui.guiView.OldMainCalendarView;





/**
 * @author Bo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MainCalendarController {
	private MainCalendarView view;
	
	// Controller
	private LoginHandler lh;
	private PatientController p_controller;
	private EmployeeController e_controller;
	private AftaleController a_controller;
	private ColorChooserController cc_controller;
	private HelpController h_controller;
	private TableHandler th;
	
	// Model
	private PatientHandler p_handler;
	private EmployeeHandler e_handler;
	private TidsreservationsHandler t_handler;
	
	//Timer variabler:
	private Timer timer, clockTimer;
	public final static int ONE_SECOND = 1000;
	int min=0, sec=0, tidsopdeling=-1;
	konsultationProgress task = new konsultationProgress();
	long taskprogress,endtime;
	String konsultationMessage="";
	GregorianCalendar c = new GregorianCalendar();
    Timestamp konsultationSlut, konsultationStart, forventSlut;
	ITidsreservation currentKonsultation, viewKonsultation;
	
	
	//Controller Constructor:
	public MainCalendarController(LoginHandler lh){
		try {
			this.lh = lh;
            p_handler = lh.getPatientHandler();
            e_handler = lh.getEmployeeHandler();
            t_handler = lh.getTidsreservationsHandler();
        }
        catch (RemoteException e1) {
            e1.printStackTrace();
        }
        catch (SecurityDisallowedException e1) {
            e1.printStackTrace();
        }
        th = new TableHandler(p_handler,e_handler,t_handler,lh);
		view = new MainCalendarView(th.getDoctors());
		p_controller = new PatientController(p_handler,e_handler,this);
		e_controller = new EmployeeController(e_handler,view);
		a_controller = new AftaleController(t_handler,p_handler,e_handler);
		cc_controller = new ColorChooserController(view);
		h_controller = new HelpController();
		
//Timerne:
		timer=new Timer(ONE_SECOND, new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                sec++;
				if(sec==60){
					min++;
					sec=0;
                }
				taskprogress = task.getCurrent();
                view.setProgressBarValue((int)taskprogress);
                view.setProgressBarString("Konsultations fremgang : "+min+" minuter "+sec+" sekunder");
                konsultationMessage = task.getMessage();

                if (task.isDone()) {
                    Toolkit.getDefaultToolkit().beep();
                    view.printText("Tiden er udl�bet....\nDe k�re p� overtid....");
                }
            }
        });
		
		clockTimer =new Timer(ONE_SECOND, new ActionListener(){
            public void actionPerformed(ActionEvent evt) {
                view.setClockText((String)task.getTimeString());
            }
        });

        task.goClock();
        clockTimer.start();
        
       try {
			// S�t markerede l�ge - hvis det er en l�ge der er logget ind
			if (lh.getUser().getType()==IEmployee.TYPE_DOCTOR){
				view.setSelectedDoc(lh.getUser());
			}
		} catch (RemoteException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		

	//UPDATE EVENT
		
		try {
			Signal sig = t_handler.getOnUpdate();
			Event event = new Event() {
				public void onAction(Object msg) {
					calendar_change();
					//System.out.println("test");
				}
			};
			sig.addListener(event);
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
		try {
			Signal sig = e_handler.getOnUpdate();
			Event event = new Event() {
				public void onAction(Object msg) {
					view.updateDocList(e_controller.getDoctors());
					view.updateDocs(e_controller.getDoctors());
					System.out.println("test");
				}
			};
			sig.addListener(event);
			
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
/*
	//AFTALE MENU EVENTS
		// Menu -> Aftale -> Tilf�j
		view.setAftaleAddListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	//a_controller.showViewAdd();

		            }	
		        });
				
			}
		});		
		// Menu -> Aftale -> S�g
		view.setAftaleSearchListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	a_controller.showViewSearch();
		            }	
		        });
				
			}
		});	
		*/			
	//PATIENT MENU EVENTS
		// Menu -> Patient -> Tilf�j 
		view.setPatientAddListener(new ActionListener(){
				public void actionPerformed(ActionEvent e) {
			        java.awt.EventQueue.invokeLater(new Runnable() {
			            public void run() {
			            	p_controller.showViewAdd();
			            }	
			        });
					
				}
			});
		// Menu -> Patient -> S�g
		view.setPatientSearchListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	p_controller.showViewSearch();
		            }	
		        });
				
			}
		});
		
	//ANSATTE MENU EVENTS
		// Menu -> Ansatte -> Tilf�j
		view.setEmployeeAddListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	e_controller.showViewAdd();
		            }	
		        });
				
			}
		});
		// Menu -> Ansatte -> S�g
		view.setEmployeeSearchListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	e_controller.showViewSearch();
		            }	
		        });
				
			}
		});		

	//INDSTILLING MENU EVENTS
		//Menu -> Indstilling -> Udseende
		view.setFarverListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	cc_controller.showCCView();
		            }	
		        });
				
			}
		});	
	
	//HJ�LP MENU EVENTS
		view.setHj�lpMenuListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	h_controller.showHelpView();
						//System.out.println("Help...");
		            }	
		        });}
		});	
    //POPUP MENU EVENTS
		// Popup -> Ret Patient
		view.setPopupEditPatientListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	editPatient();
		            	//System.out.println("POPUP - Edit patient");
		            }	
		        });
				
			}
		});
		/*
		// Popup -> Ret Tidsreservation
		view.setPopupEditAftaleListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	System.out.println("POPUP - Ret tidsreservation");
		            }	
		        });
				
			}
		});
		*/		
		// Popup -> Slet Tidsreservation
		view.setPopupDeleteAftaleListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	try {
							//System.out.println("POPUP - Slet aftale");
							Object key = view.getSelectedTidsreservation();
							ITidsreservation t = (ITidsreservation)th.getHashtable().get(key);
							t_handler.deleteTidsreservation(t_handler.editTidsreservation(t));
							//th.getHashtable().remove(t);
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ConsistanceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ObjectLockException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (UnkErrException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
		            }	
		        });
				
			}
		});		
		// Popup -> Tilf�j tidsreservation
		view.setPopupAddAftaleListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		    			Object key = view.getSelectedTidsreservation();
		    			if(key != null) {
		    				return;
		    			}
		    			Timestamp t = view.getSelectedTimestamp();
		    			
		    			//System.out.println(t);

							/*try {
								//System.out.println(view.getSelectedDoc().asString());
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}*/

		    			
		    			//ITidsreservation t = (ITidsreservation)th.getHashtable().get(key);
		    			//if (t==null){

							try {
								ITidsreservationEditable it = t_handler.createNewTidsReservation();
								it.setEmployee(view.getSelectedDoc());
								it.setTidStart(t);
								a_controller.showViewAdd(it);
							} catch (RemoteException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (ConsistanceException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (ObjectLockException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
		    				
		    			//}
		            	
		            	//System.out.println("POPUP - Tilf�j tidsreservation");
		            }	
		        });
				
			}
		});		
		
	//KALENDER SELECTION EVENTS
		// Vis patient/aftale info
		view.setTableMouseListener(new java.awt.event.MouseAdapter() {
        public void mouseReleased(java.awt.event.MouseEvent evt) {
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run() {
	            	showPatientInfo();
	            	//System.out.println("Vis patientinfo");
	            }	
	        });        	
            
        }
    });
		// Skift dato for kalender (Kalender - klik)
		view.setCalendarListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
            	calendar_change();
				if(view.isAnsatSelectionEmpty()){
					view.printText("De skal v�lge en l�ge f�rst...");
				}
				view.setStartButtonStatus(false);
				view.clearList2();
                //System.out.println("Skift dato for kalender...");
            }
        });
		// Skift l�ge for kalender (liste - klik)
		view.setCalendarFromDoctorList( new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
				calendar_change();
				view.setStartButtonStatus(false);
				view.clearList2();
				//System.out.println("Vis aftaler for anden l�ge...");
              }
  		});
		// Liste -> Tidligere konsultationer -> Vis (klik)
		view.setPatientInfoFromAftale( new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {

                if(evt.getClickCount()==2){
                	view.showOldReservation();
                }
            }
		});
		// CMD - Start konsultation
		view.setStartListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
						try {
							if(view.getStartButtonStatus()){
								tidsopdeling = getTidsopdelingFromEmployee();
								view.setProgressBarMax(tidsopdeling*60);
								
								view.setProgressBarMax(tidsopdeling*60);
								
								konsultationStart = new Timestamp(c.getTimeInMillis());
								forventSlut = new Timestamp(c.getTimeInMillis());
								forventSlut.setMinutes(konsultationStart.getMinutes()+tidsopdeling);
								
								task.setLengthOfTask(tidsopdeling);
								task.go();
								timer.start();
								view.setStartButtonStatus(false);
								view.setLabel1Text("Startede: "+task.getTimeString());
								view.setKonsultationVisible(true);
								view.setKonsultationText("Konsultation igang....");
								view.setAfslutButtonStatus(true);
							}
							
							else
								view.printText("For at starte en konsultation:\n" +
										"1. V�lge en gyldig tidreservation p� tabellen til venstre.\n" +
										"2. Trykke p� Start Konsultation knappen.");
						}
						catch(Exception exception){
							exception.printStackTrace();
						}
		            }	
		        });
				
			}
		});
		// CMD - Slut konsultation
		view.setSlutListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	//System.out.println("Slut konsultation");
						if(view.getStartButtonStatus()==false){
				            //view.setLabel1Text("Konsultation Afsluttet");
							konsultationSlut = DateToTimestamp(task.getTime());
							try{
								t_handler.updateReelTider(currentKonsultation,konsultationStart,konsultationSlut);
							}
							catch(Exception exception){
								exception.printStackTrace();
							}
							view.setKonsultationText("Sluttet: "+task.getTimeString());
				            if(!task.isDone()){
								task.stop();
				            }
				            timer.stop();
							view.printText("Konsultaion Afsluttet");
				            view.setProgressBarString("Konsultation Afsluttet - Total tid: "+min+" minutter og "+sec+" sekunder");
							clearProgressBarTime();
							view.setAfslutButtonStatus(false);
							view.setStartButtonStatus(false);
						}

		            }	
		        });
				
			}
		});
	}

	/**
	 * 
	 */
	public int getTidsopdelingFromEmployee(){
		try {
			IEmployee doc = view.getSelectedDoc();
			return doc.getTidsOpdeling();
		} 
		catch (RemoteException e) {
			e.printStackTrace();
		}
		return -1;
	}
	
	protected void calendar_change() {
			IEmployee doc = view.getSelectedDoc();
			Timestamp t = (java.sql.Timestamp)DateToTimestamp(view.getCalendarData());
			
			if (doc!=null){
				Vector<ITidsreservation> reservation;
				try {
					reservation = t_handler.getFromDate(t,doc);
					view.update_calendar(reservation);
				} catch (RemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (UnkErrException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
	}


	/**
	 * @param d
	 * @return
	 */
	//Laver en Util date objekt til et SQL timestamp objekt
	public java.sql.Timestamp DateToTimestamp(java.util.Date d){
		GregorianCalendar c = new GregorianCalendar();
		//Dump tidsreservations databasen i tabellen     
		Timestamp t = new Timestamp(d.getYear(),d.getMonth(),d.getDate(),d.getHours(),d.getMinutes(),d.getSeconds(),0); 
		return t;  
	}


	/**
	 * 
	 */
	public void showView() {
		// Indl�s tidsinddeling fra Employee-objekt
		try {
            view.setTidsopdeling(lh.getUser().getTidsOpdeling());
			view.setVisible(true);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
    public Object[][] makeCalendarFromEmployee(IEmployee e) {
        try{
        	DateFormat df2 = DateFormat.getTimeInstance(DateFormat.SHORT);
            int tidsopdeling = e.getTidsOpdeling();
            Object[][] data = new Object[60/tidsopdeling*8][2];
            GregorianCalendar c = new GregorianCalendar();
            //Timestamp t = new Timestamp(c.getTimeInMillis());
            Timestamp t = new Timestamp(c.getTimeInMillis());
            t.setHours(8);
            t.setMinutes(00);
            String s = df2.format(t);
            //set tids kolloner:
            for(int i=0; i<data.length;i++){
                data[i][0]= s;
                t.setMinutes(t.getMinutes()+tidsopdeling);
                s = df2.format(t);
            }
            return data;
        }
        catch(Exception exception){
            exception.printStackTrace();
        }
        return null;
    }     
   
    private void showPatientInfo(){
    	// Vis Patient-info udfra valgte tidsreservation p� dagens kalender
		try {
			if(th.getHashtable()==null){
				view.setStartButtonStatus(false);
				view.clearList2();
			}	
			else{
				Object key = view.getSelectedTidsreservation();
				System.out.println(key);
				ITidsreservation t = (ITidsreservation)th.getHashtable().get(key);
				if (t!=null){
					/*
					view.setPopupAddTidsreservation(false);
					view.setPopupEditPatient(true);
					view.setPopupSletTidsreservation(true);
					*/
					if(!timer.isRunning()){				
						currentKonsultation = t;
						view.setStartButtonStatus(true);
						IPatient p = currentKonsultation.getPatient();
						Vector v = th.getReservationFromPatient(p);
						view.setPatientInfo(currentKonsultation,p,v);
					}
					else{
						viewKonsultation = t;
						view.setStartButtonStatus(false);
						IPatient p = viewKonsultation.getPatient();
						Vector v = th.getReservationFromPatient(p);
						view.setPatientInfo(viewKonsultation,p,v);
					}
				}
				else{
					/*
					view.setPopupAddTidsreservation(true);
					view.setPopupEditPatient(false);
					view.setPopupSletTidsreservation(false);
					*/
					view.printText("Tiden er LEDIG");
					view.setStartButtonStatus(false);
					view.clearList2();
				}
			}
//			int row = view.getTableSelectedRow();
//			int col = view.getTableSelectedColumn();
//			if(view.getTableValueAt(row,col)==null || view.getTableValueAt(row,col).equals("")){
//				view.setPopupAddTidsreservation(true);
//				view.setPopupEditPatient(false);
//				view.setPopupSletTidsreservation(false);
//			}
//			else{
//				view.setPopupAddTidsreservation(false);
//				view.setPopupEditPatient(true);
//				view.setPopupSletTidsreservation(true);
//			}
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
    }

    private void editPatient(){
       	try {
			Object key = view.getSelectedTidsreservation();
			ITidsreservation t = (ITidsreservation)th.getHashtable().get(key);
			if (t!=null){
				IPatient p = t.getPatient();
				p_controller.showViewEdit(p);
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   	
    }
	public void clearProgressBarTime(){
		min=0;
		sec=0;
	}
	
}
