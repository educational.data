/*
 * Created on 2005-04-26
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.JOptionPane;

import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.IEmployeeEditable;
import drno.swingui.guiView.AddEmployee;
import drno.swingui.guiView.MainCalendarView;
import drno.swingui.guiView.SearchView;
import drno.swingui.guiView.SelectView;




/**
 * @author Bo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EmployeeController {
	private EmployeeHandler e_handler;
	private AddEmployee viewAdd;
	private AddEmployee viewEdit;
	private SearchView viewSearch;
	private SelectView viewSelect;
	private MainCalendarView view;
	
	public EmployeeController(EmployeeHandler eh, final MainCalendarView view){
		this.view = view;
		e_handler = eh;
		viewAdd = new AddEmployee();
		viewEdit = new AddEmployee();
		viewSearch = new SearchView("Indtast hele eller dele af navnet:");
		viewSelect = new SelectView(false);
		
		
		viewAdd.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewAdd.setVisible(false);
		      }
		    });
		viewAdd.setOKListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	addEmployee();
		                return null;
		            }
		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });
		// EVENTS - S�g
		viewSearch.setSearchListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // S�g efter ansat
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	searchEmployee();
		                return null;
		            }

		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });
		viewSearch.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewSearch.setVisible(false);
		      }
		    });
		viewSelect.setEditListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Ret patient
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run(){
		            	viewSelect.setVisible(false);
		            	/** System.out.println((IEmployee)viewSelect.getSelected());
		            	System.out.println("Ret Ansat");*/
		            	showViewEdit((IEmployee)viewSelect.getSelected());
		            		
		            }


		        });
		      }
		    });
		viewSelect.setDeleteListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Slet patient
		      	
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run(){
				        //final SwingWorker worker = new SwingWorker() {
				            //public Object construct() {
						System.out.println("Test");
				            	viewSelect.setVisible(false);
								System.out.println("Test");
				            	deleteEmployee((IEmployee)viewSelect.getSelected());
								System.out.println("Test");
				                //return null;
				            //}
				        //};
				        //worker.start();  //required for SwingWorker 3		      	
		            }
		        });
		      }
		    });
		viewSelect.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
				  viewSelect.setVisible(false);
		      }
		});
		viewEdit.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewEdit.setVisible(false);
		      }
		    });
		viewEdit.setOKListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	saveEmployee(viewEdit.getEmployee());
		                return null;
		            }
		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });	
		
	}		

	
	
	public void showViewAdd(){
		viewAdd.setVisible(true);
	}
	
	/**
	 * 
	 */
	public void showViewSearch() {
		
		viewSearch.setVisible(true);
	}
	
	private void showViewEdit(final IEmployee employee) {
		// TODO Auto-generated method stub
	       java.awt.EventQueue.invokeLater(new Runnable() {
            public void run(){
            	try{
            			viewEdit.setTitle("Ret Ansat");
						viewEdit.clear();
            			IEmployeeEditable ee = e_handler.editEmployee(employee);
            			viewEdit.setEmployee(ee);
            			viewEdit.setVisible(true);
            	}
            	catch (Exception e){
            		System.out.println(e.getMessage());
            		e.printStackTrace();
            	}
                
            }
        });		
	}
	
	

//	 Swingworker-funktioner	
	public void addEmployee(){
        try{
			IEmployeeEditable e = e_handler.createNewEmployee();
			e.setFornavn(viewAdd.getJTextFornavn());
			e.setEfternavn(viewAdd.getJTextEfternavn());
			e.setLogin(viewAdd.getJTextLogin());
			e.setTelefonNummer(viewAdd.getJTextTlf());
			e.setTidsOpdeling(Integer.parseInt(viewAdd.getJTextTidsopdeling()));
			e.setType(viewAdd.getJComboStilling());
			e.setPassword(viewAdd.getJPasswordField1());
			/* System.out.println(viewAdd.getJPasswordField1());
			System.out.println(viewAdd.getJPasswordField2()); */
			if (viewAdd.getJPasswordField1().equals(viewAdd.getJPasswordField2())){
				e_handler.saveEmployee(e);
				JOptionPane.showMessageDialog(viewAdd, "Ansat oprettet og gemt i database.");
				viewAdd.setVisible(false);				
			}
			else{
				JOptionPane.showMessageDialog(viewAdd, "Password-felterne er ikke ens!");
			}
		} catch (SecurityDisallowedException ex) {
            JOptionPane.showMessageDialog(viewSelect, "Du har ikke ret til at oprette en bruger.");
        } catch(Exception ex){
			ex.printStackTrace();
			JOptionPane.showMessageDialog(viewAdd, "Ansat kunne ikke oprettes: "+ex.toString());
		}
			
	}


	private void searchEmployee() {
		viewSearch.setVisible(false);
		// System.out.println(viewSearch.getSearchText());
		// S�g efter kriterier og vis ansatte
		try{
			viewSelect.setEmployeeData(e_handler.searchEmployeesByName(viewSearch.getSearchText()));
			//viewSelect.setData(patienter);
			viewSelect.setVisible(true);			
		}
		catch (Exception e){
			e.printStackTrace();
		}		
	}	

	public void saveEmployee(IEmployeeEditable e){
		try {
			e_handler.saveEmployee(e);
			JOptionPane.showMessageDialog(viewEdit, "Ansat �ndret og gemt i database.");
			viewEdit.setVisible(false);
		}catch (SecurityDisallowedException ex) {
            JOptionPane.showMessageDialog(viewSelect, "Du har ikke ret til at �ndre denne bruger.");
        }catch (Exception ex){
            JOptionPane.showMessageDialog(viewSelect, "Der er sket en fejl. Bekseden er: " + ex.getMessage());
		}
	}

	public void deleteEmployee(IEmployee e){
		try {
			e_handler.deleteEmployee(e_handler.editEmployee(e));
			JOptionPane.showMessageDialog(viewSelect, "Ansat er slettet fra databasen.");
			viewSelect.setVisible(false);
		}
		catch (ObjectLockException ex){
			JOptionPane.showMessageDialog(viewSelect, "Ansat kan ikke slettes fra databasen, da Ansat-objektet er l�st af anden bruger.");
		} catch (SecurityDisallowedException ex) {
            JOptionPane.showMessageDialog(viewSelect, "Du har ikke ret til at slette denne bruger.");
		} catch (Exception ex) {
            JOptionPane.showMessageDialog(viewSelect, "Der er sket en fejl. Bekseden er: " + ex.getMessage());
        }
	}
	
	public Object[] getDoctors(){
        try{
            Vector docs = e_handler.getAllDoctors();
            Object[] docData = new Object[docs.size()];
            docs.copyInto(docData);
            return docData;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return null;
    }

	
	
}
