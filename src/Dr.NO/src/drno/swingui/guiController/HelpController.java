package drno.swingui.guiController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Vector;

import javax.swing.ImageIcon;

import drno.support.FileLoader;
import drno.swingui.guiView.Help;

public class HelpController {
	
	private Help helpview;
	private Vector helpLib;
	private Vector searchResault;
	String notFound = "Ikke fundet...";
	String helpLibEmpty = "Ingen hj�lp katagori fundet";
	String searchWord="";
	String line, defaultImage="Images/default.JPG", notfound="Images/notfound.JPG",file = "HelpLib.txt", header="...",imagetype=".JPG",images="Images/",foundImage ="";
	String [] ImageLib= {"tilfoj_patienter", "sog_patienter", "slet_patienter", "ret_patienter", "tilfoj_ansat", "sog_ansat", "slet_ansat", "ret_ansat", "tilfoj_aftale", "slet_aftale", "udseende", "time_aftaler"};
	//ImageIcon image; 
	
	public HelpController(){
		helpview = new Help();
		helpLib = new Vector();
		searchResault = new Vector();
		
		helpview.setS�gButtonListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run() {
		            	search();
		            }	
		        });
				
			}
		});
		helpview.setCancelButtonListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
				helpview.setVisible(false);
			}
		});
		helpview.setListListener( new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
				String selectedListElement = (String)helpview.getSelectedListElement();
				getHelpAndImage(selectedListElement);
            }
		});
	}
	
	public void showHelpView(){
		//System.out.println("Help...");
		helpview.setVisible(true);
	}
	
	public void search(){
		//System.out.println("S�ger.....");
		searchResault.clear();
		helpLib = helpview.getAllListData();
		searchWord = helpview.getSearchWord();						
		if(!searchWord.equals("")){														
			if(helpLib!=null){
				String temp="";
				for(int i=0; i<helpLib.size(); i++){
					temp=(String)helpLib.elementAt(i);									
					if(temp.contains(searchWord)){										
						searchResault.add(temp);
					}
				}
				if(searchResault.isEmpty()){
					searchResault.add(notFound);
					helpview.printText("");
					helpview.setImageIcon(new ImageIcon(FileLoader.inst().getBytes(notfound)));
				}								
			}
			else{
				searchResault.add(helpLibEmpty);								
			}
			helpview.setListAfterSearch(searchResault);
		}
		else{
			helpview.setListAfterSearch(helpLib);
		}
	}
	
	
	public void getHelpAndImage(String selectedListElement){
		try{
			InputStreamReader fr = new InputStreamReader(FileLoader.inst().getInputStream(file));
			BufferedReader inFile = new BufferedReader(fr);
			line = inFile.readLine();
			//helpview.printText(line);
			//helpview.printTextTogether(header+selectedListElement);
			while(line!=null){
				if(line.contains(header+selectedListElement)){
					line=inFile.readLine();
					helpview.printText("");
					while(line!=null && !line.contains(header)){
						helpview.printTextTogether(line);
						line=inFile.readLine();
					}
				}
				line=inFile.readLine();
			}
			inFile.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		setImage(selectedListElement.replaceAll(" ","_").replaceAll("�","o"));
	}
	
	
	public void setImage(String SelectedElement){
		foundImage ="";
		for(int i=0; i<ImageLib.length; i++){
			if((images+ImageLib[i]+imagetype).equals(images+SelectedElement+imagetype)){
				foundImage=images+ImageLib[i]+imagetype;
			}
		}
		if(foundImage.equals("")){
				helpview.setImageIcon(new ImageIcon(FileLoader.inst().getBytes(notfound)));
				helpview.printTextTogether("\n\n(NO SCREENSHOT FOUND FOR HELP)");
		}
		else{
			helpview.setImageIcon(new ImageIcon(FileLoader.inst().getBytes(foundImage)));
		}
	}
}
