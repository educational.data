/*
 * Created on 2005-05-09
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.swing.JOptionPane;

import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservation;
import drno.interfaces.ITidsreservationEditable;
import drno.interfaces.PatientHandler;
import drno.interfaces.TidsreservationsHandler;
import drno.swingui.guiView.AddAftaleView;
import drno.swingui.guiView.SearchView;
import drno.swingui.guiView.SelectView;





/**
 * @author Bo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AftaleController {
	private AddAftaleView viewAdd;
	private SearchView viewSearch;
	private SelectView viewSelect;
	
	private SearchView viewSearchPatient;
	private SelectView viewSelectPatient;
	
	private PatientHandler p_handler;
	private EmployeeHandler e_handler;
	private TidsreservationsHandler t_handler;
	
	public AftaleController(TidsreservationsHandler th, PatientHandler ph, EmployeeHandler eh){
		p_handler = ph;
		e_handler = eh;
		t_handler = th;
		
		viewAdd = new AddAftaleView();
		viewSearch = new SearchView("Hele eller dele af aftalens emne:");
		viewSelect = new SelectView(false);
		viewSelectPatient = new SelectView(true);
		viewSearchPatient = new SearchView("Hele eller dele af patientens navn:");
		

		
		
		// EVENTS - TILF�J AFTALE
		viewAdd.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewAdd.setVisible(false);
		      }
		    });
		viewAdd.setOKListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	addEmployee();
		                return null;
		            }


		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });

		// EVENTS - S�G PATIENT
		viewAdd.setSearchPatientListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	// S�g efter patient
		            	showViewSearchPatient();
		                return null;
		            }




		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });    
		
		viewSearchPatient.setSearchListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	searchPatient();
		                return null;
		            }
		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });
		viewSearchPatient.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewSearchPatient.setVisible(false);
		      }
		    });
		viewSelectPatient.setEditListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Patient valgt 
		        java.awt.EventQueue.invokeLater(new Runnable() {
		            public void run(){
		            	viewSelectPatient.setVisible(false);
		            	/* System.out.println((IPatient)viewSelectPatient.getSelected());
		            	System.out.println("Valgt patient"); */
		            	viewAdd.setPatient((IPatient)viewSelectPatient.getSelected());	
		            }
		        });
		      }
		    });	
		viewSelectPatient.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
				  viewSelectPatient.setVisible(false);
		      }
		});

		
		// EVENTS - S�G AFTALE
		viewSearch.setSearchListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Tilf�j patient til database
		        final SwingWorker worker = new SwingWorker() {
		            public Object construct() {
		            	searchAftale();
		                return null;
		            }
		        };
		        worker.start();  //required for SwingWorker 3		      	
		      	
		      }
		    });
		viewSearch.setCancelListener(new ActionListener(){
		      public void actionPerformed(ActionEvent e){
		        // Afslut sk�rmbillede
		      	viewSearch.setVisible(false);
		      }
		    });
	
	viewSelect.setEditListener(new ActionListener(){
	      public void actionPerformed(ActionEvent e){
	        // Ret patient
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run(){
	            	viewSelectPatient.setVisible(false);
	            	
	            	/* System.out.println((ITidsreservation)viewSelect.getSelected());
	            	System.out.println("Ret aftale"); */
	            	//showViewEdit((IPatient)viewSelect.getSelected());
	            		
	            }
	        });
	      }
	    });
	viewSelect.setDeleteListener(new ActionListener(){
	      public void actionPerformed(ActionEvent e){
	        // Slet patient
	      	
	        java.awt.EventQueue.invokeLater(new Runnable() {
	            public void run(){
			        final SwingWorker worker = new SwingWorker() {
			            public Object construct() {
			            	viewSelect.setVisible(false);
			            	//deletePatient((IPatient)viewSelect.getSelected());

			                return null;
			            }
			        };
			        worker.start();  //required for SwingWorker 3		      	

	            	
	            }
	        });
	      }
	    });
}	
	
	public void showViewAdd(ITidsreservationEditable it){
		try {
			viewAdd.clearDoctorList();
			viewAdd.clearPatient();
			Vector e_doc = e_handler.getAllDoctors();
			viewAdd.setAftale(it,e_doc);
			viewAdd.setVisible(true);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnkErrException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void showViewSearch(){
		viewSearch.setVisible(true);
	}	
	private void showViewSearchPatient() {
		viewSearchPatient.setVisible(true);
		
	}	
	public void searchAftale(){
		viewSearch.setVisible(false);
		// System.out.println(viewSearch.getSearchText());
		// S�g efter kriterier og vis aftaler
		try{
			//viewSelect.setAftaleData(t_handler.(viewSearch.getSearchText()));
			
			//viewSelect.setData(patienter);
			viewSelect.setVisible(true);			
		}
		catch (Exception e){
			e.printStackTrace();
		}

		
	}	
	
	private void addEmployee() {
		// TODO Auto-generated method stub
		
	        try{
	        	ITidsreservationEditable aftale = viewAdd.getAftale();
				t_handler.saveTidsreservation(aftale);
				if(viewAdd.doubletidSelected()){
					System.out.println(viewAdd.doubletidSelected());
					ITidsreservationEditable aftale2 = viewAdd.getAftale2();
					t_handler.saveTidsreservation(aftale2);
				}
				JOptionPane.showMessageDialog(viewAdd, "Aftale oprettet og gemt i database.");
				viewAdd.setVisible(false);
			}catch(Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(viewAdd, "Aftale kunne ikke oprettes: "+e.toString());
			}
	}	
	
	public void searchPatient(){
		viewSearchPatient.setVisible(false);
		System.out.println(viewSearchPatient.getSearchText());
		// S�g efter kriterier og vis patienter
		//Object [][] patienter = getPatientData(viewSearch.getSearchText());	
		try{
			viewSelectPatient.setPatientData(p_handler.searchPatientsByName(viewSearchPatient.getSearchText()));
			//viewSelect.setData(patienter);
			viewSelectPatient.setVisible(true);			
		}
		catch (Exception e){
			e.printStackTrace();
		}		
	}	
}
