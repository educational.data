/*
 * konsultationProgress.java
 *
 * Created on 30. april 2005, 15:15
 */

package drno.swingui.guiController;

import java.util.Date;
import java.text.DateFormat;
import java.util.GregorianCalendar;

/**
 *
 * @author  Leo
 */
public class konsultationProgress {
    int konsultationLength=0;
    long taskCurrent=0;
    long kstart,kcurrent;
    boolean done = false;
    boolean canceled = false;
    boolean clockDone = false;
	Date timeCurrent;
    String statMessage,clockString;
    //TableHandler th = new TableHandler();
    
    /** Creates a new instance of konsultationProgress */
    public konsultationProgress() {
        
    }
    
   public void go(){
        GregorianCalendar c = new GregorianCalendar();
        kstart = (c.getTime()).getTime()/1000;
        final SwingWorker worker = new SwingWorker() {
            public Object construct() {
                taskCurrent = 0;
                done = false;
                canceled = false;
                statMessage = null;
                return new ActualTask();
            }
        };
        worker.start();
    }
   
   public void goClock(){
       final SwingWorker worker2 = new SwingWorker(){
           public Object construct(){
               clockString ="";
               return new Clock();
           }
       };
       worker2.start();
   }

    /**
     * Called from ProgressBarDemo to find out how much work needs
     * to be done.
     */
    public int getLengthOfTask() {
        return konsultationLength;
    }
    public void setLengthOfTask(int length) {
        konsultationLength = length*60;
    }

    /**
     * Called from ProgressBarDemo to find out how much has been done.
     */
    public long getCurrent() {
        return taskCurrent;
    }

    public void stop() {
        canceled = true;
        statMessage = null;
    }

    /**
     * Called from ProgressBarDemo to find out if the task has completed.
     */
    public boolean isDone() {
        return done;
    }

    /**
     * Returns the most recent status message, or null
     * if there is no current status message.
     */
    public String getMessage() {
        return statMessage;
    }
    
    class Clock{
        Clock(){
            while(!clockDone){
                try{
                    Thread.sleep(1000);
                    GregorianCalendar c = new GregorianCalendar();
                    DateFormat df = DateFormat.getTimeInstance(DateFormat.MEDIUM);
                    timeCurrent=c.getTime();
					clockString = df.format(c.getTime());
                    //System.out.println(clockString);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
    
    public String getTimeString(){
        return clockString;
    }
	public Date getTime(){
		return timeCurrent;
	}

    /**
     * The actual long running task.  This runs in a SwingWorker thread.
     */
    class ActualTask {
        ActualTask() {
            //Fake a long task,
            //making a random amount of progress every second.
            while (!canceled && !done) {
                try {
                    Thread.sleep(1000); //sleep for a second
                    GregorianCalendar c = new GregorianCalendar();
                    kcurrent = (c.getTime()).getTime()/1000;
                    taskCurrent = kcurrent-kstart; 
                    if (taskCurrent >= konsultationLength) {
                        done = true;
                        taskCurrent = konsultationLength;
                    }
                    statMessage = "Completed " + taskCurrent +
                                  " out of " + konsultationLength + ".";
                } catch (Exception e) {
                    System.out.println("ActualTask interrupted");
                }
            }
        }
    }
}
