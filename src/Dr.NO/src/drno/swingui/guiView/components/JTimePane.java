package drno.swingui.guiView.components;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.SystemColor;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.TimeZone;
import java.util.Vector;

import javax.swing.JComponent;

@SuppressWarnings("serial")
public class JTimePane extends JComponent {
	class lDate {
		String Description;
		Object Value;
	}
	
	class Slot {
		lDate date;
		int Time;
		Slot(int tid) {
			Time = tid;
			date = null;
		}
		Slot(int tid, lDate dat) {
			Time = tid;
			date = dat;
		}
	}

	private static final int CELL_HEIGHT = 40;
	private static final int CELL_HHEIGHT = 20;
	
	private Date curDate;
	private int dayoffset;
	private int endoffset;
	Point lastPos;
	private Vector<ActionListener> listeners;
	private PopupMenu mnuPop;
	
	protected int selectedItem;
	HashMap<Integer,Slot> slots;
	
	public JTimePane() {
		//setBorder(BorderFactory.createLoweredBevelBorder());
		removeAll();
		dayoffset = 6;
		endoffset = 20;
		listeners = new Vector<ActionListener>();
		addMouseListener(new MouseListener() {
			public void mouseClicked(MouseEvent arg0) {
				
			}

			public void mouseEntered(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			public void mouseExited(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			public void mousePressed(MouseEvent arg0) {
				// TODO Auto-generated method stub
                userClickedMouse(arg0);
				
			}

			public void mouseReleased(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
		});
	}

	protected int getIdAt(Point pos) {
		if(pos == null) {
			System.err.println("Why the hell is pos null");
			return -1;
		}
		int val = (pos.y/CELL_HEIGHT) + dayoffset;
		return val;
	}
	
	public Dimension getMinDimension() {
		return new Dimension(300,100);
	}
	
	@Override
	public Dimension getPreferredSize() {
		int height = CELL_HEIGHT * (endoffset - dayoffset);
		System.out.println("height: " + height);
		return new Dimension(300,height);
	}

	public Object getSelectedItem() {
		System.out.println("1");
		int id = getIdAt(lastPos);
		System.out.println("2");
		return slots.get(id);
	}
	
	public Timestamp getSelectedTimestamp() {
		GregorianCalendar cal;
		cal = new GregorianCalendar();
		
		if(curDate!=null) cal.setTime(curDate);
		int id = getIdAt(lastPos);
		cal.set(Calendar.HOUR_OF_DAY, id);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return new Timestamp(cal.getTimeInMillis());
	}
	
	public void insertDate(String description, Object obj, int from, int to) {
		lDate dt = new lDate();
		dt.Description = description;
		dt.Value = obj;
		from = from < 24 ? from : 23;
		from = from >= 0 ? from : 0;
		to = to < 24 ? to : 23;
		to = to >= 0 ? to : 0;
		to = to>=from ? to : from;
		
		for(int i = from;i<=to;i++) {
			slots.get(new Integer(i)).date = dt;
		}
	}
	
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		Insets insets = getInsets();
		Graphics2D g2 = (Graphics2D)g.create();
		FontMetrics metrics = g.getFontMetrics();
		
		for(int i = dayoffset;i<endoffset;i++) {
			int bottom = ((i)-dayoffset) * (CELL_HEIGHT);
			//int top = ((i+1)-dayoffset) * (metrics.getHeight()*2+4);
			
			if((i % 2) == 0)
				g2.setColor(SystemColor.info);
			else
				g2.setColor(Color.white);
			
			g2.fillRect(insets.left, bottom, getWidth()-insets.right, CELL_HEIGHT-1);
			
			g2.setColor(Color.GRAY);
			g2.drawLine(insets.left, bottom, getWidth()-insets.right, bottom);
			String text = String.format("%02d:00", i);
			
			g2.drawLine(
					insets.left+metrics.stringWidth(text)+5, 
					bottom+CELL_HHEIGHT+2, 
					getWidth()-insets.right, 
					bottom+CELL_HHEIGHT+2);
			
			g2.setColor(Color.BLACK);
			g2.drawString(text, insets.left, bottom+CELL_HHEIGHT-2);
		}
		class mDate {
			int bottom;
			lDate date;
			int top;
		}
		Vector<mDate> dates = new Vector<mDate>();
		mDate current = null;
		lDate date_old = null;
		for(int i = dayoffset;i<endoffset;i++) {
			int top = ((i)-dayoffset) * (CELL_HEIGHT);
			int bottom = ((i+1)-dayoffset) * (CELL_HEIGHT);
			
			lDate date = slots.get(new Integer(i)).date;
			if(date_old == null && date != null) {
				current = new mDate();
				current.date = date;
				current.top = top+3;
				current.bottom = bottom-3;
			}
			if(date_old != null && date != null && date_old != date) {
				if(current != null)dates.add(current);
				current = new mDate();
				current.date = date;
				current.top = top+3;
				current.bottom = bottom-3;
			}
			if(date_old != null && date != null && date_old == date) {
				current.bottom = bottom-3;
			}
			if(date_old != null && date == null) {
				//current.bottom = bottom-3;
				dates.add(current);
			}
			date_old = date;
		}
		
		for (mDate date : dates) {
			int w = insets.left+metrics.stringWidth("00:00")+10;
			g2.setColor(Color.orange);
			g2.fillRoundRect(insets.left+metrics.stringWidth("00:00")+10, date.top, getWidth() - insets.right-5-w, date.bottom-date.top, 
					20,20);
			g2.setColor(Color.BLACK);
			g2.drawRoundRect(insets.left+metrics.stringWidth("00:00")+10, date.top, getWidth() - insets.right-5-w, date.bottom-date.top, 
					20,20);
			
			g2.drawString(date.date.Description, w+10, date.top+3+metrics.getHeight());
		}
		g2.dispose();
	}
	
	public void removeAll() {
		slots = new HashMap<Integer,Slot>();
		
		for(int i = 0 ;i < 24;i++) {
			slots.put(i,new Slot(i));
		}
	}
	
	public void setPopupMenu(PopupMenu mnu) {
		mnuPop = mnu;
	}

	public void setDate(Date dt) {
		curDate = dt;
	}
	
	public void setEndOfDay(int offset) {
		endoffset = offset;
		repaint();
	}

	public void setStartOfDay(int offset) {
		dayoffset = offset;
		repaint();
	}

    /**
     * @param arg0
     */
    private void userClickedMouse(MouseEvent arg0)
    {
        Point pos = arg0.getPoint();
        lastPos = pos;
        int newItem = getIdAt(pos);
        
        if(newItem != selectedItem) {
        	selectedItem = newItem;
        	repaint();
        }
        if(arg0.getButton() == MouseEvent.BUTTON3) {
        	//mnuPop.show(arg0.getComponent(), pos.x, pos.y);
        }
    }
}
