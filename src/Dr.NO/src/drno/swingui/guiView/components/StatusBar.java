package drno.swingui.guiView.components;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JPanel;

public class StatusBar extends JPanel {
	private static final long serialVersionUID = 6738149352453219846L;
	private GridLayout layout;
	public StatusBar() {
		layout = new GridLayout();
		setLayout(layout);
		//setAlignmentX(Component.LEFT_ALIGNMENT);
	}
	
	public void addComponent(JComponent comp) {
		JPanel pan = new JPanel(new FlowLayout());
		pan.setBorder(BorderFactory.createLoweredBevelBorder());
		pan.add(comp);
		
		add(pan);
	}
}
