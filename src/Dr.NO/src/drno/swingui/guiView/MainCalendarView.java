/*
 * Created on 2005-04-25
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiView;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.beans.PropertyChangeListener;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import com.toedter.calendar.JCalendar;

import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservation;
import drno.swingui.guiController.TableHandler;
import drno.swingui.guiController.konsultationProgress;
import drno.swingui.guiView.components.JTimePane;
import drno.swingui.guiView.components.StatusBar;

/**
 * @author Leo
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
@SuppressWarnings("serial")
public class MainCalendarView extends JFrame {
	public final static int ONE_SECOND = 1000;
	private JButton btnAfslutKonsultation;
	private JButton btnStartKonsultation;
	GregorianCalendar c = new GregorianCalendar();
	public Object[][] data = new Object[35][2];

	Object[] DocList;

	Object[] DocStringList;

	private com.toedter.calendar.JCalendar jcCal;

	private JMenuItem jPopupAddTidsreservation;

	private JMenuItem jPopupEditPatient;

	private JPopupMenu popupMenu;

	// private JMenuItem jPopupEditTidsreservation;
	private JMenuItem jPopupSletTidsreservation;

	private JScrollPane jscrpHistory;
	final String[] kolloner1 = { "Tid", "Mandag", "Tirsdag", "Onsdag",
			"Torsdag", "Fredag" };

	final String[] kolloner2 = { "Tid", "Reservationer" };

	String konsultationMessage = "";

	Timestamp konsultationSlut, konsultationStart;

	private JLabel lblClock;

	private JLabel lblKonsultation;

	private JLabel lblKonsultationsInfo;

	private JList lstDocs;

	// GEN-END:initComponents

	private JList lstHistorik;

	private JMenuItem mnuAddPatient;

	private JMenuItem mnuEmployeeAdd;

	private JMenuItem mnuEmployeeSearch;

	// Variables declaration - do not modify//GEN-BEGIN:variables
	/*
	 * private JMenuItem jMenuAftaleSearch; private
	 * JMenuItem jMenuAftaleAdd; private JMenu
	 * AftaleMenu;
	 */
	private JMenu mnuEmployes;

	private JMenuItem mnuFarver;

	private JMenuItem mnuHelp;

	private JMenu mnuHelpBar;

	private JMenuBar mnuMainBar;

	private JMenu mnuPatients;

	private JMenuItem mnuSearchPatient;

	private JMenu mnuSettings;

	private JProgressBar pbKonsultation;

	Hashtable reservations = new Hashtable();

	private JScrollPane scrpnDoctorsHists;

	private JScrollPane scrpnMain;

	public int selectedCol = -1;

	int selectedList1Element = -1;

	int selectedList2Element = -1;

	public int selectedRow = -1;

	konsultationProgress task;

	long taskprogress;

	String test = "DR.NO system online...";

	private JTextArea textHistory;

	TableHandler th;

	private JTimePane timepane;

	private Timer timer, clockTimer;

	private JTabbedPane tpDocHist;

	Vector v = new Vector();

	/** Creates new form GUI */
	public MainCalendarView(Object[] docs) {
		// th = new TableHandler(lh);
		DocList = docs;
		DocStringList = new Object[DocList.length];
		task = new konsultationProgress();
		initComponents();
	}

	public void clearList2() {
		lstHistorik.setListData(new Vector());
		lstHistorik.repaint();
	}

	// End of variables declaration//GEN-END:variables

	public boolean getAfslutButtonStatus() {
		return btnAfslutKonsultation.isEnabled();
	}

	public Component[] getAllComponents() {
		return getContentPane().getComponents();
	}

	/**
	 * @return
	 */
	public java.util.Date getCalendarData() {
		// TODO Auto-generated method stub
		return jcCal.getDate();
	}

	public int getProgressBarMin() {
		return pbKonsultation.getMinimum();
	}

	public Object[][] getReservationFromDateAndDoc(Vector reservation) {
		try {
			DateFormat df2 = DateFormat.getTimeInstance(DateFormat.SHORT);
			reservations.clear();

			if (reservation.size() == 0) {
				return null;
			} else {
				for (int i = 0; i < reservation.size(); i++) {
					ITidsreservation rv = (ITidsreservation) reservation
							.elementAt(i);
					String tbase = df2.format(rv.getTidStart()); // tid fra
																	// databasen

					for (int j = 0; j < data.length; j++) {
						if (data[j][0].equals(tbase)) {
							reservations.put(data[j][0], rv);
							IPatient p = rv.getPatient();
							String navn = p.getFornavn() + " "
									+ p.getEfternavn();
							data[j][1] = navn;
						}
					}
				}
				return data;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	public int getSelectedCol() {
		return selectedCol;
	}

	/**
	 * @return
	 */
	public IEmployee getSelectedDoc() {
		// TODO Auto-generated method stub
		if (lstDocs.isSelectionEmpty()) {
			return null;
		} else {
			if (selectedList1Element >= 0) {
				return (IEmployee) DocList[selectedList1Element];
			} else {
				return null;
			}
		}
	}

	public int getSelectedRow() {
		return selectedRow;
	}

	/**
	 * @return
	 */
	public Object getSelectedTidsreservation() {
		Object key = timepane.getSelectedItem();
		return key;
	}

	public Timestamp getSelectedTimestamp() {
		return timepane.getSelectedTimestamp();
	}

	public boolean getStartButtonStatus() {
		return btnStartKonsultation.isEnabled();
	}



	private void initComponents() {
		// jPopupEditTidsreservation = new JMenuItem();

		scrpnMain = new JScrollPane();
		jscrpHistory = new JScrollPane();
		
		textHistory = new JTextArea();

		jcCal = new JCalendar();
		scrpnDoctorsHists = new JScrollPane();
		tpDocHist = new JTabbedPane();
		lstDocs = new JList();
		lstHistorik = new JList();
		pbKonsultation = new JProgressBar();
		btnStartKonsultation = new JButton();
		btnAfslutKonsultation = new JButton();
		

		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		//setResizable(false);
		setTitle("Dr. No Management System - Klient");
		initPopups();
		// jTable2.setBackground(new java.awt.Color(204, 204, 255));
		//jTable2.setComponentPopupMenu(popupMenu);

		// TODO
		/*
		 * Object[][] data = th.makeCalendarFromLogin(); jTable2.setModel(new
		 * DefaultTableModel(data, kolloner2)); TableColumn column = null;
		 * column = jTable2.getColumnModel().getColumn(0);
		 * column.setPreferredWidth(70); column =
		 * jTable2.getColumnModel().getColumn(1); column.setPreferredWidth(420);
		 * column.setResizable(false);
		 */
		// jTable2.setVisible(false);


		// getContentPane().add(jTable2, new
		// org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 490, 480));
		
		JPanel centerPane = new JPanel();
		timepane = new JTimePane();
		scrpnMain.setViewportView(timepane);
		scrpnMain.setPreferredSize(new Dimension(330,400));
		timepane.setComponentPopupMenu(popupMenu);
		scrpnMain.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createEmptyBorder(10,10,10,10),
                BorderFactory.createLoweredBevelBorder())); 
            
		
		centerPane.add(scrpnMain);

		textHistory.setEditable(false);
		textHistory.setBorder(BorderFactory.createTitledBorder("Information"));
		textHistory.setBackground(getBackground());
		jscrpHistory.setViewportView(textHistory);

		JPanel rightPanel = new JPanel();
		rightPanel.add(jscrpHistory);
		jscrpHistory.setAlignmentX(Component.CENTER_ALIGNMENT);
		jscrpHistory.setBorder(BorderFactory.createEmptyBorder());
		
		jcCal.setBorder(BorderFactory.createEtchedBorder());
		rightPanel.add(jcCal);
		jcCal.setAlignmentX(Component.CENTER_ALIGNMENT);
		jcCal.setMaximumSize(new Dimension(200,200));

		lstDocs.setBorder(BorderFactory.createTitledBorder("Doktor"));

		try {
			for (int i = 0; i < DocList.length; i++) {
				IEmployee e = (IEmployee) DocList[i];
				DocStringList[i] = e.asString();
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lstDocs = new JList(DocStringList);
		lstDocs.setVisible(true);
		ListSelectionModel LSM1 = lstDocs.getSelectionModel();
		LSM1.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();
				if (lsm.isSelectionEmpty()) {
					System.out.println("No rows are selected.");
				} else {
					selectedList1Element = lsm.getMinSelectionIndex();
				}
			}
		});

		tpDocHist.addTab("L�ger", lstDocs);

		lstHistorik.setBorder(BorderFactory.createTitledBorder(
				"Tidligere konsultationer"));
		ListSelectionModel LSM2 = lstHistorik.getSelectionModel();
		LSM2.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();
				if (lsm.isSelectionEmpty()) {

					// System.out.println("No rows are selected.");
				} else {
					selectedList2Element = lsm.getMinSelectionIndex();
				}
			}
		});
		/* getContentPane().add(
				lstHistorik,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 0, 170,
						280));*/
		tpDocHist.addTab("Historik", lstHistorik);

		scrpnDoctorsHists.setViewportView(tpDocHist);
		// getContentPane().add(jTabbedPane1, new
		// org.netbeans.lib.awtextra.AbsoluteConstraints(730, 0, 170, 280));
		
		pbKonsultation.setMaximum(60);
		pbKonsultation.setMinimum(0);
		pbKonsultation.setValue(0);
		pbKonsultation.setStringPainted(true);
		StatusBar statusBar = new StatusBar();
		statusBar.addComponent(pbKonsultation);
		
		JPanel leftPanel = new JPanel();
		
		leftPanel.add(scrpnDoctorsHists);
		scrpnDoctorsHists.setAlignmentX(Component.CENTER_ALIGNMENT);
		scrpnDoctorsHists.setBorder(BorderFactory.createEmptyBorder());
		leftPanel.setLayout(new BoxLayout(leftPanel,BoxLayout.Y_AXIS));
		
		rightPanel.setLayout(new BoxLayout(rightPanel,BoxLayout.Y_AXIS));
		centerPane.setLayout(new BoxLayout(centerPane,BoxLayout.Y_AXIS));
		
		lblKonsultationsInfo = new JLabel("Ingen Konsultationer i gang");
		lblKonsultationsInfo.setAlignmentX(Component.CENTER_ALIGNMENT);
		leftPanel.add(lblKonsultationsInfo);
		
		lblClock = new JLabel("Clock");
		lblClock.setForeground(new java.awt.Color(153, 0, 0));
		lblClock.setAlignmentX(Component.CENTER_ALIGNMENT);
		leftPanel.add(lblClock);
		
		btnStartKonsultation = new JButton("Start Konsultation");
		btnStartKonsultation.setEnabled(false);
		btnStartKonsultation.setAlignmentX(Component.CENTER_ALIGNMENT);
		leftPanel.add(btnStartKonsultation);
		
		btnAfslutKonsultation.setText("Slut Konsultation");
		btnAfslutKonsultation.setEnabled(false);
		btnAfslutKonsultation.setAlignmentX(Component.CENTER_ALIGNMENT);
		leftPanel.add(btnAfslutKonsultation);
		
		
		JPanel mainPane = new JPanel(new BorderLayout());
		
		mainPane.add(leftPanel,BorderLayout.WEST);
		mainPane.add(rightPanel,BorderLayout.EAST);
		
		statusBar.addComponent(new JLabel("Hello world"));
		mainPane.add(statusBar,BorderLayout.SOUTH);
		mainPane.add(centerPane,BorderLayout.CENTER);
		centerPane.setBorder(BorderFactory.createEmptyBorder(3, 3, 3, 3));
		rightPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		leftPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		getContentPane().removeAll();
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(mainPane,BorderLayout.CENTER);
		        
        initMenus();
				
		setMinimumSize(new Dimension(800,600));
		pack();
		
		setLocationRelativeTo(null);
		setResizable(true);
	}

    /**
     * 
     */
    private void initMenus()
    {
        mnuMainBar = new JMenuBar();
        /*
         * AftaleMenu = new JMenu(); AftaleMenu.setName("Menu");
         * jMenuAftaleAdd = new JMenuItem();
         * jMenuAftaleAdd.setName("Menu"); jMenuAftaleSearch = new
         * JMenuItem(); jMenuAftaleSearch.setName("Menu");
         */
        
//        mnuSearchPatient = new JMenuItem();
//        mnuSettings = new JMenu();
//        mnuFarver = new JMenuItem();
//        mnuHelpBar = new JMenu();
//        mnuHelp = new JMenuItem();
//        

		mnuMainBar.add(mnuPatients = new JMenu("Patienter"));
        mnuPatients.setMnemonic(KeyEvent.VK_P);
        
        mnuPatients.add(mnuAddPatient = new JMenuItem("Add patient"));
		mnuPatients.add(mnuSearchPatient = new JMenuItem("Seek patients"));
		
		mnuMainBar.add(mnuEmployes = new JMenu("Employees"));
		mnuEmployes.add(mnuEmployeeAdd = new JMenuItem("Add employee"));
		mnuEmployes.add(mnuEmployeeSearch = new JMenuItem("Seek employee"));
		mnuMainBar.add(mnuSettings = new JMenu("Settings"));
		mnuSettings.add(mnuFarver = new JMenuItem("Looks"));
		mnuMainBar.add(mnuHelpBar = new JMenu("Help"));
		mnuHelpBar.add(mnuHelp = new JMenuItem("Seek help"));
		mnuMainBar.setBorder(BorderFactory.createEmptyBorder());
		setJMenuBar(mnuMainBar);
    }

    /**
     * Generates the default popup menu for the calendar
     */
    private void initPopups()
    {
        
        popupMenu = new JPopupMenu();
        jPopupEditPatient = new JMenuItem("Ret Patient");
		popupMenu.add(jPopupEditPatient);
		jPopupEditPatient.setEnabled(false);
		
//		jPopupEditTidsreservation.setText("Ret Tidsreservation");
//		popupMenu.add(jPopupEditTidsreservation);
//		jPopupEditTidsreservation.setEnabled(false);
		
        jPopupSletTidsreservation = new JMenuItem("Slet Tidsreservation");
		popupMenu.add(jPopupSletTidsreservation);
		jPopupSletTidsreservation.setEnabled(false);
        
        jPopupAddTidsreservation = new JMenuItem("Tilf�j Tidsreservation");
		popupMenu.add(jPopupAddTidsreservation);
		jPopupAddTidsreservation.setEnabled(true);
    }

	public boolean isAnsatSelectionEmpty() {
		return lstDocs.isSelectionEmpty();
	}

	public String PInfo_toString(IPatient p) {
		try {
			String output = "PATIENT INFO:\n" + "Fornavn:\t" + p.getFornavn()
					+ "\n" + "Efternavn:\t" + p.getEfternavn() + "\n"
					+ "CPR:\t" + p.getCpr() + "\n" + "Adresse:\t"
					+ p.getAdresse() + "\n" + "TLF:\t" + p.getTlf() + "\n"
					+ "Alder:\t" + p.getAlder() + "\n\n";
			return output;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	// set/get operations...
	public void printText(String str) {
		textHistory.setText(str);
	}

	public void printTextTogether(String str) {
		textHistory.setText(textHistory.getText() + "\n" + str);
	}

	public void setAfslutButtonStatus(boolean e) {
		btnAfslutKonsultation.setEnabled(e);
	}

	public void setBackgroundColor(JComponent k, Color c) {
		k.setBackground(c);
	}
	
	public void setCalendarFromDoctorList(MouseAdapter ma) {
		lstDocs.addMouseListener(ma);
	}

	public void setCalendarListener(PropertyChangeListener pl) {
		jcCal.addPropertyChangeListener(pl);
	}

	public void setClockText(String t) {
		lblClock.setText(t);
	}


	public void setEmployeeAddListener(ActionListener al) {
		mnuEmployeeAdd.addActionListener(al);
	}

	public void setEmployeeSearchListener(ActionListener al) {
		mnuEmployeeSearch.addActionListener(al);
	}

	/*
	 * public void setAftaleAddListener(ActionListener al) {
	 * jMenuAftaleAdd.addActionListener(al); } public void
	 * setAftaleSearchListener(ActionListener al) {
	 * jMenuAftaleSearch.addActionListener(al); }
	 */
	public void setFarverListener(ActionListener al) {
		mnuFarver.addActionListener(al);
	}

	public void setForegroundColor(JComponent k, Color c) {
		k.setForeground(c);
		// System.out.println(jScrollPane1.getViewport().getComponents()[0].getName());
	}

	public void setHj�lpMenuListener(ActionListener al) {
		mnuHelp.addActionListener(al);
	}

	public void setKonsultationText(String t) {
		lblKonsultation.setText(t);
	}

	public void setKonsultationVisible(boolean e) {
		lblKonsultation.setVisible(e);
	}

	public void setLabel1Text(String t) {
		lblKonsultationsInfo.setText(t);
	}

	public void setLblClockVisible(boolean e) {
		lblClock.setVisible(e);
	}

	public void setList1BackgroundColor(Color c) {
		lstDocs.setBackground(c);
	}

	public void setList1ForegroundColor(Color c) {
		lstDocs.setForeground(c);
	}

	public void setList2BackgroundColor(Color c) {
		lstHistorik.setBackground(c);
	}

	
	public void setList2ForegroundColor(Color c) {
		lstHistorik.setForeground(c);
	}

	public void setMenuBackgroundColor(Color c) {
		MenuElement[] menus = getJMenuBar().getSubElements();
		for (int i = 0; i < menus.length; i++) {
			JMenu menu = (JMenu) menus[i];
			menu.setBackground(c);
			int count = menu.getItemCount();
			for (int j = 0; j < count; j++) {
				menu.getItem(j).setBackground(c);
			}
		}
	} 

	public void setMenuForegroundColor(Color c) {
		MenuElement[] menus = getJMenuBar().getSubElements();
		for (int i = 0; i < menus.length; i++) {
			JMenu menu = (JMenu) menus[i];
			menu.setForeground(c);
			int count = menu.getItemCount();
			for (int j = 0; j < count; j++) {
				menu.getItem(j).setForeground(c);
			}
		}
	}

	// Actionlisteners
	public void setPatientAddListener(ActionListener al) {
		mnuAddPatient.addActionListener(al);
	}

	/**
	 * @param t
	 * @param p
	 * @param v2
	 */

	public void setPatientInfo(ITidsreservation t, IPatient p, Vector v2) {
		try {
			printText(TInfo_toString(t));
			printTextTogether(PInfo_toString(p));
			v.clear();
			v = (Vector) v2.clone();
			Object[] rp = new Object[v.size()];
			for (int i = 0; i < v.size(); i++) {
				ITidsreservation it = (ITidsreservation) v.elementAt(i);
				rp[i] = it.getTidStart();
			}
			lstHistorik.setListData(rp);
			lstHistorik.setVisible(true);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setPatientInfoFromAftale(MouseAdapter ma) {
		lstHistorik.addMouseListener(ma);
	}

	public void setPatientSearchListener(ActionListener al) {
		mnuSearchPatient.addActionListener(al);
	}

	public void setPopupAddAftaleListener(ActionListener listener) {
		jPopupAddTidsreservation.addActionListener(listener);
	}

	public void setPopupAddTidsreservation(boolean e) {
		jPopupAddTidsreservation.setEnabled(e);
	}

	public void setPopupDeleteAftaleListener(ActionListener listener) {
		jPopupSletTidsreservation.addActionListener(listener);
	}

	public void setPopupEditPatient(boolean e) {
		jPopupEditPatient.setEnabled(e);
	}

	/*
	 * public void setPopupEditAftaleListener(ActionListener listener) {
	 * jPopupEditTidsreservation.addActionListener(listener); }
	 */
	public void setPopupEditPatientListener(ActionListener listener) {
		jPopupEditPatient.addActionListener(listener);
	}

	/*
	 * public void setPopupEditTidsreservation(boolean e){
	 * jPopupEditTidsreservation.setEnabled(e); }
	 */
	public void setPopupSletTidsreservation(boolean e) {
		jPopupSletTidsreservation.setEnabled(e);
	}

	public void setProgressBarMax(int max) {
		pbKonsultation.setMaximum(max);
	}

	public void setProgressBarString(String s) {
		pbKonsultation.setString(s);
	}

	public void setProgressBarValue(int v) {
		pbKonsultation.setValue(v);
	}

	/**
	 * @param user
	 */
	public void setSelectedDoc(IEmployee user) {
		try {
			// TODO Auto-generated method stub

			for (int i = 0; i < DocList.length; i++) {
				IEmployee e = (IEmployee) DocList[i];
				if (user.asString().equals(e.asString())) {
					lstDocs.setSelectedIndex(i);
					break;
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setSlutListener(ActionListener al) {
		btnAfslutKonsultation.addActionListener(al);
	}

	public void setStartButtonStatus(boolean e) {
		btnStartKonsultation.setEnabled(e);
	}

	public void setStartListener(ActionListener al) {
		btnStartKonsultation.addActionListener(al);
	}



	public void setTableMouseListener(MouseAdapter ma) {
		//jTable2.addMouseListener(ma);
	}

	public void setTextAreaBackgroundColor(Color c) {
		textHistory.setBackground(c);
	}

	public void setTextAreaForegroundColor(Color c) {
		textHistory.setForeground(c);
	}

	/**
	 * 
	 */
	public void showOldReservation() {
		// TODO Auto-generated method stub
		try {
			ITidsreservation t = (ITidsreservation) v
					.elementAt(selectedList2Element);
			printText(TInfo_toString(t));
			IPatient p = t.getPatient();
			printTextTogether(PInfo_toString(p));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String TInfo_toString(ITidsreservation t) {
		try {
			String output = "RESERVATION INFO:\n" + "Tidstart:\t"
					+ t.getTidStart() + "\n" + "Emne:\t" + t.getEmne() + "\n";
			return output;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public void update_calendar(Vector<ITidsreservation> reservation) {
		timepane.removeAll();
		for(ITidsreservation res : reservation) {
			Timestamp start;
			Timestamp slut;
			try {
				start = res.getTidStart();
				slut = res.getTidSlut();
			
				GregorianCalendar cStart = new GregorianCalendar();
				GregorianCalendar cSlut = new GregorianCalendar();
				cStart.setTimeInMillis(start.getTime());
				cSlut.setTimeInMillis(slut.getTime());
				int iStart = cStart.get(Calendar.HOUR_OF_DAY);
				int iSlut = cSlut.get(Calendar.HOUR_OF_DAY);
				timepane.insertDate(res.toString(), res, iStart, iSlut);
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}




	public void updateDocList(Object[] dl) {
		DocList = dl;
	}

	public void updateDocs(Object[] doclist) {
		try {
			for (int i = 0; i < doclist.length; i++) {
				IEmployee e = (IEmployee) doclist[i];
				doclist[i] = e.asString();
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		lstDocs.setListData(doclist);
	}

	public void updateList2(Vector v) {
		lstHistorik.setListData(v);
	}

    public void setTidsopdeling(int tidsOpdeling)
    {
        System.out.println("setTidsopling ikke impl");
        
    }
}