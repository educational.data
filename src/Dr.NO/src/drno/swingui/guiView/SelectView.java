/*
 * temp.java
 *
 * Created on 10. april 2005, 17:21
 */

package drno.swingui.guiView;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.util.Vector;

import javax.swing.table.*;

import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;

/**
 *
 * @author  Leo
 */
public class SelectView extends JFrame {
    private Vector vdata = new Vector();   
    /** Creates a new instance of temp */
    
    //PatientHandler p_handler = new PatientHandler();
    //TableHandler th = new TableHandler();
    //ImageIcon Leo = new ImageIcon("Leo.jpg", "Leo");
    final String[] kolloner1 = {"Navn", "CPR", "Adresse", "TLF", "K�n", "Patient hos"};
    final String[] kolloner2 = {"Navn","Type","Login","Telefonnr","Tidsopdeling"};
    //final Object[][] data1 = th.getTableData();
    //final String[] kolloner2 = {"Tid", "Mandag","Tirsdag","Onsdag","Torsdag","Fredag"};
    //final Object[][] data2 = th.makeCalendar(20);

    //private javax.swing.JTable jTable1;
   //Object[][] data1;
   
    private void jbInit(boolean b) throws Exception{
       
        jTable1 = new JTable();
        jPanel1 = new JPanel();
        
        jScrollPane1 = new JScrollPane();
        jButtonEdit = new javax.swing.JButton();
        jButtonDelete = new javax.swing.JButton();
		jButtonCancel = new javax.swing.JButton();
        
        getContentPane().setLayout(new BoxLayout(getContentPane(),BoxLayout.PAGE_AXIS));
        
       	jScrollPane1.setViewportView(jTable1);
       	
       	
    	jTable1.setVisible(true);
    	
    	jScrollPane1.getViewport().add(jTable1,null);
              
        getContentPane().add(jScrollPane1);
        jTable1.setVisible(true);
        jScrollPane1.setMaximumSize(new Dimension(600,200));
        jButtonEdit.setText("Ret");
        jButtonDelete.setText("Slet");
		jButtonCancel.setText("Annuller");
		
        jPanel1.add(jButtonEdit);
		if (b==true)
		{
			jButtonEdit.setText("OK");
		}else{
			jPanel1.add(jButtonDelete);
		}
		jPanel1.add(jButtonCancel);
        getContentPane().add(jPanel1);
        
        
        setDefaultCloseOperation(javax.swing.WindowConstants.HIDE_ON_CLOSE);
        pack(); 
        
        Dimension dim = getToolkit().getScreenSize();
        Rectangle rec = getBounds();
        int x = dim.width/2-rec.width/2;
        int y = dim.height/2-rec.height/2;
        setLocation(x,y);
    }
    /** Creates a new instance of DatabaseGUI 
     * @param b*/
    public SelectView(boolean b) {
        try{
            jbInit(b);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    private void pressed(java.awt.event.MouseEvent evt) {
        jTable1.setVisible(true);
    }
    //public ImageIcon Leo = new ImageIcon("Leo.jpeg", "LeoB");
    //public ImageIcon Tob = new ImageIcon("Tob.jpeg", "TobB");
    //private PatientHandler p_handler = new PatientHandler();
    //private TableHandler th = new TableHandler();

    private JButton jButtonEdit;
    private JButton jButtonDelete;
	private JButton jButtonCancel;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel0;
    private JScrollPane jScrollPane1;
    private javax.swing.JPasswordField jPasswordField1;
    private javax.swing.JTable jTable1;
    
    public Object getSelected(){
    	return vdata.elementAt(jTable1.getSelectedRow());
    	
    }
    

    
    public void setPatientData(Vector ip){
    	vdata = (Vector)ip.clone();
    	Object[][] data = new Object[ip.size()][6];
        
    	try {
			for (int i = 0; i < ip.size(); i++) {
				data[i][0] = ((IPatient) ip.elementAt(i)).getFornavn() + " "
						+ ((IPatient) ip.elementAt(i)).getEfternavn();
				data[i][1] = ((IPatient) ip.elementAt(i)).getCpr();
				data[i][2] = ((IPatient) ip.elementAt(i)).getAdresse();
				data[i][3] = ((IPatient) ip.elementAt(i)).getTlf();
				//data[i][4] = new Integer(((IPatient) ip.elementAt(i)).getSex());
				if(((IPatient) ip.elementAt(i)).getSex()==IPatient.SEX_MALE){
					data[i][4] = "Mand";
				}
				else{
					data[i][4] = "Kvinde";
				}
				data[i][5] = new EmployeeStringContainer(((IPatient) ip.elementAt(i)).getDoctor());
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    	
    	jTable1.setModel(new DefaultTableModel(data, kolloner1));
    	jScrollPane1.setViewportView(jTable1);
    	jTable1.setVisible(true);
    }
    public void setData(Object[][] data)
    {
    	Object [][] data1 = data;
    	jTable1.setModel(new DefaultTableModel(data1, kolloner1));
    	jScrollPane1.setViewportView(jTable1);
    	jTable1.setVisible(true);
    	//System.out.println("�sdalfkj");
    
    }
    
    
    public void setEditListener(ActionListener al){
    	jButtonEdit.addActionListener(al);
    }
    
    public void setDeleteListener(ActionListener al){
    	jButtonDelete.addActionListener(al);
    }
	public void setCancelListener(ActionListener al){
    	jButtonCancel.addActionListener(al);
    }
	/**
	 * @param vector
	 */
	public void setAftaleData(Vector vector) {
		// TODO Auto-generated method stub
		
	}
	/**
	 * @param vector
	 */
	public void setEmployeeData(Vector ip) {
    	vdata = (Vector)ip.clone();
    	Object[][] data = new Object[ip.size()][5];
        
    	try {
			for (int i = 0; i < ip.size(); i++) {
				data[i][0] = ((IEmployee) ip.elementAt(i)).getFornavn() + " "
						+ ((IEmployee) ip.elementAt(i)).getEfternavn();
				if(((IEmployee) ip.elementAt(i)).getType()==IEmployee.TYPE_ADMIN){
					data[i][1] = "Admin";
				}
				if(((IEmployee) ip.elementAt(i)).getType()==IEmployee.TYPE_DOCTOR){
					data[i][1] = "L�ge";
				}
				if(((IEmployee) ip.elementAt(i)).getType()==IEmployee.TYPE_NURSE){
					data[i][1] = "Sygeplejerske";
				}
				if(((IEmployee) ip.elementAt(i)).getType()==IEmployee.TYPE_SECRETARY){
					data[i][1] = "Sekret�r";
				}
				data[i][2] = ((IEmployee) ip.elementAt(i)).getLogin();
				data[i][3] = ((IEmployee) ip.elementAt(i)).getTelefonNummer();
				data[i][4] = new Integer(((IEmployee) ip.elementAt(i)).getTidsOpdeling());
				
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    	
    	jTable1.setModel(new DefaultTableModel(data, kolloner2));
    	jScrollPane1.setViewportView(jTable1);
    	jTable1.setVisible(true);
	}
}
