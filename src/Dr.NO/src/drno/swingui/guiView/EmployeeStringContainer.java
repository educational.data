/*
 * Created on 2005-05-02
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiView;

import java.rmi.RemoteException;

import drno.interfaces.IEmployee;


/**
 * @author Bo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EmployeeStringContainer {
	private IEmployee emp;
	public EmployeeStringContainer(IEmployee e) {
		emp = e;
	}
	
	public boolean equals(Object o){
		if(o instanceof EmployeeStringContainer)
			return toString().equals(o.toString());
		
		return false;
	}
	
	public IEmployee getEmployee() { 
		return emp;
	}
	public String toString() {
		try {
			return emp.getFornavn() + " " + emp.getEfternavn();
		} catch (RemoteException e) {
			e.printStackTrace();
			return new String();
		}
	}
}
