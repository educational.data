/*
 * Created on 2005-04-25
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.swingui.guiView;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.beans.PropertyChangeListener;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.DebugGraphics;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.MenuElement;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservation;
import drno.swingui.guiController.TableHandler;
import drno.swingui.guiController.konsultationProgress;

/**
 * @author Leo
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
@SuppressWarnings("serial")
public class OldMainCalendarView extends JFrame {
	public int selectedRow = -1;

	public int selectedCol = -1;

	int selectedList1Element = -1;

	int selectedList2Element = -1;

	public final static int ONE_SECOND = 1000;

	long taskprogress;

	Hashtable reservations = new Hashtable();

	TableHandler th;

	final String[] kolloner2 = { "Tid", "Reservationer" };

	public Object[][] data = new Object[35][2];

	Vector v = new Vector();

	Object[] DocList;

	Object[] DocStringList;

	private Timer timer, clockTimer;

	GregorianCalendar c = new GregorianCalendar();

	Timestamp konsultationSlut, konsultationStart;

	konsultationProgress task;

	String konsultationMessage = "";

	String test = "DR.NO system online...";

	final String[] kolloner1 = { "Tid", "Mandag", "Tirsdag", "Onsdag",
			"Torsdag", "Fredag" };

	/** Creates new form GUI */
	public OldMainCalendarView(Object[] docs) {
		// th = new TableHandler(lh);
		DocList = docs;
		DocStringList = new Object[DocList.length];
		task = new konsultationProgress();
		initComponents();
	}

	private void initComponents() {// GEN-BEGIN:initComponents
		jPopupMenu1 = new JPopupMenu();
		jPopupEditPatient = new JMenuItem();
		// jPopupEditTidsreservation = new JMenuItem();
		jPopupSletTidsreservation = new JMenuItem();
		jPopupAddTidsreservation = new JMenuItem();
		jScrollPane1 = new JScrollPane();
		jScrollPane1.setName("ScrollPane");
		jTable2 = new JTable();
		jTable2.setName("Tabel");
		jScrollPane3 = new JScrollPane();
		jScrollPane3.setName("ScrollPane3");
		jTextArea2 = new JTextArea();
		jTextArea2.setText(test);
		jTextArea2.setName("Infomation");
		jCalendar1 = new com.toedter.calendar.JCalendar();
		jCalendar1.setName("Kalender");
		jScrollPane2 = new JScrollPane();
		jScrollPane2.setName("ScrollPane");
		jTabbedPane1 = new JTabbedPane();
		jTabbedPane1.setName("TabbedPane");
		jList1 = new JList();
		jList1.setName("Ansat");
		jList2 = new JList();
		jList2.setName("Tidsreservation");
		jProgressBar1 = new JProgressBar();
		jProgressBar1.setName("ProgressBar");
		jLabel1 = new JLabel();
		jLabel1.setName("Label");
		jLabel2 = new JLabel();
		jLabel2.setName("Label");
		jLabel3 = new JLabel();
		jLabel3.setName("Label");
		jButtonStartKonsultation = new JButton();
		jButtonStartKonsultation.setName("Knapper");
		jButtonAfslutKonsultation = new JButton();
		jButtonAfslutKonsultation.setName("Knapper");
		MainMenuBar = new JMenuBar();
		MainMenuBar.setName("Menu");
		/*
		 * AftaleMenu = new JMenu(); AftaleMenu.setName("Menu");
		 * jMenuAftaleAdd = new JMenuItem();
		 * jMenuAftaleAdd.setName("Menu"); jMenuAftaleSearch = new
		 * JMenuItem(); jMenuAftaleSearch.setName("Menu");
		 */
		PatienterMenu = new JMenu();
		PatienterMenu.setName("Menu");
		jMenuAddPatient = new JMenuItem();
		jMenuAddPatient.setName("Menu");
		jMenuSearchPatient = new JMenuItem();
		jMenuSearchPatient.setName("Menu");
		AnsatteMenu = new JMenu();
		AnsatteMenu.setName("Menu");
		jMenuEmployeeAdd = new JMenuItem();
		jMenuEmployeeAdd.setName("Menu");
		ERet = new JMenuItem();
		ERet.setName("Menu");
		jMenuEmployeeSearch = new JMenuItem();
		jMenuEmployeeSearch.setName("Menu");
		IndstillingMenu = new JMenu();
		IndstillingMenu.setName("Menu");
		jMenuFarver = new JMenuItem();
		jMenuFarver.setName("Menu");
		Hj�lpMenu = new JMenu();
		Hj�lpMenu.setName("Menu");
		jMenuHj�lp = new JMenuItem();
		jMenuHj�lp.setName("Menu");

		// ** Ops�tning af vindue
		getContentPane().setLayout(
				new org.netbeans.lib.awtextra.AbsoluteLayout());
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setResizable(false);
		setTitle("Dr. No Management System - Klient");
		// ** Popup-menu
		jPopupEditPatient.setText("Ret Patient");
		jPopupMenu1.add(jPopupEditPatient);
		jPopupEditPatient.setEnabled(false);
		/*
		 * jPopupEditTidsreservation.setText("Ret Tidsreservation");
		 * jPopupMenu1.add(jPopupEditTidsreservation);
		 * jPopupEditTidsreservation.setEnabled(false);
		 */
		jPopupSletTidsreservation.setText("Slet Tidsreservation");
		jPopupMenu1.add(jPopupSletTidsreservation);
		jPopupSletTidsreservation.setEnabled(false);
		jPopupAddTidsreservation.setText("Tilf�j Tidsreservation");
		jPopupMenu1.add(jPopupAddTidsreservation);
		jPopupAddTidsreservation.setEnabled(true);
		// jTable2.setBackground(new java.awt.Color(204, 204, 255));
		jTable2.setComponentPopupMenu(jPopupMenu1);

		// TODO
		/*
		 * Object[][] data = th.makeCalendarFromLogin(); jTable2.setModel(new
		 * DefaultTableModel(data, kolloner2)); TableColumn column = null;
		 * column = jTable2.getColumnModel().getColumn(0);
		 * column.setPreferredWidth(70); column =
		 * jTable2.getColumnModel().getColumn(1); column.setPreferredWidth(420);
		 * column.setResizable(false);
		 */
		// jTable2.setVisible(false);
		jTable2.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		jTable2.setCellSelectionEnabled(true);
		jTable2.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		jTable2.setVisible(false);

		ListSelectionModel rowSM = jTable2.getSelectionModel();
		rowSM.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();
				if (lsm.isSelectionEmpty()) {

					// System.out.println("No rows are selected.");
				} else {
					selectedRow = lsm.getMinSelectionIndex();

				}
			}
		});

		jTable2.setColumnSelectionAllowed(true);
		ListSelectionModel colSM = jTable2.getColumnModel().getSelectionModel();
		colSM.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();
				if (lsm.isSelectionEmpty()) {
					// System.out.println("No columns are selected.");
				} else {
					selectedCol = lsm.getMinSelectionIndex();
					// System.out.println("Column " + selectedCol
					// + " is now selected.");
				}
			}
		});

		// getContentPane().add(jTable2, new
		// org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 490, 480));
		jScrollPane1.setViewportView(jTable2);
		getContentPane().add(
				jScrollPane1,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 490,
						480));

		jTextArea2.setEditable(false);
		jTextArea2
				.setBorder(BorderFactory.createTitledBorder("Information"));
		jScrollPane3.setViewportView(jTextArea2);
		// getContentPane().add(jTextArea2, new
		// org.netbeans.lib.awtextra.AbsoluteConstraints(490, 0, 240, 280));
		getContentPane().add(
				jScrollPane3,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 0, 240,
						280));

		jCalendar1.setBorder(BorderFactory.createEtchedBorder());
		getContentPane().add(
				jCalendar1,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 280,
						240, 200));

		jList1.setBorder(BorderFactory.createTitledBorder("Doktor"));

		try {
			for (int i = 0; i < DocList.length; i++) {
				IEmployee e = (IEmployee) DocList[i];
				DocStringList[i] = e.asString();
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		jList1 = new JList(DocStringList);
		jList1.setVisible(true);
		ListSelectionModel LSM1 = jList1.getSelectionModel();
		LSM1.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();
				if (lsm.isSelectionEmpty()) {

					// System.out.println("No rows are selected.");
				} else {
					selectedList1Element = lsm.getMinSelectionIndex();
				}
			}
		});
		getContentPane().add(
				jList1,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 0, 170,
						280));
		jTabbedPane1.addTab("L�ger", jList1);

		jList2.setBorder(BorderFactory.createTitledBorder(
				"Tidligere konsultationer"));
		ListSelectionModel LSM2 = jList2.getSelectionModel();
		LSM2.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				// Ignore extra messages.
				if (e.getValueIsAdjusting())
					return;

				ListSelectionModel lsm = (ListSelectionModel) e.getSource();
				if (lsm.isSelectionEmpty()) {

					// System.out.println("No rows are selected.");
				} else {
					selectedList2Element = lsm.getMinSelectionIndex();
				}
			}
		});
		getContentPane().add(
				jList2,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 0, 170,
						280));
		jTabbedPane1.addTab("Historik", jList2);

		jScrollPane2.setViewportView(jTabbedPane1);
		// getContentPane().add(jTabbedPane1, new
		// org.netbeans.lib.awtextra.AbsoluteConstraints(730, 0, 170, 280));
		getContentPane().add(
				jScrollPane2,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 0, 170,
						280));

		jProgressBar1.setMaximum(60);
		jProgressBar1.setMinimum(0);
		jProgressBar1.setValue(0);
		jProgressBar1.setStringPainted(true);
		getContentPane().add(
				jProgressBar1,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 480, 730,
						20));

		jLabel1.setText("Ingen Konsultationer i gang");
		jLabel1.setHorizontalTextPosition(SwingConstants.CENTER);
		getContentPane().add(
				jLabel1,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 420,
						160, 30));
		jButtonStartKonsultation.setText("Start Konsultation");
		jButtonStartKonsultation.setEnabled(false);
		jButtonStartKonsultation
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						// jButton1ActionPerformed(evt);
					}
				});

		getContentPane().add(
				jButtonStartKonsultation,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(745, 340,
						140, -1));
		jButtonAfslutKonsultation.setText("Slut Konsultation");
		jButtonAfslutKonsultation.setEnabled(false);
		jButtonAfslutKonsultation
				.addActionListener(new java.awt.event.ActionListener() {
					public void actionPerformed(java.awt.event.ActionEvent evt) {
						// jButton2ActionPerformed(evt);
					}
				});

		jLabel2.setForeground(new java.awt.Color(153, 0, 0));
		jLabel2.setHorizontalAlignment(SwingConstants.CENTER);
		jLabel2.setText("Clock");
		// jLabel2.setBorder(new border.LineBorder(new
		// java.awt.Color(0, 0, 0)));
		jLabel2.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		getContentPane().add(
				jLabel2,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 300,
						130, 20));

		
		jLabel3.setText("jLabel3");
		jLabel3.setVisible(false);
		getContentPane().add(
				jLabel3,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 460,
						150, -1));

		getContentPane().add(
				jButtonAfslutKonsultation,
				new org.netbeans.lib.awtextra.AbsoluteConstraints(745, 380,
						140, -1));
		/*
		 * AftaleMenu.setText("Aftale"); AftaleMenu.addActionListener(new
		 * java.awt.event.ActionListener() { public void
		 * actionPerformed(java.awt.event.ActionEvent evt) { //
		 * AftaleMenuActionPerformed(evt); } });
		 * 
		 * jMenuAftaleAdd.setText("Tilf\u00f8j");
		 * jMenuAftaleAdd.addActionListener(new java.awt.event.ActionListener() {
		 * public void actionPerformed(java.awt.event.ActionEvent evt) { //
		 * ATilf�jActionPerformed(evt); } });
		 * jMenuAftaleAdd.addMouseListener(new java.awt.event.MouseAdapter() {
		 * public void mouseClicked(java.awt.event.MouseEvent evt) { //
		 * ATilf�j(evt); } });
		 * 
		 * AftaleMenu.add(jMenuAftaleAdd);
		 * 
		 * jMenuAftaleSearch.setText("S\u00f8g");
		 * AftaleMenu.add(jMenuAftaleSearch);
		 * 
		 * MainMenuBar.add(AftaleMenu); //getContentPane().add(MainMenuBar);
		 */
		PatienterMenu.setText("Patienter");
		jMenuAddPatient.setText("Tilf\u00f8j");

		PatienterMenu.add(jMenuAddPatient);

		jMenuSearchPatient.setText("S\u00f8g");

		PatienterMenu.add(jMenuSearchPatient);

		MainMenuBar.add(PatienterMenu);

		AnsatteMenu.setText("Ansatte");
		jMenuEmployeeAdd.setText("Tilf\u00f8j");

		AnsatteMenu.add(jMenuEmployeeAdd);

		jMenuEmployeeSearch.setText("S�g");
		AnsatteMenu.add(jMenuEmployeeSearch);

		MainMenuBar.add(AnsatteMenu);

		MainMenuBar.add(IndstillingMenu);
		IndstillingMenu.setText("Indstilling");
		IndstillingMenu.add(jMenuFarver);
		jMenuFarver.setText("Udseende");

		MainMenuBar.add(Hj�lpMenu);
		Hj�lpMenu.setText("Hj�lp");
		Hj�lpMenu.add(jMenuHj�lp);
		jMenuHj�lp.setText("S�g Hj�lp");
		setJMenuBar(MainMenuBar);
		pack();

		Dimension dim = getToolkit().getScreenSize();
		Rectangle rec = getBounds();
		int x = dim.width / 2 - rec.width / 2;
		int y = dim.height / 2 - rec.height / 2;
		setLocation(x, y);
	}

	// GEN-END:initComponents

	// Variables declaration - do not modify//GEN-BEGIN:variables
	/*
	 * private JMenuItem jMenuAftaleSearch; private
	 * JMenuItem jMenuAftaleAdd; private JMenu
	 * AftaleMenu;
	 */
	private JMenu AnsatteMenu;

	private JMenuItem ERet;

	private JMenuItem jMenuEmployeeSearch;

	private JMenuItem jMenuEmployeeAdd;

	private JMenuBar MainMenuBar;

	private JMenuItem jMenuSearchPatient;

	private JMenuItem jMenuAddPatient;

	private JMenu PatienterMenu;

	private JMenu IndstillingMenu;

	private JMenuItem jMenuFarver;

	private JMenu Hj�lpMenu;

	private JMenuItem jMenuHj�lp;

	private com.toedter.calendar.JCalendar jCalendar1;

	private JScrollPane jScrollPane1;

	private JScrollPane jScrollPane3;

	private JTable jTable2;

	private JTextArea jTextArea2;

	private JButton jButtonStartKonsultation;

	private JButton jButtonAfslutKonsultation;

	private JLabel jLabel1;

	private JLabel jLabel2;

	private JLabel jLabel3;

	private JList jList1;

	private JList jList2;

	private JMenuItem jPopupEditPatient;

	// private JMenuItem jPopupEditTidsreservation;
	private JMenuItem jPopupSletTidsreservation;

	private JMenuItem jPopupAddTidsreservation;

	private JPopupMenu jPopupMenu1;

	private JProgressBar jProgressBar1;

	private JScrollPane jScrollPane2;

	private JTabbedPane jTabbedPane1;

	// End of variables declaration//GEN-END:variables

	// Actionlisteners
	public void setPatientAddListener(ActionListener al) {
		jMenuAddPatient.addActionListener(al);
	}

	public void setPatientSearchListener(ActionListener al) {
		jMenuSearchPatient.addActionListener(al);
	}

	public void setEmployeeAddListener(ActionListener al) {
		jMenuEmployeeAdd.addActionListener(al);
	}

	public void setEmployeeSearchListener(ActionListener al) {
		jMenuEmployeeSearch.addActionListener(al);
	}

	/*
	 * public void setAftaleAddListener(ActionListener al) {
	 * jMenuAftaleAdd.addActionListener(al); } public void
	 * setAftaleSearchListener(ActionListener al) {
	 * jMenuAftaleSearch.addActionListener(al); }
	 */
	public void setFarverListener(ActionListener al) {
		jMenuFarver.addActionListener(al);
	}

	public void setHj�lpMenuListener(ActionListener al) {
		jMenuHj�lp.addActionListener(al);
	}

	public void setCalendarListener(PropertyChangeListener pl) {
		jCalendar1.addPropertyChangeListener(pl);
	}

	public void setTableMouseListener(MouseAdapter ma) {
		jTable2.addMouseListener(ma);
	}

	public void setCalendarFromDoctorList(MouseAdapter ma) {
		jList1.addMouseListener(ma);
	}

	public void setPatientInfoFromAftale(MouseAdapter ma) {
		jList2.addMouseListener(ma);
	}

	public void setStartListener(ActionListener al) {
		jButtonStartKonsultation.addActionListener(al);
	}

	public void setSlutListener(ActionListener al) {
		jButtonAfslutKonsultation.addActionListener(al);
	}

	public void setPopupAddAftaleListener(ActionListener listener) {
		jPopupAddTidsreservation.addActionListener(listener);
	}

	public void setPopupDeleteAftaleListener(ActionListener listener) {
		jPopupSletTidsreservation.addActionListener(listener);
	}

	/*
	 * public void setPopupEditAftaleListener(ActionListener listener) {
	 * jPopupEditTidsreservation.addActionListener(listener); }
	 */
	public void setPopupEditPatientListener(ActionListener listener) {
		jPopupEditPatient.addActionListener(listener);
	}

	// set/get operations...
	public void printText(String str) {
		jTextArea2.setText(str);
	}

	public void printTextTogether(String str) {
		jTextArea2.setText(jTextArea2.getText() + "\n" + str);
	}

	public void setData(Object[][] data) {
		// Object[][] data = th.makeCalendarFromLogin();
		jTable2.setModel(new DefaultTableModel(data, kolloner2));
		TableColumn column = null;
		column = jTable2.getColumnModel().getColumn(0);
		column.setPreferredWidth(70);
		column = jTable2.getColumnModel().getColumn(1);
		column.setPreferredWidth(420);
		column.setResizable(false);

	}

	public String TInfo_toString(ITidsreservation t) {
		try {
			String output = "RESERVATION INFO:\n" + "Tidstart:\t"
					+ t.getTidStart() + "\n" + "Emne:\t" + t.getEmne() + "\n";
			return output;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	public String PInfo_toString(IPatient p) {
		try {
			String output = "PATIENT INFO:\n" + "Fornavn:\t" + p.getFornavn()
					+ "\n" + "Efternavn:\t" + p.getEfternavn() + "\n"
					+ "CPR:\t" + p.getCpr() + "\n" + "Adresse:\t"
					+ p.getAdresse() + "\n" + "TLF:\t" + p.getTlf() + "\n"
					+ "Alder:\t" + p.getAlder() + "\n\n";
			return output;
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "";
		}
	}

	/**
	 * @return
	 */
	public Object getSelectedTidsreservation() {
		if (selectedCol > 0) {
			Object key = data[selectedRow][0];
			return key;
		} else {
			return null;
		}
	}

	/**
	 * @param t
	 * @param p
	 * @param v2
	 */

	public void setPatientInfo(ITidsreservation t, IPatient p, Vector v2) {
		try {
			printText(TInfo_toString(t));
			printTextTogether(PInfo_toString(p));
			v.clear();
			v = (Vector) v2.clone();
			Object[] rp = new Object[v.size()];
			for (int i = 0; i < v.size(); i++) {
				ITidsreservation it = (ITidsreservation) v.elementAt(i);
				rp[i] = it.getTidStart();
			}
			jList2.setListData(rp);
			jList2.setVisible(true);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * @return
	 */
	public IEmployee getSelectedDoc() {
		// TODO Auto-generated method stub
		if (jList1.isSelectionEmpty()) {
			return null;
		} else {
			if (selectedList1Element >= 0) {
				return (IEmployee) DocList[selectedList1Element];
			} else {
				return null;
			}
		}
	}

	/**
	 * @return
	 */
	public java.util.Date getCalendarData() {
		// TODO Auto-generated method stub
		return jCalendar1.getDate();
	}

	/**
	 * @param reservation
	 * @param calendar
	 */
	public void updateCalendar(Vector reservation) {
		// TODO Auto-generated method stub
		Object[][] eCalendar = getReservationFromDateAndDoc(reservation);
		if (eCalendar == null) {
			printText("Der er ingen tidsreservationer til:\n"
					+ jList1.getSelectedValue() + "\n" + jCalendar1.getDate());
			jTable2.setVisible(false);
		} else {
			data = (Object[][]) eCalendar.clone();
			jTable2.setModel(new DefaultTableModel(eCalendar, kolloner2));
			TableColumn column = null;
			column = jTable2.getColumnModel().getColumn(0);
			column.setPreferredWidth(70);
			column = jTable2.getColumnModel().getColumn(1);
			column.setPreferredWidth(jTable2.getWidth() - 70);
			jTable2.setVisible(true);
			printText("De har ialt " + th.getHashtable().size()
					+ " patienter idag.");
		}

	}

	public Object[][] getReservationFromDateAndDoc(Vector reservation) {
		try {
			DateFormat df2 = DateFormat.getTimeInstance(DateFormat.SHORT);
			reservations.clear();

			if (reservation.size() == 0) {
				return null;
			} else {
				for (int i = 0; i < reservation.size(); i++) {
					ITidsreservation rv = (ITidsreservation) reservation
							.elementAt(i);
					String tbase = df2.format(rv.getTidStart()); // tid fra
																	// databasen

					for (int j = 0; j < data.length; j++) {
						if (data[j][0].equals(tbase)) {
							reservations.put(data[j][0], rv);
							IPatient p = rv.getPatient();
							String navn = p.getFornavn() + " "
									+ p.getEfternavn();
							data[j][1] = navn;
						}
					}
				}
				return data;
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return null;
	}

	/**
	 * @param calendar
	 */
	public void updateCalendar(Object[][] eCalendar, int numberOfPatients) {
		// TODO Auto-generated method stub
		if (numberOfPatients == -1) {
			data = (Object[][]) eCalendar.clone();
			jTable2.setModel(new DefaultTableModel(eCalendar, kolloner2));
			setTableLayout();
			printText("Der er ingen tidsreservationer til:\n"
					+ jList1.getSelectedValue() + "\n" + jCalendar1.getDate());
		} else {
			data = (Object[][]) eCalendar.clone();
			jTable2.setModel(new DefaultTableModel(eCalendar, kolloner2));
			setTableLayout();
			printText("De har ialt " + numberOfPatients + " patienter idag.");
		}
	}

	public void setTableLayout() {
		TableColumn column = null;
		column = jTable2.getColumnModel().getColumn(0);
		column.setPreferredWidth(70);
		column = jTable2.getColumnModel().getColumn(1);
		column.setPreferredWidth(jTable2.getWidth() - 70);
		jTable2.setVisible(true);
	}

	/**
	 * 
	 */
	public void showOldReservation() {
		// TODO Auto-generated method stub
		try {
			ITidsreservation t = (ITidsreservation) v
					.elementAt(selectedList2Element);
			printText(TInfo_toString(t));
			IPatient p = t.getPatient();
			printTextTogether(PInfo_toString(p));
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void setStartButtonStatus(boolean e) {
		jButtonStartKonsultation.setEnabled(e);
	}

	public boolean getStartButtonStatus() {
		return jButtonStartKonsultation.isEnabled();
	}

	public void setAfslutButtonStatus(boolean e) {
		jButtonAfslutKonsultation.setEnabled(e);
	}

	public boolean getAfslutButtonStatus() {
		return jButtonAfslutKonsultation.isEnabled();
	}

	public void setLabel1Text(String t) {
		jLabel1.setText(t);
	}

	public void setLabel2Text(String t) {
		jLabel2.setText(t);
	}

	public void setLabel2Visible(boolean e) {
		jLabel2.setVisible(e);
	}

	public void setLabel3Text(String t) {
		jLabel3.setText(t);
	}

	public void setLabel3Visible(boolean e) {
		jLabel3.setVisible(e);
	}

	public void setProgressBarValue(int v) {
		jProgressBar1.setValue(v);
	}

	public void setProgressBarMax(int max) {
		jProgressBar1.setMaximum(max);
	}

	public void setProgressBarString(String s) {
		jProgressBar1.setString(s);
	}

	public int getProgressBarMin() {
		return jProgressBar1.getMinimum();
	}

	public int getSelectedRow() {
		return selectedRow;
	}

	public int getSelectedCol() {
		return selectedCol;
	}

	public void setTabelBackgroundColor(Color c) {
		jTable2.setBackground(c);
	}

	public void setTabelForegroundColor(Color c) {
		jTable2.setForeground(c);
	}

	public void setList1BackgroundColor(Color c) {
		jList1.setBackground(c);
	}

	public void setList1ForegroundColor(Color c) {
		jList1.setForeground(c);
	}

	public void setList2BackgroundColor(Color c) {
		jList2.setBackground(c);
	}

	public void setList2ForegroundColor(Color c) {
		jList2.setForeground(c);
	}

	public void setTextAreaForegroundColor(Color c) {
		jTextArea2.setForeground(c);
	}

	public void setTextAreaBackgroundColor(Color c) {
		jTextArea2.setBackground(c);
	}

	public void setMenuBackgroundColor(Color c) {
		MenuElement[] menus = getJMenuBar().getSubElements();
		for (int i = 0; i < menus.length; i++) {
			JMenu menu = (JMenu) menus[i];
			menu.setBackground(c);
			int count = menu.getItemCount();
			for (int j = 0; j < count; j++) {
				menu.getItem(j).setBackground(c);
			}
		}
	}

	public void setMenuForegroundColor(Color c) {
		MenuElement[] menus = getJMenuBar().getSubElements();
		for (int i = 0; i < menus.length; i++) {
			JMenu menu = (JMenu) menus[i];
			menu.setForeground(c);
			int count = menu.getItemCount();
			for (int j = 0; j < count; j++) {
				menu.getItem(j).setForeground(c);
			}
		}
	}

	public Component[] getAllComponents() {
		return getContentPane().getComponents();
	}

	public void setBackgroundColor(JComponent k, Color c) {
		k.setBackground(c);
	}

	public void setForegroundColor(JComponent k, Color c) {
		k.setForeground(c);
		// System.out.println(jScrollPane1.getViewport().getComponents()[0].getName());
	}

	public void clearList2() {
		jList2.setListData(new Vector());
		jList2.repaint();
	}

	public void updateList2(Vector v) {
		jList2.setListData(v);
	}

	public void setPopupEditPatient(boolean e) {
		jPopupEditPatient.setEnabled(e);
	}

	/*
	 * public void setPopupEditTidsreservation(boolean e){
	 * jPopupEditTidsreservation.setEnabled(e); }
	 */
	public void setPopupSletTidsreservation(boolean e) {
		jPopupSletTidsreservation.setEnabled(e);
	}

	public void setPopupAddTidsreservation(boolean e) {
		jPopupAddTidsreservation.setEnabled(e);
	}

	public boolean isAnsatSelectionEmpty() {
		return jList1.isSelectionEmpty();
	}

	/**
	 * @param user
	 */
	public void setSelectedDoc(IEmployee user) {
		try {
			// TODO Auto-generated method stub

			for (int i = 0; i < DocList.length; i++) {
				IEmployee e = (IEmployee) DocList[i];
				if (user.asString().equals(e.asString())) {
					jList1.setSelectedIndex(i);
					break;
				}
			}
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void updateDocs(Object[] doclist) {
		try {
			for (int i = 0; i < doclist.length; i++) {
				IEmployee e = (IEmployee) doclist[i];
				doclist[i] = e.asString();
			}
		} catch (RemoteException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		jList1.setListData(doclist);
	}

	public void updateTable(Object[][] newdata) {
		jTable2.setModel(new DefaultTableModel(newdata, kolloner2));
	}

	public int getTableSelectedRow() {
		return jTable2.getSelectedRow();
	}

	public int getTableSelectedColumn() {
		return jTable2.getSelectedColumn();
	}

	public Object getTableValueAt(int r, int c) {
		return jTable2.getValueAt(r, c);
	}

	public void updateDocList(Object[] dl) {
		DocList = dl;
	}
}
