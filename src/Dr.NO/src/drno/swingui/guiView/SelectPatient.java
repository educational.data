/*
 * temp.java
 *
 * Created on 10. april 2005, 17:21
 */

package drno.swingui.guiView;

import javax.swing.*;
import java.awt.*;

import javax.swing.table.*;
/**
 *
 * @author  Leo
 */
public class SelectPatient extends JFrame {
    
    /** Creates a new instance of temp */
    JTable jTable1 = new JTable();
    //PatientHandler p_handler = new PatientHandler();
    //TableHandler th = new TableHandler();
    //ImageIcon Leo = new ImageIcon("Leo.jpg", "Leo");
    final String[] kolloner1 = {"Navn", "CPR", "Adresse", "TLF", "K�n", "Patient hos"};
    //final Object[][] data1 = th.getTableData();
    //final String[] kolloner2 = {"Tid", "Mandag","Tirsdag","Onsdag","Torsdag","Fredag"};
    //final Object[][] data2 = th.makeCalendar(20);

    //private javax.swing.JTable jTable1;
    Object[][] data1;
   
    private void jbInit() throws Exception{
        jFrame1 = new javax.swing.JFrame();
        MainMenuBar = new javax.swing.JMenuBar();
        AftaleMenu = new javax.swing.JMenu();
        ATilf�j = new javax.swing.JMenuItem();
        ARet = new javax.swing.JMenuItem();
        ASlet = new javax.swing.JMenuItem();
        AS�g = new javax.swing.JMenuItem();
        PatienterMenu = new javax.swing.JMenu();
        PTilf�j = new javax.swing.JMenuItem();
        PRet = new javax.swing.JMenuItem();
        PSlet = new javax.swing.JMenuItem();
        PS�g = new javax.swing.JMenuItem();
        AnsatteMenu = new javax.swing.JMenu();
        ETilf�j = new javax.swing.JMenuItem();
        ERet = new javax.swing.JMenuItem();
        ESlet = new javax.swing.JMenuItem();
        IndstillingerMenu = new javax.swing.JMenu();
        Dagskalender = new javax.swing.JMenuItem();
        Ugekalender = new javax.swing.JMenuItem();
        
        JScrollPane jScrollPane1 = new JScrollPane();
        jTable1.setModel(new DefaultTableModel(data1, kolloner1));
        jFrame1.getContentPane().add(jScrollPane1, BorderLayout.CENTER);
        jTable1.setVisible(true);
        
        jScrollPane1.getViewport().add(jTable1,null);
        jFrame1.getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        

        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        

        

        AftaleMenu.setText("Aftale");
        

        ATilf�j.setText("Tilf\u00f8j");
/*        ATilf�j.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ATilf�jActionPerformed(evt);
            }
        });*/
        

        AftaleMenu.add(ATilf�j);

        ARet.setText("Ret");
        

        AftaleMenu.add(ARet);

        ASlet.setText("Slet");
        AftaleMenu.add(ASlet);

        AS�g.setText("S\u00f8g");
        AftaleMenu.add(AS�g);

        MainMenuBar.add(AftaleMenu);

        PatienterMenu.setText("Patienter");
        PTilf�j.setText("Tilf\u00f8j");
        PatienterMenu.add(PTilf�j);

        PRet.setText("Ret");
        PatienterMenu.add(PRet);

        PSlet.setText("Slet");
        PatienterMenu.add(PSlet);

        PS�g.setText("S\u00f8g");
/*       PS�g.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PS�gActionPerformed(evt);
            }
        });*/

        PatienterMenu.add(PS�g);

        MainMenuBar.add(PatienterMenu);

        AnsatteMenu.setText("Ansatte");
        ETilf�j.setText("Tilf\u00f8j");
        AnsatteMenu.add(ETilf�j);

        ERet.setText("Ret");
        AnsatteMenu.add(ERet);

        ESlet.setText("Slet");
        AnsatteMenu.add(ESlet);

        MainMenuBar.add(AnsatteMenu);

        IndstillingerMenu.setText("Indstillinger");
        Dagskalender.setText("Dagskalender");
        IndstillingerMenu.add(Dagskalender);

        Ugekalender.setText("Ugekalender");
        IndstillingerMenu.add(Ugekalender);

        MainMenuBar.add(IndstillingerMenu);

        setJMenuBar(MainMenuBar);                        
        pack(); 
        
        Dimension dim = getToolkit().getScreenSize();
        Rectangle rec = getBounds();
        int x = dim.width/2-rec.width/2;
        int y = dim.height/2-rec.height/2;
        setLocation(x,y);
        /*addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                pressed(evt);
            }
        });*/
        //jTable1.setModel(new DatamodelFraHjaelpeklasse());
    }
/*     private void PS�gActionPerformed(java.awt.event.ActionEvent evt) {                                     
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new S�gPatient().setVisible(true);
            }
        });
    }                                    

    private void ATilf�jActionPerformed(java.awt.event.ActionEvent evt) {                                        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Tilf�jAftale().setVisible(true);
            }
        });
    }          */
    /** Creates a new instance of DatabaseGUI */
    public SelectPatient() {
        try{
            jbInit();
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    private void pressed(java.awt.event.MouseEvent evt) {
        jTable1.setVisible(true);
    }
    //public ImageIcon Leo = new ImageIcon("Leo.jpeg", "LeoB");
    //public ImageIcon Tob = new ImageIcon("Tob.jpeg", "TobB");
    //private PatientHandler p_handler = new PatientHandler();
    //private TableHandler th = new TableHandler();
    private javax.swing.JMenuItem ARet;
    private javax.swing.JMenuItem ASlet;
    private javax.swing.JMenuItem AS�g;
    private javax.swing.JMenuItem ATilf�j;
    private javax.swing.JMenu AftaleMenu;
    private javax.swing.JMenu AnsatteMenu;
    private javax.swing.JMenuItem Dagskalender;
    private javax.swing.JMenuItem ERet;
    private javax.swing.JMenuItem ESlet;
    private javax.swing.JMenuItem ETilf�j;
    private javax.swing.JMenu IndstillingerMenu;
    private javax.swing.JMenuBar MainMenuBar;
    private javax.swing.JMenuItem PRet;
    private javax.swing.JMenuItem PSlet;
    private javax.swing.JMenuItem PS�g;
    private javax.swing.JMenuItem PTilf�j;
    private javax.swing.JMenu PatienterMenu;
    private javax.swing.JMenuItem Ugekalender;
    private javax.swing.JFrame jFrame1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPasswordField jPasswordField1;
    
    public void setData(Object[][] data)
    {
    	data1 = data;
    }
}
