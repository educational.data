/*
 * Created on Apr 26, 2005
 */
package drno.tests;

import drno.exception.ConsistanceException;
import drno.interfaces.IPatient;
import drno.server.model.Patient;
import junit.framework.TestCase;

/**
 * @author tobibobi
 */
public class PatientTest extends TestCase {

	private Patient pat;
	public PatientTest() {
		pat = new Patient();
	}

	public void testSetAdresse() {
			try {
				pat.setAdresse("");
				fail("zero length should throw drno.exception");
			} catch (ConsistanceException e) {
				// if it throws it was correct.
			}
			try {
				String testval = "Lucernemarken 12";
				pat.setAdresse(testval);
				assertEquals(pat.getAdresse(),testval);
			} catch (ConsistanceException e) {
				fail(e.toString());
			}
	}

	public void testSetCpr() {
		try {
			pat.setCpr("");
			fail("zero length should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "112233-1122";
			pat.setCpr(testval);
			assertEquals(pat.getCpr(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public void testSetDoctor() {
		try {
			pat.setDoctor(null);
			fail("Man m� ikke kunne s�tte en tom doktor ind i systemet.");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
	}

	public void testSetEfternavn() {
		try {
			pat.setEfternavn("");
			fail("zero length should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "Hansen";
			pat.setEfternavn(testval);
			assertEquals(pat.getEfternavn(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public void testSetFornavn() {
		try {
			pat.setFornavn("");
			fail("zero length should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "Hansen";
			pat.setFornavn(testval);
			assertEquals(pat.getFornavn(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public void testSetSex() {
		try {
			pat.setSex(1000);
			fail("zero length should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			int testval = IPatient.SEX_MALE;
			pat.setSex(testval);
			assertEquals(pat.getSex(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public void testSetTlf() {
		try {
			pat.setTlf("1000");
			fail("wrong format should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "1212 12 12";
			pat.setTlf(testval);
			assertEquals(pat.getTlf(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public void testSetPostnummer() {
		try {
			pat.setPostnummer(new Integer(0));
			fail("wrong format should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			Integer testval = new Integer(5000);
			pat.setPostnummer(testval);
			assertEquals(pat.getPostnummer(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

}
