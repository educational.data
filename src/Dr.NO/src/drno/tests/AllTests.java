/*
 * Oprettet Apr 26, 2005 af tobibobi
 */
package drno.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * Unit test suite beregnet til at teste alle de interne funktioner i systemet.
 * @author tobibobi
 */
public class AllTests {

	public static void main(String[] args) {
		junit.swingui.TestRunner.run(AllTests.class);	
	}

	public static Test suite() {
		TestSuite suite = new TestSuite("Test for drno.tests");
		//$JUnit-BEGIN$
		suite.addTestSuite(RegressionTest.class);
		suite.addTestSuite(PatientTest.class);
		suite.addTestSuite(EmployeeTest.class);
		//$JUnit-END$
		return suite;
	}
}
