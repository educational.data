/*
 * Oprettet 2005-05-12 af tobibobi 
 */
package drno.tests;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Timestamp;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.IEmployeeEditable;
import drno.interfaces.IPatient;
import drno.interfaces.IPatientEditable;
import drno.interfaces.ITidsreservation;
import drno.interfaces.ITidsreservationEditable;
import drno.interfaces.LoginHandler;
import drno.interfaces.PatientHandler;
import drno.interfaces.TidsreservationsHandler;
import drno.server.Server;


import junit.framework.TestCase;

/**
 * @author tobibobi
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class RegressionTest extends TestCase {
	private EmployeeHandler employeeHandler;

	private PatientHandler patientHandler;

	private LoginHandler loginHandler;

	private TidsreservationsHandler tidsreservationsHandler;

	/**
	 * @param fornavn
	 * @param login
	 * @param pw
	 * @param efternavn
	 * @param tlf
	 * @return
	 * @throws RemoteException
	 * @throws SecurityDisallowedException
	 * @throws ConsistanceException
	 * @throws UnkErrException
	 * @throws ObjectLockException
	 */
	private IEmployee createEmployee(String fornavn, String login, String pw,
			String efternavn, String tlf) throws RemoteException,
			SecurityDisallowedException, ConsistanceException, UnkErrException,
			ObjectLockException {
		IEmployeeEditable employee = employeeHandler.createNewEmployee();
		employee.setFornavn(fornavn);
		employee.setEfternavn(efternavn);
		employee.setLogin(login);
		employee.setPassword(pw);
		employee.setTelefonNummer(tlf);
		employee.setTidsOpdeling(10);
		employee.setType(IEmployee.TYPE_DOCTOR);
		IEmployee empl = employeeHandler.saveEmployee(employee);
		return empl;
	}

	/**
	 * @param fornavn
	 * @param efternavn
	 * @param adresse
	 * @param postnr
	 * @param cpr
	 * @param sex
	 * @param tlf
	 * @return
	 * @throws RemoteException
	 * @throws ConsistanceException
	 * @throws ObjectLockException
	 * @throws UnkErrException
	 */
	private IPatient createPatient(String fornavn, String efternavn,
			String adresse, int postnr, String cpr, int sex, String tlf)
			throws RemoteException, ConsistanceException, ObjectLockException,
			UnkErrException {
		IPatientEditable pat = patientHandler.createNewPatient();
		pat.setFornavn(fornavn);
		pat.setEfternavn(efternavn);
		pat.setAdresse(adresse);
		pat.setPostnummer(new Integer(postnr));
		pat.setCpr(cpr);
		pat.setSex(sex);
		pat.setDoctor(loginHandler.getUser());
		pat.setTlf(tlf);
		IPatient pat2 = patientHandler.savePatient(pat);
		return pat2;
	}

	/**
	 * @param pat2
	 * @param empl
	 * @param start
	 * @param slut
	 * @param emne
	 * @return
	 * @throws RemoteException
	 * @throws ConsistanceException
	 * @throws ObjectLockException
	 * @throws UnkErrException
	 */
	private ITidsreservation createTidsReservation(IPatient pat2,
			IEmployee empl, Timestamp start, Timestamp slut, String emne)
			throws RemoteException, ConsistanceException, ObjectLockException,
			UnkErrException {
		ITidsreservationEditable tidsr = tidsreservationsHandler.createNewTidsReservation();
		tidsr.setEmployee(empl);
		tidsr.setEmne(emne);
		tidsr.setPatient(pat2);
		tidsr.setTidStart(start);
		tidsr.setTidSlut(slut);
		tidsr.setType(ITidsreservation.TYPE_CONSULTATION);
		ITidsreservation reservation = tidsreservationsHandler.saveTidsreservation(tidsr);
		return reservation;
	}

	/**
	 * @param empl
	 * @throws ObjectLockException
	 * @throws RemoteException
	 * @throws SecurityDisallowedException
	 * @throws UnkErrException
	 */
	private void deleteEmployee(IEmployee empl) throws ObjectLockException,
			RemoteException, SecurityDisallowedException, UnkErrException {
		employeeHandler.deleteEmployee(employeeHandler.editEmployee(empl));
	}

	/**
	 * @param pat2
	 * @throws ObjectLockException
	 * @throws UnkErrException
	 * @throws RemoteException
	 */
	private void deletePatient(IPatient pat2) throws ObjectLockException,
			UnkErrException, RemoteException {
		patientHandler.deletePatient(patientHandler.editPatient(pat2));
	}

	/**
	 * @param reservation
	 * @throws ConsistanceException
	 * @throws ObjectLockException
	 * @throws UnkErrException
	 * @throws RemoteException
	 */
	private void deleteTidsReservation(ITidsreservation reservation)
			throws ConsistanceException, ObjectLockException, UnkErrException,
			RemoteException {
		tidsreservationsHandler.deleteTidsreservation(tidsreservationsHandler
				.editTidsreservation(reservation));
	}

	public void setUp() {
		try {

			Server.instance();
			Registry reg = LocateRegistry.getRegistry("localhost");
			loginHandler = ((LoginHandler) reg.lookup("DrNOServer")).login(
					"admin", "admin");
			patientHandler = loginHandler.getPatientHandler();
			employeeHandler = loginHandler.getEmployeeHandler();
			tidsreservationsHandler = loginHandler.getTidsreservationsHandler();

		} catch (Exception ex) {
			ex.printStackTrace();
			fail();
		}
	}

	public void tearDown() {
	}

	public final void testCreateAndDeleteEmployee() {
		try {
			String fornavn = "Albertawdgwaref";
			String login = "DummyLogIn";
			String pw = "DummyPassWord";
			String efternavn = "Einstein";
			String tlf = "12345678";
			IEmployee empl = createEmployee(fornavn, login, pw, efternavn, tlf);

			Vector eVec = employeeHandler.searchEmployeesByName(fornavn);
			assertEquals("Der er for mange employees", 1, eVec.size());
			assertEquals(fornavn, ((IEmployee) eVec.get(0)).getFornavn());

			deleteEmployee(empl);

			eVec = employeeHandler.searchEmployeesByName(fornavn);
			assertEquals("Der er for mange employees", 0, eVec.size());
		} catch (RemoteException e) {

			e.printStackTrace();
		} catch (SecurityDisallowedException e) {

			e.printStackTrace();
		} catch (ConsistanceException e) {

			e.printStackTrace();
		} catch (UnkErrException e) {

			e.printStackTrace();
		} catch (ObjectLockException e) {

			e.printStackTrace();
		}

	}

	public final void testCreateAndDeletePatient() throws Exception {
		try {

			String fornavn = "Albert";
			String efternavn = "Åberg";
			String adresse = "Lansgade";
			int postnr = 1000;
			String cpr = "122456-1233";
			int sex = IPatient.SEX_MALE;
			String tlf = "12345678";
			IPatient pat2 = createPatient(fornavn, efternavn, adresse, postnr,
					cpr, sex, tlf);

			Vector vPat = patientHandler.searchPatientsByCPR(cpr);
			assertEquals("Patienten er ikke fundet", 1, vPat.size());
			IPatient p = (IPatient) vPat.get(0);

			assertEquals("Fornavn er ikke ens", fornavn, p.getFornavn());
			assertEquals("Efternavn er ikke ens", efternavn, p.getEfternavn());

			deletePatient(pat2);
		} catch (RemoteException e) {
			e.printStackTrace();
			fail();
		} catch (ConsistanceException e) {
			e.printStackTrace();
			fail();
		} catch (ObjectLockException e) {
			e.printStackTrace();
			fail();
		} catch (UnkErrException e) {
			e.printStackTrace();
			fail();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public final void testCreateAndDeleteTidsreservation() {
		try {

			IPatient pat2 = createPatient("A", "B", "C", 1000, "123456-4321",
					IPatient.SEX_MALE, "12345678");
			IEmployee empl = createEmployee("A", "Berta", "QWERTY", "C", "12345678");

			Timestamp start = Timestamp.valueOf("2004-1-1 12:00:00");
			Timestamp slut = Timestamp.valueOf("2004-1-1 12:10:00");
			String emne = "Rektalundersøgelse med tilhørende højtryks puling";

			ITidsreservation reservation = createTidsReservation(pat2, empl,
					start, slut, emne);
			
			Vector v = tidsreservationsHandler.getFromDate(start);
			
			assertEquals("Der er flere reservationer den enkelte dag",1,v.size());

			deleteTidsReservation(reservation);

			deleteEmployee(empl);
			deletePatient(pat2);
		} catch (RemoteException re) {
			re.printStackTrace();
			fail();
		} catch (ConsistanceException e) {
			e.printStackTrace();
			fail();
		} catch (ObjectLockException e) {
			e.printStackTrace();
			fail();
		} catch (UnkErrException e) {
			e.printStackTrace();
			fail();
		} catch (SecurityDisallowedException e) {
			e.printStackTrace();
			fail();
		}
	}
}
