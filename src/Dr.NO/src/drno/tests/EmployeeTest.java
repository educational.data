/*
 * Oprettet Apr 26, 2005 af tobibobi
 */
package drno.tests;

import drno.exception.ConsistanceException;
import drno.interfaces.IEmployee;
import drno.server.model.EmployeeImpl;
import junit.framework.TestCase;

/**
 * @author tobibobi
 */
public class EmployeeTest extends TestCase {

	private EmployeeImpl emp;
	/**
	 * Constructor for EmployeeTest.
	 * @param arg0
	 */
	public EmployeeTest(String arg0) {
		super(arg0);
		emp = new EmployeeImpl();
	}

	public final void testSetEfternavn() {
		try {
			emp.setEfternavn("");
			fail("zero length should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "Hansen";
			emp.setEfternavn(testval);
			assertEquals(emp.getEfternavn(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public final void testSetFornavn() {
		try {
			emp.setFornavn("");
			fail("zero length should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "Hansen";
			emp.setFornavn(testval);
			assertEquals(emp.getFornavn(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public final void testSetLogin() {
		try {
			emp.setLogin("");
			fail("zero length should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "Hansen";
			emp.setLogin(testval);
			assertEquals(emp.getLogin(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public final void testSetPassword() {
		try {
			String phrase = "Alloal";
			emp.setPassword(phrase);
			assertFalse("The passphrase tester accepts true even if the object is in the database",emp.verify(phrase));
			
			
		} catch(ConsistanceException e) {
			fail(e.toString());
		}
		
	}

	public final void testSetTelefonNummer() {
		try {
			emp.setTelefonNummer("1000");
			fail("wrong format should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			String testval = "1212 12 12";
			emp.setTelefonNummer(testval);
			assertEquals(emp.getTelefonNummer(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public final void testSetTidsOpdeling() {
		try {
			emp.setTidsOpdeling(0);
			fail("wrong format should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			int testval = 10;
			emp.setTidsOpdeling(testval);
			assertEquals(emp.getTidsOpdeling(),testval);
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
	}

	public final void testSetType() {
		try {
			emp.setType(1000);
			fail("wrong format should throw drno.exception");
		} catch (ConsistanceException e) {
			// if it throws it was correct.
		}
		try {
			int type = IEmployee.TYPE_DOCTOR;
			emp.setType(type);
			assertEquals(emp.getType(),type);
			
			type = IEmployee.TYPE_NURSE;
			emp.setType(type);
			assertEquals(emp.getType(),type);
			
			type = IEmployee.TYPE_SECRETARY;
			emp.setType(type);
			assertEquals(emp.getType(),type);			
		} catch (ConsistanceException e) {
			fail(e.toString());
		}
		//TODO Implement setType().
	}
}
