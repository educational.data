package drno;


/**
 * The first version of a user interface.
 * 
 * @author bohansen
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Vector;

import javax.swing.UIManager;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.IEmployeeEditable;
import drno.interfaces.IPatient;
import drno.interfaces.IPatientEditable;
import drno.interfaces.ITidsreservation;
import drno.interfaces.ITidsreservationEditable;
import drno.interfaces.LoginHandler;
import drno.interfaces.PatientHandler;
import drno.interfaces.TidsreservationsHandler;
import drno.server.event.Event;
import drno.server.event.Signal;
import drno.support.Logger;


public class TextUIClient {
	
	public static void main(String[] args) throws Exception{
		
		(new TextUIClient()).run();
	}
       

	private InputStreamReader isr = new InputStreamReader( System.in );

	private EmployeeHandler e_handler;
	private LoginHandler rmt_login;
	private TidsreservationsHandler t_handler;
	private PatientHandler p_handler;
	private BufferedReader stdin = new BufferedReader( isr );
	
	public void performEvent() {
		try {
			Signal sig = rmt_login.getOnUpdate();
			Event event = new Event() {
                public void onAction(Object msg) {
					System.out.println("test");
				}
			};
			
			sig.addListener(event);
			rmt_login.fireUpdate();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

    public void opretAnsat() throws Exception
    {
        println("Indtast ansat oplysninger");
        
    
        try {
			IEmployeeEditable e = e_handler.createNewEmployee();
			
			print("Fornavn: ");
			String input = stdin.readLine();
			e.setFornavn(input);
			
			print("Efternavn: ");
			input = stdin.readLine();
			e.setEfternavn(input);
			
			print("Login: ");
			input = stdin.readLine();
			e.setLogin(input);

			print("Password: ");
			input = stdin.readLine();
			e.setPassword(input);
			
			print("Telefon nummer: ");
			input = stdin.readLine();
			e.setTelefonNummer(input);  
			
			print("Tidsopdeling (tal > 0) ");
			input = stdin.readLine();
			e.setTidsOpdeling(Integer.parseInt(input));
			
			print("Type (0:Administrator 1:Doktor 2:Sekret�r 3:Sygeplejerske)");
			input = stdin.readLine();
			e.setType(Integer.parseInt(input));    
			
			e_handler.saveEmployee(e);
			println(" * Ansat oprettet og gemt i database *");println("");
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (SecurityDisallowedException e) {
			println(" ! Fejl: Du har ikke rettigheder til at oprette en bruger (Besked: " + e.getMessage() + ")");
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ConsistanceException e) {
			println(" ! Fejl: Du har tastet forkert (Besked: " + e.getMessage() + ")");
		} catch (ObjectLockException e) {
			e.printStackTrace();
		}        
        
    }   

	public void opretPatient() throws Exception
	{
		println("Indtast patient oplysninger");
		
        try{
			IPatientEditable p = p_handler.createNewPatient();
			
			print("Fornavn: ");
			String input = stdin.readLine();
			p.setFornavn(input);
			
			print("Efternavn: ");
			input = stdin.readLine();
			p.setEfternavn(input);
			
			print("CPR: ");
			input = stdin.readLine();
			p.setCpr(input);

			print("Sex: (Mand/Kvinde)");
			input = stdin.readLine();
			int sex = -1;
			if(input.equals("Mand")) sex = IPatient.SEX_MALE;
			if(input.equals("Kvinde")) sex = IPatient.SEX_FEMALE;
			
			p.setSex(sex);
			
			print("Telefon nr: ");
			input = stdin.readLine();
			p.setTlf(input);
			
			print("Adresse: ");
			input = stdin.readLine();
			p.setAdresse(input);
			
			visL�ger();

			print("V�lg l�ge: ");
            input = stdin.readLine();
            Integer eId = new Integer(Integer.parseInt(input));
            IEmployee e = (IEmployee)vAnsatte.get(eId.intValue()-1);
            
			p.setDoctor(e);
			
			p_handler.savePatient(p);
			println(" * Patient oprettet og gemt i database *");println("");		
		
		}catch(Exception e){
			println("kunne ikke oprette patient ");
			e.printStackTrace();
			e.printStackTrace();
		}			
	}	
        
    public void opretTid() {
       // IPatientEditable p = p_handler.createNewPatient();
        println("*** OPRET TIDSRESERVATION ***");
        ITidsreservationEditable t;
        try {
            t = t_handler.createNewTidsReservation();
        }
        catch (RemoteException e1) {
            e1.printStackTrace();
            return;
        }
        Vector v = null;
		try {
			v = p_handler.getAllPatients();
		} catch (UnkErrException ex) {
			System.out.println(ex.getMessage());
			return;
		}
        catch (RemoteException e) {
            e.printStackTrace();
        }
        
		if(v != null && v.size()>0)
        {
			try {
				visPatientListe();
				print("V�lg patient: ");
				String input = stdin.readLine();
				Integer pId = new Integer(Integer.parseInt(input));
				IPatient pat = (IPatient)vPatienter.get(pId.intValue()-1);
				if(pat == null)
				{
					println(" /!\\ Det angivne nummer er ikke gyldigt! /!\\ ");
				}
				else
				{
					t.setPatient(pat);
					
					println("L�ger:");
					visL�ger();
					
					print("V�lg l�ge: ");
					input = stdin.readLine();
					Integer eId = new Integer(Integer.parseInt(input));
					IEmployee e = (IEmployee)vAnsatte.get(eId.intValue()-1);
					t.setEmployee(e);
					
					print("Tidstart: (yyyy-mm-dd hh:mm:ss) ");
					input = stdin.readLine();
					Timestamp ts= Timestamp.valueOf(input);
					t.setTidStart(ts);
					print("Tidslut: (yyyy-mm-dd hh:mm:ss) ");
					input = stdin.readLine();
					ts= Timestamp.valueOf(input);
					t.setTidSlut(ts);
					print("Emne:");
					input = stdin.readLine();
					t.setEmne(input);
					t.setType(ITidsreservation.TYPE_CONSULTATION);
					
					t_handler.saveTidsreservation(t);    // Opret tidsreservation
					println("");
				}
			} catch (NumberFormatException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ObjectLockException e) {
				e.printStackTrace();
			} catch (ConsistanceException e) {
				println(" ! Fejl: Kan ikke oprette tidsreservation. (Meddelse: " + e.getMessage() + ")");
			} catch (UnkErrException e) {
				System.out.println(e.getMessage());
			}
        }
        else
        {
            println(" >>> Ingen patienter i databasen <<<"); 
        }
    }

	public void print(String str) {
        System.out.print(str);
    }
    public void println(String str) {
        System.out.println(str);
    }	
	
	public void promptMenu()
    {
        println("");
        println("Dr.NO SYSTEM MENU v2.1932");
        println("--------------------------");
		println("   0.  Vis menu\n");
		println(" Patienter");
		println("   1. Opret patient");
		println("   2. Slet patient");
		println("   3. Vis patientliste");
        println("   4. S�g efter patient\n");
		println(" Tidsreservation");
		println("   5. Opret tidsreservation");
		println("   6. Slet tidsreservation");
		println("   7. Vis tidsreservationer\n");
		println(" Ansatte");
		println("   8. Opret ansat");
		println("   9. Slet ansat");
		println("  10. Vis liste med ansatte");
        println("  11. S�g efter ansat\n");
        println("  12. Exit\n");
        println(" 200. Opret dummy data\n");
		

    }
	//IEmployee e;
	public void run() throws Exception{
		
		println("Velkommen til Dr.NO systemet:");
		print(" brugernavn > ");
		String username = stdin.readLine();
		print(" password > ");
		String password = stdin.readLine();

		try {
		    Registry reg = LocateRegistry.getRegistry("localhost");
			rmt_login = ((LoginHandler)reg.lookup("DrNOServer")).login(username,password);;
		} catch(RemoteException ex) {
		    System.err.println("Det virker som om at serveren ikke er startet.");
		    System.err.println("Programmet termineres derfor nu.");
		    Logger.log(ex);
		    System.exit(1);
		}
		catch(Exception e) {e.printStackTrace();}
		/* println("test:");
		println(rmt_login.getPassword());
		println("tester p handler:");*/
		p_handler = rmt_login.getPatientHandler();
		e_handler = rmt_login.getEmployeeHandler();
		t_handler = rmt_login.getTidsreservationsHandler();
		

        boolean exit = false;

		int valg;
		promptMenu();
		while(!exit)
		{		
			 try	{
				print("$ ");
				String input = stdin.readLine();
				if(input.length() == 0) {
					valg = 0;
				} else {
	                try{
	                    valg = Integer.parseInt(input);
	                }catch(Exception e){                    
	                    valg = -1; //Tvinger besked "forkert valg"
	                }            
				}
                
				switch(valg)
				{
					case 0:
						promptMenu();
						break;
					case 1:
						opretPatient();						
						break;
					case 2:
						sletPatient();
						break;
					case 3:
                        visPatientListe();
                        break;
                    case 4:
                        seekPatient();
                        break;
					case 5:
                        opretTid();
                        break;
					case 6:
                        sletTidsReservation();
                        break;
					case 7:
						visTidsReservation();
						break;
					case 8:
                        opretAnsat();
                        break;
					case 9:
                        sletAnsat();
                        break;
					case 10:
                        visAnsatte();
                        break;
                    case 11:
                        seekAnsat();
                        break;                                                
					case 12:
						println("DrNO signing off! Ha en go dag :)");
						exit = true;
						break;
					case 100:
					    System.gc();
					    break;
					case 200:
						opretDummyData();
						break;
					case 300:
						performEvent();
						break;
					default:						
						println(" ! Fejl: Forkert valg! /!\\");
				}		
			}
            catch(Exception e)
            {
                Logger.log(e);
                println(e.getMessage());
                e.printStackTrace();
                
            }
           /* catch(Exception e)
            {               
                println("Forkert valg!");   
                
            }*/ 
		}		
	}
    /*
	/**
	 * Inds�tter en masse nyt dummy data:
	 */
	void opretDummyData() throws RemoteException {
		try {
		    IEmployee lastEmployee = null;
		    IPatient lastPatient = null;
		    
			for(int i = 0;i<10;i++) {
				IEmployeeEditable emp = e_handler.createNewEmployee();
				emp.setFornavn("Arne" + i);
				emp.setEfternavn("Hansen" + i);
				emp.setLogin("arne"+i);
				emp.setPassword("000000" + new Integer(i));
				emp.setTelefonNummer("1234567"+i);
				emp.setTidsOpdeling(i+1);
				emp.setType(1 + i % 3);
				lastEmployee = e_handler.saveEmployee(emp);
			}
			
			for(int i = 0;i<10;i++) {
				IPatientEditable pat = p_handler.createNewPatient();
				pat.setFornavn("Tove"+i);
				pat.setEfternavn("Larsen"+i);
				pat.setAdresse("Fuskestr�de " + i);
				pat.setDoctor(lastEmployee);
				pat.setPostnummer(new Integer(1000 + i));
				pat.setCpr("000000-000"+i);
				pat.setSex(IPatient.SEX_FEMALE);
				pat.setTlf("0000000" + i);
				lastPatient = p_handler.savePatient(pat);
			}
			
			GregorianCalendar cal = new GregorianCalendar();
			cal.set(Calendar.MINUTE,0);
			cal.set(Calendar.HOUR,8);
			for(int i=0;i<10;i++) {
			    ITidsreservationEditable tid = t_handler.createNewTidsReservation();
			    tid.setPatient(lastPatient);
			    tid.setEmployee(lastEmployee);
			    tid.setTidStart(new Timestamp(cal.getTimeInMillis()));
			    cal.add(Calendar.MINUTE,lastEmployee.getTidsOpdeling());
			    tid.setTidSlut(new Timestamp(cal.getTimeInMillis()));
			    t_handler.saveTidsreservation(tid);
			}
			
			System.out.println("Oprettet dummy data.");
		} catch(SecurityDisallowedException ex) {
			System.out.println("! Fejl: Kunne ikke oprette alle disse dummy data (" + ex.getMessage() + ")");
		} catch(ConsistanceException ex) {
			System.out.println("! Fejl: Kunne ikke oprette alle disse dummy data (" + ex.getMessage() + ")");
		} catch(ObjectLockException ex) {
			System.out.println("! Fejl: Kunne ikke oprette alle disse dummy data (" + ex.getMessage() + ")");
		} catch (UnkErrException e) {
			System.out.println(e.getMessage());
		}
		
	}

	public void seekAnsat()
    {
        print("Indtast navn der skal s�ges p�: ");
        try
        {
            String input = stdin.readLine();
            Vector ep = e_handler.searchEmployeesByName(input);
            if(ep.size()>0)
            {
            	vAnsatte = ep;
                println(" ****** S�gningen fandt f�lgende ansatte i databasen *******");
                Iterator it = ep.iterator();
                int vit = 1;
                while(it.hasNext()) {
                    IEmployee i = (IEmployee)it.next();
                    print(vit++ + ": ");
                    println(i.asString());
                }
            }
            else
            {
                println(" >>> Ingen ansatte af dette navn i databasen <<<");                
            }
        }catch(Exception e){};
        
    }    
	
    public void seekPatient()
    {
        print("Indtast navn eller CPR der skal s�ges p�: ");
        try
        {
            String input = stdin.readLine();
            Vector ip = p_handler.searchPatientsByName(input);
            Vector ip2 = p_handler.searchPatientsByCPR(input);
            
            if((ip.size() != 0) && (ip2.size() != 0))
            {
                println(" ***** S�gningen fandt f�lgende patienter i databasen ******");
                Iterator it = ip.iterator();
                int vit = 1;
                while(it.hasNext()) {
                    IPatient i = (IPatient)it.next();
                    print(vit++ + ": ");
                    vPatienter.add(i);
                    println(i.asString());
                }
			
                it = ip2.iterator();
                while(it.hasNext()) {
                    IPatient j = (IPatient)it.next();
                    vPatienter.add(j);
                    print(vit++ + ": ");
                    println(j.asString());
                }
            
            }
            else
            {
                println(" >>> Ingen patienter af dette navn i databasen <<<");                
            }
        }catch(Exception e){};
        
    }
    
    public void sletAnsat()
    {
        String input;           
        Integer input_int;
        try {
            visAnsatte();
        
            if(vAnsatte.size()>0)
			{    
			    print("V�lg ansat der skal slettes (0 - afslut): ");
			    input = stdin.readLine();
			    while(!input.equals("0"))
			    {                 
			        input_int = new Integer(Integer.parseInt(input));
			        IEmployee emp = (IEmployee)vAnsatte.get(input_int.intValue()-1);
			        if(emp == null)
			        {
			            println("! Fejl: Det angivne nummer er ikke gyldigt. ");
			        }
			        else
			        {
			            String navn = emp.getFornavn() + " " +
			            emp.getEfternavn();
			            IEmployeeEditable empe = e_handler.editEmployee(emp);
			            e_handler.deleteEmployee(empe);
			            println(" * " +navn + " er slettet fra databasen * ");
			        }
			                        
			        print("V�lg ansat der skal slettes (0 - afslut) $ ");
			        input = stdin.readLine();               
			    }
			    println("");
			}
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ObjectLockException e) {
			println(" ! Fejl: kunne ikke slette bruger da objektet var l�st (besked: " + e.getMessage() +")");
		} catch (SecurityDisallowedException e) {
			println(" ! Fejl: du har ikke rettigheder til at slette bruger (besked: " + e.getMessage() +")");
		} catch (UnkErrException e) {
			System.out.println(e.getMessage());
		}
        
    }
    
    public void sletPatient()
    {
        String input;           
        Integer input_int;
        visPatientListe();
        try{
            if(vPatienter != null && vPatienter.size()>0)
            {    
                print("V�lg patient der skal slettes (0 - afslut) $ ");
                input = stdin.readLine();
                while(!input.equals("0"))
                {                 
                    input_int = new Integer(Integer.parseInt(input));
                    IPatient pat = (IPatient)vPatienter.get(input_int.intValue()-1);
                    if(pat == null)
                    {
                        println("! Fejl: Det angivne nummer er ikke gyldigt!");
                    }
                    else
                    {
                        String navn = pat.getFornavn() + " " +
                        pat.getEfternavn();
                        IPatientEditable pate = p_handler.editPatient(pat);
                        p_handler.deletePatient(pate);
                        println(" * " +navn + " er slettet fra databasen * ");
                    }
                                    
                    print("V�lg patient der skal slettes (0 - afslut) $ ");
                    input = stdin.readLine();               
                }
                println("");
            } 
        } catch(IOException e) {
        	e.printStackTrace();
        } catch(ObjectLockException e){
        	System.out.println("! Fejl: kunne ikke slette bruger (Besked: " + e.getMessage() + ")");
        } catch (UnkErrException e) {
			System.out.println(e.getMessage());
		}
    }
	public void sletTidsReservation() throws Exception
    {
        Integer input_int;
        println("Tidsreservationer:");
        visTidsReservation();
        try{
            if(t_handler.getAllTidsreservation().size()>0)
            {    
                print("V�lg tidsreservation der skal slettes (0 - afslut) $ ");
                String input = stdin.readLine();
                while(!input.equals("0"))
                {                 
                    input_int = new Integer(Integer.parseInt(input));
                    ITidsreservation res = (ITidsreservation)vTidsreservationer.get(input_int.intValue()-1);
                    if (res == null)
                    {
                        println("! Fejl: Det angivne nummer er ikke gyldigt!");
                    }
                    else
                    {
                    	ITidsreservationEditable rese = t_handler.editTidsreservation(res);
                        t_handler.deleteTidsreservation(rese);
                        println(input_int + " er slettet fra databasen * ");
                    }
                                    
                    print("V�lg tidsreservation der skal slettes (0 - afslut) $ ");
                    input = stdin.readLine();               
                }
                println("");
            } 
        }catch(ObjectLockException e){
        	System.out.println("! Fejl: kunne ikke slette bruger (Besked: " + e.getMessage() + ")");
        }catch(Exception e) {
        	e.printStackTrace();
        }
    }
    
	private Vector vAnsatte = new Vector();
	public void visAnsatte() throws RemoteException {
    
    	try {
			vAnsatte = new Vector();
			Vector ep = e_handler.getAllEmployees();
			if(ep != null && ep.size()>0)
			{
				vAnsatte = ep;
			    println(" ****** Ansatte i databasen ******");
			    Iterator it = ep.iterator();
			    int vid = 1;
			    while(it.hasNext()) {
			        IEmployee i = (IEmployee)it.next();
			        if(i == null) System.out.println("FUCK");
			        print("  " + vid++ + ": ");
			        println(i.asString());
			    }
			}
			else
			{
			    println(" >>> Ingen ansatte i databasen <<<");                
			}
		} catch (UnkErrException e) {
			System.out.println(e.getMessage());
		}
    
        println("");       
    
    }public void visL�ger()
    {
        try{
        	vAnsatte = new Vector();
            Vector ep = e_handler.getAllDoctors();
            if(ep == null) {
            	System.err.println("WHAAAAT");
            }
            if(ep.size()>0)
            {
            	vAnsatte = ep;
                println(" ****** L�ger i databasen ******");
                Iterator it = ep.iterator();
                int vid = 1;
                while(it.hasNext()) {
                	IEmployee i = (IEmployee)it.next();
                    print("  " + vid++ + ": ");
                    println(i.asString());
                }
            }
            else
            {
                println(" >>> Ingen ansatte i databasen <<<");                
            }
        }catch(Exception e){
        	e.printStackTrace();
        }
        println("");       
    
    }
    
    public Vector vPatienter;    
	public void visPatientListe()
    {
        try{
            Vector ip = p_handler.getAllPatients();
            if(ip == null) {
            	System.err.println("WHAAAAT");
            }
            if(ip.size()>0)
            {
            	vPatienter = ip;
                println(" ****** Patienter i databasen ******");
                Iterator it = ip.iterator();
                int iv = 1;
                while(it.hasNext()) {
					//IPatient i = rmt_login.getIPatient();
                    IPatient i = (IPatient)it.next();
                    print("  " + iv++ + " :");
                    println(i.asString());
                }
            }
            else
            {
                println(" >>> Ingen patienter i databasen <<<");                
            }
        }catch(Exception e) {
        	e.printStackTrace();
        }
        println("");
    }
	
	private Vector vTidsreservationer;
	public void visTidsReservation(){
	
		try {
            vTidsreservationer = new Vector();
            Calendar cal = new GregorianCalendar();
            //Vector itr = t_handler.getFromDate(new Timestamp(cal.getTimeInMillis()));
            Vector itr = t_handler.getAllTidsreservation();
            if(itr != null && itr.size()>0)
            {
            	vTidsreservationer = itr;
                println(" ****** F�lgende tidsreservationer er tilstede i databasen ***** ");
                Iterator it = itr.iterator();
                int vit = 1;
                while(it.hasNext()) {
                    ITidsreservation i  = (ITidsreservation)it.next();
                    
                    print("  " + vit++ + ": ");   
            		println(i.asString());
            	}
            }
            else
            {
                println(" >>> Ingen tidsreservationer i databasen <<<");
            }
            println("");
        }
        catch (RemoteException e) {
            e.printStackTrace();
        }
        catch (UnkErrException e) {
            e.printStackTrace();
        }
	
	}
    
}
