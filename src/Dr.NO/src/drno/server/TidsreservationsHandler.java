/*
 * Created on 2005-05-01 This file is created 2005-05-01 and
 * descripes RmtTidsreservationsHandler
 */
package drno.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservation;
import drno.interfaces.ITidsreservationEditable;
import drno.server.event.Signal;

/**
 * @author tobibobi
 * 
 * @uml.dependency supplier="drno.server.LoginHandlerImpl"
 */

public interface TidsreservationsHandler extends Remote {
    public ITidsreservationEditable createNewTidsReservation() throws RemoteException;

    public ITidsreservation saveTidsreservation(ITidsreservationEditable obj) throws ObjectLockException,
            ConsistanceException,
            UnkErrException,
            RemoteException;

    public Vector getAllTidsreservation() throws RemoteException,
            UnkErrException;

    public void deleteTidsreservation(ITidsreservationEditable obj) throws ConsistanceException,
            ObjectLockException,
            UnkErrException,
            RemoteException;

    public ITidsreservationEditable editTidsreservation(ITidsreservation obj) throws ObjectLockException,
            RemoteException;

    public ITidsreservation releaseTidsreservation(ITidsreservationEditable obj) throws UnkErrException,
            RemoteException,
            ObjectLockException;

    /**
     * Retrives the time reservations on a single date.
     * 
     * @throws UnkErrException
     * @throws RemoteException
     */
    public Vector getFromDate(Timestamp ts) throws RemoteException,
            UnkErrException;

    /**
     * Retrives the time reservations on a single date.
     * 
     * @throws UnkErrException
     * @throws RemoteException
     */
    public Vector getFromDate(Timestamp ts, IEmployee e) throws RemoteException,
            UnkErrException;

    /**
     * onUpdate is signalled when an object from this
     * handler is updated.
     * 
     * @return an interface to a Signal
     * @throws RemoteException
     */
    public Signal getOnUpdate() throws RemoteException;

    public Vector getFromPatient(IPatient p) throws RemoteException,
            UnkErrException;

    /**
     * Opdaterer statestik tiderne fra en tids konsultation.
     * <p>
     * V�r forberedt p� at h�ndtere Lock exceptions da
     * systemet selv foretager l�s.
     * 
     * @param reserv
     *            Den aktuelle tidsreservation
     * @param start
     *            tidspunktet for p�begyndelse af
     *            konsultationen
     * @param slut
     *            tidspunktet for afslutningen af
     *            konsultationen
     * @throws RemoteException
     * @throws UnkErrException
     * @throws ObjectLockException
     * @throws ConsistanceException
     */
    public void updateReelTider(ITidsreservation reserv, Timestamp start,
            Timestamp slut) throws RemoteException,
            UnkErrException,
            ObjectLockException,
            ConsistanceException;

}
