/**
 * 
 */
package drno.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import drno.exception.DatabaseDisconnectedException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.LoginHandler;
import drno.interfaces.PatientHandler;
import drno.interfaces.TidsreservationsHandler;
import drno.server.database.broker.BrokerController;
import drno.server.database.broker.EmployeeBroker;
import drno.server.event.Signal;
import drno.server.event.SignalImpl;
import drno.server.marshall.Marshall;
import drno.server.model.EmployeeImpl;

/**
 * LoginHandler - not furher implemented
 * 
 * @author tobibobi
 */

public class LoginHandlerImpl extends UnicastRemoteObject implements
        LoginHandler {
    /**
     * sugested version id
     */
    private static final long serialVersionUID = -264467930667389644L;
    private EmployeeBroker eBroker;
    SignalImpl onUpdate;

    private TidsreservationsHandler t_handler;
    private EmployeeImpl user;
    // private String un;
    // private String pw;

    /**
     * Master entry point for all handlers.
     * <p>
     * if the user wants to connect to the system, he must
     * access by using this constructor. The ctor examines
     * if the user exists and if his pw is correct.
     * 
     * @param username
     * @param password
     * @throws SecurityDisallowedException
     *             thrown if incorrect user/password
     * @throws UnkErrException
     */
    public LoginHandlerImpl() throws RemoteException {
        // if(onUpdate == null)
        onUpdate = new SignalImpl();
    }

    private LoginHandlerImpl(EmployeeImpl e) throws RemoteException,
            SecurityDisallowedException {
        user = e;
        t_handler = new TidsreservationsHandlerImpl(e);
    }

    /*
     * (non-Javadoc)
     * 
     * @see drno.server.RmtLoginHandler#fireUpdate()
     */
    public void fireUpdate()
    {
        onUpdate.emit(new String("Halloooooo"));
    }

    public EmployeeHandler getEmployeeHandler() throws RemoteException,
            SecurityDisallowedException
    {
        return new EmployeeHandlerImpl(user);
    }

    public Signal getOnUpdate()
    {
        return onUpdate;
    }

    public PatientHandler getPatientHandler() throws RemoteException,
            SecurityDisallowedException
    {
        return new PatientHandlerImpl(user);
    }

    public TidsreservationsHandler getTidsreservationsHandler() throws RemoteException,
            SecurityDisallowedException
    {
        return t_handler;
    }

    public IEmployee getUser() throws RemoteException
    {
        return (IEmployee) Marshall.marshall(user);
    }

    public synchronized LoginHandler login(String username, String password) throws SecurityDisallowedException,
            UnkErrException,
            RemoteException
    {

        eBroker = BrokerController.instance().getEmployeeBroker();
        
        EmployeeImpl e = eBroker.getUserFromLogin(username);
        
        if (e == null) { throw new SecurityDisallowedException(
                "Wrong credentials"); }
        e.verify(password);
        if (!e.isVerified()) { throw new SecurityDisallowedException(
                "Wrong credentials"); }
        return (LoginHandler) new LoginHandlerImpl(e);
        
    }

}
