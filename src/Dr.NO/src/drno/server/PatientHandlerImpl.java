/*
 * Created on 18-03-2005
 * This file is created 18-03-2005 and descripes PatientHandler
 */
package drno.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.IPatientEditable;
import drno.interfaces.PatientHandler;
import drno.server.database.broker.BrokerController;
import drno.server.database.broker.PatientBroker;
import drno.server.database.persistence.LockObject;
import drno.server.event.Signal;
import drno.server.event.SignalImpl;
import drno.server.marshall.Marshall;
import drno.server.model.EmployeeImpl;
import drno.server.model.Patient;



/**
 * Usecase handler for Patients.
 * <p>
 * This class is an implementation of a facade
 * 
 * @author tobibobi
 */
class PatientHandlerImpl extends UnicastRemoteObject implements PatientHandler {
    /**
     * 
     */
    private static final long serialVersionUID = 4323390371654205882L;
    /** Connection to the actual broker */
    private PatientBroker broker;
    private HashMap Locks = new HashMap();
    //private HashMap exportedObjects = new HashMap(); 
    private SignalImpl onUpdate = new SignalImpl();

    public PatientHandlerImpl(IEmployee employee) throws SecurityDisallowedException, RemoteException {
    	if(((EmployeeImpl)employee).isVerified() == false) 
    		throw new SecurityDisallowedException();
    	
        try {
            broker = BrokerController.instance().getPatientBroker();
        }
        catch (DatabaseDisconnectedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Creates an empty IPatient object.
     * <p>
     * The object can be edited or dropped. But remember that it will not be
     * persisted before it has been called with save.
     * 
     * @return e a new editable IPatientEditable object
     * @throws Exception
     */
    public IPatientEditable createNewPatient() throws RemoteException{ 
    	return (IPatientEditable)Marshall.marshall(new Patient()); 
    }

    /**
     * Deletes the patient from the database.
     * 
     * @param e
     *            {@link IPatientEditable} object former marked as editable
     * @throws ObjectLockException
     * @throws UnkErrException
     * @throws Exception
     */
    public void deletePatient(IPatientEditable e) throws 
		ObjectLockException, UnkErrException, RemoteException 
	{
        Patient patient = (Patient) Marshall.unMarshall(e);
        
        LockObject lo = (LockObject)Locks.get(patient);
        patient.delete(lo);
        try {
			patient.commit(lo);
		} catch (ConsistanceException ignore) {	} 
    }
    

    /**
     * Locks a patient object object.
     * <p>
     * By locking an object, it means that the <code>set..()</code>
     * operations can be called on the returned object. If the operation is not called,
     * and the class is simply typecast to {@link drno.interfaces.IPatientEditable}
     * 
     * @param patient
     * @return An IPatient object ready to be edited. The object must be
     *         released by a call to either
     *         {@link #deletePatient(IPatientEditable)},
     *         {@link #savePatient(IPatientEditable)} or
     * @throws RemoteException
     * @throws 
     * 
     * @throws Exception
     * @see IPatientEditable
     */

    public synchronized IPatientEditable editPatient(IPatient patient) 
        throws ObjectLockException, RemoteException 
    {
        Patient prxPatient = (Patient) Marshall.unMarshall(patient);

		LockObject lo = prxPatient.save();
		Locks.put(prxPatient,lo);

        return (IPatientEditable) Marshall.marshall(prxPatient);
    }

    public synchronized IPatient freeLockedPatient(IPatientEditable patient) 
    	throws UnkErrException, RemoteException, ObjectLockException 
    {
        Patient prxPatient = (Patient) Marshall.unMarshall(patient);
        LockObject lo = (LockObject)Locks.get(prxPatient);
        
        prxPatient.rollback(lo);
        

        return (IPatient)Marshall.marshall(prxPatient);
    }

    public Vector getAllPatients() throws UnkErrException, RemoteException {
        return Marshall.marshallVector(broker.getAll());
    }

    /**
     * Persist objects if they are correct.
     * <p>
     * The object must be locked prior to calling this function. If the object
     * is not, an ObjectLockException will be thrown.
     * <p>
     * The object can be locked by calling either {@link #createNewPatient()} or
     * {@link #editPatient(IPatient p)}
     * 
     * @param patient
     *            The patient to be saved. If the object is not locked, the
     *            function will fail.
     * @throws ObjectLockException
     * @throws ConsistanceException
     * @throws UnkErrException
     * @throws Exception
     *             If the object is not locked, it will throw one of two
     *             possible drno.exception.
     * @see drno.exception.ObjectLockException
     * @see drno.exception.DatabaseDisconnectedException
     * @see #createNewPatient()
     * @see #editPatient(IPatient)
     */

    public IPatient savePatient(IPatientEditable patient) throws 
	ObjectLockException, ConsistanceException, 
	UnkErrException, RemoteException 
    {
	Patient prxPatient = (Patient)Marshall.unMarshall(patient);
	
	LockObject lo = (LockObject)Locks.get(prxPatient);
	
	prxPatient.commit(lo);
	
	prxPatient.invalidateLock();
	Locks.remove(lo);
	return (IPatient) Marshall.marshall(prxPatient);
    }

    /**
     * searches for all patients fitting with name resolvement.
     * <p>
     * The name can contain both first and last name. Both will be tested.
     * 
     * @param name
     *            An name or names of the person that is to be searched for.
     * @return a Vector with objects fitting the description of the search
     *         parameters.
     */
    public Vector searchPatientsByName(String name) 
    	throws UnkErrException, RemoteException
    {
        return Marshall.marshallVector(broker.searchByName(name));
    }
    

    public Vector searchPatientsByCPR(String cpr)
    	throws RemoteException,UnkErrException
    {
        return Marshall.marshallVector(broker.searchByCPR(cpr));       
    }

	/* (non-Javadoc)
	 * @see drno.server.RmtPatientHandler#getOnUpdate()
	 */
	public Signal getOnUpdate() throws RemoteException {
		return onUpdate;
	}
}
