/*
 * Created on 2005-05-09
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.server.event;

import java.rmi.RemoteException;

/**
 * @author Sloggi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
class EmitterThread implements Runnable {
	private IEvent event;
	private SignalImpl sig;
	private Object msg;

	public EmitterThread(SignalImpl sig, IEvent event, Object msg) {
		this.event = event;
		this.sig = sig;
	}

	/* (non-Javadoc)
	 * @see java.lang.Runnable#run()
	 */
	public void run() {
		try {
			event.onAction(msg);
		} catch (RemoteException e) {
			// if throws add it to list of deletedEvents
			sig.deleteListener(event);
		}

	}

}
