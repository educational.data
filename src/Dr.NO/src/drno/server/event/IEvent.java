/*
 * Created on 2005-05-09
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package drno.server.event;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * @author Sloggi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IEvent extends Remote {
	public void onAction(Object msg) throws RemoteException;
}