/*
 * Oprettet 2005-04-27 af tobibobi 
 */
package drno.server.event;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Iterator;
import java.util.Vector;

/**
 * @author tobibobi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class SignalImpl extends UnicastRemoteObject implements Signal{
	/**
     * Most be srialized more than once
     */
    private static final long serialVersionUID = 3258416127250936113L;
    private Vector events = new Vector();
	private Object monitor = new Object();
	public SignalImpl() throws RemoteException {
		
	}
	public void addListener(IEvent event) throws RemoteException {
		events.add(event);
	}
	
	public void deleteListener(IEvent event) {
		synchronized (monitor) {
			events.remove(event);
		}
	}
	
	public void emit(Object msg) {
		synchronized (monitor) {
			Iterator it = events.iterator();
			while(it.hasNext()) {
				IEvent event = (IEvent) it.next();
				
				new Thread(new EmitterThread(this,event,msg)).start();
			}

		}
	}
}
