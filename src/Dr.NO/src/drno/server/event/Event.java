/*
 * Oprettet 2005-04-27 af tobibobi 
 */
package drno.server.event;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author tobibobi
 *
 */
public abstract class Event extends UnicastRemoteObject implements IEvent {
	public Event() throws RemoteException {	}
	public abstract void onAction(Object msg) throws RemoteException;
}
