/**
 * 
 */
package drno.server;


import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservation;
import drno.interfaces.ITidsreservationEditable;
import drno.interfaces.TidsreservationsHandler;
import drno.server.database.broker.BrokerController;
import drno.server.database.broker.PatientBroker;
import drno.server.database.broker.TidsreservationBroker;
import drno.server.database.persistence.LockObject;
import drno.server.event.Signal;
import drno.server.event.SignalImpl;
import drno.server.marshall.Marshall;
import drno.server.model.EmployeeImpl;
import drno.server.model.Patient;
import drno.server.model.Tidsreservation;



/**
 * Implements the TidsreservationsHandler.
 * <p>
 * 
 * @author tobibobi
 */
class TidsreservationsHandlerImpl extends UnicastRemoteObject implements TidsreservationsHandler {
    /**
     * Stupid eclipse needs this one
     */
    private static final long serialVersionUID = 3618695297053046836L;
    /** Broker for all patients. */
    private PatientBroker mPatientBroker;
    private HashMap Locks = new HashMap();
    private TidsreservationBroker mTidsBroker;
    private static SignalImpl onUpdate;
    /**
     * Constructor for Tidsreservations handler.
     * <p>
     * if a verified employee object is not passed to this function, 
     * a SecurityDisallowedException is thrown.
     *  
     * @param employee
     * @throws SecurityDisallowedException
     */
    public TidsreservationsHandlerImpl(IEmployee employee) throws SecurityDisallowedException, RemoteException{
    	if(employee == null) 
    		throw new SecurityDisallowedException();
    	if(((EmployeeImpl)employee).isVerified() == false) throw new SecurityDisallowedException();
    	
        try {
            mPatientBroker = BrokerController.instance().getPatientBroker();
            mTidsBroker = BrokerController.instance().getTidsreservationBroker();
        }
        catch (DatabaseDisconnectedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    
    
	
    public ITidsreservationEditable createNewTidsReservation() throws RemoteException {
        return (ITidsreservationEditable)Marshall.marshall(new Tidsreservation());
    } 
    
    public ITidsreservation saveTidsreservation(ITidsreservationEditable obj) throws 
        ObjectLockException, ConsistanceException, UnkErrException,
		RemoteException
    {
        Tidsreservation prxT = (Tidsreservation)Marshall.unMarshall(obj);
        
        LockObject lo = (LockObject)Locks.get(prxT);
		prxT.commit(lo);
		
        Locks.remove(lo);
        // send besked om at der er komme en ny tidsreservation.
        
        getOnUpdate();
        onUpdate.emit((ITidsreservation)Marshall.marshall(prxT));
		return (ITidsreservation)Marshall.marshall(prxT);
    }
	
	public Vector getAllTidsreservation() throws RemoteException, UnkErrException {
        return (Vector) Marshall.marshallVector(mTidsBroker.getAll());
    }
    
    public void deleteTidsreservation(ITidsreservationEditable obj) throws
		ConsistanceException,
		ObjectLockException, UnkErrException, RemoteException
	{
        Tidsreservation prxTid = (Tidsreservation)Marshall.unMarshall(obj);
        // if the object is new, its still not placed in the system.
        LockObject lo = (LockObject)Locks.get(prxTid);
        prxTid.delete(lo);
        try {
			prxTid.commit(lo);
		} catch (ConsistanceException e) {
			// totalt ligegyldigt
		}
        Locks.remove(prxTid);
        
        onUpdate.emit(Marshall.marshall(prxTid));
    }
    
    public ITidsreservationEditable editTidsreservation(ITidsreservation obj) throws 
		ObjectLockException, RemoteException 
	{
        Tidsreservation prxTid = (Tidsreservation)Marshall.unMarshall(obj);
        LockObject lo = prxTid.save();
        Locks.put(prxTid,lo);
        return (ITidsreservationEditable)Marshall.marshall(prxTid);
    }
    
    public ITidsreservation releaseTidsreservation(ITidsreservationEditable obj) throws UnkErrException, 
        RemoteException, ObjectLockException 
    {
        Tidsreservation prxTid = (Tidsreservation)Marshall.unMarshall(obj);
        prxTid.rollback((LockObject)Locks.get(prxTid));
        Locks.remove(prxTid);
        return (ITidsreservation)Marshall.marshall(prxTid);
    }
    
    /**
     * Retrives the time reservations on a single date.
     * @throws UnkErrException
     * @throws RemoteException
     */
	public Vector getFromDate(Timestamp ts) throws RemoteException, UnkErrException {
        return Marshall.marshallVector(mTidsBroker.getFromDate(ts));
    }
	
	/**
     * Retrives the time reservations on a single date.
	 * @throws UnkErrException
	 * @throws RemoteException
     */
	public Vector getFromDate(Timestamp ts, IEmployee e) throws RemoteException, UnkErrException {
        EmployeeImpl employee = (EmployeeImpl)Marshall.unMarshall(e);
        return Marshall.marshallVector(mTidsBroker.getTRsFromDateAndDoctor(ts,employee));
    }
    public Vector getFromPatient(IPatient p) throws RemoteException, UnkErrException {
        Patient patient = (Patient)Marshall.unMarshall(p);
        return Marshall.marshallVector(mTidsBroker.getTRsFromPatient(patient));
    }



	/**
	 * @see drno.interfaces.TidsreservationsHandler#getOnUpdate()
	 */
	public Signal getOnUpdate() throws RemoteException {
		if(onUpdate == null)
			onUpdate = new SignalImpl();
		return onUpdate;
	}

    /**
     * @see drno.interfaces.TidsreservationsHandler#updateReelTider(drno.interfaces.ITidsreservation, java.sql.Timestamp, java.sql.Timestamp)
     */

    public void updateReelTider(ITidsreservation reserv, Timestamp start, Timestamp slut) throws RemoteException, UnkErrException, ObjectLockException, ConsistanceException
    {
        Tidsreservation reservation = (Tidsreservation)Marshall.unMarshall(reserv);
        LockObject lo = reservation.save();
        reservation.setReelTidStart(start);
        reservation.setReelTidSlut(slut);
        reservation.commit(lo);
        getOnUpdate();
        onUpdate.emit(reserv);
    }

}
