/*
 * Oprettet 2005-04-25 af tobibobi
 */
package drno.server.database.broker;

import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.MappingException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.server.database.persistence.LockObject;
import drno.server.database.persistence.repository.MapperFacade;
import drno.server.database.persistence.repository.ObjectMapper;
import drno.server.model.EmployeeImpl;
import drno.server.model.Patient;

/**
 * Broker klasse for Patienter.
 * <p>
 * Vedligeholder alle mappings for de enkelte Patient
 * objekter og dets felter. N�r den bliver instantieret,
 * fors�ger den at oprette en ny tabel til at indeholde
 * klassens felter.
 * 
 * @author tobibobi
 */
public class PatientBroker {
    PatientBroker()  {
        ObjectMapper mp = new ObjectMapper("Patienter", Patient.class);
        try {
            mp.map("Adresse");
            mp.map("Cpr");
            mp.map("Efternavn");
            mp.map("Fornavn");
            mp.map("Sex");
            mp.map("Tlf");
            mp.map("Postnummer");
            mp.map("Doktor", "Doctor").setClass(EmployeeImpl.class);
        }
        catch (MappingException e) {
            e.printStackTrace();
            System.exit(-1);
        }
        try {
            MapperFacade.instance().addMapper(mp);
        
            MapperFacade.instance().CreateTable(mp);
        }
        catch (DatabaseDisconnectedException ex) {
            ex.printStackTrace();
        }
    }

    public Vector<Patient> getAll() throws UnkErrException
    {
        try {
            MapperFacade mf = MapperFacade.instance();
            return (Vector<Patient>) ((Vector) mf.find(Patient.class, ""));
        }
        catch (DatabaseDisconnectedException e) {
            return new Vector<Patient>();
        }
    }

    public Vector<Patient> searchByName(String name) throws UnkErrException
    {
        try {
            MapperFacade mf = MapperFacade.instance();
            return (Vector<Patient>) ((Vector) mf.find(Patient.class, "Fornavn like '%" + name + "%'"));
        }
        catch (DatabaseDisconnectedException e) {
            return new Vector<Patient>();
        }
    }

    public Vector<Patient> searchByCPR(String name) throws UnkErrException
    {
        try {
            MapperFacade mf = MapperFacade.instance();
            return (Vector<Patient>) ((Vector) mf.find(Patient.class, "Cpr like '" + name + "'"));
        }
        catch (DatabaseDisconnectedException e) {
            return new Vector<Patient>();
        }
    }

    /**
     * Sletter en patient og alle dens tilh�rende
     * tidsreservationer.
     * 
     * @param pat
     * @param lock
     * @throws ObjectLockException
     * @throws UnkErrException
     * @throws ConsistanceException
     */
    public void deletePatient(Patient pat, LockObject lock) throws ObjectLockException,
            UnkErrException,
            ConsistanceException
    {
        if (pat.isLockedForEdit(lock)) {
            //MapperFacade mf = MapperFacade.instance();
            TidsreservationBroker brok = BrokerController.instance()
                    .getTidsreservationBroker();
            brok.deletePatientsReservationer(pat);
            pat.delete(lock);
            pat.commit(lock);
        } else
            throw new ObjectLockException();
    }
}
