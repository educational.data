/*
 * Oprettet 2005-04-25 ad tobibobi
 */
package drno.server.database.broker;

import drno.exception.DatabaseDisconnectedException;
import drno.server.database.broker.EmployeeBroker;
import drno.server.database.broker.PatientBroker;
import drno.server.database.broker.TidsreservationBroker;

/**
 * Controller for alle Broker objekter.
 * <p>
 * Alle andre brokers er oprettet i dette objekt med deres default constructor.
 * 
 * @author tobibobi
 */
public final class BrokerController {
	EmployeeBroker e_Broker;
	PatientBroker p_Broker;
	TidsreservationBroker t_Broker;
	
	BrokerController() {
		e_Broker = new EmployeeBroker();
		p_Broker = new PatientBroker();
		t_Broker = new TidsreservationBroker();
	}
	
	static BrokerController inst;
	
	public static BrokerController instance() throws DatabaseDisconnectedException {
		if (inst == null)
			inst = new BrokerController();
		
		return inst;
	}
	
	public EmployeeBroker getEmployeeBroker() { 
		return e_Broker;
	}
	public PatientBroker getPatientBroker() {
		return p_Broker;
	}
	public TidsreservationBroker getTidsreservationBroker() {
		return t_Broker;
	}
}
