/*
 * Created on 2005-04-25

 */
package drno.server.database.broker;

import java.sql.Timestamp;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.MappingException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.server.database.persistence.LockObject;
import drno.server.database.persistence.repository.MapperFacade;
import drno.server.database.persistence.repository.ObjectMapper;
import drno.server.model.EmployeeImpl;
import drno.server.model.Patient;
import drno.server.model.Tidsreservation;



/**
 * @author tobibobi
 * 
 * Vedligeholder alle mappings for de enkelte Tidsreservation objekter og dets felter.
 * N�r den bliver instantieret, fors�ger den at oprette en ny tabel til at indeholde
 * klassens felter.
 */
public class TidsreservationBroker {
	
	TidsreservationBroker() {
		ObjectMapper mp = new ObjectMapper("Tidsreservation",Tidsreservation.class);
		try {
			mp.map("Emne","Emne");
			mp.map("Doctor","Employee").setClass(EmployeeImpl.class);
			mp.map("Patient","Patient").setClass(Patient.class);
			mp.map("Reel_start","ReelTidStart");
			mp.map("Reel_slut","ReelTidSlut");
			mp.map("Slut","TidSlut");
            mp.map("Start","TidStart");
			mp.map("Type","Type");

		} catch (MappingException e) {
			e.printStackTrace();
            System.exit(-1);
		}
        try {
            MapperFacade.instance().addMapper(mp);
			MapperFacade.instance().CreateTable(mp);
		} catch (Exception ex) {
            ex.printStackTrace();
		}
	}
	
	public Vector getAll() throws UnkErrException {
		MapperFacade mf = MapperFacade.instance();
		return mf.find(Tidsreservation.class,"");
	}
	
	public Vector getFromDate(Timestamp ts) throws UnkErrException {
		Vector all = new Vector();
        
        Vector span = getBeginAndEndOfDateVector(ts);
        
        
		MapperFacade mf = MapperFacade.instance();
		return mf.find(Tidsreservation.class,"Start between \"" + span.get(0) + 
                "\" and \"" + span.get(1) + "\";");
	}

    /**
     * @param ts
     * @return
     */
    private Vector getBeginAndEndOfDateVector(Timestamp ts)
    {
        GregorianCalendar cal = new GregorianCalendar();
        Vector span = new Vector(2);
        cal.setTime(ts);
        System.out.println(cal);
        ts.setHours(0);
        ts.setMinutes(0);
        ts.setSeconds(0);
        
        System.out.println(ts);
        span.add(ts.clone());
        ts.setHours(23);
        ts.setMinutes(59);
        ts.setSeconds(59);
        span.add(ts.clone());
        return span;
    }
	
	public Vector getTRsFromDateAndDoctor(Timestamp ts,EmployeeImpl e) throws UnkErrException {
		Vector all = new Vector();
        Vector span = getBeginAndEndOfDateVector(ts);
		
		MapperFacade mf = MapperFacade.instance();
		return mf.find(Tidsreservation.class,"Start between \"" + span.get(0) + "\" and \"" 
                + span.get(1)  + "\" and Doctor=" + e.getOId() + ";");
	}

	/**
	 * @param patient
	 * @return
	 * @throws UnkErrException
	 */
	public Vector getTRsFromPatient(Patient patient) throws UnkErrException{
        MapperFacade mf = MapperFacade.instance();
		return mf.find(Tidsreservation.class,"Patient=" + patient.getOId());	
     }

    public void deletePatientsReservationer(Patient pat) throws UnkErrException, ObjectLockException
    {
        Vector pats = new Vector();
        HashMap keys = new HashMap();
        Vector LockedTid = new Vector();
        MapperFacade mf = MapperFacade.instance();
        pats = mf.find(Tidsreservation.class,"Patient=" + pat.getOId());
        Iterator it = pats.iterator();
        try {
            while(it.hasNext()) {
                Tidsreservation tid = (Tidsreservation)it.next();
                keys.put(tid,tid.save());
                LockedTid.add(tid);
            }
        } catch(ObjectLockException ex) {
            it = LockedTid.iterator();
            while(it.hasNext()) {
                Tidsreservation tid = (Tidsreservation)it.next();
                tid.rollback((LockObject)keys.get(pat));
            }
            throw ex;
        }
        
        it = pats.iterator();
        
        // in transaction...
        
        while(it.hasNext()) {
            Tidsreservation tid = (Tidsreservation)it.next();
            tid.delete((LockObject)keys.get(tid));
            try { tid.commit((LockObject)keys.get(tid)); }
            catch (ConsistanceException newer_happens) {  }
            
        }
    }
}
