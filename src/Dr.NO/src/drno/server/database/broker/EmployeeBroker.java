/*
 * Oprettet 2005-04-25 af tobibobi
 */
package drno.server.database.broker;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.MappingException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.interfaces.IEmployee;
import drno.interfaces.IEmployeeEditable;
import drno.server.database.persistence.LockObject;
import drno.server.database.persistence.PersistentObject;
import drno.server.database.persistence.repository.MapperFacade;
import drno.server.database.persistence.repository.ObjectMapper;
import drno.server.model.EmployeeImpl;
import drno.server.model.Tidsreservation;
import drno.support.Logger;



/**
 * Broker objekt for Employee objekter.
 * <p>
 * Vedligeholder alle mappings for de enkelte Employee objekter og dets felter.
 * N�r den bliver instantieret, fors�ger den at oprette en ny tabel til at indeholde
 * klassens felter.
 * 
 * @author tobibobi
 */
public class EmployeeBroker {
	/**
	 * CTor for EmployeeBroker og opretter tabel.
	 * <p>
	 * Opretter ObjektMapper for Employee og s�ger for at tilf�je den til
	 * Persistens laget.
	 * @throws DatabaseDisconnectedException 
	 */
	EmployeeBroker() {		
		ObjectMapper mp = new ObjectMapper("Employee",EmployeeImpl.class);
		try {
			mp.map("Fornavn");
			mp.map("Efternavn");
			mp.map("Login");
			mp.map("Password","getPassword","setEncryptedPassword");
			mp.map("Telefon","TelefonNummer");
			mp.map("Tid","TidsOpdeling");
			mp.map("Type");
		} catch (MappingException e) {
			e.printStackTrace();
            System.exit(-1);
		} 
        try {
            MapperFacade.instance().addMapper(mp);
				
			if(MapperFacade.instance().CreateTable(mp))			
				createDummyData();	// Opretter ansatte hvis databasen ikke er tilstede
		} catch (DatabaseDisconnectedException ignore) {
		    
        }
	}
    
	/**
	 * Find en Medarbejder ud fra s�gning p� navn.
	 * Navnet bliver b�de fors�gt p� fornavn og efternavn, hvilket 
	 * burde give en fornuftig opdeling i s�geresultater.
	 * 
	 * @param name
	 * @return vector med resultater
	 * @throws UnkErrException
	 * @throws DatabaseDisconnectedException 
	 */
	public Vector searchEmployeeByName(String name) throws UnkErrException, DatabaseDisconnectedException {

		MapperFacade mf = MapperFacade.instance();
		return mf.find(EmployeeImpl.class,"Fornavn like '%"+name+"%' OR Efternavn like '%"+name+"%'");
	}
	/**
	 * Finder alle medarbejdere i databasen.
	 * 
	 * @return vector med Employee objekter.
	 * @throws UnkErrException
	 * @throws DatabaseDisconnectedException 
	 */
	public Vector getAll() throws UnkErrException, DatabaseDisconnectedException {
		MapperFacade mf = MapperFacade.instance();
		return mf.find(EmployeeImpl.class,"");
	}
    
    public synchronized void deleteEmployee(IEmployeeEditable employee, LockObject lock) 
    throws UnkErrException, ObjectLockException, ConsistanceException {
    	
    	EmployeeImpl emp = (EmployeeImpl) employee;
        try {
            MapperFacade mf = MapperFacade.instance();
            Vector<Tidsreservation> tRes = (Vector<Tidsreservation>)(Vector)mf.find(Tidsreservation.class, "Doctor = "
                    + emp.getOId());
            HashMap<Tidsreservation,LockObject> lockedTHash = new HashMap<Tidsreservation,LockObject>();
            Vector<Tidsreservation> lockedTVector = new Vector<Tidsreservation>();

            if (emp.isLockedForEdit(lock) == false)
                throw new ObjectLockException();

            for (Iterator<Tidsreservation> it = tRes.iterator(); it.hasNext();) {
                Tidsreservation t = it.next();
                if (t.canLock()) {
                    lockedTHash.put(t, t.save());
                    lockedTVector.add(t);

                } else {
                    for (int a = 0; a < lockedTVector.size(); a++) {
                        t = (Tidsreservation) lockedTVector.get(a);
                        t.rollback((LockObject) lockedTHash.get(t));
                    }
                    throw new ObjectLockException();
                }
            }
            for (Iterator it = tRes.iterator(); it.hasNext();) {
                Tidsreservation t = (Tidsreservation) it.next();
                if (t.canLock()) {
                    t.commit(t.delete((LockObject) lockedTHash.get(t)));
                }
            }
            emp.commit(emp.delete(lock));
        }
        catch (DatabaseDisconnectedException e) {
            Logger.log(e);
            e.printStackTrace();
        }
    }


	/**
	 * Find en bestemt bruger ud fra login.
	 * <p>
	 * Dette bruges til at finde en bruger prim�rt under login.
	 * @param login
	 * @return Employee som matcher login eller null hvis ingen match er fundet.
	 * @throws UnkErrException
	 * @throws DatabaseDisconnectedException 
	 */
	public EmployeeImpl getUserFromLogin(String login) throws UnkErrException {
		MapperFacade mf = MapperFacade.instance();

		Vector em;
        try {
            em = mf.find(EmployeeImpl.class,"Login='"+ login + "'");
        
        		if(em.size() == 0)
        			return null;
        		else return (EmployeeImpl)em.get(0);
        }
        catch (DatabaseDisconnectedException e) {
            Logger.log(e);
        }
        return null;
	}
	/**
	 * Finder alle Doktorer i systemet.
	 * 
	 * @return vector med Doctor objekter.
	 * @throws UnkErrException
	 * @throws DatabaseDisconnectedException 
	 */
	public Vector getAllDoctors() throws UnkErrException, DatabaseDisconnectedException {
		MapperFacade mf = MapperFacade.instance();
		return mf.find(EmployeeImpl.class,"Type=" + IEmployee.TYPE_DOCTOR);
	}
	/** 
     * @author Bo 
     * @throws Exception
	 */
	private void createDummyData() {
		try{
		IEmployeeEditable emp = createNewEmployee();
		emp.setFornavn("Administrator");
		emp.setEfternavn("Administrator");
		emp.setPassword("admin");
		emp.setTelefonNummer("00000000");
		emp.setType(IEmployee.TYPE_ADMIN);
		emp.setLogin("admin");
		emp.setTidsOpdeling(10);
		saveEmployee(emp, null);
		
		emp = createNewEmployee();
		emp.setFornavn("Dr. H");
		emp.setEfternavn("Bjergh");
		emp.setPassword("bjergh");
		emp.setTelefonNummer("88888888");
		emp.setType(IEmployee.TYPE_DOCTOR);
		emp.setLogin("bjergh");
		emp.setTidsOpdeling(20);
		saveEmployee(emp, null);	
		
		emp = createNewEmployee();
		emp.setFornavn("Dr. Mikael");
		emp.setEfternavn("H�yer");
		emp.setPassword("mikael");
		emp.setTelefonNummer("55555555");
		emp.setType(IEmployee.TYPE_DOCTOR);
		emp.setLogin("mikael");
		emp.setTidsOpdeling(15);
		saveEmployee(emp, null);	

		emp = createNewEmployee();
		emp.setFornavn("Yvonne");
		emp.setEfternavn("Laumann");
		emp.setPassword("yvonne");
		emp.setTelefonNummer("33333333");
		emp.setType(IEmployee.TYPE_SECRETARY);
		emp.setLogin("yvonne");
		emp.setTidsOpdeling(15);
		saveEmployee(emp, null);
		
		emp = createNewEmployee();
		emp.setFornavn("Anita");
		emp.setEfternavn("Dahl");
		emp.setPassword("anita");
		emp.setTelefonNummer("22222222");
		emp.setType(IEmployee.TYPE_NURSE);
		emp.setLogin("anita");
		emp.setTidsOpdeling(10);
		saveEmployee(emp, null);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public IEmployeeEditable createNewEmployee() {
		return new EmployeeImpl();
	}
	
	public boolean saveEmployee (IEmployee employee, LockObject lock) throws 
		ConsistanceException, 
		UnkErrException 
	{
		try {
			((EmployeeImpl) employee).commit(lock);
		} catch (ObjectLockException e) {
			return false;
		}
		return true;
	}
	
	
}
