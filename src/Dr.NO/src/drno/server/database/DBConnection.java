/*
 * DBConnection.java Created on 5. marts 2005, 14:28 $Log: DBConnection.java,v $
 * DBConnection.java Created on 5. marts 2005, 14:28 Revision 1.1  2006/12/25 14:55:38  tobibobi
 * DBConnection.java Created on 5. marts 2005, 14:28 Initial checkin
 * DBConnection.java Created on 5. marts 2005, 14:28
 */
package drno.server.database;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.Statement;

import drno.exception.DatabaseDisconnectedException;
import drno.server.database.persistence.DBAccessRights;
import drno.support.Logger;
import drno.support.Properties;

/**
 * Is the basic database connection to a mysql database.
 * 
 * @author Leo
 */
public class DBConnection {
    static private String connection = Properties.get("DatabaseConnection");
    static private String databasename = Properties.get("DatabaseName");
    static private String driver = "com.mysql.jdbc.Driver";
    
    private Connection con;
    private boolean connected;

    public DBConnection(DBAccessRights rights) throws DatabaseDisconnectedException {
        connected = false;
        loadDriver();
        try {
            setupConnection(rights.getUser(), rights.getPassword());
            return;
        } catch(DatabaseDisconnectedException ex) {
            DBAccessRights rg = requestRootCreadentials();
            tryToCreateDatabase(rg.getUser(), rg.getPassword(),rights);
        }
        setupConnection(rights.getUser(), rights.getPassword());
    }

    /**
     * @return
     */
    private DBAccessRights requestRootCreadentials()
    {
        InputStreamReader isr = new InputStreamReader( System.in );
        BufferedReader stdin = new BufferedReader( isr );
        try {
        
            System.out.println("Trying to set up a database:");
            System.out.print("provide a user with credentials to create a db \nUsername: ");
            
            String user = new String(stdin.readLine());
            System.out.print("Password: ");
            
            String passwd = new String(stdin.readLine());
            
            DBAccessRights rg = new DBAccessRights(user,passwd);
            return rg;
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    public void close()
    {
        try {
            con.close();
        }
        catch (Exception ex) {
            System.out.println("Failed close of connection");
            System.out.println(ex);
        }
    }

    /**
     * Setup and create basic structure for connection.
     * 
     * @param user
     *            default user to connect to the database
     * @param password
     *            password to access user with
     * @return a valid connection
     * @throws SQLException
     * @throws DatabaseDisconnectedException
     */
    private void tryToCreateDatabase(String user, String password,DBAccessRights rights) throws
            DatabaseDisconnectedException
    {
        Connection con = null;
        
        con = tryConnectionPW(password, user,"mysql");
        if (con == null)
            throw new DatabaseDisconnectedException();
        
        try {
        // try to find the database that is needed.
        if (!lookupDatabase(con)) {
            createDatabase(con, user, password,rights);
        }
        con.close();
        
        } catch(SQLException ex) {
            Logger.log(ex);
            System.err
                    .println("For some reason i cannot connect to the database.");
            System.err.println("Have you started your MySQL database?\r\n");
            System.err.println("Cause: " + ex.getLocalizedMessage());
        }
    }

    /**
     * @param con
     * @return
     * @throws SQLException
     */
    private boolean lookupDatabase(Connection con) throws SQLException
    {
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("Show Databases;");
        boolean isFound = false;
        while (rs.next())
            if (rs.getString(1).equals(databasename))
                isFound = true;
        stmt.close();
        return isFound;
    }

    /**
     * @param con
     * @param user
     * @param password
     * @throws SQLException
     */
    private void createDatabase(Connection con, String user, String password,DBAccessRights rights) throws SQLException
    {
        Statement stmt;
        stmt = con.createStatement();
        System.out.print("Tilsyneladende er databasen ikke oprettet,");
        System.out.println(" s� nu oprettes den for dig....");
        
        stmt.execute("create database " + databasename + ";");

        String s = "GRANT SELECT,INSERT,UPDATE,DELETE,CREATE,DROP" +
                " ON " + databasename + ".* TO '" + rights.getUser()
                + "'@'localhost' IDENTIFIED BY '" + rights.getPassword() + "';";
        stmt.execute(s);
        printWarnings(stmt);
        stmt.execute("FLUSH PRIVILEGES;");
        printWarnings(stmt);
        
        
        stmt.close();
    }

    /**
     * @param stmt
     * @throws SQLException
     */
    private void printWarnings(Statement stmt) throws SQLException
    {
        SQLWarning warn =  stmt.getWarnings();
        if(warn != null) {
            while(warn != null) {
                System.err.println(warn.getMessage());
                warn = warn.getNextWarning();
            }
        }
    }

    public int executeInsert(String s) throws SQLException, DatabaseDisconnectedException
    {
        if(!connected)
            throw new DatabaseDisconnectedException();
        
        Statement stmt = con.createStatement();
        stmt.executeUpdate(s);
        ResultSet set = stmt.getGeneratedKeys();
        if (set.next()) {
            return set.getInt(1);
        } else
            return -1;
    }

    public ResultSet executeQuery(String s) throws SQLException,DatabaseDisconnectedException
    {
        if(!connected)
            throw new DatabaseDisconnectedException();
        
        Statement stmt = con.createStatement();
        return stmt.executeQuery(s);
    }

    public void executeUpdate(String s) throws SQLException, DatabaseDisconnectedException
    {
        if(!connected)
            throw new DatabaseDisconnectedException();
        
        Statement stmt = con.createStatement();
        stmt.executeUpdate(s);
    }

    public Connection getConnection() throws DatabaseDisconnectedException
    {
        if(!connected)
            throw new DatabaseDisconnectedException();
        
        return con;
    }

    /**
     * @param user
     * @param password
     */
    private void setupConnection(String user, String password) throws DatabaseDisconnectedException
    {
        try {
            // make sure that the database is allready
            // present.
            con = tryConnectionPW(password,user,databasename);
            System.out.println("Database Connection established...");
            // mark that our connection is now valid;
            connected = true;
        }
        catch (Exception ex) {
            Logger.log(ex);
            System.out.println("Cannot connect to database.");
            System.out.println("Cause: " + ex.getLocalizedMessage());
            //System.exit(-1);
            throw new DatabaseDisconnectedException();
        }
    }

    /**
     * 
     */
    private void loadDriver()
    {
        try {
            Class.forName(driver);
        }
        catch (ClassNotFoundException ex) {
            System.err
                    .println("It seems that a loadable JConnector driver is not available.");
            System.err
                    .println("1. You MUST download JConnector from MySQL's homepage to use");
            System.err
                    .println("   on this this system. (http://dev.mysql.com/downloads/connector/j)");
            System.err
                    .println("2. You can place the resulting jar file besides this .jar file,");
            System.err
                    .println("   but when doing so, it must be renamed to jconnector.jar");
            System.err
                    .println("   You can also place the .jar file in your java\\ext directory");
            System.err.println("        -- but that is untested by me. --");

            System.exit(-1);
        }
    }

    /**
     * Simplified method for connecting to the database.
     * 
     * @param pw
     * @param user
     *            TODO
     * @param con
     * @return
     * @throws DatabaseDisconnectedException 
     */
    private Connection tryConnectionPW(String pw, String user,String database) throws DatabaseDisconnectedException
    {
        try {
            return DriverManager.getConnection(connection + database, user, pw);
        }
        catch (SQLException ignore) {
            System.out.print("User:" + user + " PW:" + pw + " - ");
            System.out.println(ignore.getLocalizedMessage());
            throw new DatabaseDisconnectedException();
        }
    }

    public boolean isConnected()
    {
        return connected;
    }
}
