/*
 * Created on 2005-04-02
 * This file is created 2005-04-02 and descripes PersistentObject
 */
package drno.server.database.persistence;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;

/**
 * @author tobibobi
 */
public abstract class PersistentObject {
    /**
     * OID is the objects abstracted modifier.
     * it is used to identify the object in the repository.
     */
    private OID oid;
    private AbstractState state;
    private LockObject lock;
    
    public PersistentObject() {
    	oid = new OID();
    	setState(StateNew.instance);
    }
    
    /**
     * Is this actually needed?
     * 
     * @param lock
     */
    public void setLock(LockObject lock) {
    	this.lock = lock;
    }
    
    /**
     * verifies that an object can be locked for editing.
     * 
     * @return true if the object is able to be locked.
     */
    
    public boolean canLock() {
    	if(lock != null) {
    		return lock.isValid();
    	}
    	return true;
    }
    /**
     * Verifies that an objectLock is valid.
     * 
     * If an object is able to be edited, it must own a valid lock object
     * and the user of the object, must be able to present the object with 
     * a valid lock object.
     * 
     * @param lock
     * @return true if the object is locked for editing by the lock parameter
     */
    public boolean isLockedForEdit(LockObject lock) {
    	// if the provided lock is empty - fail
    	if(lock == null) 
    		return false;
    	
    	// if the object has not provided with a lock object - fail
    	if(this.lock == null)
    		return false;
    	
    	// a lock exists, tjeck if the object match  
    	if( this.lock.equals( lock ) ) 
    		return lock.isValid();
    	
    	// all tjecks failed.
    	return false;
    }
	
    public OID getOId() {
        return oid;
    }
    public void setOID(OID oid) {
        this.oid = oid;
    }
    
    public boolean isExpired() {
        return state.canUnload();
    }
    
    public void setState(AbstractState state) {
        this.state = state;
    }

    public synchronized void commit(LockObject lock) throws 
		ObjectLockException, 
		ConsistanceException,
		UnkErrException
	{
        state.commit(lock, this);
    }
    public synchronized LockObject delete(LockObject lock) throws ObjectLockException {
        state.delete(lock, this);
        return lock;
    }
    public synchronized void invalidateLock() {
    	if(lock != null)
    		lock.invalidate();
    }
    public synchronized void rollback(LockObject lock) throws UnkErrException, ObjectLockException {
        state.rollback(lock,this);
    }
    public synchronized LockObject save() throws ObjectLockException {
        return state.save(this);
    }
    /**
     * verify the subObjects integrety.
     * <p>
     * The method is called prior to saving the object to the database.
     * this is because we do not want the object to be saved if its inconsistent.
     * 
     * @throws ConsistanceException is thrown upward if the object is not 
     * able to be persisted.
     */
    public abstract void verify() throws ConsistanceException;
}
