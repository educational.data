/*
 * Created on 2005-04-02
 * This file is created 2005-04-02 and descripes OID
 */
package drno.server.database.persistence;

/**
 * @author tobibobi
 */
public class OID {
    public String key;
    public Class cls;
    
    public OID(Class cls, String Key) {
        this.key = Key;
        this.cls = cls;
    }
    
    public OID() {
    	key = new String("");
    }
    
    public OID(Class cls, Integer Key) {
        this.key = Key.toString();
        this.cls = cls;
    }
    public OID(Class cls, int Key) {
        this.key = new Integer(Key).toString();
        this.cls = cls;
    }
    
    public boolean equals(Object obj) {
    	OID ob = (OID)obj;
    	return (cls.equals(ob.cls) && key.equals(ob.key));
    }
    
    public String toString() {
        return key;
    }

	/**
	 * @return
	 */
	public boolean valid() {
		if(key == null) return false;
		if(key.length() == 0) return false;
		if(key.equals("0")) return false;
		return true;
	}
}
