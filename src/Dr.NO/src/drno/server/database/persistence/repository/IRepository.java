package drno.server.database.persistence.repository;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.UnkErrException;
import drno.server.database.persistence.OID;
import drno.server.database.persistence.PersistentObject;

import java.util.Vector;

public interface IRepository {

    /**
     * @throws UnkErrException
     * @throws DatabaseDisconnectedException 
     * @see database.persistence.AbstractPersistenceMapper#getObjectFromStorage(drno.server.database.persistence.OID)
     */
    abstract public <T extends PersistentObject> T get(OID oid, ObjectMapper<T> mapper) throws UnkErrException, DatabaseDisconnectedException;
    abstract public <T extends PersistentObject> Vector<T> get(ObjectMapper<T> mapper, String query) throws UnkErrException, DatabaseDisconnectedException;
    abstract public void reload(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException;
    abstract public void delete(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException;

    /**
     * @throws SQLException
     * @see database.persistence.AbstractPersistenceMapper#putObjectToStorage(drno.server.database.persistence.OID, drno.server.database.persistence.PersistentObject)
     */
    abstract public void update(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException;
    abstract public void insert(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException;

}