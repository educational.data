package drno.server.database.persistence.repository;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import drno.exception.DatabaseDisconnectedException;
import drno.exception.UnkErrException;
import drno.server.database.persistence.OID;
import drno.server.database.persistence.PersistentObject;


public class FieldSet<T> {
    Method getMethod;
    Method setMethod;
    Class<T> dataType;
    boolean bDirect;
    String field;

    FieldSet(Method get, Method set, Class<T> type, String dbField) {
        getMethod = get;
        setMethod = set;
        dataType = type;
        field = dbField;
        bDirect = false;
    }
    
    /**
     * Call the objects <i>put</i> method for the registred value field.
     * 
     * @param containedObject
     * @param data
     * @throws UnkErrException
     * @throws DatabaseDisconnectedException 
     */
    void put(T containedObject, Object data) throws UnkErrException, DatabaseDisconnectedException
    {
    	try {
	    	Object[] args = new Object[1];
	    	args[0] = data;
	    	if(bDirect) {
	    		args[0] = MapperFacade.instance().get(new OID(dataType,(Integer)data),dataType);
	    	}
	        setMethod.invoke(containedObject, args);
        }
        catch (IllegalArgumentException e) {
        	System.err.println("Field: " + field);
            e.printStackTrace();
            
        }
        catch (IllegalAccessException e) {
        	System.err.println("Field: " + field);
            e.printStackTrace();
        }
        catch (InvocationTargetException e) {
        	System.err.println("Field: " + field);
            e.printStackTrace();
        }
    }
    /**
     * call the get method on the object instantiated with PersistentObject as a superclass.
     * 
     * @param obj
     * @return
     */
    Object get(T obj)
    {
        Object[] args = new Object[0];
        try {
        	if(bDirect) {
        		Object obbj = getMethod.invoke(obj, args);
        		PersistentObject po = (PersistentObject)obbj;
        		if(po == null) return new Integer(0);
        		return new Integer(po.getOId().toString());
        	} else {
        		return getMethod.invoke(obj, args);
        	}
        }
        catch (IllegalArgumentException e) {
            System.err.println("Error with field: " + field + " on method: " + getMethod.toString());
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        catch (ClassCastException e) {
        	System.err.println("Error with field: " + field + " on method: " + getMethod.toString());
        	e.printStackTrace();
        }
        return null;
    }
    
    public void setClass(Class<T> gClass) {
    	dataType = gClass;
    	bDirect = true;
    }
}