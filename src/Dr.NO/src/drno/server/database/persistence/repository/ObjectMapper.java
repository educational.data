package drno.server.database.persistence.repository;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;

import drno.exception.MappingException;
import drno.server.database.persistence.PersistentObject;

/**
 * Maps datafields to object get/set methods.
 * <p>
 * This is an rich object that is supposed to be the single
 * point where an object will be mapped. Makes sure that a
 * field may only be needed to set at one location.
 * <p>
 * <h3>Example:</h3>
 * We have a class in our object domain with following
 * settings:
 * 
 * <pre>
 * class DomainAccount extends PersistentObject {
 *     public String getName()
 *     {
 *         return name;
 *     }
 * 
 *     public void setName(String name)
 *     {
 *         this.name = name;
 *     }
 * }
 * </pre>
 * 
 * Next we wants to map the object to a repository:
 * 
 * <pre>
 * 
 * // create mapper for table dbAccounts &lt;-&gt; DomainAccounts
 * ObjectMapper map = new ObjectMapper(&quot;dbAccounts&quot;, DomainAccount.class);
 * 
 * // map the Name &lt;-&gt; dbName
 * map.addMapping(&quot;dbName&quot;, &quot;Name&quot;, String.class);
 * </pre>
 * 
 * @author tobibobi
 */
public final class ObjectMapper<T extends PersistentObject> {
    private HashMap<String,FieldSet<?> > fields = new HashMap<String,FieldSet<?> >();
    private String table;
    private Class<T> cls;

    public ObjectMapper(String table, Class<T> cls) throws ClassCastException {
        this.table = table;
        this.cls = cls;
        
        // test to see if we have a PersistentObject
        if (!cls.getSuperclass().equals(PersistentObject.class)) 
            throw new ClassCastException();
    }

    public FieldSet map(String field) throws MappingException
    {
        return map(field, field);
    }

    /**
     * Asccociates a specific name in the database with a
     * get/set method.
     * <p>
     * To use this method, the methods in the class, must be
     * prefixed with a "get" and a "set" <br>
     * Examble:
     * 
     * <pre>
     * class A {
     * int getD() { ... }
     * void setD(int d) { ... }}
     * 
     * // the mapping object.
     * class B extends ObjectMapper {
     *     B() {
     *         super(&quot;Atable&quot;, A.class);
     *         map(&quot;field&quot;, &quot;D&quot;);
     *     }
     * }
     * </pre>
     * 
     * If this is not suitable, one may use the
     * {@link #map(String, String, String)} function
     * instead.
     * 
     * @param dbfield
     *            name of the field in the database
     * @param clsfield
     *            name of the get/set method's in the class
     *            that must be used to update/retrive the
     *            object.
     * @throws SecurityException
     *             If the method is not accesable
     * @throws NoSuchMethodException
     *             there must be an associated get/set
     *             method.
     * @throws MappingException
     * @see #map(String, String, String)
     */
    public FieldSet map(String dbfield, String clsfield) throws MappingException
    {
        return map(dbfield, "get" + clsfield, "set" + clsfield);
    }

    /**
     * Associates a field name with a method for retriving
     * and a method for setting.
     * <p>
     * This method should be used when
     * {@link #map(String,String)} do not suffice. <br>
     * One must also know that the two datatypes used in the
     * retriving and setting methods, must be equal.
     * 
     * @param dbfield
     *            field in repository that is to be mapped.
     * @param sGetMethod
     *            name of retriving method in class
     * @param sSetMethod
     *            name of setting method in class
     * @throws SecurityException
     *             if the method is somehov hidden from
     *             view.
     * @throws NoSuchMethodException
     *             will be thrown if the method dosnt exist,
     *             or if the argument class is not the same.
     * @throws MappingException
     * @see #map(String, String)
     */
    @SuppressWarnings("unchecked")
	public FieldSet map(String dbfield, String sGetMethod, String sSetMethod) throws MappingException
    {
        try {
            Method getMethod = null;
            Method setMethod = null;
            if (sGetMethod != null) {
                try {
                    getMethod = cls.getMethod(sGetMethod, (Class[]) null);
                }
                catch (NoSuchMethodException e) {
                    throw new MappingException("Could not map to method: "
                            + sGetMethod, e);
                }
            }
            if (sSetMethod != null) {
                Class[] bl = new Class[1];
                bl[0] = getMethod.getReturnType();
                try {
                    setMethod = cls.getMethod(sSetMethod, bl);
                }
                catch (NoSuchMethodException e) {

                    throw new MappingException("Could not map to method: "
                            + sSetMethod, e);
                }
            }
            Class<?> cls;
            try {
                cls = verifyUsesEqualClass(setMethod, getMethod);
            }
            catch (NoSuchMethodException e) {

                throw new MappingException("Could not map to methods: "
                        + sGetMethod + " or " + sSetMethod, e);
            }
            // perform the actual mapping
            FieldSet set = new FieldSet(getMethod, setMethod, cls, dbfield);
            // before we add the mapping to the field, we
            // must make sure the mapping
            // has not allready been performed on the field.
            if (fields.containsKey(dbfield))
                throw new drno.exception.MappingException(
                        "Dublicate access to field: " + dbfield);
            fields.put(dbfield, set);
            return set;
        }
        catch (SecurityException e) {
            throw new MappingException("Access violation on field: " + dbfield,
                    e);
        }

    }

    private Class<?> verifyUsesEqualClass(Method setMethod, Method getMethod) throws NoSuchMethodException
    {
        Class<?> s = null;
        Class<?> g = null;
        if (setMethod != null) {
            s = setMethod.getParameterTypes()[0];
        }
        if (getMethod != null) {
            g = getMethod.getReturnType();
            // the getMethod must return a valid Object
            if (g == void.class)
                throw new NoSuchMethodException();
        }

        if ((s == null) && (g == null))
            throw new NoSuchMethodException();
        // if one of the two classes is undef then we need
        // no
        // checking and can just return;
        if (s == null)
            return g;
        if (g == null)
            return s;
        //
        if (s.equals(g) == false)
            throw new NoSuchMethodException();
        // all is okay - lets return the setting method.
        return s;
    }

    public String getBlockName()
    {
        return table;
    }

    public Collection getFields()
    {
        return fields.values();
    }

    T newInstance()
    {
        try {
            return cls.newInstance();
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @return
     */
    public Class<T> getMapClass()
    {
        return cls;
    }
}
