/*
 * Created on 2005-04-02
 * This file is created 2005-04-02 and descripes Cacheable
 */
package drno.server.database.persistence.repository.cache;

/**
 * Enables an object to be cacheable.
 * <p>
 * This interface defines the methods, which must be implemented by all objects
 * wishing to be placed in the cache.
 * 
 * @author Jonathan Lurie
 * @version 1.0
 */

public interface ICacheable {
    /**
     * By requiring all objects to determine their own expirations, the
     * algorithm is abstracted from the caching service, thereby providing
     * maximum flexibility since each object can adopt a different expiration
     * strategy.
     */
    public boolean isExpired();

}