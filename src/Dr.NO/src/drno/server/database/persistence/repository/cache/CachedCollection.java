/*
 * Created on 2005-04-03
 * This file is created 2005-04-03 and descripes CachedCollection
 */
package drno.server.database.persistence.repository.cache;

import java.util.HashMap;

import drno.server.database.persistence.OID;


/**
 * @author tobibobi
 */
class CachedCollection implements ICache {
    private HashMap objs;
    
    CachedCollection() {
        objs = new HashMap();
    }
    
    /**
     * @see drno.server.database.persistence.repository.cache.ICache#get(drno.server.database.persistence.OID)
     */
    public Object get(OID key) {
        return objs.get(key);
    }

    /**
     * @see drno.server.database.persistence.repository.cache.ICache#put(drno.server.database.persistence.OID, drno.server.database.persistence.repository.cache.ICacheable)
     */
    public ICacheable put(OID key, ICacheable value) {
        return (ICacheable) objs.put(key,value);
    }
}
