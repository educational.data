package drno.server.database.persistence.repository;
import java.util.HashMap;
import java.util.Vector;

import drno.exception.DatabaseDisconnectedException;
import drno.exception.UnkErrException;
import drno.server.database.DBConnection;
import drno.server.database.persistence.DBAccessRights;
import drno.server.database.persistence.DefaultDBAccessRights;
import drno.server.database.persistence.OID;
import drno.server.database.persistence.PersistentObject;


/**
 * @author tobibobi
 */
public class MapperFacade {
    private HashMap<Class,ObjectMapper> mappers;
    private MySQLRepository repository;

    private MapperFacade(DBAccessRights rights) throws DatabaseDisconnectedException {
        mappers = new HashMap<Class,ObjectMapper>();
        repository = new MySQLRepository(new DBConnection(rights));
    }
    
    private static MapperFacade fas = null;
    
    public static MapperFacade instance() throws DatabaseDisconnectedException {
    	if(fas == null)
    		fas = new MapperFacade(new DefaultDBAccessRights());
    	return fas;
    }
    
    public PersistentObject get(OID id, Class cls) throws UnkErrException, DatabaseDisconnectedException
    {
        ObjectMapper map = mappers.get(cls);
        return repository.get(id, map);
    }
    
    public <T> Vector<T> find(Class<T> cls, String Query) throws UnkErrException, DatabaseDisconnectedException
    {
        ObjectMapper map = mappers.get(cls);
        return repository.get(map,Query);
    }

    public void insert(PersistentObject obj) throws UnkErrException, DatabaseDisconnectedException 
    {
        ObjectMapper map = mappers.get(obj.getClass());
        repository.insert(obj, map);
    }

    public void update(PersistentObject obj) throws UnkErrException, DatabaseDisconnectedException
    {
        ObjectMapper map = mappers.get(obj.getClass());
        repository.update(obj, map);
    }

    public void reload(PersistentObject obj) throws UnkErrException, DatabaseDisconnectedException
    {
        ObjectMapper map = mappers.get(obj.getClass());
        repository.reload(obj, map);
    }

    public void delete(PersistentObject obj) throws UnkErrException, DatabaseDisconnectedException
    {
        ObjectMapper map = mappers.get(obj.getClass());
        repository.delete(obj, map);
    }

	/**
	 * @param mapper
	 */
	public void addMapper(ObjectMapper mapper) {
		mappers.put(mapper.getMapClass(),mapper);
	}

	/**
	 * @param mp
	 */
	public boolean CreateTable(ObjectMapper mp) throws DatabaseDisconnectedException{
		return repository.createTable(mp);
	}

}