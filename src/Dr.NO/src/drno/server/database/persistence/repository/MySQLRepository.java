package drno.server.database.persistence.repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import com.mysql.jdbc.Statement;

import drno.exception.DatabaseDisconnectedException;
import drno.exception.UnkErrException;
import drno.server.database.DBConnection;
import drno.server.database.persistence.OID;
import drno.server.database.persistence.PersistentObject;
import drno.server.database.persistence.StateOldClean;
import drno.support.Logger;

/**
 * @author tobibobi
 */
public final class MySQLRepository implements IRepository {
    private DBConnection conn;
    private HashMap<OID,PersistentObject> objs = new HashMap<OID,PersistentObject>();

    /**
     * Creates access to the actual MySQL database
     */
    public MySQLRepository(DBConnection conn) {
        this.conn = conn;
    }

    /**
     * @param mp
     */
    public <T extends PersistentObject> boolean createTable(ObjectMapper<T> mp) throws DatabaseDisconnectedException
    {
        HashMap<String,String> maps = new HashMap<String,String>();

        maps.put("java.sql.Timestamp", "timestamp");
        maps.put("int", "int");
        maps.put("java.lang.Number", "int");
        maps.put("java.lang.Integer", "int");
        maps.put("java.lang.String", "varchar(255)");
        

        if(testTable(mp.getBlockName()) == true)
            return false;
        
        try {
            Statement st = (Statement) conn.getConnection().createStatement();
        
            Iterator fields = mp.getFields().iterator();
            String endl = "\n";
            String s = "create table " + mp.getBlockName() + " (" + endl
                    + "  id int(32) primary key auto_increment";
    
            while (fields.hasNext()) {
                FieldSet set = (FieldSet) fields.next();
                s = s + ", " + endl + "  " + set.field + " ";
                String t = (String) maps.get(set.dataType.getName());
    
                if (t == null) {
                    if (set.dataType.getSuperclass() != null) {
                        if (set.dataType
                                .getSuperclass()
                                .equals(
                                        drno.server.database.persistence.PersistentObject.class))
                            s = s + "int";
                        else
                            System.err.println("Unknown mapping from "
                                    + set.dataType.getSuperclass().getName());
                    } else {
                        System.err.println("Unknown mapping from "
                                + set.dataType.getName());
                    }
                } else {
                    s = s + t;
                }
            }
            s = s + endl + ");";
            st.execute(s);
            st.close();
        }
        catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            throw new DatabaseDisconnectedException();
        }
        return true;
    }

    private boolean testTable(String blockName) throws DatabaseDisconnectedException
    {
        try {
            Statement st = (Statement) conn.getConnection().createStatement();
            ResultSet set = st.executeQuery("show tables;");
            boolean result = false;
            while(set.next()) {
                if(set.getString(1).equalsIgnoreCase(blockName))
                    result = true;
            }
            set.close();
            st.close();
            return result;
        }

        catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        
        return false;
    }

    /**
     * @throws DatabaseDisconnectedException 
     * @throws SQLException
     * @see drno.server.database.persistence.repository.IRepository#delete(drno.server.database.persistence.PersistentObject,
     *      drno.server.database.persistence.repository.ObjectMapper)
     */
    public void delete(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException
    {
        String q = "delete from " + mapper.getBlockName() + " where id='"
                + obj.getOId() + "';";

        try {
            conn.executeUpdate(q);
        }
        catch (SQLException e) {
            Logger.log(e);
            Logger.reThrow(e);
        }
    }

    @SuppressWarnings("unchecked")
	public <T extends PersistentObject> Vector<T> get(ObjectMapper<T> mapper, String query) throws UnkErrException, DatabaseDisconnectedException
    {
        String q = "select * from " + mapper.getBlockName();
        if (query.length() > 0) {
            q = q + " where " + query;
        }
        q = q + ";";
        Vector<T> retVec = new Vector<T>();
        try {
            ResultSet rs = conn.executeQuery(q);
            while (rs.next()) {
                T obj = mapper.newInstance();
                OID oid = new OID(obj.getClass(), rs.getString("id"));
                if (objs.containsKey(oid)) {
                    obj = (T)objs.get(oid);
                } else {
                    obj.setOID(oid);
                    obj.setState(StateOldClean.instance);

                    updateObject(mapper, rs, obj);
                }
                retVec.add(obj);
            }
            return retVec;
        }
        catch (SQLException ex) {
            Logger.log(ex);
            Logger.reThrow(ex);
        }
        return null;
    }

    /**
     * @throws DatabaseDisconnectedException 
     * @see drno.server.database.persistence.repository.IRepository#get(drno.server.database.persistence.OID,
     *      drno.server.database.persistence.repository.ObjectMapper)
     */
    @SuppressWarnings("unchecked")
	public <T extends PersistentObject> T get(OID oid, ObjectMapper<T> mapper) throws UnkErrException, DatabaseDisconnectedException
    {
        if (objs.containsKey(oid)) { return (T) objs.get(oid); }
        String q = "select * from " + mapper.getBlockName() + " where id='"
                + oid + "';";

        try {
            ResultSet rs = conn.executeQuery(q);
            if (rs.next()) {
                T obj = mapper.newInstance();

                obj.setOID(oid);
                obj.setState(StateOldClean.instance);

                updateObject(mapper, rs, obj);

                objs.put(oid, obj);
                return obj;
            } else
                return null;

        }
        catch (SQLException ex) {
            Logger.log(ex);
            Logger.reThrow(ex);
        }
        return null;
    }

    /**
     * @throws DatabaseDisconnectedException 
     * @throws SQLException
     * @see drno.server.database.persistence.repository.IRepository#insert(drno.server.database.persistence.PersistentObject,
     *      drno.server.database.persistence.repository.ObjectMapper)
     */
    public void insert(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException
    {
        try {
            String q = "insert into " + mapper.getBlockName() + " (";
            String l = new String();
            boolean isFirst = true;

            Iterator fields = mapper.getFields().iterator();
            while (fields.hasNext()) {
                FieldSet set = (FieldSet) fields.next();
                if (isFirst == false) {
                    q = q + ", ";
                    l = l + ", ";
                }
                isFirst = false;
                q = q + set.field + "";
                l = l + "?";
            }
            q = q + ") values(" + l + ");";
            int id = performStatement(mapper, q, obj);
            obj.setOID(new OID(obj.getClass(), new Integer(id)));
        }
        catch (SQLException e) {
            Logger.log(e);
            Logger.reThrow(e);
        }
    }

    /**
     * @return a value of an generated key id.
     * @param mapper
     * @param query
     * @throws SQLException
     * @throws DatabaseDisconnectedException 
     */
    private int performStatement(ObjectMapper mapper, String query,
            PersistentObject obj) throws SQLException, DatabaseDisconnectedException
    {
        PreparedStatement preparedStatement = conn.getConnection()
                .prepareStatement(query);

        int i = 1;
        Iterator it = mapper.getFields().iterator();
        while (it.hasNext()) {
            FieldSet set = (FieldSet) it.next();
            Object val = set.get(obj);
            if (val instanceof PersistentObject) {
                val = ((PersistentObject) val).getOId().toString();
            }
            preparedStatement.setObject(i++, val);

        }
        preparedStatement.execute();
        ResultSet rs = preparedStatement.getGeneratedKeys();

        int result = 0;
        if (rs != null) {
            if (rs.next()) {
                result = rs.getInt(1);
            }
            rs.close();
        }
        preparedStatement.close();
        return result;
    }

    /**
     * @throws UnkErrException
     * @throws DatabaseDisconnectedException 
     * @see drno.server.database.persistence.repository.IRepository#reload(drno.server.database.persistence.PersistentObject,
     *      drno.server.database.persistence.repository.ObjectMapper)
     */
    public void reload(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException
    {
        String q = "select * from " + mapper.getBlockName() + " where id='"
                + obj.getOId() + "';";
        try {
            ResultSet rs = conn.executeQuery(q);
            updateObject(mapper, rs, obj);
            obj.setState(StateOldClean.instance);
        }
        catch (SQLException ex) {
            Logger.log(ex);
            Logger.reThrow(ex);
        }

    }

    public void setConnection(DBConnection conn)
    {
        this.conn = conn;
    }

    /**
     * @throws DatabaseDisconnectedException 
     * @throws SQLException
     * @see drno.server.database.persistence.repository.IRepository#update(drno.server.database.persistence.OID,
     *      drno.server.database.persistence.PersistentObject,
     *      drno.server.database.persistence.repository.ObjectMapper)
     */
    public void update(PersistentObject obj, ObjectMapper mapper) throws UnkErrException, DatabaseDisconnectedException
    {
        String q = "UPDATE " + mapper.getBlockName() + " SET ";
        boolean isFirst = true;

        Iterator it = mapper.getFields().iterator();
        while (it.hasNext()) {
            if (isFirst == false) {
                q = q + ", ";
            }
            isFirst = false;
            q = q + ((FieldSet) it.next()).field + " = ?";
        }
        q = q + " WHERE id = '" + obj.getOId() + "';";
        try {
            performStatement(mapper, q, obj);
        }
        catch (SQLException e) {
            Logger.log(e);
            Logger.reThrow(e);
        }
    }

    /**
     * @param mapper
     * @param rs
     * @param obj
     * @throws UnkErrException
     * @throws DatabaseDisconnectedException 
     */
    private void updateObject(ObjectMapper mapper, ResultSet rs,
            PersistentObject obj) throws UnkErrException, DatabaseDisconnectedException
    {
        try {
            Iterator fields = mapper.getFields().iterator();
            while (fields.hasNext()) {
                FieldSet field = (FieldSet) fields.next();
                field.put(obj, rs.getObject(field.field));
            }
        }
        catch (SQLException ex) {
            Logger.log(ex);
            Logger.reThrow(ex);
        }
    }
}
