/*
 * Created on 2005-04-03
 * This file is created 2005-04-03 and descripes ICache
 */
package drno.server.database.persistence.repository.cache;

import drno.server.database.persistence.OID;



public interface ICache {

    /**
     * @see java.util.Map#get(java.lang.Object)
     */
    public Object get(OID key);

    /**
     * @see java.util.Map#put(PersistentObject, V)
     */
    public ICacheable put(OID key, ICacheable value);

}