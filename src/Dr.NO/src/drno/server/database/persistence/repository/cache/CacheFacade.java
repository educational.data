/*
 * Created on 2005-04-03
 * This file is created 2005-04-03 and descripes CacheFacade
 */
package drno.server.database.persistence.repository.cache;


/**
 * @author tobibobi
 */
public class CacheFacade {
    private static CacheFacade instance;
    public static CacheFacade getInstance() {
        if(instance == null) {
            instance = new CacheFacade();
        }
        return instance;
    }
    
    private ICache Cache;
    private CacheFacade() {
        Cache = new CachedCollection();
    }
    public ICache getCache() {
        return Cache;
    }
}
