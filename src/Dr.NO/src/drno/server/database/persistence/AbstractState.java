/*
 * Created on 2005-04-02
 * This file is created 2005-04-02 and descripes AbstractState
 */
package drno.server.database.persistence;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;

/**
 * State pattern to represent the database relevant information.
 * <p>
 * The states inherited from this state is:
 * <ul>
 *  <li>StateNew - obj is not yet existent in the database</li>
 *  <li>StateOldClean - obj is in db and unmodified.</li>
 *  <li>StateOldDirty - obj is in db and modified.</li>
 *  <li>StateOldDelete - obj - is in db and about to delete</li>
 * </ul>
 * @author tobibobi
 */
public abstract class AbstractState {
    abstract boolean canUnload();
    void commit(LockObject lock, PersistentObject obj) throws 
		ObjectLockException, 
		ConsistanceException,
		UnkErrException 
	{
        // no-op body.
        // overridden by subclasses
    }
    LockObject delete(LockObject lock, PersistentObject obj) throws ObjectLockException {
        // no-op body.
        // overridden by subclasses
        
        return lock;
    }
    void rollback(LockObject lock, PersistentObject obj) throws UnkErrException, ObjectLockException {
        // no-op body.
        // overridden by subclasses
    }
    LockObject save(PersistentObject obj) throws ObjectLockException {
        // no-op body.
        // overridden by subclasses
    	return null;
    }
}
