/*
 * Created on 2006-03-12
 * This file is created 2006-03-12 and descripes DBAccessRights
 */
package drno.server.database.persistence;

public class DBAccessRights {
    private String user;
    private String password;
    public DBAccessRights(String user, String password) {
        this.user = new String(user);
        this.password = new String(password);
    }
    
    public String getUser() {
        return user;
    }
    
    public String getPassword() {
        return password;
    }
}
