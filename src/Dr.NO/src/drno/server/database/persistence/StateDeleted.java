/*
 * Created on 2005-04-02
 * This file is created 2005-04-02 and descripes StateDeleted
 */
package drno.server.database.persistence;

/**
 * @author tobibobi
 */
final class StateDeleted extends AbstractState {
    /**
     * Singleton constructor.
     * <p>
     * private forces use of instance.
     */
    private StateDeleted() {
    }
    /** singleton instance */
    public final static AbstractState instance = new StateDeleted();
    
    
    boolean canUnload() {
        // the object is now deleted, so no need to keep it in memory
        return true;
    }
}
