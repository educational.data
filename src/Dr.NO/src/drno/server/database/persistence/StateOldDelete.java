/*
 * Created on 2005-04-02 This file is created 2005-04-02 and
 * descripes StateOldDelete
 */
package drno.server.database.persistence;

import drno.exception.DatabaseDisconnectedException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.server.database.persistence.repository.MapperFacade;
import drno.support.Logger;

/**
 * @author tobibobi
 */
final class StateOldDelete extends AbstractState {
    /** singleton instance */
    public final static AbstractState instance = new StateOldDelete();

    /**
     * Singleton constructor.
     * <p>
     * private forces use of instance.
     */
    private StateOldDelete() {
    }

    /**
     * The object is marked as deleted - so make it ready
     * for unload.
     */
    boolean canUnload()
    {
        return false;
    }

    void commit(LockObject lock, PersistentObject obj) throws ObjectLockException,
            UnkErrException
    {
        if (obj.isLockedForEdit(lock)) {
            try {
                MapperFacade.instance().delete(obj);
                obj.setState(StateDeleted.instance);
                obj.invalidateLock();
            }
            catch (DatabaseDisconnectedException e) {
                e.printStackTrace();
                Logger.log(e);
            }
        } else
            throw new ObjectLockException();
    }

    void rollback(LockObject lock, PersistentObject obj) throws UnkErrException,
            ObjectLockException
    {
        if (obj.isLockedForEdit(lock)) {
            try {
                MapperFacade.instance().reload(obj);
                obj.setState(StateOldClean.instance);
                obj.invalidateLock();
            }
            catch (DatabaseDisconnectedException e) {
                Logger.log(e);
                e.printStackTrace();
            }
        } else
            throw new ObjectLockException();
    }
}
