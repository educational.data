/*
 * Created on 2005-04-02
 * This file is created 2005-04-02 and descripes StateOldClean
 */
package drno.server.database.persistence;

import drno.exception.ObjectLockException;

/**
 * Represents the object as existent and non modified.
 * 
 * By calling save, you try to make the object modifyable.
 * 
 * @author tobibobi
 */
final public class StateOldClean extends AbstractState {
    /**
     * Singleton constructor.
     * <p>
     * private forces use of instance.
     */
    private StateOldClean() {
    }
    
    /** singleton instance */
    public final static AbstractState instance = new StateOldClean();
    
    LockObject delete(LockObject lock, PersistentObject obj) throws ObjectLockException {
        if(obj.isLockedForEdit(lock)) {
            obj.setState(StateOldDelete.instance);
            return lock;
        } else throw new ObjectLockException();
    }
    
    /**
     * If the object is unlocked, lock it.
     * <p>
     * In the situation we shift to OldDirty 
     * after the lock has been made.
     * @throws ObjectLockException 
     */
    LockObject save(PersistentObject obj) throws ObjectLockException {
    	System.out.println("here");
    	if(obj.canLock()) {
    		LockObject lo = new LockObject();
    		obj.setLock(lo);
    		obj.setState(StateOldDirty.instance);
    		return lo;
    	} else throw new ObjectLockException();
    }
    
    /** The object is not modified, so therefore yes, it can unload */
    boolean canUnload() {
        return true;
    }
}
