/*
 * Created on 2005-04-02 This file is created 2005-04-02 and
 * descripes StateNew
 */
package drno.server.database.persistence;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.server.database.persistence.repository.MapperFacade;
import drno.support.Logger;

/**
 * Represents the situation when an object is new and still
 * not committed to the database.
 * <p>
 * When its new, the only meaningfull operation will be to
 * commit the object to the database by using the commit
 * operation. all other operations are ignored.
 * 
 * @author tobibobi
 */
final class StateNew extends AbstractState {
    /** singleton instance */
    public final static AbstractState instance = new StateNew();

    /**
     * Singleton constructor.
     * <p>
     * private forces use of instance.
     */
    private StateNew() {
    }

    boolean canUnload()
    {
        // the object is actually not placed in any cache
        // yet.
        return false;
    }

    /**
     * persists the object and changes state to
     * StateOldClean
     * 
     * @throws UnkErrException
     */

    void commit(LockObject lock, PersistentObject obj) throws ObjectLockException,
            ConsistanceException,
            UnkErrException
    {
        try {
            obj.verify();
            MapperFacade.instance().insert(obj);
            obj.setState(StateOldClean.instance);
        }
        catch (Exception e) {
            Logger.log(e);
        }
    }

    LockObject delete(LockObject lock, PersistentObject obj)
    {
        obj.setState(StateDeleted.instance);
        return lock;
    }
}
