/*
 * Oprettet 2005-04-21 af tobibobi
 */
package drno.server.database.persistence;

/**
 * Marks an object to be locked by using this object.
 * 
 * An object gets an object lock that a handler must 
 * be able to equal in order to modify it.
 * 
 * @author tobibobi
 */
public final class LockObject {
	private long lockId;
	private boolean valid;
	private static long curId = 0;
	
	public LockObject() {
		lockId = curId++;
		valid = true;
	}
	
	/**
	 * to invalidate an objects use.
	 * in order to retrive a new lock, one must retrive a new lock
	 */
	public void invalidate() {
		valid = false;
	}
	
	/**
	 * If the object is equal in lock type.
	 * 
	 * and if the object is an actually valid object.
	 */
	public boolean equals(Object lo) {
		if(lo.getClass().equals(LockObject.class)) {
			if(((LockObject)lo).lockId == lockId)
				return isValid();
		}
		return false;
	}
	/**
	 * @return if the object lock is valid.
	 */
	public boolean isValid() {
		return valid;
	}
}
