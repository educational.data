/*
 * Created on 2005-04-02 This file is created 2005-04-02 and
 * descripes StateOldDirty
 */
package drno.server.database.persistence;

import drno.exception.ConsistanceException;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.ObjectLockException;
import drno.exception.UnkErrException;
import drno.server.database.persistence.repository.MapperFacade;
import drno.support.Logger;

/**
 * The object is loaded from the database and needs saving.
 * 
 * @author tobibobi
 */
final class StateOldDirty extends AbstractState {
    /** singleton instance */
    public final static AbstractState instance = new StateOldDirty();

    /**
     * Singleton constructor.
     * <p>
     * private forces use of instance.
     */
    private StateOldDirty() {
    }

    boolean canUnload()
    {
        // Since the object is modified, it must not be
        // unloaded.
        return false;
    }

    void commit(LockObject lock, PersistentObject obj) throws ObjectLockException,
            ConsistanceException,
            UnkErrException

    {
        if (obj.isLockedForEdit(lock)) {
            try {
                obj.verify();
                MapperFacade.instance().update(obj);
                obj.setState(StateOldClean.instance);
            }
            catch (DatabaseDisconnectedException e) {
                e.printStackTrace();
                Logger.log(e);
            }
        } else
            throw new ObjectLockException();
    }

    LockObject delete(LockObject lock, PersistentObject obj) throws ObjectLockException
    {
        if (obj.isLockedForEdit(lock)) {
            obj.setState(StateOldDelete.instance);
            return lock;
        } else
            throw new ObjectLockException();

    }

    void rollback(LockObject lock, PersistentObject obj) throws UnkErrException,
            ObjectLockException
    {
        if (obj.isLockedForEdit(lock)) {
            try {
                MapperFacade.instance().reload(obj);
                obj.setState(StateOldClean.instance);
                obj.invalidateLock();
            }
            catch (DatabaseDisconnectedException e) {
                Logger.log(e);
                e.printStackTrace();
            }
        } else
            throw new ObjectLockException();
    }

    LockObject save(PersistentObject obj) throws ObjectLockException
    {
        // hvis at man pr�ver at kalde den her funktion i
        // denne state,
        // tyder det p� at en anden fors�ger at f� adgang -
        // s� fy fy...
        System.err.println("Argh");
        throw new ObjectLockException();
    }
}
