/*
 * Created on 2006-03-12
 * This file is created 2006-03-12 and descripes DefaultDBAccessRights
 */
package drno.server.database.persistence;

import drno.support.Properties;

public class DefaultDBAccessRights extends DBAccessRights {
    public DefaultDBAccessRights() {
        super(Properties.get("DatabaseUser"),Properties.get("DatabaseUserPassword"));
    }
}
