package drno.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Vector;

import drno.exception.ConsistanceException;
import drno.exception.DatabaseDisconnectedException;
import drno.exception.ObjectLockException;
import drno.exception.SecurityDisallowedException;
import drno.exception.UnkErrException;
import drno.interfaces.EmployeeHandler;
import drno.interfaces.IEmployee;
import drno.interfaces.IEmployeeEditable;
import drno.server.database.broker.BrokerController;
import drno.server.database.broker.EmployeeBroker;
import drno.server.database.persistence.LockObject;
import drno.server.event.Signal;
import drno.server.event.SignalImpl;
import drno.server.marshall.Marshall;
import drno.server.model.EmployeeImpl;

/**
 * Implementationen af interfacet EmployeeHandler. * <p> * Denne klasse har nogle enkelte ting som er en smule anderledes end * de andre handler klasser da det her er muligt at en employee ikke * er i stand til at udf�re alle operationer p� dette interface. * Det er kr�vet at brugeren er administrator i systemet for at man * kan udf�re �ndringer af de andre brugere. *  * @see drno.interfaces.IEmployee * @author Tobias Nielsen, tobibobi@mip.sdu.dk * @author Kim Slot, sloggi@mip.sdu.dk
 * 
 * @uml.dependency supplier="drno.server.LoginHandler"
 */

class EmployeeHandlerImpl  extends UnicastRemoteObject implements EmployeeHandler {
	/**
     * 
     */
    private static final long serialVersionUID = -8525951853532865571L;
    
    /** Connection to the actual broker */
    private EmployeeBroker broker;
    private EmployeeImpl mUser;
    private HashMap<EmployeeImpl,LockObject> Locks = new HashMap<EmployeeImpl,LockObject>();
    private static SignalImpl onUpdate;
    
    public final SignalImpl onEmployeeIsUpdated = new SignalImpl();

    /**
     * Test for om den aktuelle bruger m� instantiere denne klasse.
     * <p>
     * @callgraph
     */
    EmployeeHandlerImpl(IEmployee employee)	throws SecurityDisallowedException, 
    	RemoteException
    {
    	EmployeeImpl e = (EmployeeImpl)employee;
    	if(e.isVerified() == false) {
    		throw new SecurityDisallowedException();
    	}
    	if(onUpdate == null)onUpdate = new SignalImpl();
    	mUser = e;
    	try {
            broker = BrokerController.instance().getEmployeeBroker();
        }
        catch (DatabaseDisconnectedException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
    }

    /**
     * Opretter et tomt IPatient objekt.
     * <p>
     * Objektet kan direkte afl�ses redigeres eller blot
     * droppes. Objektet bliver f�rst gemt med et kald til 
     * {@link #saveEmployee(IEmployeeEditable employee) saveEmployee}
     * hvorefter at man s�ledes igen er n�dt til at l�se objektet.
     * 
     * @return e a new editable IPatientEditable object
     * @throws Exception
     * @deprecated brug den mere udvidede version af createNewEmployee.
     */
    @Deprecated
    public IEmployeeEditable createNewEmployee() 
    	throws SecurityDisallowedException,	RemoteException 
    {
        warnDeprecated(
                "IEmployeeEditable createNewEmployee()", 
                "use public IEmployee createNewEmployee(" +
                "String fornavn, String efternavn, " +
                "String login, String password, " +
                "String tlf, int tidsopdeling, " +
                "int type)"
        );
    	if(mUser.getType() != IEmployee.TYPE_ADMIN)
    		throw new SecurityDisallowedException();
        return (IEmployeeEditable)Marshall.marshall(new EmployeeImpl());
    }
    
    /**
     * Opretter en medarbejder og gemmer ham i databasen.
     * <p>
     * Hvis ikke at den nuv�rende bruger er administrator,
     * f�r vedkommende en drno.exception kastet.
     * 
     * @see drno.exception.SecurityDisallowedException
     * 
     * @param fornavn
     * @param efternavn
     * @param login
     * @param password
     * @param tlf
     * @param tidsopdeling
     * @param type
     * @return Et (@link drno.interfaces.IEmployee medarbejder) objekt som
     * repr�senterer det nyligt oprettede objekt.
     * @throws SecurityDisallowedException
     * @throws RemoteException
     * @throws ConsistanceException
     * @throws UnkErrException
     */
    public IEmployee createNewEmployee(
            String fornavn, 
            String efternavn,
            String login,
            String password,
            String tlf,
            int tidsopdeling,
            int type
    ) 
        throws SecurityDisallowedException, RemoteException, ConsistanceException, UnkErrException 
    {
        if(mUser.getType() != IEmployee.TYPE_ADMIN)
            throw new SecurityDisallowedException();
         EmployeeImpl emp = new EmployeeImpl();
        setEmployee(emp, fornavn, efternavn, tlf, tidsopdeling, login, password, type);
        try {
            emp.commit(null);
        } catch (ObjectLockException ex) {
            
        }
        onUpdate.emit(null);
        return (IEmployee)Marshall.marshall(emp);        
    }

    /**
     * @param emp
     * @param fornavn
     * @param efternavn
     * @param tlf
     * @param tidsopdeling
     * @param login
     * @param password
     * @param type
     * @throws ConsistanceException
     */
    private void setEmployee(EmployeeImpl emp, String fornavn, String efternavn, String tlf, int tidsopdeling, String login, String password, int type) throws ConsistanceException
    {
        emp.setFornavn(fornavn);
        emp.setEfternavn(efternavn);
        emp.setLogin(login);
        emp.setPassword(password);
        emp.setTelefonNummer(tlf);
        emp.setTidsOpdeling(tidsopdeling);
        emp.setType(type);
    }

    /**
     * Sletter en medarbejder.
     * <p>
     * Det er selvf�lgeligt n�dvendigt at den enkelte medarbejder er
     * l�st ved et kald til {@link #lockEmployee(IEmployee)}
     * forinden. 
     * Efterf�lgende kald med dette objekt, vil ikke kunne udf�res.
     * 
     * @param e
     *            {@link IPatientEditable} object former marked as editable
     * @throws ObjectLockException
     * @throws UnkErrException
     * @throws Exception
     * 
     */
    public void deleteEmployee(IEmployee e) throws 
        ObjectLockException, RemoteException, SecurityDisallowedException, UnkErrException
    {
		if(mUser.getType() != IEmployee.TYPE_ADMIN)
            throw new SecurityDisallowedException();   
        EmployeeImpl employee = (EmployeeImpl) Marshall.unMarshall(e);
        if(e.getLogin().equals("admin")) 
            throw new SecurityDisallowedException("Cannot delete admin user");
        LockObject lo = (LockObject)Locks.get(employee);
        try {
            broker.deleteEmployee(employee,lo);
        } catch (ConsistanceException ignore) { }
        onUpdate.emit(null);
    }
    

    /**
     * L�ser et medarbejder objekt for editering.
     * <p>
     * By locking an object, it means that the <code>set..()</code>
     * operations can be called on the returned object. If the operation is not called,
     * and the class is simply typecast to {@link drno.interfaces.IPatientEditable}
     * 
     * @param employee the readonly employee object (IEmployee) that needs to be edited.
     * @return An IPatient object ready to be edited. The object must be
     *         released by a call to either
     *         {@link #deleteEmployee(IEmployeeEditable)},
     *         {@link #saveEmployee(IEmployeeEditable)}
     * 
     * @throws Exception
     * @see IPatientEditable
     * @deprecated anvend i stedet LockObject
     */
	public IEmployeeEditable editEmployee(IEmployee employee) 
        throws ObjectLockException, SecurityDisallowedException, RemoteException 
    {
        warnDeprecated(
                "IEmployeeEditable editEmployee(IEmployee employee)",
                "use lockEmployee Instead"
                );
		if(mUser.getType() != IEmployee.TYPE_ADMIN)
    		throw new SecurityDisallowedException();
		
		EmployeeImpl e = (EmployeeImpl)Marshall.unMarshall(employee);
		LockObject lo = e.save();
		Locks.put(e,lo);
		return (IEmployeeEditable) Marshall.marshall(e);
    }

    /**
     * Hj�lpe funktion til at beskrive hvis en funktion er for�ldet.
     * @param func Navn p� funktionen som er for�ldet.
     * @param use Alternativ ny funktion som man kan anvende i stedet.
     */

    private void warnDeprecated(
                String func,
				String use
				)
    {
        System.out.println("<WARNING> Using deprecated funktion");
        System.out.println("          Function: " + func);
        System.out.println("          use " + use);
    }
    
    /**
     * @see drno.interfaces.EmployeeHandler#freeLockedEmployee(drno.interfaces.IEmployeeEditable)
     * @deprecated use unLockEmployee Instead;
     */
    public IEmployee freeLockedEmployee(IEmployeeEditable employee) 
    	throws SecurityDisallowedException, 
	       UnkErrException, 
	       ObjectLockException, 
	       RemoteException 
    {
        warnDeprecated("IEmployee freeLockedEmployee(IEmployeeEditable employee)", 
		       "use unLockEmployee instead");
    	if(mUser.getType() != IEmployee.TYPE_ADMIN)
	    throw new SecurityDisallowedException();
    	
	EmployeeImpl e = (EmployeeImpl) Marshall.unMarshall(employee);
	
	e.rollback((LockObject)Locks.get(e));
        
        Locks.remove(e);
        
        return (IEmployee) Marshall.marshall(e);
    }

    /**
     * Returnerer alle medarbejdere i databasen.
     * <p>
     * @return Vector med {@link drno.interfaces.IEmployee IEmployee} objekter
     */
    public Vector getAllEmployees() 
	throws UnkErrException, 
	       RemoteException 
    {
        return Marshall.marshallVector(broker.getAll());
    }
    
    /**
     * Returnerer alle medarbejdere som er af typen
     * {@link drno.interfaces.IEmployee.TYPE_DOCTOR TYPE_DOCTOR} i databasen.
     * <p>
     * @return Vector med {@link drno.interfaces.IEmployee IEmployee} objekter
     */
    public Vector getAllDoctors() 
	throws UnkErrException, 
	       RemoteException 
    {
        return Marshall.marshallVector(broker.getAllDoctors());
    }

    /**
     * Gemmer objektet hvis det er konsistent.
     * <p>
     * Medarbejderen skal v�re l�st inden at man kalder denne
     * funktion, ellers vil man f� kastet en 
     * {$link drno.exception.ObjectLockException ObjectLockException}
     * <p>
     * Objktet kan l�ses ved et kald til enten
     * {@link #createNewEmployee()} eller
     * {@link #editEmployee(IEmployee)}
     * <p>
     * If the object is not locked, it will throw one of two
     * possible drno.exception.
     *  - drno.exception.ObjectLockException
     *  - drno.exception.DatabaseDisconnectedException
     * 
     * @param employee
     *            The employee to be saved. If the object is not locked, the
     *            function will fail.
     * @throws UnkErrException
     * @throws ObjectLockException
     * @throws ConsistanceException
     * @throws RemoteException
     * @throws Exception
     * @see #createNewEmployee()
     * @see #editEmployee(IEmployee)
     * 
     * @deprecated 
     */
    public IEmployee saveEmployee(IEmployeeEditable employee) 
	throws SecurityDisallowedException, 
	       UnkErrException, 
	       RemoteException, 
	       ObjectLockException, 
	       ConsistanceException  
    {
        warnDeprecated("IEmployee saveEmployee(IEmployeeEditable employee)",
		       "IEmployee saveEmployee(IEmployee employee, String fornavn, " +  
		       "String efternavn, String login, String password, String tlf, " +
		       "int tidsopdeling, int type) "
		       );
        
    	if(mUser.getType() != IEmployee.TYPE_ADMIN)
	    throw new SecurityDisallowedException();
    	
		EmployeeImpl e = (EmployeeImpl)Marshall.unMarshall(employee);
		e.commit((LockObject)Locks.get(e));
		
		// we must invalidate the lock before unloading it.
		//((LockObject)Locks.get(e)).invalidate();
		Locks.remove(e);
		onUpdate.emit(null);
        return (IEmployee) Marshall.marshall(e);
    }
    
    
    /**
     * Gem en medarbejder ved at specificere hans nye egenskaber.
     * <p>
     * {@link drno.interfaces.IEmployee employee} skal v�re l�st inden dette kald. Hvis ikke at det er sket, vil 
     * en drno.exception blive kastet.
     *
     * \callgraph  
     *
     * @param employee (@see drno.interfaces.IEmployee)
     * @param fornavn
     * @param efternavn
     * @param login
     * @param password
     * @param tlf
     * @param tidsopdeling
     * @param type
     * @return the same employee object as passed in.
     * @throws SecurityDisallowedException
     * @throws UnkErrException
     * @throws RemoteException
     * @throws ObjectLockException
     * @throws ConsistanceException
     */
    public IEmployee saveEmployee(
				  IEmployee employee,
				  String fornavn, 
				  String efternavn,
				  String login,
				  String password,
				  String tlf,
				  int tidsopdeling,
				  int type) 
	throws SecurityDisallowedException, 
	       UnkErrException, 
	       RemoteException, 
	       ObjectLockException, 
	       ConsistanceException  
    {
        EmployeeImpl e = (EmployeeImpl)Marshall.unMarshall(employee);
        
        if(mUser.getType() != IEmployee.TYPE_ADMIN)
            throw new SecurityDisallowedException();
        
        setEmployee(e, fornavn, efternavn, tlf, tidsopdeling, login, password, type);
        
        e.commit((LockObject)Locks.get(e));
        onUpdate.emit(null);
        //The lock has already been invalidated.        
        Locks.remove(e);
        return (IEmployee) Marshall.marshall(e);
    }
    
    /**
     * searches for all employee fitting with name resolvement.
     * <p>
     * The name can contain both first and last name. Both will be tested.
     * 
     * @param name
     *            An name or names of the person that is to be searched for.
     * @return a Vector with objects fitting the description of the search
     *         parameters.
     * @throws UnkErrException
     */
    public Vector searchEmployeesByName(String name) 
    	throws UnkErrException, 
	       RemoteException 
    {
        return Marshall.marshallVector(broker.searchEmployeeByName(name)); 
    }

    /**
     * ObUpdate signal bliver signaleret hvis et objekte i denne handler
     * bliver opdateret.
     * @return Signal interface.
     */
    public Signal getOnUpdate() throws RemoteException {
	return onUpdate;
    }
}
