/*
 * Created on 2005-04-29
 * This file is created 2005-04-29 and descripes PatientMarshaller
 */
package drno.server.marshall;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.Unreferenced;

import drno.exception.ConsistanceException;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatientEditable;
import drno.server.model.EmployeeImpl;
import drno.server.model.Patient;



/**
 * @author tobibobi
 */
class PatientMarshaller extends UnicastRemoteObject implements
        IPatientEditable, Marshaller, Unreferenced {
    private Patient pat;
    
    PatientMarshaller(Patient pat) throws RemoteException {
        super();
        this.pat = pat;
    }
    
    Patient getPatient() {
        return pat;
    }

    public void setCpr(String cpr) throws ConsistanceException, RemoteException
    {
        pat.setCpr(cpr);
    }

    public void setEfternavn(String efternavn) throws ConsistanceException,
            RemoteException
    {
        pat.setEfternavn(efternavn);

    }

    public void setFornavn(String fornavn) throws ConsistanceException,
            RemoteException
    {
        pat.setFornavn(fornavn);

    }

    public void setSex(int sex) throws ConsistanceException, RemoteException
    {
        pat.setSex(sex);
    }

    public void setTlf(String tlf) throws ConsistanceException, RemoteException
    {
        pat.setTlf(tlf);
    }

    public void setDoctor(IEmployee doctor) throws ConsistanceException,
            RemoteException
    {
        pat.setDoctor((IEmployee) Marshall.unMarshall(doctor));

    }

    public void setPostnummer(Integer postnummer) throws ConsistanceException,
            RemoteException
    {
        pat.setPostnummer(postnummer);

    }

    public void setAdresse(String adresse) throws ConsistanceException,
            RemoteException
    {
       	pat.setAdresse(adresse);

    }

    public String getAdresse() throws RemoteException
    {
        return pat.getAdresse();
    }

    public int getAlder() throws RemoteException
    {
        return pat.getAlder();
    }

    public String getCpr() throws RemoteException
    {
        return pat.getCpr();
    }

    public String getEfternavn() throws RemoteException
    {
        return pat.getEfternavn();
    }

    public String getFornavn() throws RemoteException
    {
        return pat.getFornavn();
    }

    public Integer getPostnummer() throws RemoteException
    {
        return pat.getPostnummer();
    }

    public int getSex() throws RemoteException
    {
        return pat.getSex();
    }

    public String getTlf() throws RemoteException
    {
        return pat.getTlf();
    }

    public String asString() throws RemoteException
    {
        return pat.asString();
    }

    public IEmployee getDoctor() throws RemoteException
    {
        return (IEmployee)Marshall.marshall((EmployeeImpl)pat.getDoctor());
    }
    
    public String toString()
	{
    	return pat.asString();
	}

    /**
     * Kaldt af det omkringliggende framework for at indikere at 
     * alle referencer er tabt.
     * 
     * Foresager at den bliver fjernet fra listen af marshalled objects.
     * @see java.rmi.server.Unreferenced#unreferenced()
     */
    public void unreferenced()
    {
        Marshall.instance().removeMarshaller(this);        
    }

}
