/*
 * Created on 2005-04-29
 * This file is created 2005-04-29 and descripes TidsreservationMarshaller
 */
package drno.server.marshall;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.Unreferenced;
import java.sql.Timestamp;

import drno.exception.ConsistanceException;
import drno.exception.ObjectLockException;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservationEditable;
import drno.server.model.Tidsreservation;



/**
 * @author tobibobi
 */
public class TidsreservationMarshaller extends UnicastRemoteObject implements
        ITidsreservationEditable, Marshaller, Unreferenced {

    
    private Tidsreservation tid;
    public TidsreservationMarshaller(Tidsreservation tid) throws RemoteException {
        super();
        this.tid = tid;
    }
    
    Tidsreservation getTidsreservation() {
        return tid;
    }

    public void setPatient(IPatient a) throws ConsistanceException,
            ObjectLockException, RemoteException
    {
        tid.setPatient((IPatient)Marshall.unMarshall(a));

    }

    public void setEmployee(IEmployee a) throws ConsistanceException,
            ObjectLockException, RemoteException
    {
        tid.setEmployee((IEmployee)Marshall.unMarshall(a));

    }

    public void setTidStart(Timestamp a) throws ConsistanceException,
            ObjectLockException, RemoteException
    {
        tid.setTidStart(a);

    }

    public void setTidSlut(Timestamp a) throws ConsistanceException,
            ObjectLockException,RemoteException
    {
        tid.setTidSlut(a);
    }

    public void setReelTidStart(Timestamp a) throws ConsistanceException,
            ObjectLockException, RemoteException
    {
        tid.setReelTidStart(a);
    }

    public void setReelTidSlut(Timestamp a) throws ConsistanceException,
            ObjectLockException, RemoteException
    {
        tid.setReelTidSlut(a);
    }

    public void setEmne(String a) throws ConsistanceException,
            ObjectLockException, RemoteException
    {
        tid.setEmne(a);
    }

    public void setType(int a) throws ConsistanceException, ObjectLockException, RemoteException
    {
        tid.setType(a);
    }

    public IPatient getPatient() throws RemoteException
    {
        return (IPatient) Marshall.marshall(tid.getPatient());
    }

    public IEmployee getEmployee() throws RemoteException
    {
        return (IEmployee) Marshall.marshall(tid.getEmployee());
    }

    public Timestamp getTidStart() throws RemoteException
    {
        return tid.getTidStart();
    }

    public Timestamp getTidSlut() throws RemoteException
    {
        return tid.getTidSlut();
    }

    public Timestamp getReelTidStart() throws RemoteException
    {
        return tid.getReelTidStart();
    }

    public Timestamp getReelTidSlut() throws RemoteException
    {
        return tid.getReelTidSlut();
    }

    public String getEmne() throws RemoteException
    {
        return tid.getEmne();
    }

    public String asString() throws RemoteException
    {
        return tid.asString();
    }

    public int getType() throws RemoteException
    {
        return tid.getType();
    }

    /**
     * Kaldt af det omkringliggende framework for at indikere at 
     * alle referencer er tabt.
     * 
     * Foresager at den bliver fjernet fra listen af marshalled objects.
     * @see java.rmi.server.Unreferenced#unreferenced()
     */
    public void unreferenced()
    {
        Marshall.instance().removeMarshaller(this);        
    }
}
