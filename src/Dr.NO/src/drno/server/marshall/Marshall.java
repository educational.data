/*
 * Created on 2005-04-29
 * This file is created 2005-04-29 and descripes Marshaller
 */
package drno.server.marshall;

import java.rmi.NoSuchObjectException;
import java.rmi.RemoteException;
import java.rmi.server.RemoteObject;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

import drno.server.database.persistence.PersistentObject;
import drno.server.model.EmployeeImpl;
import drno.server.model.Patient;
import drno.server.model.Tidsreservation;



/**
 * @author tobibobi
 */
public class Marshall {

    private Marshall() {
        remoteObjs = new HashMap();
    }
    private static Marshall _instance;
    private HashMap remoteObjs;
    
    static Marshall instance() {
        if(_instance == null)
            _instance = new Marshall();
        
        return _instance;
    }
    
    void removeMarshaller(UnicastRemoteObject mars) {
        // hack for at sikre at ingen objekter bliver tabt p� gulvet...
        PersistentObject obj = (PersistentObject)unMarshall(mars);
        if(obj != null)obj.invalidateLock();
        
        try {
            remoteObjs.remove(UnicastRemoteObject.toStub(mars));
        }
        catch (NoSuchObjectException e) {
            e.printStackTrace();
        }
    }
    
    public static java.util.Vector marshallVector(java.util.Vector v) throws ClassCastException,RemoteException {
    	if(v == null) return null;
        java.util.Vector v2 = new java.util.Vector();
        java.util.Iterator it = v.iterator();
        while(it.hasNext()) {
            v2.add(Marshall.marshall(it.next()));
        }
        return v2;
    }
    
    public static Marshaller marshall(Object obj) throws ClassCastException,RemoteException {
        if(obj == null) return null;
        UnicastRemoteObject ms = null;
        if(obj instanceof Patient) 
            ms = new PatientMarshaller((Patient)obj);
        
        if(obj instanceof EmployeeImpl) 
            ms = new EmployeeMarshaller((EmployeeImpl)obj);
        
        if(obj instanceof Tidsreservation) 
            ms = new TidsreservationMarshaller((Tidsreservation)obj);
        
        if(ms != null) {
            instance().remoteObjs.put(RemoteObject.toStub(ms),ms);
            return (Marshaller)ms;
        }
        else throw new ClassCastException("cannot cast marshaller for " + obj.getClass().getName());
    }
    
    
    public static Object unMarshall(Object obj) {
        if(obj == null) return null;
        obj = instance().remoteObjs.get(obj);
        if(obj == null) {
            return null;
        }
        if(obj instanceof EmployeeMarshaller) 
            return ((EmployeeMarshaller)obj).getEmployee();
        if(obj instanceof PatientMarshaller) 
            return ((PatientMarshaller)obj).getPatient();
        if (obj instanceof TidsreservationMarshaller)
            return ((TidsreservationMarshaller)obj).getTidsreservation();
        
        throw new ClassCastException("Unknown marshaller type " + obj.getClass().getName());
    }

}
