/*
 * Created on 2005-04-29
 * This file is created 2005-04-29 and descripes EmployeeMarshaller
 */
package drno.server.marshall;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.rmi.server.Unreferenced;

import drno.exception.ConsistanceException;
import drno.interfaces.IEmployeeEditable;
import drno.server.model.EmployeeImpl;



/**
 * @author tobibobi
 */
class EmployeeMarshaller extends UnicastRemoteObject implements
        IEmployeeEditable, Marshaller, Unreferenced {
    
    private EmployeeImpl emp;

    public EmployeeMarshaller(EmployeeImpl emp) throws RemoteException {
        super();
        this.emp = emp;
    }

    public void setLogin(String login) throws ConsistanceException,
            RemoteException
    {
        emp.setLogin(login);

    }
    
    public EmployeeImpl getEmployee() {
        return emp;
    }

    public void setPassword(String password) throws ConsistanceException,
            RemoteException
    {
        emp.setPassword(password);

    }

    public void setEfternavn(String efternavn) throws ConsistanceException,
            RemoteException
    {
        emp.setEfternavn(efternavn);

    }

    public void setFornavn(String fornavn) throws ConsistanceException,
            RemoteException
    {
        emp.setFornavn(fornavn);

    }

    public void setTidsOpdeling(int tidsopdeling) throws ConsistanceException,
            RemoteException
    {
        emp.setTidsOpdeling(tidsopdeling);

    }

    public void setType(int type) throws ConsistanceException, RemoteException
    {
        emp.setType(type);

    }

    public void setTelefonNummer(String nr) throws ConsistanceException,
            RemoteException
    {
        emp.setTelefonNummer(nr);

    }

    public String getLogin() throws RemoteException
    {
        return emp.getLogin();
    }

    public String getPassword() throws RemoteException
    {
        return emp.getPassword();
    }

    public String getEfternavn() throws RemoteException
    {
        return emp.getEfternavn();
    }

    public String getFornavn() throws RemoteException
    {
        return emp.getFornavn();
    }

    public int getTidsOpdeling() throws RemoteException
    {
        return emp.getTidsOpdeling();
    }

    public int getType() throws RemoteException
    {
        return emp.getType();
    }

    public String getTelefonNummer() throws RemoteException
    {
        return emp.getTelefonNummer();
    }

    public String asString() throws RemoteException
    {
        return emp.asString();
    }
    
    public String toString()
    {
    	return emp.asString();
    }

    /**
     * Kaldt af det omkringliggende framework for at indikere at 
     * alle referencer er tabt.
     * 
     * Foresager at den bliver fjernet fra listen af marshalled objects.
     * @see java.rmi.server.Unreferenced#unreferenced()
     */
    public void unreferenced()
    {
        Marshall.instance().removeMarshaller(this);        
    }
}
