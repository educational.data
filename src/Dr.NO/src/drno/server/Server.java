package drno.server;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AccessException;
import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.security.Permission;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.UIManager;

import drno.exception.DatabaseDisconnectedException;
import drno.interfaces.LoginHandler;
import drno.server.database.broker.BrokerController;



public class Server {
	private static Server inst;
    private JLabel label;
    private JFrame frm;
    
	public static void main(String[] args) throws Exception {
		instance();
	}
	
	public static Server instance() {
		if(inst == null)
			inst =  new Server();
		return inst;
	}
	
	
	public Server() {
	    createAndShowGUI();
	    startServer();
	}
	
	public void startServer() {
        new Thread(new Runnable() {
            public void run() {

        		LoginHandler rmtloginhandler = null;
        		Registry reg = null;
                
                // for at teste om en database overhovedet er tilg�ngelig,
                // kalder vi lige brokercontrolleren som starter alt db op for os.
                setState("forbinder til DB");
                try {
                    BrokerController.instance();
                }
                catch (DatabaseDisconnectedException e2) {
                    setState("No connection - exit");
                    new Timer(true).schedule(new TimerTask() {
                        @Override
                        public void run()
                        {
                            System.exit(-1);
                        }
                        
                    },2000);
                    return;
                }
                    
                
        		SecurityManager man = new SecurityManager() {
        			public void checkPermission(Permission perm) { }
        			public void checkPermission(Permission perm, Object context) { }
        		};
        		System.setSecurityManager(man);
        
        		//System.setSecurityManager(new RMISecurityManager());
        		setState("starter RMI");
        		try {
        			rmtloginhandler = new LoginHandlerImpl();
        		} catch (RemoteException e1) {
        			// TODO Auto-generated catch block
        			e1.printStackTrace();
        		}
        		try {
        			reg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
        			reg.bind("DrNOServer",rmtloginhandler);
        			
        			
        		} catch(RemoteException ex2) {
        		    System.out.println("Failed to create registry: " + ex2.getMessage());
        		    System.exit(1);
        		} catch (AlreadyBoundException e) {
                    e.printStackTrace();
                } 
        		if(reg == null)
        		    System.exit(-1); 
                setState("k�rende");
            }
        }).start();
	}

    /**
     * 
     */
    void createAndShowGUI()
    {
        String lookAndFeel = null;
        
        lookAndFeel = UIManager.getCrossPlatformLookAndFeelClassName();
        JFrame.setDefaultLookAndFeelDecorated(false);
        try {
            UIManager.setLookAndFeel(lookAndFeel);
        } catch (Exception e) {}
        
        frm = new JFrame();
		
		JFrame _frame = frm;
        _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton but = new JButton("Luk ned for Dr.No serveren");

		but.addActionListener(new ActionListener() {
		   public void actionPerformed(ActionEvent e) {
               setState("lukker ned");
		       //System.out.println("Shutdown server...");
		       try {
		           Registry reg = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
                   reg.unbind("DrNOServer");
                   System.gc();
	            }
	            catch (AccessException e1) {
	                e1.printStackTrace();
	            }
	            catch (RemoteException e1) {
	                e1.printStackTrace();
	            }
	            catch (NotBoundException e1) {
	                e1.printStackTrace();
	            } finally {
	            	System.gc();
	            	System.exit(1);
	            }
		       
		   }
		});
		JPanel panel = new JPanel(new GridLayout(0,1));
        label = new JLabel("<HTML><b>Serveren <font color=red>ved at starte</font></b>");
		panel.add(label);
		panel.add(but);
		panel.add(new JLabel("<HTML><center><font color=#555555><small>Server: <i><b>Dr.<sub>N</sub><sup>O</sup></b></i></small></font></center>"));
		panel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		_frame.getContentPane().add(panel);
        

		_frame.pack();
		_frame.setTitle("DrNO server");
		_frame.setResizable(false);
		Dimension dim = _frame.getToolkit().getScreenSize();
		Rectangle rec = _frame.getBounds();

		int x = dim.width-rec.width;
		int y = dim.height-rec.height;
		_frame.setLocation(x-10,y-40);
		_frame.setAlwaysOnTop(true);

		_frame.setVisible(true);
    }
    
    public void setState(String msg) {
        label.setText("<HTML><b>Serveren er <font color=red>" + msg + "</font></b>");
        frm.pack();
        Dimension dim = frm.getToolkit().getScreenSize();
        Rectangle rec = frm.getBounds();

        int x = dim.width-rec.width;
        int y = dim.height-rec.height;
        frm.setLocation(x-10,y-40);
    }

	/**
	 * 
	 */
	public static void tearDown() {
		try {
            instance().setState("lukker ned");
			Registry reg = LocateRegistry.getRegistry(Registry.REGISTRY_PORT);
			reg.unbind("DrNOServer");
			instance().frm.setVisible(false);
			inst = null;
			System.gc();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		
	}
}
