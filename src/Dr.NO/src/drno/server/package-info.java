/**
 * \section Dr.NO
 * Her findes roden for systemet v. Dr.NO
 * <p>
 * Det er typisk her at alle kald ned til serveren foretages via. RMI.<p>
 * For at f� adgang til systemet, skal serveren n�vendigtvis v�re
 * startet, samt en tilsvarende MySQL drno.server.
 * <p>
 * MySQL serveren skal kunne p�logges med brugeren root og med tomt
 * password.
 * <p>
 * For at starte serveren eksekveres 
 * <pre>
 *  java -cp drno.jar drno.server.Server
 * </pre>
 * 
 */
package drno.server;
/**
 * @mainpage Dr.NO
 * \section Dr.NO - overblik
 * Dr.NO er opbygget af tre entiteter som kan v�re fuldst�ndigt
 * adskildt.
 * Disse er 
 *  - MySQL drno.server
 *  - Server for Dr.NO (indeholdt i en seperat JAR fil)
 *  - Gui del fordelt som en JAR fil.
 */
