/*
 * Created on 2005-03-21
 *
 * $Log: Tidsreservation.java,v $
 * Revision 1.1  2006-12-25 14:55:21  tobibobi
 * Initial checkin
 *
 */
package drno.server.model;
import java.rmi.RemoteException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.GregorianCalendar;

import drno.exception.ConsistanceException;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.ITidsreservationEditable;
import drno.server.database.persistence.PersistentObject;



/**
 * The model version of Tidsreservationer.
 * <p>
 * More docs are to come...
 * 
 * @author  Leo
 * @version $LastChangedBy:$
 * @since   1
 */
public class Tidsreservation extends PersistentObject implements ITidsreservationEditable{
	private IPatient mPatient;
	private IEmployee mEmployee;
	private Timestamp mTidStart;
	private Timestamp mTidSlut;
	private Timestamp mReelTidStart;
	private Timestamp mReelTidSlut;
	private String mEmne;
	private int mType;
	
	public Tidsreservation(){
		mPatient = null;
		mEmployee = null;
		GregorianCalendar c = new GregorianCalendar();
		
		Timestamp t = new Timestamp(c.getTimeInMillis());
		c.add(Calendar.MINUTE,20);
		Timestamp t2 = new Timestamp(c.getTimeInMillis());
		
		
		mTidStart = t;
		mTidSlut = t2;
		mReelTidStart = t;
		mReelTidSlut = t2;
		mEmne = "";
		mType = TYPE_CONSULTATION;
 	}
	
	public IPatient getPatient(){
		return mPatient;
	}
	public IEmployee getEmployee(){
		return mEmployee;
	}
	public Timestamp getTidStart(){
		return mTidStart;
	}
	public Timestamp getTidSlut(){
		return mTidSlut;
	}

	public Timestamp getReelTidStart(){
		return mReelTidStart;
	}
	public Timestamp getReelTidSlut(){
		return mReelTidSlut;
	}
	public String getEmne(){
		return mEmne;
	}
	public int getType(){
		return mType;
	}
	
	public void setPatient(IPatient patient) throws ConsistanceException{
	    checkPatient(patient);
		mPatient = patient;
	}
	/**
     * @param patient
     * @throws ConsistanceException
     */
    private void checkPatient(IPatient patient) throws ConsistanceException
    {
        if(patient == null)
	        throw new ConsistanceException("Cannot add an empty patient to the database");
    }

    public void setEmployee(IEmployee employee) throws ConsistanceException{
	    checkDoctor(employee);
		mEmployee = employee;
	}
	/**
     * @param employee
     * @throws ConsistanceException
     */
    private void checkDoctor(IEmployee employee) throws ConsistanceException
    {
        if(employee == null)
	        throw new ConsistanceException("Cannot add an empty employee to the database");
    }

    public void setTidStart(Timestamp t) throws ConsistanceException{
		mTidStart = t;
	}
	public void setTidSlut(Timestamp t) throws ConsistanceException{
		mTidSlut = t;
	}
	public void setReelTidStart(Timestamp t) throws ConsistanceException{
		mReelTidStart = t;
	}
	public void setReelTidSlut(Timestamp t) throws ConsistanceException{
		mReelTidSlut = t;
	}
	public void setEmne(String emne) throws ConsistanceException {
		mEmne = emne;
	}
	public void setType(int type) throws ConsistanceException {
		if(		(type != TYPE_CONSULTATION) &&
				(type != TYPE_PHONECONSULTATION) &&
				(type != TYPE_HOLIDAY))
			throw new ConsistanceException("Type m� v�re enten TYPE_CONSULTATION, TYPE_PHONECONSULTATION eller TYPE_HOLIDAY");
		mType = type;
	}
	public String asString() throws RemoteException {
        String endl = ",";

		String stype = "";
		switch(mType) {
			case TYPE_CONSULTATION:
				stype = "Konsultation";
				break;
			case TYPE_HOLIDAY:
				stype = "Ferie";
				break;
			case TYPE_PHONECONSULTATION:
				stype = "Telefon konsultation";
				break;				
		}
		
		return "["+
			"Ansat: " + mEmployee.getFornavn() + " " + mEmployee.getEfternavn() + ", " + 
			"Patient: " + mPatient.getFornavn() + " " + mPatient.getEfternavn()+ ", " + 
			"Tidstart: " + mTidStart+ ", " + 
			"Tidslut: " + mTidSlut +
			"Art: " + stype +
			"]";
    }

	/**
	 * @see drno.server.database.persistence.PersistentObject#verify()
	 */
	public void verify() throws ConsistanceException {
		checkPatient(mPatient);
		checkDoctor(mEmployee);
		
	}
}

