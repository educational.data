/**
 * The main model of the problem domain.
 * <p>
 * The model is the innermost part of the system and is not aware of any
 * locks, use case handlers, database etc..
 * 
 * @author tobibobi
 * @author silex
 * @author bo
 * @author sloggi 
 */
package drno.server.model;