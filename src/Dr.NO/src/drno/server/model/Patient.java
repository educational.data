/*
 * Patient.java
 *
 * Created on 6. marts 2005, 15:35
 */

package drno.server.model;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import drno.exception.ConsistanceException;
import drno.interfaces.IEmployee;
import drno.interfaces.IPatient;
import drno.interfaces.IPatientEditable;
import drno.server.database.persistence.PersistentObject;



/**
 * Patient is the model representation of a real life patient.
 * 
 * @author Leo
 * @version $LastChangedRevision: 148 $
 */

public class Patient extends PersistentObject implements IPatientEditable {
	private String adresse;
	private Integer postnummer;
	private int alder;
	private String cpr;
	private EmployeeImpl doctor;
	private String efternavn;
	private String fornavn;
	private int sex;

	private String tlf;

	/**
	 * Creates a new (non persisted) object.
	 * <p>
	 * This is to be used when a new object is to be created.
	 * 
	 * @see database.proxy.PatientProxy#PatientProxy()
	 */
	public Patient() {
		super();
		fornavn = "Tomt navn";
		efternavn = "Tomt efternavn";
		cpr = "000000-0000";
		adresse = "Tom adresse";
		postnummer = new Integer(5000);
		tlf = "Tomt tlf nummer";
		sex = IPatient.SEX_MALE;
		doctor = null;
	}

	public String getAdresse() {
		return adresse;
	}

	public int getAlder()  {
		int day = Integer.parseInt(cpr.substring(0, 1));
		int month = Integer.parseInt(cpr.substring(2, 3));
		int year = Integer.parseInt(cpr.substring(4, 5));

		// Create a calendar object with the date of birth
		Calendar dateOfBirth = new GregorianCalendar(year, month, day);

		// Create a calendar object with today's date
		Calendar today = Calendar.getInstance();

		// Get age based on year
		int age = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);

		// Add the tentative age to the date of birth to get this year's
		// birthday
		dateOfBirth.add(Calendar.YEAR, age);

		// If this year's birthday has not happened yet, subtract one from age
		if (today.before(dateOfBirth)) {
			age--;
		}

		return age;
	}

	public String getCpr() {
		return cpr;
	}

	public IEmployee getDoctor() {
		return doctor;
	}

	public String getEfternavn()  {
		return efternavn;
	}

	public String getFornavn()  {
		return fornavn;
	}

	public int getSex()  {
		return sex;
	}

	public String getTlf()  {
		return tlf;
	}

	public void setAdresse(String value) throws ConsistanceException {
		checkAdresse(value);
		this.adresse = value;
	}

	/**
	 * @param value
	 * @throws ConsistanceException
	 */
	private void checkAdresse(String value) throws ConsistanceException {
		if (value.length() == 0) {
			throw new ConsistanceException(
					"Adressen skal v?re l?ngere end 0 tegn");
		}
	}

	public void setCpr(String cpr) throws ConsistanceException {
		checkCpr(cpr);
		this.cpr = cpr;
	}

	/**
	 * @param cpr
	 * @throws ConsistanceException
	 */
	private void checkCpr(String cpr) throws ConsistanceException {
		// check the integrety of the object.
		Pattern patt = Pattern.compile("^\\d{6}-\\d{4}");
		Matcher match = patt.matcher(cpr);

		if (!match.matches()) {
			throw new ConsistanceException(
					"Ikke acceptabelt CPR. (Skal overholde ######-####)");
		}
	}

	public void setDoctor(IEmployee value) throws ConsistanceException {
	    EmployeeImpl emp = (EmployeeImpl)value;
		checkDoctor(emp);
		doctor = emp;
	}

	/**
	 * @param value
	 * @throws ConsistanceException
	 * @throws RemoteException
	 */
	private void checkDoctor(EmployeeImpl value) throws ConsistanceException {
		if(value == null)
			throw new ConsistanceException("Der skal v�re tilf?jet en l�ge");
		if((value.getType() != IEmployee.TYPE_DOCTOR) && (value.getType() != IEmployee.TYPE_ADMIN))
			throw new ConsistanceException(value.getFornavn() + " " + value.getEfternavn() + " er ikke en l?ge.");
	}

	public void setEfternavn(String value) throws ConsistanceException {
		checkEfternavn(value);
		this.efternavn = value;
	}

	/**
	 * @param value
	 * @throws ConsistanceException
	 */
	private void checkEfternavn(String value) throws ConsistanceException {
		if (value.length() == 0)
			throw new ConsistanceException("Efternavn m? ikke v?re tomt");
	}

	public void setFornavn(String value) throws ConsistanceException {
		checkFornavn(value);
		this.fornavn = value;
	}

	/**
	 * @param value
	 * @throws ConsistanceException
	 */
	private void checkFornavn(String value) throws ConsistanceException {
		if (value.length() == 0)
			throw new ConsistanceException("Fornavn m? ikke v?re tomt");
	}

	public void setSex(int value) throws ConsistanceException {
		checkSex(value);
		this.sex = value;
	}

	/**
	 * @param value
	 * @throws ConsistanceException
	 */
	private void checkSex(int value) throws ConsistanceException {
		if (value != IPatient.SEX_MALE && value != IPatient.SEX_FEMALE)
			throw new ConsistanceException(
					"Sex skal v?re mand eller kvinde (IPatient.SEX_FEMALE eller IPatient.SEX_MALE)");
	}

	public void setTlf(String value) throws ConsistanceException {
		checkTlf(value);
		this.tlf = value;
	}

	/**
	 * @param value
	 * @throws ConsistanceException
	 */
	private void checkTlf(String value) throws ConsistanceException {
		// check the integrety of the object.
		Pattern patt = Pattern.compile("^ *(\\d *){8}");
		Matcher match = patt.matcher(value);

		if (!match.matches()) {
			throw new ConsistanceException("Ikke acceptabelt tlf nr (" + value
					+ "). Nummeret skal v?re p? min 8 cifre.");
		}
	}

	/**
	 * Generates a normal string representing this patient.
	 * 
	 * @since 101
	 * @see java.lang.Object#toString()
	 */
	public String asString() {
		String doctorname = "<NULL>";
		if (doctor != null)
			doctorname = doctor.getFornavn();
		String endl = ", ";
		return "[" + "CPR:" + cpr + endl + "Fornavn:" + fornavn + endl
				+ "Efternavn:" + efternavn + endl + "Doktor:" + doctorname
				+ "]";
	}

	/**
	 * @return Returns the postnummer.
	 */
	public Integer getPostnummer() {
		return postnummer;
	}

	/**
	 * @param value The postnummer to set.
	 */
	public void setPostnummer(Integer value) throws ConsistanceException {
		checkPostnummer(value);
		this.postnummer = value;
	}

	/**
	 * @param value
	 * @throws ConsistanceException
	 */
	private void checkPostnummer(Integer value) throws ConsistanceException {
		if(value.intValue() < 1000)
			throw new ConsistanceException("Postnummeret er ikke et gyldigt postnummer");
	}

	/**
	 * @see drno.server.database.persistence.PersistentObject#verify()
	 */
	public void verify() throws ConsistanceException {
		checkDoctor(doctor);
		checkAdresse(adresse);
		checkPostnummer(postnummer);
		checkCpr(cpr);
		checkEfternavn(efternavn);
		checkFornavn(fornavn);
		checkSex(sex);
		checkTlf(tlf);
	}
	
	public String toString(){
		return new String (fornavn + " " + efternavn);
	}
}
