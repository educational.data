/*
 * Created on 2005-03-16
 *
 * $Log: EmployeeImpl.java,v $
 * Revision 1.1  2006-12-29 17:10:07  tobibobi
 * Inferring generics
 *
 * Revision 1.1  2006/12/25 14:55:21  tobibobi
 * Initial checkin
 *
 *
 */
package drno.server.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import drno.exception.ConsistanceException;
import drno.interfaces.IEmployee;
import drno.interfaces.IEmployeeEditable;
import drno.server.database.persistence.PersistentObject;



/**
 * This class represents the user in this system.
 * <p>
 * When a user logs on to the system, a user must authenticate himself with
 * a username and login.
 * Later on one could find a more general verification function implemented in this class.
 * @author Bo
 */
@SuppressWarnings("serial")
public class EmployeeImpl extends PersistentObject implements IEmployeeEditable {

	private String mLogin;
	private String mPassword;
	private String mEfternavn;
	private String mFornavn;
    private String mTelefonNummer;
	private int mTidsOpdeling;
	private int mType;
    
    /** marks if a user has supplied a correct password */
    private boolean mIsVerified;
	
    public EmployeeImpl() {
        mLogin = new String();
        mPassword = new String();
        mEfternavn = new String();
        mFornavn = new String();
        mIsVerified = false;
    }

	public String getLogin(){
		return mLogin;
	}    
    public String getTelefonNummer()
    {
        return mTelefonNummer;
    }
	public String getPassword(){
		return mPassword;
	}
	public String getEfternavn(){
		return mEfternavn;
	}
	public String getFornavn(){
		return mFornavn;
	}
	public int getTidsOpdeling(){
		return mTidsOpdeling;
	}
	public int getType(){
		return mType;
	}
	
    /**
     * @see drno.interfaces.IEmployeeEditable#setEfternavn(java.lang.String)
     */
    public void setEfternavn(String efternavn) throws ConsistanceException {
    	checkEfternavn(efternavn);
        mEfternavn = efternavn;
    }
    /**
	 * @param efternavn
	 * @throws ConsistanceException
	 */
	private void checkEfternavn(String efternavn) throws ConsistanceException {
		if(efternavn.length() == 0) 
    		throw new ConsistanceException("Efternavn m� ikke v�re tomt");
	}

	/**
     * @see drno.interfaces.IEmployeeEditable#setFornavn(java.lang.String)
     */
    public void setFornavn(String fornavn) throws ConsistanceException {
    	checkFornavn(fornavn);
        mFornavn = fornavn;
    }
    /**
	 * @param fornavn
	 * @throws ConsistanceException
	 */
	private void checkFornavn(String fornavn) throws ConsistanceException {
		if(fornavn.length() == 0)
    		throw new ConsistanceException("Fornavn m� ikke v�re tomt");
	}

	/**
     * @see drno.interfaces.IEmployeeEditable#setLogin(java.lang.String)
     */
    public void setLogin(String login) throws ConsistanceException {
    	checkLogin(login);
    	
        mLogin = login;
    }
    /**
	 * @param login
	 * @throws ConsistanceException
	 */
	private void checkLogin(String login) throws ConsistanceException {
		if(login.length() < 4)
    		throw new ConsistanceException("Login skal v�re p? mindst fire tegn");
	}

	/**
     * @see drno.interfaces.IEmployeeEditable#setPassword(java.lang.String)
     */
    public void setPassword(String password) throws ConsistanceException {
    	checkPassword(password);
        mPassword = encrypt(password);
    }
    private void checkPassword(String password) throws ConsistanceException {
    	if(password.length() < 5) 
    		throw new ConsistanceException("Password skal v�re p� mindst 5 tegns l�ngde");
    }
    /**
     * @see drno.interfaces.IEmployeeEditable#setTidsOpdeling(int)
     */
    public void setTelefonNummer(String value) throws ConsistanceException
    {
    	checkTlf(value);
        mTelefonNummer = value;
    }
    private void checkTlf(String value) throws ConsistanceException
	{
    	// check the integrety of the object.
        Pattern patt = Pattern.compile("^ *(\\d *){8}");
        Matcher match = patt.matcher(value);

        if (!match.matches()) {
            throw new ConsistanceException("Ikke acceptabelt tlf nr (" +value +"). Nummeret skal v�re p� min 8 cifre.");
        }
	}
    public void setTidsOpdeling(int value) throws ConsistanceException {
    	checkTidsOpdeling(value);
        mTidsOpdeling = value;
    }
    
    private void checkTidsOpdeling(int value) 
    	throws ConsistanceException
    {
    	if(value <= 0)
    		throw new ConsistanceException("Tidsopdeling m? ikke v�re mindre end 0");
    }
    /**
     * @see drno.interfaces.IEmployeeEditable#setType(int)
     */
    public void setType(int value) throws ConsistanceException {
    	checkType(value);
    	mType = value;
    }
    
    private void checkType(int value) 
    	throws ConsistanceException 
	{
    	if(		(value != IEmployee.TYPE_ADMIN) &&
    			(value != IEmployee.TYPE_DOCTOR) && 
    			(value != IEmployee.TYPE_SECRETARY) &&
				(value != IEmployee.TYPE_NURSE))
    		throw new ConsistanceException("Type skal v�re enten ADMIN, DOCTOR, NURSE eller SECRETERY");
    }
    
    public boolean verify(String passphrase) {
        return mIsVerified = encrypt(passphrase).equals(mPassword) && getOId().valid();    
    }
    
    public void verify() throws ConsistanceException 
	{
    	checkType(mType);
    	checkTidsOpdeling(mTidsOpdeling);
    	checkTlf(mTelefonNummer);
    	checkLogin(mLogin);
    	checkFornavn(mFornavn);
    	checkEfternavn(mEfternavn);
    }
    
    public void setEncryptedPassword(String password) {
        mPassword = password;
    }
    public String toString()
    {
    	return new String(mFornavn+" "+mEfternavn);
    }
    
    public String asString(){
    	/*
        String endl = ", ";

        String stype = "none";
        switch(mType) {
        	case IEmployee.TYPE_ADMIN:
        		stype = "Administrator";
        		break;
        	case IEmployee.TYPE_DOCTOR:
        		stype = "L�ge";
        		break;
        	case IEmployee.TYPE_NURSE:
        		stype = "Sygeplejerske";
        		break;	
        	case IEmployee.TYPE_SECRETARY:
        		stype = "Sekret�r";
        		break;
        } */
        
        //return "[" +
		//	"LOGIN:" + mLogin + endl +
		//	"FORNAVN:" + mFornavn + endl + 
		//	"EFTERNAVN:" + mEfternavn + endl +
		//	"ROLLE:" + stype +
		//	"]";
        
        return mFornavn + " " + mEfternavn;

    }
    
    private String encrypt(String passphrase) {
        String newpass = new String();
        for(int roll = 0;roll<3;roll++) {
            for(int i=0;i<passphrase.length();i++) {
                newpass = newpass + (passphrase.charAt(i) ^ (char)i+1);
            }
            passphrase = newpass;
            newpass = "";
        }
        for(int i=0;i<passphrase.length();i++) {
            newpass = newpass + (char)(passphrase.charAt(i)-'1'+'A'+i%18);
        }
        return newpass;
    }
    
    public boolean isVerified() {
        return mIsVerified;
    }    
}
