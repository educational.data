/**
 * 
 */
package drno.exception;

/**
 * @author tobibobi
 *
 */
public class ConsistanceException extends Exception{
    private static final long serialVersionUID = 3258134673748930867L;

    public ConsistanceException(String msg) {
        super(msg);
    }
    
    public String toString()
    {
    	return getMessage();
    }
    
}
