/*
 * Oprettet 2005-04-27 af tobibobi 
 */
package drno.exception;

/**
 * Denne drno.exception bliver kastet i tilf�lde hvor at der er andre eksterne fejl
 * som er forekommet et sted i systemet, og som p�virker systemets samlede tilstand.
 * <p>
 * Den bliver typisk kastet via et logger objekt som s�rger for at gemme den 
 * korrekte fejlbesked fra den oprindelige drno.exception.
 *  
 * @author tobibobi
 */
public class UnkErrException extends Exception {
	private static final long serialVersionUID = 3617292246423847223L;
	public UnkErrException(String message) {
		super(message);
	}
}
