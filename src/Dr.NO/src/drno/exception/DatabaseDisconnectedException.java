package drno.exception;
/**
 * Exception to indicate that the connection to the database
 * is currently lost.
 * <p>
 * This drno.exception is typically thrown up to the UI to indikate 
 * that the connection is lost and no work on the system 
 * can currently be done. 
 * 
 * @author tobibobi@mip.sdu.dk
 */
public class DatabaseDisconnectedException extends UnkErrException {
	public DatabaseDisconnectedException() {
		super(Messages.getString("DatabaseDisconnectedException.0")); //$NON-NLS-1$
	}
	/**
	 * An internal serial id.
	 */
	private static final long serialVersionUID = 4051326747239004469L;
}
