/*
 * Created on 2005-05-19
 * This file is created 2005-05-19 and descripes MappingException
 */
package drno.exception;

/**
 * Exception thrown to mark that a double mapping of a single field has been attempted.
 * 
 * @author tobibobi
 */
public class MappingException extends Exception {
    /**
     * internel serial id
     */
    private static final long serialVersionUID = 3906368207974316088L;

    public MappingException(String message) {
        super(message);
    }

    public MappingException(String message, Throwable e) {
        super(message,e);
    }
}
