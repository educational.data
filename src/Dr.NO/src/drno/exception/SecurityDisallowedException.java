/**
 * 
 */
package drno.exception;

/**
 * cast if an invalid action is tried.
 * 
 * @author tobibobi
 */
public final class SecurityDisallowedException extends Exception {
    private static final long serialVersionUID = 3617291246423847223L;
    public SecurityDisallowedException(String msg) {
    	super(msg);
    }
    public SecurityDisallowedException() {
        super(Messages.getString("SecurityDisallowedException.0"));
    }
}
