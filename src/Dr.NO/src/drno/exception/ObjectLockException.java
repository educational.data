/*
 * Created on 18-03-2005
 *
 * $Log: ObjectLockException.java,v $
 * Revision 1.1  2006-12-25 14:55:15  tobibobi
 * Initial checkin
 *
 */
package drno.exception;

/**
 * Marks an object is locked.
 * <p>
 * If an object is accidently trying to be altered by using any of its setMethods,
 * This drno.exception will be thrown. The right way to edit an object will be to 
 * mark the object as being locked prior to trying to set it. This will probally be 
 * altered somewhat by using states patterns in a later implementation.
 * 
 * @author tobibobi
 */
public class ObjectLockException extends Exception {
	private static final long serialVersionUID = 3257844389844760373L;

	public ObjectLockException(String msg) {
		super(msg);
	}
	public ObjectLockException() {
		super(Messages.getString("ObjectLockException.0"));
	}
}
