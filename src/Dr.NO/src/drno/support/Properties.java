/**
 * 
 */
package drno.support;

import java.util.*;

/**
 * @author tobibobi
 * 
 */
public class Properties {
	private HashMap props;
	private static Properties inst;
	private Properties() {
		props = new HashMap();
		set("DatabaseName","drno");
		set("DatabaseConnection","jdbc:mysql://localhost/");
        set("DatabaseUser","drno");
        set("DatabaseUserPassword","1234");
	}
	private void set(String id,String value) {
		props.put(id,value);
	}
	public static String get(String id) throws RuntimeException {
		if(inst == null) {
			inst = new Properties();
		}
		if (inst.props.containsKey(id) == false) {
			throw new RuntimeException("Id cannot be found: " + id);
		}
		return (String)inst.props.get(id);
	}
}
