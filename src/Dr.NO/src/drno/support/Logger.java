/*
 * Created on 18-03-2005
 * This file is created 18-03-2005 and descripes Logger
 * 
 * $Log: Logger.java,v $
 * Revision 1.1  2006-12-25 14:55:36  tobibobi
 * Initial checkin
 *
 */
package drno.support;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.GregorianCalendar;

import drno.exception.UnkErrException;


/**
 * In order to use correct logging, one must abstract logging in to a seperate class.
 * <p>
 * This is it!
 * 
 * @author tobibobi
 */
public class Logger extends OutputStream {
    private Logger() {
        super();
        _stream = new PrintStream(this);
    }
    private static Logger _instance;
    private PrintStream _stream;
    
    public static PrintStream instance() {
        if(_instance == null)
            _instance = new Logger();
        return _instance._stream;
    }
    
    public void write(int t){
        System.out.print((char)t);
    }
    
    public static void log(Exception ex) {
    	if(_instance == null)
            _instance = new Logger();
    	GregorianCalendar gc = new GregorianCalendar();
    	try {
			FileOutputStream file = new FileOutputStream("drno.log",true);
			PrintStream ps = new PrintStream(file);
			ps.println("Exception kastet klokken " + gc.getTime());
			ex.printStackTrace(ps);
			file.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    public static void reThrow(Exception ex) throws UnkErrException {
    	if(_instance == null)
            _instance = new Logger();
    	
    	
    	throw new UnkErrException("Intern fejl er forekommet. Fejlbeskeden er: " + ex.getMessage() + ".\n" +
    			"Resten af beskeden bliver gemt i \"drno.log\"");
    }

}
