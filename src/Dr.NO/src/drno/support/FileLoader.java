/*
 * Created on 2005-05-29
 * This file is created 2005-05-29 and descripes FileLoader
 */
package drno.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarInputStream;
import java.util.zip.ZipEntry;

/**
 * @author tobibobi
 */
public class FileLoader {
    private boolean isJar;
    private JarFile jFile;
    private static FileLoader instance = new FileLoader(null);
    
    public static void setPath(String path) {
        instance = new FileLoader(path);
    }
    public static FileLoader inst() {
        return instance;
    }

    private FileLoader(String zeropath) {
        try {
            if(zeropath == null){
                isJar=false;
                return;
            }
            if(zeropath.endsWith(".jar")) {
                isJar=true;
                jFile = new JarFile(zeropath);
            } else isJar=false;
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public byte[] getBytes(String file) {
        if(isJar) {
            try {
            	//file = file.replaceAll("/","\\");
            	System.out.println("Trying to load: " + file);
                JarEntry entry = jFile.getJarEntry(file);;
                if(entry == null) {
                	System.out.println("file " + file + " not found!");
                	return null;
                }
                	
                int size = (int)entry.getSize();
                System.out.println("Size of image in bytes: " + size);
                byte[] retval = new byte[size];
                InputStream str = jFile.getInputStream(entry);
                byte[] bt = new byte[1];
                for(int ps = 0;ps<size;++ps) {
                	str.read(bt);
                	retval[ps] = bt[0];
                }
                str.read(retval);
                str.close();                
                return retval;
            }
            catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
            	//file = new String("..\\" + file).replaceAll(".JPG",".jpg");
                File fil = new File(file);
                int size = (int)fil.length();
                
                FileInputStream stream = new FileInputStream(file);
                byte[] retval = new byte[size];
                stream.read(retval,0,size);
                stream.close();
                return retval;
            }
            catch (FileNotFoundException e) {
        
                e.printStackTrace();
                return null;
            }
            catch (IOException e) {
        
                e.printStackTrace();
                return null;
            }
        }
    }
    public InputStream getInputStream(String file)
    {
        if(isJar) {
            try {
                ZipEntry entry = jFile.getEntry(file);
                return jFile.getInputStream(entry);
            }
            catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        } else {
            try {
                return new FileInputStream(file);
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
}
