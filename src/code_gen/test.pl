#!/usr/bin/perl

use strict:

my @classes = ();
my $cClass = 0;
my $lastComments = ();

sub loadFile 
{
    ($file) = @_;
    open (CLASSFILE, $file) || die "Could not open file $ARGV[0]";
    
    @raw_data = <CLASSFILE>;
    
    close (CLASSFILE);
    
    foreach $line ( @raw_data ) {
        chomp $line;
        $line =~ /^([^ ]*) (.*)/;
        $command = $1;
        $line = $2;
        parseCommand($command, $line);
    }
    pushCurrentClass();
}

sub parseCommand 
{
    my ($cmd, $line) = @_;

    parseCreateClass ($line) if $cmd eq "CC";

    push ( @{$cClass->{ PUBLIC_CTOR }}, parseCreateConstructor ($line) )        if $cmd eq "CTOR+";
    push ( @{$cClass->{ PRIVATE_CTOR }}, parseCreateConstructor ($line) )       if $cmd eq "CTOR-";
    push ( @{$cClass->{ PROTECTED_CTOR }}, parseCreateConstructor ($line) )     if $cmd eq "CTOR#";

    push ( @{$cClass->{ PUBLIC_FUNCTIONS }}, parseCreateFunction ($line) )      if $cmd eq "F+";
    push ( @{$cClass->{ PRIVATE_FUNCTIONS }}, parseCreateFunction ($line) )     if $cmd eq "F-";
    push ( @{$cClass->{ PROTECTED_FUNCTIONS }}, parseCreateFunction ($line) )   if $cmd eq "F#";

    push ( @{$cClass->{ PUBLIC_ATTRIBUTES }}, parseCreateAttribute ($line) )    if $cmd eq "A+";
    push ( @{$cClass->{ PRIVATE_ATTRIBUTES }}, parseCreateAttribute ($line) )   if $cmd eq "A-";
    push ( @{$cClass->{ PROTECTED_ATTRIBUTES }}, parseCreateAttribute ($line) ) if $cmd eq "A#";

}

sub parseCreateConstructor
{
    ($line) = @_;
    $line =~/\((.*)\)$/;
     my $attrib_str = $1;
     
    $attrib_str =~ s/ //g;
    @attribs = split(/,/,$attrib_str);
    $attr = ();
    foreach my $attrib (@attribs) {
        $attrib =~ /^(.*):(.*)$/;
        $attr_name = $1;
        $attr_type = $2;
        $attribHash = {
            NAME => $attr_name,
            TYPE => $attr_type
        };
        push @{$attr}, $attribHash;
    }

    my $function = {
        ATTRIBS       => $attr,
        COMMENTS      => $lastComments
    };
    $lastComments = ();
    return $function;
}

sub parseCreateFunction
{
    ($line) = @_;
    $line =~ s/ //g;
    $line =~ /^([\w]*)[\W]*\((.*)\)[\W]*:[\W]*(.*)$/;
    my $name = $1;
    my $attrib_str = $2;
    my $retval = $3;
    
    $attrib_str =~ s/ //g;
    @attribs = split(/,/,$attrib_str);
    $attr = ();
    foreach my $attrib (@attribs) {
        $attrib =~ /^(.*):(.*)$/;
        $attr_name = $1;
        $attr_type = $2;
        $attribHash = {
            NAME => $attr_name,
            TYPE => $attr_type
        };
        push @{$attr}, $attribHash;
    }

    my $function = {
        FUNCTION_NAME => $name,
        RETVAL        => $retval,
        ATTRIBS       => $attr,
        VALIDATORS    => parseValidators($line),
        COMMENTS      => $lastComments
    };
    $lastComments = ();
    return $function;
}



sub parseComment 
{
    ($line) = @_;
    push @{$lastComments},$line;
}

sub parseValidators 
{
    ($line) = @_;
    my @content = ();
    if($line =~ /\{(.*)\}/ ) {
        my $val = $1;
        print $line . " => " . $val . "\n";
        @content = split(/,/,$val);
    }
    return \@content;
}

sub parseCreateAttribute
{
    ($line) = @_;
    $line =~ s/ //g;
    
    $line =~ /^([\w]*):([\w]*)/;
    $attr_name = $1;
    $attr_type = $2;
    $attribHash = {
        NAME       => $attr_name,
        TYPE       => $attr_type,
        VALIDATORS => parseValidators($line),
        COMMENTS   => $lastComments
    };
    $lastComments = ();
    return $attribHash;
}

sub parseCreateClass 
{
    ($line) = @_;
    pushCurrentClass();
    $cClass = { 
        CLASSNAME            => '',

        PUBLIC_CTOR          => [ ],
        PRIVATE_CTOR         => [ ],
        PROTECTED_CTOR       => [ ],

        PUBLIC_FUNCTIONS     => [ ],
        PRIVATE_FUNCTIONS    => [ ],
        PROTECTED_FUNCTIONS  => [ ],

        PUBLIC_ATTRIBUTES    => [ ],
        PRIVATE_ATTRIBUTES   => [ ],
        PROTECTED_ATTRIBUTES => [ ]
    };

    $cClass->{ 'CLASSNAME' } = $line;
}

sub pushCurrentClass
{
    if ($cClass != 0)
    {
        push @classes,$cClass;
    }
    else {
    }
}

sub dumpValidators 
{
    ($val) = @_;
    if ( @{$val} != 0 )
    {
        my $s = "<<@{$val}>>";
        $s =~ s/ /:/g;
        return " " .$s
    }
    return "";
}

sub dumpFunctions 
{
    @funcs = @_;
    
    foreach my $func (@funcs) 
    {
        $attrib_str = "";
        my $setComma = 0;
        foreach my $attrib (@{$func->{ ATTRIBS }})
        {
            $attrib_str .="," if ($setComma == 1);
                
            $attrib_str .= " " . $attrib->{NAME} . ":" .
                $attrib->{TYPE};
            $setComma = 1;
        }
        print "    " . $func->{ FUNCTION_NAME } . " (";
        print $attrib_str;
        print " ) : " . $func->{ RETVAL } . dumpValidators($func->{VALIDATORS}) . "\n";
    }
}

sub dumpAttribs
{
    @attribs = @_;
    
    foreach my $attr (@attribs) 
    {
        print "    " . $attr->{ NAME } . " : " . $attr->{ TYPE } . dumpValidators($attr->{VALIDATORS}) ."\n";
    }
}

sub printList 
{
    foreach my $class (@classes)
    {
        my $className = $class->{ CLASSNAME };
        print "Class description for $className:\n";
        if(@{$cClass->{ PUBLIC_FUNCTIONS }} != 0)
        {
            print "  Public functions:\n";
            dumpFunctions @{$cClass->{ PUBLIC_FUNCTIONS }};
        }
        if(@{$cClass->{ PROTECTED_FUNCTIONS }} != 0)
        {
            print "  Protected functions:\n";
            dumpFunctions @{$cClass->{ PROTECTED_FUNCTIONS }};
        }

        if(@{$cClass->{ PRIVATE_FUNCTIONS }} != 0)
        {
            print "  Private functions:\n";
            dumpFunctions @{$cClass->{ PRIVATE_FUNCTIONS }};
        }

        if ( @{$cClass->{ PUBLIC_ATTRIBUTES }} != 0 )
        {
            print "  Public attributes:\n";
            dumpAttribs @{$cClass->{ PUBLIC_ATTRIBUTES }};
        }
        if ( @{$cClass->{ PRIVATE_ATTRIBUTES }} != 0 )
        {
            print "  Private attributes:\n";
            dumpAttribs @{$cClass->{ PRIVATE_ATTRIBUTES }};
        }
    }
}


loadFile $ARGV[0];
printList


