# Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

SHELL = @SHELL@

subdir = report

srcdir = @srcdir@
top_srcdir = @top_srcdir@
VPATH = @srcdir@
prefix = @prefix@
exec_prefix = @exec_prefix@

bindir = @bindir@
sbindir = @sbindir@
libexecdir = @libexecdir@
datadir = @datadir@
sysconfdir = @sysconfdir@
sharedstatedir = @sharedstatedir@
localstatedir = @localstatedir@
libdir = @libdir@
infodir = @infodir@
mandir = @mandir@
includedir = @includedir@

top_builddir = ..

tex_builddir = .build

ACLOCAL = @ACLOCAL@
AUTOCONF = @AUTOCONF@
AUTOMAKE = @AUTOMAKE@
AUTOHEADER = @AUTOHEADER@

DVI2PDF = @DVI2PDF@

org_tex_files:=	${wildcard ${srcdir}/*.tex}
tex_files:=	$(subst ${srcdir}/,${tex_builddir}/,${org_tex_files})
fig_files:=	${wildcard ${srcdir}/fig/*.fig}
pdf_src_files:=	${wildcard ${srcdir}/pdf/*.pdf}
png_src_files:= ${wildcard ${srcdir}/png/*.png}
png_files:=     ${subst ${srcdir}/png/,${tex_builddir}/,${png_src_files}}

pdf_files:=	$(subst ${srcdir}/fig/,${tex_builddir}/,${fig_files:.fig=.pdf}) \
		$(subst ${srcdir}/pdf/,${tex_builddir}/,${pdf_src_files})

pdf_targets:=	${subst ${srcdir}/,,${tex_files:.tex=.pdf}}

distdir = $(top_builddir)/$(PACKAGE)-$(VERSION)/$(subdir)

datadir = @datadir@
datarootdir = @datarootdir@
docdir = @docdir@
dvidir = @dvidir@
PDFLATEX = @pdflatex@
BIBTEX = @bibtex@

foreingdir=/home/tobibobi/WWWpublic/studier/prc

# MAKEFLAGS += --no-print-directory
DVI_VIEWER=	xdvi

# .SUFFIXES: .tex .pdf .fig .bib
.PHONY: pdf clean publish test
.SECONDARY: ${pdf_files} ${png_files} ${tex_files} ${tex_builddir}/references.bib
ifdef V
  ifeq ("$(origin V)", "command line")
    KBUILD_VERBOSE = $(V)
  endif
endif
ifndef KBUILD_VERBOSE
  KBUILD_VERBOSE = 0
endif
ifeq ($(KBUILD_VERBOSE),0)
  define Q
    @
  endef
  define SILENT
    >/dev/null 2>/dev/null
  endef
  LATEXFLAGS= --interaction batchmode
else
  define USILENT
    >/dev/null 2>/dev/null
  endef
  LATEXFLAGS=
endif

all: Makefile ${tex_builddir}/rapport.pdf

ifeq ($(SHELL),/bin/bash)
  CL=\33[1m
  CLW=\33[1;31m
  CLK=\33[0m
  CLM=\33[4m
  ECHO=echo -e
else
  CL=
  CLM=
  CLW=
  CLK=
  ECHO=echo
endif

help: Makefile
	@${ECHO} "${CLM}Tilg�ngelige \"targets\"${CLK}"
	@${ECHO} " * ${CL}pdf${CLK}::         Generer PDF dokumenter af alle tex filer og viser index.pdf"
	@${ECHO} " * ${CL}dvi${CLK}::         Generer DVI dokumenter af alle tex filer og viser index.dvi"
	@${ECHO} " * ${CL}print${CLK}::       Genererer en postscript fil til udprintning"
	@${ECHO} "                 optionen PRINTER bestemmer m�lprinter"
	@${ECHO} " * ${CL}doc${CLK}::         Generer alle DVI og PDF filer"
	@${ECHO} " * ${CL}all${CLK}::         Alle dokumenter samt java compile"
	@${ECHO} " * ${CL}clean${CLK}::       Ryder op i alle genererede filer"
	@${ECHO} " * ${CL}clean-all${CLK}::   Ryder op i alle genererede filer samt stylesheets"
	@${ECHO} " * ${CL}zip${CLK}::         Zipper alle LaTeX, DVI og PDF filer, ned i \$$ZIP_TARGET" 
	@${ECHO} " * ${CL}publish${CLK}::     Kopierer alle PDF dokumenter samt ZIP filer op p� serveren"
	@${ECHO} " * ${CL}help${CLK}::        Denne lille hj�lp som er standard target"
	@${ECHO} ""
	@${ECHO} "${CLM}Noter:${CLK}"
	@${ECHO} " * Makefilen bliver nu genereret af GNU Automake og GNU Autoconf."
	@${ECHO} "   I den forbindelse kan man hvis man f�r st�rre problemer, udf�re"
	@${ECHO} "   f�lgende: (bare copy/paste)"
	@${ECHO} "     cvs upd"
	@${ECHO} "     (cd ..;./bootstrap.sh;./configure)"
	@${ECHO} "     make"
	@${ECHO} ""
	@${ECHO} "Optioner:"
	@${ECHO} " * ${CL}DVI_VIEWER${CLK}::  DVI fremviser som man �nsker at anvende (${DVI_VIEWER})"
	@${ECHO} " * ${CL}PDF_VIEWER${CLK}::  PDF fremviser som man �nsker at anvende"
	@${ECHO} " * ${CL}USEPDFLATEX${CLK}:: Bestemmer om pdf filer skal genereres med latex eller pdf"
	@${ECHO} " * ${CL}ZIP_TARGET${CLK}::  Navn p� den endelige zipfil (${ZIP_TARGET})"
	@${ECHO} " * ${CL}V${CLK}::           Niveau af verbosity. Normalt =0 men =1 giver fuld dump"
	@${ECHO} ""
	@${ECHO} ""
	@${ECHO} "-- Makefile udarbejdet af \e[4mtobibobi@mip.sdu.dk\e[0m --"

Makefile: $(srcdir)/Makefile.in $(top_builddir)/config.status
	@${ECHO} " ${CL}update${CLK}   $@... "; ${SILENT}
	${Q}cd $(top_builddir) \
	  && CONFIG_FILES=$(subdir)/$@ CONFIG_HEADERS= $(SHELL) ./config.status

rapport.pdf:${tex_builddir}/rapport.pdf
	${Q}cp $< $@

pdf: rapport.pdf Makefile
ifdef PDF_VIEWER
	@${ECHO} " ${CL}SPECIAL->${CLK}rapport.pdf" ${USILENT}
	${Q}${PDF_VIEWER} rapport.pdf
else
	@if type acroread >/dev/null; \
	then \
		echo "acroread ${tex_builddir}/rapport.pdf" ${SILENT}; \
		${ECHO} " ${CL}acrobat${CLK}   rapport.pdf" ${USILENT}; \
		acroread rapport.pdf; \
	else \
		echo "xpdf rapport.pdf" ${SILENT}; \
		${ECHO} " ${CL}xpdf${CLK}      rapport.pdf" ${USILENT}; \
		xpdf rapport.pdf; \
	fi &
	${Q}sleep 1
endif

clean: Makefile
	@${ECHO} " ${CLW}clean${CLK}    ${RELDIR}" $(USILENT)
	${Q}${RM} ${pdf_target_files}
	${Q}${RM} -rf ${tex_builddir}
	${Q}${RM} ${ZIP_TARGET}

clean-all: Makefile
	@${ECHO} " ${CLW}cleanall${CLK} ${RELDIR}" $(USILENT)
	${Q}${MAKE} clean

# this is for autoconf implementation
distclean:clean-all Makefile # target needed for use with autoconf

install installcheck installcheck-am:
	@:

distdir: Makefile
	mkdir -p ${distdir}
	cp *.tex *.bib *.tx Makefile.in ${distdir}
	mkdir -p ${distdir}/fig
	mkdir -p ${distdir}/pdf
	cp fig/*.fig ${distdir}/fig
	cp eps/*.pdf ${distdir}/pdf

check: Makefile

maintainer-clean: distclean
	rm -rf Makefile

#end of autoconf implementation

${tex_builddir}/%.pdf : \
	${tex_builddir}/%.tex \
	${tex_builddir}/references.bib \
	${pdf_files} \
	${png_files} \
	${tex_files} \
	${tex_builddir}/tobibobi.sty \
	Makefile

#	${Q}cat ${srcdir}/template.tx|sed "s/<input>/\\\\input{tmp.tx}/" > ${tex_builddir}/temp.texi
#	@echo " latex 1 $@" $(USILENT)
#	${Q}set -e; (cd ${tex_builddir};latex ${LATEXFLAGS} temp.texi) ${SILENT}
	@${ECHO} " ${CL}pdflatex 1${CLK}  $@" $(USILENT)
	${Q}set -e; (cd ${tex_builddir}; \
		if ! ${PDFLATEX} ${LATEXFLAGS} ${<F}  ${SILENT}; then \
		echo "FAILED to pdflatex `pwd` ${<F}"; \
		rm ${@F}; exit -1; \
	fi)
	@${ECHO} " ${CL}bibtex${CLK}   $@" $(USILENT)
	${Q}set -e; (cd ${tex_builddir};${BIBTEX} ${<F:.tex=}) ${SILENT}
	@${ECHO} " ${CL}pdflatex 2${CLK}  $@" $(USILENT)
	${Q}set -e; (cd ${tex_builddir};${PDFLATEX} ${LATEXFLAGS} ${<F}) ${SILENT}
	@${ECHO} " ${CL}pdflatex 3${CLK}  $@" $(USILENT)
	${Q}set -e; (cd ${tex_builddir};${PDFLATEX} ${LATEXFLAGS} ${<F}) ${SILENT}
	@${ECHO} " ${CL}pdflatex 4${CLK}  $@" $(USILENT)
	${Q}set -e; if test x${REFERENCES} == x1; then \
		(cd ${tex_builddir} && ${PDFLATEX} ${<F} > dump); \
		((cd ${tex_builddir} && grep Ref dump) || true); \
	else \
		(cd ${tex_builddir};${PDFLATEX} ${LATEXFLAGS} ${<F}) ${SILENT}; \
	fi
	${Q}rm -f ${tex_builddir}/$@
	${Q}rm -f ${tex_builddir}/temp.texi
	${Q}rm -f ${tex_builddir}/*.aux
	${Q}rm -f ${tex_builddir}/*.log
	${Q}rm -f ${tex_builddir}/*.toc
#	${Q}mv ${tex_builddir}/temp.pdf $@

${tex_builddir}/%.tex : ${srcdir}/%.tex
	@${ECHO} " ${CL}cp${CLK}       $< -> $@" ${USILENT}
	${Q}mkdir -p ${tex_builddir}
	${Q} cp $< $@

${tex_builddir}/%.bib : ${srcdir}/%.bib
	@${ECHO} " ${CL}cp${CLK}       $< -> $@" ${USILENT}
	${Q}mkdir -p ${tex_builddir}
	${Q}cp $< $@


${tex_builddir}/%.pdf:${srcdir}/pdf/%.pdf
	@${ECHO} " ${CL}cp${CLK}       $< -> $@" $(USILENT)
	${Q}mkdir -p ${tex_builddir}
	${Q}cp -f $< $@

${tex_builddir}/%.png:${srcdir}/png/%.png
	@${ECHO} " ${CL}cp${CLK}       $< -> $@" $(USILENT)
	${Q}mkdir -p ${tex_builddir}
	${Q}cp -f $< $@

${tex_builddir}/%.pdf:${srcdir}/fig/%.fig
	@${ECHO} " ${CL}fig2pdf${CLK}  $@" $(USILENT)
	${Q}mkdir -p ${tex_builddir}
	${Q}fig2dev -L pdftex $< $@ $(SILENT)
	${Q}fig2dev -L pdftex_t -p ${@F} $< ${@:.pdf=.pdftex_t} $(SILENT)

${tex_builddir}/tobibobi.sty:tobibobi.sty
	@${ECHO} " ${CL}cp [sty]${CLK} $@" $(USILENT)
	${Q}mkdir -p ${tex_builddir}
	${Q}cp tobibobi.sty $@

${dvi_files}: \
	${eps_files} \
	${tex_files} \
	${tex_builddir}/references.bib \
	${tex_builddir}/tobibobi.sty \
	${srcdir}/template.tx 


ttest:
	@echo ${png_files}