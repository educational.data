\documentclass[twoside, pdftex,a4paper,11pt,twosided]{report}
\usepackage[pdftex]{graphicx,color}
\usepackage{rotating}
\usepackage{varioref}
\usepackage{here}
\usepackage{fancyheadings}
\usepackage{amsmath}
\usepackage{float,times}
\usepackage{a4wide}
\usepackage{psboxit}
\usepackage{subfigure}
\usepackage[pdftex,colorlinks=true,
                      pdfstartview=FitV,
                      linkcolor=blue,
                      citecolor=blue,
                      urlcolor=blue,
          ]{hyperref}
          \pdfinfo{
            /Title      (AIPP01: UAV path planner)
            /Author     (Tobias Nielsen)
            /Keywords   (Unmanned Airborn Vehicle)
          }

\PScommands
\newenvironment{graybox}{\begin{boxitpara}{box 0.7 setgray fill}}{\end{boxitpara}}

\title{AIPP01 2007\\ UAV path planner}
\author{Peter Tobias G\o nge Nielsen\\email: tobibobi@gmail.com}
\date\today
\floatstyle{ruled}
\newfloat{Program}{thp}{lop}[section]


\begin{document}
\maketitle
\pagestyle{empty}

\begin{abstract}
  This report describes the implementation of a evolutionary planner
  for a flying drone. The purpose of the planner is to allow the drone
  to find its way through a dynamic environment. The method is based
  on genetic algorithms to optimize a randomly picked solution. This
  method is then repeated as the plane progress through the
  environment and from there updated to a good solution.
\end{abstract}

\tableofcontents
\listoffigures
\pagestyle{headings}
\newpage

\parindent=0pt
\parskip=0.5\baselineskip

\chapter {Description}
%\section{Common definitions of a UAV}
\label{sec:definition}
The acronym for \emph{UAV} are numerous, but some of the known ones
and most widely accepted versions is \emph{Unmanned Aerial Vehicle},
or \emph{Unmanned Airborne Vehicle}. Even
though the definitions slightly differ, the understanding is the same.
A quick search on the term \emph{UAV} on \emph{google} yields the
following definitions:
\begin{graybox}
\begin{description}
\item[Unmanned Aerial Vehicle] A powered aerial vehicle sustained in
flight by aerodynamic lift and guided without an on-board crew. It may
be expendable or recoverable, and can fly autonomously or be piloted
remotely. There are two categories of UAVs: drones and RPVs.\\
\href{http://www.rcmicroflight.com/library/glossary.asp}
     {www.rcmicroflight.com/library/glossary.asp}

\item[An Unmanned Aerial Vehicle] (UAV), also called a drone, is a
self-descriptive term used by the US military, the Israeli Defense
Forces and others to describe the latest generations of pilot-less
aircraft. Taken literally, the term could describe anything from
kites, through hobbyist radio-controlled aircraft, to cruise missiles
from the V-1 Flying Bomb onwards, but in the military parlance is
restricted to reusable heavier-than-air craft.\\
\href{http://en.wikipedia.org/wiki/UAV} {en.wikipedia.org/wiki/UAV}.
\end{description}
\end{graybox}

There may exists many different types of \emph{UAV} (which is the way
i will refer to it from now on), but they are mostly divided in to the
groups ``fixed wind'' and ``rotary wings''. In the rest of this proposal i
will be referring to a ``fixed wing'' version when I use the term
\emph{UAV}.

\section{Problem description}
An intelligent system that is able to control a UAV, may consists of
many parts. There may at the very top be a strategic planner which
chooses some high-level plans like for instance deciding to fly to the
next way-point, staying at a location since a task is still not
complete or perhaps voiding all presents task and return to base since
the fuel state is perhaps to low for the UAV to continue. This may
off course differ quite a lot depending on the demands that is put on
the UAV. A simple example of a drone is where it is used by the navy
for target practice (The story appeared in a old number of
``Modelflyve nyt'' from mid nineties - but the specific issue is lost
from my collection, so I can only tell the story as I recall it).  In
this example the robots are remotely controlled or more precisely
directed by some very simple methods to pass through an area where the
different aggressors will try to shoot upon the drone. In this situation
the drone is not using a sophisticated complex engine to decide the
path flown - only a course is sent to the drone which is being tracked
by some simple gyro hardware.

\paragraph{The path}

\begin{figure}[ht]
\centering\resizebox{0.7\linewidth}{!}{\input arcsandlines.pdftex_t}
\caption{A route consists of a the basic elements lines and arcs}
\label{fig:arcsandlines}
\end{figure}


Planning a course from $a$ to $b$ can be quite complex since there
might exists a few obstacles along the path in the form of weather
systems that makes flying difficult and fuel consuming, there may be
prohibited airspace's enforced by different airspace classifications (
\href{http://da.wikipedia.org/wiki/Luftrumsklasse}
{da.wikipedia.org/wiki/Luftrumsklasse}
{http://da.wikipedia.org/wiki/Luftrumsklasse}) and simply airspace
traffic. A special case is the controlled airspace where the drone is
forced to follow directions laid out by a human flight controller -
but this can be seen as a quite different planning problem.

Another issue is the fact that a \emph{UAV} is always moving and
therefore cannot perform any sharp turns. A course therefore is
described as a list consisting of straight lines and arcs with some
minimum radius like visualized in figure \vref{fig:arcsandlines}.

For simplicity this can be separated in to two categories:
\begin{itemize}
\item Completely restricted airspace.
\item Airspace with a higher flight cost. These can again be divided
  in to a airspace with some specific flight restrictions or a
  weather system that is quite costly in terms of fuel and time
  consumption.
\end{itemize}

\subsection{Focus}
The focus for this project so far is to make a path planner that
chooses a path using evolutionary algorithms, in an airspace with
different kinds of obstructions. In this report I simplify this by
giving the planner some fixed kind of obstacles to avoid.

The plane is a moving item (and in this report, it is running at fixed
speed), therefore it cannot allow the algorithm to run forever in
order to find an extremely optimized solution. The amount of runs
available to run the evolutionary algorithm is therefore limited to
only 40 generations at which an useful solution \emph{must} exists.


As the plane fly through an area with fixed obstacles, the decision of
the path is relatively easy since the position is know at any point in
time. But in the case where the objects would not be static, the
problem of finding a plausible position far in to the future, can be
problematic. Therefore the development of a path is repeated at
intervals in order to adapt to any changes in the position of the
elements in the environment. This is off course also an idealistic
approach since its not expected that the sensory system will
accurately be able to decide the position of all the objects in the
environment - its actually very likely that it will miss some.

Therefore the next goal for this project is to adapt it to a more
dynamic environment where both moving and fixed position objects
appears in the airspace. The moving objects may again appear in two
general forms, one which heading and speed is fixed and one that may
move arbitrarily around. i do though, not expect this to be reached
within the confines of this report.


The first is basically the easiest to prepare for since it can be said
with a rather high precision where the object will be at a certain
points all the time.  In the second case it may be a lot more
difficult since as the object travels in time it may reach a point
further away from its beginning location. This point in the future is
however not easy to decide since the object could decide to change its
current heading to directions completely unknown to the UAV. Therefore
the position of the object is visualized as a proximity sector wherein
the object will be present at some unknown location. As we try to
foresee further and further in to the future, the size of this sector
grows so large that they will cover the entire area.


I hope to be able to solve for the fixed heading and speed kind of
object before the end this project. Secondary if the time permits, i
will try to expand this to a more dynamic environment.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{The platform}
%\section{Description of the platform} 
\label{sec:platform}
In order to fully understand the implementation for the planner, it is
necessary to get a basic idea of how the control system could be
build.  It is a very common method to use a high level of parallel
computing in order to achieve different sub task in a robotic
agent. This aids the design of the controller since each isolated task
is a function that is able to look at the total problem from an
isolated perspective. This and the sum of all the other functions
leads to the total behavior of the robotic agent and is able to reach
highly complex functionality. One of the most famous authors in this
fields is Rodney Brooks that in his article \cite{BrooksLayered}
describes how it can be implemented using his proposed subsumption
architecture. This idea is based on the rule that every sub behavior
should be able to take a direct input and produce a direct
output. This yields a very fast response time which is highly
interesting when we are talking about real-time systems. But the
behaviors is also solely based on the idea that each task should be
as simple as possible and therefore able to provide a response
directly based on its input.

I suggest this project to follow a similar structure where a lot of
the task is performed in a parallel fashion and by that I am actually
referring to directly separate processes providing true parallelism in
execution. 

This architecture is needed since some of the tasks involved is of
such a nature where they will need offline execution that would halt
the rest of the controller system if it was to be conducted in series
with other task that needed fast response times.

Architectures exists that supports this type of execution and is based
on the idea that at the very button of the system, there exists task that
has true real-time performance providing precise response times in a
time domain that is based on regulation and resonance frequencies. At
the highest level tasks exist with almost no time constraints that is
able to perform lengthy planning processes.

In this architecture we find the planner at the very top - but with
the difference that I allow the planner to be time or length
constrained. This is due to the fact that the planner I am using is
in fact creating a plan quite quickly - its just not truly optimized
yet.

The planner sends its current plan at suitable times steps to a lover
level that is able to extract the plan information and put it to
execution by the rest of the system.


\section{Idea behind the algorithm}\label{sec:idea}
In order to represent a path, its necessary to make a quick review of
how an UAV is able to perform its movements. As described in the
\vref{sec:definition}, I have restricted the space movement to be of a
two dimensional order where the world is seen from above.  A fixed
wing UAV is not able to stop in its path compared to a rotary wing
(heli) UAV. Therefore the UAV is limited to move by only two methods:
a straight path between two points where the direction at the two
points is the same as the vector that is stretched out between the two
points; to move in circles where the start heading is the same as the
exit heading of the previous segment and similar in the end. The
heading maps equivalent to the start heading of the next element.

These segments is gathered in a sequence which forms a complete path
from start to end point.

In order to create a useful path, a genetic algorithm generates a
population of random paths that is optimized continuously. The
execution path of the control system is feed with the currently best
path at some predefined time steps. This method ensures that the robot
always has the best solution possible which off course in the
beginning is a very crude solution - but in most cases the selected
path will at least be in the right direction of the target.

The algorithm is always trying to look a very far distance in to the
future, but the path that is fed to the execution system, is always
only the closest path segments.

Static objects is off course a part of the obstacles in the area, and
therefore the algorithm tries to find the most optimal path around the
objects. Another class of objects is the dynamic objects which most
likely will be a another plane crossing the path that the UAV is
trying to fly through. This adds a certain amount of complexity since
it may be unknown if the plane continues its path through the or if
chooses to alter its path at some point or even tries to target you.
In the situation where the plane is moving at a fixed speed with fixed
direction, its easy to depict the position at all times which means
that the UAV in principle can evolve a path that passes close the
other plane. But in the case where the plane is moving in some unknown
manner - maybe even hostile, a more loose prediction model must be
established. This gives a large area where the opposing plane may
exists within and therefore a larger area that the UAV have to fly
around.

\section{Representing the path}
As mentioned in the section \vref{sec:idea}, the constructed path
consist of curve segments. Each path segment is an relative
vector. Therefore in order to find the absolute position for a path
segment, one must start at the start point and then summarize these
vectors.  Furthermore each segment also has a start and an end vector
that indicated the heading in each of the two ends of the path vector.

In order for the plane to follow a path, one must remember that no
turn can at any point be performed at an instant. The turn factor for
the plane has to be accommodated - and that; is off course not
infinite. As I have no specific kind of airplane type that I try to
solve the problem for, the problem is approached in a somewhat generic
way.

\cite[parallelEA1] suggests that a curve can be build in two different
ways, dependent on the situation. These two situations both describes
solving the curve with two curves that intersects the segment at both
ends having a tangent at these points being respectively entry heading
and exit heading. This two curves should off course also intersect
each other so that the transition from traveling on curve to the next
will be smooth.

A quick study of this shows that there may exists two different
scenarios as told by \cite{parallelEA1}: One with both curve centers
on the opposite side of the segment and one with the centers on the
same side.

\subsection{Building of segment type 1}
\begin{figure}[ht]
\centering\resizebox{0.7\linewidth}{!}{\input arctype1.pdftex_t}
\caption{The setup of a single section with curve segments on opposite
  sides of the base segment line.}\label{fig:curve1}
\end{figure}

In figure \vref{fig:curve1}, I have presented the graphical solution
to the second problem. Here one can see that a path is possible by
placing a curve segment on either side of the connecting line
$\vec{l}$ and the center of the arcs with center in $C_1$ and $C_2$
can be found by solving for the length $R_1 = R_2 = {R_3 \over 2}$

This yields for the following description of the problem:
\[
(0,0) = k - 2 R \widehat{k} \qquad k = R(\hat{r_1} + \hat{r_2}) + \vec{l}
\]
If I insert $\vec{l} = (x_3,y_3)$, $\vec{r_1} = (x_1,y_1)$ and
$\vec{r_2} = (x_2,y_2)$ the system will expand to this:
\[
(0,0)^T = \begin{pmatrix} R \left(x_1+x_2\right)+x_3-\frac{2 R \left(R
  \left(x_1+x_2\right)+x_3\right)}{\sqrt{\left(R
  \left(x_1+x_2\right)+x_3\right)^2+\left(R
  \left(y_1+y_2\right)+y_3\right)^2}} \\ R
  \left(y_1+y_2\right)+y_3-\frac{2 R \left(R
  \left(y_1+y_2\right)+y_3\right)}{\sqrt{\left(R
  \left(x_1+x_2\right)+x_3\right)^2+\left(R
  \left(y_1+y_2\right)+y_3\right)^2}}
  \end{pmatrix}
  \]

When I solve this system with respect for $R$ (using
\emph{mathematica} to solve it for me) I get the following solution:
\[
R = \frac{m\pm\sqrt{i^2-4 j \left(x_3^2+y_3^2\right)}}{2 j}
\]
where
\begin{align*}
i &= 2 x_1 x_3+2 x_2 x_3+2 y_1 y_3+2 y_2 y_3\\
j &= x_1^2+2 x_2 x_1+x_2^2+y_1^2+y_2^2+2 y_1 y_2-4\\
m &= -2 x_1 x_3-2 x_2 x_3-2 y_1 y_3-2 y_2 y_3
\end{align*}

This gives me a plausible solution for finding the length of $R$ and
now is the task of finding the angles for the arc a simple matter of
reconstructing the vectors $r_1,r_2$ and $r_3$. 

\subsection{Building of segment type 2}
\begin{figure}[ht]
\centering\resizebox{0.7\linewidth}{!}{\input arctype2.pdftex_t}
\caption{The setup of a single section with curve segments on the same
  side of the base segment line.}\label{fig:curve2}
\end{figure}

In this segment piece the idea is to realize a vector $\vec{v_2}$ from
$\vec{v_1}$ and $\vec{l}$. This vector is made by adding the unit
vectors of each. The same is done for the vector $\vec{w_2}$. The
length of the piece $\left|\vec{v_2}\right|$ and
$\left|\vec{w_2}\right|$ is found by find the point where the two
vectors intersects.

The resulting length of the vectors is then named $a$ and $b$.

This information is assembled to a equation:
\[
(0,0) = a \hat{v_2} + b \hat{w_2} + \vec{l}
\]

If i expand this to a coordinate form, the result looks like:
\[
(0,0)=\left(a x_1-b x_2+x_3,a y_1-b y_2+y_3\right)
\]

If i let mathematica solve this, i get the result:
\begin{equation}
\left\{a\to -\frac{x_3 y_2-x_2 y_3}{x_1 y_2-x_2 y_1},b\to -\frac{x_3
   y_1-x_1 y_3}{x_2 y_1-x_1 y_2}\right\} \label{eq:a_len}
\end{equation}

From this intersection point ($C_1$), I can now draw a line in space
that is perpendicular to $\vec{l}$ and find the intersection points
with the transpose of the vectors $\vec{v_1}$ and $\vec{w_1}$. With
the same approach as above in equation \ref{eq:a_len}, I am able to
find the points $C_2$ and $C_3$ which is the center points for the two
arcs that connects the endpoints off the segment.



\section{The general framework for building the
  path}\label{sec:general_framework}
\begin{figure}[ht]
\centering\resizebox{\linewidth}{!}{\input evolutionary.pdftex_t}
\caption{The engine that is evaluating the system for the
path}\label{fig:evaluator}
\end{figure}

The genetic algorithm as I have seen it, hosts collection of
populations of paths (genomes). In order to do this the evaluator
needs three important parts.

\begin{description}
\item[Mutation] Mutation of a typical genome is normally done in a
  genome by shifting the individual bits. In this situation I am using
  a Genetic Programming method which means that the mutation is quite
  a bit more complex. Here the path is altered by either expanding,
  shrinking, altering direction at an arbitrary point or simply moving
  one point or two points so that either the entire path is offset or
  only a minor part of it.  In addition to mutation, the individual
  paths is also victim of a crossover so that the genome will be mixed
  with another genome.

\item[Evaluation] After the mutation of the paths an evaluation will
  be performed according to a series of constraints. These constraints
  is in is basic form based on the length of the path forcing the
  system to reach as straight a line as possible. Next the path is
  also evaluated against the environment. This specific part is one of
  the places where a lot of extra parameters can be added in, like
  wind drift, altitude (if we where to include the third dimension)
  The environment currently only includes distance measurement to
  static objects in the environment.  Next is there the Plane
  parameter's where the system currently includes a system to avoid to
  sharp turns with to narrow turn radius.

\item[Selection] After the fitness function has rated the quality of
  the different paths, Its up to the selector to sort and make a
  reasonable selection of the best paths available. The method used is
  based on filling a list with ids to the paths. The higher rating a
  path gets the more entries it has in the list. Then when we randomly
  pick an id from the list, its most likely that we grab a path with a
  high fitness rate - but its not impossible to grab a bad path, its
  just statistically hard.
\end{description}

The whole process of selection, evaluation and mutation is visualized
in figure \vref{fig:evaluator}.

\subsection{Mutation}
The mutation is based on the ideas proposed in
\cite{parallelEA1,parallelEA2} who proposes either a 1 point and a 2
point mutation method. In my system this is used in their basic form
plus a few additions like the ability to rotate a the entire path from
referenced from one random point in the path.

\paragraph{1 point mutation}
In this simple mutation form a single point in the path is selected
and moved relative to its previous node. The point is offset relative
to its old position so that there is a high probability that the
system is still able to execute. In this basic setup, the reset of the
path is similarly offset by the same amount that this single point is
moved. This is due to the fact that each point is described relatively
to the last point.

The 1. point mutation can also be performed in a different way where
I instead of shifting the point, choose to rotate the remainder of the
path around this point. 

\paragraph{2 point mutation}
Instead of using a single point which will provoke a movement of the
rest of the path, a two point mutation based on the one point mutation
is applied in such a way that the second segment is offset similarly
in the opposite direction so that the point will effectively be
staying at the same point.

\paragraph{Mutate to end}
At some point the path should really try to reach an end state that
ultimately reaches the end point in the system. Instead of relying on
the algorithm so find a solution that provides an end position close
enough to the end pose, I have provided this mutate to end pose
function. 

\paragraph{Mutate expand}
The last of the mutate methods is the expand method. At the beginning
all paths consists of relatively few reference points.
Slowly these points are expanded until they reach a maximum length.
Furthermore the function also removes arbitrary points in an attempt
to reduce the complexity of the system as much as possible.

\paragraph{Crossover}
The crossover is done as a one point crossover. The resulting path is
constructed so that the absolute remaining path is not altered and
therefore is not shifted in position.



\subsection{Evaluation}
In my architecture, the Evaluation is a task that includes evaluation
of a large sum of factors. As described in
\vref{sec:general_framework}, the factors are the environment, vehicle
constraints, goals and constraints. The different factors are
summarizing a penalty which means that the best fitness is the
lowest. This gives the resemblance of a potential field where the path
tries to reach as low an potential as possible.

\paragraph{The environment}
Currently the only represented thing in the environment is fixed
static objects. The collection of the different obstacles is specified
with an XML file so different solutions can be tested easily.

In order to give an estimation of the distance, each line segment is
evaluated for the shortest possible distance to each of the static
objects that the UAV is to evade. In program snip
\vref{prog:fitness_to_point}, It is shown how the operation is
accomplished. 

\begin{Program}
\begin{verbatim}
double punishment = 0;

std::vector<Obstacle> obs =
    environment.getObstaclesAtTime(segLen);

for(std::vector<Obstacle>::iterator it = obs.begin();
    it!=obs.end();
    ++it)
{
  double dd = seg.distanceToPoint(offset,it->position);

  if(dd <= it->minDistToTarget)
    punishment += 200;
  else
    punishment += 10/(0.1 + (dd - it->minDistToTarget));
}
return punishment;
\end{verbatim}
\caption{Evaluation of distance to any
objects}\label{prog:fitness_to_point}
\end{Program}

A thing that is interesting is the fact that a path that manage to put
a part of its segments within the forbidden area of the static point,
is punished with a relatively high value. In earlier versions I had
used a method that would discard a path that was violating the
limits. 

This method of not discarding the path gives the algorithm a
possibility to optimize the currently bad solution to a better path
that ultimately doesn't violate any obstacles. The environment
constraints is represented by $F_e$ shown in \vref{eq:fitness}.

\paragraph{Vehicle constraints}
Normally a whole series of constraints that is known from the real
plane would be added in here. This could for instance be a fuel
consumption, min/max speed or acceleration constraints or turn radio
constraints. This is off course highly application specific
information and is not modeled in currently. This would off course be
more interesting when a concrete UAV type was added. The article
\cite{parallelEA2} is describing different approaches in this matter,
but is also not fully representative as it is off course highly
dependent of the capabilities of the single UAV type.  The vehicle
constraints is represented with $F_v$ shown in equation \vref{eq:fitness}.

\paragraph{constrains}
This is where the other constrains come in to play. This is typically
application specific constraints like a constraint that minimizes the
overall path length in order to reach the goal of the operation.  This
is represented by $F_c$ shown in equation \vref{eq:fitness}.

\paragraph{Goals}
In the calculation of the goals, I have mainly focused on the
possibility of a single goal. This is directly based on the paths end
position and its distance to the goal. But this could easily have been
expanded to represent multiple goals where the resulting fitness would
be the distance to the goal.  This is represented by $F_g$ shown in equation
\vref{eq:fitness}.

\paragraph{Summarizing}
The resulting fitness is summarized in the following form:
\begin{equation}
F = \alpha F_g + \beta F_c + \gamma F_v + \lambda F_e
\label{eq:fitness}
\end{equation}
where $\alpha, \beta, \gamma$ and $\lambda$ off course are weights for
the individual constraints. 

\subsection{Selection}
Its is the goal for the selection faze to find the best paths and then
select a proper collection of these for mutation and crossover. There
is several methods available for doing this. I have chosen to
represent all the paths as selectable - but with a statistically higher
chance of selecting a path with a good fitness than a poor fitness
value.

In order to do this, I have a collection of fitness values that
reference the corresponding path. This collection is being sorted in
an decreasing manner so the path with the lowest fitness (the best
fitness) is last. After this, the code snippet in
\vref{program:pool_selection} performs the actual sorting for selection.

\begin{Program}
\begin{verbatim}
for(fitnessIterator fIt = fitness.begin();
    fIt != fitness.end();++fIt) 
{
  for(int i = 0;i < pSize*2;++i) 
  {
    poolOfPaths.push_back(fIt->id);
  }
  ++pSize;
}
\end{verbatim}
\caption{Creating a pool of fitted paths to select
  from}\label{program:pool_selection} 
\end{Program}

The list is created by adding increasingly more and more entries for
each path. This makes sure that there is a higher chance to select a
good path when we make a random selection from \verb!poolOfPaths!







\chapter{Test and evaluation}
%\section{Evaluating the result}
In order to evaluate the project, I have in parallel designed a
graphical program to visualize the result of the algorithm. This
visualization provides a top down view of the algorithm as the path is
resolved and it can therefore be seen how the algorithm converges
towards a solution that yields a plausible good path.

\begin{figure}[ht]
\noindent\begin{minipage}{15.5cm}
\subfigure[One period]{\resizebox{7.75cm}{!}{\input eq1}}
\subfigure[Four periods]{\resizebox{7.75cm}{!}{\input eq2}}
\end{minipage}
\caption{Evaluation of the algorithm. (a) describes one period of 40
  steps which is the number of steps that the algorithm is allowed to
  run before the UAV must execute the result. (b) shows the same
  picture, but includes several such periods. It can be seen here that
  the algorithm slowly performs better and better.}
\label{fig:it_periods}
\end{figure}

But since its impossible to visualize such a graphical sequence here
in paper, i have instead included a dump that visualizes how the
algorithm converges over time towards a plausible solution. First I
have in figure \ref{fig:it_periods} (a) described one iteration period
for the algorithm. As described above, this period is initially
carried out in order to produce a path for the plane to start
executing initially. As can be seen, the algorithm shows convergence
towards a low level rather quickly and in principle we already have a
some what usable solution that will at least bring the UAV close
towards its goal.


As the UAV continues its path, it is at certain intervals allowed to
reevaluate its path. As described at each such interval, the algorithm
starts from a new randomized generation, but injected with the best
solution from the last evaluation period. This gives us the graph
shown in figure \ref{fig:it_periods} (b). Here it can be seen how the
algorithm is being re-initialized every 40 step. There is off course a
distortion of time as the algorithm is also halted every time the
algorithm reaches this point, allowing the plane to execute the chosen
path. It can also be seen that even though the mean fitness for all
the paths jump at this specific time-step, the best possible path
slowly drops and is not affected severely by the reinitialization. I
have a few times seen the best path make a jump down at this step, but
unfortunately i haven't been able to reproduce a graph for this
report.

The reason that it may jump suddenly is because it can start out with
a path that needs to take an unnecessary long path around some
obstacle. Since the algorithm may lock itself into a local minima
area, it may need a rather drastic mutation to get out of this area
again.

It is off course debateable how much value this initialization yields
since the graph shows rather little improvement in the given situation
and it can also be said that its perhaps unnecessary extra work to
perform for little gain. One could perhaps increase the amount of
populations to get out of the mentioned problems above.  But if we
consider a more dynamic scenario, it gives some meaning since a
solution that was by far impossible earlier because of the
uncertainties of the moving objects, has now decreased this
uncertainty area and therefore some completely different solutions
becomes possible since the obstacles has gotten much smaller.

\begin{figure}[H]
\centering
\resizebox{0.45\linewidth}{!}{\includegraphics{one.png}}\qquad
\resizebox{0.45\linewidth}{!}{\includegraphics{Two.png}}\\
\vspace{10pt}
\resizebox{0.45\linewidth}{!}{\includegraphics{Tree.png}}
%\resizebox{0.45\linewidth}{!}{\includegraphics{Four.png}}
\caption{Three steps in execution of the planner algorithm. The
  algorithm begins at the top left where planner step has occurred (40
  GA iterations) The next two is 2 and 3 planner steps. The figure
  shows improvement that the algorithm succeeds in directing the plane
  towards the goal (Lower right black cross)}
\label{fig:evol_steps}
\end{figure}

As to the quality of the fitness function, it is a bit more difficult
to see from the plot if the robot actually gets to the goal. A well
implemented genetic algorithm would exploit every possibility to reach
a minima no matter what the function may be. Therefore its necessary
to take a look directly at some screen dumps of the visualizations to
see if we actually get a path that leads to the goal while avoiding
the obstacles present. An example in four steps is shown in figure
\vref{fig:evol_steps}.

The three steps shown in figure \ref{fig:evol_steps}, reflects from
left to right, top to bottom, the progress as the algorithm
progress. Just to elaborate on what can be seen, the pictures shown
displays a couple of things. First, it shows the path as a series of
straight lines (the black lines). This path is shown simply because
the algorithm that is supposed to translate the path in too smooth
curves, do not work properly. A cross is shown in the locations where
the algorithm has placed the centers of those arcs.  Next and most
important the gray circles illustrate obstacles in the scenario that
the algorithm is supposed to avoid.

Each picture indicates evolution after one major evolution step (this
off course means that the GA has been running for 40 iterations). 

In the beginning (top left) the first step is shown and it can be seen
that the algorithm gives a solution that is a bit rough. But it still
gives a possible solution that directs the plane in the right
direction. But as the algorithm progresses, the path gets more
smooth.

The red arcs present in the view are the result after the arc
calculations. But as mentioned I have only been able to make the first
of the two segment types to actually work properly. Therefore there is
some sporadic arcs appearing around the path.

\section{Future work}
This project has proved some of the basic ideas in the realization of
an evolutionary algorithm for a path planner. So far I have been able
to provide a basic planner framework for this. A lot of work is off
course still to be done in the areas of simulation of the system. My
suggestion is to connect the framework with a system like
\emph{flightgear.org} or \emph{Microsoft Flight simulator} as they are
able to provide a sufficient simulation of the environment that
resembles the environment that a UAV is likely to be placed in. Both
simulators supports adding a flight model that the user provides (and
codes in C++, java or something else). In these environments there can
also be inserted both stationary and dynamic objects.

Here there lies also a task of creating a lower level controller that
will be able to execute the path as described in section
\ref{sec:platform}.

With reference to the mapping it would have been really nice if I
could have imported a map directly in to the program. From here I can
read important fixed flight zones. There exists such public available
files that describes generic flying areas with special markup for
special flight zones. I have in parallel been working on a parser for
these descriptive files - but I have not been able to get it ready for
the deadline. Therefore it is undocumented in this version. Instead I
have a more generic \verb!XML! format that can be read with the
library \verb!libxml! and directly translate in to useful data.

Another thing that would be nice to have, is having the entire path
Cartesian coordinate system transferred in to a spherical coordinate
system.

I have still to make the last part of the curve system work correctly,
but I have at the deadline not been able to complete this part
sufficiently.

\section{Conclusion}
In this project I have implemented and tested an neural network
algorithm for planning the path for an arbitrary UAV. The method seems
to be very promising, but its also evident that to really evaluate the
algorithm, a more complete test with more data on a UAV could be
useful. The second part of this project that I didn't reach, was the
part that would be really interesting since it would have shown the
algorithms real potential with an changing environment.

Therefore its my absolute suggestion to take this project one step
further and try to realize it in some kind of simulation like
\emph{FlightGear.org} (http://www.flightgear.org) In this environment
its a much more complete environment and therefore its evident that a
planner could have been tested more thoroughly.

The documentation available was in general good, but some of the
references that had been noted in \cite{parallelEA1} was not
achievable and therefore the realization of the path is almost
completely by own design since it was only slightly hinted how the
paths could have been build. Therefore I have tried several different
designs of the path algorithms before settling with these methods
documented in this report.

\bibliographystyle{unsrt}
\bibliography{references}
\parskip=\lineskip

\appendix

\end{document}