AC_DEFUN([AC_JNI_HEADER],[
AC_PROVIDE([$0])
AC_MSG_CHECKING([for jni.h])
JNI_INC=;
if test -r /usr/local/packages/j2sdk1.4.1_01/include/jni.h; then
  JNI_INC="-I/usr/local/packages/j2sdk1.4.1_01/include/"
  JNI_INC="$JNI_INC -I/usr/local/packages/j2sdk1.4.1_01/include/solaris"
elif test -r /usr/include/java/jni.h; then
  JNI_INC="-I/usr/include/java"
  if test -d /usr/include/java/linux; then
    JNI_INC="$JNI_INC -I/usr/include/java/linux"
  elif test -d /usr/include/java/solaris; then
    JNI_INC="$JNI_INC -I/usr/include/java/solaris"
  fi
elif test -r /usr/include/jni.h; then
  if test -r /usr/include/linux/jawt.h; then
    JNI_INC="$JNI_INC -I/usr/include/linux"
  elif test -r /usr/include/java/solaris/jawt.h; then
    JNI_INC="$JNI_INC -I/usr/include/solaris"
  fi
elif test -r /usr/local/include/jni.h; then
  JNI_INC="-I/usr/local/include/"
  if test -d /usr/local/include/linux; then
    JNI_INC="$JNI_INC -I/usr/local/include/linux"
  elif test -d /usr/local/include/solaris; then
    JNI_INC="$JNI_INC -I/usr/local/include/solaris"
  fi
elif test -r /cygdrive/c/jdk1.5.0/include/jni.h; then
  JNI_INC="-I/cygdrive/c/jdk1.5.0/include"
  if test -d /cygdrive/c/jdk1.5.0/include/win32; then
    JNI_INC="$JNI_INC -I/cygdrive/c/jdk1.5.0/include/win32"
  fi
elif test -r /cygdrive/c/j2sdk1.4.2_06/include/jni.h; then
  JNI_INC="-I/cygdrive/c/j2sdk1.4.2_06/include"
  if test -d /cygdrive/c/j2sdk1.4.2_06/include/win32; then
    JNI_INC="$JNI_INC -I/cygdrive/c/j2sdk1.4.2_06/include/win32"
  fi
fi

if test -z "$JNI_INC"; then
  AC_MSG_RESULT([not found])
  echo "JNI is needed to compile"
  echo "see java.sun.com or check aclocal/java.m4 to update"
  echo "if you prefer to update aclocal/java.m4, remember to run ./bootstrap"
  echo "before running ./configure again"
  exit 1
else
  AC_MSG_RESULT([found])
fi
AC_SUBST([JNI_INC])
])