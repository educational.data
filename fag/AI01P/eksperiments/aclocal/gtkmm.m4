AC_DEFUN([AC_LIB_GTKMM],[
  AC_PROVIDE([$0])
  AC_REQUIRE([AC_PATH_X])
  AC_MSG_CHECKING([for gtkmm])
  GTKMM_CC=;
  GTKMM_LIB=;
  version=0;

  if pkg-config --exists gtkmm-2.4; then
    version=2.4;
    GTKMM_CC=`pkg-config gtkmm-2.4 --cflags`
    GTKMM_LIB=`pkg-config gtkmm-2.4 --libs`
    AC_MSG_RESULT([found gtkmm-2.4])
  elif pkg-config --exists gtkmm-2.0; then
    version=2.0;
    GTKMM_CC=`pkg-config gtkmm-2.0 --cflags`
    GTKMM_LIB=`pkg-config gtkmm-2.0 --libs`
    AC_MSG_RESULT([found gtkmm-2.0])
  elif pkg-config --exists gtkmm-1.2; then
    version=1.2;
    GTKMM_CC=`pkg-config gtkmm-1.2 --cflags`
    GTKMM_LIB=`pkg-config gtkmm-1.2 --libs`
    AC_MSG_RESULT([found gtkmm-1.2])
  else
    AC_MSG_RESULT([gtkmm not found])
  fi
  AC_DEFINE_UNQUOTED(GTKMM_VERSION,$version,[
 This is the version number of GTK-- found
 The version is found using gtkmm. if you are certain that you have a version
 better than this, you must make sure that pkg-config can find it.
])
  AC_SUBST([GTKMM_CC])
  AC_SUBST([GTKMM_LIB])
])