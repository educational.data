AC_DEFUN([AC_PREP_CC_SOLARIS],[
INC_SOLARIS=;
AC_MSG_CHECKING([for solaris params])
if test -d /usr/local/packages/gcc-2.95.3/include/g++-3/; then
  INC_SOLARIS="-I/usr/local/packages/gcc-2.95.3/include/g++-3/"
  AC_MSG_RESULT([found])
else
  AC_MSG_RESULT([not found])
fi;
AC_SUBST(INC_SOLARIS)
])