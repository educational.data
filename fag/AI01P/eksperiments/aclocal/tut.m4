AC_DEFUN([AC_LIBRARY_TUT],[
AC_PROVIDE([$0])

AC_ARG_ENABLE(
	[tut],
	[  --disable-tut           disable the use of the template unit test framework (tut)],
	[enable_tut=no],
	[enable_tut=yes])dnl

if test "x$enable_tut" = "xyes"; then
	abs_ac_aux_dir=`(cd $ac_aux_dir;pwd)`
	AC_CACHE_CHECK(
	    [for the tut unit testing framework],
	    [ac_cv_tut_path],
	    [
		if test -r $abs_ac_aux_dir/test/tut.h; then
		    ac_cv_tut_path="found";
		else
		    ac_cv_tut_path="not found"
		fi

	    ]
	)
	TUT_INCLUDE=""
	if test "$ac_cv_tut_path" != "not found"; then
	    TUT_INCLUDE="-I${abs_ac_aux_dir}/test"
	fi;
	
	set > exp

	AC_CACHE_CHECK(
	    [for extra paths needed for tut], 
	    ac_cv_tut_deps,
	    [
		if test -d "/usr/local/packages/gcc-2.95.3/include/g++-3/"; then
		    ac_cv_tut_deps="/usr/local/packages/gcc-2.95.3/include/g++-3/"
		else
		    ac_cv_tut_deps="not needed";
		fi;
	    ]
	)
	
	if test "$ac_cv_tut_deps" != "not needed"; then
	    TUT_INCLUDE="$TUT_INCLUDE -I$ac_cv_tut_deps";
	fi
	AC_CACHE_CHECK(
	    [if tut kan be used to perform unit tests],
	    ac_cv_can_tut_compile, [
		oldCXXFLAGS=$CXXFLAGS;
		CXXFLAGS="$TUT_INCLUDE $CXXFLAGS";
		AC_LANG_CPLUSPLUS
		AC_TRY_LINK(
		    [#include "tut.h"],
		    [/* perform none what so ever */ 
			int r;],
		    ac_cv_can_tut_compile=yes,
		    ac_cv_can_tut_compile=no
		)
		CXXFLAGS=$oldCXXFLAGS;
	    ]
	)
fi
if test "$ac_cv_can_tut_compile" = "yes" && 
   test "x$enable_tut" = "xyes" &&
   test "$ac_cv_tut_path" != "not found"; then
	USE_TUT=yes
else
	USE_TUT=no
	TUT_INCLUDE=""	
	echo "TUT is needed to perform tests and is vital to this package ::EXIT"
	exit 1
fi
AC_SUBST(USE_TUT)
AC_SUBST(TUT_INCLUDE)

])
