dnl @synopsis AC_LATEX_CLASS_REPORT
dnl
dnl same as AC_LATEX_CLASS(report,report)
dnl
dnl @category LaTeX
dnl @author Mathieu Boretti boretti@bss-network.com
dnl @version 2005-01-21
dnl @license GPLWithACException

AC_DEFUN([AC_LATEX_CLASS_REPORT],[
AC_LATEX_CLASS(report,report)
if test $report = "no";
then
    AC_MSG_ERROR([Unable to find the report class])
fi
])
