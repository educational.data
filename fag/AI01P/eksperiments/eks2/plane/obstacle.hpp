#ifndef OBSTACLE_HPP_INCLUDED
#define OBSTACLE_HPP_INCLUDED

#include <vector.hpp>

struct Obstacle {
  graph::Vector position;
  double minDistToTarget;
};

struct ObstacleObserver {
  virtual const Obstacle getObstacleAtTime(int time) = 0L;
  virtual ~ObstacleObserver() { };
};

#endif
