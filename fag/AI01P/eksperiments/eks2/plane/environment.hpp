#ifndef ENVIRONMENT_HPP_INCLUDED
#define ENVIRONMENT_HPP_INCLUDED

#include <boost/utility.hpp>
#include <vector>
#include "obstacle.hpp"

typedef std::vector<Obstacle> Obses;
typedef Obses::iterator Obses_it;

class Environment : boost::noncopyable {
  struct impl;
  impl &pI;
  Environment();
public:
  static Environment& instance();
  ~Environment();  

  void clearObstacles();
  void addObstacleObserver(ObstacleObserver *obs);
  
  Obses getObstaclesAtTime(int time);
};
#endif

