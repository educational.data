// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#include <iostream>
#include <segment.hpp>
#include <path.hpp>
#include <population.hpp>

using namespace graph;
using namespace genetic;
using namespace std;

#define null 0L;


static int check_init_population() {
  Population pop;

  pop.initPaths(10);

  if(pop.size() != 10) {
    cerr << "Size of population is not 10 as expected" << endl;
    return 1;
  } else
    return 0;
}  

int check_genetic() {
  return check_init_population();
}

