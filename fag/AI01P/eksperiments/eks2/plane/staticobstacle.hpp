#ifndef STATICOBSTACLE_HPP_INCLUDED
#define STATICOBSTACLE_HPP_INCLUDED

#include "obstacle.hpp"

class StaticObstacleObserver : public ObstacleObserver {
  struct impl;
  impl &pI;
public:
  StaticObstacleObserver(const graph::Vector pos, const int offset);
  virtual ~StaticObstacleObserver();

  const Obstacle getObstacleAtTime(int time);
};
#endif
