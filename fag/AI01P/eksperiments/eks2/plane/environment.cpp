

#include "environment.hpp"
#include <vector>
#include "obstacle.hpp"
#include <vector.hpp>
#include "staticobstacle.hpp"
#include <boost/shared_ptr.hpp>
#include <iostream>

using namespace std;
using namespace graph;

typedef boost::shared_ptr<ObstacleObserver> oo_ref;
typedef vector<oo_ref> oo_ref_vec;
typedef oo_ref_vec::iterator oo_ref_it;

struct Environment::impl 
{
  oo_ref_vec observers;

  void insert (ObstacleObserver *obs) { 
    oo_ref oref;
    oref.reset(obs);
    observers.push_back(oref);
  }
  void clearObservers() {
    observers.clear();
  }

};

Environment::Environment() : pI(*(new impl))
{
  /*
  pI.insert(new StaticObstacleObserver(Vector(10,10),5));
  pI.insert(new StaticObstacleObserver(Vector(10,-40),10));
  pI.insert(new StaticObstacleObserver(Vector(50,50),20));
  pI.insert(new StaticObstacleObserver(Vector(100,80),10));
  pI.insert(new StaticObstacleObserver(Vector(100,0),20));
  */
}

void 
Environment::clearObstacles() 
{
  pI.clearObservers();
}

void 
Environment::addObstacleObserver(ObstacleObserver *obs) 
{
  pI.insert(obs);
}

Environment::~Environment()
{
  delete &pI;
}

Environment& 
Environment::instance() {
  static Environment env;
  return env;
}

std::vector<Obstacle> 
Environment::getObstaclesAtTime(int time) 
{
  std::vector<Obstacle> obs;
  
  for(oo_ref_it it = pI.observers.begin();
      it != pI.observers.end(); 
      ++it) 
    {
      obs.push_back((*it)->getObstacleAtTime(time));
    }
  return obs;
}
 
