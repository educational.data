// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>

#include "uav-config.h"

#include <population.hpp>
#include <vector.hpp>
#include <path.hpp>
#include <segment.hpp>
#include <fitness_evaluator.hpp>
#include <orientedpoint.hpp>

#ifdef HAVE_STDEXCEPT
# include <stdexcept>
#else

struct out_of_range
{
  out_of_range (const char *)
  {
  }
};
#endif



namespace genetic {
  using namespace graph;
  using namespace std;

  typedef vector<Path> pathVector;
  typedef pathVector::iterator pathIterator;

  /// @cond
  struct PathEnumerator::impl 
  {
    pathIterator current;
    pathIterator end;
  };

  /// @endcond

  PathEnumerator::PathEnumerator() 
  {
    pImpl = new impl;
  }

  PathEnumerator::~PathEnumerator() 
  {
    delete pImpl;
  }

  PathEnumerator::PathEnumerator(const PathEnumerator &path) 
  {
    pImpl = new impl(*(path.pImpl));
  }




  /////////////////////////////////////////////////////////////

  struct _path_fitness {
    int id;
    double fitness;
  };
  
  int operator < (const struct _path_fitness &f1, 
	      const struct _path_fitness &f2) 
  {
    return f1.fitness > f2.fitness;
  }

  typedef vector<_path_fitness> fitnessVector;
  typedef fitnessVector::iterator fitnessIterator;
  
  /**
   * @brief pImpl ideom class for population class.
   * @see Population
   */
  struct Population::impl 
  {
    impl() {
      isOpen = false;
      if(piCnt++ == 0) {
        os.open("Dump.txt",ios::out|ios::trunc);
        isOpen = true;
      }
      i=0;
    }

    ~impl() {
      if(!isOpen) return;
      os.close();
    }

    void dumpMeanMin(double mean, double max) {
      if(!isOpen) return;
      os << i++ << " " << mean << " " << max << " END \r\n";
      os.flush();
    }
    
    int i;
    static int piCnt;
    bool isOpen;
    ofstream os;

    pathVector paths;

    Vector target;
    FitnessEvaluator evaluator;

    Path bestPath;
    int lastSize;

    //double evaluateFitness(const Path &p);
  };

  Population::Population() 
  {
    pImpl = new impl;

    time_t now;
    
    time(&now);
    
    srandom(now % 99);
  }

  Population::~Population() 
  {
    delete pImpl;
  }

  bool PathEnumerator::hasNext()
  {
    return pImpl->current != pImpl->end;
  }

  graph::Path &PathEnumerator::next() 
  {
    Path &path = *(pImpl->current);
    ++(pImpl->current);
    return path;
  }

  void Population::crossPopulation(Population &pop) 
  {
    pop.pImpl->paths[0] = pImpl->bestPath;
    pImpl->paths[0] = pop.pImpl->bestPath;
  }

  void Population::initPaths(const int count) {
    pImpl->paths.resize(0);
    pImpl->lastSize = count;
    for(int i = 0;i< count-1;++i) {
      Path p;
      int bLen = 20;
      p.reset(bLen);
      
      pImpl->paths.push_back(p);
    }
    if(pImpl->bestPath.length() != 0)
      pImpl->paths.push_back(Path(pImpl->bestPath));
  }

  void Population::setTarget(const Vector &target) {
    pImpl->target = target;
    pImpl->evaluator.setTarget(target);
  }

  const Vector &
  Population::getTarget() const 
  {
    return pImpl->target;
  }

  const Path &
  Population::getBestPath() const
  {
    return pImpl->bestPath;
  }

  int 
  Population::size() const {
    return pImpl->paths.size();
  }


  void Population::evolve() 
  {

    //cout << "a" << flush;
    if(pImpl->bestPath.size() != 0) 
      {
	if( pImpl->evaluator.evaluate( pImpl->bestPath ) < 1e20)
	  {
	    pImpl->paths.resize(size()-1);
	    pImpl->paths.push_back(pImpl->bestPath);
	  }
      }
    //cout << "b" << flush;
    pathIterator it = pImpl->paths.begin();
    pathIterator end = pImpl->paths.end();
    int id = 0;

    fitnessVector fitness;

    double minFitness = 100000000.0f;
    double meanFitness = 0.0f;
    int mCnt =0;
    for(;it != end;++it) 
      {
	int tmpid = id++;

	bool found = false;
	while(found == false) {
	  _path_fitness pf;
	  pf.id = tmpid;
	  pf.fitness = pImpl->evaluator.evaluate(*it);
	  if(pf.fitness < 1e20)  // make sure that punished data gets lost..
	    {
              minFitness = min(minFitness,pf.fitness);
              meanFitness+= pf.fitness;
              ++mCnt;

	      fitness.push_back(pf);
	      found = true;
	    }
	  else
	    it->reset();
	}
	
      }
    //cout << "c" << flush;
    if(fitness.size() == 0)
      {
	cout << "retry" << endl;
	initPaths(pImpl->lastSize);
	evolve();
	return;
      }

    pImpl->dumpMeanMin(meanFitness/mCnt,minFitness);

    sort(fitness.begin(),fitness.end());
    //cout << "A" << endl;
    //cout << "d" << flush;

    vector<int> poolOfPaths;
    int pSize = 1;

    pImpl->bestPath = pImpl->paths[fitness.back().id];
    //cout << "e" << flush;      
    //cout << "Best fit = " << fitness.back().fitness << endl;

    for(fitnessIterator fIt = fitness.begin();fIt != fitness.end();++fIt) 
      {
	for(int i = 0;i < pSize*2;++i) 
	  {
	    poolOfPaths.push_back(fIt->id);
	  }
	++pSize;
      }
    
    // cout << "-" << flush;
    
    // selection of new generation

    pathVector newPV;

    //cout << "currentSize = " << size() << endl;
    for(int i = 0;i < size()/2;++i) 
      {
	int id1 = poolOfPaths[random() % poolOfPaths.size()];
	int id2 = poolOfPaths[random() % poolOfPaths.size()];
	Path p1 = pImpl->paths.at(id1);
	Path p2 = pImpl->paths.at(id2);
	
	// perform crossover

	int crossPos = random() % min(p1.size(),p2.size());
	/* if(random() % 2 == 1) */
	  p1.crossover(p2,crossPos);
	
	newPV.push_back(p1);
	newPV.push_back(p2);
      }
    
       
    // mutation - random method
    for(it = newPV.begin();it != newPV.end();++it) 
      {
	int chance = random() % 4;

	switch(chance) 
	  {
	  case 1:
	    it->mutate1p();
	    break;
	  case 2:
	    it->mutate2p();
	    break;
	  case 3:
	    it->mutateExpand();
	    break;
	  }
      }
    newPV.resize(newPV.size()-1);
    newPV.push_back(pImpl->bestPath);
    pImpl->paths = newPV;
  }



  PathEnumerator Population::enumerator() {
    PathEnumerator pe;
    pe.pImpl->current = pImpl->paths.begin();
    pe.pImpl->end = pImpl->paths.end();
    return pe;
  }

  int Population::impl::piCnt = 0;
}
