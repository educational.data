#include "staticobstacle.hpp"

struct StaticObstacleObserver::impl {
  Obstacle mainPos;
};

StaticObstacleObserver::StaticObstacleObserver(const graph::Vector pos, const int offset) : pI(*(new impl))
{
  pI.mainPos.position = pos;
  pI.mainPos.minDistToTarget = offset;
}

StaticObstacleObserver::~StaticObstacleObserver() 
{
  delete &pI;
}

const Obstacle 
StaticObstacleObserver::getObstacleAtTime(int time) 
{
  return pI.mainPos;
}
