// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <vector>
#include <algorithm>
#include <iostream>

#include "uav-config.h"

#include <fitness_evaluator.hpp>
#include <vector.hpp>
#include <path.hpp>
#include <segment.hpp>
#include <orientedpoint.hpp>
#include <vector>
#include <environment.hpp>

#ifdef HAVE_STDEXCEPT
# include <stdexcept>
#else

struct out_of_range
{
  out_of_range (const char *)
  {
  }
};
#endif


namespace genetic {
  using namespace graph;
  using namespace std;

  /*
  struct avoid_point_t {
    Vector center;
    double radius;
  };
  */
		   
  struct FitnessEvaluator::impl 
  {
    Vector target;
    /*typedef vector<avoid_point_t> avoids_v;
    typedef avoids_v::iterator avoids_it;
    avoids_v avoids; */

    Environment &environment;

    impl() : environment(Environment::instance()) { }

    /*
      void addAvoid(Vector center, double radius) {
      avoid_point_t ap;
      ap.center = center;
      ap.radius = radius;
      avoids.push_back(ap);
      } 
    */

    double distanceToAvoid(Vector offset,Segment &seg, int segLen) {
      double dist = 0;

      std::vector<Obstacle> obs = environment.getObstaclesAtTime(segLen);

      if(obs.size() == 0) {
	// cout << "The size of obstacles is zero!" << endl;
	return 0;
	//exit (-1);
      }

      
      for(std::vector<Obstacle>::iterator it = obs.begin();it!=obs.end();++it)
	{
	  double dd = seg.distanceToPoint(offset,it->position);
	  double vDist = (dd - it->minDistToTarget);
	  if(dd <= it->minDistToTarget)
	    dist += 200 + vDist*vDist;
	  else	  
	    dist += 10/(0.1 + vDist);
	}
      return dist;
    }
  };

  FitnessEvaluator::FitnessEvaluator() {
    pImpl = new impl;
    // pImpl->addAvoid(Vector(50, 50),30);
    // pImpl->addAvoid(Vector(100, 80),10);
  }

  FitnessEvaluator::~FitnessEvaluator() {
    delete pImpl;
  }

  void FitnessEvaluator::setTarget(const graph::Vector &target) {
    pImpl->target = target;
  }
  
#define PUNISH penalty = 1e21
  double FitnessEvaluator::evaluate(const graph::Path &path) 
  {

    SegmentEnumerator se = path.enumerator();

    double dst = 0;
    double segLen = 0;

    Vector vDanger (50,50);
    Vector vCurrent(0,0);
    double penalty = 0;
    int elemCount = 0;
    while(se.hasNext()) {
      Segment &seg = se.next();
      dst += pImpl->distanceToAvoid(vCurrent,seg,segLen);
      //elemCount+=5;
      segLen = max(seg.length(),segLen);
      if(seg.length() < 5.0)
	PUNISH;

      vCurrent += seg.getEndOffset();
    }

    Vector tarToPath = vCurrent - pImpl->target;

    double distToTarget = tarToPath.length();
    double res = path.length()*10 + (distToTarget*distToTarget) + dst + penalty + elemCount;

    return res;
  }
}
