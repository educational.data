#ifndef PLANE_HPP_INCLUDED
#define PLANE_HPP_INCLUDED


namespace graph {
  class Vector;
  class Path;
}

class Environment;

class Plane {
  struct impl;
  impl &pI;

public:
  Plane();
  ~Plane();

  graph::Path getCurrentPath();
  graph::Vector getCurrentPosition();
  graph::Vector getCurrentHeading();
  graph::Vector getCurrentTarget();

  void move_n_secs(int secs);
  
  Environment &getEnvironment();
};

#endif // PLANE_HPP_INCLUDED
