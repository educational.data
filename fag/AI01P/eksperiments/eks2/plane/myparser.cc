#include "myparser.hpp"
#include "environment.hpp"
#include <stdexcept>

#include <iostream>
#include <vector.hpp>
#include "staticobstacle.hpp"

using namespace std;
using namespace graph;

struct MySaxParser::impl {
  Vector currentLocation;
  string currentName;
  //glib::ustring currentName;
  double currentOffset;
  bool hasLocation;
  bool hasOffset;

  void beginEnvironment(const AttributeList& attrib) {
    cout << "environment {" << endl;

    for(xmlpp::SaxParser::AttributeList::const_iterator iter = attrib.begin(); iter != attrib.end(); ++iter) {
      if(iter->name == "name") {
	currentName = iter->value;
	continue;
      }
    }

    Environment& env = Environment::instance();
    env.clearObstacles();
  }

  void beginStaticObstacle(const AttributeList& attributes) {
    cout << "  StaticObstacle {" << endl;
    hasLocation = false;
    hasOffset = false;
  }

  void setLocation(const AttributeList& attributes) {
    currentLocation = parseVector(attributes);
    cout << "    location: " << currentLocation << endl;
    hasLocation = true;
  }

  void setOffset(const AttributeList& attrib) {
    for(xmlpp::SaxParser::AttributeList::const_iterator iter = attrib.begin(); iter != attrib.end(); ++iter)
      if(iter->name == "size")
	currentOffset = (atof(iter->value.c_str()));
    cout << "    offset: " << currentOffset << endl;
    hasOffset = true;
  }

  Vector parseVector(const AttributeList& attributes) {
    Vector loc;
    bool hasX = false;
    bool hasY = false;
    for(xmlpp::SaxParser::AttributeList::const_iterator iter = attributes.begin(); iter != attributes.end(); ++iter)
      {
	if(iter->name == "X") {
	  hasX = true;
	  loc.setX(atof(iter->value.c_str()));
	  continue;
	}
	
	if(iter->name == "Y") {
	  hasY = true;
	  loc.setY(atof(iter->value.c_str()));
	  continue;
	}

	cout << "unknown attribute " << iter->name << endl;
	
	//std::cout << "    Attribute " << iter->name << " = " << iter->value << std::endl;
	/* if(iter->name != "Fisk")
	   throw std::runtime_error("Unknown name"); */
      } 
    if(!(hasX && hasY))
      cout << "You have failed to specify the X or Y coordinate" << endl;
    return loc;
  }

  void endStaticObstacle() {
    if(!hasLocation) {
      cout << "Missing location" << endl;
      return;
    }

    if(!hasOffset) {
      cout << "Missing offset" << endl;
      return;
    }

    Environment::instance().addObstacleObserver(new StaticObstacleObserver(currentLocation,currentOffset));

    hasLocation = hasOffset = false;

    cout << "  }" << endl;
  }
  void endEnvironment() {
    cout << "}" << endl;
  }
};

MySaxParser::MySaxParser() 
  : xmlpp::SaxParser(), pI(*(new impl))
{
  
}

MySaxParser::~MySaxParser()
{
  delete &pI;
}

void MySaxParser::on_start_document()
{
  //std::cout << "on_start_document()" << std::endl;
  
}

void MySaxParser::on_end_document()
{
  //std::cout << "on_end_document()" << std::endl;
}

void MySaxParser::on_start_element(const Glib::ustring& name,
                                   const AttributeList& attributes)
{
  //std::cout << "node name=" << name << std::endl;

  // Print attributes:

  if(name == "environment")
    return pI.beginEnvironment(attributes);

  if(name == "StaticObstacle")
    return pI.beginStaticObstacle(attributes);

  if(name == "location")
    return pI.setLocation(attributes);
  
  if(name == "offset")
    return pI.setOffset(attributes);
 
  cout << "Unknown tag: " << name << endl;
  return;
  for(xmlpp::SaxParser::AttributeList::const_iterator iter = attributes.begin(); iter != attributes.end(); ++iter)
  {
    std::cout << "  Attribute " << iter->name << " = " << iter->value << std::endl;
    /* if(iter->name != "Fisk")
       throw std::runtime_error("Unknown name"); */
  }
  
}

void MySaxParser::on_end_element(const Glib::ustring& name)
{
  if(name == "environment")
    pI.endEnvironment();

  if(name == "StaticObstacle")
    pI.endStaticObstacle();

  // std::cout << "on_end_element()" << std::endl;
}

void MySaxParser::on_characters(const Glib::ustring& text)
{
  // std::cout << "on_characters(): " << text << std::endl;
}

void MySaxParser::on_comment(const Glib::ustring& text)
{
  //std::cout << "on_comment(): " << text << std::endl;
}

void MySaxParser::on_warning(const Glib::ustring& text)
{
  // std::cout << "on_warning(): " << text << std::endl;
}

void MySaxParser::on_error(const Glib::ustring& text)
{
  std::cout << "on_error(): " << text << std::endl;
}

void MySaxParser::on_fatal_error(const Glib::ustring& text)
{
  std::cout << "on_fatal_error(): " << text << std::endl;
}


