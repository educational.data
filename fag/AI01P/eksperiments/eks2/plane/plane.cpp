// #include "environment.hpp"
#include "plane.hpp"

#include "environment.hpp"

#include <population.hpp>
#include <path.hpp>
#include <vector.hpp>
#include <segment.hpp>
#include <iostream>

using namespace graph;
using namespace std;

struct Plane::impl {
  Environment &env;
  genetic::Population pop1;
  genetic::Population pop2;
  Vector pos;
  Vector heading;

  Path currentPath;
  int cSecs;

  SegmentEnumerator *sen;

  impl() :env(Environment::instance()) {
    pop1.initPaths(100);
    pop1.setTarget(Vector(100,100));

    pop2.initPaths(100);
    pop2.setTarget(Vector(100,100));
    cSecs = 0;
    sen = NULL;
  }

  void setCurrentPath(const Path &path) {
    if(sen != 0)
      delete sen;

    sen = new SegmentEnumerator(currentPath.enumerator());

    cout << "." << endl;

    currentPath = path;
  }
  
  void movePlane(unsigned secs) {
    //cout << "here" << endl << flush;
    if(sen != NULL && sen->hasNext()) 
      {
        static Segment currentSeg;
        if(currentSeg.length() <= cSecs) 
          {
            if(sen->hasNext())
            {
              cSecs-=currentSeg.length();
              currentSeg = sen->next();
            } else {
              cerr << "ERRRRRROOOOOOR::: no more path segments!!!!!" << endl;
              return;
            }
          }
        heading = currentSeg.getDirectionAfterNSteps(cSecs);
	//heading = seg.getStartHeading();
      }
    else 
      {
	heading = Vector(1,0);
      }

    pos += heading;
    cSecs += secs;
  }
};

Plane::Plane() : pI(*(new impl)) { }

Plane::~Plane() {
  delete &pI;
}

void 
Plane::move_n_secs(int secs) {
  static int nextSec = 0;
  if(nextSec <= 0)
    {
      cout << "here" << endl;
      nextSec = 10;
      pI.pop1.initPaths(30);
      pI.pop2.initPaths(30);
      
      for(int i = 0;i<40;++i) 
	{
	  pI.pop1.evolve(); 
	  pI.pop2.evolve(); 
	  if(i % 10 == 0)
	    pI.pop1.crossPopulation(pI.pop2);
	}
      pI.setCurrentPath(pI.pop1.getBestPath());
    }
  nextSec-=secs;
  pI.movePlane(secs);
}

graph::Path 
Plane::getCurrentPath() 
{
  return pI.pop1.getBestPath();
}

graph::Vector
Plane::getCurrentHeading() {
  return pI.heading;
}

graph::Vector
Plane::getCurrentPosition() 
{
  return pI.pos;
}

graph::Vector
Plane::getCurrentTarget() 
{
  return pI.pop1.getTarget();
}

Environment &
Plane::getEnvironment() {
  return pI.env;
}





