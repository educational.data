// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <iostream>
#include <vector>
#include <boost/tuple/tuple.hpp>
#include <boost/operators.hpp>
#include <math.h>

#include <population.hpp>

// #include "loader.hpp"



#include <time.h>
#ifdef HAVE_STDEXCEPT
# include <stdexcept>
#else
struct out_of_range {
  out_of_range(const char *) { }
};
#endif

#include "vector.hpp"

using namespace std;

int check_segment();
int check_genetic();

int main(char* args[], int argcnt) {

  genetic::Population pop;

  // testGraphBuilder();
  // testAddTurn();

  //int res = check_segment() + check_genetic();

  //if(res != 0) {
  //  cerr << "Test failed, number of tests failed: " << res << endl;
  //  return -1;
  //}else return 0;
}


/**
 * @mainpage Mainpage of AI project 01.
 * @section velcome Velcome. 
 *
 * This library is an attempt to implement a evolutionary
 * method for building a high level path planner for a UAV
 * 
 * This is offcourse a work in progress and so is allso the progress
 * of the documentation. So therefore i hope that you will forgive the
 * clumsy and highly lacking amount of documentation that has been
 * produced for this extraordinary excellent piece of work.
 *
 * In order to get to know this system, i will highly suggest that you
 * start by looking at the #genetic::Population class which contains a
 * lot of details on using the system.
 */
