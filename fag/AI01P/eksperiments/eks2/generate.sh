#!/bin/bash
set -- $(getopt -u -n $0 -l "config-file: output:" -o c:o: -- $@)

for o
  do
  case "$o" in
      -c | --config-file) shift; input="$1";shift;;
      -o | --output)shift;output="$1";shift;;
      --) shift; shift;;
  esac
done

if test x$input == x; then
    echo "Error: must have a file to read from" >&2
    exit 1
else
    if test ! -r $input; then
	echo "Error: $input is not readable" >&2
	exit 1
    fi
fi

if test x$output == x; then
    exec 3>&1
else 
    exec 3>$output
fi

echo "#ifndef __UAV_CONFIG_INCLUDED__" >&3
echo "#define __UAV_CONFIG_INCLUDED__" >&3
echo "" >&3
echo "#define NOT_IMPLEMENTED std::cerr << \" *** ERROR *** The function \" << __FUNCTION__ << \" in file \" << __FILE__ << \" is not implemented\" << endl" >&3
echo "" >&3
echo "namespace UAV_CONFIG {" >&3
cat $input| \
sed "s/^\([.^\#]*\)\#\(.*\)/  \1\/\/\2/g" | \
sed "s/\(\w*\):\([0-9]*\)$/  static const int \1 = \2;/g"| \
sed "s/\(\w*\):\(-\{0,1\}[0-9]*\.[0-9]*\)$/  static const float \1 = \2;/g" >&3
echo "};" >&3
echo "#endif // __UAV_CONFIG_INCLUDED__" >&3

exec 3>&-