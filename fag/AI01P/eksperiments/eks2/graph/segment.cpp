// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <iostream>
#include <vector>
#include <math.h>

#include "uav-config.h"

#include <time.h>
#ifdef HAVE_STDEXCEPT
# include <stdexcept>
#else
struct out_of_range
{
  out_of_range (const char *)
  {
  }
};
#endif

#include <uav-config.h>

#include <segment.hpp>
#include <vector.hpp>

static inline double abs(double a) 
{
  return a < 0.0 ? -a : a;
}


namespace graph 
{
  using namespace std;
  
  struct Segment::impl 
  {
    Vector startHeading;
    Vector startOffset;

    Vector endHeading;
    Vector endOffset;

    Vector tp1;
    Vector tp2;
    Angle a1t1;
    Angle a2t1;
    Angle a1t2;
    Angle a2t2;
    double tp1radius;
    double tp2radius;

    double side_calculate_a(const Vector &vd, const Vector &wd, const Vector &w) 
    {
      double x1 = vd.getX();
      double y1 = vd.getY();
      double x2 = wd.getX();
      double y2 = wd.getY();
      double x3 = w.getX();
      double y3 = w.getY();
      
      double a1 = x3*y2 - x2*y3;
      double a2 = x2*y1 - x1*y2;
      double a = a1/a2;

      return a;
    }

#define Power(a,b) pow(a,b)
#define Sqrt(a) sqrt(a)
    double side_calculate_b(const Vector &vd, const Vector &wd, const Vector &w) 
    {
      double x1 = vd.getX();
      double y1 = vd.getY();
      double x2 = wd.getX();
      double y2 = wd.getY();
      double x3 = w.getX();
      double y3 = w.getY();
      
      double b1 = x3*y1 - x1*y3;
      double b2 = x2*y1 - x1*y2;
      double b = b1/b2;
      
      return b;
    }

    bool calcR(const Vector &vd, const Vector &wd, const Vector &k)
    {
      // double x1 = vd.unit().getX();
      // double y1 = vd.unit().getY();
      // double x2 = wd.unit().getX();
      // double y2 = wd.unit().getY();
      double x3 = k.getX();
      double y3 = k.getY();

      Vector uv = vd.unit();
      Vector uw = wd.unit();
      Vector utv = (uw*-1).transpose();
      Vector utw = uw.transpose();

      double avV = uv.angle().value() - k.angle().value();
      double avW = uw.angle().value() - k.angle().value();

      bool swap;

      if(avV * avW >= 0.0) 
	{
	  if(avV > 0) 
	    {
	      utw = (uw*-1).transpose()*-1;
	      utv = uv.transpose()*-1;
	      swap = false;
	    }
	  else
	    {
	      utw = (uw*-1).transpose();
	      utv = uv.transpose();
	      swap = true;
	    }
	  
	  if(abs(avW) + abs(avV) == 0.0) // they are straight in line
	    return false;
	}
      else 
	return false;

      double x4 = -(utv.getX()) + utw.getX();
      double y4 = -(utv.getY()) + utw.getY();

      double R = (-2*x3*x4 - 2*y3*y4 - Sqrt(Power(2*x3*x4 + 2*y3*y4,2) - 4*(Power(x3,2) + Power(y3,2))*(-4 + Power(x4,2) + Power(y4,2))))/(2.*(-4 + Power(x4,2) + Power(y4,2)));

      tp1 = utv*R;
      tp2 = k+utw*R;

      Vector Lt = R*(-utv+utw)+k;

      tp1radius = tp2radius = R;
      
      a1t2 = (utv*-1).angle();
      a2t2 = (utw*-1).angle();

      a1t1 = Lt.angle();
      a2t1 = (Lt * -1).angle();
      
      if(avV < 0 || avW < 0||swap) 
	{
	  Angle tmp = a1t2;
	  a1t2 = a1t1;
	  a1t1 = tmp;
	  
	  tmp = a2t1;
	  a2t1 = a2t2;
	  a2t2 = tmp;
	}

      cout << "ANG: " << a1t1 << " <-> " << a1t2 << endl;
      cout << "Radius : " << abs(R) << endl;
      //      double Rt = abs(R) < 5*k.length();
      return true;
    }

    bool calcSegment() 
    {
      if(endOffset.length() == 0.0) {
        return true;
      }

      bool res = calcR(startHeading,endHeading,endOffset);
      
      if(res == true) 
	return true;

      Vector v(0,0);
      Vector w = endOffset;
      Vector vd = (startHeading.unit() + w.unit()).unit();
      Vector wd = (endHeading.unit() + w.unit()).unit();

      Vector sht = startHeading.transpose().unit();
      Vector eht = endHeading.transpose().unit();
      Vector wt = w.transpose().unit()*-1;

      double a = side_calculate_a(vd,wd,w);
      //double b = side_calculate_b(vd,wd,w);

      Vector v1= a*vd;
      Vector v2= sht;
      Vector v3= eht;
      Vector v4 = wt;

      //double aa = side_calculate_a(v2,v4,v1);

      //cout << "vd = " << vd << endl;

      tp1 = -a*vd;
      tp2 = tp1;

      Vector startT(startHeading.transpose().unit());

      double da = abs(a);
      double d_ab = acos(tp1.dot(startT)/(da));
      double s_d_ab = sin(d_ab);
      double d_ang_wt = acos(startT.dot(wt));
      cout << d_ab << "<<->>" << d_ang_wt << endl;
      double s_l_ang = sin(M_PI - (d_ab + d_ang_wt));

      cout << "Angle from the inside: " << ((d_ab))*180/M_PI << endl;
      double c = (da * s_l_ang) / s_d_ab;
      //c = 0.1;
      tp1 = startT*c;


      tp1radius = abs(c);
      tp2radius = abs(0.1);
      
      a1t2 = startT.angle();
      a1t1 = (tp1-tp2).angle();
      
      a2t1 = 0;
      a2t2 = 2*M_PI;
      cout << "A=" << a << ",C=" << c << endl;
      return true;
    }
  };

  Segment::Segment() {
    pImpl = new impl;
  }

  Segment::Segment(const Vector &startOffset,
		   const Vector &startHeading,
		   const Vector &endOffset,
		   const Vector &endHeading)
  {
    pImpl = new impl;
    pImpl->startOffset = startOffset;
    pImpl->startHeading = startHeading;
    pImpl->endOffset = endOffset;
    pImpl->endHeading = endHeading;
    /* if(endHeading == startHeading)
      cerr << "collaps" << endl;
    else
      cerr << "no collaps" << endl;
    */
  }

  Segment::Segment(const Segment &segment) 
  {
    pImpl = new impl(*(segment.pImpl));
  }
  
  Vector Segment::getTP1() const { return pImpl->tp1;  }
  Vector Segment::getTP2() const { return pImpl->tp2;  }
  Angle Segment::getTP1Angle1() const { return pImpl->a1t1; }
  Angle Segment::getTP1Angle2() const { return pImpl->a1t2; }
  Angle Segment::getTP2Angle1() const { return pImpl->a2t1; }
  Angle Segment::getTP2Angle2() const { return pImpl->a2t2; }  
  double Segment::getTP1radius() const { return  pImpl->tp1radius; }
  double Segment::getTP2radius() const { return  pImpl->tp2radius; }

  /**
   * needs to be reimplemented
   */
  double Segment::length() const { 
    pImpl->calcSegment();
    return pImpl->endOffset.length(); 
  }

  Vector Segment::getEndOffset() const { return pImpl->endOffset; }
  Vector Segment::getEndHeading() const { return pImpl->endHeading; }
  Vector Segment::getStartOffset() const   { return pImpl->startOffset;  }
  Vector Segment::getStartHeading() const { return pImpl->startHeading; }

  bool Segment::calcData() 
  {
    return pImpl->calcSegment();
  }

  const Segment &Segment::operator=(const Segment &segment) 
  {
    delete pImpl;
    pImpl = new impl(*(segment.pImpl));
    return *this;
  }

  Segment::~Segment() { delete pImpl; }

  
  Vector Segment::setEndOffset(const Vector & vec)   
  {
    pImpl->endOffset = vec;
    return pImpl->endOffset;
  }
  
  Vector Segment::setEndHeading(const Vector & vec)  
  {
    pImpl->endHeading = vec;
    return pImpl->endHeading;
  }

  Vector Segment::addEndOffset(const Vector & vec)   
  {
    pImpl->endOffset += vec;
    return pImpl->endOffset;
  }

  Vector Segment::addEndHeading(const Vector & vec)   
  {
    pImpl->endHeading += vec;
    return pImpl->endHeading;
  }
  
  Vector Segment::setStartOffset(const Vector & vec) 
  {
    pImpl->startOffset = vec;
    return pImpl->startOffset;
  }

  Vector Segment::setStartHeading(const Vector & vec) 
  {
    pImpl->startHeading = vec;
    return pImpl->startHeading;
  }

  Vector Segment::addStartOffset(const Vector & vec) 
  {
    pImpl->startOffset += vec;
    return pImpl->startOffset;
  }

  Vector 
  Segment::addStartHeading(const Vector & vec) 
  {
    pImpl->startHeading += vec;
    return pImpl->startHeading;
  }
  const Vector 
  Segment::getDirectionAfterNSteps(const int cSecs) {
    calcData();
    return pImpl->endHeading;
  }



#define dot(u,v)   ((u).getX() * (v).getX() + (u).getY() * (v).getY())
#define norm(v)    sqrt(dot(v,v))  // norm = length of vector
#define d(u,v)     norm(u-v)       // distance = norm of difference
  /**
   * This piece of code uses the scheme found at the location
   * http://softsurfer.com/Archive/algorithm_0102/algorithm_0102.htm#dist_Point_to_Line()
   */
  double Segment::distanceToPoint(const Vector &offset, const Vector &point) 
  {
    Vector v = pImpl->endOffset;
    Vector w0 = point - offset;

    double c0 = dot(w0,v);
    if(c0 <= 0)
      return d(point,offset);
    
    double c1 = dot(v,v);
    if(c0 >= c1)
      return d(point,offset + v);

    double b = c0 / c1;
    Point Pb = offset + b * v;
    return d(point,Pb);
  }
  
  double Segment::getMinRadius() 
  {
    NOT_IMPLEMENTED;
    return -1;
  }
  
  double Segment::getMaxRadius() 
  {
    NOT_IMPLEMENTED;
    return -1;
  }
}
