// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <math.h>
#include "vector.hpp"

namespace graph {
  using namespace std;


  void Angle::sizeIt() {
    while(angle >= 2*M_PI)
      angle -= 2*M_PI;

    while(angle < 0)
      angle += 2*M_PI;
  }
  
  Angle::Angle() {
    angle = 0;
  }
  
  Angle::Angle(double val) {
    angle = val;
    sizeIt();
  }
  Angle 
  Angle::operator+=(const Angle &ang) {
    angle+=ang.angle;
    sizeIt();
    return *this;
  }
  
  Angle 
  Angle::operator*=(const double val) {
    angle*=val;
    sizeIt();
    return *this;
  }
  
  Angle 
  Angle::operator-=(const Angle &ang) {
    angle-=ang.angle;
    sizeIt();
    return *this;
  }
  
  const float 
  Angle::degrees() const {
    return 180* angle/M_PI;
  }
  
  const double 
  Angle::value() const {
    return angle;
  }

  bool 
  operator < (const Angle &a, const Angle &b) { 
    return a.value() < b.value(); 
  }
  bool 
  operator > (const Angle &a, const Angle &b) { 
    return a.value() > b.value(); 
  }
  bool 
  operator <= (const Angle &a, const Angle &b) { 
    return a.value() <= b.value(); 
  }
  bool 
  operator >= (const Angle &a, const Angle &b) { 
    return a.value() >= b.value(); 
  }
}
