// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <math.h>
#include "vector.hpp"
#include <sstream>
#include <string>

namespace graph {
  using namespace std;
  void Vector::sizeIt() {
    X = X < -0.0001 || X > 0.0001 ? X : 0.0;
    Y = Y < -0.0001 || Y > 0.0001 ? Y : 0.0;
  }
  
  Vector::Vector() : 
    X(0), 
    Y(0) 
  { 
    sizeIt();
  }
  
  Vector::Vector(const Angle &ang) 
  {
    X = cos(ang.value());
    Y = sin(ang.value());
    
    sizeIt();
  } 
    
  Vector::Vector(const float x, const float y) : 
    X(x),
    Y(y) 
  {
    sizeIt(); 
  }

  Vector::~Vector() {

  }

  double
  Vector::dot(const Vector &v) const {
    return Y*v.X + X*v.Y;
  }

  double 
  Vector::norm() const {
    return sqrt(dot(*this));
  }

  double 
  Vector::d(const Vector &v) const {
    return (*this - v).norm();
  }
  
  Vector &
  Vector::operator+=(const Vector &vect) {
    X += vect.X;
    Y += vect.Y;
    sizeIt();
    return *this;
  }
    
  Vector &
  Vector::operator-=(const Vector &vect) {
    X -= vect.X;
    Y -= vect.Y;
    sizeIt();
    return *this;
  }
  
  Vector &
  Vector::operator*=(const double value) {
    X*=value;
    Y*=value;
    sizeIt();
    return *this;
  }
  
  double 
  Vector::length() const {
    return sqrt(X*X + Y*Y);
  }
  
  const Vector 
  Vector::unit() const {
    double len = length();
    return Vector(X/len,Y/len);
  }
  const Vector 
  Vector::transpose() const {
    return Vector(Y,-X);
  }
  
  const Vector 
  Vector::reverse() const {
    return Vector(-X,-Y);
  }
  
  Angle 
  Vector::angle() const throw(runtime_error) {

    if(X == 0.0 && Y == 0.0)
      throw runtime_error("Vector::angle():vector has no length");
    
    if(X == 0.0f)
      return Y<0 ? M_PI/2 : 3*M_PI/2;

    return atan2(-Y,X);
  }

  const Vector Vector::rotate(const Angle &ang) 
  {
    double cv = cos(ang.value());
    double sv = sin(ang.value());

    Vector res(X * cv - Y * sv,
	       X * sv + Y * cv);
    return res;
  }

  bool 
  Vector::operator==(const Vector &vec) const {
    return 
      vec.X < X+0.001 && vec.X > X-0.001 &&
      vec.Y < Y+0.001 && vec.Y > Y-0.001;
  }

  bool 
  Vector::operator!=(const Vector &vec) const {
    return ! operator==(vec);
  }

  string
  Vector::str() const 
  {
    stringstream ost;
    ost << "[" << getX() << "," << getY() << "]"; 
    return ost.str();
  }

  ostream & 
  operator<<(ostream &str, const Angle &ang) 
  { 
    str << ang.value()/M_PI << "*PI"; 
    return str; 
  }
  ostream & 
  operator<<(ostream &str, const Vector &vec) { 
    //str << "[" << vec.getX() << "," << vec.getY() << "]"; 
    str << vec.str();
    return str; 
  }


  Point::Point() : X(0), Y(0) {
    
  }

  Point::Point(double x, double y):X(x),Y(y) {

  }

  Point::Point(const Vector &v) : X(v.getX()),Y(v.getY()) {

  }

  Point::operator Vector() {
    return Vector(X,Y);
  }

}
