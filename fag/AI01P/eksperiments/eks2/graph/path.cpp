// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <iostream>
#include <vector>
#include <math.h>

#include "uav-config.h"

#include <time.h>
#ifdef HAVE_STDEXCEPT
# include <stdexcept>
#else
struct out_of_range
{
  out_of_range (const char *)
  {
  }
};
#endif

#include <vector.hpp>
#include <orientedpoint.hpp>
#include <path.hpp>
#include <segment.hpp>

#include <vector>

namespace graph
{
  using namespace std;
  typedef vector<Segment> vSegment;
  typedef vSegment::iterator segIterator;


  struct SegmentEnumerator::impl {
    segIterator current;
    segIterator end;
  };

  SegmentEnumerator::SegmentEnumerator() 
  {
    pImpl = new impl;
  }

  SegmentEnumerator::SegmentEnumerator(const SegmentEnumerator &en) 
  {
    pImpl = new impl(*(en.pImpl));
  }

  SegmentEnumerator::~SegmentEnumerator() 
  {
    delete pImpl;
  }

  bool SegmentEnumerator::hasNext() 
  {
    return pImpl->current != pImpl->end;
  }

  Segment & SegmentEnumerator::next() 
  {
    Segment &seg = *(pImpl->current);
    ++(pImpl->current);
    return seg;    
  }


  ////////////////////////////////////////////////////////////
  struct Path::impl {
    vector<Segment> segments;
    Vector startOffset;
    Vector startHeading;

    /// sets the intial number of steps in a single path.
    unsigned int initialSize;

    void insertSegment(segIterator it, const Segment &seg) throw (runtime_error) {
      Vector sheading = it->getEndHeading();

      if(seg.getStartHeading() != sheading)
	throw runtime_error(__FILE__ "World" "__LINE__");
    }

    /**
     * updates all the headings from the list of offsets in each segment
     */
    void updateHeadings() {
      if(segments.empty())
	return;

      segIterator it = segments.begin();
      it->setStartHeading(it->getEndOffset().unit());
      Segment *oldSegment = &(*it);
      ++it;
      while(it != segments.end()) {
	Vector v = 
	  (oldSegment->getEndOffset().unit() * (1 / oldSegment->getEndOffset().length()) + 
	  it->getEndOffset().unit() * (1 / it->getEndOffset().length())).unit();

	oldSegment->setEndHeading(v);
	it->setStartHeading(v);

	oldSegment = &(*it);
	++it;
      }

      oldSegment->setEndHeading(oldSegment->getEndOffset().unit());
    }
  };


  Path::Path()
  {
    pImpl = new impl;
    pImpl->startHeading = Vector(1,0);
  }


  Path::Path(const Path &path)
  {
    pImpl = new impl(*(path.pImpl));
  }

  const Path &
  Path::operator=(const Path &path) {
    delete pImpl;
    pImpl = new impl(*(path.pImpl));
    return *this;
  }

  Path::~Path() {
    delete pImpl;
  }

  void
  Path::reset() 
  {
    pImpl->segments.clear();

    for (unsigned int i = 0;i< pImpl->initialSize;++i) {
      mutateExpand();
    }
  }

  void 
  Path::reset(int size)
  {
    pImpl->initialSize = size;
    reset();
  }

  void 
  Path::addSegment(const Segment &seg) throw (runtime_error) {
    if(! pImpl->segments.empty()) 
      {
// 	if(seg.getStartHeading().unit() != pImpl->segments.back().getEndHeading())
// 	  {
// 	    char str[100];
// 	    sprintf(str,"Path::addSegment(): the end headings do not collapse: %s <-> %s", 
// 		    seg.getStartHeading().str().c_str(), 
// 		    pImpl->segments.back().getEndHeading().str().c_str());

// 	    throw runtime_error(str);
// 	  }
      } 
    else
      {
	//if(seg.getStartHeading().unit() != pImpl->startHeading)
	  //throw runtime_error("Path::addSegment(): the path is not started with the same direction.");
      }
    pImpl->segments.push_back(seg);
    pImpl->updateHeadings();
  }

  void 
  Path::addPoint(const Vector &offset, const Vector &heading) {
    if(pImpl->segments.size() != 0) {
      Segment &seg = pImpl->segments.back();
      addSegment(Segment(seg.getEndOffset(), seg.getEndHeading(),offset,heading));
    }
    else {
      addSegment(Segment(pImpl->startOffset,pImpl->startHeading,offset,heading));
    }
    pImpl->updateHeadings();
  }

  SegmentEnumerator Path::enumerator() const {
    SegmentEnumerator pe;
    pe.pImpl->current = pImpl->segments.begin();
    pe.pImpl->end = pImpl->segments.end();
    return pe;
  }

  double
  Path::length() const {
    double len = 0.0;
    for(segIterator it = pImpl->segments.begin();it != pImpl->segments.end();++it) {
      len += it->getEndOffset().length();
    }
    return len;
  }
  
  /////////////////////////////////////////////////////////
  //     genetics region
  /////////////////////////////////////////////////////////
  void 
  Path::mutate2p() {
    // pick two points where we cross the two
    unsigned int n1 = random() % (pImpl->segments.size() - 1);
    unsigned int n2 = n1 + 1;

    Segment &seg1 = pImpl->segments[n1];
    Segment &seg2 = pImpl->segments[n2];

    double X = ((random() % 600) - 300)/100;
    double Y = ((random() % 600) - 300)/100;
    Vector vt(X,Y);
    seg1.setEndOffset(seg1.getEndOffset() + vt);
    seg2.setEndOffset(seg2.getEndOffset() - vt);
    pImpl->updateHeadings();
  }
  
  void 
  Path::mutate1p() {
    //NOT_IMPLEMENTED;
    int chance  = random() % 2;
    //chance = 1;
    switch(chance) {
    case 0:
      {
	unsigned int size = pImpl->segments.size();
	int id = random() % size;
	Segment &seg = pImpl->segments[id];
	Vector vt(((random() % 600) - 300)/10,
		  ((random() % 600) - 300)/10);
	seg.addEndOffset(vt);
	
	if(random() % 2 == 1)
	  return;
	
	double ang = (random() % 100 - 50)*M_PI/180;
	
	double cv = cos(ang);
	double sv = sin(ang);
	for (unsigned int i = id;i < size;++i) 
	  {
	    Segment &sg = pImpl->segments[i];
	    
	    Vector offs = sg.getEndOffset();
	    Vector head = sg.getEndHeading();
	    
	    Vector offset;
	    offset.setX(offs.getX() * cv - offs.getY() * sv);
	    offset.setY(offs.getX() * sv + offs.getY() * cv);
	    
	    //Vector direction;
	    //direction.setX(head.getX() * cv - head.getY() * sv);
	    //direction.setY(head.getX() * sv + head.getY() * cv);
	    
	    sg.setEndOffset(offset);
	    //sg.setEndHeading(direction);

	    //if(i != pImpl->segments.size()-1) 
	    //  {
	    //pImpl->segments[i+1].setStartHeading(direction);
	    //}
	  }
      }
      break;
    case 1:
      {
	int size = pImpl->segments.size();
	if(size <= 2)
	  break;
	int id = 1+ (random() % (size-2));
	Segment seg = pImpl->segments[id-1];
	segIterator it = pImpl->segments.begin();
	for(int i = 0;i < id;i++)
	  ++it;
	seg.addEndOffset(it->getEndOffset());
	//seg.setEndHeading(it->getEndHeading());
	pImpl->segments.erase(it);
      }
      break;
    default: break;
    }
    pImpl->updateHeadings();
  }
  
  void
  Path::mutateTo(const Vector & point) 
  {
    segIterator end = pImpl->segments.end();
    
    Vector vPos(0,0);
    Vector Dir;
    for(segIterator it = pImpl->segments.begin();it != end;++it) 
      {
	vPos += it->getEndOffset();
	Dir = it->getEndHeading();
      }
    OrientedPoint pStart;
    OrientedPoint pEnd;

    Vector vEnd = vPos - point;

    //pEnd.Direction = vEnd.unit();
    pEnd.Offset = vEnd;

    pStart.Direction = Dir;

    pImpl->updateHeadings();
  }
  
  void
  Path::crossover(Path &path, int pos) {
    Vector v1(0,0);
    Vector v2(0,0);

    Vector vOld1, vOld2;

    for(int i = 0; i< pos;i++) {
      Segment &s1 = pImpl->segments[i];
      Segment &s2 = path.pImpl->segments[i];

      vOld1 = v1;
      vOld2 = v2;

      v1 += s1.getEndOffset();
      v2 += s2.getEndOffset();
    }
    Vector f12 = v1 - vOld2;
    Vector f21 = v2 - vOld1;

    pImpl->segments[pos].setEndOffset(f12);
    //path.pImpl->segments[pos].setEndOffset(f21);

    // now we can swap the rest

    for(int i = 0; i< pos;i++) {
      Segment s1 = pImpl->segments[i];
      Segment s2 = path.pImpl->segments[i];

      pImpl->segments[i] = s2;
      path.pImpl->segments[i] = s1;
    }
    pImpl->updateHeadings();
  }

  int Path::size() const
  {
    return pImpl->segments.size();
  }
  
  void
  Path::mutateExpand() 
  {
    if(pImpl->segments.size() >= 10) 
      return;

    int chance = random() % 2;
    chance = 0;

    switch(chance) 
      {
      case 0:
	{
	  Vector startOffset;
	  Vector startHeading;
	  Vector endOffset;
	  Vector endHeading;
	  
	  if(pImpl->segments.size() == 0) 
	    {
	      startOffset = pImpl->startOffset;
	      startHeading = pImpl->startHeading;      
	    } 
	  else 
	    {
	      startOffset = pImpl->segments[pImpl->segments.size()-1].getEndOffset();
	      startHeading = pImpl->segments[pImpl->segments.size()-1].getEndHeading();
	    }
	  
	  
	  // add an orientation to this unit
	  endOffset = startHeading.unit()*20.0;

	  
	  // add some noise
	  endOffset += Vector(((random() % 600) - 300) / 100,
			      ((random() % 600) - 300) / 100);
	  
	  // transfer this to the direction
	  endHeading = endOffset;
	  
	  // add more noise to this 
	  endHeading += Vector(((random() % 500) - 250) / 100,
			       ((random() % 500) - 250) / 100);
	  
	  addSegment(Segment(startOffset,startHeading,endOffset,endHeading));
	}
	break;
      case 1:
	{
	  //cout << "A" << flush;
	  vSegment &segs = pImpl->segments;
	  int id = random() % segs.size()-1;

	  if(segs.size() <= 2)
	    return;

	  segIterator it = segs.begin();
	  for (int i = 0;i < id;++i)
	    ++it;

	  Vector dir = it->getEndOffset();

	  double v = (double)5;
	  double v2 = 10-v;

	  Segment s(dir * 0.5,it->getEndHeading(), dir * 0.5,it->getEndHeading());

	  //it->setEndOffset(dir * 0.5);
	  pImpl->insertSegment(it,s);	  
	  //cout << "B" << flush;
	}
	break;
      }
    pImpl->updateHeadings();
  }
}

