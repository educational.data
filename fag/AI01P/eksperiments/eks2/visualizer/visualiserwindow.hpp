#ifndef VISUALISERWINDOW_HPP_INCLUDED
#define VISUALISERWINDOW_HPP_INCLUDED
#include <gtkmm.h>
#include "path_drawer.hpp"

class VisualiserWindow : public Gtk::Window
{
  // private impl ideom
  struct impl;
  impl *pImpl;
public:
  VisualiserWindow();
  virtual ~VisualiserWindow();
  
  void set_path_drawer(struct IPathDrawer *da);
};

#endif
