#ifndef _PATH_DRAWABLE_HPP_INCLUDED__
#define _PATH_DRAWABLE_HPP_INCLUDED__

namespace graph {
  class Path;
}

namespace genetic {
  class Population;
}

struct PathDrawable {
  virtual int getWidth() = 0L;
  virtual int getHeight() = 0L;

  virtual void drawPoint(int x, int y) = 0L;
  virtual void drawLine(int x1, int y1, int x2, int y2) = 0L;
  virtual void drawCircle(int x, int y, int radius) = 0L;
  virtual void drawArc(int cx, int cy, double startA, double endA, int radius) = 0L;

  virtual void setBlueColor() = 0L;
  virtual void setRedColor() = 0L;
  virtual void setGreenColor() = 0L;
  virtual void setGrayColor() = 0L;
  virtual void setYellowColor() = 0L;
  virtual void setBlackColor() = 0L;

  virtual ~PathDrawable() {}
};

#endif
