#include <ltdl.h>
#include <iostream>
#include <string>

using namespace std;

extern "C" {
  void (*libtest_LTX_goahead)();
}

class loader {
  lt_dlhandle handle;
public:
  loader(string name) {
    lt_dlinit();
    //LTDL_SET_PRELOADED_SYMBOLS();
    handle = lt_dlopenext(name.c_str());
  }

  bool testErr() {
    const char *res = lt_dlerror();
    if(res != 0L) {
      std::cout << res << std::endl;
      return false;
    }
    return true;
  }
  
  template <class R> bool setFunction(R (**func)(),string name) 
  {
    lt_ptr ptr = lt_dlsym(handle, name.c_str());
    *func = (R (*)())(ptr);
    return testErr();
  }
  template <class R,class V1> bool setFunction(R (**func)(V1),string name) 
  {
    lt_ptr ptr = lt_dlsym(handle, name.c_str());
    *func = (R (*)(V1))(ptr);
    return testErr();
  }

  template <class R,class V1, class V2> bool setFunction(R (**func)(V1,V2),string name) 
  {
    lt_ptr ptr = lt_dlsym(handle, name.c_str());
    *func = (R (*)(V1,V2))(ptr);
    return testErr();
  }
  
  //libtest_LTX_goahead();
  //lt_dlclose(handle);


  ~loader() {
    lt_dlclose(handle);
    lt_dlexit();
  }
};

int main(int argc, char *args[]) {
  loader load("libtest");
  if(!load.setFunction(&libtest_LTX_goahead,"goahead2"))
    return -1;
  (*libtest_LTX_goahead)();
  return 0;
}

