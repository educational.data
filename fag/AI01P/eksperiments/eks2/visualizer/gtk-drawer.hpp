#ifndef __GTK_DRAWER_HPP_INCLUDED__
#define __GTK_DRAWER_HPP_INCLUDED__

#include <gtkmm/drawingarea.h>
#include <boost/shared_ptr.hpp>

namespace graph {
  class Path;
}

namespace genetic {
  class Population;
}

class IPathDrawer;

class Clock : public Gtk::DrawingArea
{
  struct impl;
  impl *pImpl;
public:
  Clock();
  virtual ~Clock();

  // void set_path(boost::shared_ptr<graph::Path> currentPath);
  // void set_population(boost::shared_ptr<genetic::Population> currentPopulation);
    void set_path_drawer(IPathDrawer *);
  bool onSecondElapsed(void);
protected:
  //Override default signal handler:
  virtual bool on_expose_event(GdkEventExpose* event);
  double m_radius;
  double m_lineWidth;
};

#endif // __GTK_DRAWER_HPP_INCLUDED__
