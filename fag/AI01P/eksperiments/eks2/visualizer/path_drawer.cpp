#ifdef HAVE_CONFIG_H
# include <config.h>
#else
#warning  Not configured properly
#endif

#ifdef HAVE_STDEXCEPT
# include <stdexcept>
#else
struct out_of_range
{
  out_of_range (const char *)
  {
  }
};
#endif

#include <iostream>
#include <math.h>

#include "path_drawable.hpp"
#include "path_drawer.hpp"

using namespace std;
using namespace graph;
using namespace genetic;
using namespace boost;

#include "vector.hpp"
#include "segment.hpp"
#include "path.hpp"
#include <plane.hpp>
#include <environment.hpp>

//#include "orientedpoint.hpp"

#include "uav-config.h"

static double abs(double i) {
  return i < 0.0 ? -i : i;
}

struct PathDrawer::impl 
{
  int width;
  int height;

  // flags setting drawing params
  bool bdPath;
  bool bdNormals;
  bool bdHeadings;
  bool bdCircles;

  // state that tells us if we may run or what to do
  uint test_state;

  double minX,minY,maxX,maxY;

  //shared_ptr<Population> pop, pop2;
  shared_ptr<Plane> plane;

  int toX(double x) 
  {
    double base = x - minX;
    double bLen = maxX - minX;

    double dm = min(height,width);
    double d = dm*base / bLen;
    return (int)((double)d);
  }

  int toY(double y) 
  {
    double base = y - minY;
    double bLen = maxY - minY;
    double dm = min(height,width);
    double d = dm*base / bLen;
    return (int)((double)d);
  }

  int toLength(double len)
  {
    double span1 = len/(maxX - minY);
    
    return (int)round((double)min(width,height) * span1);
  }

  void calculateExtends(const Path &path) 
  {
    SegmentEnumerator enu = path.enumerator();
    
    double x = 0.0;
    double y = 0.0;
    
    while(enu.hasNext()) 
      {
	Segment &seg = enu.next();
	
	optimizeExtends ( x + seg.getEndOffset().getX(),
			  y + seg.getEndOffset().getY());
	
	x += seg.getEndOffset().getX();
	y += seg.getEndOffset().getY();
      }
    
    maxX+=1.0;
    maxY+=1.0;
    minX-=1.0;
    minY-=1.0;
  }
  void resetExtends() 
  {
    minX = 0.0;
    minY = 0.0;
    maxX = 0.0;
    maxY = 0.0;
  }

  void optimizeExtends(double x, double y) 
  {
    minX = min ( minX, x );
    minY = min ( minY, y );
    
    maxX = max ( maxX, x );
    maxY = max ( maxY, y );
    
    maxX = max(-minX,maxX);
    minX = min(-maxX,minX);
    
    maxY = max(-minY,maxY);
    minY = min(-maxY,minY);
    
    maxY = maxX = max(maxX,maxY);
    minX = minY = min(minY,minX); 
  }

  void draw_seg1(PathDrawable *drawer) 
  {
    resetExtends();
    optimizeExtends(5,5);
    
    try {
      Path optimalPath;
      cout << "a"<< flush;
      optimalPath.addSegment(Segment(Vector(0,0),
				     Vector(1,0),
				     Vector(1,-1),
				     Vector(1,-1)));

      cout << "b"<< flush;

      optimalPath.addSegment(Segment(Vector(0,0),
				     Vector(1,-1),
				     Vector(3,0),
				     Vector(1,0)));
      cout << "c"<< flush;
      optimalPath.addSegment(Segment(Vector(0,0),
				     Vector(1,0),
				     Vector(1,-1),
				     Vector(1,-1)));
      cout << "d"<< flush;
      
      drawPath(optimalPath,drawer);
      cout << "*"<< endl;
    }
    catch(const exception &e) {
      cerr << endl << e.what() << endl;
      exit(0);
    }
  }

  void draw_seg2(PathDrawable *drawer) 
  {
    static double d = 0.1;
    resetExtends();
    optimizeExtends(5,2);
    d+=0.2;
    Path optimalPath;
    optimalPath.addSegment(Segment(Vector(0,0),
				   Vector(1,0),
				   Vector(1,0),
				   Vector(1,0)));

    optimalPath.addSegment(Segment(Vector(0,0),
				   Vector(1,0),
				   Vector(3,1),
				   Vector(1,1)));

    optimalPath.addSegment(Segment(Vector(0,0),
				   Vector(1,1),
				   Vector(0,1),
				   Vector(0,1)));
			   
    drawPath(optimalPath,drawer);
  }

  void drawObstacles(PathDrawable *drawer) {

    drawer->setGrayColor();
    //drawer->drawCircle(toX(50),toY(50),toLength(20));
    //drawer->drawCircle(toX(100),toY(80),toLength(10));

    Environment &env = plane->getEnvironment();

    Obses obs = env.getObstaclesAtTime(0);
    for(Obses_it it = obs.begin(); it != obs.end(); ++it) 
      {
	drawer->drawCircle(toX(it->position.getX()),
			   toY(it->position.getY()),
			   toLength(it->minDistToTarget));
      }
  }

  void drawLine(PathDrawable *drawer,const Vector &v1, const Vector &v2) {
    drawer->drawLine(toX(v1.getX()),toY(v1.getY()),
		     toX(v2.getX()),toY(v2.getY()));
  }

  void drawPlane(PathDrawable *drawer) {
    Vector planePos = plane->getCurrentPosition();
    drawer->setBlackColor();

    double scale=5.0;

    Angle angle = plane->getCurrentHeading().angle();

    Vector body1 = Vector(-0.5,0).rotate(angle)*scale + planePos;
    Vector body2 = Vector(0.2,0).rotate(angle)*scale + planePos;
    Vector MW1 = Vector(0,-0.6).rotate(angle)*scale + planePos;
    Vector MW2 = Vector(0,0.6).rotate(angle)*scale + planePos;
    Vector TW1 = Vector(-0.5,-0.2).rotate(angle)*scale + planePos;
    Vector TW2 = Vector(-0.5,0.2).rotate(angle)*scale + planePos;
    
    drawLine(drawer,body1,body2);
    drawLine(drawer,MW1,MW2);
    drawLine(drawer,TW1,TW2);
  }

  void drawTarget(PathDrawable *drawer) 
  {
    Vector target = plane->getCurrentTarget();
    drawer->setBlackColor();
    drawer->drawPoint(toX(target.getX()),toY(target.getY()));
  }

  void drawPath(const graph::Path &path, PathDrawable *drawer) 
  {
    width = drawer->getWidth();
    height = drawer->getHeight();
    
    SegmentEnumerator enu = path.enumerator();
    
    double x = 0.0;
    double y = 0.0;
    
    int n=0;
    
    while(enu.hasNext()) 
      {
	Segment &seg = enu.next();
	bool res = seg.calcData();
	Vector tp;
	
	double radius1 = seg.getTP1radius();
	double radius2 = seg.getTP2radius();
	
	if(radius1 < 0.0 && radius2 > 0.0||true) 
	  {
	    try 
	      {
		
		drawer->setGrayColor();
		if(!res && bdPath) 
		  { // straight line data
		    drawer->drawLine(toX(x),
				     toY(y),
				     toX(x + seg.getEndOffset().getX()),
				     toY(y + seg.getEndOffset().getY()));
		  }

		// draw normals for header data.
		if(bdHeadings)
		  {
		    drawer->setGreenColor();
		    drawer->drawLine(toX(x),
				     toY(y),
				     toX(x + (seg.getStartHeading().unit()).getX()),
				     toY(y + (seg.getStartHeading().unit()).getY()));
		    drawer->setBlueColor();
		    drawer->drawLine(toX(x + seg.getEndOffset().getX()),
				     toY(y + seg.getEndOffset().getY()),
				     toX(x + seg.getEndOffset().getX() + (seg.getEndHeading().unit()).getX()),
				     toY(y + seg.getEndOffset().getY() + (seg.getEndHeading().unit()).getY()));
		  }
		if(res)
		  {		
		    tp= seg.getTP1();
		    
		    if(bdCircles) 
		      {
			// drawer->setGreenColor();
			
			/*drawer->drawCircle(toX(tp.getX()+x),
			  toY(tp.getY()+y),
			  10);
			*/
			drawer->setRedColor();
			drawer->drawArc(toX(tp.getX() + x),
					toY(tp.getY() + y),
					seg.getTP1Angle1().value(),
					seg.getTP1Angle2().value(),
					toLength(radius1)
					);
			

			 drawer->drawPoint(toX(tp.getX() + x),
			   toY(tp.getY() + y));


			tp = seg.getTP2();	

			
			/* drawer->drawCircle(
			   toX(tp.getX()+x),
			   toY(tp.getY()+y),
			   10);
			*/
			drawer->setYellowColor();

			drawer->drawPoint(toX(tp.getX()+x),
					  toY(tp.getY()+y));
			

			drawer->drawArc(toX(tp.getX()+x),
					toY(tp.getY()+y),
					seg.getTP2Angle1().value(),
					seg.getTP2Angle2().value(),
					toLength(radius2)
					);
		    
		      }
		    cout << radius1 << " " << radius2 << endl;
		    res = false;
		  }
	      }
	    catch(const exception &ex) 
	      {
		cout << "exception: " << ex.what() << endl;
	      }
	  }
	
	
	
	if(!res && bdPath)
	  {
	    drawer->setBlackColor();
	    drawer->drawLine(toX(x),
			     toY(y),
			     toX(x + seg.getEndOffset().getX()),
			     toY(y + seg.getEndOffset().getY()));
	  }
	drawer->setBlueColor();
	//drawer->drawPoint(toX(x), toY(y));
	x += seg.getEndOffset().getX();
	y += seg.getEndOffset().getY();
	
	//drawer->drawPoint(pImpl->toX(x), pImpl->toY(y));
      }
  }
};
  
PathDrawer::PathDrawer(boost::shared_ptr<Plane> plane) 
{
  pImpl = new impl;

  pImpl->plane = plane;
  
  pImpl->resetExtends();
}

PathDrawer::~PathDrawer() 
{
  delete pImpl;
}

void PathDrawer::tick_a_sec() 
{
  if(pImpl->test_state != IPathDrawer::TEST_NORMAL)
    return;

  /* pImpl->pop->initPaths(30);
  pImpl->pop2->initPaths(30);

  for(int i = 0;i<40;++i) {
    pImpl->pop->evolve(); 
    pImpl->pop2->evolve(); 
    if(i % 10 == 0)
      pImpl->pop->crossPopulation(*(pImpl->pop2));
  }
  */
  try {
    pImpl->plane->move_n_secs(1);
  } catch(const exception &ex) {
    cerr << ex.what() << endl;
  }
}

void PathDrawer::drawSystem(PathDrawable *drawer)
{
  if(pImpl->test_state == IPathDrawer::TEST_DISABLE)
    return;

  if(pImpl->test_state == IPathDrawer::TEST_SEGMENT1)
    return pImpl->draw_seg1(drawer);

  if(pImpl->test_state == IPathDrawer::TEST_SEGMENT2)
    return pImpl->draw_seg2(drawer);

  if(pImpl->plane == NULL)
    return;

  
  Vector target = pImpl->plane->getCurrentTarget();

  pImpl->resetExtends();

  pImpl->optimizeExtends(target.getX(),
			 target.getY());

  
  /* for(PathEnumerator en = pop.enumerator();en.hasNext();) {
    Path &path = en.next();
    pImpl->calculateExtends(path);
    } */
  try 
    {
      pImpl->calculateExtends(pImpl->plane->getCurrentPath());
      pImpl->drawPath(pImpl->plane->getCurrentPath(),drawer);
    } 
  catch(const exception &ex) {
    cerr << ex.what() << endl;
    //exit(-1);
  }

  pImpl->drawPlane(drawer);
  pImpl->drawTarget(drawer);
  pImpl->drawObstacles(drawer);

  /* for(PathEnumerator en = pop.enumerator();en.hasNext();) {
    Path &path = en.next();
    drawPath(path,drawer);
    }*/
}

void PathDrawer::setTest(uint val) {
  pImpl->test_state = val;
}

uint PathDrawer::getTest() {
  return pImpl->test_state;
}


void PathDrawer::setFlag(uint flag, bool data) {
  switch (flag) 
    {
    case FLAG_DRAW_PATH:
      pImpl->bdPath = data;
      break;
    case FLAG_DRAW_NORMALS:
      pImpl->bdNormals = data;
      break;
    case FLAG_DRAW_HEADINGS:
      pImpl->bdHeadings = data;
      break;
    case FLAG_DRAW_CIRCLES:
      pImpl->bdCircles = data;
      break;
    default:
      cerr << "Unknown flag" << endl;
    }
}

bool PathDrawer::getFlag(uint flag) 
{
  switch (flag) 
    {
    case FLAG_DRAW_PATH:
      return pImpl->bdPath;

    case FLAG_DRAW_NORMALS:
      return pImpl->bdNormals;

    case FLAG_DRAW_HEADINGS:
      return pImpl->bdHeadings;

    case FLAG_DRAW_CIRCLES:
      return pImpl->bdCircles;

    default:
      cerr << "Unknown flag" << endl;
    }
}
