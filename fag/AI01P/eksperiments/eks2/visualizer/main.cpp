#include <ltdl.h>
#include <iostream>
#include "path_drawer.hpp"
#include <boost/shared_ptr.hpp>
#include <population.hpp>
#include <vector.hpp>

#define LIB_GTKMMNAME "libmygtkmm"

using namespace std;

int (* libvis_buildSys)(int argc, char *argv[]);
void (* libvis_setdrawer)(IPathDrawer *);
void (* libvis_run)();
void (* libvis_stop)();

bool testErr() {
  const char *res = lt_dlerror();
  if(res != 0L) {
    cout << res << endl;
    exit(-1);
    return false;
  }
  else 
    return true;
}

int main(int argc, char *argv[]) {
  {
    lt_dlinit();

    lt_dlhandle handle = lt_dlopenext(LIB_GTKMMNAME);
    testErr();
    libvis_buildSys = (int (*)(int,char **))lt_dlsym(handle,"libvis_buildSys");testErr();
    libvis_run = (void (*)())lt_dlsym(handle,"libvis_run");testErr();
    libvis_stop = (void (*)())lt_dlsym(handle,"libvis_stop");testErr();
    libvis_setdrawer = (void (*)(IPathDrawer *))lt_dlsym(handle,"libvis_setdrawer");testErr();
  }
  libvis_buildSys(argc, argv);

  boost::shared_ptr<genetic::Population> pop, pop2;
  pop.reset(new genetic::Population);
  pop2.reset(new genetic::Population);
  
  pop->initPaths(50);
  pop->setTarget(graph::Vector(100,100));

  pop2->initPaths(50);
  pop2->setTarget(graph::Vector(100,100));

  PathDrawer pd(pop,pop2);

  libvis_setdrawer(&pd);
  
  libvis_run();
}
