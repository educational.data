#ifndef _PATH_DRAWER_HPP_INCLUDED__
#define _PATH_DRAWER_HPP_INCLUDED__

#include <boost/shared_ptr.hpp>

namespace graph {
  class Path;
}

class PathDrawable;
class Plane;

class IPathDrawer {
public:
  static const unsigned int TEST_DISABLE      = 0;
  static const unsigned int TEST_NORMAL       = 1;
  static const unsigned int TEST_SEGMENT1     = 2;
  static const unsigned int TEST_SEGMENT2     = 3;

  static const unsigned int FLAG_DRAW_PATH     = 0;
  static const unsigned int FLAG_DRAW_NORMALS  = 1;
  static const unsigned int FLAG_DRAW_HEADINGS = 2;
  static const unsigned int FLAG_DRAW_CIRCLES  = 3;

  virtual ~IPathDrawer() { };

  virtual void drawSystem(PathDrawable *drawer) = 0L;
  virtual void tick_a_sec() = 0L;
  virtual void setTest(unsigned int id) = 0L;
  virtual unsigned int getTest() = 0L;

  virtual void setFlag(uint flag, bool value) = 0L;
  virtual bool getFlag(uint flag) = 0L;
};

class PathDrawer : public IPathDrawer {
  struct impl;
  impl *pImpl;
public:
  PathDrawer(boost::shared_ptr<Plane> plane);

  ~PathDrawer();

  void tick_a_sec();
  //void drawPath(const graph::Path &path, PathDrawable *drawer);
  void drawSystem(PathDrawable *drawer);

  void setTest(unsigned int id);
  unsigned int getTest();
  void setFlag(uint flag, bool value);
  bool getFlag(uint flag);
};

#endif
