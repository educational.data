#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include "population.hpp"
#include "vector.hpp"
#include "math.h"
#include "visualiserwindow.hpp"

void buildGraph(VisualiserWindow &window) {
  boost::shared_ptr<genetic::Population> pop;
  pop.reset(new genetic::Population);

  pop->initPaths(10);
  pop->setTarget(graph::Vector(100,100));

  //window.setPopulation(pop);
}

struct sys {
  Gtk::Main kit;
  VisualiserWindow window;

  sys(int argc, char *argv[]) : kit(argc,argv) {
    
  }

  void run() {
    Gtk::Main::run(window); //Shows the window and returns when it is closed.
  }
};

static sys *mSys;

extern "C" {
  int libvis_buildSys(int argc, char *argv[])
  {
    mSys = new sys(argc,argv);
    
    //buildGraph(mSys->window);
    
    return 0;
  }

  void libvis_run() {
    mSys->run();
  }

  void libvis_stop() {
    delete mSys;
  }

  void libvis_setdrawer(struct IPathDrawer *da) {
    mSys->window.set_path_drawer(da);
  }
}

