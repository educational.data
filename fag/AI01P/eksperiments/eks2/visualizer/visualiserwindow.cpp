#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "gtk-drawer.hpp"
#include "path_drawer.hpp"
#include "visualiserwindow.hpp"
#include <iostream>

using namespace std;



struct VisualiserWindow::impl 
{
  void closeSystem() 
  {
    cout << "Signaled to quit now!" << endl;
    Gtk::Main::quit();
  }
  
  void toggleFlags() 
  {
    drawer->setFlag(IPathDrawer::FLAG_DRAW_PATH,cbDPaths->get_active());
    drawer->setFlag(IPathDrawer::FLAG_DRAW_NORMALS,cbDNormals->get_active());
    drawer->setFlag(IPathDrawer::FLAG_DRAW_HEADINGS,cbDHeadings->get_active());
    drawer->setFlag(IPathDrawer::FLAG_DRAW_CIRCLES,cbDCircles->get_active());
  }

  void time_change() 
  {
    Glib::ustring text = cTid->get_active_text();
    TIMELOOP = atoi(text.c_str());
    cout << "Changed time to " << text << endl;
  }

  void toggleRun() 
  {
    if(drawer) 
      {
	uint tval = drawer->getTest();
	
	if(tval != IPathDrawer::TEST_DISABLE)
	  tval = IPathDrawer::TEST_DISABLE;
	else
	  tval = IPathDrawer::TEST_NORMAL;
	
	drawer->setTest(tval);
      }
  }

  void draw_segment1() 
  {
    cout << "draw segment type one!" << endl;
    if(drawer)drawer->setTest(IPathDrawer::TEST_SEGMENT1);
  }
  
  void draw_segment2() 
  {
    cout << "draw segment type two!" << endl;
    if(drawer)drawer->setTest(IPathDrawer::TEST_SEGMENT2);;
  }

  bool onSecondElapsed() 
  {
    population_visualiser->onSecondElapsed();
    Glib::signal_timeout().connect(sigc::mem_fun(this, &impl::onSecondElapsed), TIMELOOP);
    return false;
  }

  bool mustrun;
  unsigned int TIMELOOP;
  Gtk::Button *bRun;
  Gtk::CheckButton *cbDPaths;
  Gtk::CheckButton *cbDNormals;
  Gtk::CheckButton *cbDHeadings;
  Gtk::CheckButton *cbDCircles;

  Gtk::ComboBoxText *cTid;
  
  Clock *population_visualiser;
  IPathDrawer *drawer;  
};

using namespace boost;
VisualiserWindow::VisualiserWindow()
{
  /* Set some window properties */
  set_title ("Visualization of 1 population.");
  set_size_request (800, 600);

  /* Sets the border width of the window. */
  set_border_width (5);

  pImpl = new impl;
  pImpl->TIMELOOP = 1000;

  Gtk::Frame &frame    = * manage(new Gtk::Frame);
  Gtk::Button &button  = * manage(new Gtk::Button("  _Close  ", true));
  Gtk::Button &btnSeg1 = * manage(new Gtk::Button("Draw segment type 1", false));
  Gtk::Button &btnSeg2 = * manage(new Gtk::Button("Draw segment type 2", false));
  Gtk::VBox &vbox      = * manage(new Gtk::VBox(false,10));
  Gtk::HBox &hbox1     = * manage(new Gtk::HBox(false,10));
  Gtk::HBox &hbox2     = * manage(new Gtk::HBox);
  Gtk::VBox &vbox2     = * manage(new Gtk::VBox(false,2));
  pImpl->bRun          =   manage(new Gtk::Button("Toggle Run"));
  pImpl->cbDPaths      =   manage(new Gtk::CheckButton("Draw paths"));
  pImpl->cbDNormals    =   manage(new Gtk::CheckButton("Draw normals"));
  pImpl->cbDHeadings   =   manage(new Gtk::CheckButton("Draw headings"));
  pImpl->cbDCircles    =   manage(new Gtk::CheckButton("Draw circles"));
  pImpl->cTid          =   manage(new Gtk::ComboBoxText);
  pImpl->population_visualiser 
                       =   manage(new Clock);

  //pImpl->cb2->set_sensitive(false);

  pImpl->cTid->append_text("250");
  pImpl->cTid->append_text("500");
  pImpl->cTid->append_text("1000");

  pImpl->cTid->signal_changed().connect(sigc::mem_fun(pImpl,&impl::time_change));
  pImpl->cTid->set_active_text("250");

  vbox.pack_start(hbox1);
  vbox.pack_start(*manage(new Gtk::HSeparator),Gtk::PACK_SHRINK);
  vbox.pack_end(hbox2,Gtk::PACK_SHRINK);
  hbox2.pack_start(*manage(new Gtk::Label("Simulator by Tobias Nielsen - tobibobi@gmail.com")),Gtk::PACK_SHRINK);
  hbox2.pack_end(button,Gtk::PACK_SHRINK);
  hbox1.pack_start(frame);
  hbox1.pack_end(vbox2 , Gtk::PACK_SHRINK);
  
  // configure pane
  vbox2.pack_start(* manage(new Gtk::Label("Control of application")),Gtk::PACK_SHRINK);
  vbox2.pack_start(* manage(new Gtk::HSeparator),Gtk::PACK_SHRINK);
  vbox2.pack_start(*pImpl->bRun,Gtk::PACK_SHRINK);
  vbox2.pack_start(* manage(new Gtk::HSeparator),Gtk::PACK_SHRINK);
  vbox2.pack_start(* manage(new Gtk::Label("Timing:")),Gtk::PACK_SHRINK);
  vbox2.pack_start(*pImpl->cTid, Gtk::PACK_SHRINK);
  vbox2.pack_start(* manage(new Gtk::HSeparator),Gtk::PACK_SHRINK);
  vbox2.pack_start(* manage(new Gtk::Label("Drawing:")),Gtk::PACK_SHRINK);
  vbox2.pack_start(btnSeg1,Gtk::PACK_SHRINK);
  vbox2.pack_start(btnSeg2,Gtk::PACK_SHRINK);
  vbox2.pack_start(* manage(new Gtk::HSeparator),Gtk::PACK_SHRINK);
  vbox2.pack_start(* manage(new Gtk::Label("Flag:")),Gtk::PACK_SHRINK);
  vbox2.pack_start(*pImpl->cbDPaths, Gtk::PACK_SHRINK);
  vbox2.pack_start(*pImpl->cbDNormals, Gtk::PACK_SHRINK);
  vbox2.pack_start(*pImpl->cbDHeadings, Gtk::PACK_SHRINK);
  vbox2.pack_start(*pImpl->cbDCircles, Gtk::PACK_SHRINK);



  pImpl->cbDPaths->set_active(true);
  pImpl->cbDNormals->set_active(true);
  pImpl->cbDHeadings->set_active(true);
  pImpl->cbDCircles->set_active(true);

  button.signal_clicked().connect(sigc::mem_fun(pImpl,&impl::closeSystem));
  pImpl->bRun->signal_clicked().connect(sigc::mem_fun(pImpl,&impl::toggleRun));
  pImpl->cbDPaths->signal_toggled().connect(sigc::mem_fun(pImpl,&impl::toggleFlags));
  pImpl->cbDNormals->signal_toggled().connect(sigc::mem_fun(pImpl,&impl::toggleFlags));
  pImpl->cbDHeadings->signal_toggled().connect(sigc::mem_fun(pImpl,&impl::toggleFlags));
  pImpl->cbDCircles->signal_toggled().connect(sigc::mem_fun(pImpl,&impl::toggleFlags));

  //cb2->signal_toggled().connect(&toggle);
  btnSeg1.signal_clicked().connect(sigc::mem_fun(pImpl,&impl::draw_segment1));
  btnSeg2.signal_clicked().connect(sigc::mem_fun(pImpl,&impl::draw_segment2));


  add(vbox);

  /* Set the frames label */
  frame.set_label("Population");
  frame.add(*pImpl->population_visualiser);
  pImpl->mustrun = false;

  
  /* Align the label at the right of the frame */
  //frame.set_label_align(Gtk::ALIGN_RIGHT, Gtk::ALIGN_TOP);

  /* Set the style of the frame */
  //frame.set_shadow_type(Gtk::SHADOW_ETCHED_IN);

  show_all_children();
}

VisualiserWindow::~VisualiserWindow()
{
  if (pImpl != NULL)
    delete pImpl;
}

void VisualiserWindow::set_path_drawer(struct IPathDrawer *da) {
  pImpl->population_visualiser->set_path_drawer(da);
  pImpl->drawer = da;
  da->setTest(IPathDrawer::TEST_DISABLE);
  pImpl->toggleFlags();
  Glib::signal_timeout().connect(sigc::mem_fun(pImpl, &VisualiserWindow::impl::onSecondElapsed), 1000);
}

// void 
// VisualiserWindow::setPopulation(boost::shared_ptr<genetic::Population> rPop)
// {
//   population_vizualiser.set_population(rPop);
// }
