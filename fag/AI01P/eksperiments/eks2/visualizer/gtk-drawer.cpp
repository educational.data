#include <ctime>
#include "math.h"
#include "gtk-drawer.hpp"
//#include "path.hpp"
#include "vector.hpp"
#include "path_drawable.hpp"
#include "path_drawer.hpp"
#include "population.hpp"

#include <iostream>

struct Clock::impl : public PathDrawable {
  Glib::RefPtr<Gdk::Window> _window;
  Glib::RefPtr<Gdk::GC> _gc;  

  Clock &rThis;

  IPathDrawer *pDrawer;
  
  /**
   * @brief Setup and draw a system.
   * 
   * Sets up a population for the system to draw.
   *
   * @callgraph
   */
  void drawPath(Glib::RefPtr<Gdk::Window> window, Glib::RefPtr<Gdk::GC> gc) {
    using namespace graph;
    using namespace std;
    
    this->_gc = gc;
    this->_window = window;
    if(pDrawer != NULL)
      pDrawer->drawSystem(this);
  }

  
  /**
   * @brief draw a single blue point using a cross.
   */
  void drawPoint(int x, int y) {
    _window->draw_line( _gc, x, y-5, x, y+5);
    _window->draw_line( _gc, x-5, y, x+5, y);
  }
  

  /**
   * @brief Draw a single black point using a cross.
   *
   * This method is internal and can only be used as such.
   */
  void drawPoint(Glib::RefPtr<Gdk::Window> window, 
		 Glib::RefPtr<Gdk::GC> gc, 
		 graph::Vector &point) {
    using namespace graph;
    using namespace std;
    
    window->draw_line(gc,
		      (int)(point.getX()*10),
		      (int)(point.getY()*10-5),
		      (int)(point.getX()*10),
		      (int)(point.getY()*10+5));
    
    window->draw_line(gc,
		      (int)(point.getX()*10-5),
		      (int)(point.getY()*10),
		      (int)(point.getX()*10+5),
		      (int)(point.getY()*10));
  }
  
  /**
   * @brief Draw a circle using the described offsets.
   *
   * The position is actually offset so the given parametrers 
   * is offset in the center of the circle.
   */
  void drawCircle(int x, int y, int radius) 
  {
    _window->draw_arc(_gc,false,x-radius,y-radius,radius*2,radius*2,0,360*64);
  }

  void drawLine(int x1, int y1, int x2, int y2) {
    _window->draw_line( _gc, x1, y1, x2, y2);
  }

  void drawArc(int cx, int cy, double startA, double endA, int radius)
  {
    int tx = cx - radius;
    int ty = cy - radius;
    
//     if(startA > endA) 
//       {
// 	drawArc( cx, cy, endA, (2*M_PI)-endA, radius);
// 	drawArc( cx, cy, 0, startA, radius);
// 	return;
//       }
    
//     if(startA < 0 )startA += 2*M_PI;
//     if(endA < 0 )endA += 2*M_PI;
    
    double d = (endA-startA);
    if(d<=0) 
      d += 2*M_PI;
    int aA = (int)(180*64.0*startA/M_PI);
    int bA = (int)(180*64.0*d/M_PI);

    _window->draw_arc(_gc,false,tx,ty,radius*2,radius*2,aA,bA);
  }

  int getWidth() {
    Gtk::Allocation allocation = rThis.get_allocation();
    return allocation.get_width();
  }

  int getHeight() {
    Gtk::Allocation allocation = rThis.get_allocation();
    return allocation.get_height();
  }

  /**
   * @name color Color functions
   *
   * These functions will be used to change the default color for 
   * a basic element
   * @{
   **/
  void setBlueColor() {
    _gc->set_foreground( blue_ );
  }
  void setRedColor() {
    _gc->set_foreground( red_ );
  }
  void setGreenColor() {
    _gc->set_foreground( green_ );
  }
  void setGrayColor() {
    _gc->set_foreground( grey_ );
  }
  void setYellowColor() {
    _gc->set_foreground( yellow_ );
  }
  void setBlackColor() {
    _gc->set_foreground( black_ );
  }
  /// @}
  
  impl(Clock *clk) : rThis(*clk) 
  { 
    pDrawer = NULL;
  }
  ~impl() { }

  Gdk::Color blue_, red_, green_, black_, white_, grey_, yellow_;
};

Clock::Clock()
  : m_radius(0.42), m_lineWidth(0.05)
{

  
  Glib::RefPtr<Gdk::Colormap> colormap = get_default_colormap();
  pImpl = new impl(this);

  pImpl->blue_ = Gdk::Color("blue");
  pImpl->red_ = Gdk::Color("red");
  pImpl->green_ = Gdk::Color("green");
  
  pImpl->black_ = Gdk::Color("black");
  pImpl->white_ = Gdk::Color("white");
  pImpl->grey_ = Gdk::Color("grey");

  pImpl->yellow_ = Gdk::Color("yellow");

  colormap->alloc_color(pImpl->blue_);
  colormap->alloc_color(pImpl->red_);
  colormap->alloc_color(pImpl->green_);
  
  colormap->alloc_color(pImpl->black_);
  colormap->alloc_color(pImpl->white_);
  colormap->alloc_color(pImpl->grey_);

  colormap->alloc_color(pImpl->yellow_);   

}

Clock::~Clock()
{
  delete pImpl;
}

bool Clock::on_expose_event(GdkEventExpose* event)
{
  Gtk::DrawingArea::on_expose_event(event);
  // This is where we draw on the window
  Glib::RefPtr<Gdk::Window> window = get_window();
  if(window)
  {
    /* Gtk::Allocation allocation = get_allocation();
    const int width = allocation.get_width();
    const int height = allocation.get_height();
    */

    Glib::RefPtr<Gdk::GC> gc = Gdk::GC::create(window);
    window->set_background(pImpl->white_);
    window->clear();
    pImpl->drawPath(window,gc);
  }

  return true;
}

void 
Clock::set_path_drawer(IPathDrawer *drawer) {
  pImpl->pDrawer = drawer;
}

bool Clock::onSecondElapsed(void)
{
    // force our program to redraw the entire clock.
    Glib::RefPtr<Gdk::Window> win = get_window();
    if (win)
      {
	if(pImpl->pDrawer)
	  pImpl->pDrawer->tick_a_sec();
	Gdk::Rectangle r(0, 0, get_allocation().get_width(),
			 get_allocation().get_height());
	win->invalidate_rect(r, false);
      }
    return true;
}
