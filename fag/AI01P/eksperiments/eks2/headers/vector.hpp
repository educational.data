// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef __VECTOR_HPP_INCLUDED__
#define __VECTOR_HPP_INCLUDED__

#include <boost/operators.hpp>
#include <stdexcept>

namespace graph {
  using namespace std;

  class Angle : public boost::addable< Angle , 
		 boost::multipliable2< Angle , double , 
		 boost::subtractable < Angle > > >
  {
    double angle;
    void sizeIt();    
  public:
    Angle();
    Angle(double val);
    Angle operator+=(const Angle &ang);    
    Angle operator*=(const double val);    
    Angle operator-=(const Angle &ang);
    const float degrees() const;
    const Angle negative() const;
    const double value() const;
  };
  
  bool operator < (const Angle &a, const Angle &b);
  bool operator > (const Angle &a, const Angle &b);
  bool operator <= (const Angle &a, const Angle &b);
  bool operator >= (const Angle &a, const Angle &b);
  
  /*!
   * \author Tobias Nielsen tobibobi@gmail.com
   * 
   *  Class for representing a vector and give basic algebra functions 
   * for the manipulation of the vector.
   *
   *  The vector supports adding, subtraction and multiplication
   * through the boost operators. 
   *
   *  Of speciallity it can also be created with a Angle operator which
   * gives the possibility to create a unit vector pointing in the
   * specific diretion.
   *
   *  Accessing the different entities in the vector must be done
   * through either the constant #getX(), #getY() methods for reading,
   * and simular the #set() functionality.
   */
  class Vector : public boost::addable< Vector, 
	         boost::subtractable< Vector,
		 boost::multipliable2< Vector, double > > >
  {
    void sizeIt();
  protected:
    double X, Y;
  public:
    /// \name Constructors
     
    /// @{
    Vector();
    Vector(const Angle &ang);
    virtual ~Vector();
    Vector(const float x, const float y);    
    /// @}
    Vector &operator+=(const Vector &vect);    
    Vector &operator-=(const Vector &vect);    
    Vector &operator*=(const double value);    

    Vector operator-() const {
      return *this * -1;
    }
    double length() const;    

    double dot(const Vector &v) const;
    double norm() const;
    double d(const Vector &v) const;

    //! \brieaf generates a 1 unit length version of this vector */
    const Vector unit() const;
    const Vector transpose() const;    
    const Vector reverse() const;
    
    /**
     * \brief Compute the angle of the present vector.
     *
     * The function is build by using a method basic from stdlib \c atan2 function.
     */
    Angle angle() const throw(runtime_error);

    bool operator==(const Vector &vec) const;
    bool operator!=(const Vector &vec) const;
    
    /*! \name Reading functions
     * The following functions give the user the posibility to read
     * data directly from the internal parametres. Remember that these
     * functions is implemented as const which prohibits access to its 
     * members.
     *@{
     */
    double getX() const {
      return X;
    }
    double getY() const {
      return Y;
    }
    /*!@}
     * \name Setting functions
     *
     * In the following functions, you modify the internal state of
     * the vector. Remember that these functions is not available if
     * the vector is \tt const
     * @{
     */
    void set(double x, double y) {
      X = x;
      Y = y;
    }

    void setX(double x) {
      X = x;
    }

    void setY(double y) {
      Y = y;
    }
    /*!@}*/

    const Vector rotate(const Angle &ang);

    string str() const;
  };

  /**
   * @Deprecated use vector instead
   */
  struct Point {
    Point();
    Point(double x, double y);
    Point(const Vector &v);
    operator Vector();

    double X;
    double Y;
  };

  ostream & operator<<(ostream &str, const Angle &ang);
  ostream & operator<<(ostream &str, const Vector &vec);
}

#endif
