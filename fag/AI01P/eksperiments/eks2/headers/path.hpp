// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

/*!
 * @file path.hpp
 * @author Tobias Nielsen tobibobi@gmail.com
 */

#ifndef __PATH_HPP_INCLUDED__
#define __PATH_HPP_INCLUDED__

#include <stdexcept>

namespace graph {
  class Segment;
  //struct OrientedPoint;
  class Vector;

  class SegmentEnumerator {
    struct impl;
    impl *pImpl;
    friend class Path;
    SegmentEnumerator();
  public:    
    SegmentEnumerator(const SegmentEnumerator &en);
    ~SegmentEnumerator();

    bool hasNext();
    Segment &next();
  };

  class Path {
    struct impl;
    impl *pImpl;
  public:
    Path();
    ~Path();
    Path(const Path &path);
    const Path &operator=(const Path &path);

    void addSegment(const Segment &seg) throw (std::runtime_error);

    void addPath(const Path &path);
    void addPoint(const Vector& offset, const Vector& heading);

    /**
     * @brief Resets the path to an initial size.
     * Generally regenerates a new path and tries to get a 
     * path that is able to be executed and evolved.
     * 
     * The size is set with the first call to #reset(int)
     */
    void reset();
    
    /**
     * @brief Resets the path to an initial size.
     * Generally regenerates a new path and tries to get a 
     * path that is able to be executed and evolved.
     *
     * The system is reset to the initial size set with size
     * @param size number of steps in a single path.
     */
    void reset(int size);

    double length() const;
    int size() const;

    SegmentEnumerator enumerator() const;

    // genetic region
    void mutateExpand();
    void mutate1p();
    void mutate2p();

    void mutateTo(const Vector & point);
    void crossover(Path &path, int pos);
  };
}

#endif
