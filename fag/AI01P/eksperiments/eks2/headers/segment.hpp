// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef __SEGMENT_HPP_INCLUDED__
#define __SEGMENT_HPP_INCLUDED__

#include <stdexcept>

namespace graph {
  class Vector;
  class Angle;

  class Segment {
    struct impl;
    impl *pImpl;
  public:
    Segment();
    Segment(const Vector& startOffset, const Vector& startHeading,
	    const Vector& endOffset, const Vector& endHeading);

    Segment(const Segment &segment);
    ~Segment();
    /** 
     * @name Positions functions.
     * @{
     */
    Vector getEndOffset() const;
    Vector getEndHeading() const;

    Vector setEndOffset(const Vector & vec);
    Vector setEndHeading(const Vector & vec);

    Vector addEndOffset(const Vector & vec);
    Vector addEndHeading(const Vector & vec);

    Vector getStartOffset() const;
    Vector getStartHeading() const;

    Vector setStartOffset(const Vector & vec);
    Vector setStartHeading(const Vector & vec);
    
    Vector addStartOffset(const Vector & vec);
    Vector addStartHeading(const Vector & vec);
    ///@}

    const Segment &operator=(const Segment &seg);

    const Vector getDirectionAfterNSteps(const int cSecs);

    /**
     * @brief length of segment.
     *
     * Evaluate the length of this intere path segment
     */
    double length() const;

    double square_length() const;
    /**
     * @name Calculate curves.
     *
     * These functions is essentially a wrap 
     * for impl::calcSegment()
     * 
     * @todo Calculate all the different segments, 
     *       normals and offsets needed to complete 
     *       this step.
     * @{
     * @bief Calculates the curvedate for one sepcific curve,
     * @return true if the data is curve data and false if 
     *       its a straight line.
     */
    bool calcData();

    Vector getTP1() const;
    Vector getTP2() const;
    double getTP1radius() const;
    double getTP2radius() const;

    Angle getTP1Angle1() const;
    Angle getTP1Angle2() const;

    Angle getTP2Angle1() const;
    Angle getTP2Angle2() const;
    ///@}
    
    /** 
     * @name Convenience functions.
     * 
     * Calculates the minimum radius that this solution constitutes - 
     * needed for determining if the UAV is able to turn this fast.
     * @{
     */
    double getMinRadius();
    double getMaxRadius();
    ///@}

    /**
     * @brief Get the distance from this segment to any point available in this world.
     */
    double distanceToPoint(const Vector &offset, const Vector &point);
  };
}

#endif
