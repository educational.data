// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

/*!
 * @brief Implementation of an population and its interfaces.
 *
 * @file population.hpp
 * @author Tobias G�nge tobibobi@gmail.com
 * @version $Revision: 1.5 $
 */

#ifndef __POPULATION_HPP_INCLUDED__
#define __POPULATION_HPP_INCLUDED__

namespace graph { 
  class Path;
  class Vector;
}

namespace genetic {
  class Population;
  
  /*!
   * @brief Iterator class implemented for iterating populations paths
   *
   * In order to use this class one can use the following code:
   * @code
   * PathEnumerator enu = population.enumerator();
   * while( enu.hasNext() == true ) {
   *   Path &p = enu.next();
   *   // do work on path
   * }
   * @endcode
   *
   * The class is a helper class that gives access to internal data in
   * much the same way as described in the iterator pattern. Its only
   * possible to obtain an object by calling the
   * #Poppulation::enumerator() function, but then the object can
   * easily be copied if the user prefers to maintain local state.
   *
   * @see Population
   */
  class PathEnumerator {
    friend class Population;
    struct impl;
    impl *pImpl;

    PathEnumerator();
  public:
    PathEnumerator(const PathEnumerator &ignore);
    ~PathEnumerator();
    
    /**
     * @brief See if there is anymore data available in collection.
     */
    bool hasNext();

    /**
     * @brief Retrive next item in list and advance one step.
     *
     * @throws runtime_exception if an attempt is made to go past the
     *         end of the collection.
     * @return a reference to the item - this is the ACTUAL item, so
     *         any modifications will affect the internal path.
     */
    graph::Path& next();
  };


  /**
   * @brief Single population that handles the intire evolution of the
   *        population. 
   *
   * @section introducton Introduction
   * This is in princple the centric class for this system, even
   * though this is not the class that by it self a intensive class as
   * most of the actual labour is done in a series of the agregated
   * classes. 
   *
   * @section intention Intentions
   * First of all this is the point where all the different generated
   * genes is gathered in to an intire poppulation. Directly in this
   * population life is built directly from maiding (crossover) and
   * mutation.
   *
   * @subsection principle Principle of implementation
   * The idea is that this class handles out responsibilitues to
   * different aggregated subclasses like #graph::path for mutation
   * and crossover and #genetic::fitnessevaluator for evaluation of
   * the quality of the different path.
   */
  class Population {
    struct impl;
    impl *pImpl;

  public:
    Population();
    ~Population();

    /*! 
     * @brief Returns the size of the population 
     */
    int size() const;
    
    /*!
     * @brief Sets the target of the evolutaionary algorithm
     *
     * This method will most likely be deprecated in the future as the
     * target is supposed to be handled by the fitness evaluator
     * class. 
     *
     * @param target a vector from the planes relative origo
     */
    void setTarget(const graph::Vector &target);

    /*!
     * @brief get the target that formerly has been set with the
     * #setTarget() function.
     */
    const graph::Vector &getTarget() const;

    /*!
     * @brief returns the current best path in the population
     *
     * In order to expect a reasonable result from this function, one
     * must remember to call #evolve() once in order to find the
     * currently best path available.
     */
    const graph::Path &getBestPath() const;

    /*!
     * @brief evolve the system one generation.
     *
     * This function will use the fitness evaluator to examine the
     * list of paths and from that build a prioritized list of
     * paths. The list of paths is afterwards chosen randomly from
     * with a likelyhood of selection according to thier rating. This
     * means that also bad paths have a chance of being selected for
     * maiting.
     */
    void evolve();
    
    /*!
     * @brief Generate a random population.
     *
     * It is a demand that this function must be called prior to
     * calling the #evolve() function since the evolve function is
     * dependent on the existence of paths.
     *
     * @param count is the number of different paths in the
     * population. 
     */
    void initPaths(const int count);

    /*!
     * @brief Exchange the best path from populations.
     *
     * With this method two populations gets the option of exchanging 
     * their best paths and in that way get out of any local minima 
     * that tends to get the GP to be stuck.
     */
    void crossPopulation(Population &pop); 

    /*!
     * @brief Obtain an enumerator for the paths present in this
     * population.
     *
     * @see PathEnumerator
     */
    PathEnumerator enumerator();
  };
}

#endif
