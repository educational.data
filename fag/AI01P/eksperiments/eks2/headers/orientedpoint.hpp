// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef __ORIENTEDPOINT_HPP_INCLUDED__
#define __ORIENTEDPOINT_HPP_INCLUDED__

#include <vector.hpp>

namespace graph {
  struct OrientedPoint {
    Vector Offset;
    Vector Direction;
    OrientedPoint();
    OrientedPoint(Vector pos, Vector direction);
    OrientedPoint(const OrientedPoint &point);
  };
}

#endif
