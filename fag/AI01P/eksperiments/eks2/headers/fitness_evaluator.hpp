
// Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

#ifndef __FITNESS_EVALUATOR_HPP_INCLUDED__
#define __FITNESS_EVALUATOR_HPP_INCLUDED__

namespace graph {
  class Vector;
  class Path;
}

class Environment;

namespace genetic {
  class FitnessEvaluator {
    struct impl;
    impl *pImpl;
    FitnessEvaluator(const FitnessEvaluator &) { }
  public:
    FitnessEvaluator();
    ~FitnessEvaluator();
    
    void setTarget(const graph::Vector &target);
    double evaluate(const graph::Path &path);
  };
}


#endif
