#include <ltdl.h>
#include <iostream>
#include "visualizer/path_drawer.hpp"
#include <boost/shared_ptr.hpp>
#include <plane.hpp>
#include <vector.hpp>
#include "plane/myparser.hpp"

#define LIB_GTKMMNAME "visualizer/libmygtkmm"

using namespace std;

int (* libvis_buildSys)(int argc, char *argv[]);
void (* libvis_setdrawer)(IPathDrawer *);
void (* libvis_run)();
void (* libvis_stop)();

bool testErr() {
  const char *res = lt_dlerror();
  if(res != 0L) {
    cout << res << endl;
    exit(-1);
    return false;
  }
  else 
    return true;
}

void parseFile(Glib::ustring filepath) 
{
  try
    {
      MySaxParser parser;
      parser.set_substitute_entities(true); //
      parser.parse_file(filepath);
    }
  catch(const xmlpp::exception& ex)
    {
      std::cout << "libxml++ exception: " << ex.what() << std::endl;
    }
}

int main(int argc, char *argv[]) {
  {
    lt_dlinit();

    lt_dlhandle handle = lt_dlopenext(LIB_GTKMMNAME);
    testErr();
    libvis_buildSys = (int (*)(int,char **))lt_dlsym(handle,"libvis_buildSys");testErr();
    libvis_run = (void (*)())lt_dlsym(handle,"libvis_run");testErr();
    libvis_stop = (void (*)())lt_dlsym(handle,"libvis_stop");testErr();
    libvis_setdrawer = (void (*)(IPathDrawer *))lt_dlsym(handle,"libvis_setdrawer");testErr();
  }
  parseFile("example.xml");
  libvis_buildSys(argc, argv);

  boost::shared_ptr<Plane> rPlane;

  rPlane.reset(new Plane);
  PathDrawer pd(rPlane);

  libvis_setdrawer(&pd);
  
  libvis_run();
}
