#!/bin/sh
# Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

# $Revision: 1.8 $
# $Author: tobibobi $

print_help() {
  echo "usage: $0 <OPTIONS>";
  echo " -c,--configure        run ./configure";
  echo " -x,--maintainer-mode  enable maintainer-mode when configuring."
  echo " -m,--make             run make";
  echo " -b,--bootstrap        do bootstrapping";
  echo " -h,--help             This small help page";
  echo " -l,--changelog        build a changelog file (only valid if checked out from CVS)"
  echo " -r,--clean            clean generated files away"
  echo " -F,--force            if the system doesn't think it needs a rebuild, this forces it to do it anyway"
  echo ""
  echo "author: Tobias Nielsen <tobibobi@gmail.com>";
  exit 0;
}

default=yes
set -- $(getopt -n $0 --longoptions="force configure make help bootstrap maintainer-mode clean changelog" Fcmbhlrx $@)
for o
  do
  case "$o" in
      -c | --configure) shift; configure=yes;default=no;;
      -m | --make) shift; make=yes;default=no;;
      -b | --bootstrap) shift; bootstrap=yes;default=no;;
      -h | --help) shift; print_help;default=no;;
      -l | --changelog) shift; build_changelog=yes;default=no;;
      -r | --clean) shift; clean=yes;default=no;;
      -x | --maintainer-mode) shift; maintainer=yes; configure=yes;default=no;;
      -F | --force) shift; force=yes;;
      --) shift; shift;;
  esac
done

if test x$clean == xyes; then
  if test -f Makefile; then
      make distclean
  fi
  echo -n Cleaning backup files
  find . -name \*~ -exec rm {} \; -exec echo -n . \;  
  echo " done"
  echo -n Cleaning Makefiles
  find . -name Makefile -exec rm {} \; -exec echo -n . \;
  find . -name Makefile.in -exec rm {} \; -exec echo -n . \;
  echo " done"
  echo -n Cleaning other files
  rm -f configure;echo -n .
  rm -f config.h;echo -n .
  rm -f config.h.in;echo -n .
  rm -rf config;echo -n .
  rm -f COPYING INSTALL;echo -n ..
  rm -f aclocal.m4;echo -n .
  rm -f stamp-h.in;echo -n .
  rm -rf autom4te.cache;echo -n .
  rm -f bootstrap.log;echo -n .
  echo " done"
fi

if test x$build_changelog == xyes; then
    cvs2cl -P --hide-filenames -U USERS --summary --FSF --utc -w -R "\\*.*" --no-wrap --stdout|cat -s >ChangeLog
fi

do_bootstrap() {
    test x$has_run_bootstrap == xyes && return
    if test ! -f configure || test configure.ac -nt configure || test x$force == xyes; then
	echo -n Bootstrapping
	rm -f bootstrap.log
	mkdir -p config >>bootstrap.log 2>>bootstrap.log;echo -n .
	aclocal -I aclocal >>bootstrap.log 2>>bootstrap.log;echo -n .
	autoheader >>bootstrap.log 2>>bootstrap.log;echo -n .
	libtoolize --ltdl --force >>bootstrap.log 2>>bootstrap.log;echo -n .
	automake --add-missing >> bootstrap.log 2>>bootstrap.log;echo -n .
	autoconf >>bootstrap.log 2>>bootstrap.log;echo -n .;
	echo " done"
	if test x$make == xyes; then 
	    configure=yes
	fi
    else
	echo "I see no reason to run bootstrap (use --force to override)"
    fi
    has_run_bootstrap=yes
}

do_configure() {
    test x$has_run_conf == xyes && return
    if test configure.ac -nt configure; then
	do_bootstrap
    fi
    if test ! -f configure || test configure -nt Makefile || test x$force == xyes; then
	if test x$maintainer == xyes; then
	    ./configure --enable-maintainer-mode
	else
	    ./configure
	fi
    else
	echo "I can see no reason to rerun configure  (use --force to override)"
    fi
    has_run_conf=yes
}


do_make() {
    if test -f Makefile && test configure -nt Makefile; then
	do_configure
    fi
    make all
}

if test x$bootstrap == xyes; then 
    do_bootstrap
fi

if test x$configure == xyes; then
    do_configure
fi

if test x$make == xyes; then
    do_make
fi

if test x$default == xyes; then 
    echo "usage: $0 <OPTIONS>";
    echo "  run $0 --help for guidance on running"
    exit 1
fi

