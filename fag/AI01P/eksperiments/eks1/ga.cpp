#include <iostream>
#include <stdlib.h>

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#ifdef HAVE_BOOST_OPERATORS_HPP
# include <boost/operators.hpp>
#else 
# error cannot compile without boost/operators.hpp
#endif

#ifdef HAVE_BOOST_UTILITY_HPP
# include <boost/utility.hpp>
#else
# error cannot compile without boost/utility.hpp
#endif
#include <vector>
#include <algorithm>
#include <string>
#include <strstream>

#include <time.h>
#ifdef HAVE_STDEXCEPT
# include <stdexcept>
#else
# error We miss the standard exceptions!! (stdexcept)
struct out_of_range {
  out_of_range(const char *) { }
};
#endif

using namespace std;

void doGA();

int main(int cnt, char *args []) {
  doGA();
  return 0;
}

void Mutate1B(int bit, unsigned char *genome);
int fitness(int cnt, unsigned char *val);
void crossover1P(int bit, int len, unsigned char *gen1, unsigned char *gen2, unsigned char *output);



struct direction : boost::addable<direction> {

  int X,Y,Z;

  direction(int x, int y, int z) : X(x),Y(y),Z(z) { }
  const direction &operator+=(const direction &dir) { 
    X+=dir.X; 
    Y+=dir.Y; 
    Z+=dir.Z;
    return *this;
  }
};

class ga_operations;

class genome : boost::noncopyable {
  bool *gene;
  int _size;
  friend class ga_operations;
public:
  genome(const int genesize) : _size(0) {
    set_size(genesize);
  }

  genome() : _size(0) { }

  virtual ~genome() {
    if(_size != 0)
      delete[] gene;
  }

  const int size() const {
    return _size;
  }

  const int set_size(const int genesize) {
    if(_size != 0)
      delete[] gene;
    _size = genesize;

    gene = new bool[_size];

    for(int i=0;i<_size;++i)
      gene[i] = random() % 2 == 0;
  }

  bool &operator[](const int offset) throw(out_of_range){
    if ( offset < _size )
      return gene[offset];
    else
      throw out_of_range("genome::operator[]");
  }

  const genome &operator=(const genome &genee) throw (length_error) {
    if(_size != genee._size) 
      throw length_error("The two genomes are not equal in size");

    for(int i = 0;i<_size;++i)
      gene[i] = genee.gene[i];
    return *this;
  }

  bool operator[](const int offset) const throw(out_of_range) {
    if ( offset < _size )
      return gene[offset];
    else
      throw out_of_range("genome::operator[]");
  }

  const string c_str() const {
    char str[_size+1];
    for(int i = 0;i<_size;++i) {
      str[i] = gene[i] ? '1' : '0';
    }
    str[_size]=0;
    return string(str);
  }
};

class ga_operations {
  ga_operations() {};
public:
  static void crossover1P(int bit, int len, unsigned char *gen1, unsigned char *gen2, unsigned char *output) {
    int lb = bit / 8;
    int lbt = bit % 8;
    
    // bytewise crossover
    for (int l = 0;l<lb;++l)
      output[l]=gen1[l];

    for (int l = lb+1;l<len;++l)
      output[l]=gen2[l];

    unsigned char left;
    unsigned char right;
    switch(lbt) {
    case 0: left = 0;   right = 255; break;
    case 1: left = 128; right = 127; break;
    case 2: left = 192; right = 63;  break;
    case 3: left = 224; right = 31;  break;
    case 4: left = 240; right = 15;  break;
    case 5: left = 248; right = 7;   break;
    case 6: left = 252; right = 3;   break;
    case 7: left = 254; right = 1;   break;
    default: left = 0; right = 0; break;
    }
    output[lb] = gen1[lb] & left | gen2[lb] & right;
  }


  static void mutate(genome &gene, int bit) throw(out_of_range) {
    // invert single bit;
    gene[bit] ^= 1;
  }

  static void mutate(genome &gene) throw(out_of_range) {
    mutate(gene,random() % gene.size());
  }

  static void mutate_n(genome &gene, int num) throw(out_of_range) {
    for(int i =0;i<num;++i) mutate(gene,random() % gene.size());
  }

  static void crossover(genome &gene1,genome &gene2,int c1,int c2) 
    throw(range_error,out_of_range)
  {

    if(c1 >= gene1.size() || c2 >= gene2.size())
      throw out_of_range("crossover::crosspoint is out of range");

    if(gene1.size() != gene2.size())
      throw range_error("crossover::two genes not of same size");

    int min = c1 > c2 ? c2 : c1;
    int max = c1 > c2 ? c1 : c2;
    int size = max-min;
    bool buffer;
    for(int i = 0;i<size;++i) {
      buffer = gene1[i+min];
      gene1[i+min] = gene2[i+min];
      gene2[i+min] = buffer;
    }
  }

  static void crossover(genome &gene1,genome &gene2) 
    throw(range_error,out_of_range) 
  {
    crossover(gene1,gene2,
	      random() % gene1.size(), 
	      random() % gene2.size());
  }

  static void crossover50(genome &gene1, genome &gene2, int pos)
        throw(range_error,out_of_range)
  {
    //assume the sizes are equal and divideable by 2:
    int hlen = gene2.size() / 2;
    if(pos < hlen)
      crossover(gene1,gene2,pos,pos+hlen);
    else {
      crossover(gene1,gene2,pos,gene1.size()-1);
      crossover(gene1,gene2,0,pos-hlen);
    }    
  }

  static void crossover50(genome &gene1, genome &gene2)
        throw(range_error,out_of_range)
  {
    crossover50(gene1,gene2,random() % gene1.size());
  }

  static int fitness(const genome &gene) 
  {
    int a,b,c;
    a=b=c=0;
    
    for(int i = 0;i < 8; i++) {
      a = ((a << 1) & 0xfe) + gene[i];
      b = ((b << 1) & 0xfe) + gene[i+8];
      c = ((c << 1) & 0xfe) + gene[i+16];
    }
    return (int)((float)-a*a+b*c);
    int bits = 0;
    for(int i = 0; i < gene.size();++i)
      if(gene[i]) ++bits;

    return bits;
  }
};

struct gpair {
  int k,v;
  void let(int _k,int _v) { 
    k=_k;
    v=_v; 
  }
  operator int() {
    return v;
  }
  const string str() {
    ostrstream stream;
    stream << "(" << k << "=>" << v << ")" << '\0';
    return stream.str();
  }
};

bool operator < (const gpair &p1, const gpair &p2) {
  return p1.v < p2.v;
}

class population : boost::noncopyable {
  int _popsize, _genesize;
  genome *genes;

public:
  vector<gpair> vFitness;  

  population(int population_size, int gene_size) : 
    _popsize ( population_size ), 
    _genesize ( gene_size ) 
  {
    genes = new genome[population_size];
    vFitness.resize(_popsize);
    for(int i=0;i<population_size;++i) {
      genes[i].set_size(gene_size);
      vFitness[i].let(i,0);
    }

  }
  virtual ~population() {
    delete[] genes;
  }

  const genome &operator[](int offset) 
    throw (out_of_range) 
  {
    if(offset >= _popsize)
      throw out_of_range("genome::operator[] population is not that large");
    return genes[offset];
  }

  int calculate_fitness() {
    int fit = 0;
    for(int i = 0;i<_popsize;++i) 
      {
	int v = ga_operations::fitness(genes[i]);
	fit += v;
	vFitness[i].let(i,v);
      }
    sort(vFitness.begin(),vFitness.end());
    return fit / _popsize;
  }  

  void evolve() {
    int btn = 0;
    
    for(int rt = _popsize-1; rt > 0 && btn <= rt;rt--) 
      {
	for(int tp=_popsize-1;tp>rt && btn <= rt;tp--) 
	  {
	    genes[vFitness[btn].k] = genes[vFitness[tp].k];
	    genes[vFitness[btn+1].k] = genes[vFitness[tp].k];
	    genes[vFitness[btn+2].k] = genes[vFitness[tp].k];
	    //genes[vFitness[btn+3].k] = genes[vFitness[tp].k];
	    ga_operations::crossover50(genes[vFitness[btn].k], genes[vFitness[btn+1].k]);
	    //ga_operations::crossover50(genes[vFitness[btn+2].k], genes[vFitness[btn+3].k]);
	    //ga_operations::mutate_n(genes[vFitness[btn].k],10); 
	    //ga_operations::mutate(genes[vFitness[btn].k]);
	    //ga_operations::mutate(genes[vFitness[btn+1].k]);
	    //ga_operations::mutate(genes[vFitness[btn+2].k]);
	    ga_operations::mutate(genes[vFitness[btn+2].k]);

	    btn+=3;
	  }
      } 
  
    for(int tp = _popsize-2; btn <= tp; tp--) 
      {
	//ga_operations::mutate(genes[vFitness[tp].k]);
      }
  }
  int best_fitness() {
    return vFitness[29].v;
  }
};


void doGA() {
  time_t now;

  time(&now);

  srandom(now % 99);

  population pop(100,24);

  int fitness = pop.calculate_fitness();
  int ev = 0;
  int va = 1;
  while(pop.best_fitness() < 255*255-1) 
    {
      ev++;
      pop.evolve();
      fitness = pop.calculate_fitness();
      if(ev % va == 0) {
	cout << "population " << ev << " avg fitness: " << fitness << " best fit: " << pop.best_fitness() << endl; 
	va*=2;
      }
    }
  cout << "Final population " << ev << " avg fitness: " << fitness << " best fit: " << pop.best_fitness() << endl; 
}
