dnl Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
dnl 
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl 
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl 
dnl $Revision: 1.20 $
dnl $Author: tobibobi $

AC_PREREQ(2.59)
AC_INIT(AI01P-eksperiments,0.1.210,[tobibobi@gmail.com])
AC_REVISION([$Revision: 1.20 $])
AC_CONFIG_AUX_DIR(config)

AM_INIT_AUTOMAKE
AC_CONFIG_SRCDIR([config.h.in])
AC_CONFIG_HEADER([config.h])

AM_MAINTAINER_MODE

AC_PROG_CC dnl cross compiler ability
AC_PROG_CXX
AC_PROG_CPP

dnl LF_HOST_TYPE
dnl LF_CONFIGURE_CXX
dnl LF_CPP_PORTABILITY
dnl LF_SET_WARNINGS

AC_LANG([C++])
AC_CHECK_HEADERS([boost/utility.hpp boost/integer.hpp boost/thread/condition.hpp boost/thread/exceptions.hpp boost/operators.hpp],,
		AC_MSG_ERROR([Missing boost libraries!!]))

AC_CHECK_HEADERS([stdexcept])

AC_ARG_WITH([simulator],
	AC_HELP_STRING(
		[--with-simulator],
		[use a simulator. Use with caution since this needs a rather new version of gtkmm]),
	[ac_use_sim=$withval],
	[ac_use_sim=auto])
CAN_DO_GTK_BUILDING=no							

if test x$ac_use_sim == xauto || test x$ac_use_sim == xyes; then
  CAN_DO_GTK_BUILDING=yes
  PKG_CHECK_EXISTS([gtkmm-2.4],,[CAN_DO_GTK_BUILDING=])
dnl   PKG_CHECK_EXISTS([cairomm-1.0],[echo cairomm-1.0],[CAN_DO_GTK_BUILDING=])
  PKG_CHECK_EXISTS([gdkmm-2.4],,[CAN_DO_GTK_BUILDING=])

  if test x$CAN_DO_GTK_BUILDING == xyes; then
    AC_MSG_CHECKING([for gtkmm <= 2.8.3])
    can_build=yes;
    if pkg-config gtkmm-2.4 --atleast-version=2.8.3; then 
       AC_MSG_RESULT([yes])
    else
       AC_MSG_RESULT([no])
       AC_MSG_ERROR([Cannot build without gtkmm > version 2.8.3])
    fi
    AC_MSG_CHECKING([for gdkmm-2.4 <= 2.8.3])
    if pkg-config gdkmm-2.4 --atleast-version=2.8.3; then
       AC_MSG_RESULT([yes])
    else
       AC_MSG_RESULT([no])
       AC_MSG_ERROR([Cannot build without gdkmm > version 2.8.3])
    fi
#    AC_MSG_CHECKING([for libgnomeuimm-2.6 <= 2.10.0])
#    if pkg-config libgnomeuimm-2.6 --atleast-version=2.10.0; then
#       AC_MSG_RESULT([yes])
#    else
#       AC_MSG_RESULT([no])
#       AC_MSG_ERROR([Cannot build without libgnomeuimm-2.6 > version 2.10.3])
#    fi

    AC_MSG_CHECKING([for libxml++-2.6 <= 2.10.0])
    if pkg-config libxml++-2.6 --atleast-version=2.10.0; then
       AC_MSG_RESULT([yes])
    else
       AC_MSG_RESULT([no])
       AC_MSG_ERROR([Cannot build without libxml > version 2.10.0])
    fi
    DEPS_CFLAGS=`pkg-config gtkmm-2.4 gdkmm-2.4 libxml++-2.6 --cflags`
    DEPS_LIBS=`pkg-config gtkmm-2.4 gdkmm-2.4 libxml++-2.6 --libs`
    LIBXML_CFLAGS=`pkg-config libxml++-2.6 --cflags`
    LIBXML_LIBS=`pkg-config libxml++-2.6 --libs`
  else
    if test x$ac_use_sim = xyes; then
      AC_MSG_ERROR([Cannot perform a valid simulator build (use --without-simulator])
    else
      AC_MSG_WARN([Cannot perform a valid GTK-build - why not install a new gtkmm with cairomm?])
    fi
  fi
fi
AC_SUBST([DEPS_CFLAGS DEPS_LIBS CAN_DO_GTK_BUILDING])
AC_SUBST([LIBXML_CFLAGS LIBXML_LIBS])

AC_LANG([C])
AC_PROG_SED
AM_CONDITIONAL(BUILD_GTKMM, [test "x$DEPS_CFLAGS" != x])



AC_C_CONST

AC_PROG_LN_S
AC_PROG_MAKE_SET

AC_LIB_LTDL

dnl Enable building of the convenience library
dnl and set LIBLTDL accordingly
AC_LIBLTDL_CONVENIENCE
dnl Substitute LTDLINCL and LIBLTDL in the Makefiles
AC_SUBST(LTDLINCL)
AC_SUBST(LIBLTDL)
dnl Check for dlopen support
AC_LIBTOOL_DLOPEN
dnl Configure libtool
AC_PROG_LIBTOOL

AC_CACHE_SAVE

AC_PROG_PDFLATEX
AC_PROG_BIBTEX

AC_CHECK_PROGS(doxygen,[doxygen],no)
export doxygen;
if test x$doxygen == xno; then
   AC_MSG_ERROR([doxygen is needed in order to support the 
documentation of this project.])
fi
AC_SUBST([doxygen])

AC_CHECK_PROGS(dot,[dot],no)
if test x$dot == xno; then
   AC_MSG_ERROR([doxygen needs the dot tool from the graphviz package])
fi

dnl AC_PROG_INSTALL
AM_SYS_POSIX_TERMIOS

AC_SUBST(DLOPEN)
AC_SUBST(DLREOPEN)
AC_CONFIG_SUBDIRS([libltdl])
AC_CONFIG_FILES([
	    Makefile
	    eks1/Makefile
	    eks2/Makefile
	    eks2/graph/Makefile
	    eks2/visualizer/Makefile
	    eks2/plane/Makefile
	    report/Makefile
	    dox/Makefile
	    dox/Doxyfile
	    ])

AC_OUTPUT
