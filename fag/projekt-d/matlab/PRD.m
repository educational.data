rho=1000;
g=9.81;
r = 0.01675;
PI = 3.1415;
L = 0.8;
eta = 0.001;
P2 = 0;%100000;

J=0.01;
K=0.01;

p=2;
R1 = 7.0077;
R2 = 5.7983;
X1 = 5.9310;
X2 = 14.2386;
Xm = 218.8145;
Zth = (i*X1*(R1+i*X1))/(R1+i*X1+i*Xm)
Xth = angle(Zth)*180/pi
Rth = abs(Zth)

Ti=0.01
num=1;
den=[Ti 0];
PIreg=tf(num,den)
PIreg_z=c2d(PIreg,T)
[num_z den_z] = tfdata(PIreg_z,'v')