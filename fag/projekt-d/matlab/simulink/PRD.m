clear; clc

rho=1000;   %vands densitet
g=9.81;     %tyngdeaccelerationen

PI = 3.1415;
L = 0.8;            %rørets længde
roer_fric = 0.0475  %estimeret friktionskoefficient

v_f = 8     %konstant forhold mellem spænding og frekvens
            %i frekvensomformerens outputsignal

J=0.02;             %motorens inertimoment
K_fric=0.000141;    %motorens friktionskoefficient
poles=2;            %antallet af poler i motoren

%data for motoren
R1 = 7.0077;        
R2 = 5.7983;        
X1 = 5.9310;        
X2 = 14.2386;       
Xm = 218.8145;      

%theveninækvivalenter
Zth = (i*Xm*(R1+i*X1))/(R1+i*X1+i*Xm)
Xth = imag(Zth)
Rth = real(Zth)

%forholdet mellem Vth og V
Vth_coeff = abs((i*Xm)/(R1+i*(X1+Xm)))

%estimeret forhold mellem omdrejningshastighed og inputspænding
n_per_v = 278
