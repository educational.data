clear; clc
run('modifiedPRD')
%startguesses
x0=[290]%[156]%
u0=[10]%[5]%
y0=[2772]%[2980]%361000,2949.8
%parametres to be fixed
ix=[];
iu=[1];
iy=[];
%find steady state point
[x,u,y,dx]=trim('for_linearisation',x0,u0,y0,ix,iu,iy)%,ix,iu,iy
%derive linear model around point
[A,B,C,D]=linmod('for_linearisation',x,u)%,0.001, 0.001
Blin_man = -A*x/u

sys_struct = linmod('for_linearisation',x,u)
[A1,B1,C1,D1] = linmod('for_linearisation')
[num,den]=ss2tf(A,B,C,D);

%calculate transferfunction
%TF=tf(num,den)
%plot the system
%plottype={'step','bode','pzmap','nyquist'};
%ltiview(plottype,TF)

system_ss = ss(A,B,C,D)
%discrete
T=0.01
system_ss_z = c2d(system_ss, T)
[Ad,Bd,Cd,Dd]=ssdata(system_ss_z)

tau=0.05
tau_o = 0.1
r_s=exp(-T/tau)
r_o=exp(-T/tau_o)
zeta = 0.9
theta = (-log(r_s)*sqrt(1 -zeta^2))/zeta
P = [ r_s*exp(j*theta) r_s*exp(-j*theta)]

Ay = [Ad 0; -Cd 1]
By = [ Bd; 0]
Ky = acker(Ay,By,[P(1) r_o])
K = Ky(1)
Ki = -Ky(2)
% Ky matricen indeholder [K  -Ki]
%K = [Ky(1) Ky(2)]
%Ki = -Ky(3)


%K = acker(Ad,Bd,P(1))

%observer poles
%p_o = [r_o r_o]
G_c = (acker(Ad', (Cd*Ad)', r_o))'


format long