#include "regulator.h"
#include <iolpc2148.h>

#define SET_PUNKT 512
#define KP 2

int
PRegulator ()
{
  DACR_bit.VALUE = AD0GDR_bit.RESULT;

  // define constants
  const float Tv = 1.0f;
  const float Tn = 1.0f;
  const float Kp = 1.0f;

  const float iMax=1024.0f;
  const float iMin=0.0f;


  float SP = (float)SET_PUNKT;

  ///////////////////////
  // calculate dPVdt
  ///////////////////////
  static int PV = 1;
  float dPVdt = DACR_bit.VALUE/PV;
  PV = DACR_bit.VALUE > 1 ? DACR_bit.VALUE : 1;

  ///////////////////////
  // calculate f
  ///////////////////////
  float f = PV + Tv * dPVdt;

  ///////////////////////
  // calculate error
  ///////////////////////
  float e = SP - f;

  ///////////////////////
  // Calculate intergrator
  ///////////////////////
  static float ie = 0;
  ie+=e;
  // Reduce spinup errors.
  if(ie > iMax)ie = iMax;
  if(ie < iMin)ie = iMin;


  ///////////////////////
  // calculate output
  ///////////////////////
  float u = Kp * ( e + ( 1/Tn ) * ie);

  // Okay, where do i put the output????

  return (int)u;
}
