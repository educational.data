#include "rprintf.h"
#include "vt100.h"
void
vt100Init (void)
{
  // initializes terminal to "power-on" settings
  // ESC c
  rprintfProgStrM ("\x1B\x63");
}

void
vt100ClearScreen (void)
{
  // ESC [ 2 J
  rprintfProgStrM ("\x1B[2J");
}

void
vt100CursorHome (void)
{
  // ESC [ H
  rprintfProgStrM ("\x1B[H");
}

void
vt100SetAttr (unsigned char attr)
{
  // ESC [ Ps m
  //rprintf("\x1B[%dm",attr);
}

void
vt100SetCursorMode (unsigned char visible)
{
  if (visible)
    // ESC [ ? 25 h
    rprintf ("\x1B[?25h");
  else
    // ESC [ ? 25 l
    rprintf ("\x1B[?25l");
}

void
vt100SetCursorPos (unsigned char line, unsigned char col)
{
  // ESC [ Pl ; Pc H
  rprintf ("\x1B[%d;%dH", line, col);
}
