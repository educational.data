
#include "uart.h"
#include "rprintf.h"

void
sendByte (unsigned char byte)
{
  while (!(U0LSR & 0x20));	//U1THR THRE bit set when U1THR contains valid data
  U0THR = byte;
}

unsigned char
getBytePolling (void)
{
  while (!(U0LSR & 0x01));
  return U0RBR;
}

void
initUART (void)
{
  U0FCR = 0x01;
  U0LCR = 0x83;
  //U0DLL = 0xC3; //baud = 9600
  U0DLL = 0x20;			// baud = 57600
  U0DLM = 0x00;
  U0LCR = 0x03;
}
