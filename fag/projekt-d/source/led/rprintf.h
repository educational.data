/*! \file rprintf.h \brief printf routine and associated routines. */
//****************************************************************************
//
// File Name    : 'rprintf.h'
// Title                : printf routine and associated routines
// Author               : Pascal Stang - Copyright (C) 2000-2002
// Created              : 2000.12.26
// Revised              : 2003.5.1
// Version              : 1.0
// Target MCU   : Atmel AVR series and other targets
// Editor Tabs  : 4
//
// NOTE: This code is currently below version 1.0, and therefore is considered
// to be lacking in some functionality or documentation, or may not be fully
// tested.  Nonetheless, you can expect most functions to work.
//
// This code is distributed under the GNU Public License
//              which can be found at http://www.gnu.org/licenses/gpl.txt
//
//****************************************************************************

#ifndef RPRINTF_H
#define RPRINTF_H

// include configuration

#define RPRINTF_SIMPLE

// defines/constants
//#define STRING_IN_RAM 0
//#define STRING_IN_ROM 1

// make a putchar for those that are used to using it
//#define putchar(c)    rprintfChar(c);

// functions

//! initializes the rprintf library for an output stream
// you must call this initializer once before using any other rprintf function
// the argument must be a single-character stream output function
void rprintfInit (void (*putchar_func) (unsigned char c));

//! prints a single character to the current output device
void rprintfChar (unsigned char c);

//! prints a null-terminated string stored in RAM
void rprintfStr (char str[]);

//! prints a section of a string stored in RAM
// begins printing at position indicated by <start>
// prints number of characters indicated by <len>
void rprintfStrLen (char str[], unsigned char start, unsigned char len);
void rprintfNum (char base, char numDigits, char isSigned, char padchar,
		 long n);
//! prints a string stored in program rom
// NOTE: this function does not actually store your string in
// program rom, but merely reads it assuming you stored it properly.
void rprintfProgStr (char str[]);
// Using the function rprintfProgStrM(...) automatically causes
// your string to be stored in ROM, thereby not wasting precious RAM
// Example usage:
// rprintfProgStrM("Hello, this string is stored in program rom");
//#define rprintfProgStrM(string)                       (rprintfProgStr(PSTR(string)))
#define rprintfProgStrM(string)			(rprintfStr(string))

//! prints a carriage return and line feed
// useful when printing to serial ports/terminals
void rprintfCRLF (void);

void rprintfFloat (char numDigits, double x, char printSign);

// prints the number contained in "data" in hex format
// u04,u08,u16,and u32 functions handle 4,8,16,or 32 bits respectively
void rprintfu04 (unsigned char data);	///< print 4-bit hex number
void rprintfu08 (unsigned char data);	///< print 8-bit hex number
void rprintfu16 (unsigned short data);	///< print 16-bit hex number
void rprintfu32 (unsigned long data);	///< print 32-bit hex number

#ifdef RPRINTF_SIMPLE
	// a simple printf routine
	//int rprintf1RamRom(unsigned char stringInRom, const char *format, ...);
int rprintf (const char *format, ...);
	// #defines for RAM or ROM operation
	//#define rprintf1(format)              rprintf1RamRom(STRING_IN_ROM, PSTR(format))
	//#define rprintf1RAM(format)   rprintf1RamRom(STRING_IN_RAM, format)

	// *** Default rprintf(...) ***
	// this next line determines what the the basic rprintf() defaults to:
	//#define rprintf(format)               rprintf1RamRom(STRING_IN_RAM, format)
	//#define rprintf(format)               rprintf1RamRom(format)
#define rprintftest(format,...)   		rprintf1RamRom(format,...)
#endif

#endif
