/**************************************************************************
* DEBUG FLASH RDI.  Debug code from FLASH with RDI debugger server
* DEBUG RAM RDI.    Debug code from RAM with RDI debugger server
* SIMULATOR.        Debug code without board.
* Release.          Code without debug informations.
**************************************************************************/

#include <iolpc2148.h>
#include <intrinsic.h>
#include "main.h"
#include "uart.h"
#include "rprintf.h"
#include "vt100.h"
#include "timer.h"

#include "regulator.h"

// #define MAX_MOTOR_RPM 3000

#define DEBUG_ADC_VOLTAGE
#define DEBUG_DAC_VOLTAGE
#define DEBUG_ADC_BAR
#define DEBUG_DAC_RPM

#define PRINT_STEP_RESPONSE

unsigned int bl_TimerFlag;
unsigned int ADCresult;

float ADCresmV;
float DACmV;

void updateMenu ();
void initMenu ();
void initSystem (void);
void initADC ();

int
main (void)
{
  initSystem ();

  // flag set by the timer to indicate an menu output.
  bl_TimerFlag = FALSE;

  while (1)
    {
      if (bl_TimerFlag)
	{
#ifndef PRINT_STEP_RESPONSE
	  updateMenu ();
#else
	  rprintfNum (10, 4, FALSE, ' ', AD0GDR_bit.RESULT);
	  rprintfChar (' ');
	  rprintfNum (10, 4, FALSE, ' ', DACR_bit.VALUE + 20);
	  rprintfCRLF ();
#endif
	  bl_TimerFlag = FALSE;
	}

      if (IO0PIN & 0x00010000)
	{
	  IO0SET = 0x00000800;
	}
      else
	{
	  IO0CLR = 0x00000800;
	}

      if (IO0PIN & 0x00008000)
	{
	  IO0SET = 0x00000400;
	}
      else
	{
	  IO0CLR = 0x00000400;
	}
    }
}

/*********************************************************
 ** Purpose: Output menu and display values
 **
 *********************************************************/
void
updateMenu ()
{
  static char loadSigns[] = { '/', '-', 0x5C, '|' };
  static int loadSignCnt = 0;
#ifdef DEBUG_ADC_VOLTAGE
  vt100SetCursorPos (2, 7);
  ADCresmV = (float) (ADCresult) * 3.3 / 1024;
  rprintfFloat (4, ADCresmV, FALSE);
#endif

#ifdef DEBUG_ADC_BAR
  vt100SetCursorPos (2, 19);
  ADCresmV = ((float) (ADCresult) - 205) * 6 / 819;
  rprintfFloat (3, ADCresmV, TRUE);
#endif

#ifdef DEBUG_DAC_VOLTAGE
  vt100SetCursorPos (3, 7);
  DACmV = (float) (DACR_bit.VALUE * 3.3) / 1024;
  rprintfFloat (4, DACmV, FALSE);
#endif

#ifdef DEBUG_DAC_RPM
  vt100SetCursorPos (3, 19);
  DACmV = (float) (DACR_bit.VALUE) * 100 / 1024;
  rprintfFloat (4, DACmV, FALSE);
#endif

  vt100SetCursorPos (5, 10);
  rprintfChar (loadSigns[loadSignCnt]);
  if (loadSignCnt < 3)
    loadSignCnt++;
  else
    loadSignCnt = 0;
}

void
initMenu ()
{
  vt100SetCursorMode (FALSE);
  vt100ClearScreen ();
  vt100SetCursorPos (2, 1);
  rprintfProgStrM (" ADC:");
  vt100SetCursorPos (2, 12);
  rprintfProgStrM (" Volt");
  vt100SetCursorPos (2, 24);
  rprintfProgStrM (" Bar");
  rprintfCRLF ();
  rprintfProgStrM (" DAC:");
  vt100SetCursorPos (3, 12);
  rprintfProgStrM (" Volt");
  vt100SetCursorPos (3, 24);
  rprintfProgStrM (" %rpm");
  rprintfCRLF ();
  rprintfCRLF ();
  rprintfProgStrM (" RUNNING");
}


void
initSystem ()
{
  __disable_interrupt ();

  // set CCLK to 60 Mhz and PCLK to 30 Mhz.
  PLLCFG = 0x00000024;		// set multiplier and divider of PLL to give 60 Mhz. M=5;P=2
  PLLCON = 0x00000001;		// enable PLL
  PLLFEED = 0x000000AA;		// Update PLL registrs with feed sequence
  PLLFEED = 0x00000055;

  while (!(PLLSTAT & 0x00000400));

  PLLCON = 0x00000003;		// Connect the PLL
  PLLFEED = 0x000000AA;		// Update PLL registrs with feed sequence
  PLLFEED = 0x00000055;

  VPBDIV = 0x00000002;		// Set the VLSI peripheral bus to 30.000 MHz.

  // Enable Memory Acceleration Module
  MAMCR = 0;			// Disbale MAN
  MAMTIM = 0x03;		// Use 3 MAM fetch cycles for 60 Mhz. CCLK.
  MAMCR = 0x02;			// Enable MAN fully.

  IO0DIR = 0x00000C00;
  PINSEL0 = 0x00000005;		// UART RX og TX pins enabled
  PINSEL1_bit.P0_25 = 2;

  // init the AD converter
  initADC ();

  // setup timer to initial sample period
  timer0Init (SAMPLE_TIME);
  //timer1Init();
  __enable_interrupt ();	// Global interrupt enable

  initUART ();
  rprintfInit (sendByte);

  delay (4000000);
#ifndef PRINT_STEP_RESPONSE
  initMenu ();
#endif
}

void
initADC ()
{
  //enable AD0.3 only
  AD0CR_bit.SEL = 8;

  AD0CR_bit.CLKDIV = PCLKFREQ / 4500000;

  // put A/D into continuous convert mode
  AD0CR_bit.BURST = 1;

  //11 clocks/10 bit accuracy
  AD0CR_bit.CLKS = 0;

  //power up the unit
  AD0CR_bit.PDN = 1;

  // This is probably the potentiometer output
  PINSEL1_bit.P0_30 = 1;

  //start 1st cnvrsn immediately
  AD0CR_bit.START = 0x0001;
}
