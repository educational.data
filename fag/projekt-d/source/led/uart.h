#ifndef UART_H
#define UART_H

#include "iolpc2148.h"

void sendByte (unsigned char byte);
unsigned char getBytePolling (void);
void initUART (void);

#endif
