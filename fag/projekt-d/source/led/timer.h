#ifndef TIMER_H
#define TIMER_H

#pragma vector=0x18
__irq __arm void IRQ_ISR_Handler (void);

void MM_TIMER0_ISR ();

void timer0Init (float sample_time);

void delay (int delayTime);

#endif
