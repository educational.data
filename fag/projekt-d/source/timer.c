#include "iolpc2148.h"
#include "timer.h"
#include "main.h"
#include "rprintf.h"
#include "regulator.h"

extern unsigned int bl_TimerFlag;
extern unsigned int ADCresult;
void delay(int d)
{
    for(; d; --d)
    {
      ;  
    }
}


#pragma vector=0x18
__irq __arm void IRQ_ISR_Handler (void)
{
  void (*interrupt_function)();
  unsigned int vector;
  vector = VICVectAddr;                   // Get interrupt vector.
  interrupt_function = (void(*)())vector; // Call MM_TIMER0_ISR thru pointer
  (*interrupt_function)();  // Call vectored interrupt function
  VICVectAddr = 0;          // Clear interrupt in VIC
}

#define TICKS_PER_SECOND 1      // TIMER0 interrupt is 100Hz

void MM_TIMER0_ISR()
{
  static unsigned int us_Ticks;
   
  while(AD0GDR_bit.DONE == 0);                //wait until conversion done
  AD0GDR_bit.CHN = 0;                         // Data in channel 0
  ADCresult = AD0GDR_bit.RESULT ;              // save the converted data
  
  PIregulator();
              
  AD0CR_bit.START = 0x0001;                  // start the next conversion

  us_Ticks++;
  if(us_Ticks == TICKS_PER_SECOND)
  {
    bl_TimerFlag = TRUE;        // The background "task"
    us_Ticks = 0;
  }  
  T0IR = 1;                     // Clear timer interrupt
}

void timer0Init()
{
  T0IR=0xFF;           // reset match and capture event interrupts
  T0TC=0;              // Clear timer counter
  T0PR= 0;             // No Prescalar
  T0MR0=SAMPLE_TIME*PCLKFREQ;
  //T0MR0=0x000493E0;    //  period = 10ms
  T0MCR = 3;           // Reset Timer Counter & Interrupt on match
  T0TCR = 1;           // Counting enable

  VICIntSelect  =  0;             // Set all VIC interrupts to IRQ for now
  VICIntEnClear = 0xFFFFFFFF;     // Diasable all interrupts
  VICProtection = 0;              // VIC registers can be accessed in User or
                                  // privileged mode
  VICVectAddr = 0;                // Clear interrupt
  VICProtection = 0;              // Accesss VIC in USR | PROTECT


  VICIntSelect &= ~(1<<VIC_TIMER0);            // Timer 0 intrpt is an IRQ (VIC_TIMER0 = 4)
  VICVectAddr0 = (unsigned int)&MM_TIMER0_ISR; // Install ISR in VIC addr slot 0
  VICVectCntl0 = 0x20 | VIC_TIMER0;            // IRQ type, TIMER 0 int enabled
  VICIntEnable |= (1<<VIC_TIMER0);             // Turn on Timer0 Interrupt  
}
