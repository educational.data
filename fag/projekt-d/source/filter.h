#ifndef FILTER8_H
#define FILTER8_H

#define FALSE 0
#define TRUE !(FALSE)

int filter(int data);

#endif
