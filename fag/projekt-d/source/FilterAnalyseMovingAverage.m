T = 0.02 %Sample tid
[ADC,DAC,ERR] = textread('40procent_rpm.txt','%d%d%d','delimiter',' ')

SEK = 4; % Tidsperioden der skal plottes over

tid = 0:T:SEK-T;

ADC_offset = 400; % 0.4V offset
for i=1:((SEK/T)),
    ADCny(i)=((ADC(i)*3300)/1024)-ADC_offset; % Omregn til mV
end

plot(tid,ADCny,'r'); % Plot data i mV

title('Stepresponse med lukket ventil, 4V udgangsspænding');
xlabel('Tid i sek');
ylabel('Tryk i mV');

h=get(gca,'xlabel');
set(h,'FontSize',20);

h=get(gca,'ylabel');
set(h,'FontSize',20);

h=get(gca,'title');
set(h,'FontSize',20);

% Beregn Moving average filter
M=8;
B=ones(M,1)/M;
y=filter(B,1,ADCny);

figure(2)
plot(tid,y,tid,ADCny)
legend('Filtreret data','Ufiltreret data');

title('Stepresponse med lukket ventil, 4V udgangsspænding');
xlabel('Tid i sek');
ylabel('Tryk i mV');

h=get(gca,'xlabel');
set(h,'FontSize',20);

h=get(gca,'ylabel');
set(h,'FontSize',20);

h=get(gca,'title');
set(h,'FontSize',20);

fvtool(B,1) % Analyser filteret
