#include "regulator.h"
#include <iolpc2148.h>
#include "filter.h"

#define SET_PUNKT 278
#define Kp 0.8
#define Ki 0.2

int e_func;

int PIregulator()
{   
    static float outp=0, outp_old=0, e_old=0;
    e_func = SET_PUNKT - AD0GDR_bit.RESULT;    
    //e_func = SET_PUNKT - filter(AD0GDR_bit.RESULT);
    outp = outp_old + Kp*((float)e_func-e_old)+Ki*((float)e_func+e_old)/2;
    
    e_old = (float)e_func;

    if(outp < 1023)
    {      
      if(outp < 0)
        DACR_bit.VALUE = outp = 0;
      else
        DACR_bit.VALUE = outp;
    }
    else
    {
      DACR_bit.VALUE = outp = 1023;
    }    
    outp_old = outp;
    
    return (unsigned int)outp;    
}
