T = 0.02 %Sample tid

[ADC,DAC,ERR] = textread('40procent_rpm.txt','%d%d%d','delimiter',' ');

ADC_offset = 400; % 0.4V offset
SEK = 4;  % Over hvor mange sekunder skal data plottes
for i=1:((SEK/T)),
    ADCny(i)=((ADC(i)*3300)/1024)-ADC_offset; % Omregn data til mV
end

tid = 0:T:SEK-T;

% Definere Single Pole koeeficienterne
x = 0.85;
a0 = 1-x;
b1 = x;

y(1) = ADCny(1); % Init
for i=2:(length(ADCny)),
    y(i) = a0*ADCny(i)+b1*y(i-1); % Beregn ny output       
end

plot(tid,y,'r',tid,ADCny,'b') % Plot data
title('Single pole filtering af m�ledata med x=0.85');

legend('filtreret','r� data')

xlabel('Tid i sek');
ylabel('Tryk i mV');

h=get(gca,'xlabel');
set(h,'FontSize',20);

h=get(gca,'ylabel');
set(h,'FontSize',20);

h=get(gca,'title');
set(h,'FontSize',20);

% Opstil overf�ringsfunktion for filteret
num=[a0];
den=[1 -b1];

fvtool(num,den) % Analyser med fvtool


