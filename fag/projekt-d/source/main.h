#ifndef MAIN_H
#define MAIN_H

#include <iolpc2148.h>
#include <intrinsics.h>


#define CCLK 60000000        
#define PCLKFREQ (CCLK/2) 

#define FALSE 0
#define TRUE !(FALSE)

#define SetBit(x,y);       ( (x) |= (1<<y) )
#define ClrBit(x,y);       ( (x) &= (~(1<<y)) )

#endif
