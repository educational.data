
rho=1000;
g=9.81;
%r = 0.02;
radius = 0.01775;
PI = 3.1415;
L = 0.3;
roer_fric = 0.030
eta = 0.01;
P2 = 0;%100000;

%V/F const
v_f = 8

J=0.02;
K_fric=0.002%0.000045;

poles=2;
R1 = 7.0077;
R2 = 5.7983;
X1 = 5.9310;
X2 = 14.2386;
Xm = 218.8145;
Zth = (i*Xm*(R1+i*X1))/(R1+i*X1+i*Xm)
Xth = imag(Zth)
Rth = real(Zth)
Vth_coeff = abs((i*Xm)/(R1+i*(X1+Xm)))
eta_pumpe = 1

%(-0.0163*u[1]^2 + 0.2109*u[1] + 0.4026)/(u[2]+1)
%u[1]*u[3]/(367*eta_pumpe*u[2])