#include <iolpc2148.h>
#include "filter.h"

#define FILTER_SIZE 8

//Moving average filter 
int filter(int data)
{
  int fOut = 0;
  static int init = TRUE;  
  static int buffer[FILTER_SIZE];  
  static int bufCount = 0;
  
  if(init==TRUE)
  {
    for(int i=0;i<FILTER_SIZE;i++)
    {
        buffer[i]=data;        
    }
    init=FALSE;
    bufCount=1;
  }
  else
  {
    buffer[bufCount] = data;
    bufCount++;
    if(bufCount == FILTER_SIZE)
      bufCount = 0;    
  }
  
  for(int i=0;i<FILTER_SIZE;i++)
  {
    fOut+=buffer[i];
  }
  
  fOut=fOut/FILTER_SIZE;
  return fOut;  
}

