#include <iostream>
#include <vector>
#include <list>
#include <functional>

#include "algorithm.hxx"
#include "world.hxx"
#include "node.hxx"
#include "problem.hxx"
#include "solution.hxx"

	



PNode 
algorithm::Astar(const World &world, refptr<Problem> problem ) {
    priority_queue<PNode,deque<PNode>,greater<PNode> > q;
    q.push(problem->initial_state);
    problem->clearClosedSet();

    while(! q.empty() ) 
      {
	PNode p = q.top();q.pop();

	if(problem->isInClosedSet(p) || ! problem->isValid(p))
	  continue;

	
	//printNode(cout,p,world);
	if(problem->goalTest(p)) 
	  return p;
	
	Node::vPNode nodes = problem->successor(p);
	for(Node::PNodeIterator it = nodes.begin();it != nodes.end();++it)
	  q.push(*it);
      }
    return PNode();
  }



void algorithm::tickOut() {
  static int ic = 0;
  switch (++ic % 4) {
  case 0:
    cout << "\b-" << flush;
    break;
  case 1:
    cout << "\b\\" << flush;
    break;
  case 2:
    cout << "\b|" << flush;
    break;
  case 3:
    cout << "\b/" << flush;
    break;
  }
  if(ic == 200) {
    ic = 0;
    cout << "\b#*";
  }
}
