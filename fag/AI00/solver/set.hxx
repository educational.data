#ifndef __SETV_H__INCLUDED__
#define __SETV_H__INCLUDED__

#include <set>
#include "defs.h"

using namespace std;

struct SetV {
  long iVal;

  SetV(const vector<Position> &vec) {
    int c = vec.size();
    
    Position pos[c];
    for(int v = 0;v<c;v++) {
      pos[v] = vec[v];
    }
    register bool notClean = true;
    while(notClean) 
      {
	notClean = false;
	for(int v = 0;v<c-1;v++) 
	  {
	    if(pos[v+1]<pos[v]) 
	      {
		register Position pswap;
		pswap = pos[v];
		pos[v] = pos[v+1];
		pos[v+1] = pswap;
		notClean = true;
	      }
	  }
      }
    iVal = 0;
    for(int v = 0;v<c;v++) {
      iVal = (iVal << 8) + pos[v];
    }
  }
  bool compare(const SetV &t) const {
    return iVal < t.iVal;
  }
};

inline bool operator < (const SetV &a, const SetV &b) 
{
  return a.compare(b);
}

#endif
