#ifndef __PROBLEM_H__INCLUDED__
#define __PROBLEM_H__INCLUDED__

#include "world.hxx"
#include "node.hxx"
#include <vector>


/**
 * Define the basic description of a problem.
 */



struct Problem {
  virtual ~Problem() { }
  PNode initial_state;
  virtual std::vector<PNode> successor(const PNode &node) = 0;
  virtual bool goalTest(const PNode &node) = 0;
  virtual int cost(const PNode &node) = 0;
  virtual bool isValid(PNode &node) = 0;

  virtual void clearClosedSet() = 0;
  virtual bool isInClosedSet(const PNode &node) = 0;
};

#endif
