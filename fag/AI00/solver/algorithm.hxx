#ifndef __ALGORITHM_H__INCLUDED__
#define __ALGORITHM_H__INCLUDED__

#include "refptr.hxx"
#include "node.hxx"
#include "problem.hxx"

class algorithm {
public:
  PNode Astar(const World &world, refptr<Problem> problem );
  void tickOut();
};

#endif
