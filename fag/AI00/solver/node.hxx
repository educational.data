/*
 * $Id: node.hxx,v 1.9 2006-10-24 19:47:04 tobibobi Exp $
 */
#ifndef __NODE_H_INCLUDED__
#define __NODE_H_INCLUDED__

#include "world.hxx"
#include <vector>
#include <queue>
#include "refptr.hxx"

using namespace std;

class Node;

typedef enum { DIR_UP,DIR_DOWN,DIR_LEFT,DIR_RIGHT,DIR_STILL } man_direction;
typedef refptr<Node> PNode;

class Node {
  //friend class PNode;
  //PNode pthis;
public:
  typedef vector < Position > vJPosition;
  typedef vJPosition::iterator JPositionIterator;
  typedef vJPosition::const_iterator const_JPositionIterator;

  typedef vector < PNode > vPNode;
  typedef vPNode::iterator PNodeIterator;
  typedef vPNode::const_iterator const_PNodeIterator;

  Node(const World &world);
  Node(const Node &node);

  ///
  /// Expand constructor. Used when we wants to expand a single node 
  /// with the expandNode function
  Node(const PNode parent, const int hysteresis, const vJPosition &t, 
       const Position &nJPos, const int id);

  Node(const PNode parent, const Position newManPos);

  ///
  /// Inserts the new jewel pos in the jewel array
  void merge(Position nJPos,int id);

  /// Indicates the distance traveled so far
  int cost;

  /// calculated distance { f(x) = h(x) + c(x) }
  int distance;

  /// The positions of the different jewels in this node point
  vJPosition jewelPos;
  Position manPos;

  /// Reverse traversion parent
  PNode parentNode;

  bool valid();
  void invalidate();
  bool isValid;
  
  /// indicate which jewel is actually moved in this node
  int jewelMoved;
  

  /// tell what direction whe have moved the man
  man_direction manDirection;

  bool jewelMoving;

};


void printNode(ostream &stream, const PNode &nod, const World &world);

/// operator function needed for priority_queue
bool operator < (const PNode &a, const PNode &b);
bool operator > (const PNode &a, const PNode &b);

#endif
