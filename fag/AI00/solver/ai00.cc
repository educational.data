/*
 * $Id: ai00.cc,v 1.17 2006-10-25 14:13:17 tobibobi Exp $
 */
#include "world.hxx"
#include "node.hxx"
#include "solution.hxx"
#include "refptr.hxx"
#include "jewelproblem.hxx"
#include <iostream>
#include <vector>
#include <list>
#include "algorithm.hxx"
#include "manproblem.hxx"



using namespace std;


void dumpResult(vector<PNode> &sol, string filename) {
  string forward = "\tperform(1);\n";
  string left = "\tperform(2);\n";
  string right = "\tperform(3);\n";
  string push = "\tperform(5);\n";
  
  ofstream cs(filename.c_str());

  man_direction cdir = DIR_UP;
  bool isCarring = false;
  bool firstMove = true;
  for(vector<PNode>::iterator it = sol.begin();it != sol.end();++it) {
    if(isCarring == true && (*it)->jewelMoving == false)
      cs << push;

    isCarring = (*it)->jewelMoving;

    if(cdir != (*it)->manDirection) {
      switch((*it)->manDirection) {
      case DIR_UP:
	if(cdir == DIR_LEFT)
	  cs << right;
	if(cdir == DIR_RIGHT)
	  cs << left;
	if(cdir == DIR_DOWN)
	  cs << left << left;
	break;

      case DIR_LEFT:
	if(cdir == DIR_DOWN)
	  cs << right;
	if(cdir == DIR_UP)
	  cs << left;
	if(cdir == DIR_RIGHT)
	  cs << left << left;
	break;

      case DIR_DOWN:
	if(cdir == DIR_RIGHT)
	  cs << right;
	if(cdir == DIR_LEFT)
	  cs << left;
	if(cdir == DIR_UP)
	  cs << left << left;
	break;

      case DIR_RIGHT:
	if(cdir == DIR_DOWN)
	  cs << left;
	if(cdir == DIR_UP)
	  cs << right;
	if(cdir == DIR_LEFT)
	  cs << left << left;
	break;
	
      default:
	break;
      }
      cdir = (*it)->manDirection;
    }
    if(!firstMove)
      cs << forward;
    firstMove = false;
  }
  cs << push;
  cs.close();
}

int
main (int argc,char *argv[])
{
  if(argc != 2) 
    {
      cerr << "This program can accept one and only one argument" << endl;
      return -1;
    }

  try 
    {
      World world(argv[1]);

      refptr<Problem> prop = new JewelProblem(world);
      //PNode n1 = new Node(world);
      //PNode n2 = new Node(n1,16);

      //refptr<Problem> prop = new ManProblem(world,n1,n2);
      
      algorithm alg;
      PNode tNode = alg.Astar(world,prop);
      JewelProblem *j = static_cast<JewelProblem *>(prop.instance());

      if(! tNode)
	{
	  cerr << "No solution" << endl;
	  return -1;
	}

      vector<PNode> &sol = j->solution;
      
      dumpResult(sol, "solution-data.txt");

      for(vector<PNode>::iterator it = sol.begin();it != sol.end();++it)
	printNode(cout,*it,world);

    } 
  catch(const std::exception &ex) 
    {
      cerr << ex.what() << endl;
    }
  return 0;
}
