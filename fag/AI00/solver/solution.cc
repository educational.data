#include "solution.hxx"


CompleteSolution::CompleteSolution(PNode top,const World &w): world(w) {
  cout << "Got to the goal in " << top->cost << "steps" << endl;
  steps.push_front(top);
  PNode nd = top;
  while(nd->cost != 0) {
    steps.push_front(nd->parentNode);
    nd = nd->parentNode;
  }
}
bool CompleteSolution::hasSolution() { 
  return true; 
}

bool CompleteSolution::testSolution() {
  return true;
}


ostream &operator << (ostream &stream, CompleteSolution &sol) {
  list<PNode>::iterator lit;
  sol.world.print(stream);
  stream << "\x1B[2J";
  for (lit = sol.steps.begin();lit != sol.steps.end();++lit) {
    printNode(stream, *lit,sol.world);
  }
  return stream;
}


