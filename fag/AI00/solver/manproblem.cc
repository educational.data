#include "manproblem.hxx"
#include <iostream>

using namespace std;

ManProblem::ManProblem(const World &world, const PNode from, const PNode to) 
:
  _world(world)
{
  initial_state = from;
  target = to;
}

void
ManProblem::deattachNodes() {
  initial_state = new Node(initial_state.value());
  target = new Node(target.value());

  initial_state->parentNode.clear();
  target->parentNode.clear();
}
  

bool 
ManProblem::testJewelPosFree(const Position pos, const PNode &nod) {
  Node::JPositionIterator jit;
  for(jit = nod->jewelPos.begin();jit != nod->jewelPos.end();++jit)
    if(*jit == pos)
      return false; 

  return ! _world.isWall(pos);
}

std::vector<PNode> 
ManProblem::successor(const PNode &node) {
  std::vector<PNode> resultset;
  std::vector<PNode>::iterator it;
  Position cPos = node->manPos;
  Position nPos;
  man_direction cdir = node->manDirection;

  nPos = cPos+1;
  
  if(testJewelPosFree(nPos,node)) {
    PNode nod = new Node(node,nPos);
    if(cdir != DIR_RIGHT)nod->cost++;
    nod->manDirection = DIR_RIGHT;
    nod->cost++;
    resultset.push_back(nod);
  }
  
  nPos = cPos-1;

  if(testJewelPosFree(nPos,node)) {
    PNode nod = new Node(node,nPos);
    if(cdir != DIR_LEFT)nod->cost++;
    nod->manDirection = DIR_LEFT;
    nod->cost++;
    resultset.push_back(nod);
}

  nPos = _world.above(cPos);

  if(testJewelPosFree(nPos,node)){
    PNode nod = new Node(node,nPos);
    if(cdir != DIR_UP)nod->cost++;
    nod->manDirection = DIR_UP;
    nod->cost++;
    resultset.push_back(nod);
  }

  nPos = _world.below(cPos);

  if(testJewelPosFree(nPos,node)) {
    PNode nod = new Node(node,nPos);
    if(cdir != DIR_DOWN)nod->cost++;
    nod->manDirection = DIR_DOWN;
    nod->cost++;
    resultset.push_back(nod);
  }

  for(it=resultset.begin();it != resultset.end();++it)
    cost(*it);

  return resultset;
}

bool 
ManProblem::goalTest(const PNode &node) {
  return (node->manPos == target->manPos);
}

int 
ManProblem::cost(const PNode &node) {
  int cost = node->cost;
  cost+=(int)((double)_world.manhattenDistance(node->manPos,target->manPos));

  *(const_cast<int*>(&node->distance)) = cost;
  return node->distance;
}
bool 
ManProblem::isValid(PNode &node) {
  return true;
}

void 
ManProblem::clearClosedSet() {
  closed_set.clear();
}

bool 
ManProblem::isInClosedSet(const PNode &node) {
  if(closed_set.find(node->manPos) != closed_set.end())
    return true;

  closed_set.insert(node->manPos);
  return false;
}
