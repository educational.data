/*
 * $Id: node.cc,v 1.11 2006-10-24 19:47:04 tobibobi Exp $
 */
#include "node.hxx"
#include <iostream>
#include <vector>

using namespace std;

Node::Node(const PNode parent, 
	   const int hysteresis, 
	   const vJPosition &t, 
	   const Position &nJPos, 
	   const int id)
  : 
  cost(hysteresis),
  distance(0),
  jewelPos(t),
  manPos(parent->manPos),
  parentNode(parent),
  isValid(true),
  manDirection(parent->manDirection),
  jewelMoving(true)
{ 
  merge(nJPos,id);
}

Node::Node(const World &world) :
  cost(0),
  distance(0),
  jewelPos(world.jewels),
  manPos(world.man),
  isValid(true),
  jewelMoved(-1),
  manDirection(DIR_STILL),
  jewelMoving(false)
{

}

Node::Node(const Node &node) :
  cost(node.cost),
  distance(node.distance),
  jewelPos(node.jewelPos),
  manPos(node.manPos),
  parentNode(node.parentNode),
  isValid(node.isValid),
  jewelMoved(node.jewelMoved),
  manDirection(node.manDirection),
  jewelMoving(node.jewelMoving)
{

}

Node::Node(const PNode parent, const Position newManPos) :
  cost(parent->cost+1),
  distance(0),
  jewelPos(parent->jewelPos),
  manPos(newManPos),
  parentNode(parent),
  isValid(true),
  jewelMoved(-1),
  manDirection(DIR_STILL),
  jewelMoving(false)
{
}

void Node::merge(Position nJPos,int id) {
  // remember which jewel we have moved
  jewelMoved = id;

  /*   
  // where did we come from?
  if(_world.above(_jewelPos[id]) == nJPos)
    _jewelDirection = JEWEL_DOWN;

  if(_world.below(_jewelPos[id]) == nJPos)
    _jewelDirection = JEWEL_UP;

  if(_jewelPos[id]-1 == nJPos)
    _jewelDirection = JEWEL_RIGHT;

  if(_jewelPos[id]+1 == nJPos)
  _jewelDirection = JEWEL_LEFT; 
  
  */

  // save the new pos of the jewel.
  jewelPos[id] = nJPos;
}

bool 
Node::valid() {
  if(isValid == false) return false;
  PNode n = parentNode;
  while(n) 
    {
      if(n->isValid == false) return false;
      n = n->parentNode;
    }
  return true;
}

void 
Node::invalidate() {
  isValid = false;
}

bool operator < (const PNode &a, const PNode &b)  {
  return a->distance < b->distance;
}

bool operator > (const PNode &a, const PNode &b) {
  return a->distance > b->distance;
}


void
printNode(ostream &stream, const PNode &nod, const World &world) {
  stream << "\x1B[1;1H";
  priority_queue<Position, vector<Position>,greater<Position> > 
    q(nod->jewelPos.begin(),nod->jewelPos.end());
  q.push(nod->manPos);
  int start = 0;
  cout << endl << endl;
  while(!q.empty()) {
    world.printFromTo(stream,start,q.top());
    start = q.top()+1;
    if(q.top() != nod->manPos)
      stream << "J";
    else
      stream << "M";
    q.pop();      
  } 
  
  world.printFrom(stream,start);
  
  usleep(100000);
  stream << endl;
}


