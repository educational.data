/*
 * $Id: world.hxx,v 1.6 2006-10-13 05:06:36 tobibobi Exp $
 */
#ifndef __WORLD_H_INCLUDED__
#define __WORLD_H_INCLUDED__
#include <fstream>
#include <vector>
#include "defs.h"

using namespace std;

typedef enum  {
  CELL_NONE,
  CELL_EMPTY,
  CELL_WALL,
} cells;

/**
 * Exception class to indicate an error while running a
 * loader. Its most likely that there already exists such
 * an exception, but i just made one to be sure.
 */
class file_exception : public std::exception {
  string msg;
public:
  file_exception(string reason) throw () {
    msg = reason;
  }

  ~file_exception() throw () {

  }

  const char *what() const throw () {
    return msg.c_str();
  }
};

/**
 * The representation of the world.
 *
 * The world class handles loading of a worldmap file.
 * after that it has knowledge of the static features
 * of the world representation.
 */
class World {
  ifstream _file;
  cells *_cells;
  int size_x, size_y, num_jewels;
public:
  World(const char *filename);
  ~World();

  inline Position above(Position pos) const {
    return pos-size_x;
  }
  inline Position below(Position pos) const {
    return pos + size_x;
  }

  void printFromTo(ostream &ost, Position from, Position to) const;
  void printFrom(ostream &ost, Position from) const;
  void print(ostream &ost) const;
  

  int manhattenDistance(const Position pos) const;
  int manhattenDistance(const Position from, const Position to) const;

  inline int xPos(const Position pos) const { 
    return pos % size_x;
  }
  inline int yPos(const Position pos) const {
    return (int)((float)pos / size_x);
  }

  bool foundGoal(const vector<Position> & positions) const;

  bool isWall(const int x, const int y) const;  

  bool isWall(const Position pos) const;

  vector < Position > jewels;
  vector < Position > goals;
  Position man;
};

#endif
