#include "world.hxx"
#include <iostream>

World::World(const char *filename) {
  _file.open(filename,ios::in);
  
  if(! _file.good())
    throw file_exception("Error, cannot open file");
  _file >> size_x;
  _file >> size_y;
  _file >> num_jewels;
  _file.ignore(10,'\n');
  
  // we know the size of the cells - make room for the matrix
  _cells = new cells[size_x*size_y];
  int array_pos = 0;

  for(int lineNum=0;lineNum<size_y;lineNum++) {
    char line[size_x+1];
    _file.getline(line,size_x+1);
    
    for(int pos=0;pos<size_x;pos++) {
      switch(line[pos]) {
      case ' ':
	_cells[array_pos] = CELL_NONE;
	break;
      case '.':
	_cells[array_pos] = CELL_EMPTY;
	break;
      case '#':
	_cells[array_pos] = CELL_WALL;
	break;
      case 'J':
	_cells[array_pos] = CELL_EMPTY;
	jewels.push_back(array_pos);
	break;
      case 'G':
	_cells[array_pos] = CELL_EMPTY;
	goals.push_back(array_pos);
	break;
      case 'M':
	_cells[array_pos] = CELL_EMPTY;
	man = array_pos;
	break;
      default:
	cout << lineNum << "," << pos << endl;
	throw file_exception("Encountered a non cell charecter");
      }
      ++array_pos;
    }
  }
}

World::~World() {
  delete[] _cells;
}

int
World::manhattenDistance(Position pos) const 
{
  vector < Position >:: const_iterator it;
  int shortest_distance = 0xFF;
  for (it=goals.begin();it!=goals.end();++it) {
    int distance = manhattenDistance(*it,pos);
    shortest_distance = distance < shortest_distance ? distance : shortest_distance;
  }
  return shortest_distance;
}

bool 
World::foundGoal(const vector<Position> & positions) const
{
  for(vector <Position>::const_iterator pit = positions.begin(); pit != positions.end();++pit) 
    {
      bool notFound = true;
      for(vector <Position>::const_iterator git = goals.begin(); git != goals.end();++git) 
	{
	  if(*git == *pit) 
	    {
	      notFound = false;
	      continue;
	    }
	}
      if(notFound) 
	return false;
    }
  return true;
    
  //return includes(goals.begin(),goals.end(),positions.begin(),positions.end());
}

int
World::manhattenDistance(const Position from, const Position to) const 
{
  return abs(xPos(from)-xPos(to)) + abs(yPos(from)-yPos(to));
}

bool 
World::isWall(const Position pos) const {
  return _cells[pos] == CELL_WALL;
}

bool
World::isWall(int x, int y) const {
  return isWall(x+y*size_x);
}

void 
World::printFromTo(ostream &ost,int f, int t) const {
  if(f==0) cout << "   ";
  for (int i = f;i<t;i++) {
    switch(_cells[i]) {
    case CELL_NONE:
      ost << " ";
      break;
    case CELL_WALL:
      ost << "#";
      break;
    case CELL_EMPTY:
      ost << ".";
    }
    if((i+1) % size_x == 0)
      ost << "    " << endl << "   ";
  }
}

void 
World::printFrom(ostream &ost,int f) const {
  printFromTo(ost,f,size_x*size_y);
}

void
World::print(ostream & ost) const {
  printFromTo(ost,0,size_x*size_y);
}
