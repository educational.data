/*
 * $Id: refptr.hxx,v 1.6 2006-10-24 19:47:04 tobibobi Exp $
 */
#ifndef __REFPTR_H__INCLUDED__
#define __REFPTR_H__INCLUDED__

#include <exception>

struct ref_exception: public std::exception {
  ref_exception() throw () {

  }
  const char *what() const throw() {
    return "ref_exception:: tried to access empty refptr value";
  }
};

template <typename T>
class refptr {
  T *theInstance;
  int *refCnt;
  bool _empty;
public:
  typedef T value_type;
  typedef T &reference;
  typedef const T const_value_type;
  typedef const T &const_reference;
  typedef T*pointer;
  typedef const T* const_pointer;
  typedef refptr<T> refptr_type;
  typedef refptr<T> &reference_refptr;
  typedef const refptr<T> const_refptr;
  typedef const refptr<T> &const_reference_refptr;

  refptr()
  {
    _empty = true;
    theInstance = NULL;
    refCnt = NULL;
  }
  
  refptr(const_pointer instance) 
  {
    if(instance == NULL) {
      refCnt = NULL;
      theInstance = NULL;
      _empty = true;
      return;
    }
    _empty = false;
    refCnt = new int;
    *refCnt = 1;
    theInstance = const_cast<T *>(instance);
  }

  refptr(const_reference_refptr rinstance) {
    _empty = rinstance._empty;
    refCnt = rinstance.refCnt;    
    theInstance = rinstance.theInstance;

   if(_empty)
      return;

    ++(*refCnt);
  }

  reference_refptr operator = (const_reference_refptr rinstance) {
    release();
    refCnt = rinstance.refCnt;
    theInstance = rinstance.theInstance;
    _empty = rinstance._empty;
    if(!_empty)
      ++(*refCnt);
    return *this;
  }

  virtual ~refptr() {
    release();
  }

  T *operator->() const {
    if(_empty) 
      throw std::bad_alloc();
    return theInstance;
  }

  bool empty() const {
    return _empty;
  }

  void release() {
    if(!_empty) 
      {
	if(--(*refCnt) == 0)
	  {
	    delete theInstance;
	    delete refCnt;
	  }
      }
    _empty = true;
  }

  void clear() { release(); }

  pointer instance() throw (std::bad_alloc) {
    if(_empty) 
      throw std::bad_alloc();
    return theInstance;
  }

  operator bool() {
    return !empty();
  }

  const_pointer instance() const throw (std::bad_alloc) {
    if(_empty) 
      throw std::bad_alloc();
    return theInstance;
  }

  reference value() throw (std::bad_alloc) {
    if(_empty) 
      throw std::bad_alloc();
    return *theInstance;
  }

  const_reference value() const throw (std::bad_alloc) {
    if(_empty) 
      throw std::bad_alloc();
    return *theInstance;
  }
};

template <typename T>
inline bool operator==(const refptr<T> &a, const refptr<T> &b) {
  return a.instance() == b.instance();
}

template <typename T>
inline bool operator!=(const refptr<T> &a, const refptr<T> &b) {
  return a.instance() != b.instance();
}

#endif
