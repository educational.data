#include "jewelproblem.hxx"
#include "node.hxx"
#include "manproblem.hxx"
#include "solution.hxx"
#include "algorithm.hxx"


#include <iostream>
using namespace std;


JewelProblem::JewelProblem(const World &world) : _world(world)
{
  _goals = world.goals;
  initial_state = PNode(new Node(world));
}

bool 
JewelProblem::canMoveHoriz(const PNode &nod,const Position pos) {
  Node::JPositionIterator jit;
  for(jit = nod->jewelPos.begin();jit != nod->jewelPos.end();++jit)
    if(*jit == pos-1 || *jit == pos+1)
      return false; 

  return (! _world.isWall(pos-1) && ! _world.isWall(pos+1));
}

bool 
JewelProblem::canMoveVert(const PNode &nod,const Position pos) {
  Node::JPositionIterator jit;
  for(jit = nod->jewelPos.begin();jit != nod->jewelPos.end();++jit)
    if(*jit == _world.above(pos) || *jit == _world.below(pos))
      return false; 

  return (! _world.isWall(_world.above(pos)) && ! _world.isWall(_world.below(pos)));
}
std::vector<PNode> 
JewelProblem::successor(const PNode &node) {
  Node::JPositionIterator jewel_it;
  /*  if(! node->valid()) 
      return std::vector<PNode>(); */

  int id = 0;

  Node::vPNode reachableNodes;

  int walk_cost = node->cost+1;

  Position manPos = node->manPos;

  for(jewel_it = node->jewelPos.begin(); jewel_it != node->jewelPos.end(); ++jewel_it) 
    {
      Position pos = *jewel_it;
      PNode n;
      if(canMoveHoriz(node,pos)) 
	{
	  if(manPos != pos-1) 
	    {
	      n = new Node(node,pos-1);
	      n->jewelMoved = id;
	      n->manDirection = DIR_RIGHT;
	      n = buildManRoute(node,n);
	    }
	  else 
	    {
	      n = new Node(node,walk_cost,node->jewelPos,pos+1,id);
	      n->manPos = pos;
	      n->manDirection = DIR_RIGHT;
	    }
	  if(n)reachableNodes.push_back(n);
	  n.clear();


	  if(manPos != pos+1) 
	    {
	      n = new Node(node,pos+1);
	      n->jewelMoved = id;
	      n->manDirection = DIR_LEFT;
	      n = buildManRoute(node,n);
	    }
	  else 
	    {
	      n = new Node(node,walk_cost,node->jewelPos,pos-1,id);
	      n->manPos = pos;
	      n->manDirection = DIR_LEFT;
	    }
	  if(n)reachableNodes.push_back(n);
	  n.clear();
	}

      if(canMoveVert(node,pos)) 
	{
	  if(manPos != _world.above(pos)) 
	    {
	      n = new Node(node,_world.above(pos));
	      n->jewelMoved = id;
	      n->manDirection = DIR_DOWN;
	      n = buildManRoute(node,n);
	    }
	  else 
	    {
	      n = new Node(node,walk_cost,node->jewelPos,_world.below(pos),id);
	      n->manPos = pos;
	      n->manDirection = DIR_DOWN;
	    }
	  if(n)reachableNodes.push_back(n);
	  n.clear();

	  if(manPos != _world.below(pos)) 
	    {
	      n = new Node(node,_world.below(pos));
	      n->jewelMoved = id;
	      n->manDirection = DIR_UP;
	      n = buildManRoute(node,n);
	    }
	  else 
	    {
	      n = new Node(node,walk_cost,node->jewelPos,_world.above(pos),id);
	      n->manPos = pos;
	      n->manDirection = DIR_UP;
	    }
	  if(n)reachableNodes.push_back(n);
	  n.clear();
	}
      id++;
    }
  for(Node::vPNode::iterator it = reachableNodes.begin();it!= reachableNodes.end();++it)
    cost(*it);
  return reachableNodes;
}

bool 
JewelProblem::goalTest(const PNode &node) {
  for(vector <Position>::const_iterator pit = node->jewelPos.begin(); pit != node->jewelPos.end();++pit) 
    {
      bool notFound = true;
      for(vector <Position>::const_iterator git = _goals.begin(); git != _goals.end();++git) 
	{
	  if(*git == *pit) 
	    {
	      notFound = false;
	      continue;
	    }
	}
      if(notFound) 
	return false;
    }
  return testForManMotion(node);
}

PNode 
JewelProblem::buildManRoute(PNode start, PNode end) {
  refptr<Problem> prop = new ManProblem(_world,start,end);
  algorithm alg;
	      
  PNode endNode = alg.Astar(_world,prop);

  return endNode;
}

bool
JewelProblem::testForManMotion(PNode node) {
  deque<PNode> steps;

  PNode referenceNode = node;

  while(referenceNode) {
    steps.push_front(referenceNode);
    referenceNode = referenceNode->parentNode;
  }
  
  cout << "." << flush;
  deque<PNode>::iterator it = steps.begin();
  solution.clear();
  solution = vector<PNode>(steps.begin(),steps.end());
  return true;
}


int 
JewelProblem::cost(const PNode &node) {
  
  int cost = node->cost;
  
  for(Node::const_JPositionIterator it = node->jewelPos.begin();it != node->jewelPos.end();++it) {
    cost += (int)((float)_world.manhattenDistance(*it));
  }
  
  if(! node->parentNode) 
    if(node->parentNode->jewelMoved != node->jewelMoved) 
      cost += _world.manhattenDistance(node->parentNode->manPos,
				       node->manPos);
  

  *(const_cast<int*>(&node->distance)) = cost;
  return node->distance;
}

bool
JewelProblem::isValid(PNode &nd) {
  return true;
  PNode pnd = nd->parentNode;
  if(!pnd) 
    return true;

  if(pnd->jewelMoved != nd->jewelMoved || pnd->manDirection != nd->manDirection) 
    {
      PNode endNode = buildManRoute(nd,pnd);
      if(! endNode) 
	return false;
      nd->parentNode = endNode;
      
      return true;
    }
  return true;//nd->valid();
}

void JewelProblem::clearClosedSet() {
  closed_set.clear();
}


static long calcPosition(const vector<Position> &vec) {
  int c = vec.size();
  
  Position pos[c];
  for(int v = 0;v<c;v++) {
    pos[v] = vec[v];
  }

  register bool notClean = true;
  while(notClean) 
    {
      notClean = false;
      for(int v = 0;v<c-1;v++) 
	{
	  if(pos[v+1]<pos[v]) 
	    {
	      register Position pswap;
	      pswap = pos[v];
	      pos[v] = pos[v+1];
	      pos[v+1] = pswap;
	      notClean = true;
	    }
	}
    }
  long long iVal = 0;
  for(int v = 0;v<c;v++) {
    iVal = (iVal << 7) | (pos[v] & 0x7f);
  }
  return iVal;
}

bool JewelProblem::isInClosedSet(const PNode &node) {
  long long val = calcPosition(node->jewelPos);
  val = (val << 9) + ((node->manPos & 0x7f)<<3) + ((node->manDirection & 3));
  if(closed_set.find(val) != closed_set.end()) {
    return true;
  }

  closed_set.insert(val);
  return false;
}






