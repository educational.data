#ifndef __MANPROBLEM_H__INCLUDED__
#define __MANPROBLEM_H__INCLUDED__

#include "problem.hxx"
#include <set>

class World;

class ManProblem : public Problem {
  PNode target;
  const World &_world;
  bool testJewelPosFree(const Position pos, const PNode &nod);
  std::set<Position> closed_set;
public:
  ManProblem(const World &world, const PNode from, const PNode to);

  void deattachNodes();
  
  std::vector<PNode> successor(const PNode &node);
  bool goalTest(const PNode &node);
  int cost(const PNode &node);
  bool isValid(PNode &node);

  void clearClosedSet();
  bool isInClosedSet(const PNode &node);
};

#endif
