/*
 * $Id: solution.hxx,v 1.3 2006-10-13 05:06:36 tobibobi Exp $
 */
#ifndef __SOLUTION_H__INCLUDED__
#define __SOLUTION_H__INCLUDED__

#include "node.hxx"
#include "world.hxx"

#include <iostream>
#include <vector>
#include <queue>
#include <list>
#include <functional>
using namespace std;

struct Solution {
  virtual bool hasSolution() = 0;
  virtual ~Solution() { }
};

struct NoSolution : public Solution {
  bool hasSolution() { return false; }
};

struct CompleteSolution : public Solution {
  list <PNode> steps;
  bool solutionIsFound;
  const World &world;

  CompleteSolution(PNode top, const World &w);

  bool testSolution();

  bool hasSolution();
};

ostream &operator << (ostream &stream, CompleteSolution &sol);

#endif
