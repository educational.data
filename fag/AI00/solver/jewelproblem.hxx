#ifndef __JEWELPROBLEM_H__INCLUDED__
#define __JEWELPROBLEM_H__INCLUDED__

#include "world.hxx"
#include "problem.hxx"
#include <vector>
#include <set>

/**
 * Define the basic description of a problem.
 */

class JewelProblem : public Problem {
  const World &_world;
  std::vector<Position> _goals;
  std::set<long long> closed_set;
  bool testForManMotion(PNode parent_node);
  bool canMoveHoriz(const PNode &nod,const Position pos);
  bool canMoveVert(const PNode &nod,const Position pos);
  PNode buildManRoute(PNode start, PNode end);
public:
  std::vector<PNode> solution;
  JewelProblem(const World &world);

  std::vector<PNode> successor(const PNode &node);
  bool goalTest(const PNode &node);
  int cost(const PNode &node);
  bool isValid(PNode &node);

  void clearClosedSet();
  bool isInClosedSet(const PNode &node);
};

#endif
