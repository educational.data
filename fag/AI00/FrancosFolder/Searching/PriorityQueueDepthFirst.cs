namespace Searching{

  public class PriorityQueueDepthFirst:PriorityQueue{

    int nodesAdded = -1;

    public PriorityQueueDepthFirst():base(){}

    ///This method leaves at the beginning of the queue, the node with biggest depth.
    override protected void Insert(Node node){
      nodesAdded++;
      Key2D key = new Key2D(node.Depth,nodesAdded);
      openList.Add(key,node);
    }

  }

}
