namespace Searching{

  public class PriorityQueueAStar:PriorityQueue{
    
    private int nodesAdded = -1;

    public PriorityQueueAStar():base(){}

    override protected void Insert(Node node){
      nodesAdded++;
      int evaluationFunction = node.State.HeuristicFunction();
      if(evaluationFunction < System.Int32.MaxValue){
  	evaluationFunction += node.PathCost;
      }
      Key2D key = new Key2D(evaluationFunction,nodesAdded);
      openList.Add(key,node);
    }

  }

}
