namespace Searching{
  
  public class Solver{

    private PriorityQueue fringe;

    public Solver(Node root, Strategies strategy){
      fringe = CreateFringe(strategy);
      Node[] nodes = {root};
      fringe.InsertAll(nodes);
    }

    ///This function returns a solution to the search (an array of nodes), or null if not solutions
    ///are found. This function is indeed a GraphSearch, because there is not repeated stages.
    public Node[] TreeSearch(bool verbose, int speed){
      
      while(true){

	//It returns null if the fringe is empty...
	Node leaf = fringe.RemoveFirst();

	if(leaf == null){
	  return null;
	}

	if(verbose){
          System.Console.Clear();
  	  System.Console.SetCursorPosition(0,0);
  	  System.Console.WriteLine(leaf.State);
  	  //System.Console.WriteLine(leaf);
	  //System.Console.WriteLine(fringe);
  	  System.Threading.Thread.Sleep(speed);
	}

	
	if(leaf.State.IsGoal()){
	  //We return the solution...
	  return CreateSolution(leaf);
	}
	else{
	  //We expand and insert the new leafs in the fringe...
	  Node[] newLeafs = leaf.Expand();
	  if(newLeafs != null){
	    fringe.InsertAll(newLeafs);
	  }
	}

      }//End main loop...
      
      return null;
    }

    private PriorityQueue CreateFringe(Strategies strategy){
      switch(strategy){
	case Strategies.DEPTH_FIRST:
	  return new PriorityQueueDepthFirst();
	case Strategies.ITERATIVE_DEPTH_FIRST:
	  return new PriorityQueueIterativeDepthFirst();
	case Strategies.GREEDY_BEST_FIRST:
	  return new PriorityQueueGreedyBestFirst();
	case Strategies.A_STAR:
	  return new PriorityQueueAStar();
	default:
	  return null;
      }
    }

    private Node[] CreateSolution(Node goalLeaf){
      System.Collections.Generic.Stack<Node> nodes = new System.Collections.Generic.Stack<Node>();
      nodes.Push(goalLeaf);
      Node ancestor = goalLeaf.Parent;
      while(ancestor != null){
	nodes.Push(ancestor);
	ancestor = ancestor.Parent;
      }
      return nodes.ToArray();
    }

  }

}
