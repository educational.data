namespace Searching{

  public enum Strategies{
    DEPTH_FIRST,
    ITERATIVE_DEPTH_FIRST,
    GREEDY_BEST_FIRST,
    A_STAR
  }
  
}
