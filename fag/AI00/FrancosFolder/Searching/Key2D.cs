namespace Searching{

  ///This class should implement at future the interface System.IComparable<Key2D>, which
  ///is the generic version of the interface being implemented now. Mono is not ready for
  ///this generic interface yet.
  public class Key2D:System.IComparable{
    
    private int x;
    private int y;

    public int X{get{return x;}}
    public int Y{get{return y;}}

    public Key2D(int x,int y){
      this.x = x;
      this.y = y;
    }

    public int CompareTo(System.Object other){
      Key2D otherKey = (Key2D)other;
      if(this.x > otherKey.x){
	return 1;
      }
      else if(this.x < otherKey.x){
	return -1;
      }
      else{//Here means x's are equals...
	if(this.y > otherKey.y){
	  return 1;
	}
	else if(this.y < otherKey.y){
	  return -1;
	}
	else{
  	  return 0;
	}
      }
    }

  }

}
