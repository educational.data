namespace Searching{

  ///This class must be specifically impemented for the problem being faced.
  ///The two methods to be implemented do not require to be used in combination.
  ///It is not required to detect a goal by comparing an specific state. A goal
  ///can be expressed in several ways.
  abstract public class State{

    abstract public bool IsGoal();

    abstract public bool IsEqual(Searching.State other);

    ///This method is to be used in Informed Search Methods. For example, this
    ///metric could give information about the distance from the state to the goal.
    ///The lowest distance the state is the one that appears to be the best.
    ///Also, the values returned by the Heuristic Function should have the magnitude
    ///of the ones in pathCost. Finally, it should deliver a huge value for states
    ///that are not going to achieve ever to the goal.
    abstract public int HeuristicFunction();

  }

}
