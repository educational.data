namespace Searching{
  
  ///This class is an abstract class. Objects cannot be created. It is used to create a flexible
  ///architecture for search algorithms (speed to be evaluated). This class must be specifically
  ///implemented for the problem being faced.
  abstract public class Node{

    ///The state that the node represents.
    protected State state;
    ///The node from which this node was expanded.
    protected Node parent;
    ///The previous action taken to arrive to this state. If the previous action is null,
    ///it means the node is the initial state.
    protected Action action;
    protected int pathCost;
    protected int depth;

    public State State{get{return state;}}
    public Node Parent{get{return parent;}}
    public Action Action{get{return action;}}
    public int PathCost{get{return pathCost;}}
    public int Depth{get{return depth;}}

    ///Root constructor. This constructor is just used to create the root node. All the other nodes
    ///should be created just by expanding.
    public Node(State initialState){
      this.state = initialState;
      this.parent = null;
      this.action = null;
      this.pathCost = 0;
      this.depth = 0;
    }

    ///This constructor is used to generate the nodes by expanding.
    public Node(State state, Node parent, Action action, int pathCost, int depth){
      this.state = state;
      this.parent = parent;
      this.action = action;
      this.pathCost = pathCost;
      this.depth = depth;
    }

    ///This function expand the node by using an specific succesor function. Also, it returns
    ///the group of new nodes generated. At every implementation, it should avoid the repeated
    ///states.
    abstract public Node[] Expand();

    ///Cool. But it could also returns some information regarding the pathCost (as propose in
    ///the book). If the pathCost are non decresing, then there is no problem to leave the
    ///function like this. Othercase, there have to be included pointers from parents to
    ///children, to replace the future pathCost cheaper parent at those nodes.
    /*public bool IsRepeated(){
      
      Node ancestor = parent;
      //I go back until the root.
      while(ancestor != null){
	if(state.IsEqual(ancestor.State)){
	  return true;
	}
	ancestor = ancestor.parent;
      }
      return false;
    }*/

  }
  
}
