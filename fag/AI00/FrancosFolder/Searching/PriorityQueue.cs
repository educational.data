namespace Searching{

  ///This class is implemented based in C# sorted dictionary. No repeated keys are allowed.
  ///It must also care about not including repeated nodes.
  abstract public class PriorityQueue{

    protected System.Collections.Generic.SortedList<Key2D,Node> openList;
    protected System.Collections.Generic.List<Node> closedList;

    public PriorityQueue(){
      openList = new System.Collections.Generic.SortedList<Key2D,Node>();
      closedList = new System.Collections.Generic.List<Node>();
    }

    public bool IsEmpty(){
      if(openList.Count == 0){
	return true;
      }
      return false;
    }

    public Node First(){
      if(openList.Count == 0){
	return null;
      }
      //return openList[0];
      return openList.Values[0];
    }

    ///This method can be override at derived classes, in case it is needed. Not obligatory.
    virtual public Node RemoveFirst(){
      if(openList.Count == 0){
	return null;
      }
      //Node first = openList[0];
      //openList.RemoveAt(0);
      //return first;
      Node first = openList.Values[0];
      openList.RemoveAt(0);
      return first;
    }

    abstract protected void Insert(Node node);

    ///This method excludes the repeated states, based in the closed list...
    public void InsertAll(Node[] nodes){
      bool matches;
      foreach(Node node in nodes){
	matches = false;
	foreach(Node previousNode in closedList){
	  if(previousNode.State.IsEqual(node.State)){
	    matches = true;
	    break;
	  }
	}
	if(!matches){
	  closedList.Add(node);
  	  Insert(node);
	}
      }
    }

    override public string ToString(){
      string queue = "";
      foreach(Key2D key in openList.Keys){
	queue += "(" + key.X + "," + key.Y + ") ";
      }
      return queue;
    }

  }

}
