namespace Searching{
  
  public class PriorityQueueIterativeDepthFirst:PriorityQueueDepthFirst{

    private int depth = 0;
    private Node root;

    public PriorityQueueIterativeDepthFirst():base(){}

    override public Node RemoveFirst(){

      if(openList.Count == 0){
	depth++;
	closedList.Clear();
	Node[] nodes = {root};
	InsertAll(nodes);
      }

      Node first = openList.Values[0];
      openList.RemoveAt(0);

      if(depth == 0){
	root = first;
      }

      return first;
    }

    override protected void Insert(Node node){
      if(node.Depth <= depth){
	base.Insert(node);
      }
    }

  }

}
