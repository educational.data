namespace Searching{

  public class PriorityQueueGreedyBestFirst:PriorityQueue{
    
    private int nodesAdded = -1;

    public PriorityQueueGreedyBestFirst():base(){}

    override protected void Insert(Node node){
      nodesAdded++;
      //EvaluationFunction = HeuristicFunction.
      Key2D key = new Key2D(node.State.HeuristicFunction(),nodesAdded);
      openList.Add(key,node);
    }

  }
}
