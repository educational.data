///This namespace contains the classes needed to solve the Sokoban game,
///by using AI searching algorithms.
namespace Sokoban{

  public class Game{

    ///The instruction should be something like this #mono Sokoban.exe MapFile2.txt Solution2Jewels.txt
    static public void Main(string[] args){
      bool verbose = false;
      if(args.Length < 1){
	System.Console.WriteLine("Map file not included...");
	return;
      }
      else{
	//Try to read the last argument... args[args.Length-1] to be sure they give us a right file.
	for(int i=0;i<args.Length-2;i++){
	  switch(args[i]){
	    case "-v":
	      verbose = true;
	      break;
	  }
	}
      }
      System.Console.WriteLine("Solving sokoban game...");
      State initialState = new State(args[args.Length-2]);
      Node root = new Node(initialState);
      System.Console.WriteLine(root.State);
      //Searching.Solver solver = new Searching.Solver(root,Searching.Strategies.DEPTH_FIRST);
      //Searching.Solver solver = new Searching.Solver(root,Searching.Strategies.ITERATIVE_DEPTH_FIRST);
      //Searching.Solver solver = new Searching.Solver(root,Searching.Strategies.GREEDY_BEST_FIRST);
      Searching.Solver solver = new Searching.Solver(root,Searching.Strategies.A_STAR);
      System.DateTime startTime = System.DateTime.Now;
      Searching.Node[] solution = solver.TreeSearch(verbose,100);
      if(solution != null){
	System.DateTime endTime = System.DateTime.Now;
	System.TimeSpan duration = endTime - startTime;
	System.Console.WriteLine("GOOOOOOOOOOAAAAAALLLLLL!");
	System.Console.WriteLine(duration);
	System.Threading.Thread.Sleep(2000);
	Game myGame = new Game();
	myGame.ShowSolution(solution);
	myGame.ExportSolution(solution,args[args.Length-1]);
      }
      else{
	System.Console.WriteLine("null solution...");
	System.Threading.Thread.Sleep(2000);
      }
    }
       
    public void ShowSolution(Searching.Node[] solution){
      System.Console.WriteLine("Showing solution...");
      int steps = 0;
      for(int i=0; i<solution.Length-1;i++){
	
	Sokoban.State currentState = (Sokoban.State)solution[i].State;
	Sokoban.State nextState = (Sokoban.State)solution[i+1].State;
	Sokoban.Action nextAction = (Sokoban.Action)solution[i+1].Action;

	System.Console.SetCursorPosition(0,0);
	System.Console.WriteLine(currentState);
	System.Threading.Thread.Sleep(100);

	char[,] stateMap = (char[,])Sokoban.State.map.Clone();
	foreach(Math.Point2D tempJewel in currentState.Jewels){
	  stateMap[tempJewel.X,tempJewel.Y] = 'J';
	}

	Math.Point2D man = (Math.Point2D)currentState.Man.Clone();
	Math.Point2D goal = new Math.Point2D(-1,-1);

	switch(nextAction.Movement){
	  case Connectivity.Movements.UP:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X+1;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y;
	    break;
	  case Connectivity.Movements.LEFT:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y+1;
	    break;
	  case Connectivity.Movements.DOWN:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X-1;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y;
	    break;
	  case Connectivity.Movements.RIGHT:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y-1;
	    break;
	}

	Connectivity.State initialState = new Connectivity.State(man,goal,stateMap);
	Connectivity.Node connectivityRoot = new Connectivity.Node(initialState);
	Searching.Solver solver = new Searching.Solver(connectivityRoot,Searching.Strategies.A_STAR);
	Searching.Node[] solutionMan = solver.TreeSearch(false,500);

	foreach(Connectivity.Node node in solutionMan){
	  System.Console.SetCursorPosition(0,0);
  	  System.Console.WriteLine(node.State);
	  steps++;
	  //Here is included one extra step, which is the initial one.
  	  System.Threading.Thread.Sleep(100);
	}

	System.Console.SetCursorPosition(0,0);
	System.Console.WriteLine(nextState);
	System.Threading.Thread.Sleep(100);

      }

      System.Console.WriteLine("Number of steps: "+steps);
      
      System.Threading.Thread.Sleep(2000);
    }

    public void ExportSolution(Searching.Node[] solution, string pathToSolutionFile){
      
      string path = "";
      string leiPath = "";
      //We assume the man is looking UP by default.
      Connectivity.Movements lastMovement = Connectivity.Movements.UP;
      Connectivity.Movements currentMovement = Connectivity.Movements.UP;

      for(int i=0; i<solution.Length-1;i++){
	Sokoban.State currentState = (Sokoban.State)solution[i].State;
	//Sokoban.State nextState = (Sokoban.State)solution[i+1].State;
	Sokoban.Action nextAction = (Sokoban.Action)solution[i+1].Action;
	char[,] stateMap = (char[,])Sokoban.State.map.Clone();
	
	foreach(Math.Point2D tempJewel in currentState.Jewels){
	  stateMap[tempJewel.X,tempJewel.Y] = 'J';
	}

	Math.Point2D man = (Math.Point2D)currentState.Man.Clone();
	Math.Point2D goal = new Math.Point2D(-1,-1);

	switch(nextAction.Movement){
	  case Connectivity.Movements.UP:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X+1;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y;
	    break;
	  case Connectivity.Movements.LEFT:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y+1;
	    break;
	  case Connectivity.Movements.DOWN:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X-1;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y;
	    break;
	  case Connectivity.Movements.RIGHT:
	    goal.X = currentState.Jewels[nextAction.JewelIndex].X;
	    goal.Y = currentState.Jewels[nextAction.JewelIndex].Y-1;
	    break;
	}

	Connectivity.State initialState = new Connectivity.State(man,goal,stateMap);
	Connectivity.Node connectivityRoot = new Connectivity.Node(initialState);
	Searching.Solver solver = new Searching.Solver(connectivityRoot,Searching.Strategies.A_STAR);
	Searching.Node[] solutionMan = solver.TreeSearch(false,0);

	for(int j=1;j<solutionMan.Length;j++){
	  Connectivity.Node node = (Connectivity.Node)solutionMan[j];
	  currentMovement = ((Connectivity.Action)node.Action).Movement;
	  switch(currentMovement){
	    case Connectivity.Movements.UP:
	      leiPath += "u";
	      break;
	    case Connectivity.Movements.LEFT:
	      leiPath += "l";
	      break;
	    case Connectivity.Movements.DOWN:
	      leiPath += "d";
	      break;
	    case Connectivity.Movements.RIGHT:
	      leiPath += "r";
	      break;
	  }

	  //The order of the movements is UP,LEFT,DOWN,RIGHT (See Connectivity.Constants)

	  if(lastMovement == currentMovement){
	    path += "perform("+1+")\n";
	  }//One step forward...
	  else if(
	      (lastMovement == Connectivity.Movements.UP && currentMovement == Connectivity.Movements.DOWN) ||
	      (lastMovement == Connectivity.Movements.LEFT && currentMovement == Connectivity.Movements.RIGHT) ||
	      (lastMovement == Connectivity.Movements.DOWN && currentMovement == Connectivity.Movements.UP) ||
	      (lastMovement == Connectivity.Movements.RIGHT && currentMovement == Connectivity.Movements.LEFT)
	      )
	  {//Turn 180 degrees and one step forward...
	    path += "perform("+2+")\n";
	    path += "perform("+2+")\n";//I turn around my left...
	    path += "perform("+1+")\n";
	  }
	  else if(
	      (lastMovement == Connectivity.Movements.UP && currentMovement == Connectivity.Movements.LEFT) ||
	      (lastMovement == Connectivity.Movements.LEFT && currentMovement == Connectivity.Movements.DOWN) ||
	      (lastMovement == Connectivity.Movements.DOWN && currentMovement == Connectivity.Movements.RIGHT) ||
	      (lastMovement == Connectivity.Movements.RIGHT && currentMovement == Connectivity.Movements.UP)
	      )
	  {//Turn to the left and one step forward...
	    path += "perform("+2+")\n";
	    path += "perform("+1+")\n";
	  }
	  else if(
	      (lastMovement == Connectivity.Movements.UP && currentMovement == Connectivity.Movements.RIGHT) ||
	      (lastMovement == Connectivity.Movements.LEFT && currentMovement == Connectivity.Movements.UP) ||
	      (lastMovement == Connectivity.Movements.DOWN && currentMovement == Connectivity.Movements.LEFT) ||
	      (lastMovement == Connectivity.Movements.RIGHT && currentMovement == Connectivity.Movements.DOWN)
	      )
	  {//Turn to the right and one step forward...
	    path += "perform("+3+")\n";
	    path += "perform("+1+")\n";
	  }
  
	  lastMovement = currentMovement;

	}

	//Moving jewels...
	currentMovement = nextAction.Movement;

	switch(currentMovement){
	  case Connectivity.Movements.UP:
	    leiPath += "u";
	    break;
	  case Connectivity.Movements.LEFT:
	    leiPath += "l";
	    break;
	  case Connectivity.Movements.DOWN:
	    leiPath += "d";
	    break;
	  case Connectivity.Movements.RIGHT:
	    leiPath += "r";
	    break;
	}

	//The order of the movements is UP,LEFT,DOWN,RIGHT (See Connectivity.Constants)
	
	if(lastMovement == currentMovement){
	  path += "perform("+1+")\n";
	}
	else if(
	    (lastMovement == Connectivity.Movements.UP && currentMovement == Connectivity.Movements.DOWN) ||
	    (lastMovement == Connectivity.Movements.LEFT && currentMovement == Connectivity.Movements.RIGHT) ||
	    (lastMovement == Connectivity.Movements.DOWN && currentMovement == Connectivity.Movements.UP) ||
	    (lastMovement == Connectivity.Movements.RIGHT && currentMovement == Connectivity.Movements.LEFT)
	    )
	{//Turn 180 degrees and one step forward...
	  path += "perform("+2+")\n";
	  path += "perform("+2+")\n";//I turn around my left...
	  path += "perform("+1+")\n";
	}
	else if(
	    (lastMovement == Connectivity.Movements.UP && currentMovement == Connectivity.Movements.LEFT) ||
	    (lastMovement == Connectivity.Movements.LEFT && currentMovement == Connectivity.Movements.DOWN) ||
	    (lastMovement == Connectivity.Movements.DOWN && currentMovement == Connectivity.Movements.RIGHT) ||
	    (lastMovement == Connectivity.Movements.RIGHT && currentMovement == Connectivity.Movements.UP)
	    )
	{//Turn to the left and one step forward...
	  path += "perform("+2+")\n";
	  path += "perform("+1+")\n";
	}
	else if(
	    (lastMovement == Connectivity.Movements.UP && currentMovement == Connectivity.Movements.RIGHT) ||
	    (lastMovement == Connectivity.Movements.LEFT && currentMovement == Connectivity.Movements.UP) ||
	    (lastMovement == Connectivity.Movements.DOWN && currentMovement == Connectivity.Movements.LEFT) ||
	    (lastMovement == Connectivity.Movements.RIGHT && currentMovement == Connectivity.Movements.DOWN)
	    )
	{//Turn to the right and one step forward...
	  path += "perform("+3+")\n";
	  path += "perform("+1+")\n";
	}

	lastMovement = currentMovement;

	//Everytime I move a can I should push it to place it at the right position, with the exeption
	//of moving the same can in straight line.
	if(i<solution.Length-2){
  	  currentMovement = ((Sokoban.Action)solution[i+2].Action).Movement;
	  int jewelIndex = ((Sokoban.Action)solution[i+2].Action).JewelIndex;
	  if(!((jewelIndex == nextAction.JewelIndex) && (currentMovement == lastMovement))){
	    path += "perform("+5+")\n";
  	  }//I push the can if needed...
	}

      }
      
      //Last adjustment...
      path += "perform("+5+")\n";

      System.IO.FileStream leiFile = new System.IO.FileStream("Lei"+pathToSolutionFile, System.IO.FileMode.Create, System.IO.FileAccess.Write);
      System.IO.StreamWriter leiWriter = new System.IO.StreamWriter(leiFile);
      leiWriter.Write(leiPath);
      leiWriter.Close();
      leiFile.Close();
      System.IO.FileStream file = new System.IO.FileStream(pathToSolutionFile, System.IO.FileMode.Create, System.IO.FileAccess.Write);
      System.IO.StreamWriter writer = new System.IO.StreamWriter(file);
      writer.Write(path);
      writer.Close();
      file.Close();
    }

  }

}
