namespace Sokoban{

  public class State:Searching.State{

    ///This member stores the position of the walls, empties, and goals (static stuff).
    static public char[,] map;
    static public Math.Point2D[] goals;

    private Math.Point2D[] jewels;
    private Math.Point2D man;

    public Math.Point2D[] Jewels{get{return jewels;}}
    public Math.Point2D Man{get{return man;}}

    ///This constructor is used to generate the initial state.
    public State(string pathToMap){
      //Read the map file...
      System.IO.StreamReader reader = System.IO.File.OpenText(pathToMap);
      char[] characters = new char[2];
      characters[0] = ' ';//I detect blank spaces...
      characters[1] = (char)10;//I detect newlines...
      string[] values = reader.ReadLine().Split(characters);

      jewels = new Math.Point2D[System.UInt16.Parse(values[2])];
      goals = new Math.Point2D[System.UInt16.Parse(values[2])];
      int rows = System.UInt16.Parse(values[1]);
      int columns = System.UInt16.Parse(values[0]);
      map = new char[rows,columns];

      //At first we make all the cell empty spaces...
      for(int i=0; i < rows; i++){
	for(int j=0; j < columns; j++){
	  map[i,j] = ' ';
	}
      }

      //Here I must extract the specific placement of the objects...
      int cell = 0;
      int rowIndex = 0;
      int columnIndex = 0;
      int jewelsCounter = 0;
      int goalsCounter = 0;
      do{
	cell = reader.Read();
	switch(cell){
	  case '\n':
	    rowIndex++;
	    columnIndex = -1;
	    if(rowIndex == rows){
	      cell = -1;
	    }
	    break;
	  case 'X':
	  case '.':
	  case ' ':
	    map[rowIndex,columnIndex] = (char)cell;
	    break;
	  case 'J':
	    map[rowIndex,columnIndex] = '.';
	    jewels[jewelsCounter++] = new Math.Point2D(rowIndex,columnIndex);
	    break;
	  case 'M':
	    map[rowIndex,columnIndex] = '.';
	    man = new Math.Point2D(rowIndex,columnIndex);
	    break;
	  case 'G':
	    map[rowIndex,columnIndex] = '.';
	    goals[goalsCounter++] = new Math.Point2D(rowIndex,columnIndex);
	    break;
	}
	columnIndex++;
	//System.Console.WriteLine((char)cell);
      }while(cell != -1);
      reader.Close();
    }

    public State(Math.Point2D[] jewels, Math.Point2D man){
      this.jewels = jewels;
      this.man = man;
    }

    override public bool IsGoal(){
      bool matches;
      foreach(Math.Point2D jewel in jewels){
	matches = false;
	foreach(Math.Point2D goal in goals){
	  if(jewel.IsEqual(goal)){
	    matches = true;
	    break;
	  }
	}
	if(!matches){
	  return false;
	}
      }
      return true;
      //return IsEqual(goals);
    }

    ///This method determine which states are equal. The combination of the jewels in the same position,
    ///and also the man in the same position (it is not the same to have a group of jewels in one position
    ///if the man is in the other side of the wall.
    override public bool IsEqual(Searching.State other){
      if(!(other is Sokoban.State)){
	return false;
      }
      //I just need to compare the jewels...
      bool matches;
      foreach(Math.Point2D otherJewel in ((State)other).jewels){
	matches = false;
	foreach(Math.Point2D thisJewel in jewels){
	  if(otherJewel.IsEqual(thisJewel)){
	    matches = true;
	    break;
	  }
	}
	if(!matches){
	  return false;
	}
      }
      if(!(((State)other).man.IsEqual(man))){return false;}
      return true;
      
    }

    ///This function is going to be based in the sum of the Euclidean distance between every
    ///jewel to the nearest empty goal. It should give a very big value (infinite) if we are
    ///in an state where we cannot move the jewel (but not if the jewel is at the goal).
    ///This function return -1 when the goal is not achievable (good heuristic).
    override public int HeuristicFunction(){

      foreach(Math.Point2D jewel in jewels){
	if(IsStucked(jewel)){
	  bool matches = false;
  	  foreach(Math.Point2D goal in goals){
  	    if(jewel.IsEqual(goal)){
  	      matches = true;
  	      break;
  	    }
  	  }
  	  if(!matches){
  	    return System.Int32.MaxValue;
  	  }
	}
      }

      //There should be the same amount of jewels and goals, for everything to work.
      double[] distances = new double[jewels.Length];
      bool[] takenJewels = new bool[jewels.Length];//The default value of the elements is "false".

      //The heuristic is like this: I take the distance from the first goal to every 
      //jewel, and I store the smallest distance and nearest jewel. For the second
      //goal I do the same but with all the rest of the jewels. And so on...
      foreach(Math.Point2D goal in goals){
	int minIndex = -1;
	for(int i=0;i<jewels.Length;i++){
	  if(!takenJewels[i]){
  	    distances[i] = goal.EuclideanDistance(jewels[i]);
	    if(minIndex == -1){
	      minIndex = i;
	    }
  	    if(distances[i]<distances[minIndex]){
  	      minIndex = i;
  	    }
	  }
	}
	takenJewels[minIndex] = true;
      }

      //And finally I sum the distances...
      double sum = 0;
      foreach(double distance in distances){
	sum += distance;
      }
      return (int)(sum);//This number (100) seems to be related with the depth of the solution.

    }

    ///This function return true if the jewel is not movable. The conditions to declare a jewel not movable are:
    ///1. The jewel must have at least 2 wall cells around, and continuous.
    public bool IsStucked(Math.Point2D jewel){
      char[] neighborhood = new char[4];
      //Order very important!
      neighborhood[0] = map[jewel.X-1,jewel.Y];//UP
      neighborhood[1] = map[jewel.X,jewel.Y-1];//LEFT
      neighborhood[2] = map[jewel.X+1,jewel.Y];//DOWN
      neighborhood[3] = map[jewel.X,jewel.Y+1];//RIGHT
      
      //Here we detect if there are jewels sourounding. But the heuristic must be improved,
      //because maybe I can move one of the jewels in the neighborhood, and not to be stucked
      //anymore (I should not exclude this state). So, I must check if the other jewel
      //is also stucked.
      foreach(Math.Point2D otherJewel in jewels){
	if(otherJewel.X == jewel.X-1 && otherJewel.Y == jewel.Y){//UP
	  if(map[otherJewel.X,otherJewel.Y-1] == 'X'/*LEFT*/ || map[otherJewel.X,otherJewel.Y+1] == 'X'/*RIGHT*/){
  	    neighborhood[0] = 'X';
	  }
	}
	else if(otherJewel.X == jewel.X && otherJewel.Y == jewel.Y-1){//LEFT
	  if(map[otherJewel.X-1,otherJewel.Y] == 'X'/*UP*/ || map[otherJewel.X+1,otherJewel.Y] == 'X'/*DOWN*/){
	    neighborhood[1] = 'X';
	  }
	}
	else if(otherJewel.X == jewel.X+1 && otherJewel.Y == jewel.Y){//DOWN
	  if(map[otherJewel.X,otherJewel.Y-1] == 'X'/*LEFT*/ || map[otherJewel.X,otherJewel.Y+1] == 'X'/*RIGHT*/){
  	    neighborhood[2] = 'X';
	  }
	}
	else if(otherJewel.X == jewel.X && otherJewel.Y == jewel.Y+1){//RIGHT
	  if(map[otherJewel.X-1,otherJewel.Y] == 'X'/*UP*/ || map[otherJewel.X+1,otherJewel.Y] == 'X'/*DOWN*/){
	    neighborhood[3] = 'X';
	  }
	}
      }

      //Here I dectect the segments that are continues and not points.
      int counter = 0;
      foreach(char neighbor in neighborhood){
	//System.Console.WriteLine(neighbor);
	if(neighbor == '.'){
	  counter = 0;
	}
	if(neighbor == 'X'){
	  counter++;
	}
	if(counter >= 2){
	  return true;
	}
      }

      return false;
    }

    public bool IsJewel(Math.Point2D position){
      foreach(Math.Point2D jewel in jewels){
	if(jewel.IsEqual(position)){
	  return true;
	}
      }
      return false;
    }

    override public string ToString(){

      //char[,] stateMap = (char[,])map.Clone();
      char[,] stateMap = new char[Sokoban.State.map.GetLength(0),Sokoban.State.map.GetLength(1)];
      for(int j=0;j<stateMap.GetLength(0);j++){
	for(int k=0;k<stateMap.GetLength(1);k++){
	  stateMap[j,k] = Sokoban.State.map[j,k];
	}
      }
      
      foreach(Math.Point2D goal in goals){
	stateMap[goal.X,goal.Y] = 'G';
      }

      foreach(Math.Point2D jewel in jewels){
	stateMap[jewel.X,jewel.Y] = 'J';
      }

      stateMap[man.X,man.Y] = 'M';

      string stateDescription = "\n";

      for(int i=0; i<stateMap.GetLength(0);i++){
	for(int j=0; j<stateMap.GetLength(1);j++){
	  stateDescription += stateMap[i,j];
	}
	stateDescription += "\n";
      }

      stateDescription += "Heuristic Function: "+HeuristicFunction()+"\n";

      return stateDescription;
    }

  }

}
