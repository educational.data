namespace Sokoban{

  public class Node:Searching.Node{

    public Node(State initialState):base(initialState){}

    public Node(State state, Node parent, Action action, int pathCost, int depth)
      :base(state,parent,action,pathCost,depth){}

    override public Searching.Node[] Expand(){
      //I can return any kinf of derived node...
      System.Collections.Generic.List<Searching.Node> expandedNodes = new System.Collections.Generic.List<Searching.Node>();
      State currentState = (State) this.state;
      Math.Point2D positionBefore = new Math.Point2D(-1,-1);
      Math.Point2D positionAfter = new Math.Point2D(-1,-1);
      char cellBefore = ' ';
      char cellAfter = ' ';
      int length = currentState.Jewels.Length;
      Math.Point2D jewel;
      for(int i=0; i<length; i++){
	jewel = currentState.Jewels[i];
	foreach(int movement in System.Enum.GetValues(typeof(Connectivity.Movements))){
	  switch(movement){
	    //ATTENTION!
	    //X positive down
	    //Y positive left
	    case (int)Connectivity.Movements.UP:
	      positionBefore.X = jewel.X+1;
	      positionBefore.Y = jewel.Y;
	      positionAfter.X = jewel.X-1;
	      positionAfter.Y = jewel.Y;
	      break;
	    case (int)Connectivity.Movements.LEFT:
	      positionBefore.X = jewel.X;
	      positionBefore.Y = jewel.Y+1;
	      positionAfter.X = jewel.X;
	      positionAfter.Y = jewel.Y-1;
	      break;
	    case (int)Connectivity.Movements.DOWN:
	      positionBefore.X = jewel.X-1;
	      positionBefore.Y = jewel.Y;
	      positionAfter.X = jewel.X+1;
	      positionAfter.Y = jewel.Y;
	      break;
	    case (int)Connectivity.Movements.RIGHT:
	      positionBefore.X = jewel.X;
	      positionBefore.Y = jewel.Y-1;
	      positionAfter.X = jewel.X;
	      positionAfter.Y = jewel.Y+1;
	      break;
	  }
	  cellBefore = Sokoban.State.map[positionBefore.X,positionBefore.Y];
	  cellAfter = Sokoban.State.map[positionAfter.X,positionAfter.Y];
	  //checking empties...
	  if(cellBefore == '.' && cellAfter == '.'
	      && !currentState.IsJewel(positionBefore) && !currentState.IsJewel(positionAfter)){
	    
	    //checking connectivity and stepCost... TO BE DONE.
	    //char[,] stateMap = (char[,])Sokoban.State.map.Clone();
	    char[,] stateMap = new char[Sokoban.State.map.GetLength(0),Sokoban.State.map.GetLength(1)];
	    for(int j=0;j<stateMap.GetLength(0);j++){
  	      for(int k=0;k<stateMap.GetLength(1);k++){
		stateMap[j,k] = Sokoban.State.map[j,k];
  	      }
	    }

	    foreach(Math.Point2D tempJewel in currentState.Jewels){
	      stateMap[tempJewel.X,tempJewel.Y] = 'J';
	    }
	    Math.Point2D man = (Math.Point2D)currentState.Man.Clone();
	    Math.Point2D goal = new Math.Point2D(positionBefore.X,positionBefore.Y);
	    Connectivity.State initialState = new Connectivity.State(man,goal,stateMap);
	    Connectivity.Node connectivityRoot = new Connectivity.Node(initialState);
	    Searching.Solver solver = new Searching.Solver(connectivityRoot,Searching.Strategies.A_STAR);
	    Searching.Node[] solution = solver.TreeSearch(false,100);
	    if(solution != null){
	      int stepCost = solution[solution.Length-1].PathCost;
  	    
  	      //OLD CODE...
	      //We don't store the solution to save memory. We are going to quickly calculate
	      //it again, at the end, when we find create the final solution to Sokoban.
	      Action newAction = new Action((Connectivity.Movements)movement,i);
  	      Math.Point2D[] newJewels = new Math.Point2D[length];
  	      for(int j=0;j<length;j++){
  		newJewels[j] = (Math.Point2D)currentState.Jewels[j].Clone();
  	      }
  	      newJewels[i].X = positionAfter.X;
  	      newJewels[i].Y = positionAfter.Y;
  	      Math.Point2D newMan = new Math.Point2D(jewel.X,jewel.Y);
  	      State newState = new State(newJewels,newMan);
	      //System.Console.WriteLine(newState);
  	      //here, the new pathCost should be pathCost + manhatan distance.
	      expandedNodes.Add(new Node(newState,this,newAction,pathCost+stepCost+1,depth+1));
	      //expandedNodes.Add(new Node(newState,this,newAction,pathCost+1,depth+1));
  	      //OLD CODE...

	    }//end if solution != null
	    /*else{
	      System.Console.WriteLine("NO CONNECTIVITY");
	      System.Threading.Thread.Sleep(1000);
	    }*///end else solution != null
	  }
	}
      }
      if(expandedNodes.Count == 0){
	return null;
      }
      return expandedNodes.ToArray();
    }

    override public string ToString(){
      string node = this.State.ToString();
      if(action != null){
	node += "Jewel Index: "+((Sokoban.Action)action).JewelIndex+" Movement: "+((Sokoban.Action)action).Movement+"\n";
      }
      return node;
    }

  }

}
