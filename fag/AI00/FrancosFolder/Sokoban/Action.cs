namespace Sokoban{

  public class Action:Searching.Action{
    
    private Connectivity.Movements movement;
    private int jewelIndex;

    public Connectivity.Movements Movement{get{return movement;}}
    public int JewelIndex{get{return jewelIndex;}}

    public Action(Connectivity.Movements movement,int jewelIndex){
      this.movement = movement;
      this.jewelIndex = jewelIndex;
    }

    //Properties...

  }
}
