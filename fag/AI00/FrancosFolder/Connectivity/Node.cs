namespace Connectivity{

  public class Node:Searching.Node{

    public Node(State initialState):base(initialState){}

    public Node(State state, Node parent, Action action, int pathCost, int depth)
      :base(state,parent,action,pathCost,depth){}

    override public Searching.Node[] Expand(){
      System.Collections.Generic.List<Searching.Node> expandedNodes = new System.Collections.Generic.List<Searching.Node>();
      State currentState = (State) this.state;
      Math.Point2D man = currentState.Man;
      Math.Point2D positionAfter = new Math.Point2D(-1,-1);
      char cellAfter = ' ';
      foreach(int movement in System.Enum.GetValues(typeof(Connectivity.Movements))){
	switch(movement){
	    case (int)Movements.UP:
	      positionAfter.X = man.X-1;
	      positionAfter.Y = man.Y;
	      break;
	    case (int)Movements.LEFT:
	      positionAfter.X = man.X;
	      positionAfter.Y = man.Y-1;
	      break;
	    case (int)Movements.DOWN:
	      positionAfter.X = man.X+1;
	      positionAfter.Y = man.Y;
	      break;
	    case (int)Movements.RIGHT:
	      positionAfter.X = man.X;
	      positionAfter.Y = man.Y+1;
	      break;
	}
	cellAfter = Connectivity.State.map[positionAfter.X,positionAfter.Y];
	//Check empty space...
	if(cellAfter == '.'){
	  Action newAction = new Action((Movements)movement);
	  Math.Point2D newMan = new Math.Point2D(positionAfter.X,positionAfter.Y);
	  State newState = new State(newMan);
	  //Here the past cost added is just 1, one step.
	  expandedNodes.Add(new Node(newState,this,newAction,pathCost+1,depth+1));
	}
      }
      if(expandedNodes.Count == 0){
	return null;
      }
      return expandedNodes.ToArray();
      
    }

  }

}
