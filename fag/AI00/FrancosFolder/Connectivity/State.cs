namespace Connectivity{

  public class State:Searching.State{

    ///This member stores the position of the walls, empties, and goals (static stuff).
    static public char[,] map;
    static public Math.Point2D goal;

    private Math.Point2D man;

    public Math.Point2D Man{get{return man;}}

    ///This constructor is used to generate the initial state. The map should be a
    ///map file in Sokoban format, with jewels included, but man and goals.
    public State(Math.Point2D man, Math.Point2D goal, char[,] map){
      if(map[man.X,man.Y] == 'X' || map[man.X,man.Y] == 'J' || map[goal.X,goal.Y] == 'X' || map[goal.X,goal.Y] == 'J'){
	System.Console.WriteLine("Man and goal must be placed at empty spaces.");
	return;
      }
      Connectivity.State.map = map;
      Connectivity.State.goal = goal;
      this.man = man;
    }

    public State(Math.Point2D man){
      this.man = man;
    }

    override public bool IsGoal(){
      return man.IsEqual(goal);
    }

    ///This method determine which states are equal. The combination of the jewels in the same position.
    override public bool IsEqual(Searching.State other){
      if(!(other is Connectivity.State)){
	return false;
      }
      return man.IsEqual(((State)other).man);
      
    }

    ///I must return big values if I dont have connectivity?
    override public int HeuristicFunction(){
      return (int)man.EuclideanDistance(goal);
    }

    override public string ToString(){

      //char[,] stateMap = (char[,])map.Clone();
      char[,] stateMap = new char[Connectivity.State.map.GetLength(0),Connectivity.State.map.GetLength(1)];
      for(int j=0;j<stateMap.GetLength(0);j++){
	for(int k=0;k<stateMap.GetLength(1);k++){
	  stateMap[j,k] = Connectivity.State.map[j,k];
	}
      }
      
      stateMap[man.X,man.Y] = 'M';
      stateMap[goal.X,goal.Y] = '*';

      string stateDescription = "\n";

      for(int i=0; i<stateMap.GetLength(0);i++){
	for(int j=0; j<stateMap.GetLength(1);j++){
	  stateDescription += stateMap[i,j];
	}
	stateDescription += "\n";
      }

      stateDescription += "Heuristic Function: "+HeuristicFunction();

      return stateDescription;
    }

  }

}
