namespace Math{

  public class Point2D:System.ICloneable{
    protected int x;
    protected int y;

    public int X{get{return x;}set{x = value;}}
    public int Y{get{return y;}set{y = value;}}

    public Point2D(int x,int y){
      this.x = x;
      this.y = y;
    }

    public bool IsEqual(Point2D point2D){
      if(point2D.x == x && point2D.y == y){
	return true;
      }
      return false;
    }

    public System.Object Clone(){
      Math.Point2D clon = new Math.Point2D(x,y);
      //return MemberwiseClone();
      return clon;
    }

    public double EuclideanDistance(Point2D other){
      return System.Math.Sqrt(System.Math.Pow((this.X-other.X),2)+System.Math.Pow((this.Y-other.Y),2));
    }

  }
  
}
