\section{C\# planner\label{Csharp-planner}}

The .NET platform used to build C\# planner was Mono\footnote{http://www.go-mono.org};
which runs in Linux, Windows, and Macintosh operating systems. In this particular
case, the implementation and the performance test were made in Linux.
The compiler, libraries, and a different set of tools can be downloaded
free of cost from the project main page. Considering that no specific
Linux libraries were used at the moment of writting the code, the
program should be able to compile at any .NET implementation above
the version 2.0 (the System.Collections.Generic namespace is used).

\subsection{Specific modelling}

Some modifications were made in the problem model, in order to simplify some constraints
that could increase too much the running time of the planner. In this implementation, we did
not take in account the optimizations related with the turning steps of the agent.
Nevertheless, this is an agent tied optimization, that could be only visualized when
using specific kind of agents: the ones that decrease considerably the performance when 
turning so many times. All of this, just in the case the planner is used in conjunction
of an agent at all.

\subsubsection{Jewel problem\label{Csharp-jewel}}

The problem model of the C\# planner was slightly modified and, as proposed before, the man
problem is now integrated with the jewels primary task. The last led to the next description of
the state space problem:

\begin{description}
  \item[Initial state] The initial position of the jewels and the man, concerning the specific map
    being faced.
  \item[Successor function] Expands nodes considering all the possible movements of the jewels,
    and satisfying the environment constraints. In this case, the man problem is treated as one constraint
    more, by determining if there is connectivity or not with the empty space behind the jewel.
  \item[Goal test] The games succeeds when all the jewels are placed at the different goal positions,
    without concerning the order of the placement. 
  \item[Path cost] Each step of the man adds one to the step cost of moving one jewel, plus an extra
    one for the man to push the jewel.
\end{description}

Further, in the C\# planner we tested alternative heuristics. By instance, instead of using
Manhattan distance between jewels and goals, a modified version of Euclidian distance
was used (see section \ref{Csharp-algorithms}).

\subsubsection{Man problem}

As mentioned at the beginning of this section, we decided to exclude the optimization of the
rotations of the agent, out of the planner criteria.

Also, the fusion performed between the two problems demanded an extra member in the state
representation of the jewels problem, which is the man position (see Section \ref{Csharp-jewel}).
At this point, we need to know where the man is
at the beginnig of each jewel problem state, if we want to check the connectivity between
the man and the empty space behind the next jewel to be potentially moved.

\begin{description}
  \item[Initial state] An initial position from where we have to move
    the man.
  \item[Successor function] Expands nodes representing all the different
    free cells around the man in the present node.
  \item[Goal test] The man at the destiny position - behind some jewel.
  \item[Path cost] Each step of the man adds one to the path cost.
\end{description}

\subsection{Layout of the program}

The C\# planner is highly influenced by the structures mentioned in
the AIMA book. Basically, the planner implements abstracts methods
to deal with nodes, states, and actions. Therefore, at the moment
of facing a new problem, the user is forced to implement its own Node,
State, and Action classes, all of them inherited from the mother classes pre-built
in the planner.

\begin{figure}[!hbp]
  \centering
  \includegraphics[scale=0.43]{FrancosPictures/CsharpPlannerClassDiagram}
  \caption{C\# planner class diagram.\label{fig:Csharp-planner-class-diagram}}
\end{figure}

By using abstract classes we strongly force the user to implement
the minimal set of neccesary methods, to run a complete graph search
(behaviour by default in the planner).

As every problem must be specified by the creation of three classes; to solve the
Sokoban problem we needed six classes: three for the jewels problem and three
for the man problem. These classes can be identified in the Figure \ref{fig:Csharp-planner-class-diagram}
with the Sokoban and Connectivity namespaces; for the jewels and the man problems,
respectivelly.

Also, from the Figure \ref{fig:Csharp-planner-class-diagram},
we can see the planner assist the implementation of new searching
algorithms, by using an abstract priority queue.
To sum up, every new algorithm implemented is forced to define a new
criterion for giving priorities to the new nodes added to the fringe of
the graph search.


\subsection{Algorithms\label{Csharp-algorithms}}

First at all, the static information of the Sokokan problem are: the walls, the empty
cells, and the goals. This entities were stored in static members of the class Sokoban.State,
and they are used during the whole execution of the searches, when: checking environment
constraints, goal tests, and connectivity tests.

The successor function are implemented at every problem in the Node.Expand method. It is
responsability of the programmer trying to solve a new problem, to expand his nodes
in the right way. For the jewels problem, the nodes were expanded by applying all the possible
movement to the jewels with empty cells behind and forward, and also
with connectivity between the man and the empty cell behind.

The heuristic function of the jewels problem was implemented with a modified version of the
Euclidean distance. In this case, we find the Euclidean distance between the nearest jewel
to the group of goals and store: jewel, goal and distance; then we continue with
second nearest jewel to the group of remaining goals and store the new triplet;
and so on.

At the end, we sum up all the Euclidean distances of the jewels to the respective goals, and
obtain a value for the heuristic function of the problem. The jewel's heuristic function
is zero, just and just when all the jewels are at the goal position, and it never
overestimates the cost to reach the goals.

For this planner, the man problem was implemented with a different structure than the jewels.
For the man, the goal is just one position in the map. Therefore, the connectivity
problem was faced as initial position, goal position, and one environment map where the
jewels are obstacles (just like the walls). If the connectivity problem returns a null
solution, it simply means the man is not able to achieve the position located behind the
jewel being tested to be moved.

The heuristic function of the man problem is simply the Euclidean distance to the goal
position. This heuristic was chosen because of facility at the implementation, but the
Manhattan distance could give a more precise measurement of the distance from the man
to the goal (and it still do not overestimate the cost of reach the goal).

The final solution of the jewel problem is a sequence of jewels' movements, until they are placed
at the goals position. On the other hand, the final solution of the man problem is a sequence
of man's movements from the origin position until the destiny (located behind a jewel). Therefore,
there are one complete solution of the man problem with every node expanded by the jewel problem.

It could be tentative to store the solution of the man problem with every node of the
upper layer jewel problem; nevertheless, it demands much more memory than recalculating
the man's path once the final solution is obtained. The solutions for the man problem
are obtained rather quickly. For this reason, the delayed paths recalculation approach
was implemented in this planner.

\subsection{End result}

The facility of implementation
of new searching algorithms allowed us to try four approaches: Deep First,
Iterative Deep First, Greedy Best First, and A*.
In the case of Iterative Deep First we did not even implement the complete searching
algorithm, but we inherit the characteristics of the Deep First algorithm previously
implemented.

Nevertheless, as we are looking for the optimal solutions, we tried the planner
with the A* searching algorithm for both of the problems: the jewels and the man.
In this way, we were going to obtain the less amount of steps for the man to perform
the solution.

The time required to find the optimal solution for the four jewels problem delivered
for the demostration, was 01h25m43s on a 2.8GHz Intel Petium 4 computer.

At the time of the demostration we did not have a complete optimal solution for the
four jewels problem, by using this planner. The last because of the exclusion, when debugging,
of code segments that had the responsability of filtering states not worth to be expanded
(by instance, the states when at least one jewel is stucked against the walls). The
consequent of this was that the time to find the optimal solution was 17 hours!; which
left us without solution at hands, on time.


