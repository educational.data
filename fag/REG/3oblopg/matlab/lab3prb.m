clear;clc;

J=0.1;
B=0.05;
Ka=0.1;
Km=0.1;
R=1;
L=1;
Ts=0.1;

%Tilstandsbeskrivelse def:
Ac=[-R/L -Ka/L;
    Km/J -B/J];
Bc=[1/L;0]; %uden belastning
Bc2=[1/L,0;0,-1/J]; %med belastning
Cc=[0 1];
Dc=0;

% Continues to discrete uden belastning
G_p = ss(Ac,Bc,Cc,Dc)
G_z = c2d(G_p,Ts,'zoh')
[A B C D]=ssdata(G_z)

% Continues to discrete med belastning
G_p2 = ss(Ac,Bc2,Cc,Dc)
G_z2 = c2d(G_p2,Ts,'zoh')
[At Bt Ct Dt]=ssdata(G_z2)

% Bestemmelse af K matricen.
% for systemet
zeta_c = 0.6;
tau_c = 0.4244;
r_c = exp(-Ts/tau_c);
theta_c = (-log(r_c)*sqrt(1 -zeta_c^2))/zeta_c;
Z_c = [r_c*exp(j*theta_c) r_c*exp(-j*theta_c)];
%Z_c=[r_c r_c];
K_e = place(A,B,Z_c)

%Bestemmelse af G matrix for estimatoren
tau_e = tau_c/4; %ca. 3 gange hurtigere end ukompenceret motor
r_e = exp(-Ts/tau_e);
Z_e = [r_e r_e]
Gc = (acker(A',(C*A)',Z_e))'

%Integrator!!!!
Z_i = [0.8]
Z_p = [Z_c Z_i]
Ay = [A [0;0]; -C 1]
By = [B;0]
Ky = place(Ay,By,Z_p)

K = [Ky(1) Ky(2)]
Ki = [-Ky(3)]
N = Ki/(1-Z_i)

A_reg_ui = [A-A*Gc*C-B*K_e+B*K_e*Gc*C]
B_reg_ui = [B A*Gc-B*K_e*Gc]
C_reg_ui = [K_e*Gc*C-K_e]
D_reg_ui = [1 -K_e*Gc]

format long
A_reg = [A-A*Gc*C-B*K+B*K*Gc*C]
B_reg = [B A*Gc-B*K*Gc]
C_reg = [K*Gc*C-K]
D_reg = [1 -K*Gc]
format short
mex LabOpg3.c