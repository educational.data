#######################
## Makefile by tobibobi
#######################

bdir=build

SHELL=sh

-include makefile.extra

fig_files:=${wildcard fig/*.fig}
img_files:=${wildcard img/*.*}
pdf_from_fig_files= $(subst fig/,${bdir}/,$(fig_files:.fig=.pdf))
files:=	${bdir}/doc.sty \
	$(addprefix ${bdir}/,$(wildcard *.tex) $(wildcard *.bib)) \
	${pdf_from_fig_files} \
	$(subst img/,$(bdir)/,$(img_files))

mpdf=doc.pdf
inputfile=${mpdf:.pdf=.tex}
texfiles:=${wildcard *.tex}
allfiles:=$(texfiles) $(fig_files) $(img_files) ${inputfiles} ${EXTRA_DIST_FILES}

define out
 >> $(basename $@).pdftex_t
endef

ifdef V
  ifeq ("$(origin V)", "command line")
    KBUILD_VERBOSE = $(V)
  endif
endif
ifndef KBUILD_VERBOSE
  KBUILD_VERBOSE = 0
endif
ifeq ($(KBUILD_VERBOSE),0)
  define Q
    @
  endef
  define SILENT
    >/dev/null 2>/dev/null
  endef
#  LATEXFLAGS=-interaction=nonstopmode
  LATEXARGS=--interaction=batchmode
else
  define USILENT
    >/dev/null 2>/dev/null
  endef
  LATEXARGS=
endif

ifndef PACKAGE
  PACKAGE=${shell pwd|sed -r "s/.*\///"}
endif
TAR_PACKAGE=${PACKAGE}.tar.bz

define COMMANDS
printNE() { \
	if test "${KBUILD_VERBOSE}" = "0"; then \
		if test "$HOSTTYPE" != "sun4"; then \
			echo ""; \
			echo -e "\e[1A\e[32m * \e[0m\e[s$$1\e[u\e[10C$$2 $$3\e[0m \e[999C\e[7D\e[34m[    ]\e[0m\e[s"; \
		else \
			echo " * $$1   $$2 $$3"; \
		fi; \
	fi; \
};\
print() { \
	if test "${KBUILD_VERBOSE}" = "0" && test "$$HOSTTYPE" != "sun4"; then \
		echo -e " \e[34m$$1"; \
	else \
		echo "$$1";\
	fi; \
};\
printOK() { \
	if test "${KBUILD_VERBOSE}" = "0" && test "$$HOSTTYPE" != "sun4"; then \
		echo -e "\e[u\e[6D\e[34m[ \e[32mOK\e[34m ]\e[0m"; \
	fi; \
};\
printFAIL() { \
	if test "${KBUILD_VERBOSE}" = "0"; then \
		if test "$$HOSTTYPE" != "sun4"; then \
			echo -e "\e[u\e[6D\e[34m[\e[31;1mFAIL\e[34m]\e[0m"; \
		else \
			echo "FAILED!!!"; \
		fi; \
	fi; \
};\
printErrorLine() { \
	if test "$$HOSTTYPE" != "sun4"; then \
		echo -e " \e[31;1m!!\e[0m $$1"; \
	else \
		echo " !! $$1"; \
	fi; \
}
endef

.PHONY: help dist distclean all pdf xpdf clean template

help:
	@echo " Available targets with make"
	@echo ""
	@echo "   pdf:     build pdf file"
	@echo "   xpdf:    build and view files with acrobat reader"
	@echo "   dist:    assemble a package of tex and other dependencies"
	@echo ""

all: pdf dist
pdf: ${bdir}/${mpdf}

dist:${TAR_PACKAGE}

xpdf: ${bdir}/${mpdf} 
	@acroread ${bdir}/${mpdf} || true

clean:
	@${COMMANDS}; printNE "CLEAN"
	${Q}rm -rf ${bdir}
	${Q}rm -f *~
	${Q}rm -f fig/*.bak
	${Q}rm -f ${TAR_PACKAGE}
	@${COMMANDS}; printOK

fig_to_pdf: ${pdf_from_fig_files}

${bdir}/${mpdf} : $(files) makefile.extra
	@${COMMANDS}; printNE "PDFTEX" "$@"
	${Q}cd ${bdir};\
	${COMMANDS}; \
	pdflatex ${LATEXARGS} ${inputfile} ${SILENT}; \
	if test "$$?" != "0"; then \
		printFAIL; \
		printErrorLine "PDFTEX FAILED."; \
		printErrorLine "Try to rerun with \"\e[32mmake V=1\e[0m\"";\
		false; \
	else \
		true bibtex --terse doc ${SILENT};\
		pdflatex ${LATEXARGS} ${inputfile} ${SILENT}; \
		pdflatex ${LATEXARGS} ${inputfile} ${SILENT}; \
		printOK; \
	fi

$(bdir)/%.pdf : fig/%.fig makefile.extra
	@${COMMANDS}; printNE "FIG2PDF" "$<"
	@rm -f $@
	${Q}fig2dev -L pdftex -p 0 $< >$@
	${Q}fig2dev -L pdftex_t -p $(<F:.fig=) $< >$(basename $@).pdftex_t 
	@${COMMANDS}; printOK

$(bdir)/% : % makefile.extra
	@${COMMANDS}; printNE "COPY" $<
	${Q}mkdir -p ${bdir}
	${Q}cp $< $@
	@${COMMANDS}; printOK

${bdir}/% : img/% makefile.extra
	@${COMMANDS}; printNE "IMGCOPY" "$<"
	${Q}mkdir -p ${bdir}
	${Q}cp $< $@
	${Q}rm -f $(basename $@).pdftex_t
	${Q}echo "\\includegraphics{$(basename $(@F))}%" ${out}
	@${COMMANDS}; printOK

${mpdf}:$(bdir)/${mpdf} makefile.extra
	@${COMMANDS}; printNE "COPY" $<
	${Q}cp -f $< $@
	@${COMMANDS}; printOK


cvs_add: makefile.extra
	${Q}if test -d CVS; then \
		echo "!!! No CVS entrys found!!!"; \
		false; \
	fi
	@echo -e " \e[34;1mAdding files\e[0m "
	${Q}mkdir -p img
	${Q}mkdir -p fig
	${Q}cvs -q add ${fig_files} || true
	${Q}cvs -q add ${img_files} || true
	${Q}cvs -q add ${texfiles} || true
	${Q}cvs -q add .template || true
	${Q}cd .template/cvs -q add doc.tex || true
	-${Q}if test "x${EXTRA_DIST_FILES}" != "x"; then \
		cvs -q add ${EXTRA_DIST_FILES} || true; \
	fi;
	${Q}cvs -q add makefile.extra
	${Q}cvs -q add makefile
	${Q}cvs -q add doc.sty
	${Q}cvs -q add references.bib

${TAR_PACKAGE} : ${fig_files} ${img_files} ${texfiles} ${EXTRA_DIST_FILES} makefile.extra references.bib makefile
	@${COMMANDS}; printNE "TAR" $@
	${Q}rm -f ${TAR_PACKAGE}
	${Q}mkdir -p .tar/${PACKAGE}
	${Q}${foreach file, $(dir $(allfiles)),mkdir -p .tar/${PACKAGE}/${file};}
	${Q}${foreach file, $(allfiles),cp -rf $(file) .tar/${PACKAGE}/${file};}
	${Q}cp -rf references.bib .tar/${PACKAGE}
	${Q}cp -rf makefile .tar/${PACKAGE}
	${Q}cp -rf makefile.extra .tar/${PACKAGE}
	${Q}cp -rf doc.sty .tar/${PACKAGE}
	${Q}cd .tar;tar cjf ${TAR_PACKAGE} ${PACKAGE}
	${Q}cd .tar;mv ${TAR_PACKAGE} ../;
	${Q}rm -rf .tar
	@${COMMANDS}; printOK

distcheck : ${TAR_PACKAGE} 
	@${COMMANDS}; printNE "UNTAR" $<
	${Q}mkdir .tar
	${Q}cp -f ${TAR_PACKAGE} .tar/
	${Q}cd .tar;tar xjf ${TAR_PACKAGE}
	@${COMMANDS}; printOK
	${Q}cd .tar/${PACKAGE};${MAKE} all

template:.template/doc.tex
	@${COMMANDS}; printNE "TEMPLATE" "template.tar.bz"
	${Q}mkdir -p .template/img .template/fig .template/.template;
	${Q}cp -f makefile .template
	${Q}cp -f doc.sty .template
	${Q}cp -f references.bib .template
	${Q}cp -f .template/doc.tex .template/.template
	${Q}cd .template/;tar cjf template.tar.bz references.bib makefile doc.tex doc.sty img fig
	${Q}cd .template/;rm -rf references.bib makefile doc.sty img fig .template/.template
	${Q}cp -f .template/template.tar.bz .
	${Q}rm -rf .template/template.tar.bz
	@${COMMANDS}; printOK

.template/doc.tex:
	@${COMMANDS}; printNE "GET" "template.tar.tex" "doc.tex"
	${Q}mkdir .template
	${Q}wget http://www.mip.sdu.dk/~tobibobi/texdocs-template.tar.bz
	${Q}mkdir -p .tmp
	${Q}mv texdocs-template.tar.bz .tmp
	${Q}cd .tmp;tar xjf texdocs-template.tar.bx
	${Q}mv .tmp/.template/doc.tex .template/doc.tex
	${Q}rm -rf .tmp


template_upload:template
	@${COMMANDS}; printNE "UPLOAD" "to" "http://www.mip.sdu.dk/~tobibobi/texdocs-template.tar.bz"
	${Q}scp -q template.tar.bz tobibobi@falster:WWWpublic/texdocs-template.tar.bz
	@${COMMANDS}; printOK

makefile.extra: 
	@${COMMANDS}; printNE "DEFAULTS"
	${Q}echo "# This little file is included in the upper makefile" > $@
	${Q}echo "# and is independent of the normal template package" >> $@
	${Q}echo "# therefore all normal package data should be added here" >> $@
	${Q}echo "" >> $@
	${Q}echo "# Define a reasonable name of this package" >> $@
	${Q}echo "PACKAGE=${PACKAGE}" >> $@
	${Q}echo "" >> $@
	${Q}echo "# Add extra files here" >> $@
	${Q}echo "EXTRA_DIST_FILES=" >> $@
	${Q}echo "" >> $@
	${Q}echo "# You could add extra maketargets here" >> $@
	${Q}echo "# target:dependency" >> $@
	${Q}echo "#	command" >> $@
	${Q}echo "" >> $@
	@${COMMANDS}; printOK

