#######################
## Makefile by tobibobi
#######################

bdir=build

fig_files:=${wildcard fig/*.fig}
img_files:=${wildcard img/*.*}
pdf_from_fig_files= $(subst fig/,${bdir}/,$(fig_files:.fig=.pdf))
files:=	${bdir}/doc.sty \
	$(addprefix ${bdir}/,$(wildcard *.tex) $(wildcard *.bib)) \
	${pdf_from_fig_files} \
	$(subst img/,$(bdir)/,$(img_files))

mpdf=doc.pdf

define out
 >> $(basename $@).pdftex_t
endef

ifdef V
  ifeq ("$(origin V)", "command line")
    KBUILD_VERBOSE = $(V)
  endif
endif
ifndef KBUILD_VERBOSE
  KBUILD_VERBOSE = 0
endif
ifeq ($(KBUILD_VERBOSE),0)
  define Q
    @
  endef
  define SILENT
    >/dev/null 2>/dev/null
  endef
  LATEXFLAGS= --interaction batchmode
else
  define USILENT
    >/dev/null 2>/dev/null
  endef
  LATEXFLAGS=
endif



all: copyright ${bdir}/${mpdf}

copyright:
	@echo "This makefile is brought to you by Tobias Nielsen"

xpdf: ${bdir}/${mpdf}
	@acroread ${bdir}/${mpdf}

xdvi: ${bdir}/doc.dvi
	@xdvi $<

xps: ${bdir}/doc.ps
	@ggv $<

clean: copyright
	@echo -n "* Clean... "
	@rm -rf ${bdir}
	@rm -f *~
	@rm -f fig/*.bak
	@echo "ok"

fig_to_pdf: ${pdf_from_fig_files}

print: ${bdir}/doc.ps
	lp $<



${bdir}/doc.dvi : $(files)
	@echo " tex    $<" $(USILENT)
	cd ${bdir};latex doc.tex ${SILENT}

${bdir}/${mpdf} : $(files) ${bdir}/references.bbl ${bdir}/doc.aux
	@echo " pdftex $<" $(USILENT)
	@cd ${bdir};pdflatex ${LATEXARGS} doc.tex ${SILENT}
	@cd ${bdir};pdflatex ${LATEXARGS} doc.tex ${SILENT}

${bdir}/references.bbl : ${bdir}/references.bib ${bdir}/${mpdf:.pdf=.aux}
	@echo " bibtex $<" $(USILENT) 
	-@cd ${bdir};bibtex doc ${SILENT}

${bdir}/${mpdf:.pdf=.aux} : ${bdir}/doc.tex ${files}
	@echo " aux    $<" $(USILENT)
	@cd ${bdir};pdflatex ${LATEXARGS} doc.tex ${SILENT}

${bdir}/doc.ps : ${bdir}/doc.dvi
	dvips -o $@ $< $(SILENT)

$(bdir)/%.pdf : fig/%.fig 
	@echo " pdftex $<" $(USILENT)
	@rm -f $@
	@fig2dev -L pdftex -p 0 $< >$@ ${SILENT}
	@fig2dev -L pdftex_t -p $(<F:.fig=) $< >$(basename $@).pdftex_t ${SILENT}

$(bdir)/% : %
	@echo " copy   $< $@" $(USILENT)
	@mkdir -p ${bdir}
	@cp $< $@

${bdir}/% : img/%
	@echo " imgcp  $< $@" $(USILENT)
	@mkdir -p ${bdir}
	@cp $< $@
	@rm -f $(basename $@).pdftex_t
	@echo "\\includegraphics{$(basename $(@F))}%" ${out}
