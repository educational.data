J=0.1;
B=0.05;
Ka=0.1;
Km=0.1;
R=1;
L=1;
T=0.1;

%Tilstandsbeskrivelse def:
A=[-R/L,-Ka/L;Km/J,-B/J];
B=[1/L;0];
C=[0,1];
D=0;

%krav
w1=0.657*4;
Pm=60;

%Overf?ringsfunktion i s-dom?n:
[n1,d1]=ss2tf(A,B,C,D)
Gp=tf(n1,d1)

T = 0.1
Gz = c2d(Gp,T,'zoh')
Gw = d2c(Gz,'tustin')
[G_w1_G, G_w1_P] = bode(Gw,w1)
figure(1)
margin(Gw)

tau_i = 1/(0.1*w1);
Kp_PI=1;
PI = tf([tau_i 1],[tau_i 0])
D_wPI = Kp_PI*PI
[PI_w1_G, PI_w1_P] = bode(D_wPI,w1)
figure(2)
margin(D_wPI*Gw)

% Transformere PI regulator til diskret tid
D_zPI = c2d(D_wPI,T,'tustin')
%lukket sl?jfe step responds kun med PI regulator
G_cl_z = feedback(D_zPI*Gz,1)
figure(3)
step(G_cl_z)

%Udregne Phi max
[G_ol_w1, P_ol_w1] = bode(Gw*D_wPI,w1)
Phi_max = Pm - 180 - P_ol_w1
Phi_max_rad = (Phi_max*pi)/180
alfa = (1 - sin(Phi_max_rad))/(1 + sin(Phi_max_rad))
% bestem tau_d
tau_d = 1/(w1*sqrt(alfa))
% Bestem Kp
Kp_LEAD=1; 
% Lead regulatoren er nu bestem.
D_wLEAD = tf(Kp_LEAD*[tau_d 1],[tau_d*alfa 1])
[LEAD_w1_G, LEAD_w1_P] = bode(D_wLEAD,w1)
% Regulatorens overf?ringsfunktion i z-planet.
D_zLEAD = c2d(D_wLEAD,T,'tustin')

%lukket sl?jfe step responds kun med I og LEAD regulator
G_cl_z = feedback(D_zPI*D_zLEAD*Gz,1)
figure(6)
step(G_cl_z)

Kp=1/(G_w1_G*PI_w1_G*LEAD_w1_G)
% Verificering af kravene.
figure(4)
margin(Kp*D_wPI*D_wLEAD*Gw)

G_cl_z = feedback(Kp*D_zPI*D_zLEAD*Gz,1)
figure(5)
step(G_cl_z)