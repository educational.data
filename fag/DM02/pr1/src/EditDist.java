
public class EditDist {
    String X, Y;
    int m[][];
    public EditDist (String sX, String sY) {
	X = sX;
	Y = sY;
	// initialise matrix
	// We use a matrix of size |X| but 1 based - therefore we 
	// need an extra cell
	m = new int[X.length () + 1][Y.length () + 1];

	// build it only at initial state.
	buildMatrix ();
    } 
    

    // buildMatrix
    // performs in O(mn) time
    private void buildMatrix () {
	int i = 0;
	int j = 0;

	m[0][0] = 0;
	for (i = 1; i <= X.length (); i++) {
	    m[i][0] = i;
	}

	for (j = 1; j <= Y.length (); j++) {
	    m[0][j] = j;
	}

	// the actual magic..
	// it seems that Math.min cannot handle an array
	// but that dosn't matter since we can always
	// nest the data
	for (i = 1; i <= X.length (); i++) {
	    for (j = 1; j <= Y.length (); j++) {
		m[i][j] =
		    Math.min (Math.
			      min (m[i - 1][j - 1] +
				   (X.charAt (i - 1) ==
				    Y.charAt (j - 1) ? 0 : 1),
				   m[i - 1][j] + 1), m[i][j - 1] + 1);
	    }
	}
    }

    // just as in LCS the interesting value is 
    // at its very last cell..
    public int getMaxValue () {
	return m[X.length ()][Y.length ()];
    }
}
