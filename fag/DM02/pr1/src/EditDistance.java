import java.io.*;

public class EditDistance {
    public EditDistance () {
    }

    public static void main(String args[]) {
	String str1, str2;

	// we must just make sure that we are not
	// encountoured by stupid users.	
	if( args.length == 0){
	    System.out.println("Whoops: Must have a file to work on");
	    return;
	}

	// we simply ignores all extra arguments added at the
	// command line - we could be more nice, but...
	try {
	    FileReader fr = new FileReader(args[0]);
	    BufferedReader inFile = new BufferedReader(fr);

	    // we buffer data and run outside the try statement
	    // just because it provides us with better 
	    // debugging diagnostics
	    str1 = inFile.readLine();
	    str2 = inFile.readLine();
	}
	catch (FileNotFoundException e) {
	    // trivial...
	    System.out.println("Error: The file cannot be found");
	    return;
	}
	catch (Exception e) {
	    System.out.println("Error: something happened");
	    return;
	}

	int val = (new EditDist(str1,str2)).getMaxValue();
	System.out.println(Integer.toString(val));
    }
}
