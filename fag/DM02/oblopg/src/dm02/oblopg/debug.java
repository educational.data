/*
 * debug.java
 *
 * Created on 2005-10-11 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes debug
 */
package dm02.oblopg;

public class debug {
    private static boolean doDebug = false;
    public static void setVerbose() {
        doDebug = true;
    }
    public static boolean getDebug()
    {
        return doDebug;
    }
}
