/*
 * Created on 2005-10-10
 * This file is created 2005-10-10 and descripes Parser
 */
package dm02.oblopg.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

import dm02.oblopg.debugParser;
import dm02.oblopg.schedule.TimeSchedule;

/**
 * This class generates a graph from the read file.
 * 
 * @author tobibobi
 */
public class Builder {
    /** 
     * <i>oldStation</i> remembers which station we just left
     * This is to be used when making outher connections.
     */
    
    
    private TimeSchedule theSchedule;
    
    public Builder() {
        
    }
    
    public TimeSchedule buildFromFile(String file) throws IOException {
        theSchedule = new TimeSchedule();
        BufferedReader in = new BufferedReader(new FileReader(file));
        String line;
        
        while((line = in.readLine()) != null) {
            parseLine(line);
        }
        
        theSchedule.sortGraph();
        theSchedule.connectInnerStations();
        
        return theSchedule;
    }
    
    /**
     * extracts info from a single line.
     * 
     * @param line The line that needs to be parsed.
     */
    private void parseLine(String line)
    {
        // use the comma as the delimiter
        StringTokenizer token = new StringTokenizer(line,",");
        
        if(token.hasMoreTokens()) {
            // read the town (without comma)
            String town = token.nextToken().trim();
            
            // read the time as a string
            //
            // since we are using a comma as a delimiter,
            // we must trim the result in order to delete any
            // unusable charecters
            String time = token.nextToken().trim();
            
            theSchedule.addDeparture(town,time);
            
        } else {
            theSchedule.createNewLine();
            debugParser.printEndestation();
        }
    }
}
