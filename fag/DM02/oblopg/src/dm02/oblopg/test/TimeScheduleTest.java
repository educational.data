/*
 * TimeScheduleTest.java
 *
 * Created on 2005-10-28 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes TimeScheduleTest
 */
package dm02.oblopg.test;

import java.util.Iterator;

import junit.framework.TestCase;
import dm02.oblopg.schedule.Departure;
import dm02.oblopg.schedule.TimeSchedule;
import dm02.oblopg.schedule.graph.Edge;
import dm02.oblopg.schedule.graph.Graph;
import dm02.oblopg.schedule.graph.Node;

public class TimeScheduleTest extends TestCase {
    /*
     * Test method for 'dm02.oblopg.schedule.TimeSchedule.addDeparture(String, String)'
     */
    public void testAddDeparture()
    {
        TimeSchedule sched = new TimeSchedule();
        sched.addDeparture("Odense","10:00");
        sched.addDeparture("Slagelse","10:10");
        Graph<Departure> G = sched.getGraph();
        Node<Departure> nd = G.node(0);
        assertEquals("Odense",nd.item().getTown());
        assertEquals("Slagelse",G.node(1).item().getTown());
        
        Iterator<Edge<Departure>> it = nd.edgeIterator();
        assertEquals("No edge to look at", true, it.hasNext());
        assertEquals("Line to Slagelse was not correct", "Slagelse",((Departure)(it.next().getNode().item())).getTown());
    }

    /*
     * Test method for 'dm02.oblopg.schedule.TimeSchedule.sortGraph()'
     */
    public void testSortGraph()
    {
        TimeSchedule sched = new TimeSchedule();
        sched.addDeparture("Odense","10:00");
        sched.addDeparture("Slagelse","10:10");
        sched.createNewLine();
        sched.addDeparture("Odense","00:01");
        
        sched.sortGraph();
        
        assertEquals("Odense",sched.getDeparture(0).getTown());
        assertEquals(sched.getDeparture(0).getDepartureTime(),"00:01");
        
        assertEquals("Odense",sched.getDeparture(1).getTown());
        assertEquals(sched.getDeparture(1).getDepartureTime(),"10:00");
        
        assertEquals("Slagelse",sched.getDeparture(2).getTown());
        assertEquals(sched.getDeparture(2).getDepartureTime(),"10:10");
    }

    /*
     * Test method for 'dm02.oblopg.schedule.TimeSchedule.connectInnerStations()'
     */
    public void testConnectInnerStations()
    {
        TimeSchedule sched = new TimeSchedule();
        sched.addDeparture("Odense","00:00");
        sched.addDeparture("Slagelse","00:30");
        sched.createNewLine();
        sched.addDeparture("Odense","00:10");
        sched.addDeparture("Slagelse","00:40");
        sched.createNewLine();
        sched.addDeparture("Odense","00:12");
        sched.addDeparture("Zenpa","14:00");
        
        sched.sortGraph();
        sched.connectInnerStations();
        
        // first is odense 00:00 with an edgecount of tree since Odense 00:10 and 00:12 
        // is within the timespan
        assertEquals(3,sched.getNode(0).edgeCount());
        // second is odense 00:10 with only one node since the time of the next departure
        // 00:12 is to close and therefore not valid
        assertEquals(1,sched.getNode(1).edgeCount());
        
        
        // the rest is just to make sure that we are pointing in the right direction
        assertEquals(3,sched.getNode(0).getEdge(0).getNodeId());
        assertEquals(1,sched.getNode(0).getEdge(1).getNodeId());
        assertEquals(4,sched.getNode(1).getEdge(0).getNodeId());
        
    }

}
