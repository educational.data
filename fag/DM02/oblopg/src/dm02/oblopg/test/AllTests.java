/*
 * AllTests.java
 *
 * Created on 2005-10-31 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes AllTests
 */
package dm02.oblopg.test;

import dm02.oblopg.schedule.graph.WeightTest;
import dm02.oblopg.schedule.heap.DijkstraHeapTest;
import junit.framework.Test;
import junit.framework.TestSuite;

public class AllTests {

    public static void main(String[] args)
    {
        junit.swingui.TestRunner.run(AllTests.class);
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("Test for dm02.oblopg.test");
        //$JUnit-BEGIN$
        suite.addTestSuite(TimeScheduleTest.class);
        suite.addTestSuite(MinuteTimeTest.class);
        suite.addTestSuite(MinPriorityHeapTest.class);
        //$JUnit-END$
        
        suite.addTestSuite(WeightTest.class);
        suite.addTestSuite(DijkstraHeapTest.class);
        return suite;
    }

}
