/*
 * MinPriorityHeapTest.java
 *
 * Created on 2005-10-28 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes MinPriorityHeapTest
 */
package dm02.oblopg.test;

import junit.framework.TestCase;
import dm02.oblopg.schedule.graph.Node;
import dm02.oblopg.schedule.graph.NodeItem;
import dm02.oblopg.schedule.heap.MinPriorityHeap;

public class MinPriorityHeapTest extends TestCase {
    class TClass extends NodeItem {
        int id;
        TClass(int id,int fk) {
            this.id = id;
        }
        @Override
        public int getWeightOffset()
        {
            // TODO Auto-generated method stub
            return 0;
        }
    }

    /*
     * Test method for 'dm02.oblopg.schedule.heap.MinPriorityHeap.extractMin()'
     */
    public void testExtractMin()
    {
        MinPriorityHeap<TClass> mpri = new MinPriorityHeap<TClass>();
        mpri.insert(10, new Node<TClass>(new TClass(10,0)));
        mpri.insert(1, new Node<TClass>(new TClass(1,1)));
        mpri.insert(5, new Node<TClass>(new TClass(5,2)));
        
        assertEquals(1,mpri.pop().id);
        assertEquals(5,mpri.pop().id);
        assertEquals(10,mpri.pop().id);
    }
}
