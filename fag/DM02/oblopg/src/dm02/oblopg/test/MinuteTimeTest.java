/*
 * Created on 2005-10-11
 * This file is created 2005-10-11 and descripes MinuteTimeTest
 */
package dm02.oblopg.test;

import dm02.oblopg.schedule.MinuteTime;
import junit.framework.TestCase;

public class MinuteTimeTest extends TestCase {

    /*
     * Test method for 'dm02.oblopg.parser.MinuteTime.getAsMinutes()'
     */
    public void testGetAsMinutes()
    {
        MinuteTime time = new MinuteTime(10);
        assertEquals(10,time.getAsMinutes());
        
        time = new MinuteTime("01:10");
        assertEquals(70,time.getAsMinutes());
    }
    
    public void testConstructor() {
        MinuteTime time = new MinuteTime("01:10");
        assertEquals(10,time.getMinutes());
        assertEquals(1,time.getHour());
        
        time = new MinuteTime(70);
        assertEquals(10,time.getMinutes());
        assertEquals(1,time.getHour());
    }

    /*
     * Test method for 'dm02.oblopg.parser.MinuteTime.diff(MinuteTime)'
     */
    public void testDiff()
    {
        MinuteTime t1 = new MinuteTime(10);
        MinuteTime t2 = new MinuteTime(20);
        
        assertEquals(10,t2.diff(t1).getAsMinutes());
    }
    
    public void testDiffOverMidnight() {
        MinuteTime t1 = new MinuteTime("23:59");
        MinuteTime t2 = new MinuteTime("00:09");
        
        assertEquals(10,t2.diff(t1).getAsMinutes());
    }
    
    public void testPrintout() {
        MinuteTime t2 = new MinuteTime("00:09");
        assertEquals("00:09",t2.toString());
    }

}
