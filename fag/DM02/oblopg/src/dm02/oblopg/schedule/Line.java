/*
 * Line.java
 *
 * Created on 2005-10-27 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes Line
 */
package dm02.oblopg.schedule;

import dm02.oblopg.schedule.graph.Graph;

public class Line {
    private int oldStation = -1;
    private Graph<Departure> theGraph;
    Line (Graph<Departure> g) {
        theGraph = g;
    }
    Departure addDeparture(String town,String time) {
        Departure dep = new Departure(time,town);
        int nodeid = theGraph.addNode(dep);

        // if we came from a station - create a connection to this one.
        if( oldStation != -1) {
        
            theGraph.addDirConn(oldStation,nodeid,false);
        } 
        oldStation = nodeid;
        return dep;
    }

}
