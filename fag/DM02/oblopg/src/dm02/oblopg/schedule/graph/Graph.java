/*
 * Graph.java Created on 2005-10-22 by ex1041 and was
 * orriginally a part of the oblopg project. it descripes
 * Graph
 */
package dm02.oblopg.schedule.graph;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;

/**
 * A graph is merely a list of nodes.
 * 
 * @author ex1041
 */
public class Graph<T extends NodeItem> {
    private ArrayList<Node<T>> nodes;

    public Graph() {
        nodes = new ArrayList<Node<T>>();
    }

    public void addDirConn(int from, int to,boolean inner)
    {
        Node<T> fromNode = node(from);
        Node<T> toNode = node(to);
        Weight w = Weight.createWeightWithDayRotation(fromNode.item().getWeightOffset(),
                toNode.item().getWeightOffset());
        
        fromNode.addEdgeToNode(toNode, w,inner);
    }

    public void addDriConn(int from, int to, Weight weight, boolean inner)
    {
        Node<T> fromNode = node(from);
        Node<T> toNode = node(to);

        fromNode.addEdgeToNode(toNode, weight,inner);

    }

    public int addNode(Node<T> node)
    {
        int size = nodes.size();
        nodes.add(node);
        node.setNodeId(size);
        node.setGraph(this);
        return size;
    }

    public int addNode(T item)
    {
        Node<T> nd = new Node<T>(item);
        return addNode(nd);
    }

    public void clearGraph()
    {
        for (Node<T> node : nodes) {
            node.setNodeId(0);
            node.setGraph(null);
        }
        nodes.clear();
    }

    public Node<T> node(int id)
    {
        return nodes.get(id);
    }

    /**
     * Sorts the nodes in the hiraki
     * 
     * @param c
     */
    @SuppressWarnings("unchecked")
    public void sortNodes(Comparator<T> c)
    {
        class compCls<NI extends NodeItem> implements Comparator<Node<T>> {
            Comparator<T> pC;

            public compCls(Comparator<T> C) {
                pC = C;
            }

            public int compare(Node<T> arg0, Node<T> arg1)
            {
                T a0 = arg0.item();
                T a1 = arg1.item();
                return pC.compare(a1, a0);
            }
        }
        Comparator comp = new compCls<T>(c);
        
        Object[] arr = nodes.toArray();
        Arrays.sort(arr,comp);

        

        // This is a STUPID way of doing it!!!
        // i cannot by any means understand why we have to
        // do it so.
        nodes.clear();
        int id = 0;
        for (Object o : arr) {
            Node<T> nod = (Node<T>)o;
            nod.setNodeId(id++);
            nodes.add(nod);
        }
    }

    public Iterator<Node<T>> iterator()
    {
        return nodes.iterator();
    }

    public int size()
    {
        return nodes.size();
    }
}
