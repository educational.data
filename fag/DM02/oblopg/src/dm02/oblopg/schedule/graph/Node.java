/*
 * Node.java
 *
 * Created on 2005-10-22 by ex1041
 * and was orriginally a part of the oblopg project.
 * it descripes Node
 */
package dm02.oblopg.schedule.graph;

import java.util.Iterator;
import java.util.Vector;

public class Node <T extends NodeItem> {
	private Weight weight;
	/**
	 * all known edges to other nodes.
	 * The edges is only to the neighbours.
	 */
	private Vector<Edge<T>> edges;
	/**
	 * the graph that is owning this object
	 */
	private Graph<T> G;
	
	/**
	 * current nodeId of this object.
	 */
	private int nodeId;
	private T nodeItem;

	public Node(T item) {
		edges = new Vector<Edge<T>>();
		this.nodeItem = item;
	}

	/**
	 * Internal function in the case a more advanced insert is needed.
	 * @param edge
	 */
	private void addEdge(Edge<T> edge) {
		edges.add(edge);
	}

	/**
	 * Adds a edge directly to a different node in the adjency list.
	 * @param node
	 * @param weight 
     * @param inner
	 */
	public void addEdgeToNode(int node, Weight weight, boolean inner) {
		addEdge(new Edge<T>(node, G, weight,inner));
	}

	/**
	 * By using a node object, a directioned edge is created.
	 * @param toNode the object to connect to.
	 * @param weight 
	 */
	public void addEdgeToNode(Node<T> toNode, Weight weight, boolean inner) {
		addEdgeToNode(toNode.id(),weight,inner);
	}

	/**
	 * Instead of creating an highrisk iteration interface
	 * an lowrisk iterator is used (it was already there)
	 * 
	 * @return Iterator object that can be processed
	 */
	public Iterator<Edge<T>> edgeIterator() {
		return edges.iterator();
	}

	/** 
	 * @see #setId()
	 * @return
	 */
	public int id() {
		return nodeId;
	}

	/**
	 * In order to be able to access other nodes,
	 * the Graph object must be known.
	 * 
	 * @param graph the graph object.
	 */
	public void setGraph(Graph<T> graph) {
		G = graph;
	}

	/**
	 * each time a node is added, it is assigned its d number in order to 
	 * be able to know where its placed in case someone ask of it.
	 * 
	 * @param nodeId
	 */
	public void setNodeId(int id) {
		this.nodeId = id;
	}

	public Weight getWeight() {
		return weight;
	}

	public void setWeight(Weight weight) {
		this.weight = weight;
	}

	public T item() {
		return nodeItem;
	}

    public Edge<T> getEdge(int index)
    {
        if(edges.size() <= index) 
            return null;
        else {
            return edges.get(index);
        }
    }

    public int edgeCount()
    {
        return edges.size();
    }

    public Edge<T> getEdgeTo(int v)
    {   
        int i;
        for(i = 0;i < edgeCount() && getEdge(i).getNodeId() != v;i++);
        if(i != edgeCount())
            return getEdge(i);
        else 
            return null;
    }
    
    public String toString() {
        return "Edge count " + edges.size() + " Item: " + item().toString() + "\n";
    }
}
