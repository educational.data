/*
 * Edge.java
 *
 * Created on 2005-10-22 by ex1041
 * and was orriginally a part of the oblopg project.
 * it descripes Edge
 */
package dm02.oblopg.schedule.graph;

public class Edge <T extends NodeItem> {

	/**
	 * I stedet for at bruge et nummer til den 
     * node som skal indsættes,
	 * Bruger vi selve Noden.
	 * Dette har den fordel at ved en evt. ændring 
     * af nodenummeret, vil den stadigt pege på den 
     * rigtige node.
	 */
	private Node<T> node;
	private Weight weight;
    private boolean innerEdge;

	public Edge(int nodenumber,Graph<T> G, Weight weight, boolean inner) {
        innerEdge = inner;
		this.weight = weight;
		node = G.node(nodenumber);
        
	}

	public Edge(Node<T> node,Graph G, Weight weight) {
		this.node = node;
		this.weight = weight;
	}
    
    public boolean isInnerEdge() {
        return innerEdge;
    }


	public Node<T>getNode() {
		return node;
	}
	
	public int getNodeId() {
		return node.id();
	}

	public Weight getWeight() {
		return weight;
	}

	public void setWeight(Weight weight) {
		this.weight = weight;
	}
}
