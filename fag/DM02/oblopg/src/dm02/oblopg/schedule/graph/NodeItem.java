/*
 * NodeItem.java
 *
 * Created on 2005-10-22 by ex1041
 * and was orriginally a part of the oblopg project.
 * it descripes NodeItem
 */
package dm02.oblopg.schedule.graph;

public abstract class NodeItem {
    /**
     * used bu the edge to calculate a weight for the actual edge
     * 
     * @return a number to represent the relative offset
     */
    abstract public int getWeightOffset();

}
