/*
 * Weight.java
 *
 * Created on 2005-10-22 by ex1041
 * and was orriginally a part of the oblopg project.
 * it descripes Weight
 */
package dm02.oblopg.schedule.graph;

public class Weight {
    int w;
    private Weight (int weight) {
        w = weight;
    }
    public int getWeight() {
        return w;
    }
    public static Weight createWeight(int from, int to) {
        return new Weight(to-from);
    }
    
    public static Weight createWeight(int weight) {
        return new Weight(weight);
    }
    
    public static Weight createWeightWithDayRotation(int from, int to)
    {
        int w = to - from;
        if(w < 0) w = to+1440 - from;
        return new Weight(w);
    }
    public void addWeight(int i)
    {
        w += i;
    }
    
    public String toString() {
        return Integer.toString(w);
    }
}
