/*
 * WeightTest.java
 *
 * Created on 2005-10-27 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes WeightTest
 */
package dm02.oblopg.schedule.graph;

import junit.framework.TestCase;

public class WeightTest extends TestCase {

    /*
     * Test method for 'dm02.oblopg.graph.Weight.createWeight(int)'
     */
    public void testCreateWeightInt()
    {
        Weight w = Weight.createWeight(10);
        assertEquals(w.getWeight(),10);
    }
    
    /*
     * Test method for 'dm02.oblopg.graph.Weight.createWeight(int, int)'
     */
    public void testCreateWeightIntInt()
    {
        Weight w = Weight.createWeight(10,15);
        assertEquals(w.getWeight(),5);
    }

    

    /*
     * Test method for 'dm02.oblopg.graph.Weight.createWeightWithDayRotation(int, int)'
     */
    public void testCreateWeightWithDayRotation()
    {
        Weight w = Weight.createWeightWithDayRotation(1435,5);
        assertEquals(w.getWeight(),10);
        w = Weight.createWeightWithDayRotation(5,10);
        assertEquals(w.getWeight(),5);
    }

}
