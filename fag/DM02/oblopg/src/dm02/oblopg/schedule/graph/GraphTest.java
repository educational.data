/*
 * GraphTest.java
 *
 * Created on 2005-10-22 by ex1041
 * and was orriginally a part of the oblopg project.
 * it descripes GraphTest
 */
package dm02.oblopg.schedule.graph;

import java.util.Comparator;

import junit.framework.TestCase;

public class GraphTest extends TestCase {
	/*
	 * Test method for 'dm02.oblopg.graph.Graph.addNode(Node)'
	 */
	public void testAddNode() {
		class Nod extends NodeItem {
            public int getWeightOffset() { return 0; }
		}
        Graph<Nod> G = new Graph<Nod>();
		Node<Nod> nod = new Node<Nod>(new Nod());
		Node<Nod> nod2 = new Node<Nod>(new Nod());
		G.addNode(nod);
		G.addNode(nod2);
		assertEquals(nod.id(), 0);
		assertEquals(nod2.id(), 1);
		G.clearGraph();
	}

	/*
	 * Test method for 'dm02.oblopg.graph.Graph.addDirectedConnection(int, int)'
	 */
	public void testAddDirectedConnection() {
		class Nod extends NodeItem {
            public int getWeightOffset() { return 0; }
		}
        Graph<Nod> G = new Graph<Nod>();
		Node<Nod> nod = new Node<Nod>(new Nod());
		Node<Nod> nod2 = new Node<Nod>(new Nod());
		G.addNode(nod);
		G.addNode(nod2);
		G.addDriConn(0, 1, Weight.createWeight( 0 ),false);
		assertEquals(nod.edgeIterator().next().getNode().id(), 1);
		G.clearGraph();
	}

	public void testCompare() {
		class Nod extends NodeItem {
			private int id;

			public Nod(int i) {
				id = i;
			}

			public int getId() {
				return id;
			}
            public int getWeightOffset() { return 0; }
        }
		class comp implements Comparator<Nod> {
			public int compare(Nod arg0, Nod arg1) {
				Nod a0 = (Nod) arg0;
				Nod a1 = (Nod) arg1;

				return (a0.getId() < a1.getId() ? 1 : -1);
			}
		}
        Graph<Nod> G = new Graph<Nod>();
        
		G.addNode(new Node<Nod>(new Nod(0)));
		G.addNode(new Node<Nod>(new Nod(2)));
		G.addNode(new Node<Nod>(new Nod(1)));
		
		G.sortNodes(new comp());
		
		// test to see if ids are still correct
		assertEquals(0,G.node(0).id());
		assertEquals(1,G.node(1).id());
		assertEquals(2,G.node(2).id());
		
		// and are the objects correctly sorted?		
		assertEquals(0,((Nod)G.node(0).item()).getId());
		assertEquals(1,((Nod)G.node(1).item()).getId());
		assertEquals(2,((Nod)G.node(2).item()).getId());
		
		
	}
}
