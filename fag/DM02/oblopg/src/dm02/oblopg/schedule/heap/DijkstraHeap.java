/*
 * DijkstraHeap.java Created on 2005-10-28 by tobibobi and
 * was orriginally a part of the DM02 project. it descripes
 * DijkstraHeap
 */
package dm02.oblopg.schedule.heap;

import java.util.Vector;

import dm02.oblopg.schedule.Departure;
import dm02.oblopg.schedule.graph.Edge;
import dm02.oblopg.schedule.graph.Graph;
import dm02.oblopg.schedule.graph.Node;
import dm02.oblopg.schedule.graph.NodeItem;

/**
 * @author tobibobi
 */
public class DijkstraHeap<T extends NodeItem> {

    public static final int MAX = 2000;
    MinPriorityHeap<T> heap;
    Integer[] d;
    Integer[] pi;
    Integer[] inner;
    int length;
    Graph<T> g;

    public DijkstraHeap(Graph<T> g) {
        d = new Integer[g.size()];
        pi = new Integer[g.size()];
        inner = new Integer[g.size()];
        length = g.size();
        this.g = g;
        heap = new MinPriorityHeap<T>();
    }

    int insC = 0;
    
    void initializeSingleSource(Vector<Integer> s)
    {
        for (int v = 0; v < g.size(); v++) {
            if (s.contains(v)) {
                d[v] = 0;
            } else {
                d[v] = MAX;
            }
            pi[v] = null;
            inner[v] = 0;
        }
    }

    void relax(int u, int v)
    {
        /*
        System.out.println("weight between u: " + u + " and v: " + v + " is: " + weight(u,v));
        System.out.println("weight of u: " + d[u] + ", weight of v: " + d[v] + ".");
        */
        boolean mustInsert = false;
        if(d[v] == MAX) { // still not inserted
            mustInsert = true;
        }
        if (d[v] > d[u] + weight(u, v)) {
            
            // System.out.println("new weight is: " + d[u] + weight(u, v));
            
            d[v] = d[u] + weight(u, v);

            pi[v] = u;
            inner[v] = inner[u] + inner(u,v);
        }
        // this is instead of decreasekey
        if(mustInsert){
            heap.insert(d[v],g.node(v));
        }
    }

    /**
     * Calculates the weight from u node to v node of the
     * orriginal graph
     * 
     * @param u
     *            from
     * @param v
     *            to
     * @return the weight in minutes.
     */
    int weight(int u, int v)
    {
        Edge<T> edge = g.node(u).getEdgeTo(v);
        if (edge == null)
            return Integer.MAX_VALUE;
        else
            return edge.getWeight().getWeight();
    }
    
    int inner(int u, int v) {
        Edge<T> edge = g.node(u).getEdgeTo(v);
        if (edge == null)
            return 0;
        else
            return edge.isInnerEdge() == true ? 1 : 0;
    }

    /**
     * Performs the Dijkstra algoritm.
     * 
     * @param steps
     *            the list of start vectors.
     * @return an binary heap of shortest way to the main
     *         station.
     */
    public MinPriorityHeap<T> dijkstra(Vector<Integer> steps)
    {
        initializeSingleSource(steps);

        MinPriorityHeap<T> S = new MinPriorityHeap<T>();

        // create Q from a list of vectors.
        heap = new MinPriorityHeap<T>();
        for(int v = 0;v< steps.size();v++) {
            heap.insert(0,g.node(steps.get(v)));
        }

        // initialize to an empty set S

        while (heap.size() > 0) {
            HeapValue<T> u = heap.extractMin();
            Node<T> nod = u.getItem();
            
            int U = u.getItem().id();
            S.insert(d[U],nod);
            for (int v = 0; v < nod.edgeCount(); v++) {
                relax(U, nod.getEdge(v).getNode().id());
            }
        }
        return S;
    }
    
    public int getInner(int id) {
        return inner[id];
    }

    public Object getD(int i)
    {
        return d[i];
    }
}
