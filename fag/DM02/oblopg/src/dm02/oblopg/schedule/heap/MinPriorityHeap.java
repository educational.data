/*
 * MinPriorityHeap.java Created on 2005-10-28 by tobibobi and was
 * orriginally a part of the DM02 project. it descripes MinPriorityHeap
 */
package dm02.oblopg.schedule.heap;

import java.util.Vector;

import dm02.oblopg.schedule.graph.Node;
import dm02.oblopg.schedule.graph.NodeItem;

public class MinPriorityHeap<T extends NodeItem> {
    private Vector<HeapValue<T>> heap;

    public MinPriorityHeap() {
        heap = new Vector<HeapValue<T>>();
        //heap.add(null);
    }

    public int A(int a)
    {
        return heap.get(a).getValue();
    }
    /**
     * Give an item a lower value and moves it from the last pose to 
     * the correct hose.
     * @param i
     * @param key
     */    
    protected void decreaseKey(int i, int key) throws RuntimeException
    {
        if (key > A(i))
            throw new RuntimeException("new key is greater than current key");
        set(i, key);

        while (i > 0 && A(parent(i)) > A(i)) {
            exchange(i, parent(i));
            i = parent(i);
        }
    }
    
    public T pop() {
        return extractMin().getItem().item();
    }
    
    public T peek() {
        if(min() != null)
            return min().getItem().item();
        else 
            return null;
    }

    /**
     * swaps to items in the heap array.
     * 
     * @param av id of the first item
     * @param bv id of the second item
     */
    protected void exchange(int av, int bv)
    {
        HeapValue<T> a = heap.get(av);
        HeapValue<T> b = heap.get(bv);
        heap.set(av, b);
        heap.set(bv, a);
    }

    /**
     * implemented as Extract-Max
     * @return
     */
    public HeapValue<T> extractMin()
    {
        if (heap.size() == 0)
            return null;

        HeapValue<T> min = min();
        
        HeapValue <T> ne = heap.lastElement();
        
        heap.set(0, heap.lastElement());
        heap.removeElementAt(heap.size() - 1);
        heapify(0);
        return min;
    }

    /**
     * Almost the same as old heapify.
     * 
     * @param i the id of the element to be reheaped
     */
    protected void heapify(int i)
    {
        int l = left(i);
        int r = right(i);
        int lowest;
        
        if (l < size() && A(l) < A(i))
            lowest = l;
        else
            lowest = i;
   
        if (r < size() && A(r) < A(lowest))
            lowest = r;

        if (lowest != i) {
            exchange(i, lowest);
            heapify(lowest);
        }
    }

    
    /**
     * Just a wrap to make code a bit more simple to read
     * 
     * @return the size of the current heap
     */
    protected int size()
    {
        return heap.size();
    }

    
    /**
     * Insert an item in to the end of the heap
     * 
     * @param key the value of the item
     * @param item the actual item
     */
    public void insert(int key, Node<T> item)
    {
        int size = heap.size();
        HeapValue<T> val = new HeapValue<T>(item, Integer.MAX_VALUE);
        heap.add(size,val);
        decreaseKey(size, key);
    }
    
    /**
     * return the index of the left subitem.
     * the old algoritm is:<p>
     * <code>2i</code>
     * @param i the item that you wants to find the left subitem of
     * @return
     */
    int left(int i)
    {
        return 2 * (i+1) - 1;
    }

    /**
     * Returns the minimum value of the Heap.<p>
     * in a min-heap, the topmost item is always the smallest.
     * 
     * @return the valueitem placed with the minimum value.
     */
    public HeapValue<T> min()
    {
        if(heap.size() == 0)
            return null;
        else
            return heap.get(0);
    }

    /**
     * Returns the item above the given item (parent node).<p>
     * The original algoritm was:<p>
     * <code>L i/2 J</code>
     * 
     * @param i index of the item you wants to retrive the parant of
     * @return the parent node id
     */
    int parent(int i)
    {
        return ((int) (i+1)/ 2) - 1;
    }
    
    
    /**
     * return the index of the left subitem.
     * the old algoritm is:<p>
     * <code>2i + 1</code>
     * @param i the item that you wants to find the left subitem of
     * @return
     */
    int right(int i)
    {
        return 2 *(i+1);
    }

    /**
     * Fast internal way to set a new value of an item.<p>
     * Fast in the essence that it makes code easier to read (i hope)
     * @param i
     * @param key
     */
    void set(int i, int key)
    {
        heap.get(i).setValue(key);
    }
    
    /**
     * Returns a string representation of this Heap, containing
     * the String representation of each element.
     */
    public String toString() {
        return heap.toString();
    }
    
    /**
     * Returns a clone of this heap. The copy will contain a
     * reference to a clone of the internal data array, not a reference 
     * to the original internal data array of this <tt>Heap</tt> object. 
     *
     * @return  a clone of this heap.
     */
    @SuppressWarnings("unchecked")
    public MinPriorityHeap<T> clone() {
        MinPriorityHeap<T> newHeap = new MinPriorityHeap<T>();
        newHeap.heap = (Vector<HeapValue<T>>)heap.clone();
        return newHeap;
    }
}
