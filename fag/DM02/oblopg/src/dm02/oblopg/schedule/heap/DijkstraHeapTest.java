/*
 * DijkstraHeapTest.java Created on 2005-10-31 by tobibobi
 * and was orriginally a part of the DM02 project. it
 * descripes DijkstraHeapTest
 */
package dm02.oblopg.schedule.heap;

import java.util.Vector;

import junit.framework.TestCase;
import dm02.oblopg.schedule.graph.Graph;
import dm02.oblopg.schedule.graph.NodeItem;

public class DijkstraHeapTest extends TestCase {
    /**
     * Helper class to establish the right data...
     * 
     * @author tobibobi
     */
    class NI extends NodeItem {
        int weight;

        NI(int w) {
            weight = w;
        }

        @Override
        public int getWeightOffset()
        {
            return weight;
        }

    }

    /**
     * Test method for
     * 'dm02.oblopg.schedule.heap.DijkstraHeap.initializeSingleSource(Vector<Integer>)'
     */
    public void testInitializeSingleSource()
    {
        Graph<NI> g = new Graph<NI>();
        g.addNode(new NI(20));
        g.addNode(new NI(10));
        g.addNode(new NI(10));
        g.addDirConn(1, 0, false);
        g.addDirConn(1, 2, false);

        DijkstraHeap<NI> dh = new DijkstraHeap<NI>(g);
        Vector<Integer> vi = new Vector<Integer>();
        vi.add(1);
        dh.initializeSingleSource(vi);
        assertEquals(0, dh.getD(1));
        assertEquals(DijkstraHeap.MAX, dh.getD(2));
        assertEquals(DijkstraHeap.MAX, dh.getD(0));
        //assertEquals(10, dh.d.pop().weight);
    }

    /**
     * Test method for
     * 'dm02.oblopg.schedule.heap.DijkstraHeap.dijkstra(Vector<Integer>)'
     */
    public void testDijkstra()
    {
        Graph<NI> g = new Graph<NI>();
        g.addNode(new NI(20));
        g.addNode(new NI(10));
        g.addNode(new NI(30));
        g.addDirConn(1, 0, false);
        g.addDirConn(1, 2, false);

        DijkstraHeap<NI> dh = new DijkstraHeap<NI>(g);
        Vector<Integer> vi = new Vector<Integer>();
        vi.add(1);
        dh.dijkstra(vi);
        assertEquals(0, dh.getD(1));
        assertEquals(20, dh.getD(2));
        assertEquals(10, dh.getD(0));

        //assertEquals(10, dh.d.pop().weight);
    }

    /**
     * Test method for
     * 'dm02.oblopg.schedule.heap.weight(int,int)'
     */
    public void testWeight()
    {
        Graph<NI> g = new Graph<NI>();
        g.addNode(new NI(20));
        g.addNode(new NI(10));
        g.addNode(new NI(30));
        g.addDirConn(1, 0,false);
        g.addDirConn(1, 2,false);

        DijkstraHeap<NI> dh = new DijkstraHeap<NI>(g);
        Vector<Integer> vi = new Vector<Integer>();
        vi.add(1);
        assertEquals(Integer.MAX_VALUE, dh.weight(0, 1));
        assertEquals(Integer.MAX_VALUE, dh.weight(2, 1));
        assertEquals(10, dh.weight(1, 0));
        assertEquals(20, dh.weight(1, 2));
    }

}
