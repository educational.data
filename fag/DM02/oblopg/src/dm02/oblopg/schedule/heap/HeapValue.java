/*
 * HeapValue.java
 *
 * Created on 2005-10-28 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes HeapValue
 */
package dm02.oblopg.schedule.heap;

import dm02.oblopg.schedule.graph.Node;
import dm02.oblopg.schedule.graph.NodeItem;

public class HeapValue<T extends NodeItem> {
    private Node<T> item;
    private int value;
    private int inners;
    
    HeapValue(Node<T> item,int value) {
        this.item = item;
        this.value = value;
        inners = 0;
    }

    public Node<T> getItem() {
        return item;
    }
    
    public int getValue() {
        return value;
    }

    void setValue(int key)
    {
        value = key;
    }
    
    public String toString() {
        return "Heap value: " + value + " item: " + item.toString();
    }

    public void setInners(int inners) {
        this.inners = inners;
    }
    
    public int getInners()
    {
        // TODO Auto-generated method stub
        return inners;
    }
}
