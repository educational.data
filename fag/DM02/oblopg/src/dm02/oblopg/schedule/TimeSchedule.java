/*
 * TimeSchedule.java
 *
 * Created on 2005-10-27 by tobibobi
 * and was orriginally a part of the DM02 project.
 * it descripes TimeSchedule
 */
package dm02.oblopg.schedule;

import java.util.Comparator;
import java.util.Vector;

import dm02.oblopg.schedule.graph.Graph;
import dm02.oblopg.schedule.graph.Node;
import dm02.oblopg.schedule.graph.Weight;
import dm02.oblopg.schedule.heap.DijkstraHeap;
import dm02.oblopg.schedule.heap.HeapValue;
import dm02.oblopg.schedule.heap.MinPriorityHeap;

/**
 * Represents a single timeplan
 * It is topmost responsible of delegating tasks
 * @author tobibobi
 */
public class TimeSchedule {
    private Graph<Departure> graph;
    private Line line;
    public TimeSchedule() {
        graph = new Graph<Departure>();
        createNewLine();
    }
    
    public Departure addDeparture(String town, String time) {
        return line.addDeparture(town,time);
    }
    
    public void connectInnerStations()
    {
        int fromId = 0;
        while(fromId < graph.size()) {
            Node<Departure> fromNode = graph.node(fromId);
            Departure fromDeparture = fromNode.item();
            String fromTown = fromDeparture.getTown();
            int toId = fromId + 1;
            boolean okRunning = true;
            while(toId < graph.size() && okRunning) {
                Node<Departure> toNode = graph.node(toId);
                Departure toDeparture = (Departure)(toNode.item());
                String toTown = toDeparture.getTown();
                if(toTown.equals(fromTown)) {
                    Weight w = Weight.createWeightWithDayRotation(fromDeparture.getWeightOffset(),
                            toDeparture.getWeightOffset());
                    
                    // ensure that only departures with at least 5 mins between 
                    // are added
                    if(w.getWeight() > 5) {
                        // add penalty weight
                        w.addWeight(15);
                        graph.addDriConn(fromId,toId,w,true);
                    }
                } else okRunning = false;
                toId++;
            }
            fromId++;
        }
    }

    public Line createNewLine()
    {
        line = new Line(graph);
        return line;
    }

    public Departure getDeparture(int i)
    {
        return (Departure)graph.node(i).item();
    }
    
    public Graph<Departure> getGraph()
    {
        return graph;
    }
    
    public Line getCurrentLine() {
        return line;
    }
    
    public Node<Departure> getNode(int nodid) {
        return graph.node(nodid);
    }

    /**
     * TODO Document this one
     */
    public void sortGraph() {
        class Comp implements Comparator<Departure> {
            public int compare(Departure o1, Departure o2)
            {
                Departure d1 = (Departure)o1;
                Departure d2 = (Departure)o2;
                String t1 = d1.getTown();
                String t2 = d2.getTown();
                
                if(t1.equals(t2)) {
                    return d2.getWeightOffset() - d1.getWeightOffset();
                } else {
                    return t1.compareTo(t2) * -1;
                }
            }
            
        }
        graph.sortNodes(new Comp());        
    }

    public void findFastestWayFrom(String fromStation, String toStation)
    {
        DijkstraHeap<Departure> heap = new DijkstraHeap<Departure>(graph);
        Vector<Integer> vec = new Vector<Integer>();
        for(int v = 0;v < graph.size();v++) {
            if(graph.node(v).item().getTown().equals(fromStation)) {
                vec.add(v);
            }
        }
        
        MinPriorityHeap<Departure> S = heap.dijkstra(vec);
        boolean canRun = S.peek() != null;
        int distTime = Integer.MAX_VALUE;
        int inners = 0;
        while(canRun) {
            if(S.min() == null) {
                System.out.println("No solution");
                return;
            }
            
            HeapValue<Departure> dephv = S.extractMin();
            
            if(dephv.getItem().item().getTown().equals(toStation)) {
                canRun = false;
                distTime = dephv.getValue() - heap.getInner(dephv.getItem().id()) * 15;
                inners = heap.getInner(dephv.getItem().id());
            } 
        }
        System.out.println(distTime + " " + inners);
    }
}
