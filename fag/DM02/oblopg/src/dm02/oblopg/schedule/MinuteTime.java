/*
 * Created on 2005-10-11
 * This file is created 2005-10-11 and descripes MinuteTime
 */
package dm02.oblopg.schedule;

import java.util.StringTokenizer;

public class MinuteTime {
    private int hour;
    private int minute;
    public MinuteTime(String time) {
        StringTokenizer token = new StringTokenizer(time,":");
        hour = Integer.parseInt(token.nextToken());
        minute = Integer.parseInt(token.nextToken());
    }
    
    public MinuteTime(int minutes) {
        hour = minutes/60;
        minute = minutes % 60;
    }
    
    /**
     * calculate the time in minutes
     * @return minutes. 
     */ 
    public int getAsMinutes () {
        return hour*60+minute;
    }
    
    public String toString() {
        String sHour;
        String sMinute;
        
        // make sure that numbers is always two digs wide.
        if(hour < 10)
            sHour = "0" + hour;
        else
            sHour = "" + hour;
        
        if(minute < 10) 
            sMinute = "0" + minute;
        else
            sMinute = "" + minute;
        
        
        return sHour + ":" + sMinute;
    }

    public MinuteTime diff(MinuteTime departureTime)
    {
        int res = getAsMinutes() - departureTime.getAsMinutes();
        if (res < 0) res += 24*60;
        return new MinuteTime(res);
    }

    public Object getMinutes()
    {
        return minute;
    }

    public Object getHour()
    {
        return hour;
    }
    public boolean equals(Object obj) {
        if(obj instanceof String) {
            return toString().trim().equals(((String)obj).trim());
        } else if(obj instanceof MinuteTime) {
            return getAsMinutes() == ((MinuteTime)obj).getAsMinutes();
        } else return false;
    }
}
