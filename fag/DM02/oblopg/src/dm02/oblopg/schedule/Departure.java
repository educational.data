/*
 * Created on 2005-10-11
 * This file is created 2005-10-11 and descripes Departure
 */
package dm02.oblopg.schedule;

import dm02.oblopg.schedule.graph.NodeItem;


public class Departure extends NodeItem {
    private MinuteTime departureTime;
    private String town;
    
    public Departure(String time,String Town) {
        super();
        town = Town;
        departureTime = new MinuteTime(time);
    }
    
    public void setTown(String station)
    {
        town = station;
    }
    
    public String getTown() {
        return town;
    }
    
    public String toString() {
        return town + " klokken " + departureTime;
    }

    public MinuteTime getDepartureTime()
    {
        return departureTime;
    }

    
    public void setDepartureTime(MinuteTime departureTime)
    {
        this.departureTime = departureTime;
    }

    /**
     * @see dm02.oblopg.schedule.graph.NodeItem#getWeightOffset()
     */
    public int getWeightOffset()
    {
        return departureTime.getAsMinutes();
    }
}
