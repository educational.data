/*
 * Created on 2005-10-11
 * This file is created 2005-10-11 and descripes debugParser
 */
package dm02.oblopg;

public class debugParser {

    /**
     * 
     */
    public static void printEndestation()
    {
        if(debug.getDebug())
            System.out.println("******** Endestation ********\n");
    }

    /**
     * @param con
     */
    public static void printTimeSpan(int minutes)
    {
        if(debug.getDebug())
            System.out.println("   >> Rejsetid: " + minutes + " minutter. <<");
    }

    /**
     * @param dep
     */
    public static void printStation(String dep)
    {
        if(debug.getDebug())
            System.out.println(" " + dep);
    }

}
