

import java.io.IOException;
import java.util.StringTokenizer;

import dm02.oblopg.parser.Builder;
import dm02.oblopg.schedule.TimeSchedule;
import dm02.oblopg.debug;

/**
 * @author tobibobi
 */
public final class Rute {

    public String koreplan = null;
    public String fromStation = null;
    public String toStation = null;
    
    boolean errornous = false;
    boolean doHelp=false;

    /**
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException
    {
        Rute prog = new Rute();
        if( ! prog.parseArgs(args)) return;
        
        //System.out.println(koreplan);
        
        TimeSchedule schedule = (new Builder().buildFromFile(prog.koreplan));
        schedule.findFastestWayFrom(prog.fromStation,prog.toStation);
        
    }
    
    public boolean parseArgs(String[] args) {
        for(int p=0;p<args.length;++p) {
            String msg = args[p];
            if(msg.equals("-v") || msg.equals("--verbose")) {
                debug.setVerbose();
            } else if(msg.startsWith("--timetable=")) {
                if(koreplan != null) {
                    System.err.println("Timeplan already defined");
                    errornous = true;
                }
                koreplan=getResult(msg);
            } else if(msg.equals("-k")) {
                if(koreplan != null) {
                    System.err.println("Timeplan already defined");
                    errornous = true;
                }
                koreplan = args[++p];
            } else if(msg.startsWith("--to")) {
                if(toStation != null) {
                    System.err.println("End station already defined");
                    errornous = true;
                }
                toStation=getResult(msg);
            } else if(msg.equals("-t")) {
                if(toStation != null) {
                    System.err.println("End station already defined");
                    errornous = true;
                }
                toStation = msg;
            } else if(msg.startsWith("--from")) {
                if(fromStation != null) {
                    System.err.println("Start station already defined");
                    errornous = true;
                }
                fromStation=getResult(msg);
            } else if(msg.equals("-f")) {
                if(fromStation != null) {
                    System.err.println("Start station already defined");
                    errornous = true;
                }
                fromStation = msg;
            } else if(msg.equals("-h") || msg.equals("--help")) {
                errornous=true;
                doHelp=true;
            } else if(msg.startsWith("-"))  {
                System.err.println("Unknown parameter: " + msg);
                errornous=true;
            } else {
                if(koreplan != null) {
                    if(fromStation != null) {
                        if(toStation != null) {
                            System.err.println("to many arguments");
                            errornous = true;
                        }
                        else {
                            toStation = msg;
                        }
                    }
                    else {
                        fromStation = msg;
                    }
                } else {
                    koreplan = msg;
                }
            }
        }
        if(doHelp) {
            printHelp();
            return false;
        } else {
            if(koreplan == null || fromStation == null || toStation == null) {
                System.err.println("Missing arguments - try --help");
                return false;
            }
        }
        
        if(errornous) return false;
        return true;
    }

    private String getResult(String msg)
    {
        StringTokenizer token = new StringTokenizer(msg,"=");
        token.nextToken();
        return token.nextToken();
    }

    private void printHelp()
    {
        pln("Help:");
        pln("  -h | --help       this little help script");
        pln("  -k [timetable] | --timetable=[timetable]");
        pln("                    add a timetable to seeking program");
        pln("  -t [Station] | --to=[station] end station");
        pln("  -f [station] | --from=[station] start station");
        pln("  -v | --verbose    massive output");
        pln("program [timetable] [from] [to]");
    }
    
    private void pln(String line) {
        System.out.println(line);
    }

}
