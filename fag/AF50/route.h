#ifndef __ROUTE_H__INCLUDED__
#define __ROUTE_H__INCLUDED__

#include "support.h"
#include "timer.h"

class route : public TimerClass {	
	bool seekLineBack();
	bool seekLineLeft(int);
	bool seekLineRight(int);
	int line;
	void driveOnLine(int confidence, int line);
	enum carState { onLine,waitingForLine,lineLost,lineLostBehind,lostLeft,lostRight };
	carState state;
	
	int MIN_LINE; // if the confidence is below this limit - stop.
	int TURN_FACTOR; // how much power diff should we apply
	int bSpeed;
	int DAMPEN;
	int WAIT;
	int CORNER_POWER;

public: 
	route(int line, int turn, int speed, int wait, int dampen,int corner);
	void waitForLine();
	
	void doDrive();
	void execute();
};

#endif
