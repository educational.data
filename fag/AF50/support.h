#ifndef __SUPPORT_H__INCLUDED__
#define __SUPPORT_H__INCLUDED__
typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned long ulong;

// debug details - comment out if not needed
//#define _DEBUG_

void vent(long time);

void inline enable_interrupts(void) { asm ( "sei" ); }
void inline disable_interrupts(void) { asm ( "cli" ); }

template<typename T>
T inline abs(T i) { return i<0 ? -i : i; }

template<typename T>
T inline max(T i, T j) { return i>j ? i : j; }

template<typename T>
T inline min(T i, T j) { return i<j ? i : j; }

#endif
