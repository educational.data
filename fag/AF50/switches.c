#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>

#include "switches.h"

Switches::Switches() {
	DDRA = 0x0;
}

uchar Switches::getButs() {
	return PINA & 0b11111000;
}
