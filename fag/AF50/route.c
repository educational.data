
#include "route.h"
#include "processor.h"
#include <stdio.h>

//#define _DEBUG_

route::route(int line, int turn, int speed, int wait, int dampen, int cornerpower) : TimerClass(2) {
	MIN_LINE = line;
	TURN_FACTOR = turn;
	bSpeed = speed;
	DAMPEN = dampen;
	CORNER_POWER=cornerpower;
	this->line = 0;
	uP.motor.regulatePower(255,255);
	vent(wait);
	//uP.motor.regulatePower(255,255);
	state = waitingForLine;
}


void route::doDrive() {

		//
		// locate the line	
		//
		int lastline = line;
		line = uP.adc.findLine();
		
		//
		// is it a strong line we read?
		//
		int confidence = uP.adc.PowerOfLine;
#ifdef _DEBUG2_
		printf("State: ");
#endif
		static int spinUpTime = 0;
		++spinUpTime;
		switch(state) {
		case onLine:
#ifdef _DEBUG2_
			printf("onLine\n");
#endif
			spinUpTime = 0;
			if(confidence > MIN_LINE)
				driveOnLine(confidence,line);
			else
				if(lastline < -250)
					state = lostLeft;
				else if (lastline > 250)
					state = lostRight;
				else 
					state =lineLost;
			break;
			
		case lineLostBehind:
#ifdef _DEBUG2_
			printf("lineLostBehind\n");
#endif
			if(seekLineBack())
				state = onLine;
			else
				state = lineLost;
			break;
			
		case lostLeft:
#ifdef _DEBUG2_
			printf("lostLeft\n");
#endif		
			if(seekLineLeft(spinUpTime)) state = onLine;
			else if(spinUpTime > 500) state = lineLost;
			break;
		case lostRight:
#ifdef _DEBUG2_
			printf("lostRight\n");
#endif
			if(seekLineRight(spinUpTime)) state = onLine;
			else if(spinUpTime > 500) state = lineLost;
			break;
		case lineLost:
#ifdef _DEBUG2_
			printf("lineLost\n");
#endif
		case waitingForLine:
#ifdef _DEBUG2_
			printf("waitingForLine\n");
#endif
		default:
			if(confidence > MIN_LINE)
				state = onLine;
			else
				uP.motor.regulatePower(0,0);
		}
		
#ifdef _DEBUG_
	//printf("\nLine: %4d conf: %4d ll: %4d\n",line,confidence,lastline);
	//vent(100);
#endif
		
}
//////////////////////////////////////////////////////////////////////
// Seek the line behind us in order to find the last 
// point where we saw the line the last time
//////////////////////////////////////////////////////////////////////
bool route::seekLineBack() {
	int confidence = 0;
	uP.motor.regulatePower(0,-0);
	vent(1000);
	for(int steps=0;steps<4000 && confidence<=MIN_LINE;++steps) {
		//
		// we try to move backwards
		//
		uP.adc.findLine();
		confidence = uP.adc.PowerOfLine;
		uP.motor.regulatePower(-20,-20);
		vent(1);
	}
	uP.motor.regulatePower(0,-0);
	vent(1000);
	
	
	//
	// no matter what, we must stop the car
	//
	uP.motor.regulatePower(0,-0);
	
	//
	// did we leave the loop because we found the line
	// or because the time ran out?
	//
	return confidence > MIN_LINE;
}


//////////////////////////////////////////////////////////
// We are waiting for a line to drive on
//////////////////////////////////////////////////////////
void route::waitForLine() {
	//
	// locate the line
	//
	uP.adc.findLine();
	//
	// are it a strong line we read?
	//
	int confidence = uP.adc.PowerOfLine;
	
	
		//
		// we still havent found any thing - stop...
		//
		uP.motor.regulatePower(0,0);
#ifdef _DEBUG_
		printf("PLACE ME ON A LINE GOD DAMN IT!!!!\n");
#endif
	while(confidence < MIN_LINE) { 
		//printf("C: %d\n",confidence);
		// loop while we cannot determine a line
		uP.adc.findLine();
		confidence = uP.adc.PowerOfLine;
	}
	
}

//////////////////////////////////////////////////////////////
// Drive on the line using the line data
//
// confidence - marks how efficient we are placed 
//              on the line
//
// line - the location of the line
//////////////////////////////////////////////////////////////
void route::driveOnLine(int confidence, int line) {
	//
	// if we have enough confidence of the lines existence
	// just go ahead.
	//
	static long tickToCorner = 0;
	static long lastDistance = 0;
	
	++tickToCorner;
	//
	// Make line more responsive
	//
	//int l = line;
	//int la = l < 0 ? -l : l;
	//int ld = l < 0 ? -1 : 1;
	//line = 2*(la - l*l/200)*ld;
	
	static long lastLine = 0;
	long static dState = 0;
	//dState *=;
	dState += (lastLine - line) * DAMPEN;
	dState/=2;
	int speed  = (uP.speedSensorLeft.getSpeed() + uP.speedSensorRight.getSpeed()) / 2;
	//speed-=15;
	lastLine = line;
	
	int lineadd = CORNER_POWER * bSpeed * (abs(dState) + abs(line/10))/1000;
	
	//
	// calculate power settings.
	//
	int powA = bSpeed + TURN_FACTOR * (line/100) + dState/100 + lineadd - speed;
	int powB = bSpeed - TURN_FACTOR * (line/100) - dState/100 + lineadd - speed;
	
	int maxval = max(powA,powB);
	
	if(maxval > 255) {
		powA = (powA*255) / maxval;
		powB = (powB*255) / maxval;
	}
	
	/* if(abs(powA - powB) > 255) {
		if(tickToCorner > 100)
			lastDistance = tickToCorner-10;
		tickToCorner = 0;
	}
	
	if(tickToCorner > lastDistance) // perform break
		cSpeed= 60;
	else 
		cSpeed = bSpeed;
	
	*/
	uP.motor.regulatePower(powA,powB);
	
#ifdef _DEBUG_
	printf("L: %4d   C: %4d   BS: %4d  ",line,confidence,bSpeed);
	printf("A: %4d   B: %4d   dState: %d speed: %d\n", (int)powA,(int)powB,(int)dState/100,speed);
	//vent(100);
#endif
}	

///////////////////////////////////////////////////////
// try to drive back on the line if we have lost it
// on the left
///////////////////////////////////////////////////////
bool route::seekLineLeft(int time) {
	int confidence = 0;

	uP.motor.regulatePower(200,30-time/5);
	uP.adc.findLine();
	confidence = uP.adc.PowerOfLine;
	
	return confidence > MIN_LINE;
}

///////////////////////////////////////////////////////
// try to drive back on the line if we have lost it
// on the right
///////////////////////////////////////////////////////
bool route::seekLineRight(int time) {
	int confidence = 0;
	//uP.motor.regulatePower(60,20);
	uP.motor.regulatePower(30-time/5,200);
	uP.adc.findLine();
	confidence = uP.adc.PowerOfLine;
	
	return confidence > MIN_LINE;
}


void route::execute() {
	doDrive();
}

