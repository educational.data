#include "hardware.inc"
#define ENABLE_BIT_DEFINITIONS
#include <avrx-io.h>
#include <avrx-signal.h>
#include "avrx.h"
/*
 Basic Tasking Tests #1

 Exercises basic semaphore constructs, Interrupt handling and tasking

 The following API covered:
        AvrXRunTask()
            AvrXResume()        //Indirectly covered
        Epilog()
        IntProlog()
        AvrXIntTestSemaphore()
            AvrXTestSemaphore() //Indirectly covered
        AvrXIntSetSemaphore()
            AvrXSetSemaphore()  //Indirectly covered
        AvrXWaitSemaphore()
        AvrXChangePriority()
        AvrXSelf()

 The code queues tasks on the run queue and the Semaphore:
        in the front
        inserted in the middle
        appended to the end

 The timer code flushes the tasks off the semaphore to start
 the process over again.

 To run this test simply examine the state of the LED bits
 (PORTB) and make sure each task is running regularly.

 20010524 - Make sure the AvrXTestSemaphore() is returning SEM_WAIT
 when multiple tasks are blocked.
 
 */
/*
   One group of task def's will be dropped depending upon which
   compiler is being used.
*/
AVRX_GCC_TASK(task1, 20, 3);
AVRX_GCC_TASK(task2, 20, 2);
AVRX_GCC_TASK(task3, 20, 2);

AVRX_IAR_TASK(task1, 14, 6, 3);
AVRX_IAR_TASK(task2, 14, 6, 2);
AVRX_IAR_TASK(task3, 14, 6, 2);

AVRX_MUTEX(TimerSemaphore);

#define TCNT0_INIT (0xFF-CPUCLK/256/TICKRATE)
#define TMC8_CK256 (1<<CS02)

void main(void)
{
    AvrXSetKernelStack(0);
    
    outp((1<<SE) , MCUCR);      // Initialize Timer Hardware
    outp(TCNT0_INIT, TCNT0);
    outp(TMC8_CK256 , TCCR0);
    outp((1<<TOIE0), TIMSK);    // Enable Timer overflow interrupt

    AvrXRunTask(&task1Tcb);
    AvrXRunTask(&task2Tcb);
    AvrXRunTask(&task3Tcb);

    Epilog();                   // Switch from AvrX Stack to first task
}

AVRX_SIGINT(SIG_OVERFLOW0)
{
    IntProlog();
    outp(TCNT0_INIT, TCNT0);
    while(AvrXIntTestSemaphore(&TimerSemaphore) == SEM_WAIT)
        AvrXIntSetSemaphore(&TimerSemaphore);   // Flush all waiting tasks
    AvrXIntSetSemaphore(&TimerSemaphore);       // Set Semaphore (short path)
    Epilog();
}

NAKEDFUNC(task1)
{
    outp(0xFF, DDRB);
    outp(0xFF, PORTB);

    while(1)
    {
        AvrXWaitSemaphore(&TimerSemaphore);
        outp(inp(PORTB) ^ 1, PORTB);          // Toggle bits
    }
}

// Task two ping pongs between 1 and 3 priority
NAKEDFUNC(task2)
{
    unsigned char tPri = 3;
    while(1)
    {
        AvrXWaitSemaphore(&TimerSemaphore);
        tPri = AvrXChangePriority(AvrXSelf(), tPri);
        outp(inp(PORTB) ^ 2, PORTB);          // Toggle bits
    }
}

// Task three ping pongs between 4 and 0 priority

NAKEDFUNC(task3)
{
    unsigned char tPri = 0;
    while(1)
    {
        AvrXWaitSemaphore(&TimerSemaphore);
        tPri = AvrXChangePriority(AvrXSelf(), tPri);
        outp(inp(PORTB) ^ 4, PORTB);          // Toggle bits
    }
}

