/*
        Hardware.inc

        Define our target hardware.  Unfortunately I don't
        see how to make this work with the C compiler.
*/

#ifndef __TARGETHW
#define __TARGETHW

#define CPUCLK 8000000
#define TICKRATE 4000
#define BAUDRATE 19200

#endif /* __TARGETHW */
