The contents of these directories depend upon
avr-gcc 2.97 or greater from Gunnar Henne website.  

http://www.combio.de/avr/index.html

Follow his instructions for installing and testing the
compiler.  

Then, add to your AUTOEXEC.BAT the following line:

set AVRX=C:\The\Absolute\Path

to the directory where *THIS* file is found.

Change into the various directories and execute "make"
to build the respective applications and libraries.

AvrX		- The RTOS library
Examples	- Well, Examples, yes!
TestCases	- In case you want to hack the kernel


