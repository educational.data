#include "avrx.inc"

/*
        Copyright 1999-2001, Larry Barello
        larry@barello.net

*/

/*+
; --------------------------------------------------
; Generate Interrupt (Single Step Support)
;
; PASSED:       Nothing
; USES:         X, Y and R0
; RETURNS:      Nothing
; NOTES:        Generates an interrupt using Timer0.  Assumes global
;               interrupt flag is cleared.
;
;	NB: This routine is called by AvrX in the Epilog code.
;       This routine must be modfied to fit your particular
;	requirements.  The shipped code works with 8515 varients.
; 
;
-*/
        _MODULE(avrx_generatesinglestepinterrupt)
        _CODESECTION
        _FUNCTION(GenerateInterrupt)
        
GenerateInterrupt:
        ldi     Yl, 1<<CS00     ; 1x prescale
        ser     Xh
        in      R0, TCCR0       ; Previous prescaler
        in      Yh, TCNT0       ; Previous count
        out     TCNT0, Xh       ; Set maximum
        out     TCCR0, Yl       ; Interrupt generated NOW
        out     TCCR0, R0       ; Restore orignal prescaler
        out     TCNT0, Yh       ; Restore original count
        ret
        _ENDFUNC
        _END

