#include        "avrx.inc"
/*
        Copyright 1999-2001, Larry Barello
        larry@barello.net

*/

        _MODULE(avrx_suspend)
        _EXTERN(_Epilog)
        _EXTERN(IntProlog)
        _EXTERN(Running)
        _EXTERN(RunQueue)
        _EXTERN(_RemoveObject)
        
        _CODESECTION

/*+
; --------------------------------------------------
; AvrXSuspend
;
; Mark a pid for suspension and attempt to remove from the run queue
;
; AvrXBreakpoint
;
; Suspend Self (Running) if previously suspended (single stepped)
;
; PASSED:       R25:R24 = PID
; RETURNS:      
; USES: 
; ASSUMES:
; NOTES:        Marks PID for suspension.  If removed from the run
;               queue, then marks suspended.  _QueuePid will take care
;               of this for pids queued elsewhere when they become
;               eligable for running.
-*/
        _FUNCTION(AvrXBreakpoint)
        
AvrXBreakpoint:
        AVRX_Prolog
        lds     Zh, Running+NextH
        lds     Zl, Running+NextL
        ldd     tmp0, Z+PidState
        sbrc    tmp0, SuspendBit
        rjmp    as00
        rjmp    _Epilog
        _ENDFUNC

        _FUNCTION(AvrXSuspend)

AvrXSuspend:                    ; User Entry Point
        AVRX_Prolog
        mov     Zh, p1h
        mov     Zl, p1l
        ldd     tmp0, Z+PidState
        sbr     tmp0, BV(SuspendBit)      ; Mark process for suspending
        std     Z+PidState, tmp0
as00:
        mov     p2h, Zh
        mov     p2l, Zl
        ldi     Zh, hi8(RunQueue)
        ldi     Zl, lo8(RunQueue)
        BeginCritical
        rcall   _RemoveObject           ; Attempt to remove from run queue
        mov     Yl, tmp0
        mov     Yh, tmp1
        adiw    Yl, 0
        breq    as01
        
        ldd     Zl, Y+PidState
        sbr     Zl, BV(SuspendedBit)    ; If found, then mark suspended.
        std     Y+PidState, Zl
as01:
        rjmp    _Epilog
        _ENDFUNC
        _END

