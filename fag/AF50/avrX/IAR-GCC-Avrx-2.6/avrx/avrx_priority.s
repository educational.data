#include        "avrx.inc"
/*
        Copyright 1999-2001, Larry Barello
        larry@barello.net

*/
        _MODULE(avrx_priority)

        _EXTERN(RunQueue)

        _CODESECTION
/*+
; --------------------------------------------------
; uint8_t AvrXChangePriority(pProcessID, uint8_t)
; uint8_t AvrXPriority(void)
; pProcessID AvrXSelf(void)
;
; Routines for manipulating process priorities.  Since GCC is
; inherently little endien and AvrX is big endien, access to
; AvrX data structures is strictly limited to AvrX code.
;
; If I changed AvrX, then many routines could be written in
; C...  Maybe tomorrow
;
; PASSED:       
; RETURNS:      
; USES: 
; CALLS:
; ASSUMES:
; NOTES:        
-*/
        _FUNCTION(AvrXChangePriority)

AvrXChangePriority:
        mov     Zl, p1l
        mov     Zh, p1h
        ldd     r1l, Z+PidPriority
        std     Z+PidPriority, p2l
        ret
        _ENDFUNC
        
        _FUNCTION(AvrXSelf)

AvrXSelf:
        lds     r1h, RunQueue+NextH
        lds     r1l, RunQueue+NextL
        ret
        _ENDFUNC
        
        _FUNCTION(AvrXPriority)

AvrXPriority:
        lds     Zh, RunQueue+NextH
        lds     Zl, RunQueue+NextL
        ldd     r1l, Z+PidPriority
        ret
        _ENDFUNC
        _END
        
