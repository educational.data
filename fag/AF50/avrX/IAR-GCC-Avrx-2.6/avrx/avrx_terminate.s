#include        "avrx.inc"
/*
        Copyright 1999-2001, Larry Barello
        larry@barello.net

*/
        _MODULE(avrx_terminate)
        _EXTERN(IntProlog)
        _EXTERN(_RemoveObject)
        _EXTERN(RunQueue)
        _EXTERN(_Epilog)
        _EXTERN(Running)
        _EXTERN(AvrXIntSetObjectSemaphore)
        
        _CODESECTION
        
/*+
; --------------------------------------------------
; AvrXTaskExit
; AvrXTerminate
;
; This is where tasks that "return" come to.  Set the IDLE bit so
; it can not be queued again.  Then attempt to Dequeue from run queue.
; Can not do anything more than that if it is queued on a semaphore...
;
; PASSED:       R25:R24 = Pid to terminate   
; RETURNS:      
; USES:
; CALLS:
; ASSUMES:
; NOTES:        
-*/
        _FUNCTION(AvrXTaskExit)

AvrXTaskExit:                   ; Entry point for killing self
        AVRX_Prolog             ; Entire state of dying task is on stack
        lds     p1h, Running+NextH
        lds     p1l, Running+NextL
        rjmp    at00
        _ENDFUNC

        _FUNCTION(AvrXTerminate)
AvrXTerminate:                  ; Entry point for killing other tasks.
        AVRX_Prolog
at00:
;       rcall   AvrXIntSetObjectSemaphore       ; Later: Signal parent task
;       subi    p1l, lo8(-2)
;       sbci    p1h, hi8(-2)
        mov     Zh, p1h
        mov     Zl, p1l
        ldi     Xl, BV(IdleBit) ; Mark task dead
        std     Z+PidState, Xl
        mov     p2h, p1h
        mov     p2l, p1l
        ldi     Zh, hi8(RunQueue)
        ldi     Zl, lo8(RunQueue)
        BeginCritical
        rcall   _RemoveObject           ; Attempt to remove from run queue
        rjmp    _Epilog

        _ENDFUNC

        _END


