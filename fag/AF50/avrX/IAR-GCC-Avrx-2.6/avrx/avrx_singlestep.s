#include        "avrx.inc"
/*
        Copyright 1999-2001, Larry Barello
        larry@barello.net

*/
        _MODULE(avrx_singlestep)

        _EXTERN(IntProlog)
        _EXTERN(RunQueue)
        _EXTERN(_QueuePid)
        _EXTERN(_Epilog)

        _CODESECTION
;+
;-----------------------------------------------------------
; unsigned char AvrXSingleStepNext(pProcessID)
;
; Marks a suspended task for single step support
; Jams it on the front of the run queue.  Returns error
; if task is not suspended.
;
; PASSED:       R25:R24 = Pid to single step
; RETURN:       R24 = 1 if task not suspended (error)
; USES:         FLAGS (except T)
;-
        _FUNCTION(AvrXSingleStepNext)

AvrXSingleStepNext:
        AVRX_Prolog

        mov     Zh, p1h
        mov     Zl, p1l
        ldd     Xl, Z+PidState
        sbrs    Xl, SuspendedBit
        rjmp    AssnXErr
        
        sbr     Xl, BV(SingleStep) ; Mark for single stepping
        std     Z+PidState, Xl
        
        lds     Xh, RunQueue+NextH   ; Put task in front of run queue
        lds     Xl, RunQueue+NextL
        sts     RunQueue+NextH, Zh
        sts     RunQueue+NextL, Zl
        std     Z+NextH, Xh
        std     Z+NextL, Xl
        rjmp    AssnXOk
        _ENDFUNC

/*+
; -----------------------------------------------------------
; unsigned char AvrXStepNext(pProcessID)
;
; Unsuspends a task, adds it to the run queue
; then resuspends the task.  
;
; PASSED:       R25:R24 = Pid to single step
; RETURN:       R24 = 1 if task not suspended (error)
; USES:         
;
-*/
        _FUNCTION(AvrXStepNext)

AvrXStepNext:
        AVRX_Prolog

        mov     Zh, p1h
        mov     Zl, p1l
        ldd     Xl, Z+PidState
        sbrs    Xl, SuspendedBit
        rjmp    AssnX
        
        cbr     Xl, BV(SuspendBit) | BV(SuspendedBit)
        std     Z+PidState, Xl
        rcall   _QueuePid               ; Add to run queue
; <BUG BUG - QueuePid trashes Z?, yeah, reloads with R25:R24
        ldd     Xl, Z+PidState
        sbr     Xl, BV(SuspendBit)
        std     Z+PidState, Xl
AssnXOk:
        ldi     r1l, lo8(0)     ; Ok Return
        rjmp    AssnX
AssnXErr:
        ldi     r1l, lo8(1)     ; Err Return
AssnX:
        std     Y+_r1l, r1l
        rjmp    _Epilog

        _ENDFUNC
        _END


