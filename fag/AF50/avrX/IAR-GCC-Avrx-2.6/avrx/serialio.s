/*
File:   Serialio.asm

Copyright �1998 - 2001 Larry Barello

Revision:

1/31/01 - Changed InitSerialIO parameter register to conform to new
        GCC calling conventions (R25 -> R24)

NOTE: This code is not GCC compatible.  In general the Monitor and the
serial routines should run in their own task space and not be called by
any C code.

*/
#include        "avrx.inc"

        _MODULE(serialio)
        _EXTERN(_Epilog)
        _EXTERN(IntProlog)
        _EXTERN(AvrXWaitSemaphore)
        _EXTERN(AvrXIntSetSemaphore)
;
; Needed for USART interrupt
;
;
#define UCR_INIT  lo8((1<<TXEN) | (1<<RXEN) | (1<<RXCIE))
// UBRR_INIT is an external value defined by user application.
//#define UBRR_INIT lo8((CPUCLK)/(16 * BAUDRATE) - 1)

#define CharPushed 7
        _DATASECTION

        _GLOBAL(SioFlags,1)
        _GLOBAL(_RxByte, 1)
        _GLOBAL(RxC_Flag, MtxSz)
        _GLOBAL(TxIntFlag, MtxSz)

        _CODESECTION
;+
;------------------------------------------
; PushChar
;
; Set a flag so that the next call to GetChar just returns.
; in effect pushing the last returned charactor to be retrieved
; by the next routine
;-
        _FUNCTION(PushChar)
PushChar:
        push    Xl
        lds     Xl, SioFlags
        sbr     Xl, 1<<CharPushed
        sts     SioFlags, Xl
        pop     Xl
        ret
        _ENDFUNC
;+
;------------------------------------------
; GetChar
;
; Need to make this draw from a buffer so that multiple
; chars can be pushed... and so different input streams
; can be attached to the buffer.
;-
        _FUNCTION(GetChar)
#ifdef  __IAR_SYSTEMS_ASM__
        PUBLIC  ?C_GETCHAR
?C_GETCHAR:     ; Need this label for simulator I/O
#endif
GetChar:
        lds     tmp0, SioFlags
        sbrs    tmp0, CharPushed
        rjmp    gc00
        cbr     tmp0, 1<<CharPushed
        rjmp    gc01
        sts     SioFlags, tmp0
        rjmp    gc01
gc00:
        ldi     p1h, hi8(RxC_Flag)
        ldi     p1l, lo8(RxC_Flag)
        rcall   AvrXWaitSemaphore       ; Wait for a charactor, or flag from System
gc01:
        lds     r1l, _RxByte
        ret
        _ENDFUNC
;+
;------------------------------------------
; _GetChar
;
; Special version that echos
;
; RETURNS:      RxByte (Register)
;-
        _FUNCTION(_GetChar)

_GetChar:
        rcall   GetChar
        rjmp    PutChar
        _ENDFUNC
;+
;------------------------------------------
;
; PutCR
;
; Put a CR to the output stream
;-
        _FUNCTION(PutCR)
PutCR:
        ldi     p1l, 0x0D
        rjmp    PutChar
        _ENDFUNC
;+
;------------------------------------------
;
; PutSpace
;
; Put a space to the output stream
;-
        _FUNCTION(PutSpace)
        
PutSpace:
        ldi     p1l, ' '
        rjmp    PutChar
        _ENDFUNC
;+
;------------------------------------------
;
; const char * PutString(const char *)
;
; passed:
;       near pointer to string in FLASH memory
; Returns:
;       pointer to next char after NULL
;-
        _FUNCTION(_PrintString)

_PrintString:
        mov     Zl, p1l
        mov     Zh, p1h
_foo:
        lpm
        tst     R0
        breq    _ps00
        mov     p1l, R0
        push    Zl
        push    Zh
        rcall   PutChar
        pop     Zh
        pop     Zl
        adiw    Zl, 1
        rjmp    _foo
_ps00:
        mov     r1l, Zl
        mov     r1h, Zh
        ret
        _ENDFUNC
;+
;-------------------------------------------
; InitSerialIO
;
; PASSED:       R24 = UBBR
;
; History: parameter used to be passed in R25 l.b. 1/31/01
;
        _FUNCTION(InitSerialIO)
InitSerialIO:
        out     UBRR, p1l
        ldi     tmp0, UCR_INIT
        out     UCR, tmp0
        sbi     PORTD, PD0      ; Enable pull up on Rx
        ret
        _ENDFUNC
;
;-----------------------------------------------
; UDRE_Interrupt
;
; Disables the interrupt (UDR is empty) signal waiting
; process to stuff the UDR
;
        _FUNCTION(_uart_data_)
_uart_data_:
        cbi     UCR, UDRIE      ; Disable the interrupt
        
        rcall   IntProlog
        
        ldi     p1l, lo8(TxIntFlag)
        ldi     p1h, hi8(TxIntFlag)
        rcall   AvrXIntSetSemaphore     ; Signal we are ready for more.
        
        rjmp    _Epilog
        _ENDFUNC
        
;
;-----------------------------------------------
; RxC_Interrupt
;
; Reads the USART and flags UserTask, Char In semaphore
;
        _FUNCTION(_uart_recv_)
_uart_recv_:
        cbi     UCR, RXCIE      ; disable interrupt

        rcall   IntProlog      ; Switch to kernel stack
        
        in      Xl, UDR
        sts     _RxByte, Xl

        ldi     p1h, hi8(RxC_Flag)
        ldi     p1l, lo8(RxC_Flag)
        rcall   AvrXIntSetSemaphore

        sbi     UCR, RXCIE      ; Re-enable interrupts
        rjmp    _Epilog
        _ENDFUNC
;+
;------------------------------------------
; PutChar
;
; Passed: p1l "TxByte"
;
; Stuffs "TxByte" into usart.  The usart is double buffered.
; when the data register is full we wait on the semaphore
; until the Usart interrupts indicating the register is ready.
;-
        _FUNCTION(PutChar)
#ifdef __IAR_SYSTEMS_ASM__
        _PUBLIC(?C_PUTCHAR)     ; Enable terminal I/O
?C_PUTCHAR:
#endif
PutChar:
        sbic    USR, UDRE
        rjmp    pc00

        push    p1l
        sbi     UCR, UDRIE      ; Enable interrupts and
        ldi     p1l, lo8(TxIntFlag)
        ldi     p1h, hi8(TxIntFlag)
        rcall   AvrXWaitSemaphore; Wait until data register is ready
        pop     p1l
pc00:
        out     UDR, p1l
        ret
        _ENDFUNC
        
;-----------------------------------------
; _GetHex
;
; PASSED:       p1l = Maximum number of digits
; RETURNS:      r1 = val, Tbit, p2l = Number of digits entered
; USES:         R24, R25
; STACK:        4
;
        _FUNCTION(_GetHex)
_GetHex:
        push    R10
        push    R11
        push    R12
        push    R13
        
        mov     R13, p1l
        clr     R10
        clr     R11
        clr     R12
gh_00:
        rcall   _GetChar
        rcall   IsHex
        brtc    gh_xx
        cpi     r1l, '9'+1
        brge    gh_01
        subi    r1l, '0'
        rjmp    gh_02
gh_01:
        cbr     r1l, 0x20
        subi    r1l, 'A'-10
gh_02:
        lsl     R10
        rol     R11
        lsl     R10
        rol     R11
        lsl     R10
        rol     R11
        lsl     R10
        rol     R11
        add     R10, r1l
        inc     R12
        dec     R13
        brne    gh_00
gh_xx:
        mov     p2l, R12 
        mov     r1l, R10
        mov     r1h, R11
        
        pop     R13
        pop     R12
        pop     R11
        pop     R10
        ret
        _ENDFUNC


;-----------------------------------------
; GetHexWord
;
        _FUNCTION(GetHexWord)

GetHexWord:
        ldi     p1l, 4
        rjmp    _GetHex
        _ENDFUNC
;-----------------------------------------
; GetHexByte
;
        _FUNCTION(GetHexByte)

GetHexByte:
        ldi     p1l, 2
        rjmp    _GetHex
        _ENDFUNC
;-----------------------------------------
; GetHexByte
;
        _FUNCTION(GetHexChar)

GetHexChar:
        ldi     p1l, 1
        rjmp    _GetHex
        _ENDFUNC
;-----------------------------------------
; IsHex
;
; Tests RxByte for ascii hex char
; Returns: T bit
;
        _FUNCTION(IsHex)

IsHex:
        push    p1l
        lds     p1l, _RxByte
        clt
        cpi     p1l, '0'
        brlt    ihno
        cpi     p1l, '9'+1
        brlt    ihyes
        cpi     p1l, 'A'
        brlt    ihno
        cpi     p1l, 'F'+1
        brlt    ihyes
        cpi     p1l, 'a'
        brlt    ihno
        cpi     p1l, 'f'+1
        brge    ihno
ihyes:
        set
ihno:
        pop     p1l
        ret
        _ENDFUNC
;-----------------------------------------
; IsWhite
;
; Tests RxByte for space or CR
; Returns: T bit
;
        _FUNCTION(IsWhite)

IsWhite:
        push    p1l
        lds     p1l, _RxByte
        clt
        cpi     p1l, ' '
        breq    ihyes
        cpi     p1l, 0x0D
        breq    ihyes
        rjmp    ihno
        _ENDFUNC
;-----------------------------------------
; void _PutHex(unsigned val, char digits)
; 
; Passed:       p2 = Value, p1l = # digits
; RETURN:       Nothing
; USES:         R25, TxByte
; STACK:        2
;
        _FUNCTION(_PutHex)

_PutHex:
        ldi     tmp2, 4          ; Maximum number of digits.
        mov     tmp3, p2l
        mov     p2l, p1l
        mov     p2h, p1h
ph00:
        lsl     p2l
        rol     p2h
        rol     p1l
        lsl     p2l
        rol     p2h
        rol     p1l
        lsl     p2l
        rol     p2h
        rol     p1l
        lsl     p2l
        rol     p2h
        rol     p1l

        cp      tmp3, tmp2
        brlt    ph02

        cbr     p1l, 0xF0
        subi    p1l, lo8(-'0')          ; Addi 0
        cpi     p1l, lo8('9' + 1)       ; if > 9
        brlt    ph01                    ; subi 9 and add A
        subi    p1l, lo8('9' + 1 - 'A')
ph01:   
        rcall   PutChar
ph02:   
        dec     tmp2
        brne    ph00

        ret
        _ENDFUNC
;-----------------------------------------
; Put out a hex word
; 
; Passed: Y = word
;
        _FUNCTION(PutHexWord)

PutHexWord:
        ldi     p2l, 4
        rjmp    _PutHex
        _ENDFUNC
;-----------------------------------------
; Put out a two ascii hex byte
; Yl = byte
;
        _FUNCTION(PutHexByte)

PutHexByte:
        ldi     p2l, 2
        rjmp    _PutHex
        _ENDFUNC
        _END

