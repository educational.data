#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <stdlib.h>
#include <stdio.h>

#include "timer.h"
#include "motorsettings.h"
#include "support.h"


#define SPEED_PORTA OCR0
#define SPEED_PORTB OCR2

void set_motor_a_dutycycle(int);
void set_motor_b_dutycycle(int);

MotorSettings *mA;
MotorSettings *mB;

void steer_motors(void);

void init_motors(void) {
	// Make PortC output - using PC0 and PC1 as motor controll;
	DDRC = (uchar)0b00000011;
	// Make PortB output, pwm output on OC0 pin PB4 and OC2 pin PB7
	DDRB = (uchar)0b10010000;

	// OC0 Phase-correct PWM, prescaler 4, clear on up-counting compare
	TCCR0 = (uchar)0x64;
		// OC2 Phase-correct PWM, prescaler 4, clear on up-counting compare
	TCCR2 = (uchar)0x63;
	
	// Trigger INT0 and INT1 on rising edge
	EICRA |= (uchar)0b00001111;
	// Enable INT0 and INT1
	EIMSK |= (uchar)0b00000011;
	
	// setup mA and mB;
	mA = new MotorSettings(&set_motor_a_dutycycle);
	mB = new MotorSettings(&set_motor_b_dutycycle);
	mA->setMotorPower(0);
	mB->setMotorPower(0);
	
	add_timer(steer_motors,50);
}

/**
 * ctor for MotorSettings.
 * 
 * Setup initial input data for controller.
 */
MotorSettings::MotorSettings(void (*setMotorPowerFunction)(int power)) {
	setMotorPower = setMotorPowerFunction;
	setMotorPower(0);
	power = 0;
	speed = 0;
	steps = 0;
	dState = 0;
	iState = 0;
	cStep = 0;
	
	for(int p = 0;p<8;++p) {
		xSteps[p] = 0;
	}
	
	//
	// pid regulation
	//
	pid.I = -10;
	pid.D = 0;
	pid.P = -10;
	PID = 0;
	
	wantedSpeed = 0;
	add_timer(timer);
	
}



int step_a_diff;
int step_b_diff;
int step_b;
int step_a;

void move(int a, int b) {
	step_b = mB->steps+b;
	step_a = mA->steps+a;
	
	step_a_diff = step_a - mA->steps;
	step_b_diff = step_b - mB->steps;
}

void steer_motors() {
	
	int dif = step_a_diff - step_b_diff;
	int sum = step_a_diff + step_b_diff;
	sum/=2;
	
	// avoid calc err.
	if(sum < 0) sum = 100;
	
	dif*=80;
	dif/=sum;
	
	int speedA = 50+step_a_diff - dif;
	int speedB = 50+step_b_diff + dif;
	
	speedA = speedA<100 ? speedA : 100;
	speedB = speedB<100 ? speedB : 100;
	
	int powA = step_a_diff > 0 ? speedA : 0;
	int powB = step_b_diff > 0 ? speedB : 0;
	
	powA = powA > -100 ? powA : -100;
	powB = powB > -100 ? powB : -100;

	//printf("b_s: %d b_sd: %d b_cs: %d \n",step_b,step_b_diff,mB->steps);
	//printf("a_s: %d a_sd: %d a_cs: %d \n",step_a,step_a_diff,mA->steps);
	
	step_a_diff = step_a - mA->steps;
	step_b_diff = step_b - mB->steps;
	static bool isRunning;
	if( powA == 0 && powB == 0){
		
		if(isRunning == true) { 
			isRunning = false;
			//printf("<<cut>>");
			
			//mA->setMotorPower(-60);
			//mB->setMotorPower(-60);
			//vent(100);
			
			
		}
		mA->setMotorPower(0);
		mB->setMotorPower(0);
		return;
	} //else printf("A:%d B: %d D: %d\n",powA,powB,dif);
	isRunning = true;
	/*
	if( powA == 0 ) {
		powA = -30;
		powB+=40;
	}
	if( powB == 0 ) {
		powB = -30;
		powA+=40;
	}*/
	mA->setMotorPower(powA);
	mB->setMotorPower(powB);
}
/*
* External interrupt 0 (INT0) handler.
* Triggered by the speed sensor (when the wheel has turned 1/8th of a revolution).
*/
SIGNAL (SIG_INTERRUPT0)
{
	//mB->calc_speed();
		
	// add counted steps
	++(mB->steps);
	
	//printf("b");
	
	// invert input
	//cli();
	EICRA ^= 0b00000001;
	//sei();
	
	// Clear interrupt (done to remove a few false readings)
	EIFR = 0x01;
}

/*
* External interrupt 1 (INT1) handler.
* Triggered by the speed sensor (when the wheel has turned 1/8th of a revolution).
*/

SIGNAL (SIG_INTERRUPT1)
{
	//mA->calc_speed();
	
	// add counted steps
	++(mA->steps);
	
	//printf("a");
	
	// inverter for at sikre sig at skiftende flanke bliver detekteret.
	//cli();
	EICRA ^= 0b00000100;
	//sei();

	
		
	// Clear interrupt (done to remove a few false readings)
	EIFR = 0x01;
}

/***************************************
 ** sets the perifal H-bridge duty cycle
 **
 ** speed from 0 to 255 (complete)
 ** forward marks direction
 ***************************************/
void set_motor_a_dutycycle(int power) {
	// convert input to power settings

	const int maxPower = 255;
	bool dir = power > 0 ? true : false;
	power = power > 0 ? power : power * -1;
	power = power > maxPower ? maxPower : power;
	
	// mark the engine direction
	if(dir == false) {
		PORTC &= 0b11111101;
	} else {
		PORTC |= 0b00000010;
	}
	
	// set speed
	SPEED_PORTA = (uchar)(power);
	
}

/***************************************
 ** sets the perifal H-bridge duty cycle
 **
 ** speed from 0 to 255 (complete)
 ** forward marks direction
 ***************************************/
void set_motor_b_dutycycle(int power) {
	// convert input to power settings
	const int maxPower = 255;
	bool dir = power > 0 ? true : false;
	power = power > 0 ? power : power * -1;
	power = power > maxPower ? maxPower : power;
	
	// mark the engine direction
	if(dir == true) {
		PORTC &= 0b11111110;
	} else {
		PORTC |= 0b00000001;
	}
	
	// set speed
	SPEED_PORTB = (uchar)(power);
}



