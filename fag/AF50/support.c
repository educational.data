#include "support.h"
#include <avr/io.h>
#include <avr/signal.h>
#include <stdlib.h>
#include <stdio.h>

void vent(long time) {
	for( long tid = time; tid>0; --tid ) {
		for( long t = 100000; t > 0; --t ) { }
	}
}



void *operator new(unsigned int size) throw() {
	//printf("s: %d\n",size);
	void *pt = malloc(size);
	//printf("object offset: %08x\n",pt);
	return pt;
}
