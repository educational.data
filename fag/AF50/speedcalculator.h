#ifndef __SPEED_CALCULATOR_H__INCLUDED__
#define __SPEED_CALCULATOR_H__INCLUDED__

#include "timer.h"

class SpeedCalculator {
	volatile ulong lastSpeed;
	
	volatile ulong accel;
	
	volatile ulong tickBuf[4];
	volatile ulong lastTick;
	volatile ulong p;
	volatile ulong curBuf;
	volatile int oldspeed;
public:
	SpeedCalculator ();

	void doTick();
	
	void incTick();
	
	long getSpeed();
	
	int getAccel() { return accel; }
};

#endif
