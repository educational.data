
void moveSteps(int a, int b) {
	//
	// log starting pose
	//
	int step_b = mB->steps+b;
	int step_a = mA->steps+a;
	
	int step_a_diff = step_a - mA->steps;
	int step_b_diff = step_b - mB->steps;
	
	//printf("b_s: %d b_sd: %d b_cs: %d \n",step_b,step_b_diff,mB->steps);
	//printf("a_s: %d a_sd: %d a_cs: %d \n",step_a,step_a_diff,mA->steps);
	
	while(step_a_diff>0 || step_b_diff>0) {
		int speedA = 50+step_a_diff/3;
		int speedB = 50+step_b_diff/3;
		
		speedA = speedA<100 ? speedA : 100;
		speedB = speedB<100 ? speedB : 100;
		
		int powA = step_a_diff > 0 ? speedA : 0;
		int powB = step_b_diff > 0 ? speedB : 0;
		//printf("b_s: %d b_sd: %d b_cs: %d \n",step_b,step_b_diff,mB->steps);
		//printf("a_s: %d a_sd: %d a_cs: %d \n",step_a,step_a_diff,mA->steps);
		
		step_a_diff = step_a - mA->steps;
		step_b_diff = step_b - mB->steps;
		
		
		//printf("A: %d\n",powA);
		//printf("B: %d\n",powB);
		
		if( powA == 0 ) {
			powA = -30;
			powB+=40;
		}
		if( powB == 0 ) {
			powB = -30;
			powA+=40;
		}
		mA->setMotorPower(powA);
		mB->setMotorPower(powB);
		
		
	}
	//printf(">>cut<<");
	mA->setMotorPower(-80);
	mB->setMotorPower(-80);
	vent(100);
	
	mA->setMotorPower(0);
	mB->setMotorPower(0);
}


// translate input data to speed.
void MotorSettings::calc_speed() {
	//
	// lenght of a single period
	// 8 steps.
	//
	const int stepLen=8000*(361/72); // cm pr. steps
	
	//
	// read and swap in new timer periods.
	//
	int stamp = timer.tick; // stamp contains the current time.
	int tid = xSteps[cStep] - stamp; // tid contains the time 8 steps ago.
	xSteps[cStep++] = stamp; // now we insert the current time in the old field.
	
	//
	// makes sure it cycles
	//
	if(cStep==8) cStep = 0;
	
	//
	// calculate the actual speed
	//	
	int tspeed = (int)(stepLen/tid);
	//printf("speed: %d\n",tspeed);
	
	//
	// here we try to evaluate if we are stopped....
	// it will be changed!!!! <-- TODO
	//
	if(tspeed <= 1) stopped = true;
	else stopped = false;
	
	// reuse last calculated speed in calculation, but make sure the newest is wighted most.
	if(moving_forward == false) {
		tspeed*=-1;
	}
	
	speed = tspeed;
}

#define MAX_POWER 150

// calculate the needed power
void MotorSettings::regulate_power_by_speed() {

	const int maxDev = 5;
	
	//
	// calculate error
	//
	int err = (speed-wantedSpeed);
	
	//
	// rate of change
	//
	int dev = (speed - dState);
	if(dev>maxDev)dev=maxDev;
	if(dev<-maxDev)dev=-maxDev;
	
	//
	// find the direction of the derivitate.
	//
	char dir=1;
	if(dev<0)dir= (char)-1;
	
	//
	// calculate PID 
	// without using float...
	//
	power = 	
		pid.P * err + 				// p-reg
		pid.I * iState + 			// i-reg
		pid.D * dev * dev * dir;	// d-reg
	
	power/=100;
	
	if(power > 255)power = 255;
	if(power < -255)power = -255;
	
	//printf("p:%3d\n",power);
	
	//
	// save last error
	//
	iState *= 4;
	iState += power;
	iState /= 5;
	
	dState = speed;
	
	
	if(stopped == true && power < 0) 
		moving_forward=false;
	else
		moving_forward=true;
	
	if(power > MAX_POWER) power = MAX_POWER;
	if(power < 0) power = 0;
	
	setMotorPower(power);
}



void trySpeed(void) {
	while(1) {
		mA->setMotorPower(0);
		mB->setMotorPower(0);
		vent(1000);
		int len = 0;
		int wSpeed = 0;
		while(len == 0) {
			printf("\nloop for how many times? >");
			scanf("%d",&len);
			if(len == 0) {
				int i;
				int p;
				int d;
				printf("\nEnter P value: (perhaps 10) >");
				scanf("%d", &p);
				
				printf("\nEnter I value: (perhaps 0) >");
				scanf("%d", &i);
				
				printf("\nEnter D value: (perhaps 10) >");
				scanf("%d", &d);
				mA->pid.I = mB->pid.I = i;
				mA->pid.P = mB->pid.P = p;
				mA->pid.D = mB->pid.D = d;
			}
		}
		
		printf("\nspeed? > ");
		scanf("%d",&wSpeed);
		
		
		mA->power = 0;
		mB->power = 0;
		mB->wantedSpeed = wSpeed;
		mA->wantedSpeed = wSpeed;
		mA->moving_forward= true;
		mB->moving_forward= true;
		int waitUntilGo;
		printf("\nAntal sekunder f�r der skal k�res? > ");
		scanf("%d",&waitUntilGo);
		
		printf("\n");
		
		vent(waitUntilGo*1000);
		
		mB->calc_speed();
		mA->calc_speed();
		mB->regulate_power_by_speed();
		mA->regulate_power_by_speed();
		
		for(int forloop = 0;forloop<len;++forloop) { 
			printf("\nSpeed b: %5d\n", mB->speed);
			printf("power b: %5d\n", mB->power);
			printf("PID b: %5d\n", mB->PID);
			
			printf("Speed a: %5d\n", mA->speed);
			printf("power a: %5d\n", mA->power);
			printf("PID a: %5d\n\n", mA->PID);
			for(int loop=0;loop < 10;++loop) {
				mB->regulate_power_by_speed();
				mA->regulate_power_by_speed();
			
				vent(100);
			}
			
		}

	}
}


void steer_motors() {
	
	int dif = step_a_diff - step_b_diff;
	int sum = step_a_diff + step_b_diff;
	sum/=2;
	
	// avoid calc err.
	if(sum < 0) sum = 100;
	
	dif*=80;
	dif/=sum;
	
	int speedA = 50+step_a_diff - dif;
	int speedB = 50+step_b_diff + dif;
	
	speedA = speedA<100 ? speedA : 100;
	speedB = speedB<100 ? speedB : 100;
	
	int powA = step_a_diff > 0 ? speedA : 0;
	int powB = step_b_diff > 0 ? speedB : 0;
	
	powA = powA > -100 ? powA : -100;
	powB = powB > -100 ? powB : -100;

	//printf("b_s: %d b_sd: %d b_cs: %d \n",step_b,step_b_diff,mB->steps);
	//printf("a_s: %d a_sd: %d a_cs: %d \n",step_a,step_a_diff,mA->steps);
	
	step_a_diff = step_a - motorThis->motor_a.steps;
	step_b_diff = step_b - motorThis->motor_b.steps;
	static bool isRunning;
	if( powA == 0 && powB == 0){
		
		if(isRunning == true) { 
			isRunning = false;
			//printf("<<cut>>");
			
			//mA->setMotorPower(-60);
			//mB->setMotorPower(-60);
			//vent(100);
			
			
		}
		motorThis->motor_a.setMotorPower(0);
		motorThis->motor_b.setMotorPower(0);
		return;
	} //else printf("A:%d B: %d D: %d\n",powA,powB,dif);
	isRunning = true;
	/*
	if( powA == 0 ) {
		powA = -30;
		powB+=40;
	}
	if( powB == 0 ) {
		powB = -30;
		powA+=40;
	}*/
	motorThis->motor_a.setMotorPower(powA);
	motorThis->motor_b.setMotorPower(powB);
}