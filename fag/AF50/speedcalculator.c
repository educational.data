#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>

#include "support.h"
#include "speedcalculator.h"
#include "processor.h"
#include <stdio.h>

//#define _DEBUG_

#ifdef _DEBUG_
#warning Debug "on" in speedcalculator.c

#endif


SpeedCalculator *pSpeedA = 0;
SpeedCalculator *pSpeedB = 0;

uint curTickPos = 0;

void increment_pos_tick();

SpeedCalculator::SpeedCalculator() {
	disable_interrupts();
	static bool bIsInit = false;
	
	accel = 0;
	p=0;
	curBuf = 0;
	
	for(int t = 0;t<4;++t)
		tickBuf[t] = ( uint ) 0;
	
	if(!bIsInit) {
		// Trigger INT0 and INT1 on rising edge
		EICRA |= (uchar)0b00001111;
		// Enable INT0 and INT1
		EIMSK |= (uchar)0b00000011;
		
		
		printf("Init SpeedCalculator A\n");
		pSpeedA = this;
		
		bIsInit = true;
		
	} else { 
		pSpeedB = this;
		uP.timer.addTimer(increment_pos_tick,20);
		printf("Init SpeedCalculator B\n");
	}
}

void increment_pos_tick() {
	pSpeedB->incTick();
	pSpeedA->incTick();
}

void SpeedCalculator::incTick() {
	tickBuf[p] = curBuf;
	curBuf = 0;
	++p;
	if(p == 4) {
		p=0;
	}
}

long SpeedCalculator::getSpeed() {
	int dist = tickBuf[0] + tickBuf[1]; + tickBuf[2] + tickBuf[3];
	dist *= 5;
	
	int speed = dist;
	
	accel = speed - oldspeed;
	oldspeed = speed;
#ifdef _DEBUG_
	if(pSpeedA == this)
		printf("speed A: %d\n",speed);
	else
		printf("speed B: %d\n",speed);
#endif
	return speed;
}
void SpeedCalculator::doTick() {
	++curBuf;
}

/*
* External interrupt 0 (INT0) handler.
* Triggered by the speed sensor (when the wheel has turned 1/8th of a revolution).
*/
SIGNAL (SIG_INTERRUPT0)
{
	pSpeedB->doTick();
#ifdef _DEBUG_
	//printf("b");
#endif
	
	// flick to make sure that it tricks on other flank
	EICRA ^= 0b00000001;
	
	// Clear interrupt (done to remove a few false readings)
	EIFR = 0x01;
}

/*
* External interrupt 1 (INT1) handler.
* Triggered by the speed sensor (when the wheel has turned 1/8th of a revolution).
*/

SIGNAL (SIG_INTERRUPT1)
{
	pSpeedA->doTick();
#ifdef _DEBUG_
	//printf("a");
#endif
	
	// inverter for at sikre sig at skiftende flanke bliver detekteret.
	EICRA ^= 0b00000100;
	
	// Clear interrupt (done to remove a few false readings)
	EIFR = 0x01;
}
