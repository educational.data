#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <stdlib.h>
#include <stdio.h>

#include "timer.h"
#include "processor.h"
#include "support.h"


#define SPEED_PORTA OCR0
#define SPEED_PORTB OCR2

//#define _DEBUG_

void set_motor_a_dutycycle(int);
void set_motor_b_dutycycle(int);

/**
 * ctor for motorsettings
 *
 * decides the engine to use for control.
 */
MotorSettings::MotorSettings(int id) {
	printf("Init MotorSettings ID: %d\n",id);
	if(id == 0)  {
		setMotorPower = set_motor_a_dutycycle;
		speedSensor = &(uP.speedSensorRight);
	}
	else {
		setMotorPower = set_motor_b_dutycycle;
		speedSensor = &(uP.speedSensorLeft);
	}
	this->id = id;
	reset();
}

/**
 * ctor for MotorSettings.
 * 
 * Setup initial input data for controller.
 */
MotorSettings::MotorSettings(void (*setMotorPowerFunction)(int power)) {
	setMotorPower = setMotorPowerFunction;
	reset();
}

void MotorSettings::reset() {
	setMotorPower(0);
	
	wantedSpeed = 0;
}


void MotorSettings::calc_speed() {
	int speed = speedSensor->getSpeed();
	
	int dir = wantedSpeed < 0 ? -1 : 1;
	int err = abs(wantedSpeed) - speed; 
#ifdef _DEBUG_
	//printf("speed: %d\n",speed);
#endif
	const int p = 200;
	const int i = 20;
	const int d = 0;

	err = err > 20 ? 20: err;
	err = err < -20 ? -20: err;
	
	long pid = 0;

	if(iState > 25500) iState = 25500;
	if(iState < -8000) iState = -8000;
	
	long pState = err * p;
	iState += err * i;
	int dState = speedSensor->getAccel() * d;
	
	pid = pState + iState + dState;
	
	
	//pid = pid >100 ? 100 : pid;
	pid = pid < -8000 ? -8000 : pid;
	pid = pid >  25500 ?  25500 : pid;
	
	if(err == 0 && speed == 0) 
		pid = 0;
	
#ifdef _DEBUG_	
	printf("pidB: %3d\n",pid/100);
#endif
	setMotorPower(dir*pid/100);
	//setMotorPower(wantedSpeed*3);
}

/***************************************
 ** sets the perifal H-bridge duty cycle
 **
 ** speed from 0 to 255 (complete)
 ** forward marks direction
 ***************************************/
void set_motor_a_dutycycle(int power) {
	// convert input to power settings

	const int maxPower = 255;
	bool dir = power > 0 ? true : false;
	
	power = power > 0 ? power : power * -1;
	power = power > maxPower ? maxPower : power;
	
	// mark the engine direction
	if(dir == true) {
		PORTC &= 0b11111101;
	} else {
		PORTC |= 0b00000010;
	}
	// set speed
	SPEED_PORTA = (uchar)(power);
	
}

/***************************************
 ** sets the perifal H-bridge duty cycle
 **
 ** speed from 0 to 255 (complete)
 ** forward marks direction
 ***************************************/
void set_motor_b_dutycycle(int power) {
	// convert input to power settings
	const int maxPower = 255;
	bool dir = power > 0 ? true : false;
	power = power > 0 ? power : power * -1;
	power = power > maxPower ? maxPower : power;
	
	// mark the engine direction
	if(dir == true) {
		PORTC &= 0b11111110;
	} else {
		PORTC |= 0b00000001;
	}
	// set speed
	SPEED_PORTB = (uchar)(power);
}
