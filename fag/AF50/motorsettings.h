#ifndef __MOTORSETTINGS_H__INCLUDED__
#define __MOTORSETTINGS_H__INCLUDED__

#include "timer.h"
#include "support.h"

class SpeedCalculator;

void init_motors(void);
void test_motor(void);
void move(int, int);

class MotorSettings {
	void reset();
public:
	struct PID_settings {
		int P;
		int I;
		int D;
	} pid;
	
	void (*setMotorPower)(int power);

	MotorSettings(void (*setMotorPowerFunction)(int power));
	MotorSettings(int id);
	
	SpeedCalculator *speedSensor;
	
	void calc_speed();
	
	int id;
	
	// filled in settings
	int power;
	int speed;
	
	int steps;
	int xSteps [8];
	int cStep;
	
	// set the regulated power
	int wantedSpeed;
	
	// marker flag to say if we are stopped
	bool stopped;
	
	// marker flag to say if we are moving backwards
	bool moving_forward;
	
	//  PID data
	int PID;
	
	long iState;
	
	// Timers for speed calculation;
	TimerValue timer;
	
	void regulate_power_by_speed();
};

void moveSteps(int a, int b);

extern MotorSettings *mA;
extern MotorSettings *mB;

#endif
