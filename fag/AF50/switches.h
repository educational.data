#ifndef __SWITCHES_H__INCLUDED__
#define __SWITCHES_H__INCLUDED__

#include "support.h"

class Switches {
public:
	Switches();
	
	uchar getButs();
	bool isStartPressed() {
		bool isPressed = (getButs() & 0b00001000);
		return isPressed;
	}
	
	uchar getDip() {
		return (getButs() & 0b11110000) >> 4;
	}
};

#endif
