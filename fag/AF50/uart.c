/*
* Includes
*/
#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <stdio.h>

#include "support.h"
#include "uart.h"

/*
* Initialize UART1.
*/

uart1 *__uart;

int u_put_char(char c);
int u_get_char(void);

uart1::uart1()
{
	UCSR1A = 0b00000010;	// Enable doublespeed trans. mode
	UCSR1B = 0b00011000;	// Enable UART1 TX and RX
	UCSR1C = 0b00000110;	// 8N1 mode
	UBRR1H = 0x00;
	UBRR1L = 0x33;			// 38,4kHz (x2 mode)
	//UBRR1L = 0x11;			// 115.2kHz (x2 mode)
	
	// Register uart_putchar() and uart_getchar() for stdout
	fdevopen(u_put_char, u_get_char, 0);
	
	// Infinite loop for testing purposes
	__uart = this;
}

int u_put_char(char c) {
	vent(1);
	return __uart->put_char(c);
}

int u_get_char(void) {
	return __uart->get_char();
}

/*
* Write a char on the UART.
*/
int uart1::put_char(char c)
{
	// Convert newlines
	if (c == '\n')
		put_char('\r');
	
	// Wait for empty TX register
	loop_until_bit_is_set(UCSR1A, UDRE);
	
	// Write char
	UDR1 = c;

	return 0;
}

/*
* Read a char from the UART.
*/
int uart1 :: get_char(void)
{
	unsigned char c;
	
	// Wait for received char
	loop_until_bit_is_set(UCSR1A,RXC);
	
	// Read char
	c = UDR1;
	
	// Echo char back to the terminal
	put_char(c);

	// Return the char
	return(c);
}

