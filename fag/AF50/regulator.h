#ifndef __REGULATOR_H__INCLUDED__
#define __REGULATOR_H__INCLUDED__

#include "motorsettings.h"
#include "speedcalculator.h"

struct IRegulator {
	virtual void regulateMotor(int a, int b) { }
};

struct MotorRegulator {
	int wantedSpeed;
	long iState;
	SpeedCalculator *speedSensor;
	MotorSettings motor;
	MotorRegulator();
	void calc_speed();
};

struct Regulator : IRegulator {
	static Regulator *instance;
	class MotorRegulator regulatorA;
	class MotorRegulator regulatorB;
public:
	Regulator (	MotorSettings &rightMotor, 
				MotorSettings &leftMotor,
				SpeedCalculator &rightSensor,
				SpeedCalculator &leftSensor);
				
	
	void regulateMotor(int a, int b);
};

#endif
