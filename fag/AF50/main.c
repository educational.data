#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <avr/delay.h>


#include "adc.h"
#include "motorsettings.h"
#include "timer.h"
#include "uart.h"
#include "support.h"
#include "processor.h"
#include "route.h"

//#define _DEBUG_
//#define _DEBUG2_
#ifdef _DEBUG_
# warning Debug "on" in main.c

#endif

#include "stdio.h"

 
int line = 0;



int main(void) {
	enable_interrupts();
	//
	// tell the regulators to start
	//
	//uP.motor.start();	
	route *router;
	while(uP.buttons.isStartPressed());
	switch(uP.buttons.getDip()) {
	case 0:
		router = new route(100,-45,100,0,2000,10);
		break;
		
	case 1:
		router = new route(45,-105,25,0,0,0);
		break;
		
	case 2:
		router = new route(100,-45,15,100,1000,10);
		break;
	case 15:
		router = new route(100,-3,0,0,500,10);
		break;
	default:
		router = 0;
	}
	
	// 
	// Startup line...
	//
	if(router)router->waitForLine();
	
	//
	// enter the actual line cyclus
	//
	if(router)uP.timer.addTimer(router);
    while(1);
	return 0;
}


/////////////////////////////////////////////////
// Fatel fall back routine.
//
// if this calls, the system is fucked!!!!!!
/////////////////////////////////////////////////
SIGNAL(__vector_default) {
#ifdef _DEBUG_
	printf("ERROR\n");
#endif
}

