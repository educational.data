#ifndef __TIMER_H__INCLUDED__
#define __TIMER_H__INCLUDED__

#include "support.h"

class TimerValue {
public:
	unsigned int tick;
};

typedef void (*timerFunc)();

class TimerClass {
	int timeoutMax;
	uint pTick;
protected:
	TimerClass(int);
public:
	void tick();
	virtual void execute() { };
	
	TimerClass *next;
};

//
// class to hold a function that will be called at "timeout" interval
//
class TimerFunc : public TimerClass {
	timerFunc func;
public:
	TimerFunc(timerFunc theFunc, const int timeout);
	
	void execute();
};

class MainTimer {
private:
	TimerClass *top;
	TimerClass *last;
	volatile uint curTick;
public:
	MainTimer();
	
	uint getCurrentTick();
	void addTimer(timerFunc func, int timeout);
	void addTimer(TimerClass *func);
	/**
	 * increment all timers
	 */
	void increment_timers();
	
private:
	/**
	* insert timer in list
	*/
	void insert(TimerClass *);
};
#endif
