#ifndef __MOTOR_H__INCLUDED__
#define __MOTOR_H__INCLUDED__

#include "timer.h"
#include "support.h"
#include "motorsettings.h"


class Motor {
public: 
	Motor();
	void move(int,int);
	
	MotorSettings motor_a;
	MotorSettings motor_b;
	
	void regulatePower(int a, int b);
	void start();
};


#endif
