#ifndef __PROCESSOR_H_INCLUDED__
#define __PROCESSOR_H_INCLUDED__

#include "support.h"
#include "adc.h"
#include "timer.h"
#include "motor.h"
#include "uart.h"
#include "speedcalculator.h"
#include "switches.h"

class Processor {
public: 
	//
	// funcs
	//
	Processor();
	void DummyStart() volatile;
	
public:
	//
	// vars
	//
	// Timers
	uart1 uart;
	
	MainTimer timer;

	ADConverter adc;

	SpeedCalculator speedSensorRight;
	SpeedCalculator speedSensorLeft;
	
	// Motor controllers
	Motor motor;
	Switches buttons;
	};

extern Processor uP;
#endif
