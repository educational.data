#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <stdlib.h>
#include <stdio.h>

#include "timer.h"
#include "support.h"
#include "processor.h"

/**
 * ctor for timers.
 * It sets up the different timer registers.
 */

volatile MainTimer *pTimer = 0;

MainTimer::MainTimer() {
	disable_interrupts();
	/*
	* Setup timer 3.
	*/
	
	// Reset counter
	TCNT3 = 0;

	// Setup compare register
	OCR3A = 62; // magic number...

	// Setup Counter Control 3 register A
	TCCR3A = 0b00000000;	// Compare output mode - not used, reset just in case

	// Setup Counter Control 3 register C
	TCCR3C = 0b00000000;	// Force Output Compare - not used, reset just in case
	
	// Enable timer 3 compare A interrupt.
	ETIMSK |= 0b00010000;

	// Setup Counter Control 1 register B
	TCCR3B = 0b00001011;
	//	Bit 7,6 = 0: Input Capture stuff - Unused
	//	Bit 5 = 0: Reserved
	//	Bit 4,3 = 0,0: 10-bit Phase Correct PWM
	//	Bit 2,1,0 = 0,0,1: Set timer prescaler to 1
	//		Prescaler values are
	//			1,1,1 = ext clk		(f = ?)
	//			1,1,0 = ext clk		(f = ?)
	//			1,0,1 = 1024		(f = 15.625KHz)
	//			1,0,0 = 256			(f = 62.5KHz)
	//			0,1,1 = 64			(f = 250KHz)
	//			0,1,0 = 8			(f = 2MHz)
	//			0,0,1 = 1			(f = 16MHz)		(no scaler)
	//			0,0,0 = No clock	(f = 0)			(disable timer)
	
	// Setup Counter Control 1 register C
	TCCR1C = 0b00000000;	// Force Output Compare - not used, reset just in case
	
	// Enable timer 1 overflow interrupt.
	TIMSK |= 0b00000100;
	
	top = 0;
	last = 0;
	pTimer = this;
	
	printf("Init timer\n");
	
}

TimerClass::TimerClass(const int timeout) {
	timeoutMax = timeout;
	pTick = timeout;
	
	next = 0;
}
TimerFunc::TimerFunc(timerFunc theFunc, const int timeout) : TimerClass(timeout) {
	func = theFunc;
}

void TimerClass::tick() {
	if(pTick-- == 0) {
		execute();
		pTick = timeoutMax;
	}
}

void TimerFunc::execute() {
	(*func)();
}
/**
* insert timer in list
*/
void MainTimer::insert(TimerClass *data) {
	//printf("top: %08x\n",top);
	
	if(top == 0) {
		//printf("start %08x\n",(void*)&top);
		top = data;
	} else {
		last->next = data;
	}
	
	//printf("this: %08x\n", this);
	last = data;
}

void MainTimer::increment_timers() {
	TimerClass *cur = (TimerClass *)top;
	//
	// increment the global tick counter;
	//
	++curTick;
	
	//printf("last: %08x\n",pTimer->last);
	
	while(cur) {
		cur->tick();
		cur = cur->next;
	}
}

uint MainTimer::getCurrentTick() {
	return curTick;
}

void MainTimer::addTimer(timerFunc func, int timeout) {
	TimerClass *t = new TimerFunc(func,timeout);
	insert(t);
}

void MainTimer::addTimer(TimerClass * t) {
	insert(t);
}
	
//
// Output compare 3 match interrupt handler.
// 
// This is generated from a timer compare match.
// The timer overflow counter FRTimerOverflow is added by the value
// of the output compare register at each compare match.
// A few of these interrupts are actually processed in the
// calc_Speed function.
//
INTERRUPT (SIG_OUTPUT_COMPARE3A)
{
	static unsigned int cnt = 0;
	
	++cnt;
	
	if(cnt == 4) {
		cnt = 0;
		((MainTimer *)pTimer)->increment_timers();
	}
		
	// Clear interrupt (done to remove a few false readings)
	EIFR = 0x01;
}

