#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>
#include <stdlib.h>
#include <stdio.h>

#include "adc.h"
#include "processor.h"

#define _DEBUG2_

ADConverter::ADConverter() {
	//
	// select internal ref of 2.56 voltage
	//
	ADMUX = 0b01000000; //0b01000000;//(1<<REFS0); 
	ADCSR |= ((1<<ADEN) | (1<<ADPS2) | (1<<ADPS1));
}

int ADConverter::getValue(char port) {
	int value; 
	
	ADMUX &= 0xF0; 
	
	//
	// Select multiplexer port to read from
	//
	ADMUX |= port & 0b00000111; 
	ADCSR |= (1<<ADSC); 
	
	//
	// wait until end of conversion
	//
	while((ADCSR&(1<<ADSC))); 
	
	//
	// read value (in right order)
	//
	value = ADCL; 
	value += (ADCH*256); 
	
	return value;
}

void ADConverter::sampleSensors() {
	for(int port=0;port<8;port++) {
		int val = portval[port]=(1000 - getValue(port));
		if(val > max) {
			maxid=port;
			max=val;
		}
		min = val < min ? val : min;
		
#ifdef _DEBUG_
	printf("%d  ",val);
#endif

#ifdef _DEBUG2_		
		printf("Port %d: %3d",port,val);
		for(int p=0;p<val/20;p++)
			uP.uart.put_char('-');
			
		printf("\n");
#endif
	}
#ifdef _DEBUG_
	printf(" :%d: ",maxid);
#endif
	
}

void ADConverter::findMinMax() {
	min = 3000;
	max = 0;
	maxid = 0;
	sampleSensors();
}

int ADConverter::findLine() {
	findMinMax();
	// read data from all ports
	int y0,y1,y2;
	int result;
	// if(portval[maxid]<portval[maxid+1] && maxid < 7) maxid ++;
	// if(portval[maxid]<portval[maxid-1] && maxid > 0) maxid ++;
	if(maxid == 0) {
		y1 = portval[maxid];
		y2 = portval[maxid+1];
		y0 = y2;
	} else if(maxid == 7) {
		y0 = portval[maxid-1];
		y1 = portval[maxid];
		y2 = y0;
	} else {
		y0 = portval[maxid-1];
		y1 = portval[maxid];
		y2 = portval[maxid+1];
	}
	
	int x0 = maxid-1;
	int x1 = x0+1;
	int x2 = x1+1;
	
	long a = (x0*x0*(y1-y2) - 
		x1*x1*(y0-y2) + 
		x2*x2*(y0-y1));
	long b = (2*(x0*(y1-y2) - 
		x1*(y0-y2) + 
		x2*(y0-y1)) );
	//
	// save the result of line lookup
	//
	result = 100*a/b;
	
	
	//if(result > 690) result= 700;
	//if(result < 10) result = 10;
	//
	// determine how confident we found the line currently
	//
	PowerOfLine=max-min;
#ifdef _DEBUG_
	printf(" p: %d",PowerOfLine);
    printf(" l: %d\n",result-375);
#endif

	return result-375;
}
