#ifndef __ADC_H_INCLUDED__
#define __ADC_H_INCLUDED__

class ADConverter {
	int getValue(char value);
	void sampleSensors();
	void findMinMax();
	
	int portval[8];
	int min;
	int max;
	int maxid;
public:
	ADConverter(void);
	
	int PowerOfLine;
	
	int findLine();
};

#endif
/*

void init_adc(void) 
{ 
	ADMUX = (1<<REFS0); 
	ADCSR |= ((1<<ADEN) | (1<<ADPS2) | (1<<ADPS1)); 
} 

int get_adc_value(char adc) 
{ 
	int value; 
	ADMUX &= 0xF0; 
	ADMUX |= adc; 
	ADCSR |= (1<<ADSC); 
	while((ADCSR&(1<<ADSC))); 
	value = ADCL; 
	value += (ADCH*256); 
	return value;
}
*/
