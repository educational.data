#include <inttypes.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/signal.h>

#include "motor.h"
#include "processor.h"
#include "support.h"

//#define _DEBUG_

#ifdef _DEBUG_
#include <stdio.h>
#warning Debug mode "on" in motor.c
#endif



void calc_speed();
static Motor *motorThis;

Motor::Motor() : motor_a(0), motor_b(1) {
	// Make PortC output - using PC0 and PC1 as motor controll;
	DDRC = (uchar)0b00000011;
	// Make PortB output, pwm output on OC0 pin PB4 and OC2 pin PB7
	DDRB = (uchar)0b10010000;

	// OC0 Phase-correct PWM, prescaler 4, clear on up-counting compare
	TCCR0 = (uchar)0x64;
		// OC2 Phase-correct PWM, prescaler 4, clear on up-counting compare
	TCCR2 = (uchar)0x63;
	
	
	motor_a.setMotorPower(0);
	motor_b.setMotorPower(0);
	
	motorThis = this;
}

void Motor::start() {
	uP.timer.addTimer(calc_speed,20);
}

static int step_a_diff;
static int step_b_diff;
static int step_b;
static int step_a;


void Motor::move(int a, int b) {
	step_b = motor_a.steps+b;
	step_a = motor_b.steps+a;
	
	step_a_diff = step_a - motor_a.steps;
	step_b_diff = step_b - motor_b.steps;
}

void Motor::regulatePower(int a, int b) {
	//
	// simply transfer power to engine
	// no regulation (yet)
	//
	// motor_a.wantedSpeed = a;
	// motor_b.wantedSpeed = b;
	motor_a.setMotorPower(a*3);
	motor_b.setMotorPower(b*3);
}


void calc_speed() {
	disable_interrupts();
	uP.motor.motor_a.calc_speed();
	uP.motor.motor_b.calc_speed();
	enable_interrupts();
}
