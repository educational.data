#ifndef __THREAD_H__
#define __THREAD_H__

#include <memory>

class Thread;
static void *threadFunc(void *);

class Thread {
  friend void *threadFunc(void *);
public:
  Thread();
  virtual ~Thread();
  void Start ();
  virtual void *Run();
  void Stop ();
  void Continue ();
  void Suspend ();
  void Join();
protected:
  void Yield();
private:
  pthread_t t_id;
  //  static void* threadFunc(void *);
  enum thState {
    closed = 0,
    stopped,
    running,
    waiting,
    joined
  } state;  
};

#endif // __THREAD_H__
