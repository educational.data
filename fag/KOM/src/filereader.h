#ifndef __FILEREADER_H__
#define __FILEREADER_H__
#include <string>
#include <map>
#include "piostream.h"

class filereader {
  typedef std::pair<std::string,std::string> par;
  typedef std::map<std::string,std::string>::iterator itt;
  static std::map<std::string,std::string> files;
  std::string curBuffer;
public:
  filereader(std::string filename);
  std::string getData() const;
  std::string mimeType() const;
private:
  std::string filename;
  std::string importFile(const std::string &filename);
  
};

IOStream &operator<<(IOStream &stream, const filereader &fil);

#endif // __FILEREADER_H__
