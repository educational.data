#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include "defs.h"

class TestCase {
public:
  TestCase(string testName):
    _testName(testName)
  { 
  }
  void setFrameWork(TestFrameWork &tfw) 
  {
    _tfw = tfw;
  }
  

  void assert(bool test,string &failMsg) 
  {
    if(!test)  fail("Assert failure- " + failMsg); 
    _tfw.testCount++;
  }
  void assert_pri(bool test, string &failMsg) 
  {
    if(!test)  fail("[PRIORITY] Assert failure - " + failMsg); 
    throw NoMoreTestInThisTestCase();
  }

  void fail(string &failMsg) 
  {
    cout << _testName << " - " << failmsg << "\n";
    _tfw.failedTest++;
  }
  virtual void performTest() throw(NoMoreTestInThisTestCase) = 0;
  virtual void setUp() throw() {}
  virtual void tearDown() throw() {}

private:
  TestFrameWork &_tfw;
  string _testName;
  
  class NoMoreTestInThisTestCase {};
};



class TestFrameWork 
{
  TestFrameWork() 
  {
  }
  void addTest(TestCase tc) 
  {
    tc.setFrameWork(this);
    cases.insert(tc);
  }
  void run() 
  {
    vector<TestCase>::iterator itCase;
    itCase = cases.first();
    // TODO: continue the implementation of this file....
  }
  
private:
  vector<TestCase> cases;
  
};

