#ifndef __REQUEST_H__
#define __REQUEST_H__

#include <string>
#include "refptr.h"
#include "SocketPackage/sockiostream.h"

class request {
public:
  request(ref_ptr<SocketPackage::SockIOStream> sockstream);
  ~request();
  std::string get(std::string key);
private:
  struct cimpl;
  struct cimpl *_pimpl;
};

#endif // __REQUEST_H__
