#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include "defs.h"

#include "SocketPackage/tcpsocket.h"
#include "thread.h"
#include <map>
#include "refptr.h"

#include <iostream>
#include "clienthandler.h"

#define STAG "[SERVER] "

using namespace SocketPackage;


class Server : public SocketPackage::TcpSocket {
public:
  Server(bool singleShot) : TcpSocket() { 
    try {
      Bind(SOCKET_PORT);
      Listen();
      cout << STAG << "Waiting for connections\n";
      if(!singleShot) {
	while(1) {
	  ref_ptr<TcpSocket> client = Accept();
	  
	  cout << STAG << "a connection has been established" << endl;
	  // WARNING: Memory leaks here - who destroys this object???
	  // Solution: (temporary). - the objects deletes itself
	  // will be replaced with a more finegrained tracker object..
	  (new ClientHandler(client))->Start();
	}
      } else {
	TcpSocket *sock = Accept();
	cout << "accepted a new client" << endl;
	ClientHandler ch(sock);  // clienthandler sinks the sock with ref_ptr
	ch.Run();
      }

	  
	  //}

      /* for(string val = stream.readLine();
	  val != "\r";
	  val=stream.readLine())
	cout << stream.readLine() << "<\n";


      stream << "<html><body bgcolor=blue><h1>Hvis du kan se dette, s� virker det sku!!!</h1></body></html>\n"; 
      */
      cout << STAG << "Connection to client established\n";
    } 
    catch(SockException ex) {
      cout << STAG << "Socket Exception: " << ex.getDesc() << "\n";
    }
  }
  
};

int main (int argc,char *argv[]) {
  bool singleShot = false;
  if(argc>1) {
    for (int p=1;p<argc;p++) {
      string val = argv[p];
      if(val == "singleshot") singleShot = true;
    }
  }
  if(singleShot)
    cout << STAG << "now running in single call mode" << endl;
  else
    cout << STAG << "executing in threaded mode" << endl;

  ref_ptr<Server> server = new Server(singleShot);
  cout << STAG << "We are now complete sending client objects" << endl;
}

