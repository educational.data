#ifndef __DEFS_H__
#define __DEFS_H__

// test functions
#define FAILED(num) (num < 0)
#define SUCCESS(num) (num >= 0)

#define E_FAIL -1;
#define OK_SUCCESS 0;
typedef unsigned int UWORD;
typedef unsigned long ULONG;

#include <string>
#include <errno.h>
#include <stdio.h>

class Exception {
public:
  Exception() {
    description = strerror(errno);
  }
  std::string &getDesc() { 
    return description;
  }
protected:
  std::string description;
};

//descripe the socket to connect on
#define SOCKET_PORT 6789

#endif // __DEFS_H__
