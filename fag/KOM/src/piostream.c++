#include "piostream.h"

IOStream &endl(IOStream &stream) {
  stream << "\r\n";
  return stream;
}
