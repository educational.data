#include "filereader.h"
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

map<string,string> filereader::files;

filereader::filereader(string filename) {
  this->filename = filename;
  itt p = files.find(filename);
  if(p == files.end()) {
    curBuffer = importFile(filename);
    files.insert(par(filename,curBuffer));
  } else {
    curBuffer = p->second;
  }
}
string filereader::mimeType() const {
  if (filename.rfind(".htm") != string::npos) {
    cout << "mime is a html" << endl;
    return "text/html";
  }
  if (filename.rfind(".jpg") != string::npos || 
      filename.rfind(".jpeg") != string::npos) {
    cout << "mime is a jpeg" << endl;
    return "image/jpeg";
  }
  if (filename.rfind(".gif") != string::npos) {
    cout << "mime is a gif" << endl;
    return "image/gif";
  }
  if (filename.rfind(".c++") != string::npos) {
    cout << "mime is a code" << endl;
    return "text";
  }
  cout << "mime is other" << endl;
  return "application/octet-stream";
}

string filereader::getData() const {
  return curBuffer;
}

string filereader::importFile(const string &file) {
  ifstream ifs(file.c_str());
  if (ifs == 0) throw "Error";

  ostringstream buf;
  buf << ifs.rdbuf();
  
  return buf.str();
}
  
IOStream &operator<<(IOStream &stream, const filereader &fil) {
  stream << fil.getData();
  return stream;
}
