#include "response.h"
#include <fstream>
#include <iostream>

using namespace std;

response::response(ref_ptr<request>req,
		   ref_ptr<SocketPackage::SockIOStream> stream) {
  this->stream = stream;
  pReq = req;
  state = OK;
  contentType = "text/html";
  bufferNotFound = "<hmtl><body><h1>Error 404 - Not Found</h1>cannot find the file you requested</body></html>";;
  bufferNotImplemented = "";
}

void response::flush() {
  //dump headers
  if(state == OK) {
    *stream << "HTTP/1.0 200 OK" << endl;
    cout    << "HTTP/1.0 200 OK" << endl;
  } else { // dump errors
    *stream << "HTTP/1.0 404 Not found" << endl;
    cout    << "HTTP/1.0 404 Not found" << endl;
    buffer = bufferNotFound + "\r\n";
  }

  // We only supports closed connections
  *stream << "Connection: Close" << endl;
  cout    << "Connection: Close" << endl;
      
  // dump server name
  *stream << "Server: TobyMac 1.0" << endl;
  cout    << "Server: TobyMac 1.0" << endl;
  
  // dump content type
  *stream << "Content-Type: " << contentType << endl;
  cout    << "Content-Type: " << contentType << endl;
  
  // dump size of buffer
  *stream << "Content-Length: " << buffer.size() << endl;
  cout    << "Content-Length: " << buffer.size() << endl;
  
  // dump buffer
  *stream << endl << buffer;
  buffer = "";
}

void response::write(const char *str,int) {
  buffer += str;
}
