#ifndef __CLIENTHANDLER_H__
#define __CLIENTHANDLER_H__


#include "SocketPackage/sockiostream.h"

#include "SocketPackage/tcpsocket.h"
#include "thread.h"
#include "request.h"
#include "response.h"

#include "refptr.h"

class ClientHandler : public Thread {
public:
  ClientHandler(ref_ptr<SocketPackage::TcpSocket>sock);
  void *Run();

private:
  ref_ptr<SocketPackage::TcpSocket> socket;
  ref_ptr<SocketPackage::SockIOStream> stream;
  ref_ptr<request> req;
  ref_ptr<response> resp;
};

#endif // __CLIENTHANDLER_H__
