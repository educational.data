#ifndef __RESPONSE_H__
#define __RESPONSE_H__

#include "defs.h"
#include "SocketPackage/tcpsocket.h"
#include "refptr.h"
#include "request.h"
#include "piostream.h"

#include <stdio.h>


#include <string>

enum __respState {
  /*"200" */  OK,
  /* "201"*/  Created,
  /* "202" */ Accepted,
  /* "204" */ No_Content,
  /* "301" */ Moved_Permanently,
  /* "302" */ Moved_Temporarily,
  /* "304" */ Not_Modified,
  /* "400" */ Bad_Request,
  /* "401" */ Unauthorized,
  /* "403" */ Forbidden,
  /* "404" */ Not_Found,
  /* "500" */ Internal_Server_Error,
  /* "501" */ Not_Implemented,
  /* "502" */ Bad_Gateway,
  /* "503" */ Service_Unavailable
};


class response : public IOStream {
public:
  response(ref_ptr<request>req,
	   ref_ptr<SocketPackage::SockIOStream> stream);

  __respState state;
  void flush();

  std::string contentType;

  //  iostream dumps
  void write(const char *str, int size);
private:
  ref_ptr<request> pReq;
  ref_ptr<SocketPackage::SockIOStream> stream;

  std::string buffer;
  std::string bufferNotImplemented;
  std::string bufferNotFound;
};

#endif //__RESPONSE_H__
