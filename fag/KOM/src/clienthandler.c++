#if HAVE_CONFIG_H
#include <config.h>
#endif

#include "filereader.h"

#include "defs.h"

#include "request.h"
#include "clienthandler.h"

#define STAG "[CLIENTHANDLER] "

using namespace std;
using namespace SocketPackage;

ClientHandler::ClientHandler(ref_ptr<TcpSocket>sock) : Thread() {
  socket = sock;

  stream = new SockIOStream(socket.cast<IStreamBuffer>());
  req = new request(stream);
  resp = new response(req,stream);
}

void *ClientHandler::Run() {
  try {
    string filname = "." + req->get("Target");

    filereader fil(filname);
    *resp << fil;
    resp->contentType = fil.mimeType();
    resp->state = OK;
  } catch(...) {
    resp->state = Not_Found;
  }
  resp->flush();
  socket->Close();
}
