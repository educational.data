#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <map>

#include "defs.h"

#include "request.h"

using namespace SocketPackage;
using namespace std;

string trim(string str) {
  int posb = str.find_first_not_of(" ");
  int pose = str.find_last_not_of(" ")+1 - posb;
  return str.substr(posb,pose);
}

class request::cimpl {
public:
  map<string,string> ReqHash;
  ref_ptr<SocketPackage::SockIOStream> stream;
  string cVersionHead;
  string cGetHead;
};

typedef pair<string,string> par;

request::request(ref_ptr<SockIOStream> sockstream) {
  _pimpl = new cimpl;
  // init of use data
  _pimpl->cVersionHead = "HTTP/";
  _pimpl->cGetHead = "GET";

  _pimpl->stream = sockstream;
  string str = _pimpl->stream->readLine();
  while(str != "") {
    if(str.find(_pimpl->cGetHead) != string::npos)  {
      int last = str.find(_pimpl->cVersionHead);
      int first = str.find(_pimpl->cGetHead) + 
	_pimpl->cGetHead.size();
      int tLen = last - first;
      int verheadsize = _pimpl->cVersionHead.size();
      int sSize = str.size();

      
      _pimpl->ReqHash.insert(par("Target",
				 trim(str.substr(first,tLen))));
      _pimpl->ReqHash.insert(par("HttpProtocol",
				 str.substr(last+verheadsize,
					    sSize - last - verheadsize)));
      
      
    } else {
      int firstSplitPos = str.find(": ");
      int lastSplitPos = firstSplitPos+2;
      int len = str.size()-lastSplitPos;
      
      string key = str.substr(0,firstSplitPos);
      string value = str.substr(lastSplitPos, len);
      
      _pimpl->ReqHash.insert(par(key,value));
      
    }// str == "GET"
    str = _pimpl->stream->readLine();
  } // str != ""
}
request::~request() { 
  delete _pimpl; 
}

string request::get(string key) {
  return _pimpl->ReqHash[key];
}
