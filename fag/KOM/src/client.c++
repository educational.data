#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include "defs.h"
#include "refptr.h"

#include "SocketPackage/tcpsocket.h"
#include "SocketPackage/sockiostream.h"
#define STAG "[CLIENT] "

using namespace SocketPackage;
using namespace std;

class Client : public TcpSocket {
public:
  Client() : TcpSocket() {
    try {
      Connect("127.0.0.1",SOCKET_PORT);
      cout << STAG << "We have a connection\n";
    }
    catch(SockConnectionException ex) {
      cout << STAG << "Connection error: " << ex.getDesc() << endl;
    }
    catch(SockException ex){
      cout << STAG << "Error: " << ex.getDesc() << endl;
    }
  }
  
};

int main () {
  ref_ptr<Client>client = new Client;
  ref_ptr<SockIOStream> stream = new SockIOStream(client.obj());
  *stream << "GET / HTTP 1.0\n";
  *stream << "Host: localhost:2000\n";
  *stream << "\n";
}

