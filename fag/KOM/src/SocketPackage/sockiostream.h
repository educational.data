
#ifndef _SOCKIOSTREAM_H_
#define _SOCKIOSTREAM_H_

#include "refptr.h"

#include "streambuffer.h"
#include <iostream>

using namespace std;

namespace SocketPackage {
  class SockIOStream {
  public:
    SockIOStream ( ref_ptr<IStreamBuffer> baseStream );
    SockIOStream ( IStreamBuffer &baseStream );
    virtual ~SockIOStream();
    
    // input opperators
    void write(const char *data,int size);
    // void operator<<(const string &data);

    // output functions
    string readLine();
    char *readBytes(char *buf,int size);

    ref_ptr<IStreamBuffer> getBuffer();
  private:
    ref_ptr<IStreamBuffer> pStream;
    char buffer[100];
  };
}

typedef SocketPackage::SockIOStream & 
(__smanip)(SocketPackage::SockIOStream &);

SocketPackage::SockIOStream 
&operator<<(SocketPackage::SockIOStream &stream, const string &str);

SocketPackage::SockIOStream 
&operator<<(SocketPackage::SockIOStream &stream, const char *str);

SocketPackage::SockIOStream 
&operator<<(SocketPackage::SockIOStream &stream, __smanip func);

SocketPackage::SockIOStream
&operator<<(SocketPackage::SockIOStream &stream, const int num);

// manipulators
SocketPackage::SockIOStream &endl(SocketPackage::SockIOStream &stream);


#endif // _SOCKIOSTREAM_H_
