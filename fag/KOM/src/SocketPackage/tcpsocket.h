#ifndef __TCPSOCKET_H__
#define __TCPSOCKET_H__

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>

#include "defs.h"

#include "streambuffer.h"
#include "refptr.h"

using namespace std;

namespace SocketPackage {
  class SockException : public Exception{ };
  class SockCannotResolveNameException : public SockException{ };
  class SockBindException : public SockException{ };
  class SockOptionsException : public SockException{ };  
  class SockConnectionException : public SockException { };
  class SockConnectionClosedException : public SockException { };

  class TcpSocket : public IStreamBuffer {
  public:
    TcpSocket();
    // OBSOLETE --> TcpSocket(const TcpSocket &co);
    ~TcpSocket(); // TODO: implement
    
    void Connect(string host, 
		 UWORD port);

    void Bind(string host,
	     UWORD port);
    
    void Bind(UWORD port);

    void Close();
    /** 
     * Sets up a the port to be listning on 
     */
    void Listen();
    void Listen(int backlock);

    /**
     * Returns a socket representing the client connection
     */
    // should be wrapped in to a ref_ptr object;
    TcpSocket *Accept();

    int Recieve(void *buf, size_t len);
    void Send(void *buf, size_t len);

    // IStreamBuffer impl
    int getData(const char *msg, int len);
    int putData(const char *msg, int len);
  
  private:
    mutable int sockfd;
    // will hold the desitantion address
    struct sockaddr_in dest_addr;
    enum socketState {
      closed,
      established,
      connected,
      listening
    };
    mutable socketState state;
    
  private: // functions in the private section
    // private way of construction. Used in Accept
    TcpSocket(sockaddr_in &dd,int &socknum);
    int setUpAdr(sockaddr_in &addr,
		 string &host, 
		 UWORD port);
    
    int setUpAdr(sockaddr_in &addr,
		 ULONG host, 
		 UWORD port);
  };  
}
#endif // __TCPSOCKET_H__
