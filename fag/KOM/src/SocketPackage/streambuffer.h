#ifndef _STREAMBUFFER_H_
#define _STREAMBUFFER_H_
class IStreamBuffer {
public:
  virtual int getData(const char *msg, int size) = 0;
  virtual int putData(const char *msg, int size) = 0;
};
#endif // _STREAM_BUFFER_H_H
