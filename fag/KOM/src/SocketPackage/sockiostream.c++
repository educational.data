#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <defs.h>

#include "sockiostream.h" 
#include <iostream.h>

SocketPackage::SockIOStream::SockIOStream(ref_ptr<IStreamBuffer> baseStream) : pStream(baseStream)
{
}

SocketPackage::SockIOStream::~SockIOStream() 
{
  
}

// input functions
void SocketPackage::SockIOStream::write(const char *data,int size) 
{
  pStream->putData(data,size);
}

// ouput functions

string SocketPackage::SockIOStream::readLine() 
{
  char sign;
  string retval;
  int cnt;
  
  do {
    cnt = pStream->getData(& sign,1); // getdata one byte at a time

    // chomp data
    if((sign != (char)'\n') && 
       (sign != (char)'\r')) 
      retval += sign;

  } while (// make sure the socket is not closed.
	   (cnt > 0) && 
	   // Make sure the sign is not \n
	   (sign != '\n'));

  return retval;
}

char *SocketPackage::SockIOStream::readBytes(char *buf,int size)
{
  pStream->getData(buf, size);
  return buf;
}

SocketPackage::SockIOStream &
operator<<(SocketPackage::SockIOStream &stream, const string &str) {
  stream.write(str.c_str(),str.size());
  return stream;
}

SocketPackage::SockIOStream &
operator<<(SocketPackage::SockIOStream &stream, const char *str) {
  stream.write(str,strlen(str));
  return stream;
}

SocketPackage::SockIOStream &
operator<<(SocketPackage::SockIOStream &stream,__smanip func)
{
  return (*func)(stream);
}

SocketPackage::SockIOStream &
operator<<(SocketPackage::SockIOStream &stream, const int num)
{
  char val[10];
  sprintf(val,"%d",num);
  stream << val;
  return stream;
}
  

SocketPackage::SockIOStream &
endl(SocketPackage::SockIOStream &stream) {
  stream << "\n";
  return stream;
}
