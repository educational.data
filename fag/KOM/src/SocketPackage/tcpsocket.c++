#if HAVE_CONFIG_H
#include <config.h>
#endif

#include "defs.h"

#include <stdio.h>

#include <string>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>

#include <unistd.h>
#include <iostream>

#include "tcpsocket.h"


using namespace std;

SocketPackage::TcpSocket::TcpSocket()
{
  state = closed;
  protoent *proto = getprotobyname("tcp");
  if(proto==NULL)
    throw SockException();

  sockfd = socket(AF_INET, SOCK_STREAM, proto->p_proto);
  if(FAILED(sockfd)) 
    throw SockException();

  state = established;
}

SocketPackage::TcpSocket::~TcpSocket() {
  if(state != closed) {
    Close();
  }
}  

void SocketPackage::TcpSocket::Connect(string host, 
				       UWORD port) 
{
  if ( FAILED (setUpAdr(dest_addr,host,port)) ) 
    throw SockCannotResolveNameException();
  
  if ( FAILED(
      connect( sockfd, 
	       (struct sockaddr *)&dest_addr, 
	       sizeof(struct sockaddr)))) {
    throw SockConnectionException();
  }
  state = connected;
}

/**********************************************************
 * Bind
 *   Bind ip address and port on this server
 *   (I believe its the way it works)
 **********************************************************/

void SocketPackage::TcpSocket::Bind(UWORD port) 
{
  setUpAdr(dest_addr, INADDR_ANY, port);
  int yes=1;

  // lose the pesky "Address already in use" error message
  int succ = setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int));
  if( FAILED ( succ ) ) throw SockOptionsException ( );

  succ = bind(sockfd, 
	      (struct sockaddr *)&dest_addr, 
	      sizeof(struct sockaddr));

  if ( FAILED( succ ) ) throw SockBindException();
}

void  SocketPackage::TcpSocket::Bind(string host,
				      UWORD port) 
{
  setUpAdr(dest_addr, host, port);
  int yes=1;

  // lose the pesky "Address already in use" error message
  int succ = setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof(int));
  if( FAILED ( succ ) ) throw SockOptionsException ( );

  succ = bind(sockfd, 
	      (struct sockaddr *)&dest_addr, 
	      sizeof(struct sockaddr));
  
  if ( FAILED( succ ) ) throw SockBindException();
}


/*********************************************************
 * Pre:
 *   A ip address must be stablished on localhost 
 *   (with bind)
 * Post:
 *   System is ready to listen
 ********************************************************/

void SocketPackage::TcpSocket::Listen(int backlog) 
{
  int succ = listen(sockfd,backlog);
  if(FAILED(succ)) throw SockException();

  state = listening;
}

void SocketPackage::TcpSocket::Listen() 
{
  this->Listen(20);
}


/*********************************************************
 * Pre:
 *   A listner has been established and is ready to wait
 * Post:
 *  A new Socket object has been created and should be
 *  Destroyed be the recieving object
 ********************************************************/
SocketPackage::TcpSocket *SocketPackage::TcpSocket::Accept()
{
  sockaddr_in dest;
  socklen_t sin_size = sizeof(sockaddr_in);
  int nsock = accept(sockfd, (struct sockaddr*)&dest, &sin_size);
  if(FAILED(nsock)) throw SockException();
  TcpSocket *sock = new TcpSocket(dest, nsock);
  return sock;
}

SocketPackage::TcpSocket::TcpSocket(sockaddr_in &dd,int &socknum) {
  sockfd    = socknum;
  state     = connected;
  dest_addr = dd;
}

/*********************************************************
 * Pre: 
 *   A host and a port should be passed in
 * Post:
 *   A socket_in structure should be filled correctly
 ********************************************************/
int SocketPackage::TcpSocket::setUpAdr(sockaddr_in &addr,
					string &host, 
					UWORD port)
{
  
  // An extraordinary hack
  addr.sin_addr.s_addr = inet_addr(inet_ntoa(*((struct in_addr *)gethostbyname( host.c_str() )->h_addr)));
  
  // setup ordinary data
  addr.sin_family = AF_INET;     // host byte order
  addr.sin_port = htons(port);   // short, network byte order
  memset(&(dest_addr.sin_zero), '\0', 8);  // zero the rest of the struct
  return OK_SUCCESS;
}

/*********************************************************
 * Recv: recieve data from the socket
 * Pre: 
 */

int SocketPackage::TcpSocket::Recieve(void *buf, size_t len) {
  if (state == closed)
    throw SockConnectionClosedException();

  int success = recv(sockfd,buf, len,0);

  if(success == 0) { // dictates connection closed
    state = closed;
    throw SockConnectionClosedException();
  }
  return success;
}

void SocketPackage::TcpSocket::Send(void *buf,size_t len) {
  if(state == closed)
    throw SockConnectionClosedException();
  
  // data is sometimes to huge to send in one shot
  // probally because its supposed to send in more than
  // one packet.
  // this should handle this matter by sending in multiple 
  // packages
  size_t lensend = send(sockfd,buf,len,0);
  while(lensend < len) {
    if(FAILED(lensend)) 
      throw SockException();
    
    // very dirty hack!!!!
    buf  = (void *)((char *)buf + lensend);
    len -= lensend;
    lensend = send(sockfd,buf,len,0);
  } 
}

/*********************************************************
 * Pre: 
 *   A host and a port should be passed in
 * Post:
 *   A socket_in structure should be filled correctly
 *********************************************************/
int SocketPackage::TcpSocket::setUpAdr(sockaddr_in &addr,
				       ULONG host, 
				       UWORD port)
{
  // test to see if connection data is correct
  if ( FAILED( addr.sin_addr.s_addr = htonl(host) ) )
    return E_FAIL;// signals failure
  
  // setup ordinary data
  addr.sin_family = AF_INET;     // host byte order
  addr.sin_port = htons(port);   // short, network byte order
  memset(&(dest_addr.sin_zero), '\0', 8);  // zero the rest of the struct
  return OK_SUCCESS;
}

/**
 * I should always include a close function for this one
 */
void SocketPackage::TcpSocket::Close() {
  if(state != closed) {
    shutdown( sockfd, 3);
    close( sockfd );
    state = closed;
  }
}

/**
 * functions to implement the IStreamBuffer interface
 */

int SocketPackage::TcpSocket::getData(const char *msg, int size) {
  return Recieve((char *) msg,(size_t) size);
}

int SocketPackage::TcpSocket::putData(const char *msg, int size) {
  Send((char *) msg,(size_t) size);
  return size; // we garantee too send all data.
}

