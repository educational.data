#ifndef __IOSTREAM_H__
#define __IOSTREAM_H__

#include <string>

class IOStream;
typedef IOStream &(__IOStreamManip)(IOStream &);

class IOStream {
protected:
  virtual void write(const char *str,int size) = 0;
public:
  IOStream &operator<<(const char *str) {
    write(str,strlen(str));
    return *this;
  }
  IOStream &operator<<(const std::string &str) {
    write(str.c_str(),str.size());
    return *this;
  }
  IOStream &operator<<(__IOStreamManip func) { 
    return (*func)(*this);
  }
};

IOStream &endl(IOStream &stream);

#endif // __IOSTREAM_H__
