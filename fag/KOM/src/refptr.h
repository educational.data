#ifndef __REF_PTR_H__
#define __REF_PTR_H__

#include <exception>

template<class T>
class ref_ptr {
  class bad_ref : public std::exception 
  {
  public:
    bad_ref() throw () { }
    ~bad_ref() throw () { }
    const char *what() const throw() {
      return "Bad reference";
    }
  };
  class contT {
  public:
    contT() {
      allocated=false;
      refCnt=0;
      pT = NULL;
    }
    virtual ~contT() {
      if(allocated)delete pT;
    }
    void AddRef() {++refCnt;}
    void Release() {if((--refCnt) <= 0) delete this;}
    T* pT;
    bool allocated;
    int refCnt;
  } *pCont;
public:
  ~ref_ptr() {
    pCont->Release();
  }
  ref_ptr() {
    pCont = NULL;
  }
  ref_ptr(const T* pT) {
    pCont = new contT;
    pCont->pT = const_cast<T *>(pT);
    pCont->allocated = true;
    pCont->AddRef();
  }
  ref_ptr(const ref_ptr<T> &rT) {
    pCont = const_cast<contT *>(rT.pCont);
    pCont->AddRef();
  }
  void operator=(const T* rT) throw(bad_ref){
    if(pCont) pCont->Release();
    pCont = new contT;
    pCont->pT = const_cast<T *>(rT);
    pCont->allocated=true;
    pCont->AddRef();
  }

  void operator=(const ref_ptr<T> &rT) {
    if(pCont)pCont->Release();
    pCont = rT.pCont;
    pCont->AddRef();
  }
  template<typename X> ref_ptr<X> cast() throw(bad_ref) {
    X *obj = dynamic_cast<X*>(pCont->pT);
    if(!obj) throw bad_ref();
    return ref_ptr<X>(obj);
  }
  T *operator->() throw(bad_ref) {
    if(pCont->allocated)
      return pCont->pT;
    else
      throw bad_ref();
  }
  T &operator*() throw(bad_ref) {
    if(pCont->allocated)
      return *(pCont->pT);
    else
      throw bad_ref();
  }
  T *obj() throw(bad_ref) {
    if(pCont->allocated) 
      return pCont->pT;
    else
      throw bad_ref();
  }
};

#endif // __REF_PTR_H__
