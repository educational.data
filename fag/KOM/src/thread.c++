#if HAVE_CONFIG_H
#include <config.h>
#endif

#include <pthread.h>
#include "defs.h"
#include "thread.h"
#include <iostream>

using namespace std;


/**
 * void *threadFunc(void *)
 *
 * local function used to pass in the call from C to C++
 * standard C cannot always parse to C++ calls and must
 * therefore use a wrapper function to make the correct
 * call
 * It calls the Run function on the thread object.
 */
static void* threadFunc(void *pvThis) 
{
  Thread *pThis = static_cast<Thread *>(pvThis);
  void *r = pThis->Run();
  pThis->state = (Thread::thState)0;
  pthread_exit(r);
}

/**
 * CREATE:
 * Thread()
 *
 * Creates the pimpl object and sets the internal state
 * to closed.
 */
Thread::Thread() {
  state = closed;
}

Thread::~Thread() {
  cout << "Destoyed an object" << endl;
}

/**
 * void Start()
 *
 * Function to call from the parent.
 * It calls the std c pthread library function
 * to initialise a C++ thread
 * Its initiated through the threadFunc function
 */
void Thread::Start () {
  if(state != stopped) {
    // Make some type casting that ANSI C++ permits
    void *ths = static_cast<void *>(this);

    pthread_create(&t_id, NULL, &threadFunc,ths);
    
    state = running;
  }
}

/**
 * void Run()
 *
 * The main Thread function of this object;
 */
void *Thread::Run () {
  // empty!!! (as supposed to be)
}

/**
 * void Stop()
 *
 * Stops the running thread from being run
 * the Thread cannot resume from here on
 *
 * TODO: Implement accrding to the pthread
 *       Standard
 */
void Thread::Stop () {
  if(state = running) {
    // stop the thread
    state = stopped;
  }
}

/**
 * void Suspend()
 * 
 * If the thread is running it can be suspended
 * while later being resumed. This is a lite 
 * way of stopping the thread
 *
 * TODO: Implement according to standard POSIX
 */
void Thread::Suspend () {
  if(state == running) {
    // thr_suspend(pimpl_->t_id);
    state = waiting;
  }
}

/**
 * void Continue()
 *
 * Continue a suspended thread (see Suspend::)
 */
void Thread::Continue () {
  if(state == waiting) {
    // thr_continue(pImpl->t_id);
    state = running;
  }
}

/**
 * void Yield()
 *
 * Yield a process in the running que
 *
 * TODO: implement according to standard POSIX
 */
void Thread::Yield() {
  // thr_yield();
}

/**
 * void Join()
 *
 * Joins a thread to the calling thread
 */
void Thread::Join() {
  if(state == running) {
    pthread_join(t_id,NULL);
    state = joined;
  }
}
