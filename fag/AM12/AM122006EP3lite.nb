(************** Content-type: application/mathematica **************
                     CreatedBy='Mathematica 5.2'

                    Mathematica-Compatible Notebook

This notebook can be used with any Mathematica-compatible
application, such as Mathematica, MathReader or Publicon. The data
for the notebook starts with the line containing stars above.

To get the notebook into a Mathematica-compatible application, do
one of the following:

* Save the data starting with the line of stars above into a file
  with a name ending in .nb, then open the file inside the
  application;

* Copy the data starting with the line of stars above to the
  clipboard, then use the Paste menu command inside the application.

Data for notebooks contains only printable 7-bit ASCII and can be
sent directly in email or through ftp in text mode.  Newlines can be
CR, LF or CRLF (Unix, Macintosh or MS-DOS style).

NOTE: If you modify the data for this notebook not in a Mathematica-
compatible application, you must delete the line below containing
the word CacheID, otherwise Mathematica-compatible applications may
try to use invalid cache data.

For more information on notebooks and Mathematica-compatible 
applications, contact Wolfram Research:
  web: http://www.wolfram.com
  email: info@wolfram.com
  phone: +1-217-398-0700 (U.S.)

Notebook reader applications are available free of charge from 
Wolfram Research.
*******************************************************************)

(*CacheID: 232*)


(*NotebookFileLineBreakTest
NotebookFileLineBreakTest*)
(*NotebookOptionsPosition[     10482,        330]*)
(*NotebookOutlinePosition[     11220,        356]*)
(*  CellTagsIndexPosition[     11176,        352]*)
(*WindowFrame->Normal*)



Notebook[{
Cell["EP3: Forces in a particle model of a crane", "Title"],

Cell["\<\
Tobias Nielsen
The Maers Mc-Kinney Moller Institute for Production Technology
University of Southern Denmark
2006\
\>", "Author"],

Cell[CellGroupData[{

Cell["Initialisering", "Section"],

Cell[BoxData[
    \(Off[General::"\<spell1\>"]\)], "Input",
  InitializationCell->True],

Cell[BoxData[
    \(Off[General::"\<spell\>"]\)], "Input",
  InitializationCell->True]
}, Open  ]],

Cell[CellGroupData[{

Cell["Illustrering af systemet", "Section"],

Cell["\<\
Vi starter ud med at opstille de generelle parametrer for systemet.

Da systemet er beskrevet som et system med fast y = 0, V\[AE]lger vi at repr\
\[AE]sentere systemet i to dimensioner. Her v\[AE]lger vi mere naturligt at \
beskrive systemet med de to koordinater x og y.\
\>", "Text"],

Cell[BoxData[
    \(coords = {x1, z1, x2, z2, x3, z3, x4, z4, x5, z5, x6, z6}\)], "Input"],

Cell[BoxData[
    \(Nc = Length[coords]\)], "Input"],

Cell["Den tidsafh\[AE]ngige position:", "Text"],

Cell[BoxData[
    \(p2pt = 
      Table[coords[\([i]\)] \[Rule] \(coords[\([i]\)]\)[t], {i, 
          Nc}]\)], "Input"],

Cell[BoxData[
    \(coordst = coords /. p2pt\)], "Input"],

Cell["Vi kan nu implementere de generelle koordinater.", "Text"],

Cell[BoxData[
    \(q = {x, \[Theta]}\)], "Input"],

Cell[BoxData[
    \(Nq = Length[q]\)], "Input"],

Cell[BoxData[
    \(q2qt = Table[q[\([i]\)] \[Rule] \(q[\([i]\)]\)[t], {i, Nq}]\)], "Input"],

Cell[BoxData[
    \(qt = q /. q2qt\)], "Input"],

Cell["\<\
Inden vi begynder at oprette et transformations matrice, bliver vi \
n\[OSlash]dt til at finde ud af hvordan at vi bestemmer koordinat 3.
Vi starter med at finde l\[AE]ngden af stykket 1\[Rule]3 vha. cosinus \
relationerne\
\>", "Text"],

Cell[BoxData[
    \(aFo\  = \(Solve[c\^2 \[Equal] \ a\^2 + b\^2 - 2\ a\ b\ Cos[\[Theta]], 
            a]\)[\([2]\)] /. {b \[Rule] x, c \[Rule] l}\)], "Input"],

Cell["\<\
Vi opretter en transformation s\[ARing]ledes at vi kan realisere \
systemet i form at de generaliserede koordinater\
\>", "Text"],

Cell[BoxData[
    \(q2X1 = {\[IndentingNewLine]x1 \[Rule] 0, \[IndentingNewLine]z1 \[Rule] 
            0, \[IndentingNewLine]\[IndentingNewLine]x2 \[Rule] 
            x, \[IndentingNewLine]z2 \[Rule] 
            0, \[IndentingNewLine]\[IndentingNewLine]x3 \[Rule] \ 
            a\ Cos[\[Theta]], \[IndentingNewLine]z3 \[Rule] \ 
            a\ Sin[\[Theta]], \[IndentingNewLine]\[IndentingNewLine]x4 \
\[Rule] L\ Cos[\[Theta]], \[IndentingNewLine]z4 \[Rule] 
            L\ Sin[\[Theta]], \[IndentingNewLine]\[IndentingNewLine]x5 \
\[Rule] Cos[\[Theta]] L, \[IndentingNewLine]z5 \[Rule] 
            Sin[\[Theta]] L - 
              h, \[IndentingNewLine]\[IndentingNewLine]x6 \[Rule] 
            0, \[IndentingNewLine]z6 \[Rule] 
            H\[IndentingNewLine]\[IndentingNewLine]} /. aFo\)], "Input"],

Cell[BoxData[
    \(coordsq = coords /. q2X1\)], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Visualisering af kranen", "Section"],

Cell[CellGroupData[{

Cell["Funktioner", "Subsubsection"],

Cell["Vi starter med at implementere de basale funktioner:", "Text"],

Cell[BoxData[
    \(redstick3[r1_, r2_] := 
      Graphics3D[{RGBColor[1, 0, 0], Thickness[0.02], 
          Line[{r1, r2}]}]\)], "Input"],

Cell[BoxData[
    \(blackstick3[r1_, r2_] := 
      Graphics3D[{Thickness[0.02], Line[{r1, r2}]}]\)], "Input"],

Cell[BoxData[
    \(greenstick3[r1_, r2_] := 
      Graphics3D[{RGBColor[0, 1, 0], Thickness[0.02], 
          Line[{r1, r2}]}]\)], "Input"],

Cell[BoxData[
    \(bluestick3[r1_, r2_] := 
      Graphics3D[{RGBColor[0, 0, 1], Thickness[0.02], 
          Line[{r1, r2}]}]\)], "Input"],

Cell[BoxData[
    \(blackblob3[r1_] := Graphics3D[{PointSize[0.02], Point[r1]}]\)], "Input"],

Cell[BoxData[
    \(blueblob3[r1_] := 
      Graphics3D[{RGBColor[0, 0, 1], PointSize[0.02], Point[r1]}]\)], "Input"],

Cell[BoxData[
    \(greenblob3[r1_] := 
      Graphics3D[{RGBColor[0, 1, 0], PointSize[0.05], Point[r1]}]\)], "Input"],

Cell[BoxData[
    \(redblob3[r1_] := 
      Graphics3D[{RGBColor[1, 0, 0], PointSize[0.02], Point[r1]}]\)], "Input"],

Cell[BoxData[{
    \(\(axes3[r_, u_, v_, 
          w_] := {blackstick3[r - u\/\@\(u . u\), r + u\/\@\(u . u\)], 
          bluestick3[r - v\/\@\(v . v\), r + v\/\@\(v . v\)], 
          greenstick3[r - w\/\@\(w . w\), 
            r + w\/\@\(w . w\)]};\)\), "\[IndentingNewLine]", 
    \(redaxes3[r_, u_, v_, 
        w_] = {redstick3[r - u\/\@\(u . u\), r + u\/\@\(u . u\)], 
        redstick3[r - v\/\@\(v . v\), r + v\/\@\(v . v\)], 
        redstick3[r - w\/\@\(w . w\), r + w\/\@\(w . w\)]}\)}], "Input"]
}, Closed]],

Cell[CellGroupData[{

Cell["Visualisering", "Subsubsection"],

Cell[BoxData[
    \(\(\(systemvis = {\[IndentingNewLine]greenblob3[{x1, 0, \ z1}], 
        Graphics3D[
          Text["\<1\>", {x1, 0, \ z1}]], \[IndentingNewLine]greenblob3[{x2, 
            0, \ z2}], 
        Graphics3D[
          Text["\<2\>", {x2, 0, \ z2}]], \[IndentingNewLine]greenblob3[{x3, 
            0, \ z3}], 
        Graphics3D[
          Text["\<3\>", {x3, 0, \ z3}]], \[IndentingNewLine]greenblob3[{x4, 
            0, \ z4}], 
        Graphics3D[
          Text["\<4\>", {x4, 0, \ z4}]], \[IndentingNewLine]greenblob3[{x5, 
            0, \ z5}], 
        Graphics3D[
          Text["\<5\>", {x5, 0, \ z5}]], \[IndentingNewLine]greenblob3[{x6, 
            0, \ z6}], 
        Graphics3D[
          Text["\<6\>", {x6, 0, \ 
              z6}]], \[IndentingNewLine]\[IndentingNewLine]redstick3[{x1, 
            0, \ z1}, {x2, 0, \ z2}], \[IndentingNewLine]redstick3[{x1, 0, \ 
            z1}, {x3, 0, \ z3}], \[IndentingNewLine]redstick3[{x2, 0, \ 
            z2}, {x3, 0, \ z3}], \[IndentingNewLine]redstick3[{x3, 0, \ 
            z3}, {x4, 0, \ z4}], \[IndentingNewLine]redstick3[{x4, 0, \ 
            z4}, {x5, 0, \ z5}], \[IndentingNewLine]redstick3[{x3, 0, \ 
            z3}, {x6, 0, \ z6}], \[IndentingNewLine]redstick3[{x6, 0, \ 
            z6}, {x1, 0, \ z1}]\[IndentingNewLine]}\)\(\[IndentingNewLine]\)
    \)\)], "Input"],

Cell[BoxData[
    \(q2X1 /. {\[Theta] \[Rule] \[Pi]/2, \ H \[Rule] 3, h \[Rule] \ 2, 
        L \[Rule] \ 5, l \[Rule] \ 2}\)], "Input"],

Cell[BoxData[
    \(systemvis0 = \(systemvis /. \ q2X1\) /. {H \[Rule] 3, h \[Rule] \ 2, 
          L \[Rule] \ 5, l \[Rule] \ 2}\)], "Input"],

Cell[BoxData[
    \(Show[systemvis0 /. {x \[Rule] 1, \[Theta] \[Rule] \[Pi]/3}, 
      ViewPoint -> {0.759, \ \(-3.150\), \ 0.975}]\)], "Input"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Constraints", "Section"],

Cell["", "Text"],

Cell[BoxData[
    \(G = 
      Simplify[{\[IndentingNewLine]x1, 
          z1, \[IndentingNewLine]x2 - 
            x, \[IndentingNewLine]\((x2 - x3)\)\^2 + \((z2 - z3)\)\^2 - 
            l\^2, \[IndentingNewLine]\((x1 - x4)\)\^2 + \((z1 - z4)\)\^2 - 
            L\^2, \[IndentingNewLine]\(-z4\)\ Cos[\[Theta]] + 
            x4\ Sin[\[Theta]], \[IndentingNewLine]\(-z3\)\ Cos[\[Theta]] + 
            x3\ Sin[\[Theta]], \[IndentingNewLine]x5 - 
            x4, \[IndentingNewLine]z4 - z5 - h, \[IndentingNewLine]x6, 
          z6 - H, \[IndentingNewLine]y2\[IndentingNewLine]}]\)], "Input"],

Cell[BoxData[
    \(Ng\  = \ Length[G]\)], "Input"],

Cell["Constraints matricet er", "Text"],

Cell[BoxData[
    \(Jmat = 
      Table[D[G[\([i]\)], coords[\([j]\)]], {i, Ng}, {j, Nc}]\)], "Input"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Externe kr\[AE]fter p\[ARing] partiklerne 2, 3 og 4", "Section"],

Cell[BoxData[
    \(Fext = {0, 
        0, \[IndentingNewLine]0, \(-m\)\ g, \[IndentingNewLine]F \((x1 - 
                x3)\)/\@\(\((x1 - x3)\) + \((z1 - z3)\)\), 
        F \((z1 - z3)\)/\@\(\((x1 - x3)\) + \((z1 - z3)\)\) - 
          m\ g, \[IndentingNewLine]0, \(-m\)\ g, \[IndentingNewLine]0, \(-M\)\
\ g, \[IndentingNewLine]0, 0\[IndentingNewLine]}\)], "Input"],

Cell[BoxData[
    \(\[Mu]vec = Array[\[Mu], Ng]\)], "Input"],

Cell[BoxData[
    \(coeqs = G\)], "Input"],

Cell[BoxData[
    \(fbeqs = 
      Table[\((Transpose[Jmat] . \[Mu]vec)\)[\([i]\)] - Fext[\([i]\)], {i, 
            Nc}] /. q2X1\)], "Input"],

Cell[BoxData[
    \(zZsol = 
      Flatten[Solve[{coeqs[\([6]\)], coeqs[\([7]\)]} \[Equal] {0, 0}, {x4, 
            x3}]]\)], "Input"],

Cell[BoxData[
    \(fbeqs1 = fbeqs /. zZsol\)], "Input"],

Cell["\<\
Vi eliminere lidt af de mangfoldige problemer ved at rette \
p\[ARing] punkt 4\
\>", "Text"],

Cell[BoxData[
    \(eqs = Join[fbeqs1, coeqs]\)], "Input"],

Cell[BoxData[
    \(vars = Join[coords, \[Mu]vec]\)], "Input"],

Cell[BoxData[
    \(zvec = Table[0, {i, Length[vars]}]\)], "Input"],

Cell[BoxData[
    \(Solve[eqs \[Equal] zvec, vars]\)], "Input"]
}, Open  ]]
},
FrontEndVersion->"5.2 for X",
ScreenRectangle->{{0, 1280}, {0, 1024}},
AutoGeneratedPackage->Automatic,
WindowSize->{635, 969},
WindowMargins->{{Automatic, -1}, {-3, Automatic}},
ShowSelection->True,
Magnification->1,
StyleDefinitions -> "Classroom.nb"
]

(*******************************************************************
Cached data follows.  If you edit this Notebook file directly, not
using Mathematica, you must remove the line containing CacheID at
the top of  the file.  The cache data will then be recreated when
you save this file from within Mathematica.
*******************************************************************)

(*CellTagsOutline
CellTagsIndex->{}
*)

(*CellTagsIndex
CellTagsIndex->{}
*)

(*NotebookFileOutline
Notebook[{
Cell[1754, 51, 59, 0, 63, "Title"],
Cell[1816, 53, 139, 5, 104, "Author"],

Cell[CellGroupData[{
Cell[1980, 62, 33, 0, 61, "Section"],
Cell[2016, 64, 87, 2, 47, "Input",
  InitializationCell->True],
Cell[2106, 68, 86, 2, 47, "Input",
  InitializationCell->True]
}, Open  ]],

Cell[CellGroupData[{
Cell[2229, 75, 43, 0, 61, "Section"],
Cell[2275, 77, 296, 6, 106, "Text"],
Cell[2574, 85, 90, 1, 47, "Input"],
Cell[2667, 88, 52, 1, 47, "Input"],
Cell[2722, 91, 47, 0, 28, "Text"],
Cell[2772, 93, 120, 3, 47, "Input"],
Cell[2895, 98, 57, 1, 47, "Input"],
Cell[2955, 101, 64, 0, 28, "Text"],
Cell[3022, 103, 50, 1, 47, "Input"],
Cell[3075, 106, 47, 1, 47, "Input"],
Cell[3125, 109, 92, 1, 47, "Input"],
Cell[3220, 112, 47, 1, 47, "Input"],
Cell[3270, 115, 246, 5, 76, "Text"],
Cell[3519, 122, 159, 2, 51, "Input"],
Cell[3681, 126, 139, 3, 28, "Text"],
Cell[3823, 131, 808, 13, 351, "Input"],
Cell[4634, 146, 57, 1, 47, "Input"]
}, Open  ]],

Cell[CellGroupData[{
Cell[4728, 152, 42, 0, 61, "Section"],

Cell[CellGroupData[{
Cell[4795, 156, 35, 0, 45, "Subsubsection"],
Cell[4833, 158, 68, 0, 28, "Text"],
Cell[4904, 160, 138, 3, 63, "Input"],
Cell[5045, 165, 110, 2, 47, "Input"],
Cell[5158, 169, 140, 3, 63, "Input"],
Cell[5301, 174, 139, 3, 63, "Input"],
Cell[5443, 179, 92, 1, 47, "Input"],
Cell[5538, 182, 117, 2, 63, "Input"],
Cell[5658, 186, 118, 2, 63, "Input"],
Cell[5779, 190, 116, 2, 63, "Input"],
Cell[5898, 194, 510, 9, 242, "Input"]
}, Closed]],

Cell[CellGroupData[{
Cell[6445, 208, 38, 0, 37, "Subsubsection"],
Cell[6486, 210, 1358, 27, 303, "Input"],
Cell[7847, 239, 136, 2, 47, "Input"],
Cell[7986, 243, 142, 2, 47, "Input"],
Cell[8131, 247, 144, 2, 47, "Input"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{
Cell[8324, 255, 30, 0, 61, "Section"],
Cell[8357, 257, 16, 0, 28, "Text"],
Cell[8376, 259, 593, 10, 233, "Input"],
Cell[8972, 271, 51, 1, 47, "Input"],
Cell[9026, 274, 39, 0, 28, "Text"],
Cell[9068, 276, 102, 2, 47, "Input"]
}, Open  ]],

Cell[CellGroupData[{
Cell[9207, 283, 70, 0, 61, "Section"],
Cell[9280, 285, 369, 6, 156, "Input"],
Cell[9652, 293, 60, 1, 47, "Input"],
Cell[9715, 296, 42, 1, 47, "Input"],
Cell[9760, 299, 142, 3, 47, "Input"],
Cell[9905, 304, 135, 3, 47, "Input"],
Cell[10043, 309, 56, 1, 47, "Input"],
Cell[10102, 312, 102, 3, 28, "Text"],
Cell[10207, 317, 58, 1, 47, "Input"],
Cell[10268, 320, 62, 1, 47, "Input"],
Cell[10333, 323, 67, 1, 47, "Input"],
Cell[10403, 326, 63, 1, 47, "Input"]
}, Open  ]]
}
]
*)



(*******************************************************************
End of Mathematica Notebook file.
*******************************************************************)

