/**
 * Room.java
 * 
 * $Revision: 1.4 $
 * 
 * $Log: Room.java,v $
 * Revision 1.4  2004-04-23 09:43:14  mphilips
 * *** empty log message ***
 *
 * Revision 1.3  2004/04/01 14:58:48  thomase
 * *** empty log message ***
 *
 * Revision 1.2  2004/03/28 15:20:58  tobibobi
 * Rettede nogle bugs i meeting og tilf�jede enter func i room
 * Tilf�jet et par collections yderligere.
 * LokaleRessourcer blev til Ressourcer..
 *
 * Revision 1.1  2004/03/26 01:32:50  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 *
 *  
 */

import java.util.*;

public class Room {
    private String name;
    private int id;
    private Vector containingPersons;
    private SecurityGroup secGrp;
    private Vector ressources;
    private Calendar calendar;
        
    public Room(String name, int id, SecurityGroup grp) {
	this.name = name;
	this.id = id;
	secGrp = grp;
	containingPersons = new Vector();
	ressources = new Vector();
	calendar = new Calendar();
    }

    public void addRessource(Ressource res) {
	ressources.add(res);
    }

    public Enumeration getRessources() {
	return ressources.elements();
    }
    public void remRessource(Ressource res) {

    }
    public int getId() {
	return id;
    }

    public String getName() {
	return name;
    }
    
    public void  book(Person owner, Date start, Date end) {
	Reservation reservation = new Reservation(owner, this, start, end);
	calendar.addReservation(reservation);
    }

    public boolean enter(int personId, int doorId) {		
	ServiceFactory srfc = ServiceFactory.getInstance();
	Door door = srfc.findDoor(doorId);
	Person person = srfc.findPerson(personId);
	
	// test to see if user exists
	if(person == null) return false;

	// test to see if user has access;
	if(secGrp.isUserValid(person)) {
	    door.unlock();
	    person.enterRoom(this);
	    containingPersons.add(person);
	    return true;
	}
	return false;
    }
    
    public void exit(Person person) {
	containingPersons.remove(person);
    }

    public void setSecurityGroup(SecurityGroup grp) {
	secGrp = grp;
    }
    
    public SecurityGroup getSecurityGroup() {
	return secGrp;
    }
}
