import java.util.ArrayList;

/*
 * Created on 06-04-2004
 *
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */

/**
 * @author Kristina Aggergaard
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class SecurityGroup 
{
	private SecurityGroup scrGrpParent;
	private ArrayList scrGrpList, prsList;
	private boolean checkChildTest;

	public SecurityGroup()
		{
			
			scrGrpList = new ArrayList();
			prsList = new ArrayList();
		}
	
	
	public void setParent(SecurityGroup scrGrpParent)
	{
		this.scrGrpParent = scrGrpParent;
	}
	
	public void removeSecurityGroup(int index)
	{
		scrGrpList.remove(index);
	}
	public void addPerson(Person prs)
	{
		prsList.add(prs);
	}
	public void removePerson(int index)
	{
		prsList.remove(index);
	}
	
	
	public SecurityGroup getScrGrpParent()
	{
		return scrGrpParent;
	}
	
	///////////////////////////////////////////////////////////////////////////
	public void addSecurityGroup(SecurityGroup scrGrp) throws ChildIsParentException 
	{
		if (!checkChild(scrGrp,this))
		{
			throw new ChildIsParentException();
		}
		else
		{
			scrGrp.setParent(this);
			scrGrpList.add(scrGrp);
		}
	}
	private boolean checkChild(SecurityGroup scrGrp, SecurityGroup parent)
	{
		if(parent == null)//Hvis vi er i f�rste knude
		{
			
			checkChildTest = true;
		}
		else if (parent == scrGrp)//Hvis barnet er for�ldre, cirkul�r tr�
		{
			checkChildTest = false;
		}
		else
		{
			parent = parent.getScrGrpParent();
			checkChild(scrGrp, parent);
		}
		return checkChildTest;//kommer aldrig her!		
	}
}
