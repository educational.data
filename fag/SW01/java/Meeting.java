/**
 * Meeting.java
 * 
 * $Revision: 1.4 $
 * 
 * $Log: Meeting.java,v $
 * Revision 1.4  2004-04-02 07:06:47  tobibobi
 * Performed a series of updates. dont know which now :-((((( , but they work.
 * Most classes should now be package private.
 *
 * Added an Arrival controller.
 *
 * Revision 1.3  2004/03/28 15:20:58  tobibobi
 * Rettede nogle bugs i meeting og tilf�jede enter func i room
 * Tilf�jet et par collections yderligere.
 * LokaleRessourcer blev til Ressourcer..
 *
 * Revision 1.2  2004/03/27 10:05:14  tobibobi
 * Implemented Person collection in ServiceFactory
 *   (Person)
 * Created SecurityGroup and started implementing - still missing sub containers
 * Created Makefile which implements JUnit
 * Created JUnit tests.
 *
 * Revision 1.1  2004/03/26 01:32:49  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 *
 * 
 */

import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.Iterator;

class Meeting extends Event {
	
    private int id;
    private Map participants;
    private Map reservations;
    
    public Meeting(Person owner, Date start, Date end, int id) {
	setOwner(owner);
	setStart(start);
	setEnd(end);

	this.id = id;

	participants = new HashMap();
	reservations = new HashMap();		
    }
    
    public int getId() {
	return id;
    }
    
    public void addParticipant(Person person) {
	participants.put(new Integer(person.getId()), person);
    }
    
    public void sendInvitations() {
	Iterator ittr = participants.entrySet().iterator();
	while (ittr.hasNext()) {
	    Person person = (Person)ittr.next();
	    person.inviteTo(this);
	}
	owner.addToCalendar(this);
    }	
}
