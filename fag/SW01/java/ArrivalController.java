
public class ArrivalController {
    // D�ren modtager et signal om at en bruger gerne �nsker 
    // at tr�de ind i lokalet.
    public void signalEntry(int pid, int rid, int did) {
	ServiceFactory srfc = ServiceFactory.getInstance();
	Room room = srfc.findRoom( rid );
	room.enter( pid, did );
    }

    public void signalExit(int pid, int rid,int did) {
	ServiceFactory srfc = ServiceFactory.getInstance();
	Room room = srfc.findRoom( rid );
	Person person = srfc.findPerson( pid );
	room.exit( person );
    }
}
