/**************************************************************
 **
 ** SecurityTest
 **
 ** $Log: SecurityTest.java,v $
 ** Revision 1.2  2004-03-29 11:55:15  thomase
 ** Tilf�jet test for cykliske referencer
 **
 ** Revision 1.1  2004/03/29 08:50:46  tobibobi
 ** Har opdateret makefiler s�ledes at der nu underst�ttes et katalog med
 ** tests
 ** Har oprettet en arrival controller
 ** rettet i collection servicefactory for at underst�tte rooms
 **
 ** Revision 1.2  2004/03/28 15:20:58  tobibobi
 ** Rettede nogle bugs i meeting og tilf�jede enter func i room
 ** Tilf�jet et par collections yderligere.
 ** LokaleRessourcer blev til Ressourcer..
 **
 ** Revision 1.1  2004/03/27 18:34:24  tobibobi
 ** Glemte at tilf�je SecurityTest til cvs
 **
 **************************************************************/
import junit.framework.*;
import java.util.*;

public class SecurityTest extends TestCase {
    private SecurityGroup sec;
    private Person pers;
    public void setUp() {
	sec = new SecurityGroup(0);
	pers = new Person("Thomas",0);
	
    }

    public void testAddMember() {
	sec.addMember(pers);
	assertTrue("FirstePerson not found ",sec.getUsers().nextElement().equals(pers));
    }
    public void testAddChild() {
	SecurityGroup sec2 = new SecurityGroup(1);
	sec.addMember(sec2);
	assertTrue("Security group not found", sec.getChildren().nextElement().equals(sec2));

	SecurityGroup sec3 = new SecurityGroup(2);
	sec.addMember(sec3);

	Enumeration  e = sec.getChildren();
	assertTrue("Security group 2 not found", e.nextElement().equals(sec2));
	assertTrue("Security group 3 not found", e.nextElement().equals(sec3));

	sec3.addMember(sec);
	try {
	    assertFalse("Cyclic reference between <sec> and <sec3>", sec3.getChildren().nextElement().equals(sec));
	    // fail ("Should raise an NoSuchElementException");
	} catch (NoSuchElementException succes) {}

	sec2.addMember(sec3);
	assertTrue("Security group 3 not found as child of group 2", ((SecurityGroup)sec.getChildren().nextElement()).getChildren().nextElement().equals(sec3));	

    }
    
    public void testIsUserValid() {
	sec.addMember(pers);
	assertTrue("User not found in level secgroup",sec.isUserValid(pers));
	SecurityGroup sec2 = new SecurityGroup(2);
	sec.addMember(sec2);
	Person pers2 = new Person("Ib",1);
	sec2.addMember(pers2);
	assertTrue("User not found in child",sec.isUserValid(pers2));
	assertFalse("Wrong return on unknown user",sec.isUserValid(new Person("Flenard",3)));
    }
}
