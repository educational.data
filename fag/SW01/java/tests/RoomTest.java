import junit.framework.*;

public class RoomTest extends TestCase {
    public void testConstructor() {
	SecurityGroup grp = new SecurityGroup(0);
	Room rm = new Room("SeminarRum",1,grp);
	assertTrue("sec not set correct",rm.getSecurityGroup() == grp);
	assertTrue("Name not correct",rm.getName() == "SeminarRum");
	assertTrue("Id not correct",rm.getId() == 1);
    }

    public void testEnter() {
	ServiceFactory srfc = ServiceFactory.getInstance();
	Person person = new Person("Tobias",112);
	Door door = new Door(1);
	srfc.addPerson(person);
	srfc.addDoor(door);
	SecurityGroup grp = new SecurityGroup(0);
	grp.addMember(person);

	Room rm = new Room("SeminarRum",1,grp);
	assertTrue("User has no access",rm.enter(112,1) == true);
	assertTrue("door is still locked",door.getStatus() == Door.UNLOCKED);
    }
}
