import junit.framework.*;

public class CollectionTest extends TestCase {
    public void testPersonCollection() {
	Person person = new Person("Thomas", 0);		
	ServiceFactory fact = ServiceFactory.getInstance();

	fact.addPerson(person);
	
	fact.addPerson(new Person("Tobias", 1));
	
	fact.addPerson(new Person("Ib", 2));

	assertEquals("Person ikke korrekt i liste", person, (Person)fact.findPerson(0));
	
    }
}
