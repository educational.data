import junit.framework.*;

public class ArrivalControllerTest extends TestCase {
    ArrivalController cont;
    ServiceFactory srfc;
	
    Person thomas;
    SecurityGroup dummyGruppe;
    Room seminarrum;
    Door dummyDoor;
    public void setUp() {
	cont = new ArrivalController();
	srfc = ServiceFactory.getInstance();

	srfc.init();
	
	thomas = new Person("Thomas",1);
	dummyGruppe = new SecurityGroup(1);
	seminarrum = new Room("SeminarRum",1,dummyGruppe);
	dummyDoor = new Door(1);

	srfc.addDoor(dummyDoor);
	srfc.addRoom(seminarrum);
	srfc.addPerson(thomas);
    }

    public void testEntrySignal() {	
	cont.signalEntry(1,1,1);
	// hvis ikke brugeren er i gruppen, skal den forblive l�st
	assertTrue("D�r blev �bnet", dummyDoor.getStatus() == Door.LOCKED);

	// vi tilf�jer brugeren til sikkerhedsgruppen 
	// og sikrer sig at han har adgang
	dummyGruppe.addMember(thomas);

	cont.signalEntry(1,1,1);
	assertTrue("D�r blev ikke �bnet", dummyDoor.getStatus() == Door.UNLOCKED);
	assertTrue("Personen er ikke relokeret",thomas.getLocation() == seminarrum);
    }
}
