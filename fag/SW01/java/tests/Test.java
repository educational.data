import junit.framework.*;

public class Test {
    public static void main(String[] args) {
	
    }
    public static TestSuite suite() {
	TestSuite suite = new TestSuite("main");
	suite.addTestSuite(CollectionTest.class);
	suite.addTestSuite(PersonTest.class);
	suite.addTestSuite(SecurityTest.class);
	suite.addTestSuite(RoomTest.class);
	suite.addTestSuite(ArrivalControllerTest.class);
	return suite;
    }
}
