/**
 * Event.java
 * 
 * $Revision: 1.4 $
 * 
 * $Log: Event.java,v $
 * Revision 1.4  2004-04-02 07:06:47  tobibobi
 * Performed a series of updates. dont know which now :-((((( , but they work.
 * Most classes should now be package private.
 *
 * Added an Arrival controller.
 *
 * Revision 1.3  2004/04/01 14:57:42  thomase
 * Ingen ting �ndret :-)
 *
 * Revision 1.2  2004/04/01 13:56:09  tobibobi
 * *** empty log message ***
 *
 * Revision 1.1  2004/03/26 01:32:49  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 *
 *  
 */

import java.util.Date;

abstract class Event {
    protected Date start;
    protected Date end;
    protected Person owner;
    
    protected void setStart(Date start) { this.start = start; }
    public Date getStart() { return start; }
    
    protected void setEnd(Date end) { this.end = end; }
    public Date getEnd() { return end; }
    
    protected void setOwner(Person owner) { this.owner = owner; }
    public Person getOwner() { return owner; }
}
