/**********************************************
 **
 ** SecurityGroup
 **
 ** $Log: SecurityGroup.java,v $
 ** Revision 1.4  2004-03-29 11:56:21  thomase
 ** Tilf�jet tjeck for cykliske referencer i addMember for grupper
 **
 ** Revision 1.3  2004/03/28 15:20:58  tobibobi
 ** Rettede nogle bugs i meeting og tilf�jede enter func i room
 ** Tilf�jet et par collections yderligere.
 ** LokaleRessourcer blev til Ressourcer..
 **
 ** Revision 1.2  2004/03/27 18:33:01  tobibobi
 ** Rettede isUserValid i SecurityGroup s�ledes at den nu detekterer
 ** medlemmer af gruppen korrekt.
 **
 ** Test til SecurityGroup added
 **
 *********************************************/

import java.util.Vector;
import java.util.Enumeration;

class SecurityGroup {
    private Vector children;
    private Vector users;
    private int id;

    public SecurityGroup(int idnum) {
	id = idnum;
	children = new Vector();
	users = new Vector();
    }

    public int getId() {
	return id;
    }

    public Enumeration getUsers() {
	return users.elements();
    }

    public Enumeration getChildren() {
	return children.elements();
    }

    public void addMember(SecurityGroup secChild) {
	// first ensure that secChild is not containing a
	// reference to ``this''
	if (secChild.ensureNonCyclic(this)) children.add(secChild);
    }

    private  boolean ensureNonCyclic(SecurityGroup root) {
	//
	if (root.getId() == id) return false;

	//
	for (Enumeration e=children.elements(); e.hasMoreElements();) {
	    if (!((SecurityGroup)e.nextElement()).ensureNonCyclic(root))
		return false;
	}

	// everything is fine
	return true;
    }

    public void addMember(Person pers) {
	users.add(pers);
    }
    
    public boolean isUserValid(Person pers) {
	// first search own collection to see if its there
	if(users.contains(pers)) return true;
	
	// see if any collections below us owns the user
	for (Enumeration e=children.elements(); e.hasMoreElements();) {
	    if (((SecurityGroup)e.nextElement()).isUserValid(pers))
		return true;
	}

	// nothing found
	return false;	    
    }
}
