
class Door {
    static int LOCKED = 0;
    static int UNLOCKED = 1;

    private int id;
    private int status;

    public Door(int id) {
	this.id = id;
	status = LOCKED;
    }

    public int getId() {
	return id;
    }

    public void unlock() {
	status = UNLOCKED;
    }

    public void lock() {
	status = LOCKED;
    }
    public int getStatus() {
	return status;
    }
}
