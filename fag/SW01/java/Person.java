/**
 * Person.java
 * 
 * $Revision: 1.5 $
 * 
 * $Log: Person.java,v $
 * Revision 1.5  2004-04-02 07:06:47  tobibobi
 * Performed a series of updates. dont know which now :-((((( , but they work.
 * Most classes should now be package private.
 *
 * Added an Arrival controller.
 *
 * Revision 1.4  2004/03/28 15:20:58  tobibobi
 * Rettede nogle bugs i meeting og tilf�jede enter func i room
 * Tilf�jet et par collections yderligere.
 * LokaleRessourcer blev til Ressourcer..
 *
 * Revision 1.3  2004/03/27 18:33:01  tobibobi
 * Rettede isUserValid i SecurityGroup s�ledes at den nu detekterer
 * medlemmer af gruppen korrekt.
 *
 * Test til SecurityGroup added
 *
 * Revision 1.2  2004/03/27 10:05:14  tobibobi
 * Implemented Person collection in ServiceFactory
 *   (Person)
 * Created SecurityGroup and started implementing - still missing sub containers
 * Created Makefile which implements JUnit
 * Created JUnit tests.
 *
 * Revision 1.1  2004/03/26 01:32:50  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 *
 *  
 */

public class Person {
    private String name;
    private int id;
    private Room location;
    
    public Person(String name, int id) {
	this.name = name;
	this.id = id;
    }
    
    public void inviteTo(Event event) {		
    
    }	
    
    public void addToCalendar(Event event) {		
    
    }
    
    public void enterRoom(Room room) {		
	exitRoom();
	location = room;
    }
    
    public void exitRoom() {
	if(location != null) {
	    Room bufLoc = location;
	    location = null;
	    bufLoc.exit(this);
	}
    }

    public Room getLocation() {
	return location;
    }
    
    public int getId() { 
	return id; 
    }

    public String getName() {
	return name;
    } 
    
    public boolean equals(Person pers) {
	if(pers.getId() == getId() && pers.getName() == getName()) return true;
	else return false;
    }
}
