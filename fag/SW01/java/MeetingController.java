/**
 * MeetingController.java
 * 
 * $Revision: 1.3 $
 * 
 * $Log: MeetingController.java,v $
 * Revision 1.3  2004-04-02 07:06:47  tobibobi
 * Performed a series of updates. dont know which now :-((((( , but they work.
 * Most classes should now be package private.
 *
 * Added an Arrival controller.
 *
 * Revision 1.2  2004/03/28 15:28:43  tobibobi
 * Rettede lidt referencer til servicefactory fra meeting
 *
 * Revision 1.1  2004/03/26 01:32:49  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 * 
 * 
 */

import java.util.Date;

public class MeetingController {
    
    private Meeting meeting;
    
    public MeetingController() {	
    }
    
    public void makeMeeting(int pid, Date start, Date end) {
	
	Person owner = ServiceFactory.getInstance().findPerson(pid);
	if (owner != null) {
	    meeting = new Meeting(owner, start, end, pid);
	}		
    }
    
    public void addParticipant(int id) {
	if (meeting != null) {
	    Person person = ServiceFactory.getInstance().findPerson(id);
	    if (person != null) {
		meeting.addParticipant(person);
	    }
	}
    }
    
    public void sendInvitations() {
	if (meeting != null) {
	    meeting.sendInvitations();
	    ServiceFactory.getInstance().addMeeting(meeting);
	}
    }	
}
