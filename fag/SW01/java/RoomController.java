/**
 * RoomController.java
 * 
 * $Revision: 1.3 $
 * 
 * $Log: RoomController.java,v $
 * Revision 1.3  2004-04-02 07:06:47  tobibobi
 * Performed a series of updates. dont know which now :-((((( , but they work.
 * Most classes should now be package private.
 *
 * Added an Arrival controller.
 *
 * Revision 1.2  2004/04/01 14:58:49  thomase
 * *** empty log message ***
 *
 * Revision 1.1  2004/03/26 01:32:50  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 *
 *  
 */

import java.util.Date;

public class RoomController {
	
	public RoomController() {	
	}
	
	public void queryRoom() {	
	}
	
	public void bookRoom(int pid, int rid, Date start, Date end) {
	    Person owner = ServiceFactory.getInstance().findPerson(pid);
	    Room room = ServiceFactory.getInstance().findRoom(rid);
	    room.book(owner, start, end);
	}	
}
