/**
 * ServiceFactory.java
 * 
 * $Revision: 1.5 $
 * 
 * $Log: ServiceFactory.java,v $
 * Revision 1.5  2004-04-02 07:06:47  tobibobi
 * Performed a series of updates. dont know which now :-((((( , but they work.
 * Most classes should now be package private.
 *
 * Added an Arrival controller.
 *
 * Revision 1.4  2004/03/29 08:50:46  tobibobi
 * Har opdateret makefiler s�ledes at der nu underst�ttes et katalog med
 * tests
 * Har oprettet en arrival controller
 * rettet i collection servicefactory for at underst�tte rooms
 *
 * Revision 1.3  2004/03/28 15:20:58  tobibobi
 * Rettede nogle bugs i meeting og tilf�jede enter func i room
 * Tilf�jet et par collections yderligere.
 * LokaleRessourcer blev til Ressourcer..
 *
 * Revision 1.2  2004/03/27 10:05:14  tobibobi
 * Implemented Person collection in ServiceFactory
 *   (Person)
 * Created SecurityGroup and started implementing - still missing sub containers
 * Created Makefile which implements JUnit
 * Created JUnit tests.
 *
 * Revision 1.1  2004/03/26 01:32:50  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 * 
 * 
 */

import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.util.ArrayList;

public class ServiceFactory {
    // singleton implementation
    private static ServiceFactory instance;
    
    private static Map persons;
    private static Map doors;
    private static Map rooms;	
    private static Map meetings;
    
    private ServiceFactory() {
	init();
    }

    public void init() {
	persons = new HashMap();
	meetings = new HashMap();
	doors = new HashMap();
	rooms = new HashMap();
    }
    
    // implementation of singleton
    public static synchronized ServiceFactory getInstance() {
	if (instance == null) {
	    instance = new ServiceFactory();
	}
	return instance;
    }

    // Person collection
    public Person findPerson(int id) {
	return (Person)persons.get(new Integer(id));
    }

    public void addPerson(Person pers) {
	persons.put(new Integer(pers.getId()), pers);
    }

    // Door collection
    public void addDoor(Door door) {
	doors.put(new Integer(door.getId()),door);
    }
    
    public Door findDoor(int id) {
	return (Door)doors.get(new Integer(id));
    }

    //Room collection
    public void addRoom(Room room) {
	rooms.put(new Integer(room.getId()),room);
    }
    
    public Room findRoom(int roomId) {
	return (Room)rooms.get(new Integer(roomId));
    }

    //Meeting collection
    public void addMeeting(Meeting meet) {
	meetings.put(new Integer(meet.getId()),meet);
    }

    public Meeting findMeeting(int id) {
	return (Meeting)meetings.get(new Integer(id));
    }
}
