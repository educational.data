/**
 * reservation.java
 * 
 * $Revision: 1.3 $
 * 
 * $Log: Reservation.java,v $
 * Revision 1.3  2004-04-01 14:57:12  thomase
 * Tilf�jet en konstruktor
 *
 * Revision 1.2  2004/04/01 13:56:09  tobibobi
 * *** empty log message ***
 *
 * Revision 1.1  2004/03/26 01:32:50  thomase
 * * De f�rste klasser er oprettet og en lille smule kode er tilf�jet.
 *
 *  
 */

import java.util.Date;

public class Reservation extends Event {
	
    private Room room;

    public Reservation(Person owner, Room room, Date start, Date end) {
	this.owner = owner;
	this.room = room;
	this.start = start;
	this.end = end;    
    }	
}
