R=3E3
C=0.5E-6
V=5
I0=V/R

tau=R*C;
t=0:0.0001:0.01;
Ic=I0*exp(-t/tau);
Vr=Ic*R;
active = figure(1)
hold on
title('Sp�nding over Modstand');
plot(t,Vr)