
/********************************************
 *
 * file to include all the simplest, but most 
 * needed functionalities
 *
 * $Id: defaults.h,v 1.3 2005-12-11 16:48:02 tobibobi Exp $
 * $Log: defaults.h,v $
 * Revision 1.3  2005-12-11 16:48:02  tobibobi
 * *** empty log message ***
 *
 * Revision 1.2  2005/12/10 15:16:35  tobibobi
 * *** empty log message ***
 *
 * Revision 1.1  2005/12/06 15:26:31  tobibobi
 * *** empty log message ***
 *
 *
 ********************************************/

#ifndef DEFAULTS_H
#define DEFAULTS_H 1

#define FAILED(x) (x < 0)
#define SUCCESS(x) (x >= 0)
#define E_FAIL -1
#define OK_SUCCESS 0
#ifdef DO_DEBUG

# include <stdio.h>
# include <iostream>
# include <unistd.h>
# define BASEPRINT std::cout << "[" << __FILE__ <<  ":" << __LINE__ <<"] "
# define PRINT0(s) BASEPRINT; printf(s)
# define PRINT1(s,x) BASEPRINT; printf(s,x)
# define PRINT2(s,x,y) BASEPRINT; printf(s,x,y)
# define PRINT3(s,x,y,z) BASEPRINT; printf(s,x,y,z)
# define PRINT4(s,x,y,z,h) BASEPRINT; printf(s,x,y,z,h)
# define PRINT5(s,x,y,z,h,i) BASEPRINT; printf(s,x,y,z,h,z)
# define PRINT6(s,x,y,z,h,i,j) BASEPRINT; printf(s,x,y,z,h,z,i)
# define PERROR(s) BASEPRINT; perror(s)
#else
# define PRINT0(s)
# define PRINT1(s,x)
# define PRINT2(s,x,y)
# define PRINT3(s,x,y,z)
# define PRINT4(s,x,y,z,h)
# define PRINT5(s,x,y,z,h,i)
# define PRINT6(s,x,y,z,h,i,j)
# define PERROR(s)
#endif

typedef unsigned long ulong;
typedef unsigned int uint;
typedef unsigned char uchar;
typedef unsigned char ubyte;
typedef char byte;

#endif
