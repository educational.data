
/***********************************************
 ** 
 ** this file provides a more generic interface 
 ** to thread programming.
 ** it will mainly use pthreads, but will later 
 ** be rearanged to use other types.
 **
 ** $Id: thread.cpp,v 1.1 2005-12-11 16:55:50 tobibobi Exp $
 **
 ** $Log: thread.cpp,v $
 ** Revision 1.1  2005-12-11 16:55:50  tobibobi
 ** *** empty log message ***
 **
 ** Revision 1.1  2005/12/11 16:51:56  tobibobi
 ** *** empty log message ***
 **
 ** Revision 1.2  2005/12/11 16:12:07  tobibobi
 ** *** empty log message ***
 **
 ** Revision 1.1  2005/12/10 15:59:50  tobibobi
 ** *** empty log message ***
 **
 ** Revision 1.2  2005/12/10 15:16:36  tobibobi
 ** *** empty log message ***
 **
 ** Revision 1.1  2005/12/06 15:26:32  tobibobi
 ** *** empty log message ***
 **
 ************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "../defaults.h"

#include <pthread.h>
#include <sys/time.h>
#include <errno.h>

#include "thread.H"


struct Mutex::data {
  pthread_mutex_t mutex;
};

Mutex::Mutex() {
  pData = new data;
  pthread_mutex_init(&(pData->mutex), NULL);
}

Mutex::~Mutex() {
  pthread_mutex_destroy( &(pData->mutex) );
  delete pData;
}

void Mutex::lock() {
  pthread_mutex_lock ( &(pData->mutex) );
}

void Mutex::unlock() {
  pthread_mutex_unlock ( &(pData->mutex)  );
}

struct Condition::data {
  pthread_cond_t waitCondition;
  pthread_mutex_t mutex;
};

Condition::Condition() {
  pData = new data;
  pthread_mutex_init(&(pData->mutex), NULL);

  pthread_cond_init ( & (pData->waitCondition) , NULL );
}
Condition::~Condition() {
  pthread_cond_destroy(&(pData->waitCondition));
  pthread_mutex_destroy(&(pData->mutex));
  delete pData;
}
  
void Condition::signal() {
  pthread_mutex_lock ( &( pData->mutex ) );
  pthread_cond_signal(&(pData->waitCondition));
  pthread_mutex_unlock ( &(pData->mutex) );
}
void Condition::wait() {
  pthread_mutex_lock ( &( pData->mutex ) );
  pthread_cond_wait(&( pData->waitCondition ), &( pData->mutex ));
  pthread_mutex_unlock ( &(pData->mutex) );
}

int Condition::waitForMSecs(int msecs) {
  struct timeval now;
  struct timezone tz;
  struct timespec timeout;

  pthread_mutex_lock ( &( pData->mutex ) );
  gettimeofday ( &now, &tz );
  timeout.tv_sec = now.tv_sec + msecs / 1000;
  timeout.tv_nsec = now.tv_usec * 1000 + msecs % 1000;
  
  int ret = OK_SUCCESS;
  if(pthread_cond_timedwait ( &( pData->waitCondition ),
			      &( pData->mutex ), &timeout ) == ETIMEDOUT) {
    ret = E_FAIL;
  }
  pthread_mutex_unlock ( &(pData->mutex) );
  return ret;
}

struct Thread::data {
  pthread_t thread;
};
Thread::Thread() {
  pData = new data;
}

Thread::~Thread() {
  delete pData;
}

void Thread::create(threadFn func, void *data) {
  pthread_create ( &(pData->thread), NULL, func, data );
}
