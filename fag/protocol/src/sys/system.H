
/************************************************
 *
 * mem.h
 *
 * This fomerly wrapped memory functionality.
 * Now it also includes more generic system inter
 * faces like time.
 * 
 * $Id: system.H,v 1.1 2005-12-11 16:55:50 tobibobi Exp $
 * $Log: system.H,v $
 * Revision 1.1  2005-12-11 16:55:50  tobibobi
 * *** empty log message ***
 *
 * Revision 1.1  2005/12/11 16:51:56  tobibobi
 * *** empty log message ***
 *
 * Revision 1.2  2005/12/10 17:42:22  tobibobi
 * *** empty log message ***
 *
 * Revision 1.1  2005/12/10 16:00:58  tobibobi
 * *** empty log message ***
 *
 * Revision 1.1  2005/12/10 15:16:36  tobibobi
 * *** empty log message ***
 *
 ***********************************************/

#ifndef MEM_H
#define MEM_H 1

#include "../defaults.h"

class System {
public:
  static void free ( void *obj );
  static void *malloc ( int size );
  static void memset ( void *data, char val, int size );
  
  static ulong getTime (  );
};

#endif
