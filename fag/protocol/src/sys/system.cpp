
/************************************************
 *
 * system.c
 *
 * Memory may be handled by special functions
 * later on.
 * Maybe even omitted.
 *
 * $Id: system.cpp,v 1.2 2005-12-11 17:16:55 tobibobi Exp $
 ***********************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifdef HAVE_MALLOC
# include <malloc.h>
#endif


#include <sys/time.h>

#include "../defaults.h"
#include "system.H"
void *
System::malloc ( int size )
{
#ifdef HAVE_MALLOC
  return::malloc ( size );
#else
# error Must compile with malloc - manual alloc not made yet
#endif
}

void
System::free ( void *obj )
{
#ifdef HAVE_MALLOC
  ::free ( obj );
#else
# error Must compile with malloc - manual alloc (free) not made yet
#endif
}


void
System::memset ( void *data, char val, int size )
{
  /*
     strongly optimised code section
   */
  register int pos;
  register unsigned char *dt = ( unsigned char * ) data;
  register int sz = size;

  for ( pos = 0; pos < sz; ++pos ) {
    *dt++ = val;
  }
}


ulong
System::getTime (  )
{
  struct timeval tp;

  gettimeofday ( &tp, NULL );

  return tp.tv_sec * 10000 + tp.tv_usec / 100;
}
