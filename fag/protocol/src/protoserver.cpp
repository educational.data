
/******************************
 * $Id: protoserver.cpp,v 1.6 2005-12-11 16:23:34 tobibobi Exp $
 ******************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include "net/netselector.H"


using namespace std;

const int hostId = 0;
const int portId = 2000;

int
main (  )
{
  Netselector net(hostId);

  cout << " \e[32m*\e[0m \e[1mWaiting for data\e[0m \t" << flush;
  sleep ( 30 );
  cout << "[\e[32mDone\e[0m]" << endl;

  return 0;
}
