#include "netselector.H"

#include "udp/protocol.H"
#include "udp/socket.H"

#include <iostream>

using namespace std;

struct Netselector::data {
  UDPProtocol *protocol;
};

Netselector::Netselector(const int id) {
  pData = new data;
  pData->protocol = new UDPProtocol(1000,2000,id);
}

Netselector::~Netselector() {
  delete pData->protocol;
  delete pData;
}

Socket *
Netselector::getConnection(const int id) {
  return pData->protocol->getSocket(id);
}
