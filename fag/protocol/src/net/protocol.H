#ifndef PROTOCOL_H
#define PROTOCOL_H 1

class Socket;

class Protocol {
public:
  virtual Socket *getSocket(const int reciever) = 0;
};

#endif
