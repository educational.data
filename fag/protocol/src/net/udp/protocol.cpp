
/************************************************
 *
 * protocol.c
 *
 * This file explains the basics of the protocol
 * It will probally later include a higherlevel
 * implementation of a async and isync protocol
 *
 * $Id: protocol.cpp,v 1.5 2005-12-13 14:16:03 tobibobi Exp $
 ***********************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "protocol.H"
#include "src/sys/system.H"

#include "packet.h"
#include "socket.H"

#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/uio.h>
#include <sys/socket.h>
#include <errno.h>


/**
 *
 */
UDPProtocol::UDPProtocol ( const int bufferSize, const int port, const ubyte sender ) :
  outBuffer ( bufferSize ),
  inBuffer ( bufferSize ),
  outPacketBuffer ( (void*)((uint) outBuffer.pOffset+sizeof(packet)) )
{
  status = PROT_CLOSED;
  internalPort = port;
  senderId = sender;

  internalRcvSocket = socket ( PF_INET, SOCK_DGRAM, IPPROTO_UDP );
  if ( FAILED ( internalRcvSocket ) ) {
    PERROR ( "createProtocol(protocol.c -rcv )" );
  }


  internalSndSocket = socket ( PF_INET, SOCK_DGRAM, IPPROTO_UDP );
  if ( FAILED ( internalSndSocket ) ) {
 ( "createProtocol(protocol.c -snd )" );
  }
  doBind ( internalPort );
  for ( int i = 0; i < 20; i++ ) {
    sockets[i] = 0L;
  }
  recv_thread.create(threadRcvFn,this);
}

/*!
 * Shuts down the protocol.
 * it also makes sure that all open sockets is shut down. After
 * the protocol has been shut down, the memory will be cleaned.
 *
 * \param proto the protocol to be shut down.
 */
UDPProtocol::~UDPProtocol (  )
{
  for (int i=0;i<MAX_SOCKET_COUNT;i++)
    if(sockets[i] != NULL)
      delete sockets[i];

  close ( internalRcvSocket );
  close ( internalSndSocket );
}


void *
UDPProtocol::threadRcvFn ( void *data )
{
  UDPProtocol *dproto = ( UDPProtocol * ) data;

  dproto->handleRecieving (  );
  return 0;
}

void
UDPProtocol::handleRecieving (  )
{
  size_t size;

  int socknum = internalRcvSocket;

  for ( ; ; ) {

    // size = recv ( socknum, inBuffer.pOffset, ( size_t ) inBuffer.iSize, 0 );
    sockaddr_in addr;
    {
      msghdr hdr;
      hdr.msg_name = (void*)&addr;
      hdr.msg_namelen = sizeof(sockaddr_in);
      iovec dhdr;
      dhdr.iov_base = inBuffer.pOffset;
      dhdr.iov_len = inBuffer.iSize;
      hdr.msg_iov = &dhdr;
      hdr.msg_iovlen = 1;
#ifdef __CYGWIN__
      hdr.msg_accrights=0;
      hdr.msg_accrightslen=0;
#else
      hdr.msg_control = 0;
      hdr.msg_controllen = 0;
      hdr.msg_flags = 0;
#endif
      size = recvmsg(socknum,&hdr, 0);
      if(FAILED(size)) {
	perror("protocol.cpp");
      }
    }

    if ( size == -1 ) {
      if ( errno == EFAULT ) {
	PRINT2 ( "Bad adress, address = %d,socknum = %d\n", inBuffer.pOffset,
		 socknum );
	return;
      } else {
	PERROR ( "read" );
      }
    } else {
      struct packet *header = ( packet * ) inBuffer.pOffset;
      ((UDPSocket *)getSocket(header->sender))->hostip = ntohl(addr.sin_addr.s_addr);
      if ( header->reciever == senderId ) {
	bool b_ack = ( header->flags & PACFL_ACK );
	bool b_timer = ( header->flags & PACFL_TIMER );

	// first examine if we must send a time packet
	if ( b_timer && !b_ack ) {
	  sendTimeReply ( header->sender );
	} else {
	  ((UDPSocket *)getSocket(header->sender))->protocolEvent ( );
	}
      } /* else {
	PRINT0("I got a packet that was not for me\n");
	} */
    }
  }
}

void
UDPProtocol::socketHandler ( packet & header )
{
  sockets[header.sender]->protocolEvent ( );
}

void
UDPProtocol::sendTimeReply ( int sendTo )
{
  PRINT0 ( "Time packet [out]\n" );
  packet *header = ( packet * ) outBuffer.pOffset;

  header->reciever = sendTo;
  header->sender = senderId;
  header->flags = PACFL_TIMER | PACFL_ACK | PACFL_ASYNC;
  header->len = sizeof ( packet ) + sizeof ( timepacket );

  timepacket *tim = ( timepacket * ) ( ( uint ) header + sizeof ( packet ) );

  tim->servertime = System::getTime (  );

  sendData ( ((UDPSocket*)getSocket(sendTo))->hostip );
}


int
UDPProtocol::setUpAdr ( sockaddr_in & addr, ulong host, unsigned short port )
{
  // test to see if connection data is correct
  if ( FAILED ( ( addr.sin_addr.s_addr = htonl ( host ) ) ) )
    return E_FAIL;		// signals failure

  // setup ordinary data
  addr.sin_family = AF_INET;	// host byte order
  addr.sin_port = htons ( port );	// short, network byte order
  System::memset ( &( addr.sin_zero ), '\0', 8 );	// zero the rest of the struct
  return OK_SUCCESS;
}


void
UDPProtocol::doBind ( int port )
{
  int yes = 1;

  // lose the pesky "Address already in use" error message
  int succ = setsockopt ( internalRcvSocket,
			  SOL_SOCKET,
			  SO_REUSEADDR, &yes,
			  sizeof ( int ) );

  if ( FAILED ( succ ) ) {
    PERROR ( "bindProtocol(protocol.c) - setsockopt" );
    exit ( -1 );
  }

  succ = setsockopt ( internalSndSocket,
		      SOL_SOCKET, SO_REUSEADDR, &yes, sizeof ( int ) );
  if ( FAILED ( succ ) ) {
    PERROR ( "bindProtocol(protocol.c) - setsockopt" );
    exit ( -1 );
  }
  // setup sending of broadcast data

  succ =
    setsockopt ( internalSndSocket,
		 SOL_SOCKET, SO_BROADCAST, &yes, sizeof ( int ) );

  if ( FAILED ( succ ) ) {
    PERROR ( "bindProtocol(protocol.c) - setsockopt" );
    exit ( -1 );
  }

  sockaddr_in dest_addr;

  setUpAdr ( dest_addr, INADDR_ANY, port );

  succ =
    bind ( internalRcvSocket, ( sockaddr * ) & dest_addr,
	   sizeof ( sockaddr ) );

  if ( FAILED ( succ ) ) {
    PERROR ( "bindProtocol(protocol.c) - bind - rcv" );
    exit ( -1 );
  }
  // setUpAdr (&dest_addr, INADDR_ANY, 0);

  /*succ =
     bind (internalSndSocket,
     (struct sockaddr *) &dest_addr, sizeof (struct sockaddr)); */

  if ( FAILED ( succ ) ) {

    PERROR ( "bindProtocol(protocol.c) - bind - snd" );
    exit ( -1 );
  }
}



/*!
 * sets up the auxilary data for transmission of packet
 * \param proto this is the protocol
 * \see protocol
 */
void *
UDPProtocol::prepareSendOfPacket ( uint socket, uint len, ubyte flags )
{
  packet & header = ( packet & ) * ( packet * ) outBuffer.pOffset;
  UDPSocket & sock = ( UDPSocket & ) ( *( sockets[socket] ) );

  header.reciever = sock.reciever;
  header.sender = senderId;
  header.flags = flags;
  header.len = len + sizeof ( packet );
  return ( &header ) + sizeof ( packet );
}

/*!
 * This makes the data that is to be transfered,
 * pass out on the protocol stack.
 * a more specialized version may need to be made
 * in order to make isync data transfer more adobtable
 */
void
UDPProtocol::sendData (  )
{
  MutexLock lock = mutex;
  sockaddr_in echoserver;

  int result;

  /* Construct the server sockaddr_in structure */

  System::memset ( &echoserver, 0, sizeof ( echoserver ) );	/* Clear struct */
  echoserver.sin_family = AF_INET;	/* Internet/IP */
  echoserver.sin_addr.s_addr = htonl ( INADDR_BROADCAST );	/* IP address */
  echoserver.sin_port = htons ( 2000 );	/* server port */


  packet *header = ( packet * ) outBuffer.pOffset;

  result = sendto ( internalSndSocket,
		    header,
		    header->len,
		    0, ( struct sockaddr * ) &echoserver,
		    sizeof ( echoserver ) );
  if ( FAILED ( result ) ) {
    PERROR ( "sendData(protocol.c)" );
    exit ( -1 );
  } 
  PRINT0("Still using the wrong version\n");
}

/*!
 * This makes the data that is to be transfered,
 * pass out on the protocol stack.
 * a more specialized version may need to be made
 * in order to make isync data transfer more adobtable
 */
void
UDPProtocol::sendData ( ulong toip )
{
  MutexLock lock = mutex;
  sockaddr_in echoserver;
  long ip;
  int result;
  
  if(toip == 0) {
    ip = INADDR_BROADCAST;
  } else {
    ip = toip;
  }
  /* Construct the server sockaddr_in structure */

  System::memset ( &echoserver, 0, sizeof ( echoserver ) );	/* Clear struct */
  echoserver.sin_family = AF_INET;	/* Internet/IP */
  echoserver.sin_addr.s_addr = htonl ( ip );	/* IP address */
  echoserver.sin_port = htons ( 2000 );	/* server port */


  packet *header = ( packet * ) outBuffer.pOffset;

  result = sendto ( internalSndSocket,
		    header,
		    header->len,
		    0, ( struct sockaddr * ) &echoserver,
		    sizeof ( echoserver ) );
  if ( FAILED ( result ) ) {
    PERROR ( "sendData(protocol.c)" );
    exit ( -1 );
  } 
}

Socket *
UDPProtocol::getSocket(const int socketId) {
  if(sockets[socketId] == NULL) {
    sockets[socketId] = new UDPSocket(*this,socketId);
  }
  return (Socket *)sockets[socketId];
}
