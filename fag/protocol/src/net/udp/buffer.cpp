
/************************************************
 *
 * buffer.cpp
 *
 * using a buffer as a midle layer to be the basics 
 * of the protocol packet handler
 *
 * $Id: buffer.cpp,v 1.2 2005-12-11 16:48:02 tobibobi Exp $
 ***********************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "buffer.H"
#include "src/sys/system.H"
#include "../../defaults.h"

UDPBuffer::UDPBuffer ( int size )
{
  pOffset = System::malloc ( size );
  if ( pOffset == 0L ) {
    PERROR ( "protocolBuffer" );
  }
  iSize = size;
}

UDPBuffer::~UDPBuffer (  )
{
  System::free ( pOffset );
}
