
/************************************************
 *
 * socket.h
 *
 * this will at a later state implement the 
 * higher workings of the protocol
 * ex. multiplexing.
 *
 * $Id: socket.H,v 1.3 2005-12-13 14:16:03 tobibobi Exp $
 ***********************************************/

#ifndef LIB_UDP_SOCKET_H
#define LIB_UDP_SOCKET_H 1

#include "src/sys/thread.H"
#include "../../defaults.h"
#include "../socket.H"

// forward declaration of protocol
class UDPProtocol;

class UDPSocket : public Socket
{
  int id;			// id of socket
  ubyte reciever;
  
  UDPProtocol &proto;
  
  Condition waitCondition;

  long hostip;

  void protocolEvent();

  byte waitForReturnPacket ( );

  readBackFn readback_function;

  friend class UDPProtocol;
  UDPSocket ( UDPProtocol &, int reciever );
  ~UDPSocket();  
public:  
  unsigned long getClock ();
  int hasError();
  LockedPacketBuffer outPacketBuffer();
  void send(const int len);
  void setReadBackFunction(readBackFn fn);

  void setIsyncFrame(const int loop_len, const int offset, const int frame_len);

  void setISyncReadBlock(const void *start,const int len);
  void setISyncWriteBlock(const void *start,const int len);
};

#endif
