
/************************************************
 *
 * socket.c
 *
 * this will at a later state implement the 
 * higher workings of the protocol
 * ex. multiplexing.
 *
 * $Id: socket.cpp,v 1.3 2005-12-13 14:16:03 tobibobi Exp $
 * $Log: socket.cpp,v $
 * Revision 1.3  2005-12-13 14:16:03  tobibobi
 * - Added packetbuffer
 *   The reason why this class was brought in was that i want to prevent
 *   an accidential access from two parties in the same buffer
 *   when they wants to send a packet.
 * - Added better routing by remembering the ipadresses from the different
 *   sockets.
 *   This also forces a single socket to be created the moment a call from an
 *   unknown vendor presents itself.
 *   This can maybe be used to iterate over known clients in the nearby
 *   vicinity.
 *
 ***********************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "socket.H"
#include "protocol.H"
#include "src/sys/system.H"
#include "packet.h"

#include "sys/time.h"
#include <errno.h>


UDPSocket::UDPSocket ( UDPProtocol & protocol, int reciever ):
proto ( protocol )
{
  this->reciever = reciever;
  id = reciever;

  proto.sockets[id] = this;
  readback_function = 0;
  hostip = 0;
}

UDPSocket::~UDPSocket (  )
{

}

void
UDPSocket::protocolEvent ( )
{
  packet *header = ( packet * ) proto.inBuffer.pOffset;
  bool b_async = ( header->flags & PACFL_ASYNC );
  bool b_isync = ( header->flags & PACFL_ISYNC );
  bool b_timer = ( header->flags & PACFL_TIMER );
  bool b_command = ( header->flags & PACFL_COMMAND );
  bool b_ack = ( header->flags & PACFL_ACK );

  if ( b_async ) {
    if( b_timer || b_command) {
      waitCondition.signal();
    } else {
      if ( b_ack ) {
	// so far do nothing
      } else {
	if(readback_function) {
	  readback_function((void *)((int)header+sizeof(packet)),header->len-sizeof(packet));
	} else {
	  PRINT0("No callback funtion defined - package seems lost\n");
	}
      }
    }
  }
  if ( b_isync ) {
    PRINT0 ( "Trying b_isync\n" );
  }
}

void
UDPSocket::send(const int len) {
  proto.prepareSendOfPacket(id,
			    len,
			    PACFL_ASYNC );
  proto.sendData( hostip );
}

LockedPacketBuffer 
UDPSocket::outPacketBuffer() {
  return proto.outPacketBuffer;
}

void 
UDPSocket::setReadBackFunction(readBackFn fn){
  readback_function = fn;
}

void
UDPSocket::setIsyncFrame(const int loop_len, const int offset, const int frame_len) {
#warning Not implemented
}

void 
UDPSocket::setISyncReadBlock(const void *start,const int len) {
#warning Not implemented
}

void 
UDPSocket::setISyncWriteBlock(const void *start,const int len) {
#warning Not implemented
}

int 
UDPSocket::hasError() {
  return 0;
}
  
byte
UDPSocket::waitForReturnPacket (  )
{
  return waitCondition.waitForMSecs(1000);
}


/*!
 * Retrives the actual clock of the system with regards to other host.
 * it will make an ntp like lookup and try to recieve the time from the
 * host placed in the other end.
 * it can also be used to retrive time locally by supplying an empty 
 * socket
 *
 * \param socket:socket the socket to another host or NULL.
 *
 * \returns the corrected time of this machine or simply the time of 
 *          this machine if input is NULL.
 */

unsigned long
UDPSocket::getClock (  )
{
  static long clientAdd = 0;
  timepacket *msg = ( timepacket * ) proto.prepareSendOfPacket ( id,
								 sizeof
								 ( timepacket ),
								 PACFL_TIMER |
								 PACFL_ASYNC );

  long f1 = System::getTime (  );


  msg->clienttime = f1;

  proto.sendData ( hostip );
  if ( FAILED ( waitForReturnPacket (  ) ) ) {

    // try to send data again
    proto.sendData ( hostip );
    if ( FAILED ( waitForReturnPacket (  ) ) ) {

      PRINT0 ( "Cannot send data to server - may be down\n" );

      return 0;
    }
  }

  packet *header = ( packet * ) proto.inBuffer.pOffset;


  timepacket *time =
    ( timepacket * ) ( ( int ) ( proto.inBuffer.pOffset ) +
		       sizeof ( packet ) );
  long f2 = System::getTime (  );

  ulong serverTime = time->servertime;

  // make the actual calculation of the time diff.
  long offset = ( f2 - f1 ) / 2;

  clientAdd = ( f1 + offset ) - serverTime;

  return System::getTime (  ) + clientAdd;
}
