
/***********************************************************
 *
 * packet.H
 *
 * this file is describing the actual packet that
 * is to be transmitted over the net.
 *
 * $Id: packet.h,v 1.2 2005-12-13 14:16:03 tobibobi Exp $
 * $Log: packet.h,v $
 * Revision 1.2  2005-12-13 14:16:03  tobibobi
 * - Added packetbuffer
 *   The reason why this class was brought in was that i want to prevent
 *   an accidential access from two parties in the same buffer
 *   when they wants to send a packet.
 * - Added better routing by remembering the ipadresses from the different
 *   sockets.
 *   This also forces a single socket to be created the moment a call from an
 *   unknown vendor presents itself.
 *   This can maybe be used to iterate over known clients in the nearby
 *   vicinity.
 *
 * Revision 1.1  2005/12/11 16:12:07  tobibobi
 * *** empty log message ***
 *
 * Revision 1.3  2005/12/10 15:16:35  tobibobi
 * *** empty log message ***
 *
 * Revision 1.2  2005/12/06 15:45:20  tobibobi
 * - Har nu importeret tid fra serveren
 * - implementeret en loop back metode..
 *
 * Revision 1.1  2005/12/06 15:26:31  tobibobi
 * *** empty log message ***
 *
 ***********************************************************/

#ifndef LIB_UDP_PACKET_H
#define LIB_UDP_PACKET_H 1

#include "../../defaults.h"


#define PACFL_INIT 1
#define PACFL_ACK 2
#define PACFL_CLOSE 4
#define PACFL_ASYNC 8
#define PACFL_ISYNC 16
#define PACFL_TIMER 32
#define PACFL_COMMAND 64
#define PACFL_IDENT 128

 

/*!
 * This is the header that will be used to handle information sent over 
 * the udp channel.
 */
struct packet
{
  /*!
   * this is the id of person that needs to be accessed
   * the reciever checks this to see if the reciever is correct
   */  
  uchar reciever;

  /*! 
   * in order to send data back, this will give info about the sender.
   */
  uchar sender;

  /*!
   * In UDP, another datalink layer is placed below that handles 
   * addressing a lot faster than this layer can do.
   * so far its not used (we just broadcast data)
   */ 
  // ulong senderIP;

  /*!
   * to notify the right services, we need flags
   */
  uchar flags;

  /*!
   * what are the length of this package (including header)
   */
  uchar len;

  /*!
   * this is as far as i know, a part of the isync packet.
   */
  uint offset;
  /*
  union {
    struct _timepacket
    {
      ulong client;
      ulong server;
    } time;
  } data;
  */
};



/*!
 * packet to be used if we are to update the time
 * \TODO maybe pack it back in to the packet above.
 */
struct timepacket
{
  ulong clienttime;
  ulong servertime;
};

#endif
