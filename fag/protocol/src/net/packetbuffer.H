#ifndef PACKET_BUFFER_H
#define PACKET_BUFFER_H 1

class PacketBuffer {
  struct data;
  data *pData;
public:
  PacketBuffer(const PacketBuffer &buffer);
  PacketBuffer(const void *buffer);

  virtual ~PacketBuffer();
protected:
  PacketBuffer();
  void refCopy(const PacketBuffer &buffer);
  void *ptr();
  void lock();  
  void unlock();
};

class LockedPacketBuffer : public PacketBuffer{
public:
  LockedPacketBuffer(const PacketBuffer &buffer);
  ~LockedPacketBuffer();
  
  void *getPtr();
};
#endif
