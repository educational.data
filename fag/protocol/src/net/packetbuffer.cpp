#include "packetbuffer.H"
#include "../sys/thread.H"

struct PacketBuffer::data {
  mutable int refcnt;
  Mutex mut;
  void *dataPtr;
};

PacketBuffer::PacketBuffer() {

}

PacketBuffer::PacketBuffer(const void *buffer) {
  pData = new data;
  pData->refcnt = 0;
  pData->dataPtr = const_cast<void *>(buffer);
}

PacketBuffer::~PacketBuffer() {
  if(pData->refcnt-- == 0)
    delete pData;
}

PacketBuffer::PacketBuffer(const PacketBuffer &buffer) {
  refCopy(buffer);
}

void 
PacketBuffer::refCopy(const PacketBuffer &buffer) {
  buffer.pData->refcnt++;
  pData = buffer.pData;
}

void *
PacketBuffer::ptr() {
  return pData->dataPtr;
}

void
PacketBuffer::lock() {
  pData->mut.lock();
}

void
PacketBuffer::unlock() {
  pData->mut.unlock();
}


LockedPacketBuffer::LockedPacketBuffer(const PacketBuffer &buffer) {
  refCopy(buffer);
  lock();
}
LockedPacketBuffer::~LockedPacketBuffer() {
  unlock();
}

void *
LockedPacketBuffer::getPtr() {
  return ptr();
}
