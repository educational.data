
/****************************************************************
 * $Id: protoclient.cpp,v 1.6 2005-12-13 14:16:02 tobibobi Exp $
 ***************************************************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <iostream>
#include <unistd.h>
#include "net/netselector.H"
#include "net/socket.H"
#include "defaults.h"


const int hostId = 0;
const int recieverId = 0;
const int portId = 2000;

using namespace std;

int
main (  )
{
#ifdef DO_DEBUG
  cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
#endif

  Netselector net ( hostId );
  Socket *socket = net.getConnection(recieverId);

  socket->getClock (  );

#ifdef DO_DEBUG
  cout << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endl;
#endif


  return 0;
}
