
/******************************
 * $Id: main.c,v 1.5 2005-12-10 15:16:35 tobibobi Exp $
 ******************************/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <unistd.h>
#include "lib/protocol.h"

int
main (  )
{
  struct protocol *proto = createProtocol ( 100, 5 );

  proto->sockets[0]->reciever = 0;

  char *val = prepareSendOfPacket ( proto, 0, 1 );

  val[0] = 1;
  printf ( "Sending data\n" );
  sendData ( proto );

  sleep ( 1 );

  deleteProtocol ( proto );
  return 0;
}
