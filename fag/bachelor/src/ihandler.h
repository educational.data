#ifndef __ICALIBRATEHANDLER_H__
#define __ICALIBRATEHANDLER_H__

#include "sig.h"
#include <string>
#include <ipl98/cpp/image.h>

class ICalibrateHandler {
public:
  sig::signal1<bool,int> on_calibrate_status;
  sig::signal1<int,std::string> on_error;
  sig::signal0<int> on_image_update;

  virtual bool loadImage(const std::string &filename) = 0;
  virtual bool loadRobotPath(const std::string &filename) = 0;
  virtual bool loadScenario(const std::string &filename) = 0;
  virtual bool randomizePoints(const int numPoints) = 0;
  virtual bool saveImage(const std::string &filename) = 0;
  virtual bool saveRobotPath(const std::string &filename) = 0;
  virtual bool saveScenario(const std::string &filename) = 0;
  virtual void setImage(const ipl::CImage &newimg) = 0;

  virtual bool hasCamera() = 0;
  
  virtual bool acquireImage() = 0;
  virtual void doCalibrate() = 0;
  virtual const ipl::CImage &getImage() const = 0;
  virtual void setMethod(int) = 0;
  virtual void findPunkt() = 0;
  virtual ~ICalibrateHandler() { }
};

ICalibrateHandler *getCalibrateHandler();

#endif