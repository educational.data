
CRCRobotps.dll: dlldata.obj CRCRobot_p.obj CRCRobot_i.obj
	link /dll /out:CRCRobotps.dll /def:CRCRobotps.def /entry:DllMain dlldata.obj CRCRobot_p.obj CRCRobot_i.obj \
		mtxih.lib mtx.lib mtxguid.lib \
		kernel32.lib rpcndr.lib rpcns4.lib rpcrt4.lib oleaut32.lib uuid.lib \
		ole32.lib advapi32.lib 

.c.obj:
	cl /c /Ox /DWIN32 /D_WIN32_WINNT=0x0400 /DREGISTER_PROXY_DLL \
		/MD \
		$<

clean:
	@del CRCRobotps.dll
	@del CRCRobotps.lib
	@del CRCRobotps.exp
	@del dlldata.obj
	@del CRCRobot_p.obj
	@del CRCRobot_i.obj
