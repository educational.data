// Robot.h : Declaration of the CRobot

#ifndef __ROBOT_H_
#define __ROBOT_H_

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CRobot
class ATL_NO_VTABLE CRobot : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CRobot, &CLSID_Robot>,
	public ISupportErrorInfo,
	public IDispatchImpl<IRobot, &IID_IRobot, &LIBID_CRCROBOTLib>
{
public:
	CRobot()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_ROBOT)
DECLARE_NOT_AGGREGATABLE(CRobot)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CRobot)
	COM_INTERFACE_ENTRY(IRobot)
	COM_INTERFACE_ENTRY(IDispatch)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
END_COM_MAP()

// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IRobot
public:
};

#endif //__ROBOT_H_
