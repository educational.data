//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by CRCRobotGUI.rc
//
#define IDD_ABOUTBOX                    100
#define IDD_CRCROBOTGUI_FORM            101
#define IDR_MAINFRAME                   128
#define IDR_CRCROBTYPE                  129
#define IDC_SCR_X                       1000
#define IDC_ED_X                        1001
#define IDC_BUTTON_MOVETO               1002
#define IDC_EDIT_ERRORS                 1003
#define IDC_SCR_Y                       1004
#define IDC_SCR_Z                       1005
#define IDC_SCR_YAW                     1006
#define IDC_SCR_PITCH                   1007
#define IDC_SCR_ROLL                    1008
#define IDC_ED_Y                        1009
#define IDC_ED_Z                        1010
#define IDC_ED_YAW                      1011
#define IDC_ED_PITCH                    1012
#define IDC_ED_ROLL                     1013
#define IDC_EDIT_STATUS                 1014
#define IDC_BUTTON_PLAYTRACK            1015
#define IDC_BUTTON_RECORD_TRACK         1016
#define IDC_SLIDER1                     1017

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1018
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
