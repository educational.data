// CRCRobotGUIView.cpp : implementation of the CCRCRobotGUIView class
//

#include "stdafx.h"
#include "CRCRobotGUI.h"

#include "CRCRobotGUIDoc.h"
#include "CRCRobotGUIView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIView

IMPLEMENT_DYNCREATE(CCRCRobotGUIView, CFormView)

BEGIN_MESSAGE_MAP(CCRCRobotGUIView, CFormView)
	//{{AFX_MSG_MAP(CCRCRobotGUIView)
	ON_BN_CLICKED(IDC_BUTTON_MOVETO, OnButtonMoveto)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIView construction/destruction

CCRCRobotGUIView::CCRCRobotGUIView()
	: CFormView(CCRCRobotGUIView::IDD)
{
	//{{AFX_DATA_INIT(CCRCRobotGUIView)
	m_edit_pitch = _T("");
	m_edit_roll = _T("");
	m_edit_x = _T("");
	m_edit_y = _T("");
	m_edit_yaw = _T("");
	m_edit_z = _T("");
	//}}AFX_DATA_INIT
	// TODO: add construction code here

}

CCRCRobotGUIView::~CCRCRobotGUIView()
{
}

void CCRCRobotGUIView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCRCRobotGUIView)
	DDX_Control(pDX, IDC_SCR_Z, m_scr_z);
	DDX_Control(pDX, IDC_SCR_ROLL, m_scr_roll);
	DDX_Control(pDX, IDC_SCR_PITCH, m_scr_pitch);
	DDX_Control(pDX, IDC_SCR_YAW, m_scr_yaw);
	DDX_Control(pDX, IDC_SCR_Y, m_scr_y);
	DDX_Control(pDX, IDC_SCR_X, m_scr_x);
	DDX_Text(pDX, IDC_ED_PITCH, m_edit_pitch);
	DDX_Text(pDX, IDC_ED_ROLL, m_edit_roll);
	DDX_Text(pDX, IDC_ED_X, m_edit_x);
	DDX_Text(pDX, IDC_ED_Y, m_edit_y);
	DDX_Text(pDX, IDC_ED_YAW, m_edit_yaw);
	DDX_Text(pDX, IDC_ED_Z, m_edit_z);
	//}}AFX_DATA_MAP
}

BOOL CCRCRobotGUIView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFormView::PreCreateWindow(cs);
}

void CCRCRobotGUIView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	
	SCROLLINFO info;
	info.cbSize = sizeof(SCROLLINFO);     
	info.fMask = SIF_ALL;     
	info.nMin = 0;     
	info.nMax = 360; 
	info.nPage = 20;     
	info.nPos = 0;    
	info.nTrackPos = 2; 

	m_scr_roll.SetScrollInfo(&info);
	m_scr_yaw.SetScrollInfo(&info);
	m_scr_pitch.SetScrollInfo(&info);
	m_scr_x.SetScrollInfo(&info);
	m_scr_y.SetScrollInfo(&info);
	m_scr_z.SetScrollInfo(&info);
}

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIView diagnostics

#ifdef _DEBUG
void CCRCRobotGUIView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCRCRobotGUIView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CCRCRobotGUIDoc* CCRCRobotGUIView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CCRCRobotGUIDoc)));
	return (CCRCRobotGUIDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIView message handlers

void CCRCRobotGUIView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CCRCRobotGUIDoc &doc = *GetDocument();

	curPos = doc.getCurrentPosition();

	updateCtrls();
}

void CCRCRobotGUIView::OnButtonMoveto() 
{
	// TODO: Add your control notification handler code here
	CCRCRobotGUIDoc &doc = *GetDocument();
	doc.setPosition(curPos);
}

void CCRCRobotGUIView::updateCtrls()
{
	m_scr_yaw.SetScrollPos((int)curPos.yaw);
	m_scr_pitch.SetScrollPos((int)curPos.pitch);
	m_scr_roll.SetScrollPos((int)curPos.roll);
	m_scr_x.SetScrollPos((int)curPos.x);
	m_scr_y.SetScrollPos((int)curPos.y);
	m_scr_z.SetScrollPos((int)curPos.z);

	m_edit_yaw.Format("%3.3f",curPos.yaw);
	m_edit_pitch.Format("%3.3f",curPos.pitch);
	m_edit_roll.Format("%3.3f",curPos.roll);
	m_edit_x.Format("%f",curPos.x);
	m_edit_y.Format("%f",curPos.y);
	m_edit_z.Format("%f",curPos.z);
	UpdateData(false);
}
