; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCRCRobotGUIView
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CRCRobotGUI.h"
LastPage=0

ClassCount=5
Class1=CCRCRobotGUIApp
Class2=CCRCRobotGUIDoc
Class3=CCRCRobotGUIView
Class4=CMainFrame

ResourceCount=6
Resource1=IDD_ABOUTBOX
Resource2=IDR_MAINFRAME
Class5=CAboutDlg
Resource3=IDD_CRCROBOTGUI_FORM
Resource4=IDD_ABOUTBOX (English (U.S.))
Resource5=IDR_MAINFRAME (English (U.S.))
Resource6=IDD_CRCROBOTGUI_FORM (English (U.S.))

[CLS:CCRCRobotGUIApp]
Type=0
HeaderFile=CRCRobotGUI.h
ImplementationFile=CRCRobotGUI.cpp
Filter=N

[CLS:CCRCRobotGUIDoc]
Type=0
HeaderFile=CRCRobotGUIDoc.h
ImplementationFile=CRCRobotGUIDoc.cpp
Filter=N

[CLS:CCRCRobotGUIView]
Type=0
HeaderFile=CRCRobotGUIView.h
ImplementationFile=CRCRobotGUIView.cpp
Filter=D
BaseClass=CFormView
VirtualFilter=VWC
LastObject=CCRCRobotGUIView


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T




[CLS:CAboutDlg]
Type=0
HeaderFile=CRCRobotGUI.cpp
ImplementationFile=CRCRobotGUI.cpp
Filter=D

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_MRU_FILE1
Command6=ID_APP_EXIT
Command10=ID_EDIT_PASTE
Command11=ID_VIEW_STATUS_BAR
Command12=ID_APP_ABOUT
CommandCount=12
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command7=ID_EDIT_UNDO
Command8=ID_EDIT_CUT
Command9=ID_EDIT_COPY

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
CommandCount=13
Command4=ID_EDIT_UNDO
Command13=ID_PREV_PANE

[DLG:IDD_CRCROBOTGUI_FORM]
Type=1
Class=CCRCRobotGUIView

[DLG:IDD_CRCROBOTGUI_FORM (English (U.S.))]
Type=1
Class=CCRCRobotGUIView
ControlCount=24
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_SCR_X,scrollbar,1342242816
Control8=IDC_SCR_Y,scrollbar,1342242816
Control9=IDC_SCR_Z,scrollbar,1342242816
Control10=IDC_SCR_YAW,scrollbar,1342242816
Control11=IDC_SCR_PITCH,scrollbar,1342242816
Control12=IDC_SCR_ROLL,scrollbar,1342242816
Control13=IDC_ED_X,edit,1350631552
Control14=IDC_ED_Y,edit,1350631552
Control15=IDC_ED_Z,edit,1350631552
Control16=IDC_ED_YAW,edit,1350631552
Control17=IDC_ED_PITCH,edit,1350631552
Control18=IDC_ED_ROLL,edit,1350631552
Control19=IDC_STATIC,button,1342177287
Control20=IDC_BUTTON_MOVETO,button,1342242816
Control21=IDC_EDIT_ERRORS,edit,1352665092
Control22=IDC_EDIT_STATUS,edit,1350568004
Control23=IDC_BUTTON_PLAYTRACK,button,1476460544
Control24=IDC_BUTTON_RECORD_TRACK,button,1342242816

[MNU:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_APP_EXIT
Command6=ID_EDIT_UNDO
Command7=ID_EDIT_CUT
Command8=ID_EDIT_COPY
Command9=ID_EDIT_PASTE
Command10=ID_VIEW_STATUS_BAR
Command11=ID_APP_ABOUT
CommandCount=11

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_EDIT_UNDO
Command5=ID_EDIT_CUT
Command6=ID_EDIT_COPY
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_EDIT_COPY
Command11=ID_EDIT_PASTE
Command12=ID_NEXT_PANE
Command13=ID_PREV_PANE
CommandCount=13

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

