// CRCRobotGUIView.h : interface of the CCRCRobotGUIView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CRCROBOTGUIVIEW_H__0E22F527_1653_42A3_BC97_2F085FBCBAB1__INCLUDED_)
#define AFX_CRCROBOTGUIVIEW_H__0E22F527_1653_42A3_BC97_2F085FBCBAB1__INCLUDED_

#include "TPoint.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CCRCRobotGUIView : public CFormView
{
protected: // create from serialization only
	CCRCRobotGUIView();
	DECLARE_DYNCREATE(CCRCRobotGUIView)

public:
	//{{AFX_DATA(CCRCRobotGUIView)
	enum { IDD = IDD_CRCROBOTGUI_FORM };
	CScrollBar	m_scr_z;
	CScrollBar	m_scr_roll;
	CScrollBar	m_scr_pitch;
	CScrollBar	m_scr_yaw;
	CScrollBar	m_scr_y;
	CScrollBar	m_scr_x;
	CString	m_edit_pitch;
	CString	m_edit_roll;
	CString	m_edit_x;
	CString	m_edit_y;
	CString	m_edit_yaw;
	CString	m_edit_z;
	//}}AFX_DATA

// Attributes
public:
	CCRCRobotGUIDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCRCRobotGUIView)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CCRCRobotGUIView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCRCRobotGUIView)
	afx_msg void OnButtonMoveto();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void updateCtrls();
	TPoint curPos;
};

#ifndef _DEBUG  // debug version in CRCRobotGUIView.cpp
inline CCRCRobotGUIDoc* CCRCRobotGUIView::GetDocument()
   { return (CCRCRobotGUIDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CRCROBOTGUIVIEW_H__0E22F527_1653_42A3_BC97_2F085FBCBAB1__INCLUDED_)
