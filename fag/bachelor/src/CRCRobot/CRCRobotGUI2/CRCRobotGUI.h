// CRCRobotGUI.h : main header file for the CRCROBOTGUI application
//

#if !defined(AFX_CRCROBOTGUI_H__722221F0_6862_4E17_8B43_487A9438E1A1__INCLUDED_)
#define AFX_CRCROBOTGUI_H__722221F0_6862_4E17_8B43_487A9438E1A1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIApp:
// See CRCRobotGUI.cpp for the implementation of this class
//

class CCRCRobotGUIApp : public CWinApp
{
public:
	CCRCRobotGUIApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCRCRobotGUIApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CCRCRobotGUIApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CRCROBOTGUI_H__722221F0_6862_4E17_8B43_487A9438E1A1__INCLUDED_)
