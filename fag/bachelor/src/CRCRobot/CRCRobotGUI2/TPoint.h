// TPoint.h: interface for the TPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TPOINT_H__D5601D2B_11A2_444A_8011_F54C27064B35__INCLUDED_)
#define AFX_TPOINT_H__D5601D2B_11A2_444A_8011_F54C27064B35__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class TPoint  
{
public:
	TPoint & operator + (const TPoint &ref);
	TPoint(const TPoint &ref);
	TPoint();
	virtual ~TPoint();

	float x;
	float y;
	float z;
	float yaw;
	float pitch;
	float roll;
};

#endif // !defined(AFX_TPOINT_H__D5601D2B_11A2_444A_8011_F54C27064B35__INCLUDED_)
