// TPoint.cpp: implementation of the TPoint class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "CRCRobotGUI.h"
#include "TPoint.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

TPoint::TPoint()
{
	x = 0.0f;
	y = 0.0f;
	z = 0.0f;
	yaw = 0.0f;
	pitch = 0.0f;
	roll = 0.0f;
}

TPoint::~TPoint()
{

}

TPoint::TPoint(const TPoint &ref)
{
	x = ref.x;
	y = ref.y;
	z = ref.z;
	yaw = ref.yaw;
	pitch = ref.pitch;
	roll = ref.roll;
}

TPoint & TPoint::operator +(const TPoint &ref)
{
	x += ref.x;
	y += ref.y;
	z += ref.z;
	yaw += ref.yaw;
	pitch += ref.pitch;
	roll += ref.roll;
	return *this;
}
