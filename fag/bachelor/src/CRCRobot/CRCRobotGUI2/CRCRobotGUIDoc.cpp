// CRCRobotGUIDoc.cpp : implementation of the CCRCRobotGUIDoc class
//

#include "stdafx.h"
#include "CRCRobotGUI.h"

#include "CRCRobotGUIDoc.h"
#include <string>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIDoc

IMPLEMENT_DYNCREATE(CCRCRobotGUIDoc, CDocument)

BEGIN_MESSAGE_MAP(CCRCRobotGUIDoc, CDocument)
	//{{AFX_MSG_MAP(CCRCRobotGUIDoc)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIDoc construction/destruction

CCRCRobotGUIDoc::CCRCRobotGUIDoc()
{
	// TODO: add one-time construction code here

}

CCRCRobotGUIDoc::~CCRCRobotGUIDoc()
{
}

BOOL CCRCRobotGUIDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}



/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIDoc serialization

void CCRCRobotGUIDoc::Serialize(CArchive& ar)
{
	if (ar.IsStoring())
	{
		// TODO: add storing code here
	}
	else
	{
		// TODO: add loading code here
	}
}

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIDoc diagnostics

#ifdef _DEBUG
void CCRCRobotGUIDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CCRCRobotGUIDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCRCRobotGUIDoc commands

void CCRCRobotGUIDoc::setPosition(const TPoint &pnt)
{
	curPos = pnt;
	// tell views, that we have changed tcp.
	UpdateAllViews(NULL);
}

const TPoint &CCRCRobotGUIDoc::getCurrentPosition() const
{
	return curPos;
}
