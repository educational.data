// CRCRobotGUIDoc.h : interface of the CCRCRobotGUIDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CRCROBOTGUIDOC_H__4DD3E1C3_9275_42EF_A02A_43201ADEA408__INCLUDED_)
#define AFX_CRCROBOTGUIDOC_H__4DD3E1C3_9275_42EF_A02A_43201ADEA408__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "tpoint.h"


class CCRCRobotGUIDoc : public CDocument
{
protected: // create from serialization only
	CCRCRobotGUIDoc();
	DECLARE_DYNCREATE(CCRCRobotGUIDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCRCRobotGUIDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	//}}AFX_VIRTUAL

// Implementation
public:
	const TPoint &getCurrentPosition() const;
	void setPosition(const TPoint &pnt);
	virtual ~CCRCRobotGUIDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CCRCRobotGUIDoc)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	TPoint curPos;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CRCROBOTGUIDOC_H__4DD3E1C3_9275_42EF_A02A_43201ADEA408__INCLUDED_)
