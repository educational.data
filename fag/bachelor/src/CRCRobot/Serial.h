// Serial.h: interface for the Serial class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIAL_H__248F02C0_D828_47A5_9331_853614F26B10__INCLUDED_)
#define AFX_SERIAL_H__248F02C0_D828_47A5_9331_853614F26B10__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <string>

enum serial_parity  { spNONE,    spODD, spEVEN };

class Serial  
{

protected:
	std::string       port;                          // port name "com1",...
    int               rate;                          // baudrate
    serial_parity     parityMode;
    HANDLE            serial_handle; 
public:
	const char getChar(void);
	const int getArray(char *buffer,const size_t len);
	void sendChar(const char c);
	void sendString(const std::string &str);
	void sendArray(const char *buffer, const size_t len);
	void disconnect(void);
	const int connect(
		const std::string port, 
		const int rate, 
		const serial_parity parityMode
	);

	Serial(const Serial &s);
	Serial();
	virtual ~Serial();

};

#endif // !defined(AFX_SERIAL_H__248F02C0_D828_47A5_9331_853614F26B10__INCLUDED_)
