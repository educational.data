// Serial.cpp: implementation of the Serial class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h" 
#include "Serial.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Serial::Serial() { 
  parityMode = spNONE; 
  port = ""; 
  rate = 0;
  serial_handle = INVALID_HANDLE_VALUE; 
}

Serial::~Serial() { 
  if (serial_handle!=INVALID_HANDLE_VALUE)
    CloseHandle(serial_handle); 

  serial_handle = INVALID_HANDLE_VALUE;
}

Serial::Serial(const Serial &s) { 
  parityMode = s.parityMode; 
  port = s.port; 
  rate = s.rate; 
  serial_handle = s.serial_handle;
  
}

const int Serial::connect(const std::string port, const int rate,
			  const serial_parity parityMode) { 
  int erreur; 
  DCB dcb; 
  COMMTIMEOUTS cto = { 0, 0, 0, 0, 0 };
  
  /* --------------------------------------------- */ 
  if(serial_handle!=INVALID_HANDLE_VALUE) 
    CloseHandle(serial_handle);

  serial_handle = INVALID_HANDLE_VALUE;
  
  erreur = 0;
  
  if (port.length()!=0) { 
    this->port = port; 
    this->rate = rate;
    this->parityMode = parityMode; 
    memset(&dcb,0,sizeof(dcb));
    
    /*
      --------------------------------------------------------------------
    */ 
    
    // set DCB to configure the serial port 
    dcb.DCBlength = sizeof(dcb);
    
    /* ---------- Serial Port Config ------- */ 
    dcb.BaudRate = rate;
    
    switch(parityMode) { 
    case spNONE: 
      dcb.Parity = NOPARITY;
      dcb.fParity = 0; 
      break; 
    case spEVEN: 
      dcb.Parity = EVENPARITY;
      dcb.fParity = 1; 
      break; 
    case spODD: 
      dcb.Parity = ODDPARITY;
      dcb.fParity = 1; 
      break; 
    }
    
    
    dcb.StopBits = ONESTOPBIT; 
    dcb.ByteSize = 8;
    
    dcb.fOutxCtsFlow = 0; 
    dcb.fOutxDsrFlow = 0; 
    dcb.fDtrControl = DTR_CONTROL_DISABLE; 
    dcb.fDsrSensitivity = 0; 
    dcb.fRtsControl = RTS_CONTROL_DISABLE; 
    dcb.fOutX = 0; 
    dcb.fInX = 0;
    
    /* ----------------- misc parameters ----- */ 
    dcb.fErrorChar = 0; 
    dcb.fBinary = 1; 
    dcb.fNull = 0; 
    dcb.fAbortOnError = 0;
    dcb.wReserved = 0; 
    dcb.XonLim = 2; 
    dcb.XoffLim = 4; 
    dcb.XonChar = 0x13; 
    dcb.XoffChar = 0x19; 
    dcb.EvtChar = 0;
    
    /*
      --------------------------------------------------------------------
    */ 
    serial_handle = CreateFile(this->port.c_str(), GENERIC_READ |
			       GENERIC_WRITE, 0, NULL, 
			       OPEN_EXISTING,NULL,NULL); // opening serial port
    
    
    if (serial_handle != INVALID_HANDLE_VALUE) {
      if( ! SetCommMask(serial_handle, 0) ) 
	erreur = 1;
      
      // set timeouts 
      if( ! SetCommTimeouts(serial_handle,&cto) )
	erreur = 2;
      
      // set DCB 
      if( ! SetCommState(serial_handle,&dcb) ) 
	erreur = 4; 
    }
    else erreur = 8; 
  } 
  else erreur = 16;
  
  
  /* --------------------------------------------- */ 
  if (erreur != 0) { 
    CloseHandle(serial_handle); 
    serial_handle = INVALID_HANDLE_VALUE; 
  } 
  return erreur;
  
}

void Serial::disconnect() { 
  if (serial_handle != INVALID_HANDLE_VALUE)
    CloseHandle(serial_handle);
  serial_handle = INVALID_HANDLE_VALUE; 
}

void Serial::sendArray(const char *buffer, const size_t len) {
  unsigned long result;

  if (serial_handle != INVALID_HANDLE_VALUE) 
    WriteFile(serial_handle, buffer, len, &result, NULL); 
}
 
void Serial::sendString(const std::string &str) {
  sendArray(str.c_str(),str.length()); 
}

void Serial::sendChar(const char c) { 
  sendArray(&c,1); 
}

const int Serial::getArray(char *buffer, const size_t len) { 
  unsigned long read_nbr;
  
  read_nbr = 0; 
  if (serial_handle != INVALID_HANDLE_VALUE)
    ReadFile(serial_handle, buffer, len, &read_nbr, NULL);
  
  return (int) read_nbr; 
}
 
 const char Serial::getChar() { 
   char c = 0; // empty in case we cannot read anything 
   getArray(&c,1); 
   return c; 
 }
