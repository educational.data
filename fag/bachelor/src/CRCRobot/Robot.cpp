// Robot.cpp : Implementation of CRobot
#include "stdafx.h"
#include "CRCRobot.h"
#include "Robot.h"

/////////////////////////////////////////////////////////////////////////////
// CRobot

STDMETHODIMP CRobot::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IRobot
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}
