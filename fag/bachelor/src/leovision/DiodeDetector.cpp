#include "DiodeDetector.h"
#include <iostream>

using namespace ipl;
using namespace std;

namespace leo {
  DiodeDetector::DiodeDetector() 
  {
    COMX=COMY=0;
  }
  
  CImage DiodeDetector::findBinaryImage(const CImage &m_Img1) {
    //Histogram
	int x,y;					//Variable til for-l�kker
	int hist[256],hist1[256];	//Array til histogramv�rdier
	for(x=0;x<=255;x++){
		hist[x]=0;
		hist1[x]=0;
	}

	//genneml�ber billedet, t�ller gr�toner i hist[]
	for(y=0;y<m_Img1.GetHeight();y++){
		for(x=0;x<m_Img1.GetWidth();x++){
			hist[m_Img1.GetPixel(x,y)]++;
		}
	}

	// find den lyseste v�rdi i billedet
  for(int top=255;top>1 && hist[top]>2;--top);
  for(int bottom=0;bottom<255 && hist[bottom]>2;++bottom);

	int dummy=0;
	int maksima[2] = {0,0};
	
	/*//Smoothing
	int count=0;
	bool retning=false;
	//t�ller antal maksima
	for(x=0;x<254;x++){
		if(hist[x]>hist[x+1]){
			retning = !retning;
			count++;
			dummy++;
			maksima[dummy%2] = x;
		}
	}*/

	//S�l�nge der er mere end to lokale maxima,
	//gentages smoothing...
	//retning:	//false	->stigende
				//true	->faldende
		
	/*
  while(count>2){
		count=0;
		//Smoothing
		for(y=1;y<255;y++){
			hist1[y]=((hist[y-1]+hist[y]+hist[y+1])/3);
		}
		
		//t�ller antal maksima
		for(x=0;x<255;x++){
			if((hist1[x]>hist1[x+1])&&(!retning)){
				count++;
				dummy++;
				maksima[dummy%2] = x;
				retning = !retning;
			}
			if((hist1[x]<hist1[x+1])&&(retning)){
				retning = !retning;
			}
		}
		//Write(count);WriteLn();
		//Flytter data fra det ene array til det andet...
		for(x=0;x<255;x++){
			hist[x] = hist1[x];
		}
	}
    //Write(maksima[0]);Write(maksima[1]);
	
	//Finder den mest frekvente gr�tone
	int max=0;
	for(y=0,x=0;y<=255;y++){
		if(hist1[y]>max)
			max = hist1[y];
  } */

	/*	
	//Nyt billede, til histogrammet
	//m_Img4=CImage(256,256,1,1);

	//Tilpasser hist[] til billedets st�rrelse
	for(x=0;x<255;x++)
		hist[x]=(256*hist[x])/max;

	//Genneml�ber hist[] og retter farver i m_Img2
	for(x=0;x<255;x++){
		for(int i=0;i<hist[x];i++){
			m_Img4.SetPixel(x,m_Img4.GetHeight()-i-1,0);
		}
	}
	*/
	//S� skal threshold-v�rdien findes...
	//int threshold = (maksima[0]+maksima[1])/2;
  //threshold=20;
  int threshold = (top + bottom) /2;
  

	//Nu skal alt der er mindre end threshold v�re 0, og alt der er st�rre skal v�re 1...
	//Det nye billede gemmes i m_Img2
	CImage m_Img2(m_Img1.GetWidth(),m_Img1.GetHeight(),1);
	for(y=0;y<m_Img1.GetHeight();y++){
		for(x=0;x<m_Img1.GetWidth();x++){
			if(m_Img1.GetPixel(x,y)<threshold)
				m_Img2.SetPixel(x,y,0x00);
			else
				m_Img2.SetPixel(x,y,0xff);
		}
	}
	return m_Img2;
	//OnViewImage1();
	//OnViewImage2();
	//OnViewImage4();

  }
  void DiodeDetector::CreateNewBlob(const int x, const int y) 
  {
    TPixelPos Pos;
    Pos.x = x;
    Pos.y = y;
    vector<TPixelPos>PosArray;
    
    PosArray.push_back(Pos);
    Blobs.push_back(PosArray);
    BlobIndexMap.SetPixel(x,y,Blobs.size()-1);
  }
  void DiodeDetector::AppendNorth(const int x, const int y)
  {
    TPixelPos Pos;
    Pos.x = x;
    Pos.y = y;
    int index = BlobIndexMap.GetPixel(x,y-1);
    
    BlobIndexMap.SetPixel(x,y,index);
    Blobs[index].push_back(Pos);   
  }
  
  void DiodeDetector::AppendWest(const int x, const int y)
  {
    TPixelPos Pos;
    Pos.x = x;
    Pos.y = y;
    int index = BlobIndexMap.GetPixel(x-1,y);
        
    BlobIndexMap.SetPixel(x,y,index);
    Blobs[index].push_back(Pos);
  }
  
  void DiodeDetector::CombineBlobs(const int i, const int j)
  {
    //Giv vektor i det samme blobindex som j
    for(int x1=0; x1 < Blobs[i].size(); x1++){
      BlobIndexMap.SetPixel(Blobs[i][x1].x, Blobs[i][x1].y, j);
    }
    
    
    //TPixelPos pix;
    //Flyt vektor j til plads i
    for(int x2=0; x2<Blobs[i].size(); x2++){                
      Blobs[j].push_back(Blobs[i][x2]);
    }
    
    if(i==Blobs.size()-1){
      //Blobs[i]=Blobs.back();
      Blobs.pop_back();
    }
    else{
      Blobs[i]=Blobs.back();                
      for(int x3=0; x3 < Blobs[Blobs.size()-1].size(); x3++){
	      BlobIndexMap.SetPixel(
            Blobs[Blobs.size()-1][x3].x, Blobs[Blobs.size()-1][x3].y, i
          );
      }
      Blobs.pop_back();
    } 
  }
  
  float DiodeDetector::mypow(const float x, const float y)
  {
    if(y == 0){
      return 1.0;
    }
    else{
      return pow(x,y);
    }
  }
  
  float DiodeDetector::SimpleMoment(const int p, const int q, const vector<TPixelPos> &b)
  {
    float sum=0;
    for(int i =0; i<b.size();i++){
      sum += mypow((b[i].x),p) * mypow((b[i].y),q);
    }
    return sum;
  }
  
  float DiodeDetector::CentralMoment(const int p, const int q, const vector<TPixelPos> &b)
  {
    float m10 = SimpleMoment(1,0,b);
    float m01 = SimpleMoment(0,1,b);
    float m00 = b.size();
        
    float comx = (m10/m00);
    float comy = (m01/m00);
    COMX = comx;
    COMY = comy;
    float sum=0;
    for(int i=0;i<b.size();i++){
      sum += mypow((b[i].x - comx),p) * mypow((b[i].y - comy),q);
    }
    return sum;
  }
  
  float DiodeDetector::eta(const int p, const int q, const vector<TPixelPos> &b, const float phi)
  {
    float val =0;
    for(int i=0; i<b.size();i++){
      val += mypow(((b[i].x-COMX)*cos(phi))-((b[i].y-COMY)*sin(phi)),p)*mypow(((b[i].x-COMX)*sin(phi))+((b[i].y-COMY)*cos(phi)),q);
    }
    return val;
  }

  void DiodeDetector::findBlobs(CImage m_Img2) 
  {
    Blobs.resize(0);
    
    BlobIndexMap = CImage(m_Img2.GetWidth(), m_Img2.GetHeight(),24,0xFFFFFF);
    BlobIndexMap.SetBorder(2, 0xffffff);
    m_Img2.SetBorder(1, 0);
    
    
    for (int y=0;y<m_Img2.GetHeight();y++){
      for (int x=0;x<m_Img2.GetWidth();x++){
	if(m_Img2.GetPixel(x,y) != 0){
	  int xx = BlobIndexMap.GetPixel(x-1,y);
	  int yy = BlobIndexMap.GetPixel(x,y-1);
	  if(xx!=0xFFFFFF && yy!=0xFFFFFF){        
	    AppendWest(x,y);
	    if(xx != yy){
	      CombineBlobs(xx,yy);
	    }
	  }
	  if(xx!=0xFFFFFF && yy==0xFFFFFF){                       
	    AppendWest(x,y);
	  }
	  if(xx==0xFFFFFF && yy!=0xFFFFFF){
	    AppendNorth(x,y);
	  }
	  if(xx==0xFFFFFF && yy==0xFFFFFF){
	    CreateNewBlob(x,y);
	  }
	}
      }
    }
  }
  CPoint2D<double> DiodeDetector::findTCP(const BlobArray &Blobs) 
  {
    //DIODE:
    //Detektering ud fra afstanden mellem COM og maxpos

    float maxblob = 0.0f;
    float comx,comy;
    comx = 0.0f;
    comy = 0.0f;
    for(int i=0;i<Blobs.size();i++){
      //if(Blobs[i].size()>=10 && Blobs[i].size()<=140) {
        if(Blobs[i].size() > maxblob) {
          maxblob = Blobs[i].size();
				  float m10 = SimpleMoment(1,0,Blobs[i]);
          float m01 = SimpleMoment(0,1,Blobs[i]);
          float m00 = Blobs[i].size();
    
          float rund = m10/m01;
          // if(rund < 2.0f && rund > 0.5) {
    	      comx = (m10/m00);
	  	      comy = (m01/m00);
          // }
		    }
		  //}
	  }
    return CPoint2D<double>((double)comx,(double)comy);
  }

  void DiodeDetector::tegnFirkant(int x, int y, int color){
    for(int i=-1;i<2;i++){
      for(int j=-1;j<2;j++){
	      BlobIndexMap.SetPixel(x+i,y+j,color);
      }
    }
  }
  bool DiodeDetector::locate(const CImage &img) {
    found = false;
    findBlobs(findBinaryImage(img));
    center = findTCP(Blobs);
    if(! center.IsZero()) 
      found = true;
    else 
      found = false;
    return found;
  }
};
