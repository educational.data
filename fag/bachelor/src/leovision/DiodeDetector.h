#ifndef __LEO_DIODEDETECTOR_H_INCLUDED__
#define __LEO_DIODEDETECTOR_H_INCLUDED__

#include "../locater.h"
#include <vector>

namespace leo {
  using namespace ipl;
  using namespace std;

  typedef struct {
    int x,y;
  } TPixelPos;

  typedef vector<vector<TPixelPos> > BlobArray;

  class DiodeDetector : public Locater {
  private:
    CImage findBinaryImage(const CImage &m_Img1);
    void CreateNewBlob(const int x, const int y);
    void AppendNorth(const int x, const int y);
    void AppendWest(const int x, const int y);
    void CombineBlobs(const int i, const int j);
    void findBlobs(CImage m_Img2);
    CPoint2D<double> findTCP(const BlobArray &blobs);

    float mypow(const float x, const float y);
    float SimpleMoment(const int p, const int q, const vector<TPixelPos> &b);
    float CentralMoment(const int p, const int q, const vector<TPixelPos> &b);
    float eta(const int p, const int q, const vector<TPixelPos> &b, const float phi);
    void tegnFirkant(const int x, const int y, const int color);

    BlobArray Blobs;
    CImage BlobIndexMap;

    float COMX;
    float COMY;
    bool found;
    CPoint2D<double> center;
  public:
    DiodeDetector();
    bool locate(const CImage &img);
    bool hasPoint() const {
      return found;
    }
    CPoint2D<double> getPoint() const {
      if(found) return center;
      else return CPoint2D<double>();
    }
  };
}

#endif
