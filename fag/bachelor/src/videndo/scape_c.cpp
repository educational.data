#include "scape_c.h"
#include <sstream>
#include <iomanip>
#include <fstream>
#include <ios>
#include <ipl98/cpp/error.h>
#include <ipl98/cpp/vectors/vector3d.h>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/pixelgroups.h>
#include <ipl98/cpp/algorithms/feature_extraction.h> 
#include <ipl98/cpp/algorithms/segmentate.h>
#include <ipl98/cpp/algorithms/mask_operation.h> 
#include <ipl98/cpp/algorithms/statistic.h>
#include <time/time_date.h> 
#include <paintlib_convert/paintlib_convert.h>
#include <videndo/mark_retrieval.h>
#include <videndo/features.h>
#include "CMyHoopsDoc.h"

using std::ostringstream;
using std::endl;
using std::ios;
using std::ios_base;
using std::setw;
using std::setprecision;
using std::setfill;
using std::ofstream;
using std::ifstream;
using ipl::CError;
using ipl::IPL_ERROR;
using ipl::IPL_WARNING;
using ipl::DegreeToRad;
using ipl::CVector3D;
using ipl::CPixelGroups;
using ipl::CSegmentate;
using ipl::CMaskOperation;
using ipl::CStatistic;

namespace videndo{ // use namespace if C++

const double CScapeC::m_Version=0.51;
const int CScapeC::m_TotalFeatures=12;

CScapeC::CScapeC()
{
	m_Initialized=false;
	m_CameraInitialized=false;
	m_pCam=NULL;
	m_FeaturesInUse=false;
}

CScapeC::~CScapeC()
{
	if (m_pCam!=NULL)
		delete m_pCam;
}

bool CScapeC::Initialize(CMyHoopsDoc* pDoc)
{
	if (pDoc==NULL)
	{
		ostringstream ost;
		ost << "CScapeC::Initialize() pDoc parameter is NULL" << m_CalibFile << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_pDoc=pDoc;
	m_Initialized=true;
	return true;
}

bool CScapeC::SetCamera(unsigned int Width, unsigned int Height, double FocalLengthIdeal, unsigned int BlurFactor, 
						const string& CalibFile)
{
	m_Width=Width;
	m_Height=Height;
	m_ImageWidth=Width;
	m_ImageHeight=Height;
	m_FocalLengthIdeal=FocalLengthIdeal;
	m_BlurFactor=BlurFactor;
	m_CalibFile=CalibFile;
	m_CalibFileInUse=true;
	if (m_pCam!=NULL)
		delete m_pCam;
	m_pCam = new CCameraModel(Width,Height);
	if (m_pCam->Load(m_CalibFile.c_str())==false)
	{
		ostringstream ost;
		ost << "CScapeC::SetCamera() Failed loading camera setup file: " << m_CalibFile << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_HoopsControl.SetDimensions(Width,Height);
	m_CameraInitialized=true;
	return true;
}

bool CScapeC::SetCamera(unsigned int Width, unsigned int Height, double FocalLengthIdeal, unsigned int BlurFactor, 
						const ipl::TCameraParameters& CamParams)
{
	m_Width=Width;
	m_Height=Height;
	m_ImageWidth=Width;
	m_ImageHeight=Height;
	m_FocalLengthIdeal=FocalLengthIdeal;
	m_BlurFactor=BlurFactor;
	if (m_pCam!=NULL)
		delete m_pCam;
	m_pCam = new CCameraModel(Width,Height);
	m_pCam->m_Par = CamParams;
	// make sure the f value in TCameraParameters is set
	m_pCam->m_Par.f = 1/m_pCam->m_Par.FocalLength;
	m_CalibFileInUse=false;
//	if (m_pCam!=NULL)
//		delete m_pCam;
//	m_pCam = new CCameraModel(Width,Height);
	if (m_pCam->ParametersToMatrix()==false)
	{
		ostringstream ost;
		ost << "CScapeC::SetCamera() Failed calculating camera matrix from given parameters" 
			<< m_CalibFile << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_HoopsControl.SetDimensions(Width,Height);
	m_CameraInitialized=true;
	return true;
}

bool CScapeC::GetCameraParams(TCameraParameters& Par) const
{
	if (CameraInUse()==false)
	{
		ostringstream ost;
		ost << "CScapeC::GetCameraParams() Camera not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	Par=m_pCam->m_Par;
	return true;
}

bool CScapeC::SetTraining(double PhiMin, double PhiMax, double PhiStepSize, double ThetaMin, double ThetaMax, 
			double ThetaStepSize, const string& SavePath, const string& FilePrefix, unsigned short CompressionLevel)
{
	if ((PhiMin>PhiMax) || (PhiStepSize>(PhiMax-PhiMin)) || (ThetaMin>ThetaMax) || 
		(ThetaStepSize>(ThetaMin-ThetaMax)) || (CompressionLevel<1) || (CompressionLevel>9))
	{
		ostringstream ost;
		ost << "CScapeC::SetTraining() Input parameters invalid" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_PhiMin=PhiMin;
	m_PhiMax=PhiMax;
	m_PhiStepSize=PhiStepSize;
	m_ThetaMin=ThetaMin;
	m_ThetaMax=ThetaMax;
	m_ThetaStepSize=ThetaStepSize;
	m_SavePath=SavePath;
	m_FilePrefix=FilePrefix;
	m_TrainingInitialized=true;
	m_CompressionLevel=CompressionLevel;
	return true;
}

double CScapeC::GetVersion() const
{
	return m_Version;
}

string CScapeC::GetFileName(double Rho, double Phi, double Theta) const
{
	if (m_TrainingInitialized==false)
	{
		ostringstream ost;
		ost << "CScapeC::GetFileName() Training not initialize, need to supply Path and FilePrefix" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return string("CScapeC::GetFileName() ERROR");
	}
	return GetFileName(m_SavePath,m_FilePrefix,Rho,Phi,Theta);
}

string CScapeC::GetFeatureFileName(unsigned int BlurFactor, unsigned int Width, unsigned int Height)
{
	return GetFeatureFileName(m_SavePath, m_FilePrefix, BlurFactor, Width, Height);
}

string CScapeC::GetFeatureFileName(const string& Path, const string& FilePrefix, unsigned int BlurFactor, 
	unsigned int Width, unsigned int Height)
{
	ostringstream ost;
	ost << Path << FilePrefix << "_training_features_Blur_" << BlurFactor 
		<< "_Dim_" << Width << "_" << Height << ".txt";
	return ost.str();
}

bool CScapeC::SetImageDimensions(unsigned int Width, unsigned int Height)
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CScapeC::SetDimensions() This class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_ImageWidth=Width;
	m_ImageHeight=Height;
	m_HoopsControl.SetImageDimensions(Width,Height);
	return true;
}

string CScapeC::GetFileName(const string& Path, const string& FilePrefix,double Rho, double Phi, double Theta) const
{
	ostringstream ost;
	ost.setf(ios_base::fixed,ios_base::floatfield);
	ost.setf(ios::right,ios::adjustfield);
	string Path2(Path);
	if (ipl::AddTrailingSlashToPath(Path2)==false)
		return string("");
	Path2.append("images/");
	ost << Path2.c_str() << FilePrefix << "_" << setw(5) << setprecision(1) << Rho << "_" << 
		setfill('0') << setw(5) << setprecision(1) << Phi << "_" << 
		setfill('0') << setw(5) << setprecision(1) << Theta << "_Dim_" << m_ImageWidth << "_" << m_ImageHeight << ".png";
	return ost.str();
}

string CScapeC::GetTrainingFileName(unsigned int Width, unsigned int Height) const
{
	ostringstream ost;
	ost << m_SavePath << m_FilePrefix << "_training_Dim_" << m_ImageWidth << "_" << m_ImageHeight << ".txt";
	return ost.str();
}

bool CScapeC::TrainObject(const string& HoopsSetupFile) throw(string)
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CScapeC::TrainObject() This class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (m_TrainingInitialized==false)
	{
		ostringstream ost;
		ost << "CScapeC::TrainObject() Training not initialize, need to supply Path and FilePrefix" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (CameraInUse()==false)
	{
		ostringstream ost;
		ost << "CScapeC::TrainObject() Camera not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	CImage Img(m_ImageWidth,m_ImageHeight,8);
	if (m_HoopsControl.HoopsToCImageInit()==false)
		return false;
	string FileName;
	FileName=GetTrainingFileName(m_ImageWidth,m_ImageHeight);
	ostringstream ost;
	ofstream os;
	os.open(FileName.c_str());
	if (!os)
	{
		ostringstream ost;
		ost << "CScapeC::TrainObject() Failed opening training output file " 
			<< FileName << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		throw(ost.str());
		//return false;
	}
	double RhoStepSize=1;
	double Rho,Theta,Phi;
	double RhoMin,RhoMax;
	RhoMin=-m_pCam->GetPinHole().GetDist();
	RhoMax=RhoMin+RhoStepSize;

	double RhoSteps   = GetTotalRhoSteps();
	double ThetaSteps = GetTotalThetaSteps();
	double PhiSteps   = GetTotalPhiSteps();
//	double RhoSteps   = (RhoMax-RhoMin)/RhoStepSize;
//	double ThetaSteps = (m_ThetaMax-m_ThetaMin)/m_ThetaStepSize;
//	double PhiSteps   = (m_PhiMax-m_PhiMin)/m_PhiStepSize;
	unsigned int TotalFiles = RhoSteps*ThetaSteps*PhiSteps;
	unsigned int RhoCount=0, ThetaCount=0;

	string HoopsSetupFile2(HoopsSetupFile);
	ipl::ConvertBackslashToSlash(HoopsSetupFile2);
	ost.setf(ios_base::fixed,ios_base::floatfield);
	ost.setf(ios::right,ios::adjustfield);
	os << "# Training data for a CAD model" << endl;
	os << "HoopsSetupFile: " << HoopsSetupFile2 << endl;
	os << "SavePath:       " << m_SavePath << endl;
	os << "FilePrefix:     " << m_FilePrefix << endl;
	os << "ImageWidth:          " << m_ImageWidth << endl;
	os << "ImageHeight:         " << m_ImageHeight << endl << endl;
	os << "TotalFiles:     " << TotalFiles << endl << endl;
	os << "# Rho  Phi  Theta  UpVector  Filename" << endl;

	CTimeDate Timer;
	Timer.StartTimer();
	ostringstream txt;
	txt << "RhoSteps=" << RhoSteps << " ThetaSteps=" << ThetaSteps << " PhiSteps=" << PhiSteps 
		<< " TotalFiles=" << TotalFiles;
	m_pDoc->m_History.push_back(txt.str());
	m_pDoc->UpdateViewsNow();
	// Offset Theta start with a half step
	//m_ThetaMin += m_ThetaStepSize/2; // done in setup file instead!

	m_HoopsControl.SetCameraPose(CPoint3D<double>(0,0,RhoMin), DegreeToRad(m_ThetaMin), DegreeToRad(m_PhiMin), 0, m_FocalLengthIdeal/*m_pCam->m_Par.FocalLength*/);
	for(Rho=RhoMin;Rho<RhoMax;Rho+=RhoStepSize)
	{
		txt.str("");
		txt << "Rho=" << Rho << " (step " << RhoCount << " counting to " << RhoSteps << ")";
		m_pDoc->m_History.push_back(txt.str());
		m_pDoc->UpdateViewsNow();
		for(Theta=m_ThetaMin;Theta<m_ThetaMax;Theta+=m_ThetaStepSize)
		{
			txt.str("");
			txt << "Theta=" << Theta << " (step " << ThetaCount << " counting to " << ThetaSteps << ")";
			m_pDoc->m_History.push_back(txt.str());
			m_pDoc->UpdateViewsNow();
			for(Phi=m_PhiMin;Phi<m_PhiMax;Phi+=m_PhiStepSize)
			{
				double x=(Rho*sin(DegreeToRad(Phi)));
				double y=-(Rho*cos(DegreeToRad(Phi))*sin(DegreeToRad(Theta)));
				double z=(Rho*cos(DegreeToRad(Phi))*cos(DegreeToRad(Theta)));
				CPoint3D<double> Pos(x,y,z);
				m_HoopsControl.SetCameraPose(Pos, DegreeToRad(Theta), DegreeToRad(Phi), 0, m_FocalLengthIdeal/*m_pCam->m_Par.FocalLength*/);
				FileName=GetFileName(m_SavePath,m_FilePrefix,Rho,Phi,Theta);
				m_HoopsControl.HoopsToCImageFast(Img);
				CPaintlibConvert::SaveAndConvertPNG(FileName.c_str(),Img,m_CompressionLevel);
				ost.str("");
				ost << setw(5) << setprecision(1) << Rho << "  " 
					<< setfill(' ') << setw(5) << setprecision(1) << Phi << "  " 
					<< setw(5) << setprecision(1) << Theta 
					<< "  " << setw(4) << m_HoopsControl.GetUpVector() << "  " << FileName << endl;
				os << ost.str();
				//Sleep(10);
			}
			ThetaCount++;
		}
		RhoCount++;
	}
	os.close();
	ost.str("");
	ost << "Total time: " << Timer.StopTimer() << " seconds";
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	return true;
}

bool CScapeC::DeriveFeatures(const char* pFilePathName, unsigned int BlurFactor) throw(string)
{
	string TrainingFilePathName(pFilePathName);
	ipl::ConvertBackslashToSlash(TrainingFilePathName);
	ifstream is;
	is.open(TrainingFilePathName.c_str());
	if (!is)
	{
		ostringstream ost;
		ost << "CScapeC::DeriveFeatures() Failed opening file " << TrainingFilePathName << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	// read file header
	char* pStr = new char[300];
	string SavePath,FilePrefix,HoopsSetupFile;
	unsigned int TotalFiles,ImageWidth,ImageHeight;
	try
	{
		ipl::SkipSpaceAndComments(is);
		// skip 'HoopsSetupFile:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is.getline(pStr,300); // reads rest of line since item can consist of spaces
		HoopsSetupFile.assign(pStr);
		ipl::SkipSpaceAndComments(is);
		// skip 'SavePath:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is.getline(pStr,300); // reads rest of line since item can consist of spaces
		SavePath.assign(pStr);
		ipl::SkipSpaceAndComments(is);
		// skip 'FilePrefix:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is.getline(pStr,300); // reads rest of line since item can consist of spaces
		FilePrefix.assign(pStr);
		ipl::SkipSpaceAndComments(is);
		// skip 'ImageWidth:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> ImageWidth;
		ipl::SkipSpaceAndComments(is);
		// skip 'ImageHeight:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> ImageHeight;
		ipl::SkipSpaceAndComments(is);
		// skip 'TotalFiles:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> TotalFiles;
	}
	catch(...) // all exceptions
	{
		throw(string("Failed reading from data file containing a CScapeC object"));
	}
	// First read setup file found in this feature database
	ifstream is2;
	is2.open(HoopsSetupFile.c_str());
//	ios_base::io_state state=is2.rdstate();
//	if (state & ios_base::badbit)
//	{
//		std::cout << "badbit" << endl;
//	}
//	if (state & ios_base::goodbit)
//	{
//		std::cout << "goodbit" << endl;
//	}
//	if (state & ios_base::eofbit)
//	{
//		std::cout << "eofbit" << endl;
//	}
//	if (state & ios_base::failbit)
//	{
//		std::cout << "failbit" << endl;
//	}
	if (!is2)
	{
		ostringstream ost;
		ost << "CScapeC::DeriveFeatures() Failed opening config file: " << HoopsSetupFile;
		throw(ost.str());
		return false;
	}
	else
	{
		is2 >> *this;
		ostringstream ost;
		ost << "Setup loaded from file: " << HoopsSetupFile << endl << endl;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
	}
	// Check if training db has different blurfactor and dimensions than setup file
	if ((ImageWidth!=m_ImageWidth) || (ImageHeight!=m_ImageHeight))
	{
		ostringstream ost;
		ost << "Width and height different from setup file, using training database' width and height";
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		SetImageDimensions(ImageWidth,ImageHeight);
	}
	if (BlurFactor!=m_BlurFactor)
	{
		ostringstream ost;
		ost << "BlurFactor different from setup file, using training database' BlurFactor";
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		m_BlurFactor=BlurFactor;
	}
	ostringstream ost;
	// prepare output file
	string OutFile;
	OutFile=GetFeatureFileName(BlurFactor, m_ImageWidth, m_ImageHeight);
	ofstream os;
	os.open(OutFile.c_str());
	if (!os)
	{
		ostringstream ost2;
		ost2 << "CScapeC::DeriveFeatures() Failed opening file " << OutFile << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost2.str().c_str());
		return false;
	}

	os << "# Hu moment features" << endl;
	os << "# see buttom of file for statistics on pixel group sizes" << endl;
	os << "HoopsSetupFile: " << HoopsSetupFile << endl;
	os << "TrainingSource: " << TrainingFilePathName << endl;
	os << "ImageWidth:          " << ImageWidth << endl;
	os << "ImageHeight:         " << ImageHeight << endl;
	os << "BlurFactor:     " << BlurFactor << endl;
	os << "TotalFiles:     " << TotalFiles << endl << endl;
	os << "# Rho  Phi  Theta  Hu1  Hu2  Hu3  Hu4  Hu5  Hu6  Hu7  Hu8  Hu9 Hu10 Hu11 Hu12 Hu13 UseEta21 SignPositive PrimaryAxisAngle m00 Filename" << endl;
	int Count, NoPrimaryAxisCount=0;
	double Rho, Phi, Theta;
	vector<unsigned int> GroupSizes(TotalFiles); // used to store pixel group sizes - just for statistics
	CPoint3D<double> P;
	string FileName;
	CImage Img;
	ipl::CFeatures Features;
	ost << "Deriving features for " << FilePrefix << ", a total of " << TotalFiles << " files...";
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	CTimeDate Timer;
	Timer.StartTimer();
	for(Count=0; Count<TotalFiles; Count++)
	{
		try
		{
			ipl::SkipSpaceAndComments(is);
			is >> Rho;
			is >> Phi;
			is >> Theta;
			is >> P; // just skip the up-vector
			ipl::SkipSpaceAndComments(is);
			is.getline(pStr,300); // reads rest of line since item can consist of spaces
			FileName.assign(pStr);
		}
		catch(...) // all exceptions
		{
			throw(string("Failed reading from data file containing a CScapeC object"));
		}
		ost.str("");
		if (CPaintlibConvert::LoadAndConvert(FileName,Img)==false)
		{
			ost << "CScapeC::DeriveFeatures() Failed loading file " << FileName << IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			return false;
		}
		if ((Count % 100) == 0)
		{
			ost << Count << " Deriving features for file " << FileName;
			m_pDoc->m_History.push_back(ost.str());
			m_pDoc->UpdateViewsNow();
		}
		CPixelGroup Grp;
		if (GetGroupInImage(Grp,Img)==false)
			return false;
		GroupSizes[Count]=Grp.GetTotalPositions();
		Features.DeriveMomentsBinary(Grp,3);
		Features.DeriveCentralNormMoments();
		Features.DeriveHuFromCentralNormMom();
		// Retrieving data for roll determination
		CFeatures AxisAngle;
		bool UseEta21, SignPositive;
		double PrimaryAxisAngle;
		if (AxisAngle.AddRefObject(0,Features.m_CentrNormMom)==true)
			AxisAngle.GetRefObject(0,UseEta21,SignPositive,PrimaryAxisAngle);
		else
		{
			ost << "CScapeC::DeriveFeatures() No primary axis for file " << FileName << IPLAddFileAndLine;
			CError::ShowMessage(IPL_WARNING,ost.str().c_str());
			NoPrimaryAxisCount++;
			//return false;
		}
		os  << Rho << " " << Phi << " " << Theta << " " << Features.m_Hu[1] << " " << Features.m_Hu[2] << " " 
			<< Features.m_Hu[3] << " " << Features.m_Hu[4] << " " << Features.m_Hu[5] << " "
			<< Features.m_Hu[6] << " " << Features.m_Hu[7] << " " << Features.m_Hu[8] << " "
			<< Features.m_Hu[9] << " " << Features.m_Hu[10] << " " << Features.m_Hu[11] << " "
			<< Features.m_Hu[12] << " " << UseEta21 << " " << SignPositive << "  " << PrimaryAxisAngle 
			<< " " << Features.m_Hu[0] << " " << FileName << endl;
		if ((Count % 100) == 0)
		{
			m_pDoc->m_History.push_back("OK");
			m_pDoc->UpdateViewsNow();
		}		
	}
	CStatistic Stat;
	Stat.DeriveStats(GroupSizes);
	os << "# Statistics for all pixel group sizes: " << endl
		<< "  Mean: " << Stat.GetMean() << endl
		<< "#  Min: " << Stat.GetMin() << endl
		<< "#  Max: " << Stat.GetMax() << " StdDev=" << Stat.GetStdDev() << endl
		<< "# No primary axis detected for " << NoPrimaryAxisCount << " objects!" << endl;
	ost.str("");
	ost << "Total time: " << Timer.StopTimer() << " seconds";
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	return true;
}

bool CScapeC::ReadFeatures(const char* pFilePathName) throw(string)
{
	ifstream is;
	is.open(pFilePathName);
	if (!is)
	{
		ostringstream ost;
		ost << "CScapeC::ReadFeatures() Failed opening file " << pFilePathName << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (m_Features.Resize(GetTotalRhoSteps(),GetTotalPhiSteps(),GetTotalThetaSteps())==false)
		return false;

	// read file header
	char* pStr = new char[300];
	string HoopsSetupFile;
	unsigned int TotalFiles,ImageWidth,ImageHeight,BlurFactor;
	try
	{
		ipl::SkipSpaceAndComments(is);
		// skip 'HoopsSetupFile:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is.getline(pStr,300); // reads rest of line since item can consist of spaces - don't use this info
		HoopsSetupFile.assign(pStr);
		ipl::SkipSpaceAndComments(is);
		// skip 'TrainingSource:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is.getline(pStr,300); // reads rest of line since item can consist of spaces - don't use this info
		ipl::SkipSpaceAndComments(is);
		// skip 'ImageWidth:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> ImageWidth;
		ipl::SkipSpaceAndComments(is);
		// skip 'ImageHeight:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> ImageHeight;
		ipl::SkipSpaceAndComments(is);
		// skip 'BlurFactor:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> BlurFactor;
		// skip 'TotalFiles:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> TotalFiles;
	}
	catch(...) // all exceptions
	{
		throw(string("Failed reading from data file containing a CScapeC object"));
	}
	// First read setup file found in this feature database
	try
	{
		ifstream is;
		is.open(HoopsSetupFile.c_str());
		if (is)
		{
			is >> *this;
			ostringstream ost;
			ost << "Setup loaded from file: " << HoopsSetupFile << endl << endl;
			m_pDoc->m_History.push_back(ost.str());
			m_pDoc->UpdateViewsNow();
		}
	}
	catch(...)
	{
		throw(string("Failed opening config file"));
	}
	// Check if feature db has different blurfactor and dimensions than setup file
	// if yes, create new CCameraModel object and giver user a message
	if ((ImageWidth!=m_ImageWidth) || (ImageHeight!=m_ImageHeight))
	{
		ostringstream ost;
		ost << "Width and height different from setup file, using feature database' width and height";
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		SetImageDimensions(ImageWidth,ImageHeight);
	}
	if (BlurFactor!=m_BlurFactor)
	{
		ostringstream ost;
		ost << "BlurFactor different from setup file, using feature database' BlurFactor";
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		m_BlurFactor=BlurFactor;
	}
	// check total files
	if (TotalFiles!=GetTotalRhoSteps()*GetTotalThetaSteps()*GetTotalPhiSteps())
	{
		ostringstream ost;
		ost << "CScapeC::ReadFeatures() Wrong number of files compared to Rho,Theat, and Phi in this object" 
			<< pFilePathName << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	// read database
	unsigned int Rho,Theta,Phi;
	for(Rho=0;Rho<GetTotalRhoSteps();Rho+=1)
	{
		for(Theta=0;Theta<GetTotalThetaSteps();Theta+=1)
		{
			for(Phi=0;Phi<GetTotalPhiSteps();Phi+=1)
			{
				ipl::SkipSpaceAndComments(is);
				is >> m_Features[Rho][Phi][Theta].m_Rho;
				is >> m_Features[Rho][Phi][Theta].m_Phi;
				is >> m_Features[Rho][Phi][Theta].m_Theta;
				for(int HuMoment=1; HuMoment<=m_TotalFeatures; HuMoment++)
				{
					is >> m_Features[Rho][Phi][Theta].m_Features[HuMoment];
				}
				is >> m_Features[Rho][Phi][Theta].m_UseEta21;
				is >> m_Features[Rho][Phi][Theta].m_SignPositive;
				is >> m_Features[Rho][Phi][Theta].m_PrimaryAxisAngle;
				is >> m_Features[Rho][Phi][Theta].m_m00;
				is.getline(pStr,300); // reads rest of line since item can consist of spaces - don't use this info
			}
		}
	}
	// skip 'Mean:' token
	ipl::SkipSpaceAndComments(is);
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> m_MeanBlobSize;
//	ostringstream ost;
//	unsigned int x1=20,x2=12;
//	DBCell& Cell=m_Features[0][x1][x2];
//	ost << "Rho=" << Cell.m_Rho << " Phi=" << Cell.m_Phi << " Theta=" << Cell.m_Theta << endl;
//	for(int c=1; c<m_TotalFeatures; c++)
//	{
//		ost << Cell.m_Features[c] << " ";
//	}
//	m_pDoc->m_History.push_back(ost.str());
//	m_pDoc->UpdateViewsNow();

	// Calculate interclass variance
	m_Variance.resize(m_TotalFeatures+1);
	vector<double> sum1(m_TotalFeatures+1,0);
	vector<double> sum2(m_TotalFeatures+1,0);
	unsigned int Count=0;
	for(Rho=0;Rho<GetTotalRhoSteps();Rho+=1)
	{
		for(Theta=0;Theta<GetTotalThetaSteps();Theta+=1)
		{
			for(Phi=0;Phi<GetTotalPhiSteps();Phi+=1)
			{
				for(int HuMoment=1; HuMoment<=m_TotalFeatures; HuMoment++)
				{
					double buf=m_Features[Rho][Phi][Theta].m_Features[HuMoment];
					sum1[HuMoment]+=buf;
					sum2[HuMoment]+=buf*buf;
				}
				Count++;
			}
		}
	}
	for(int HuMoment=1; HuMoment<=m_TotalFeatures; HuMoment++)
	{
		m_Variance[HuMoment]=sum2[HuMoment]/Count-sum1[HuMoment]*sum1[HuMoment]/(Count*Count);
	}
	m_FeaturesInUse=true;
	m_pDoc->m_History.push_back("Finished reading file and calculating interclass variance");
	m_pDoc->UpdateViewsNow();
	return true;
}

bool CScapeC::Recognize(const vector<double>& Rec, vector<CGuess>& Guesses, unsigned int TotalGuesses)
{
	if (m_FeaturesInUse==false)
	{
		ostringstream ost;
		ost << "CScapeC::Recognize() Must call ReadFeatures() first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	Guesses.resize(TotalGuesses);
	FLOAT32 DistSq,buf;
	int p,p1,q;
	unsigned int Rho, Theta, Phi;
	bool Exit;
	//DistSq=0; moved into loop by edr
	for (p=0;p<TotalGuesses;p++)
		Guesses[p].m_DistSq=(p+1)*1e20;

	for (Rho=0;Rho<m_Features.size();Rho++) // Rho count
	{
		for (Theta=0;Theta<GetTotalThetaSteps();Theta++) // Theta count
		{
			for (Phi=0;Phi<GetTotalPhiSteps();Phi++) // Phi count
			{
				DistSq=0; // inserted by edr - moved from beginning
				for (q=1;q<=m_TotalFeatures;q++)
				{
					int Index=q;
					buf=Rec[Index]-m_Features[Rho][Phi][Theta].m_Features[Index];
					DistSq+=buf*buf/m_Variance[Index];
				}
				p=TotalGuesses-1;
				Exit=(DistSq>Guesses[p].m_DistSq);
				if (!Exit)
				{
					do
					{
						p--;
						if (p==-1)
							Exit=true;
						else
							Exit=(DistSq>Guesses[p].m_DistSq);
					} while (!Exit);

					if (p<(int)(TotalGuesses-2))
					{
						for (p1=TotalGuesses-1;p1>(p+1);p1--)
							Guesses[p1]=Guesses[p1-1];
					}
					for(int count=1;count<=m_TotalFeatures;count++)
					{
						Guesses[p+1].m_Rec.m_Features[count]=m_Features[Rho][Phi][Theta].m_Features[count];
					}
					Guesses[p+1].m_Rec=m_Features[Rho][Phi][Theta];
					Guesses[p+1].m_DistSq=DistSq;
				} //if (!Exit)
			}//for for for
		}
	}
	return true;
}

unsigned int CScapeC::GetPhiIndexOffset(unsigned int PhiIndex, double CamOffset) throw(string)
{
	unsigned int PhiIndexOffset=(int)CamOffset % (int)GetPhiStepSize();
	if (PhiIndexOffset != 0)
	{
		string str("CScapeC::GetPhiIndexOffset() CamOffset not a multiplum of Phi stepsize");
		throw(str);
	}
	int NewIndex=PhiIndex+(int)(CamOffset/GetPhiStepSize());
	int TotalPhiSteps=GetTotalPhiSteps();
	if (NewIndex>=TotalPhiSteps)
	{
		NewIndex -= TotalPhiSteps;
	}
	return NewIndex;
}

bool CScapeC::Recognize2Cams(const vector<double>& Rec, const vector<double>& Cam2Rec, double CamOffset, 
							 vector<CGuess>& Guesses, unsigned int TotalGuesses)
{
	if (m_FeaturesInUse==false)
	{
		ostringstream ost;
		ost << "CScapeC::Recognize() Must call ReadFeatures() first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	Guesses.resize(TotalGuesses);
	FLOAT32 DistSq,buf;
	int p,p1,q;
	unsigned int Rho, Theta, Phi;
	bool Exit;
	//DistSq=0; moved into loop by edr
	for (p=0;p<TotalGuesses;p++)
		Guesses[p].m_DistSq=(p+1)*1e20;

	for (Rho=0;Rho<m_Features.size();Rho++) // Rho count
	{
		for (Theta=0;Theta<GetTotalThetaSteps();Theta++) // Theta count
		{
			for (Phi=0;Phi<GetTotalPhiSteps();Phi++) // Phi count
			{
				DistSq=0; // inserted by edr - moved from beginning
				for (q=1;q<=m_TotalFeatures;q++)
				{
					int Index=q;
					buf=Rec[Index]-m_Features[Rho][Phi][Theta].m_Features[Index];
					DistSq+=buf*buf/m_Variance[Index];
				}
				// add camera two feature vector
				unsigned int Cam2PhiIndex=GetPhiIndexOffset(Phi,CamOffset);
				for (q=1;q<=m_TotalFeatures;q++)
				{
					int Index=q;
					buf=Cam2Rec[Index]-m_Features[Rho][Cam2PhiIndex][Theta].m_Features[Index];
					DistSq+=buf*buf/m_Variance[Index];
				}
				p=TotalGuesses-1;
				Exit=(DistSq>Guesses[p].m_DistSq);
				if (!Exit)
				{
					do
					{
						p--;
						if (p==-1)
							Exit=true;
						else
							Exit=(DistSq>Guesses[p].m_DistSq);
					} while (!Exit);

					if (p<(int)(TotalGuesses-2))
					{
						for (p1=TotalGuesses-1;p1>(p+1);p1--)
							Guesses[p1]=Guesses[p1-1];
					}
					for(int count=1;count<=m_TotalFeatures;count++)
					{
						Guesses[p+1].m_Rec.m_Features[count]=m_Features[Rho][Phi][Theta].m_Features[count];
					}
					Guesses[p+1].m_Rec=m_Features[Rho][Phi][Theta];
					Guesses[p+1].m_DistSq=DistSq;
				} //if (!Exit)
			}//for for for
		}
	}
	return true;
}

bool CScapeC::GetGroupInImage(CPixelGroup& Grp,const CImage& Img)
{
	UINT8 R,G,B;
	int y;
	Grp.Empty();
	CImage::ConstRowIterator24bp it=Img.ConstRow24bp(0);
	CImage ImgGray(Img.GetWidth(),Img.GetHeight(),8,255);
	for(y=0; y<Img.GetHeight(); y++)
	{
		int x=0;
		it=Img.ConstRow24bp(y);
		while(it!=it.End())
		{
			UINT32 Color=*it;
			R=ipl::CPalette::GetRedVal(Color);
			G=ipl::CPalette::GetGreenVal(Color);
			B=ipl::CPalette::GetBlueVal(Color);
			if ((R!=0) || (G!=0) || (B!=0))
			{
				ImgGray.SetPixelFast(x,y,0);
			}
			x++;
			it++;
		}
	}
	CMaskOperation Mask;
	for(int Count=0; Count<m_BlurFactor; Count++)
	{
		Mask.BlurEightConnected3x3(ImgGray);
	}
	CImage::ConstRowIterator8bp it8=ImgGray.ConstRow8bp(0);
	for(y=0; y<ImgGray.GetHeight(); y++)
	{
		int x=0;
		it8=ImgGray.ConstRow8bp(y);
		while(it8!=it8.End())
		{
			UINT32 Color=*it8;
			if (Color<128)
//			if (Color!=255)
			{
				Grp.AddPosition(x,y);
			}
			x++;
			it8++;
		}
	}
//	ImgGray.Save("d:/temp/test.bmp");
	return true;
}

void CScapeC::LoadSetup(const char* pFilePathName) throw(string)
{
	try
	{
		ifstream is(pFilePathName);
		if (is)
		{
			is >> *this;
		}
		else
			throw(string("ERROR"));
		ostringstream ost;
		ost << "Setup loaded from file: " << pFilePathName << endl << endl;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
	}
	catch(...)
	{
		ostringstream ost;
		ost << "Failed opening config file: " << pFilePathName;
		throw(ost.str());
	}	
}

double CScapeC::GetAngleDifference(double a0, double b0, double c0, double a1, double b1, double c1)
{
	double ca0=cos(a0);
	double sa0=sin(a0);
	double cb0=cos(b0);
	double sb0=sin(b0);
	double cc0=cos(c0);
	double sc0=sin(c0);
	double ca1=cos(a1);
	double sa1=sin(a1);
	double cb1=cos(b1);
	double sb1=sin(b1);
	double cc1=cos(c1);
	double sc1=sin(c1);
	double a11 = cc0*cb0*cc1*cb1 + (cc0*sb0*sa0 + sc0*ca0)*(cc1*sb1*sa1 + sc1*ca1) +
				(sc0*sa0-cc0*sb0*ca0)*(sc1*sa1-cc1*sb1*ca1);
	double a22 = sc0*cb0*sc1*cb1 + (cc0*ca0-sc0*sb0*sa0)*(cc1*ca1-sc1*sb1*sa1) +
				(sc0*sb0*ca0+cc0*sa0)*(sc1*sb1*ca1+cc1*sa1);
	double a33 = sb0*sb1 + cb0*sa0*cb1*sa1 + cb0*ca0*cb1*ca1;
	double v = acos(((a11+a22+a33)-1)/2);
	return v;
}

ostream& operator<<(ostream& os, const CScapeC& ScapeC) throw(string)
{
	os << "# CScapeC setup file by edr@mip.sdu.dk" << endl << endl;
	os << "CScapeCVersion:   " <<  ScapeC.GetVersion() << endl << endl;
	os << "# Camera setup. If Camera not in use, all camera tokens should not be present" << endl;
	os << "CameraInitialized: " << ScapeC.m_CameraInitialized << endl;
	if (ScapeC.m_CameraInitialized==true)
	{
		os << "# Image size and ideal focal length" << endl;
		os << "Width:            " << ScapeC.m_Width << endl;
		os << "Height:           " << ScapeC.m_Height << endl;
		os << "FocalLengthIdeal: " << ScapeC.m_FocalLengthIdeal<< endl << endl;
		if (ScapeC.m_CalibFileInUse==true)
		{
			os << "# Calibration file" << endl;
			os << "CalibFile:   " <<  ScapeC.m_CalibFile << endl << endl;
			os << "# Camera parameters - alternative to using a calibration file" << endl;
			os << "# comment in the following lines and comment out the line with CalibFile" << endl;
			os << "#CamParameters: " << endl;
			if (ScapeC.m_pCam->ParametersAvailable()==true)
			{
				os << "#dx: " << ScapeC.m_pCam->m_Par.dx << endl;
				os << "#dy: " << ScapeC.m_pCam->m_Par.dy << endl;
				os << "#dz: " << ScapeC.m_pCam->m_Par.dz << endl;
				os << "#a: " << ScapeC.m_pCam->m_Par.a << endl;
				os << "#b: " << ScapeC.m_pCam->m_Par.b << endl;
				os << "#c:" << ScapeC.m_pCam->m_Par.c << endl;
				os << "## focal length in pixels" << endl;
				os << "#FocalLength:" << ScapeC.m_pCam->m_Par.FocalLength << endl << endl;
			}
			else
			{
				os << "#dx: " << endl << "#dy: " << endl << "#dz: " << endl
					<< "#a: " << endl << "#b: " << endl << "#c:" << endl << "#FocalLength:" << endl << endl;	
			}
		}
		else
		{
			os << "# Calibration file - alternativ to using camera parameters directly" << endl;
			os << "# Comment in the following line and comment out lines with dx,dy,dz,a,b,c, and f." << endl;
			os << "# CalibFile:   " <<  ScapeC.m_CalibFile << endl << endl;
			os << "# Camera parameters" << endl;
			os << "CamParameters: " << endl;
			os << "dx: " << ScapeC.m_pCam->m_Par.dx << endl;
			os << "dy: " << ScapeC.m_pCam->m_Par.dy << endl;
			os << "dz: " << ScapeC.m_pCam->m_Par.dz << endl;
			os << "a: " << ScapeC.m_pCam->m_Par.a << endl;
			os << "b: " << ScapeC.m_pCam->m_Par.b << endl;
			os << "c:" << ScapeC.m_pCam->m_Par.c << endl;
			os << "# focal length in pixels" << endl;
			os << "FocalLength:" << ScapeC.m_pCam->m_Par.FocalLength << endl << endl;
		}
	}
	os << "# Training setup, If training not in use, all training tokens should not be present" << endl;
	os << "TrainingInitialized: " << ScapeC.m_TrainingInitialized << endl << endl;
	if (ScapeC.m_TrainingInitialized==true)
	{
		os << "# Spherical settings - Rho is fixed to cam pose distance" << endl;
		os << "# values in degrees" << endl;
		os << "PhiMin:      " << ScapeC.m_PhiMin << endl;
		os << "PhiMax:      " << ScapeC.m_PhiMax << endl;
		os << "PhiStepSize: " << ScapeC.m_PhiStepSize << endl << endl;
		os << "ThetaMin:    " << ScapeC.m_ThetaMin << endl;
		os << "ThetaMax:    " << ScapeC.m_ThetaMax << endl;
		os << "ThetaStepSize: " << ScapeC.m_ThetaStepSize << endl << endl;

		os << "# Path to save" << endl;
		os << "SavePath:    " << ScapeC.m_SavePath << endl;
		os << "FilePrefix:  " << ScapeC.m_FilePrefix << endl << endl;
		os << "# Number of 3x3 blurs to perform on original CAD image before deriving features" << endl;
		os << "BlurFactor:  " << ScapeC.m_BlurFactor << endl;
		os << "# Compression level for the PNG genereated files (9 is highest compression) " << endl;
		os << "CompressionLevel: " << ScapeC.m_CompressionLevel << endl;
	}
	// finally stream m_HoopsControl attribute
	os << endl << ScapeC.m_HoopsControl << endl;
	return os;
}

istream& operator>>(istream& is, CScapeC& ScapeC) throw(string)
{
	string Token;
	ipl::SkipSpaceAndComments(is);
	// read 'CScapeCVersion' token
	is >> Token;
	if (Token!=string("CScapeCVersion:"))
	{
		ostringstream ost;
		ost << "operator>>(istream, CScapeC) Token CScapeCVersion not found"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		throw(ost.str());
		return is;
	}
	double Version;
	is >> Version;
	if (Version!=ScapeC.GetVersion())
	{
		ostringstream ost;
		ost << "operator>>(istream, CScapeC) File version is " << Version << " but this class "
			" versions is " << ScapeC.GetVersion() << " - trying to read rest of file anyway" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
	}
	try
	{
		char* pStr = new char[300];
		string Token;
		ipl::SkipSpaceAndComments(is);
		// skip 'CameraInitialized:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> ScapeC.m_CameraInitialized;
		if (ScapeC.m_CameraInitialized==true)
		{
			ipl::SkipSpaceAndComments(is);
			// skip 'Width:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_Width;
			ScapeC.m_ImageWidth=ScapeC.m_Width;
			ipl::SkipSpaceAndComments(is);
			// skip 'Height:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_Height;
			ScapeC.m_ImageHeight=ScapeC.m_Height;
			ipl::SkipSpaceAndComments(is);
			// skip 'FocalLengthIdeal:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_FocalLengthIdeal;
			ipl::SkipSpaceAndComments(is);
			if (ScapeC.m_pCam!=NULL)
				delete ScapeC.m_pCam;
			ScapeC.m_pCam = new CCameraModel(ScapeC.m_Width,ScapeC.m_Height);
			// read 'CalibFile:' or 'CamParameters:' token
			is >> Token;
			if (Token=="CalibFile:")
			{
				ipl::SkipSpaceAndComments(is);
				is.getline(pStr,300); // reads rest of line since item can consist of spaces
				ScapeC.m_CalibFile.assign(pStr);
				ScapeC.m_CalibFileInUse=true;
				// Load camera config file
				if (ScapeC.m_pCam->Load(ScapeC.m_CalibFile.c_str())==false)
				{
					ostringstream ost;
					ost << "operator>>(istream, CScapeC) Failed loading camera setup file: " 
						<< ScapeC.m_CalibFile << IPLAddFileAndLine;
					CError::ShowMessage(IPL_ERROR,ost.str().c_str());
				}
			}
			else if (Token=="CamParameters:")
			{
				ipl::TCameraParameters Params;
				ipl::SkipSpaceAndComments(is);
				// skip 'dx:' token
				is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
				is >> Params.dx;
				ipl::SkipSpaceAndComments(is);
				// skip 'dy:' token
				is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
				is >> Params.dy;
				ipl::SkipSpaceAndComments(is);
				// skip 'dz:' token
				is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
				is >> Params.dz;
				ipl::SkipSpaceAndComments(is);
				// skip 'a:' token
				is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
				is >> Params.a;
				ipl::SkipSpaceAndComments(is);
				// skip 'b:' token
				is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
				is >> Params.b;
				ipl::SkipSpaceAndComments(is);
				// skip 'c:' token
				is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
				is >> Params.c;
				ipl::SkipSpaceAndComments(is);
				// skip 'FocalLength:' token
				is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
				is >> Params.FocalLength;
				//ScapeC.m_pCam->m_Par.f=1/ScapeC.m_pCam->m_Par.FocalLength;
				ipl::SkipSpaceAndComments(is);
				ScapeC.m_CalibFileInUse=false;
				// Set camera parameters in CCameraModel
				ScapeC.m_pCam->Set(CPoint3D<ipl::FLOAT32>(Params.dx,Params.dy,Params.dz),Params.a,Params.b,Params.c,
					Params.FocalLength,CPoint2D<FLOAT32>(ScapeC.m_Width/2,ScapeC.m_Height/2),1,0,0);
			}
		}
		ipl::SkipSpaceAndComments(is);
		// skip 'TrainingInitialized:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> ScapeC.m_TrainingInitialized;
		if (ScapeC.TrainingInUse()==true)
		{
			ipl::SkipSpaceAndComments(is);
			// skip 'PhiMin:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_PhiMin;
			ipl::SkipSpaceAndComments(is);
			// skip 'PhiMax:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_PhiMax;
			ipl::SkipSpaceAndComments(is);
			// skip 'PhiStepSize:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_PhiStepSize;
			ipl::SkipSpaceAndComments(is);
			// skip 'ThetaMin:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_ThetaMin;
			ipl::SkipSpaceAndComments(is);
			// skip 'ThetaMax:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_ThetaMax;
			ipl::SkipSpaceAndComments(is);
			// skip 'ThetaStepSize:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_ThetaStepSize;
			ipl::SkipSpaceAndComments(is);
			// skip 'SavePath:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			ipl::SkipSpaceAndComments(is);
			is.getline(pStr,64); // reads rest of line since item can consist of spaces
			ScapeC.m_SavePath.assign(pStr);
			ipl::AddTrailingSlashToPath(ScapeC.m_SavePath);
			ipl::SkipSpaceAndComments(is);
			// skip 'FilePrefix:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			ipl::SkipSpaceAndComments(is);
			is.getline(pStr,64); // reads rest of line since item can consist of spaces
			ScapeC.m_FilePrefix.assign(pStr);
			ipl::SkipSpaceAndComments(is);
			// skip 'BlurFactor:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_BlurFactor;
			ipl::SkipSpaceAndComments(is);
			// skip 'CompressionLevel:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> ScapeC.m_CompressionLevel;
		}
		is >> ScapeC.m_HoopsControl;
		ScapeC.m_HoopsControl.SetImageDimensions(ScapeC.GetImageWidth(),ScapeC.GetImageHeight());
	}
	catch(...) // all exceptions
	{
		throw(string("Failed reading from data file containing a CScapeC object"));
	}
	return is;
}

} // end namespace videndo
