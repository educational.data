/* #include "pose.h"
namespace videndo { */
  using ipl::CError;
  using ipl::IPL_ERROR;
  using std::endl;
  using std::string;

  template <class T>
  CPose<T>::CPose()
  {
	  m_Empty=true;
	  m_R[0][0]=1;
	  m_R[1][0]=0;
	  m_R[2][0]=0;

	  m_R[0][1]=0;
	  m_R[1][1]=1;
	  m_R[2][1]=0;

	  m_R[0][2]=0;
	  m_R[1][2]=0;
	  m_R[2][2]=1;
	  m_LocalOrigin.Set(0,0,0);
  }

  template <class T>
  CPose<T>::CPose(const CPose &Pose)
  {
	  *this = Pose;
	  m_Empty=Pose.m_Empty;
  }

  template <class T>
  CPose<T>::CPose(T a, T b, T c,
			   CPoint3D<T> &LocalOrigin, int AxisOrderIndex)
  {
	  SetMatrix(a,b,c,LocalOrigin,AxisOrderIndex);
  }

  template <class T>
  CPose<T>::CPose(T a, T b, T c, int AxisOrderIndex)
  {
	  SetAngularMatrix(a,b,c,AxisOrderIndex);
  }

  template <class T>
  CPose<T>& CPose<T>::operator=(const CPose<T> &Pose)
  {
	  if (!Pose.m_Empty)
	  {
		  m_LocalOrigin=Pose.m_LocalOrigin;
		  for (int i=0;i<3;i++)
			  for (int j=0;j<3;j++)
				  m_R[i][j]=Pose.m_R[i][j];
			  m_Empty=false;
      }
      else 
		  m_Empty =true;
	  return *this;
  }

  template <class T>
  void CPose<T>::SetAngularMatrix(T a,T b, T c, int AxisOrderIndex)
  {
	  T ca=cos(a),
		  sa=sin(a),
		  cb=cos(b),
		  sb=sin(b),
		  cc=cos(c),
		  sc=sin(c),
		  sacc=sa*cc,
		  sasc=sa*sc,
		  casb=ca*sb;
	  m_LocalOrigin.Set(0,0,0);
	  if (AxisOrderIndex==0)
	  {
		  m_R[0][0]=cc*cb;  
		  m_R[0][1]= sacc*sb+ca*sc;  
		  m_R[0][2]=-casb*cc+sasc;
		  
		  m_R[1][0]=-sc*cb;  
		  m_R[1][1]=-sasc*sb+ca*cc;  
		  m_R[1][2]= casb*sc+sacc;
		  
		  m_R[2][0]=sb;      
		  m_R[2][1]=-cb*sa;           
		  m_R[2][2]= cb*ca;
	  }
	  else
	  {
		  m_R[0][0]=cb*ca;              
		  m_R[0][1]=sa*cb;            
		  m_R[0][2]=-sb;
		  
		  m_R[1][0]=-sacc+casb*sc;     
		  m_R[1][1]=sasc*sb+ca*cc;   
		  m_R[1][2]=sc*cb;
		  
		  m_R[2][0]=sasc+casb*cc;     
		  m_R[2][1]=sacc*sb-ca*sc;   
		  m_R[2][2]=cc*cb;
	  }
	  m_Empty=false;
  }

  template <class T>
  bool CPose<T>::SetMatrix(const CVector3D<T> &LocalX,const CVector3D<T> &LocalY,
					    const CPoint3D<T> &LocalOrigin)
  {
	  const T MIN_DOT_PRODUCT=1e-7;
	  if (NewX.IsZero())
	  {
		  ostringstream ost;
		  ost << "CPose<T>::SetMatrix() LocalX vector is a zero-vector" 
			  << IPLAddFileAndLine;
		  CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		  return false;
	  }
	  CVector3D<T> NewXLocal(NewX);
	  NewXLocal.Normalize();
	  if (NewY.IsZero())
	  {
		  ostringstream ost;
		  ost << "CPose<T>::SetMatrix() LocalY vector is a zero-vector" 
			  << IPLAddFileAndLine;
		  CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		  return false;
	  }
	  CVector3D<T> LocalY0(NewY);
	  LocalY0.Normalize();
	  if (LocalX0.GetDot(LocalY0)>MIN_DOT_PRODUCT)
	  {
		  ostringstream ost;
		  ost << "CPose<T>::SetMatrix() Dot product between NewX and NewY < " 
			  << MIN_DOT_PRODUCT << IPLAddFileAndLine;
		  CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		  return false;
	  }
	  CVector3D<T> LocalZ0(NewXLocal.GetCross(NewYLocal));
	  m_LocalOrigin=LocalOrigin;
	  m_R[0][0]=LocalX0.GetX();
	  m_R[0][1]=LocalX0.GetY();
	  m_R[0][2]=LocalX0.GetZ();
	  m_R[1][0]=LocalY0.GetX();
	  m_R[1][1]=LocalY0.GetY();
	  m_R[1][2]=LocalY0.GetZ();
	  m_R[2][0]=LocalZ0.GetX();
	  m_R[2][1]=LocalZ0.GetY();
	  m_R[2][2]=LocalZ0.GetZ();
	  m_Empty=false;
	  return true;
  }

  template <class T>
  CPoint3D<T> CPose<T>::TransformToLocal(const CPoint3D<T>& Point) const
  {
	  if (m_Empty)
	  {
		  ostringstream ost;
		  ost << "CPose<T>::Transform() This object not initialized - continuing with default values" 
			  << IPLAddFileAndLine;
		  CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	  }
	  CPoint3D<T> Buf;
	  T x=Point.GetX()-m_LocalOrigin.GetX();
	  T y=Point.GetY()-m_LocalOrigin.GetY();
	  T z=Point.GetZ()-m_LocalOrigin.GetZ();
	  Buf.SetX(m_R[0][0]*x+m_R[0][1]*y+m_R[0][2]*z);
	  Buf.SetY(m_R[1][0]*x+m_R[1][1]*y+m_R[1][2]*z);
	  Buf.SetZ(m_R[2][0]*x+m_R[2][1]*y+m_R[2][2]*z);
	  return Buf;
  }

  template <class T>
  CPoint3D<T> CPose<T>::TransformToGlobal(const CPoint3D<T> &Point) const
  {
	  if (m_Empty)
	  {
		  ostringstream ost;
		  ost << "CPose<T>::TransformBack() This object not initialized - continuing with default values" 
			  << IPLAddFileAndLine;
		  CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	  }
      CPoint3D<T> Buf;
	  T x=Point.GetX();
	  T y=Point.GetY();
	  T z=Point.GetZ();
      Buf.SetX(m_R[0][0]*x+m_R[1][0]*y+m_R[2][0]*z+m_LocalOrigin.GetX());
	  Buf.SetY(m_R[0][1]*x+m_R[1][1]*y+m_R[2][1]*z+m_LocalOrigin.GetY());
	  Buf.SetZ(m_R[0][2]*x+m_R[1][2]*y+m_R[2][2]*z+m_LocalOrigin.GetZ());
	  return Buf;
  }

  template <class T>
  T CPose<T>::GetAngleChange(T a1,T b1, T c1,
		                      T a2,T b2, T c2, int AxisOrderIndex)
  {
	  CPose Ps1,Ps2,Ps0;
	  Ps1.SetAngularMatrix(a1,b1,c1,AxisOrderIndex);
	  Ps2.SetAngularMatrix(a2,b2,c2,AxisOrderIndex);
          Ps0=Ps1.GetInverted()*Ps2;
	  return acos(0.5*(Ps0.m_R[0][0]+Ps0.m_R[1][1]+Ps0.m_R[2][2]-1));
  }

  template <class T>
  void CPose<T>::HomographicTransformation(T &a,T &b,T &c, 
							  T a1,T b1, T c1,
							  T a2,T b2, T c2, int AxisOrderIndex)
  {
	  T r00,r01,r02,r10,r11,r12,r20,r21,r22,cb,sb,ca,sa;
	  CPoint3D<T> P(0,0,0);
	  CPose<T> PT(a1,b1,c1-c2,AxisOrderIndex);
	  ca=cos(a2);
	  sa=sin(a2);
	  cb=cos(b2);
	  sb=sin(b2);
	  if (AxisOrderIndex==0)
	  {
		  r02=-ca*sb;
		  r12=sa;
		  r22=ca*cb;
		  r01=sa*sb;
		  r11=ca;
		  r21=-cb*sa;
		  r00=cb;
		  r20=sb;
		  c = atan2(-r01*PT.m_R[0][0]-r11*PT.m_R[1][0]-r21*PT.m_R[2][0],
			  r00*PT.m_R[0][0]+r20*PT.m_R[2][0]);	
		  b = asin(r02*PT.m_R[0][0]+r12*PT.m_R[1][0]+r22*PT.m_R[2][0]);
		  a = atan2(-r02*PT.m_R[0][1]-r12*PT.m_R[1][1]-r22*PT.m_R[2][1],
			  r02*PT.m_R[0][2]+r12*PT.m_R[1][2]+r22*PT.m_R[2][2]);
	  } 
	  else
	  {
		  r00=cb*ca;
		  r01=sa*cb;
		  r02=-sb;
		  r10=-sa;
		  r11=ca;
		  r20=ca*sb;
		  r21=sa*sb;
		  r22=cb;
		  c = atan2(r01*PT.m_R[0][2]+r11*PT.m_R[1][2]+r21*PT.m_R[2][2],
			  r02*PT.m_R[0][2]+r22*PT.m_R[2][2]);	
		  b = asin(-r00*PT.m_R[0][2]-r10*PT.m_R[1][2]-r20*PT.m_R[2][2]);
		  a = atan2(r00*PT.m_R[0][1]+r10*PT.m_R[1][1]+r20*PT.m_R[2][1],
			  r00*PT.m_R[0][0]+r10*PT.m_R[1][0]+r20*PT.m_R[2][0]);
	  }
  }

  template <class T>
  std::ostream& operator<<(std::ostream& s, const CPose<T>& Pose)
  {
	  s << "CPose  IsEmpty: " << Pose.IsEmpty() << endl;
	  if (Pose.IsEmpty()==false)
	  {
		  s   << "Local-to-global matrix: " << endl
			  << Pose.m_R[0][0] << "  " << Pose.m_R[1][0] << "  " << Pose.m_R[2][0] << "  " << Pose.m_LocalOrigin.GetX() << endl
			  << Pose.m_R[0][1] << "  " << Pose.m_R[1][1] << "  " << Pose.m_R[2][1] << "  " << Pose.m_LocalOrigin.GetY() << endl
			  << Pose.m_R[0][2] << "  " << Pose.m_R[1][2] << "  " << Pose.m_R[2][2] << "  " << Pose.m_LocalOrigin.GetZ() << endl;
	  }
	  return s;
  }

  template <class T>
  std::istream& operator>>(std::istream& is, CPose<T>& Pose)
  {
	  string Token;
	  T x,y,z;
	  ipl::SkipSpaceAndComments(is);
	  // read 'CPose' token
	  is >> Token;
	  if (Token!=strig("CPose"))
	  {
		  ostringstream ost;
		  ost << "operator>>(istream, CPose) Token CPose not found"
			  << IPLAddFileAndLine;
		  CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		  return is;
	  }
	  ipl::SkipSpaceAndComments(is);
	  // skip 'IsEmpty:' token
	  is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	  ipl::SkipSpaceAndComments(is);
	  is >> Pose.m_Empty;
	  if (Pose.IsEmpty()==false)
	  {
		  // skip 'Local-to-global matrix:' token
		  is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		  ipl::SkipSpaceAndComments(is);
		  is >> Pose.m_R[0][0] >> Pose.m_R[1][0] >> Pose.m_R[2][0]>>x;
		  ipl::SkipSpaceAndComments(is);
		  is >> Pose.m_R[0][1] >> Pose.m_R[1][1] >> Pose.m_R[2][1]>>y;
		  ipl::SkipSpaceAndComments(is);
		  is >> Pose.m_R[0][2] >> Pose.m_R[1][2] >> Pose.m_R[2][2]>>z;
		  Pose.m_LocalOrigin.Set(x,y,z);
	  }
	  return is;			
  }
// }