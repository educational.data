#ifndef _VIDENDO_MARKPOSE_H
#define _VIDENDO_MARKPOSE_H

#pragma warning(disable: 4786)

#include "videndo_setup.h" /* always include the videndo setup file */
#include <points/point3d.h>
#include <ipl98/cpp/vectors/vector3d.h> 

namespace videndo{ // use namespace if C++

using ipl::CVector3D;

/** This class contains information for a reference mark, that is (x,y,z) pose, 
	orientation (direction) and identifikation number. The class is typically 
	used in conjunction with CMarkFinder.

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CMarkPose videndo/mark_pose.h
	@version 0.10
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CMarkPose
{
	protected: // attributes
		/** Set to true if Set() has been successfully called. */
		bool m_Initialized;
		/** Identifikation number */
		unsigned int m_Id;
		/** Center of reference mark in 3D. */
		CPoint3D<double> m_MarkCenter;
		/** Direction for reference mark, z-value always zero. */
		CVector3D<double> m_Direction;
	public: // methods
		/// Default constructor
		CMarkPose();
		/** Sets id, pose (mark center) and orientation (direction) for this class.
			@param Id Identifikation number for reference mark.
			@param MarkCenter Pose for the reference mark. When this class is used
				in conjunction with CMarkFinder, it should be noted that the Z-value of
				the pose is given in a setup file, that is only the (x,y)-coordinates
				are found by the algorithm CMarkFinder::FindMarks().
			@param Direction The vector direction of the reference mark. When this class is used
				in conjunction with CMarkFinder, it should be noted that the Z-value of
				the direction is given in a setup file, that is only the (x,y)-coordinates
				are found by the algorithm CMarkFinder::FindMarks().
			@return True, always. */
		bool Set(unsigned Id, const CPoint3D<double>& MarkCenter, const CVector3D<double> Direction);
		/** Returns identifikation number.
			@return Identifikation number. */
		unsigned int GetId() const;
		/** Returns pose, i.e. the center of a mark.
			@return Pose for this reference mark. */
		const CPoint3D<double>& GetMarkCenter() const;
		/** Returns the direction of mark center.
			@return Direction as a 3D vector for this reference mark. */
		const CVector3D<double>& GetDirection() const;
};


/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

} // end namespace videndo

#endif //_VIDENDO_MARKPOSE_H
