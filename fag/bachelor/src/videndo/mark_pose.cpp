#include "mark_pose.h"

namespace videndo{ // use namespace if C++

CMarkPose::CMarkPose()
{
	m_Initialized=false;
}

bool CMarkPose::Set(unsigned int Id, const CPoint3D<double>& MarkCenter, const CVector3D<double> Direction)
{
	m_Initialized=true;
	m_Id=Id;
	m_MarkCenter=MarkCenter;
	m_Direction=Direction;
	return true;
}

unsigned int CMarkPose::GetId() const
{
	return m_Id;
}

const CPoint3D<double>& CMarkPose::GetMarkCenter() const
{
	return m_MarkCenter;
}

const CVector3D<double>& CMarkPose::GetDirection() const
{
	return m_Direction;
}

} // end namespace videndo
