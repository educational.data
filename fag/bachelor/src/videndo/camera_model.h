#ifndef _VIDENDO_CAMERAMODEL_H
#define _VIDENDO_CAMERAMODEL_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <ipl98/cpp/algorithms/perspective.h>
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/std_image.h>

namespace videndo{ // use namespace if C++

using std::ostream;
using ipl::CPerspective;
using ipl::CStdImage;
using ipl::CImage;
using ipl::TCameraMatrix;
using ipl::FLOAT32;

/** CCameraModel is derived from CPerspective and includes in addition ideal images where
	focal length is normalized, and skewness and aspect ratio is set to 1. 

	Methods for dealing with ideal images:
	An ideal representation of the world is necessary when working with different cameras, the
	ideal image should not be considered a normal image, it has no dimensions, it's center is in
	the focal point (xh,yh) and its scale is set to a standardized focal length (FocalLengthIdeal).
	An ideal window has the following properties:
	no distortion,
	skewness beta=1,
	aspect ratio p=1,
	focal length FocalLengthIdeal=1000, can be changed!
	focal center (xh,yh)=(0,0), i.e. origo is set to (xh,yh)
	A set of methods are also provided for dealing with ideal images, most important are:
	CreateIdealView(), CreateIdealImage(), GetPosReal(), GetPosIdeal(). With these methods it is possible
	to find corresponding points between different cameras, if the cameras have the same pose, i.e.
	same 6 external parameters. Example:

	\verbatim
	// The container holding the corresponding 3D-2D values.
	CCorresponding3D2DPoints PointSets;

	// Insert some code that fill out the PointSets variable with at least six sets
	// .... 

	// Creates a CCameraModel object, setting image size and using default
	// ideal focal length by not providing this value, defaults to 1000.
	CCameraModel Persp(768,576);

	if (Persp.CalibrateWithRadialDist(PointSets)==true)
	{
		// print the calibration result
		cout << Persp << endl;

		// Create an ideal view with an actual image (Source) inserted
		CImage Source, Dest;
		Source.Load("a_source_image_to_be_converted.bmp");
		// Creating view of size 1024*768. Assuming Source is 8 b/p grayscale
		// we set positions without real image information to white (255)
		Persp.CreateIdealView(Dest,Img,1024,768,255);
		// Dest now contains the ideal image with Source inserted
		Dest.Save("ideal.bmp");
	}
	else
		cout << "Calibration failed" << endl;
	\endverbatim

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CCameraModel videndo/camera_model.h
	@version 1.51
	@author Ivar Balslev (ivb@mip.sdu.dk), Brian Kirstein (kirstein@protana.com) and
		Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CCameraModel : public CPerspective{
	public: // attributes
	protected: // attributes
		/** Focal length used for ideal image. GetPositionReal() and GetPositionIdeal() use 
			this value to get normalized positions, i.e. scaling all positions to this
			value. A typical value is 1000. Set by constructor. */
		double m_FocalLengthIdeal;
		/** Constants used for fast calculation of GetPosReal() and GetPosIdeal(). 
			Set by CalcRealIdealConstants(). */
		double m_C1, m_C2, m_C3, m_C4, m_C5, m_C6, m_C7, m_C8;
        bool m_RollAdjustment;
        double m_sinC,m_cosC;
	public: // methods
		/** Constructor setting the image size and ideal focal length to be used
			in this object.
			@param Img Must contain an image with dimensions to be used in this object,
				if image not available yet, call other constructor passing image dimensions
				as scalars.
			@param FocalLengthIdeal The focal length to scale ideal image to, see
				attribute m_FocalLengthIdeal for more info. Defaults to 1000 mm */
		CCameraModel(const CStdImage& Img, double FocalLengthIdeal=1000);
		/** Constructor setting the image size and ideal focal length to be used
			in this object.
			@param Width Width of images to be used for this object.
			@param Height Height of images to be used for this object.
			@param FocalLengthIdeal The focal length to scale ideal image to, see
				attribute m_FocalLengthIdeal for more info. Defaults to 1000 mm */
		CCameraModel(unsigned int Width, unsigned int Height, double FocalLengthIdeal=1000);
		/** Same as constructor CCameraModel(Width,Height,FocalLengthIdeal), except
			image dimensions are given as a CPoint2D<unsigned int> type. */
		CCameraModel(CPoint2D<unsigned int> Dimensions, double FocalLengthIdeal=1000);
		/// default destructor
		~CCameraModel();

		/** Overloading of assignment operator. All information is copied to the 
			destination. Previous data in this object are destroyed. An error is
			produced if both sides of the assignment operator is the same object. */
		CCameraModel& operator=(const CCameraModel& Source);

		/** Sets all parameters in this class, previous values are destroyed. 
			The internal attributes m_CamMatrixCalibrated and
			m_RadCalibrationPerformed are set to true. Note: Camera parameters are
			not calculated, must call MatrixToParameters() to make them available. */
		bool Set(const TCameraMatrix& Cam, double k, unsigned int Width, unsigned Height, 
				double FocalLengthIdeal=1000);

		/** Sets the camera parameters, the corresponding camera matrix will be calculated from
			these values.
			@param Pinhole Camera pinhole, that is (dx,dy,dz) in m_Par.
			@param a Tilt in radians, a in m_Par.
			@param b Pan in radians, b in m_Par.
			@param a Roll in radians, c in m_Par.
			@param FocalLength in pixels, FocalLength in m_Par (f in m_Par will also be calculated).
			@param FocalPoint in pixels on chip, (xh,yh) in m_Par.
			@param p Aspect ratio, scale of V relative to U, p in m_Par.
			@param Beta Skewness of V relative to U, beta in m_Par.
			@param k Radial distortion.
			@param Width Image dimension, used to set center for radial distortion.
			@param Height Image dimension, used to set center for radial distortion.
			@param FocalLengthIdeal The focal length in an ideal image. */
		bool Set(const CPoint3D<FLOAT32>& Pinhole, FLOAT32 a, FLOAT32 b, FLOAT32 c, FLOAT32 FocalLength, 
					const CPoint2D<FLOAT32>& FocalPoint, FLOAT32 p, FLOAT32 Beta, double k, 
					unsigned int Width, unsigned Height, double FocalLengthIdeal=1000);
		/** Same as other Set() method, just using width, height, and FocalLengthIdeal already set 
			in this class. */
		bool Set(const CPoint3D<FLOAT32>& Pinhole, FLOAT32 a, FLOAT32 b, FLOAT32 c, FLOAT32 FocalLength, 
					const CPoint2D<FLOAT32>& FocalPoint, FLOAT32 p, FLOAT32 Beta, double k);

//		/**	Returns the maximum displacement in a real image constructed from a source image.
//			Done by displacing all corner positions in source image relative to its origo, and finding
//			the maximum displacement of those.
//			@return Maximum displacement relative to focal point (xh,yh). */
//		CPoint2D<int> GetMaxIdealDisplacement(const CStdImage& Source) const;

		/** Allocates an ideal image capable of holding all pixel positions in given
			source image. Radial calibration must be performed.
			@return False, if radial calibration has not been performed. */
		bool AllocIdealImage(CImage& Dest, ipl::UINT16 Bits, ipl::UINT32 Color, const CStdImage& Source) const;

		/** Creates an ideal image from the given source image, that is an image with
			radial distortion removed, skewness beta=1, aspect ratio p=1, and focal center (xh,yh)=(0,0),
			i.e. origo is set to (xh,yh). If PreserveImageSize is false, the image is exactly the size 
			needed to show all original data and having the focal center in the center of the image.
			Background with no real image information is set to white. This method is mostly for visual 
			inspection, create an ideal image with specified size call CreateIdealView() instead. Only 
			available in C++.
			@param Dest Destination of new image, may be the same as Source. Note:
				In case of same source and destination the algorithm is a bit slower.
			@param Source Source image, may be the same as Dest. Note:
				In case of same source and destination the algorithm is a bit slower.
			@param PreserveImageSize if true, the destination image has same dimensions as source.
				Otherwise the new image will have the dimension needed to represent the radial corrected
				image without loss of information.
			@param f Focal length for ideal image, if no value provided, the ideal focal length for this
				object is returned, i.e. the value returned by calling GetFocalLengthIdeal().
			@return False, if Img is not an 8 bit gray tone image.
			@see CreateIdealView
			@version 0.7 */
		bool CreateIdealImage(CImage& Dest, CStdImage& Source, bool PreserveImageSize, double f=0);

		/** Creates an ideal image window from the given source image, that is an image with
			radial distortion removed, skewness beta=1, aspect ratio p=1, and focal center (xh,yh)=(0,0),
			i.e. origo is set to (xh,yh). The image size is set to the provided dimensions: (Width,Height).
			Origo (xh,yh) is fixed to center of image. Only available in C++.
			@param Dest Destination of new image, may be the same as Source. Note:
				In case of same source and destination the algorithm is a bit slower.
			@param Source Source image, may be the same as Dest. Note:
				In case of same source and destination the algorithm is a bit slower.
			@param Color If the new ideal image contains areas without information from the source
				image, it will contain this color. 
			@param f Focal length for ideal image, if no value provided, the ideal focal length for this
				object is returned, i.e. the value returned by calling GetFocalLengthIdeal().
			@return False, if Img is not an 8 bit gray tone image.
			@version 0.7 */
		bool CreateIdealView(CImage& Dest, CStdImage& Source, unsigned int Width, unsigned int Height, 
							ipl::UINT32 Color, double f=0);

		/** Returns the ideal focal length used in this object. Only available in C++.
			@return Focal length for ideal view. */
		inline double GetFocalLengthIdeal() const;

		/** Sets the ideal focal length used in this object. Only available in C++.
			Camera parameters must be available in order to calculate constants for ideal focal length.
			Calls CalcRealIdealConstants() to update constants.
			@param FocalLengthIdeal Ideal focal length to be used.
			@return False, if FocalLengthIdeal is negative or zero, or if camera parameters are not available. */
		bool SetFocalLengthIdeal(double FocalLengthIdeal);
		
		/** Returns the position in a real image with origo in upper left corner, given a position 
			in an ideal image (see general comments	for this class about ideal image). The user 
			must be sure that camera calibration data including radial distortion parameter, is
			available. No check is performed in order to make the method fast. Only available in C++.
			@param IdealPos Position in an ideal image.
			@return Position in real image corresponding to the provided ideal position.
				Origo in upper left corner.
			@version 0.7 */
		inline CPoint2D<FLOAT32> GetPosReal(const CPoint2D<FLOAT32>& IdealPos) const;

		/** Same as GetPosReal(CPoint2D...), except position is given as two FLOAT32 types.
			CalcRealIdealConstants() must have been called first, which is done by
			automatically by all relevant methods in this class. Only available in C++.
			@return Position in real image corresponding to the provided ideal position.
				 Origo in upper left corner.
			@version 0.7 */
		inline CPoint2D<FLOAT32> GetPosReal(FLOAT32 IdealPosX, FLOAT32 IdealPosY) const;

		/** Returns the position in an ideal image givel a position in a real image (see general comments
			for this class about ideal image). The user must be sure that camera calibration data
			including radial distortion parameter, is available. No check is performed in order to
			make the method fast. Only available in C++.
			CalcRealIdealConstants() must have been called first, which is done by
			automatically by all relevant methods in this class.
			@param RealPos Position in a real image, with origo in upper left corner.
			@return Position in ideal image corresponding to the provided real position. */
		inline CPoint2D<FLOAT32> GetPosIdeal(const CPoint2D<FLOAT32>& RealPos) const;

		/** Same as GetPosIdeal(CPoint2D...), except position is given as two FLOAT32 types.
			Only available in C++.
			@return Position in ideal image corresponding to the provided real position. */
		inline CPoint2D<FLOAT32> GetPosIdeal(FLOAT32 RealPosX, FLOAT32 RealPosY) const;

		/** writes the camera matrix data to stream and the camera parameters by calling
			CamParametersToStream.
			@see CamParametersToStream */
		friend ostream& operator<<(ostream& s,const CCameraModel& Persp);

        /** Derives the camera parameters from a precalculated camera
            matrix using the method presented in the PhD Thesis
            "Industrial Vision" by Ole Knudsen, 1997, chapter 2.2.
			The results are stored in the attribute m_Par. In addition the public attribute
			m_CamParametersAvailable is set to true and the method CalcRealIdealConstants()
			is called. This methods overwrites the base class' method with same name,
			only addition here is that some constants are calculated to speed up the
			ideal/real transformations of points.
	        @return False, if the camera matrix has not been calculated.
            @version 0.6
            @author Implementation by Brian Kirstein Ramsgaard (kirstein@protana.com).
			@see m_Par */
        bool MatrixToParameters();

		/** Load camera matrix and distortion parameter from file. Camera parameters are
			calculated, if loading succeeds, by calling MatrixToParameters. Tokens in file are:
			PerspectiveVersion, the perspective class version (double)
			Width, image width.
			Height, image heigh.
			FocalLengthIdeal, the ideal focal length from attribute m_FocalLengthIdeal.
				If it is not found (could be caused by an earlier version of this class where
				ideal focal length were not present), a warning is given and the default value
				of 1000 is used.
			CameraMatrix, the 4*4 camera matrix (12 doubles)
			k, distortion parameter (double). If not available the appropriate attributes
				is set in this class.
			If version of calibration file is different than this class version, a warning
			is given.
			Comment lines starts with a '#' character, can be placed everywhere.
			Note: This methods is only available for the C++ part.
			@param pPathAndFileName Name of file including extension and relative
				or absolute path.
			@return False, if file could not be opened or if the format is not correct.
			@version 0.9 */
		bool Load(const char* pPathAndFileName);

		/** Save camera matrix, distortion parameter and version number of this calibration
			class. If no calibration data available, a message is written to cerr. If radial
			distortion has not been calibrated, the distortion parameter is not written to file.
			Note: This methods is only available for the C++ part.
			@param pPathAndFileName Name of file and relative or absolute path. If no extension 
				is supplied in the filename a "cfg" extension is automatically added.
			@param Comments Extra comments to be added to the beginning of the file. No need
				to insert a leading '#', the method will do that. If you want more than one
				line use '\n' control character to split.
			@return False, if file could not be opened for writing or if no calibration
				data available.
			@version 0.9 */
		bool Save(const char* pPathAndFileName, const char* pComments=NULL) const;

		/** Activates roll adjustments and calculates constants used for it.
			@param RefCamera Reference camera where roll is adjusted to. */
        void SetRollAdjustment(CCameraModel &RefCamera);
		/** Inactivates roll adjustment. */
        void RemoveRollAdjustment();

	private:
		/** Calculates the constants in m_C1..m_C8 in order to make calls to GetPosReal() and
			GetPosIdeal() fast. Parameters must be available and m_FocalLengthIdeal set to relevant
			value.
			@return False, if camera parameters are not available. */
		bool CalcRealIdealConstants();
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

inline double CCameraModel::GetFocalLengthIdeal() const
{
	return m_FocalLengthIdeal;
}

inline CPoint2D<FLOAT32> CCameraModel::GetPosReal(const CPoint2D<FLOAT32>& IdealPos) const
{
	return GetPosReal(IdealPos.GetX(), IdealPos.GetY());
}	

inline CPoint2D<FLOAT32> CCameraModel::GetPosReal(FLOAT32 IdealPosX, FLOAT32 IdealPosY) const
{
	CPoint2D<FLOAT32> P0;
	if (!m_RollAdjustment)
		P0.Set(m_C1*IdealPosX+m_Par.xh,m_C2*IdealPosX+m_C3*IdealPosY+m_Par.yh);
	else
	{
		FLOAT32 u,v,u1,v1;
		u1=IdealPosX;
		v1=IdealPosY;
		u=m_cosC*u1-m_sinC*v1;
		v=m_sinC*u1+m_cosC*v1;
		P0.Set(m_C1*u+m_Par.xh,m_C2*u+m_C3*v+m_Par.yh);
	}
	return GetPosInverseRad(P0);
//	CPoint2D<FLOAT32> P0(m_C1*IdealPosX+m_Par.xh,m_C2*IdealPosX+m_C3*IdealPosY+m_Par.yh);
//	return GetPosInverseRad(P0);
}

inline CPoint2D<FLOAT32> CCameraModel::GetPosIdeal(const CPoint2D<FLOAT32>& RealPos) const
{
	FLOAT32 u,v;
	CPoint2D<FLOAT32> P0(GetPosRadRemoved(RealPos));
	CPoint2D<FLOAT32> P1(m_C4*P0.GetX()-m_C5,m_C6*P0.GetX()+m_C7*P0.GetY()+m_C8);
	if (!m_RollAdjustment) 
		return P1; 
	else
	{  
		u=P1.GetX();
		v=P1.GetY();
		return CPoint2D<FLOAT32>(m_cosC*u+m_sinC*v,-m_sinC*u+m_sinC*v);
	}  
//	CPoint2D<FLOAT32> P0(GetPosRadRemoved(RealPos));
//	return CPoint2D<FLOAT32>(m_C4*P0.GetX()-m_C5,m_C6*P0.GetX()+m_C7*P0.GetY()+m_C8);
}

inline CPoint2D<FLOAT32> CCameraModel::GetPosIdeal(FLOAT32 RealPosX, FLOAT32 RealPosY) const
{
	return GetPosIdeal(CPoint2D<FLOAT32>(RealPosX,RealPosY));
}

} // end namespace videndo

#endif //_VIDENDO_CAMERAMODEL_H
