#ifndef _VIDENDO_MARK_RETRIEVAL_H
#define _VIDENDO_MARK_RETRIEVAL_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include "kernel_c/kernel_mark_retrieval.h"
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/std_image.h>
#include <ipl98/cpp/corresponding_3d2d_points.h>
#include <ipl98/cpp/vectors/vector2d.h>
#include <ipl98/cpp/pixelgroups.h>
#include <iostream>
#include <vector>
#include <ostream>

namespace videndo{ // use namespace if C++

using std::string;
using std::vector;
using std::ostream;
using ipl::CCorresponding3D2DPoints;
using ipl::CImage;
using ipl::CStdImage;
using ipl::UINT8;
using ipl::CVector2D;
using ipl::CPixelGroups;

class CRefMark;

/** CMarkRetrieval system developed at MIP, Last updated 4/30/2003.
	This class provides a system for finding reference
	marks in an image containing a picture of a calibration plate (or at least some
	reference marks!). At present there
	are two methods available, future experience will show which one turns out to be
	the best. The FindMarksAutoLocalThreshold() method should be the best of the two.

	Note 1: There are many definitions in the kernel C header file that can be
		adjusted to optimize the search criterias for reference.
	
	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CMarkRetrieval videndo/mark_retrieval.h
	@version 0.93
	@author Brian Kirstein Ramsgaard (kirstein@protana.com) & Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CMarkRetrieval{
public: // attributes
	/** Enumeration defining what kind of reference marks are to be used. 
		To mix some of them, simply make a binary or operation with the 
		wanted names. For instance REFMARK_BIG & REFMARK_SMALL */
	enum REFMARKTYPE {REFMARK_BIG=1, REFMARK_SMALL=2};
	/** The container holding the corresponding 3D-2D values. */
	CCorresponding3D2DPoints m_PointSets;
	/** A container holding new found 2D values, the corresponding 3D values are not set.
		The user of this class must decide what to do with these new marks.
		New points are only added if SetNewRefMarks() has been called. */
	CCorresponding3D2DPoints m_NewPointSets;
	/** A container holding 2D centers for the code start circle for each reference marks.
		Indexes corresponds to the indexes in m_NewPointSets. Only containes information
		when SetNewRefMarks() has been called. */
	vector<CPoint2D<double> > m_NewCSCenters;
	/** Keep track of the state of calls to FindMarksAdaptiveReturn.
		When true the values in m_AllMarksFound, m_Up and m_Down are valid. 
		Set to true first time FindMarksAdaptiveReturn is called. */
	bool m_AdaptiveReturnStarted;
	/** Set to true, when FindMarksAdaptiveReturn has found all reference
		marks. Use this attribute to check when your loop should stop. 
		Also set by FindMarksAdaptive(). */
	bool m_AllMarksFound;
private: // attributes
	/** Used to hold the 2D center for the code start circle for each reference
		mark.*/
	vector<CPoint2D<double> > m_CSCenters;
	/** Original image to find reference marks in, set by InitAdaptiveReturn. */
	CImage m_OriginalImg;
	/** Temporary internal image, used by FindMarksAdaptiveReturn. */
	CImage m_TempImg;
	/** Holds the threshold for the iterations with increasing thresholds. 
		Internal use, FindMarksAdaptiveReturn updates the value. */
	unsigned short m_Up;
	/** Holds the threshold for the iterations with decreasing thresholds. 
		Internal use, FindMarksAdaptiveReturn updates the value. */
	unsigned short m_Down;
	/** Contains the total refmarks found so far by call to FindMarksAdaptiveReturn. */
	unsigned int m_TotalRefMarks;
	/** Stores the threshold values where refmarks are found, saved in file for future use. */
	vector<unsigned short> m_ThresholdList;
	/** File to write new thresholdlist to. Set by InitAdaptiveReturn. */
	string m_ThresholdFile;
	/** If true, important information is written to stdout. Used by 
		FindMarksAdaptiveReturn. */
	bool m_Verbose;
	/** Set by InitAdaptiveReturn(). */
	unsigned int m_RefMarkType;
	/** Set by InitAdaptiveReturn(). */
	unsigned int m_MinBlobSize;
	/** Set by InitAdaptiveReturn(). */
	bool m_UseSimpleCenter;

	bool m_AllMarksNeeded;
	unsigned int m_MinMarks;
	/// Initialized to GLOBAL_CALIB_THRESH_STEP
	unsigned int m_ThresholdStep;
	/// Initialized to GLOBAL_CALIB_THRESH_UPPER_BOUND
	unsigned int m_ThresholdUpperBound;
	/// Initialized to GLOBAL_CALIB_THRESH_LOWER_BOUND
	unsigned int m_ThresholdLowerBound;
	bool m_NotAllMarksFast;
	/// Initialized to false. Set to true, when user calls SetThresholdValues()
	bool m_UseUserThresholds;
	/// Set by SetThresholdValues()
	unsigned int m_UserThresholdUpperBound;
	/// Set by SetThresholdValues()
	unsigned int m_UserThresholdLowerBound;
	/// Set by SetNewRefMarks(), initialized to false.
	bool m_FindNewRefMarks;
public: // methods
	CMarkRetrieval();
	~CMarkRetrieval(); //destructor

/** Finds reference marks by using different global thresholds. The user must 
	have filled the member attribute m_PointSets (type CCorresponding3D2DPoints) 
	with relevant information, that is normally 3D positions and corresponding 
	Id's, see ReadWorldCoordinates() for more info. This method will add the 
	found 2D image positions at the corresponding Id in m_PointSets. A file is 
	generated containing the threshold values where reference marks were found. 
	If a threshold file already exists at startup, the mean value 
	of thresholds stored in the file is used as starting point. The standard 
	deviation is used to estimate the step size for thresholdvalues. For each
	global threshold the algorithm searches for the three center circles (or
	two if you use the newer reference mark types, see REFMARKTYPE). If 
	a set of three circles with same center of mass are found, it finds a local
	threshold according to the graytone values in the two outer circles	(CC1 
	and CC2). A subimage around the refmark is then thresholded with the new 
	value and the code circles are found. 
	Note: This method is only available in the C++ version.
	@param Img Source image. Must be 8 b/p.
	@param ThresholdFile File name of file to store threshold values. If the file 
		doesn't exist, this method will create the file.
	@param RefMarkType Type of refmarks to be used. See REFMARKTYPE for more info.
	@param Verbose If 1, important information is written to stdout, and the
		center of all circles in a reference mark are drawn directly in "Img".
		Higher numbers results in more detailed output.
	@param ThresholdStep If not supplied (defaults to 0), then a threshold 
		stepsize is calculated based on the std. deviation of the thresholds 
		used previously. Otherwise this value is used.
	@param MinBlobSize Minimum blobsize to be considered as part of a reference 
		mark, defaults to 7.
	@param UseSimpleCenter If true, the center for a reference mark is simply found 
		as the center of mass for the center disc (CC0). This is the old method.
	@return False, if source/target image is empty or not 8 b/p, if either the
		plate configuration file or the plate setup file has not been read,
		if origo of input image is not (0,0), or if
		the found id's does not match the id's given in the setup file.
	@version 0.92
	@see FindMarksAdaptiveReturn */
	bool FindMarksAdaptive(CStdImage& Img, string ThresholdFile, unsigned int RefMarkType, bool Verbose, unsigned short ThresholdStep=0, 
							unsigned int MinBlobSize=7, bool UseSimpleCenter=false);

	/** Same as FindMarksAdaptive, but this function returns after each iteration. You
		need to call InitAdaptiveReturn(...) first and after calling this function,
		call ResetAdaptiveReturn(). With this method, the main program can stop the 
		algorithm before it finishes if needed. 
		Note, that the return value is different!
		In order to go through all iterations make a while loop that continues as long
		as the attribute m_AllMarksFound is false.
		Note: This method is only available in the C++ version.
		@return False, if InitAdaptiveReturn has not been called or if
			all thresholds in the valid range has been used without finding all
			reference marks. In all other cases true is returned. To check if all
			reference marks have been found, check the m_AllMarksFound attribute.
		@version 0.91
		@see FindMarksAdaptive
		@see InitAdaptiveReturn
		@see ResetAdaptiveReturn */
	bool FindMarksAdaptiveReturn();

	/** Initializes attributes needed to run FindMarksAdaptiveReturn().
		Note: This method is only available in the C++ version.
		@param Img Source image. Must be 8 b/p.
		@param ThresholdFile File name of file to store threshold values.
		@param Verbose If not 0, important information is written to stdout, and the
			center of all circles in a reference mark are drawn directly in "Img".
			Higher numbers results in more detailed output.
		@param ThresholdStep If not supplied (defaults to 0), then a threshold stepsize is calculated
			based on the std. deviation of the thresholds used previously. Otherwise this value is used.
		@return False, if source/target image is empty or not 8 b/p, if either the
			plate configuration file or the plate setup file has not been read, or
			if origo of input image is not (0,0).
		@version 0.92
		@see FindMarksAdaptiveReturn
		@see ResetAdaptiveReturn */
	bool InitAdaptiveReturn(CStdImage& Img, string ThresholdFile, unsigned int RefMarkType, bool Verbose, unsigned short ThresholdStep=0,
							unsigned int MinBlobSize=7, bool UseSimpleCenter=false);
	/** Resets attributes after FindMarksAdaptiveReturn() has been used, should
		always be called after using FindMarksAdaptiveReturn(). */
	void ResetAdaptiveReturn();

	/** Reads a set of world coordinates.
		General remarks: The file is constructed
		from a set of tokens with one or more appropriate values seperated by whitespace. Comments can 
		be written only at whole lines, but placed anywhere in the file, begins with the character '\#'. 
		The order of tokens is not important.
		Specific information for the world coordinate file:
			WorldCoordinates token followed by the total number of world coordinates in this file.
			After this follows the coordinates and an id like this: x y z id
		@return False, if world coordinate file is illegal. */
	bool ReadWorldCoordinates(const char* FileName);
	/** Writes world coordinates according to how ReadWorldCoordinates() reads thsi file.
		@return False, if world coordinate file is illegal. */
	bool WriteWorldCoordinates(const char* FileName);
	/** Returns version of this class. */
	inline double GetVersion() const{return IPL98_MARKRETRIEVAL_VERSION;}
	/** Returns lower value for dynamic threshold. */
	unsigned int GetThresholdLowerBound() const;
	/** Returns upper value for dynamic threshold. */
	unsigned int GetThresholdUpperBound() const;
	/** Returns step size for dynamic threshold. Note: This value is used only when supplied to FindMarksAdaptive()
		or FindMarksAdaptiveReturn(). */
	unsigned int GetThresholdStep() const;
	/** Sets the threshold values used for dynamic thresholding.
		@param LowerBound Must be in the interval [0;254] and smaller than UpperBound.
		@param UpperBound Must be in the interval [1;255], and bigger than LowerBound.
		@param Step Must be in the interval [0;255] and smaller than UpperBound-LowerBound.
			If 0 (default), the threshold step size is left unchanged.
		@return False, if one of the conditions above are not fulfilled. */
	bool SetThresholdValues(unsigned int LowerBound, unsigned int UpperBound, unsigned int Step=0);
	/** If this method is called, then FindMarksAdaptive() and FindMarksAdaptiveReturn() will
		return true if it found N reference marks, where N>=MinMarks. To speed up the search
		for reference marks, the method SetNotAllMarksFast() can be called. But in this case,
		some marks may never be found.
		@param MinMarks Minimum number of marks to be found.
		@see SetAllMarksNeeded
		@see SetNotAllMarksFast */
	bool SetNotAllMarksNeeded(unsigned int MinMarks)
	{
		if (MinMarks==0)
			return false;
		m_AllMarksNeeded=false;
		m_MinMarks=MinMarks;
		return true;
	}
	/** Resets a previous call to SetNotAllMarksNeeded(). Following calls
		to FindMarksAdaptive() and FindMarksAdaptiveReturn() will only return
		true if all marks were found. */
	void SetAllMarksNeeded()
	{
		m_AllMarksNeeded=true;
	}
	/** This method only has effect if a previous call to SetNotAllMarksNeeded() succeeded.
		If parameter set to true, the methods FindMarksAdaptive() and the FindMarksAdaptiveReturn()
		will only search with thresholds in the range of previous max. and min. threshold where
		reference marks were found at last call to those methods. Otherwise it will search the 
		whole range of possible thresholds.
		@param NotAllMarksFast If true, only a small range of thresholds are used. See general
			description for this method for more information. */
	void SetNotAllMarksFast(bool NotAllMarksFast){m_NotAllMarksFast=NotAllMarksFast;}

	/** If parameter is true, the algorithms will add reference marks with Id's 
		not found in m_PointSets, in m_NewPointSets. The user decides what to
		do with such new points. He can for instance call AddNewRefMarksToExisting().
		@param FindNewRefMarks If true, new reference marks are added to 
			m_NewPointSets with 2D and Id set.
		@see AddNewRefMarksToExisting() */
	void SetNewRefMarks(bool FindNewRefMarks){m_FindNewRefMarks=FindNewRefMarks;}

	/** Adds new reference marks available in m_NewPointSets to existing ones in 
		m_PointSets. Also copies the corresponding mark code start centers from
		m_NewCSCenters to m_CSCenters. Hence, the method GetCodeStartCenters()
		will return all found reference marks including the new ones and they
		will correspond to the availabe point sets in m_PointSets. Only Id and
		the 2D point is set, the 3D value is left unused.
		@return Number of new Id's found. */
	unsigned int AddNewRefMarksToExisting();

	/** Returns positions of the Code Start circle of each found reference mark. Indexes in the returned
		array corresponds to indexes in m_PointSets. These positions can be used to derive the
		direction for each found reference mark. If not all reference marks are found, this method
		still returns the positions of the Code Start circles for the ones found. In this case, the 
		other indexes contains no valid information.
		@param CSCenters 2D positions of the Code Start circles, the order corresponds to indexes in m_PointSets.
		@return False, if no reference marks have been found, i.e. a previous call to either
			FindMarksAdaptive() or InitAdaptiveReturn()/FindMarksAdaptiveReturn() must have succeeded. */
	bool GetCodeStartCenters(vector<CPoint2D<double> >& CSCenters);
	/**@name streaming operators */
	//@{
	/** writes all the information contained in CMarkRetrieval to stream */
		friend ostream& operator<<(ostream& s,const CMarkRetrieval& Calib);
	//@}

	private: // attributes
	/** Either reads from threshold file or finds thresholds based on current image.
		Sets the members m_ThresholdStep, m_ThresholdLowerBound, and m_ThresholdUpperBound.
		@param BeginThreshold Threshold to begin with.
		@param True, always (so far). */
	bool InitializeThreshold(const string& ThresholdFile, const CStdImage& Img, 
								unsigned short ThresholdStep, unsigned short& BeginThreshold);

	/* Read a file containing threshold values, the mean value and standard
	   deviation is returned in Mean and StdDev.
	   @return False, if ThresholdFile does not exist. */
	bool ReadThresholdFile(string ThresholdFile, double& Mean, double& StdDev, 
							double& Min, double& Max);
	/* Writes a file containing threshold values.
	   @return False, if ThresholdFile does not exist. */
	bool WriteThresholdFile(const string& ThresholdFile, const vector<unsigned short>& Thresholds);
	/** Allocates directions for m_pDirs. m_PointSets must be initialized in order to know how
		many directions to be allocated. Deallocates previously allocated entries. */
	bool AllocateDirections();

	unsigned short FindCalibrationMarksAndMatchIds(CImage& TempImg, /*CCorresponding3D2DPoints& PointSets, */
				unsigned int Threshold, CStdImage& ImgPlot, unsigned int MinBlobSize, 
				bool UseSimpleCenter, unsigned int RefMarkType);

	unsigned short FindCalibrationMarksAndMatchIds(CRefMark& RefMark, /*CCorresponding3D2DPoints& PointSets, */
				CStdImage& ImgPlot);
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

} // end namespace videndo

#endif //_VIDENDO_MARK_RETRIEVAL_H