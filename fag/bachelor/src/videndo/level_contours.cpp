#include "level_contours.h"
#include <algorithm>
#include <string>
#include <set>
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/palette.h>
#include <ipl98/cpp/geometry/line_segment2d.h>


namespace videndo{ // use namespace if C++
	
using ipl::IPL_WARNING;
using ipl::CImage;
using ipl::CPalette;
using ipl::CMaskOperation;
using ipl::CLineSegment2D;
using std::string;
using std::ifstream;
using std::ofstream;
using std::endl;
using std::set;

CLevelContours::CLevelContours(unsigned int TotalBlur)
{
	m_StartStopRatio=30;
	m_MinStartGradient=25;
	SetTotalBlur(TotalBlur);
	m_MinContourSize=10;
	m_IsContourSearchInitialized=false;
	m_IsFixPointSearchInitialized=false;
	m_MaxStraightnessDevation=2;
	m_MinDistGhostRemoval=3;
	m_MinStopGradient=2;
}

ostream& operator<<(ostream& s,const CLevelContours::CPoint2DContour& P2DContour)
{
	s << "CPoint2DContour " << *dynamic_cast<const CPoint2D<float>* >(&P2DContour)
		<< " Gradient: "       << P2DContour.GetGradient() 
		<< " Color: "          << P2DContour.GetColor()
		<< " ArcLength: "      << P2DContour.GetArcLength()
		<< " Index: "          << P2DContour.GetIndex()
		<< " Curvature: "      << P2DContour.GetCurvature()
		<< " DirectionAngle: " << P2DContour.GetDirectionAngle()
		<< " PixelPos: " << P2DContour.m_PixelPos;
	return s;
}

istream& operator>>(istream& is, CLevelContours::CPoint2DContour& P2DContour)
{
	string Token;
	ipl::SkipSpaceAndComments(is);
	// read 'CPoint2DContour' token
	is >> Token;
	if (Token!=string("CPoint2DContour"))
	{
		ostringstream ost;
		ost << "operator>>(istream, CPoint2DContour) Token CPoint2DContour not found"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return is;
	}
	is >> *dynamic_cast<CPoint2D<float>* >(&P2DContour);
	ipl::SkipSpaceAndComments(is);
	// skip 'Gradient:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> P2DContour.m_Gradient;
	ipl::SkipSpaceAndComments(is);
	// skip 'Color:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> P2DContour.m_Color;
	ipl::SkipSpaceAndComments(is);
	// skip 'ArcLength:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> P2DContour.m_ds;
	ipl::SkipSpaceAndComments(is);
	// skip 'Index:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> P2DContour.m_Index;
	ipl::SkipSpaceAndComments(is);
	// skip 'Curvature:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> P2DContour.m_Curvature;
	ipl::SkipSpaceAndComments(is);
	// skip 'DirectionAngle:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> P2DContour.m_DirectionAngle;
	// skip 'PixelPos:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> P2DContour.m_PixelPos;
	return is;			
}


bool CLevelContours::DeriveContour(CByteImage &Source,const int x0,const int y0,
	int StopGradient)
{
	if (StopGradient<m_MinStopGradient)
		StopGradient=m_MinStopGradient;
	int DirIncrement=1;
	if ((int)EAST==2)
	{
		DirIncrement=2;
	}
	int FullTurn=DirIncrement*4;
	int xLL,yLL,xL,yL,xUL,yUL,xU,yU,xUR,yUR,xD,yD,xRR,yRR,xR,yR;
	DIRECTION IntDir;
	int x,y,i;
	float x1,y1,x2,y2;
	float DSq,maxD=2,minD=0.25;
	float maxDSq=maxD*maxD;
	float minDSq=minD*minD;
	CPoint2DContour PM,P0M;
	if (m_BinaryImg.GetPixel(x0,y0)==1)
	{
		ostringstream ost;
		ost << "CLevelContours::DeriveContour() No contour to start at - pixel at ("
			<< x0 << "," << y0 << ") not 1" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	x=x0;
	y=y0;
	i=0;
	CPixelGroup IntContourThick(100); // changed to be a local variable by edr@mip.sdu.dk
	//	IntContourThick.Empty();
	m_Contour.Empty();
	CContour ContourOpposite; // changed to be a local variable by edr@mip.sdu.dk
	//	m_ContourOpposite.Empty();
	CPoint2DContour P,P1;
	ipl::DIRECTION Dir, StartDir;
	int g0,ga,gb,gc;
	ga=-1;
	float Threshold=Source.m_ppPixel[y][x]-0.5;
	Source.SetBorder(3,Threshold-2);
	g0=Source.m_ppPixel[y][x];
	if (Source.m_ppPixel[y][x-1]<=Threshold)
	{
		ga=Source.m_ppPixel[y][x-1];
		Dir=NORTH;
	} 
	else if (Source.m_ppPixel[y-1][x]<=Threshold)
	{
		ga=Source.m_ppPixel[y-1][x];
		Dir=EAST;
	}
	else if (Source.m_ppPixel[y][x+1]<=Threshold)
	{
		ga=Source.m_ppPixel[y][x+1];
		Dir=SOUTH;
	}
	else if (Source.m_ppPixel[y+1][x]<=Threshold)
	{
		ga=Source.m_ppPixel[y+1][x];
		Dir=WEST;
	}
	if (ga==-1)
		return false;
	StartDir=Dir;
	int MaxDiff;
	bool ContourClosed;
	bool EdgeReached;
	bool TooLowGradient;
	bool Collision;
	bool First=true;
	do
	{
		IntContourThick.AddPositionFast(x,y);
		P.m_PixelPos.Set(x,y);
		switch (Dir)
		{
		case NORTH:
			{
				xLL=x-2;yLL=y;xL=x-1;yL=y;xUL=x-1;yUL=y-1;
				xU=x;yU=y-1;xUR=x+1;yUR=y-1;xD=x;yD=y+1;
			}
			break;
			
		case EAST:
			{
				xLL=x;yLL=y-2;xL=x;yL=y-1;xUL=x+1;yUL=y-1;
				xU=x+1;yU=y;xUR=x+1;yUR=y+1;xD=x-1;yD=y;
			}
			break;
			
		case SOUTH:
			{
				xLL=x+2;yLL=y;xL=x+1;yL=y;xUL=x+1;yUL=y+1;
				xU=x;yU=y+1;xUR=x-1;yUR=y+1;xD=x;yD=y-1;
			}
			break;
		case WEST:
			{
				xLL=x;yLL=y+2;xL=x;yL=y+1;xUL=x-1;yUL=y+1;
				xU=x-1;yU=y;xUR=x-1;yUR=y-1;xD=x+1;yD=y;
			}
		}//end SWITCH
		gb=Source.m_ppPixel[yU][xU];
		IntContourThick.AddPositionFast(xL,yL);
		IntContourThick.AddPositionFast(xLL,yLL);
		IntContourThick.AddPositionFast(xD,yD);
		if (gb<=Threshold)
		{
			if (ga<gb)
			{
				if (Dir==0)
					IntDir=WEST; 
				else
					IntDir=(DIRECTION)((int)Dir-DirIncrement);
				DeriveSubpixelPos(x1,y1,IntDir,x,y,(float)(g0-Threshold)/(g0-ga));
				MaxDiff=g0-ga;
				P.SetGradient(MaxDiff);
			}
			else
			{
				DeriveSubpixelPos(x1,y1,Dir,x,y,(float)(g0-Threshold)/(g0-gb));
				MaxDiff=g0-gb;
				P.SetGradient(MaxDiff);
			}
			Dir=(DIRECTION)(((int)Dir+DirIncrement)%FullTurn);
			ga=gb;
			IntContourThick.AddPositionFast(xU,yU);
			IntContourThick.AddPositionFast(xUR,yUR);
		} 
		else
		{
			gc=Source.m_ppPixel[yD][xD];
			if (ga<=gc)
			{
				if (Dir==0)
					IntDir=WEST;
				else 
					IntDir=(DIRECTION)((int)Dir-DirIncrement);
				DeriveSubpixelPos(x1,y1,IntDir,x,y,(float)(g0-Threshold)/(g0-ga));
				MaxDiff=g0-ga;
				P.SetGradient(MaxDiff);
			}
			else
			{
				IntDir=(DIRECTION)(((int)Dir+2*DirIncrement)%FullTurn);
				DeriveSubpixelPos(x1,y1,IntDir,x,y,(float)(g0-Threshold)/(g0-gc));
				MaxDiff=g0-gc;
				P.SetGradient(MaxDiff);
			}
			gc=Source.m_ppPixel[yUL][xUL];
			if (gc<=Threshold)
			{
				x=xU;
				y=yU;
				g0=gb;
				ga=gc;
				IntContourThick.AddPositionFast(xUL,yUL);
			}
			else
			{
				if (Dir==0)
					Dir=WEST;
				else
					Dir=(DIRECTION)((int)Dir-DirIncrement);
				x=xUL;
				y=yUL;
				g0=gc;
			}
		}
		P.Set(x1,y1);
		if (First)
		{
			m_Contour.AddPositionFast(P);
			P0M=P;
			x2=x1;
			y2=y1;
			First=false;
		} 
		else
		{
			DSq=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
			if (DSq>maxDSq)
			{
				if(DeriveInsertedPoint(Source,PM,P0M,Threshold))
					m_Contour.AddPositionFast(PM);
			}
			if (DSq<minDSq)
			{
				m_Contour.RemovePositionFast(m_Contour.GetTotalPositions()-1);
				//m_Contour.RemovePosition(m_Contour.GetTotalPositions()-1);
			}
			m_Contour.AddPositionFast(P);
			P0M=P;
			x2=x1;
			y2=y1;
		}
		ContourClosed=((x==x0)&&(y==y0)&&(Dir==StartDir));
		Collision=(m_BinaryImg.m_ppPixel[y][x]>0);
		EdgeReached = (((x==m_xl)/*&&(Dir==WEST)*/)||
			((y==m_yb-1)/*&&(Dir==SOUTH)*/)||
			((x==m_xr-1)/*&&(Dir==EAST)*/)||
			((y==m_yt)/*&&(Dir==NORTH)*/));
		TooLowGradient=(MaxDiff<StopGradient);
    }
    while(!(ContourClosed||EdgeReached||TooLowGradient||Collision));/* end do */
    if (ContourClosed)
    {
		m_Contour.SetClosed(true);
		m_Contour.SetLowIndexType(CContour::JOINED);
		m_Contour.SetHighIndexType(CContour::JOINED);
    }
    else
    {
		if (Collision)
			m_Contour.SetHighIndexType(CContour::COLLISION);
		else if (EdgeReached)
			m_Contour.SetHighIndexType(CContour::BORDER);
		else // TooLowGradient
			m_Contour.SetHighIndexType(CContour::LOWGRAD);
		m_Contour.SetClosed(false);
		ContourOpposite.Empty();
		x=x0;
		y=y0;
		i=0;
		First=true;
		if (Source.m_ppPixel[y][x+1]<=Threshold)
		{
			ga=Source.m_ppPixel[y][x+1];
			Dir=NORTH;
		}
		else if (Source.m_ppPixel[y+1][x]<=Threshold)
		{
			ga=Source.m_ppPixel[y+1][x];
			Dir=EAST;
		}
		else if (Source.m_ppPixel[y][x-1]<=Threshold)
		{
			ga=Source.m_ppPixel[y][x-1];
			Dir=SOUTH;
		}
		else if (Source.m_ppPixel[y-1][x]<=Threshold)
		{
			ga=Source.m_ppPixel[y-1][x];
			Dir=WEST;
		}
		StartDir=Dir;
		g0=Source.m_ppPixel[y][x];
		do
		{
			P.m_PixelPos.Set(x,y);
			IntContourThick.AddPositionFast(x,y);
			switch (Dir)
			{
			case NORTH:
				{
					xRR=x+2;yRR=y;xR=x+1;yR=y;xUL=x-1;yUL=y-1;
					xU=x;yU=y-1;xUR=x+1;yUR=y-1;xD=x;yD=y+1;
				}
				break;
				
			case EAST:
				{
					xRR=x;yRR=y+2;xR=x;yR=y+1;xUL=x+1;yUL=y-1;
					xU=x+1;yU=y;xUR=x+1;yUR=y+1;xD=x-1;yD=y;
				}
				break;
			case SOUTH:
				{
					xRR=x-2;yRR=y;xR=x-1;yR=y;xUL=x+1;yUL=y+1;
					xU=x;yU=y+1;xUR=x-1;yUR=y+1;xD=x;yD=y-1;
				}
				break;
			case WEST:
				{
					xRR=x;yRR=y-2;xR=x;yR=y-1;xUL=x-1;yUL=y+1;
					xU=x-1;yU=y;xUR=x-1;yUR=y-1;xD=x-1;yD=y;
				}
			} //end switch
			gb=Source.m_ppPixel[yU][xU];
			IntContourThick.AddPositionFast(xL,yL);
			IntContourThick.AddPositionFast(xLL,yLL);
			IntContourThick.AddPositionFast(xD,yD);
			if (gb<=Threshold)
            {
				if (ga<gb)
				{
					IntDir=(DIRECTION)(((int)Dir+DirIncrement)%FullTurn);
					DeriveSubpixelPos(x1,y1,IntDir,x,y,(float)(g0-Threshold)/(g0-ga));
					MaxDiff=g0-ga;
					P.SetGradient(MaxDiff);
				}
				else
				{
					DeriveSubpixelPos(x1,y1,Dir,x,y,(float)(g0-Threshold)/(g0-gb));
					MaxDiff=g0-gb;
					P.SetGradient(MaxDiff);
				}
				if (Dir==0)
					Dir=WEST;
				else
					Dir=(DIRECTION)((int)Dir-DirIncrement);
				ga=gb;
				IntContourThick.AddPositionFast(xU,yU);
				IntContourThick.AddPositionFast(xUL,yUL);
            }
			else
            {
				gc=Source.m_ppPixel[yD][xD];
				if (ga<=gc)
                {
					IntDir=(DIRECTION)(((int)Dir+DirIncrement)%FullTurn);
					DeriveSubpixelPos(x1,y1,IntDir,x,y,(float)(g0-Threshold)/(g0-ga));
					MaxDiff=g0-ga;
					P.SetGradient(MaxDiff);
                }
                else
                {
					IntDir=(DIRECTION)(((int)Dir+2*DirIncrement)%FullTurn);
					DeriveSubpixelPos(x1,y1,IntDir,x,y,(float)(g0-Threshold)/(g0-gc));
					MaxDiff=g0-gc;
					P.SetGradient(MaxDiff);
                }
				gc=Source.m_ppPixel[yUR][xUR];
				if (gc<=Threshold)
				{
					x=xU;y=yU;g0=gb;ga=gc;
					IntContourThick.AddPositionFast(xUR,yUR);
				}
				else
				{
					Dir=(DIRECTION)(((int)Dir+DirIncrement)%FullTurn);
					x=xUR;y=yUR;g0=gc;
				}
            }
			P.Set(x1,y1);
			if (First)
			{
				ContourOpposite.AddPositionFast(P);
				P0M=P;
				x2=x1;
				y2=y1;
				First=false;
			} 
			else
			{
				DSq=(x1-x2)*(x1-x2)+(y1-y2)*(y1-y2);
				if (DSq>maxDSq)
				{
					if(DeriveInsertedPoint(Source,PM,P0M,Threshold))
						ContourOpposite.AddPositionFast(PM);
				}
				if (DSq<minDSq)
				{
					ContourOpposite.RemovePositionFast(ContourOpposite.GetTotalPositions()-1);
				}
				ContourOpposite.AddPositionFast(P);
				P0M=P;
				x2=x1;
				y2=y1;
			}
			Collision=(m_BinaryImg.m_ppPixel[y][x]>0||(x==x0&&y==y0&&Dir==StartDir));
			//      Collision=(m_BinaryImg.m_ppPixel[y][x]>0);
			EdgeReached = (((x==m_xl)) || ((y==m_yb-1)) || ((x==m_xr-1)) || ((y==m_yt)));
			TooLowGradient=(MaxDiff<StopGradient);
    }
    while(!(EdgeReached||TooLowGradient||Collision));/* end do */
	if (Collision)
		m_Contour.SetLowIndexType(CContour::COLLISION);
	else if (EdgeReached)
		m_Contour.SetLowIndexType(CContour::BORDER);
	else if (TooLowGradient)
		m_Contour.SetLowIndexType(CContour::LOWGRAD);
    if (ContourOpposite.GetTotalPositions()>1)
    {
		for (int i=1;i<ContourOpposite.GetTotalPositions();i++)
		{
			if (m_Contour.GetTotalPositions()!=0)
				m_Contour.InsertPosition(ContourOpposite[i],0);
			else
				m_Contour.AddPositionFast(ContourOpposite[i]);
		}
    }
	
  }
  if (m_Contour.GetTotalPositions()<m_MinContourSize) return false;
  m_Contour.SetColor(Threshold);
  IntContourThick.UpdateBoundaries();
  IntContourThick.AddToImage(m_BinaryImg,255);
  P=m_Contour[0];
  P.SetIndex(0);
  i=1;
  while (i<m_Contour.GetTotalPositions())
  {
	  P1=m_Contour[i];
	  if (P.GetX()==P1.GetX()&&P.GetY()==P1.GetY())
	  {
		  m_Contour.RemovePositionSlow(i);
	  }
	  else
	  {
		  m_Contour[i].SetIndex(i);
		  i++;
		  P=P1;
	  }
  }
  return true;
}

bool CLevelContours::DeriveInsertedPoint(CByteImage &Src, CPoint2DContour &P, CPoint2DContour &P0, float Threshold)
{
	int x0,y0,g0,g1,grad;
	P=P0;
	x0=P0.m_PixelPos.GetX();
	y0=P0.m_PixelPos.GetY();
	float xf,yf;
	xf=P0.GetX();
	yf=P0.GetY();
	g0=Src.m_ppPixel[y0][x0];
	if (g0<Threshold) return false;
	if (abs((int)(x0-xf))<abs((int)(y0-yf)))
	{
		g1=Src.m_ppPixel[y0][x0+1];
		if (g1<=Threshold)
		{
			grad=g0-g1;
			P.SetGradient(grad);
			P.Set(x0+(g0-Threshold)/(float)grad,y0);
			return true;
		}
		g1=Src.m_ppPixel[y0][x0-1];
		if (g1<=Threshold)
		{
			grad=g0-g1;
			P.SetGradient(grad);  
			P.Set(x0-(g0-Threshold)/(float)grad,y0);
			return true;
		}
	} else
	{ 
		g1=Src.m_ppPixel[y0+1][x0];
		if (g1<=Threshold)
		{
			grad=g0-g1;
			P.SetGradient(grad);  
			P.Set(x0,y0+(g0-Threshold)/(float)grad);
			return true; 
		} 
		g1=Src.m_ppPixel[y0-1][x0];
		if (g1<=Threshold)
		{ 
			grad=g0-g1;
			P.SetGradient(grad);
			P.Set(x0,y0-(g0-Threshold)/(float)grad);
			return true;
		}
	}
	return false;
}

void CLevelContours::SmoothCurvature(CContour &Contour, unsigned int HalfWidth)
{
	if (HalfWidth==0) return;
	CContour Buf;
	Buf=Contour;
	CPoint2DContour P;
	float sum1,sum2,ds;
	int j,k,jtot=Buf.GetTotalPositions();
	if (Contour.GetClosed())
	{
		for (j=0;j<jtot;j++)
		{
			sum1=0;
			sum2=0;
			for (k=-(int)HalfWidth;k<=(int)HalfWidth;k++)
			{
				if (k+j<0)
					P=Buf[k+j+jtot];
				else if
					(k+j>=jtot)P=Buf[k+j-jtot];
				else
					P=Buf[k+j];
				ds=P.GetArcLength();
				//				if (fabs(k)==HalfWidth) ds*=0.5;
				sum1+=P.GetCurvature()*ds;
				sum2+=ds;
			}
			Contour[j].SetCurvature(sum1/sum2);
		}
	}
	else
	{
		for (j=0;j<jtot;j++)
		{
			sum1=0;
			sum2=0;
			for (k=-(int)HalfWidth;k<=(int)HalfWidth;k++)
			{
				if (k+j<0)
					P=Buf[-j-k];
				else if (k+j>jtot-1)
					P=Buf[2*jtot-1-j-k];
				else 
					P=Buf[k+j];
				ds=P.GetArcLength();
				sum1+=P.GetCurvature()*ds;
				sum2+=ds;
			}
			Contour[j].SetCurvature(sum1/sum2);
		}
	}
}

bool CLevelContours::SmoothCurvature(CContour &Contour)
{
	if (IsFixPointSearchInitialized()==false)
	{
		ostringstream ost;
		ost << "CLevelContours::SmoothCurvature() Class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	SmoothCurvature(Contour, m_HalfWidth);
	return true;
}

void CLevelContours::DeriveCurvature(vector<CContour> &Contours)
{
	int jtot,i,j;
	float z,z1;
	CPoint2DContour P;
	CContour Buf;
	float ds,xm,xp,ym,yp,x,y,xpm,ypm,xmm,ymm,dx,dy,dTheta;
	for (i=0;i<Contours.size();i++)
	{
		jtot=Contours[i].GetTotalPositions();
		if (Contours[i].GetClosed())
		{ 
			if ((Contours[i][0].GetX()==Contours[i][jtot-1].GetX()) &&
				(Contours[i][0].GetY()==Contours[i][jtot-1].GetY()))
				Contours[i].RemovePosition(jtot-1);
		}
		else
		{
			if (Contours[i][0].GetX()==Contours[i][1].GetX()
				&&Contours[i][0].GetY()==Contours[i][1].GetY())
				Contours[i].RemovePositionSlow(0);
		}
	}
	for (i=0;i<Contours.size();i++)
	{
		jtot=Contours[i].GetTotalPositions();
		
		Buf=Contours[i];
		x=Buf[0].GetX();
		y=Buf[0].GetY();
		if (Contours[i].GetClosed())
		{
			xm=Buf[jtot-1].GetX();
			ym=Buf[jtot-1].GetY();
			for (j=0;j<jtot;j++)
			{
				if (j<jtot-1)
				{
					xp=Buf[j+1].GetX();
					yp=Buf[j+1].GetY();
				}
				else
				{
					xp=Buf[0].GetX();
					yp=Buf[0].GetY();
				}
				xmm=(x+xm)/2.0;
				ymm=(y+ym)/2.0;
				xpm=(x+xp)/2.0;
				ypm=(y+yp)/2.0;
				dx=xpm-xmm;
				dy=ypm-ymm;
				ds=sqrt(dx*dx+dy*dy);
				if (ds!=0)
				{
					Contours[i][j].SetArcLength(ds);
					Contours[i][j].SetDirectionAngle(atan2(dy,dx));
					if (((yp==y)&&(xp==x))||((ym==y)&&(xm==x)))
						dTheta=0;
					else
						dTheta=atan2(yp-y,xp-x)-atan2(y-ym,x-xm);
					z=atan2(yp-y,xp-x);
					z1=atan2(y-ym,x-xm);
					if (dTheta>ipl::PI)
						dTheta-=2*ipl::PI;
					if (dTheta<-ipl::PI)
						dTheta+=2*ipl::PI;
					Contours[i][j].SetCurvature(dTheta/ds);
				}
				xm=x;
				ym=y;
				x=xp;
				y=yp;
			}
		}
		else
		{
			xp=Buf[1].GetX();
			yp=Contours[i][1].GetY();
			for (j=1;j<jtot-1;j++)
			{
				xm=x;
				ym=y;
				x=xp;
				y=yp;
				xp=Buf[j+1].GetX();
				yp=Buf[j+1].GetY();
				if (abs((int)(xp-xm))+abs((int)(yp-ym))>0.001)
				{
					xmm=(x+xm)/2.0;
					ymm=(y+ym)/2.0;
					xpm=(x+xp)/2.0;
					ypm=(y+yp)/2.0;
					dx=xpm-xmm;
					dy=ypm-ymm;
					ds=sqrt(dx*dx+dy*dy);
					Contours[i][j].SetArcLength(ds);
					Contours[i][j].SetDirectionAngle(atan2(dy,dx));
					dTheta=atan2(yp-y,xp-x)-atan2(y-ym,x-xm);
					if (dTheta>ipl::PI)
						dTheta-=2*ipl::PI;
					if (dTheta<-ipl::PI)
						dTheta+=2*ipl::PI;
					Contours[i][j].SetCurvature(dTheta/ds);
				}
			}
			Contours[i][0].SetArcLength(Contours[i][1].GetArcLength());
			Contours[i][0].SetDirectionAngle(Contours[i][1].GetDirectionAngle());
			Contours[i][0].SetCurvature(Contours[i][1].GetCurvature());
			Contours[i][jtot-1].SetArcLength(Contours[i][jtot-2].GetArcLength());
			Contours[i][jtot-1].SetDirectionAngle(Contours[i][jtot-2].GetDirectionAngle());
			Contours[i][jtot-1].SetCurvature(Contours[i][jtot-2].GetCurvature());
		}
	}
}
void CLevelContours::BlurAndEdgeDetect(CByteImage &Source, int MinGradient)
{
	int EffectiveMinGradient = 2*m_MinStartGradient;
	m_MaxGradient.clear();
	m_BinaryImg.Alloc(Source.GetWidth(),Source.GetHeight(),8,0); // changed by edr@mip.sdu.dk
	int count=m_TotalBlur;
	while(count>0)
	{
		CMaskOperation::BlurEightConnected3x3(Source);
		count--;
	}
	m_Edge.Alloc(Source.GetWidth(),Source.GetHeight(),8,0); // changed by edr@mip.sdu.dk
	ipl::UINT8 *p0,*pm,*pp,*pemm,*pem,*pe0;
	int d,d0,d3;
	int x,y;
	CPoint2DEdge P;
	for (y=m_yt+1;y<m_yt+3;y++)
	{
		x=m_xl+1;
		p0=Source.m_ppPixel[y]+x;
		pm=Source.m_ppPixel[y-1]+x;
		pp=Source.m_ppPixel[y+1]+x;
		pe0=m_Edge.m_ppPixel[y]+x;
		while (x<m_xr-1)
		{
			d=abs((int)(*(p0-1)-*(p0+1)))+abs((int)((*pp++)-(*pm++)));
			*pe0++=d;
			p0++;
			x++;
		}
	}
	for (y=m_yt+3;y<m_yb-1;y++)
	{
		x=m_xl+1;
		p0=Source.m_ppPixel[y]+x;
		pm=Source.m_ppPixel[y-1]+x;
		pp=Source.m_ppPixel[y+1]+x;
		pem=m_Edge.m_ppPixel[y-1]+x;
		pemm=m_Edge.m_ppPixel[y-2]+x;
		pe0=m_Edge.m_ppPixel[y]+x;
		while (x<m_xr-1)
		{
			d0=abs((int)(*(p0-1)-*(p0+1)));
			d3=abs((int)(*(pm++)-*(pp++)));
			d=(d0>d3)?d0:d3; //max(d0,d3);
			//     d1=(3*fabs(*(pm-1)-*(pp+1)))>>1;   //more elaborate
			//     d2=(3*fabs(*(pm+1)-*(pp-1)))>>1;   //edge detection
			//     d=max(d0,max(d1,max(d2,d3)));
			*pe0++=(ipl::UINT8)d;
			if (*pem>EffectiveMinGradient)
			{
				if ( ((*pem>=d)&&(*pem>=*pemm)&&(*pem>=*(pem-1))&&(*pem>*(pem+1))) ||
					((*pem>=d)&&(*pem>*pemm)&&(*pem>=*(pem-1))&&(*pem>=*(pem+1))) )
				{
					P.Set(x,y-1);
					P.m_Gradient=*pem;
					m_MaxGradient.push_back(P);
				}
			}
			pem++;
			pemm++;
			p0++;
			x++;
		}
	}
}

bool CLevelContours::TreatImage(CByteImage &Source,
								vector<CContour> &Contours)
{
	if (Source.IsEmpty()==true) 
	{
		ostringstream ost;
		ost << "CLevelContours::TreatImage() Source image is empty"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_xl=0;
	m_xr=Source.GetWidth();
	m_yt=0;
	m_yb=Source.GetHeight();
	Contours.clear();
	BlurAndEdgeDetect(Source,m_MinStartGradient);
	std::sort(m_MaxGradient.begin(),m_MaxGradient.end());
	int StartGradient,x,y;
	if (m_MaxGradient.size()==0)
	{
		ostringstream ost;
		ost << "CLevelContours::TreatImage() No contours found"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return true;
	}
	int MaxStartGradient=m_MaxGradient[0].m_Gradient;
	for (int i=0;i<m_MaxGradient.size();i++)
	{
		StartGradient=m_MaxGradient[i].m_Gradient;
		x=m_MaxGradient[i].GetX();
		y=m_MaxGradient[i].GetY();
		if (m_BinaryImg.GetPixel(x,y)==0)
		{
			if (DeriveContour(Source,x,y,StartGradient/m_StartStopRatio))
			{
				m_Contour.SetSeedPoint(m_MaxGradient[i]); // added by edr@mip.sdu.dk v. 0.53
				Contours.push_back(m_Contour);
			}
		}
	}
	return true;
}


bool CLevelContours::TreatImage(CByteImage &Source, CPoint2D<int> &UL,CPoint2D<int> &LR,
								vector<CContour> &Contours)
{
	if (Source.IsEmpty()==true) 
	{
		ostringstream ost;
		ost << "CLevelContours::TreatImage() Source image is empty"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	m_xl=UL.GetX();
	m_xr=LR.GetX();
	m_yt=UL.GetY();
	m_yb=LR.GetY();
	Contours.clear();
	BlurAndEdgeDetect(Source,m_MinStartGradient);
	std::sort(m_MaxGradient.begin(),m_MaxGradient.end());
	int StartGradient,x,y;
	int w=m_MaxGradient.size();
	w=0;
	int MaxStartGradient=m_MaxGradient[0].m_Gradient;
	for (int i=0;i<m_MaxGradient.size();i++)
	{
		StartGradient=m_MaxGradient[i].m_Gradient;
		x=m_MaxGradient[i].GetX();
		y=m_MaxGradient[i].GetY();
		if (m_BinaryImg.GetPixel(x,y)==0)
		{
			if (DeriveContour(Source,x,y,StartGradient/m_StartStopRatio))
			{
				m_Contour.SetSeedPoint(m_MaxGradient[i]);
				Contours.push_back(m_Contour);
			}
		}
	}
	return true;
}

bool CLevelContours::WriteSettings(ostream& os) const
{
	os << "CLevelContours-Settings" << endl;
	os << "    ContourSearchInitialized: " << IsContourSearchInitialized() << endl;
	if (IsContourSearchInitialized()==true)
	{
		os << "        TotalBlur:        " << m_TotalBlur << endl
			<< "        MinStartGradient: " << m_MinStartGradient << endl
			<< "        MinContourSize:   " << m_MinContourSize << endl
			<< "        StartStopRatio:   " << m_StartStopRatio << endl
			<< "        MinStopGradient:  " << m_MinStopGradient << endl;
	}
	os << "    FixPointSearchInitialized: " << IsFixPointSearchInitialized() << endl;
	if (IsContourSearchInitialized()==true)
	{
		os << "        HalfWidth:               " << m_HalfWidth << endl
			<< "        CurvatureLimitEx:        " << m_CurvatureLimitEx << endl
			<< "        CurvatureLimitLin:       " << m_CurvatureLimitLin << endl
			<< "        MaxStraightnessDevation: " << m_MaxStraightnessDevation << endl
			<< "        MinDistGhostRemoval:     " << m_MinDistGhostRemoval << endl;
	}
	return true;
}

bool CLevelContours::ReadSettings(istream& is)
{
	string Token;
	ipl::SkipSpaceAndComments(is);
	// read 'CLevelContours-Settings' token
	is >> Token;
	if (Token!=string("CLevelContours-Settings"))
	{
		ostringstream ost;
		ost << "CLevelContours::ReadSettings() Token CLevelContours-Settings "
			"not found" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	ipl::SkipSpaceAndComments(is);
	// skip 'ContourSearchInitialized:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> m_IsContourSearchInitialized;
	if (m_IsContourSearchInitialized==true)
	{
		// skip 'TotalBlur:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_TotalBlur;
		ipl::SkipSpaceAndComments(is);
		// skip 'MinStartGradient:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_MinStartGradient;
		ipl::SkipSpaceAndComments(is);
		// skip 'MinContourSize:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_MinContourSize;
		ipl::SkipSpaceAndComments(is);
		// skip 'StartStopRatio:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_StartStopRatio;
		ipl::SkipSpaceAndComments(is);
		// skip 'MinStopGradient:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_MinStopGradient;
		ipl::SkipSpaceAndComments(is);
	}
	
	// skip 'FixPointSearchInitialized:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	ipl::SkipSpaceAndComments(is);
	is >> m_IsFixPointSearchInitialized;
	if (m_IsFixPointSearchInitialized==true)
	{
		// skip 'HalfWidth:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_HalfWidth;
		ipl::SkipSpaceAndComments(is);
		// skip 'CurvatureLimitEx:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_CurvatureLimitEx;
		ipl::SkipSpaceAndComments(is);
		// skip 'CurvatureLimitLin:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_CurvatureLimitLin;
		ipl::SkipSpaceAndComments(is);
		// skip 'MaxStraightnessDevation:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_MaxStraightnessDevation;
		ipl::SkipSpaceAndComments(is);
		// skip 'MinDistGhostRemoval:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> m_MinDistGhostRemoval;
		ipl::SkipSpaceAndComments(is);
	}
	return true;			
}

bool CLevelContours::InitializeContourSearch(unsigned int TotalBlur, int MinStartGradient, 
											 int MinContourSize, float StartStopRatio, 
											 float MinStopGradient)
{
	if (MinStartGradient==0)
	{
		ostringstream ost;
		ost << "CLevelContours::InitializeContourSearch() MinStartGradient is zero" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (StartStopRatio==0)
	{
		ostringstream ost;
		ost << "CLevelContours::InitializeContourSearch() StartStopRatio is zero" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (MinContourSize==0)
	{
		ostringstream ost;
		ost << "CLevelContours::InitializeContourSearch() MinContourSize is zero" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (TotalBlur>=5)
	{
		ostringstream ost;
		ost << "CLevelContours::InitializeContourSearch() TotalBlur is >= 5" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (MinStopGradient<=0)
	{
		ostringstream ost;
		ost << "CLevelContours::InitializeContourSearch() MinStopGradient must be >= zero" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_MinStartGradient=MinStartGradient;
	m_StartStopRatio=StartStopRatio;
	m_MinContourSize=MinContourSize;
	m_TotalBlur=TotalBlur;
	m_MinStopGradient=MinStopGradient;
	m_IsContourSearchInitialized=true;
	return true;
}


bool CLevelContours::InitializeFixPointSearch(unsigned int HalfWidth, float CurvatureLimitEx,
											  float CurvatureLimitLin, float MaxStraightnessDevation,
											  float MinDistGhostRemoval)
{
	m_CurvatureLimitEx=CurvatureLimitEx;
	m_CurvatureLimitLin=CurvatureLimitLin;
	m_HalfWidth=HalfWidth;
	m_MaxStraightnessDevation=MaxStraightnessDevation;
	m_MinDistGhostRemoval=MinDistGhostRemoval;
	m_IsFixPointSearchInitialized=true;
	return true;
}

bool CLevelContours::DeriveFixpoints(CContour &SourceContour, vector<CPoint2DContour> &Extrema,
									 vector<CContour> &LinearSegments)
{
	if (IsFixPointSearchInitialized()==false)
	{
		ostringstream ost;
		ost << "CLevelContours::DeriveFixpoints() Fix points search not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	DeriveFixpoints(m_CurvatureLimitEx, m_CurvatureLimitLin,SourceContour,
		Extrema,LinearSegments);
	return true;
}

void CLevelContours::DeriveFixpoints(float CurvatureLimitEx, float CurvatureLimitLin,
									 CContour &SourceContour, vector<CPoint2DContour> &Extrema,
									 vector<CContour> &LinearSegments)
{
	Extrema.clear();
	LinearSegments.clear();
	CPoint2DContour PP,P0;
	CContour Buf, straightLineBuf;
	float CurvM,Curv0,CurvP;
	int i;
	int m=SourceContour.GetTotalPositions();
	bool StraightM,Straight0,StraightP;
	bool straightLineHalved;
	if (SourceContour.GetClosed())
	{
		CurvM=fabs(SourceContour[m-1].GetCurvature());
		StraightM=(CurvM<CurvatureLimitLin);
		P0=SourceContour[0];
		Curv0=fabs(P0.GetCurvature());
		Straight0=(Curv0<CurvatureLimitLin);
		straightLineHalved=StraightM&&Straight0;
		for (i=1;i<m+1;i++)
		{
			PP=SourceContour[i%m];
			CurvP=fabs(PP.GetCurvature());
			StraightP=(CurvP<CurvatureLimitLin);
			if(straightLineHalved) {
				if (Straight0) 
					straightLineBuf.AddPositionFast(P0);
				if (StraightM&&!Straight0)
				{
					straightLineHalved=false;
				}
				if (Curv0>CurvatureLimitEx)
				{
					if (CurvP<Curv0&&CurvM<=Curv0)
						Extrema.push_back(P0);
				}
			}
			else {
				if (Straight0) 
					Buf.AddPositionFast(P0);
				if (StraightM&&!Straight0)
				{
					// edr: In special cases we may insert an empty group, so at least it should be checked for !=0
					if (Buf.GetTotalPositions()>=m_MinContourSize)
					{
						//Buf.SetSeedPoint(SourceContour.GetSeedPoint()); // added by edr@mip.sdu.dk v. 0.53
						//FindMaxGradientForLineSeg(Buf); // added by edr@mip.sdu.dk v. 0.53
						AddToLineSegmentVector(LinearSegments,Buf);
						//LinearSegments.push_back(Buf);
					}
					Buf.Empty();
				}
				if (Curv0>CurvatureLimitEx)
				{
					if (CurvP<Curv0&&CurvM<=Curv0)
						Extrema.push_back(P0);
				}
			}
			StraightM=Straight0;
			CurvM=Curv0;
			Straight0=StraightP;
			Curv0=CurvP;
			P0=PP;
		}
		if(straightLineBuf.GetTotalPositions()!=0){
			for(i=0;i<straightLineBuf.GetTotalPositions();i++) // edr: this could be replaced by Buf=straightLineBuf
				Buf.AddPositionFast(straightLineBuf[i]);
		}
	} 
	else
	{
		CurvM=fabs(SourceContour[1].GetCurvature());
		StraightM=(CurvM<CurvatureLimitLin);
		P0=SourceContour[2];
		Curv0=fabs(P0.GetCurvature());
		Straight0=(Curv0<CurvatureLimitLin);
		for (i=3;i<m-1;i++)
		{
			PP=SourceContour[i];
			CurvP=fabs(PP.GetCurvature());
			StraightP=(CurvP<CurvatureLimitLin);
			if (Straight0)
				Buf.AddPositionFast(P0);
			if (StraightM&&!Straight0)
			{
				// edr: In special cases we may insert an empty group, so at least it should be checked for !=0
				if (Buf.GetTotalPositions()>=m_MinContourSize)
				{
					//Buf.SetSeedPoint(SourceContour.GetSeedPoint()); // added by edr@mip.sdu.dk v. 0.53
					//FindMaxGradientForLineSeg(Buf); // added by edr@mip.sdu.dk v. 0.53
					AddToLineSegmentVector(LinearSegments,Buf);
					//LinearSegments.push_back(Buf);
				}
				Buf.Empty();
			}
			if (Curv0>CurvatureLimitEx)
			{
				if (CurvP<Curv0&&CurvM<=Curv0)
					Extrema.push_back(P0);
			}
			StraightM=Straight0;
			CurvM=Curv0;
			Straight0=StraightP;
			Curv0=CurvP;
			P0=PP;
		}
	}
	// edr: In special cases we may insert an empty group, so at least it should be checked for !=0
	if (Buf.GetTotalPositions()>=m_MinContourSize)
	{
		//Buf.SetSeedPoint(SourceContour.GetSeedPoint()); // added by edr@mip.sdu.dk v. 0.53
		//FindMaxGradientForLineSeg(Buf); // added by edr@mip.sdu.dk v. 0.53
		AddToLineSegmentVector(LinearSegments,Buf);
		//LinearSegments.push_back(Buf);
	}
}
void CLevelContours::DeriveFixpointsExtended(float CurvatureLimitEx, float CurvatureLimitLin,
											 CContour &SourceContour,
											 vector<CPoint2DContour> &Maxima,
											 vector<CPoint2DContour> &Minima,
											 vector<CPoint2DContour> &InflectionPoints,
											 vector<CContour> &LinearSegments,
											 vector<CContour> &ArcSegments)
{
	Maxima.clear();
	Minima.clear();
	InflectionPoints.clear();
	LinearSegments.clear();
	ArcSegments.clear();
	CPoint2DContour PP,P0,PM;
	CContour BufLin,BufArc;
	float CurvM,Curv0,CurvP,SignedCurv0,SignedCurvP,SignedCurvM;
	int i;
	int m=SourceContour.GetTotalPositions();
	bool StraightM,Straight0,StraightP;
	bool P0InflectionPoint;
	if (SourceContour.GetClosed())
	{
		PM=SourceContour[m-1];
		SignedCurvM=PM.GetCurvature();
		CurvM=fabs(SignedCurvM);
		StraightM=(CurvM<CurvatureLimitLin);
		P0=SourceContour[0];
		SignedCurv0=P0.GetCurvature();
		Curv0=fabs(SignedCurv0);
		Straight0=(Curv0<CurvatureLimitLin);
		for (i=1;i<m+1;i++)
		{
			PP=SourceContour[i%m];
			SignedCurvP=PP.GetCurvature();
			CurvP=fabs(SignedCurvP);
			StraightP=(CurvP<CurvatureLimitLin);
			P0InflectionPoint=false;
			if (Straight0)
				BufLin.AddPositionFast(P0);
			else
				BufArc.AddPositionFast(P0);
			if ((SignedCurvM>0&&SignedCurv0<=0)||
                (SignedCurvM<=0&&SignedCurv0>0))
			{
				if (Curv0>CurvM) InflectionPoints.push_back(PM);
				else InflectionPoints.push_back(P0);
				P0InflectionPoint=true;
			}
			if (StraightM&&!Straight0)
			{
				// edr: In special cases we may insert an empty group, so at least it should be checked for !=0
				if (BufLin.GetTotalPositions()>0)
				{
					//	BufLin.SetSeedPoint(SourceContour.GetSeedPoint()); // added by edr@mip.sdu.dk v. 0.53
					//	FindMaxGradientForLineSeg(BufLin); // added by edr@mip.sdu.dk v. 0.53
					AddToLineSegmentVector(LinearSegments,BufLin);
					BufLin.Empty();
				}
				
			}
			if ((!StraightM&&Straight0)||P0InflectionPoint)
			{
				BufArc.SetClosed(false);
				ArcSegments.push_back(BufArc);
				BufArc.Empty();
			}
			if (Curv0>CurvatureLimitEx)
			{
				if (CurvP<Curv0&&CurvM<=Curv0)
					Maxima.push_back(P0);
				if (CurvP>Curv0&&CurvM>=Curv0)
					Minima.push_back(P0);
			}
			StraightM=Straight0;
			CurvM=Curv0;
			SignedCurvM=SignedCurv0;
			PM=P0;
			Straight0=StraightP;
			Curv0=CurvP;
			SignedCurv0=SignedCurvP;
			P0=PP;
			
		}
	}
	else
	{
		PM=SourceContour[1];
		SignedCurvM=PM.GetCurvature();
		CurvM=fabs(SignedCurvM);
		StraightM=(CurvM<CurvatureLimitLin);
		P0=SourceContour[2];
		SignedCurv0=P0.GetCurvature();
		Curv0=fabs(SignedCurv0);
		Straight0=(Curv0<CurvatureLimitLin);
		for (i=3;i<m-1;i++)
		{
			PP=SourceContour[i];
			SignedCurvP=PP.GetCurvature();
			CurvP=fabs(SignedCurvP);
			StraightP=(CurvP<CurvatureLimitLin);
			P0InflectionPoint=false;
			if (Straight0)
				BufLin.AddPositionFast(P0);
			else
				BufArc.AddPositionFast(P0);
			if ((SignedCurv0>0&&SignedCurvP<=0)||
                (SignedCurv0<=0&&SignedCurvP>0))
			{
				if (Curv0>CurvP) InflectionPoints.push_back(PP);
				else InflectionPoints.push_back(P0);
				P0InflectionPoint=true;
			}
			if (StraightM&&!Straight0)
			{
				// edr: In special cases we may insert an empty group, so at least it should be checked for !=0
				if (BufLin.GetTotalPositions()>0)
				{
					//		BufLin.SetSeedPoint(SourceContour.GetSeedPoint()); // added by edr@mip.sdu.dk v. 0.53
					//		FindMaxGradientForLineSeg(BufLin); // added by edr@mip.sdu.dk v. 0.53
					AddToLineSegmentVector(LinearSegments,BufLin);
				}
				BufLin.Empty();
			}
			if ((!Straight0&&StraightP)||
				(SignedCurvM>CurvatureLimitLin&&SignedCurv0<=-CurvatureLimitLin)||
				(SignedCurvM<=-CurvatureLimitLin&&SignedCurv0>CurvatureLimitLin))
			{
				BufArc.SetClosed(false);
				ArcSegments.push_back(BufArc);
				BufArc.Empty();
			}
			if (Curv0>CurvatureLimitEx)
			{
				if (CurvP<Curv0&&CurvM<=Curv0)
					Maxima.push_back(P0);
				if (CurvP>Curv0&&CurvM>=Curv0)
					Minima.push_back(P0);
			}
			StraightM=Straight0;
			CurvM=Curv0;
			SignedCurvM=SignedCurv0;
			Straight0=StraightP;
			Curv0=CurvP;
			SignedCurv0=SignedCurvP;
			P0=PP;
		}
	}
	// edr: In special cases we may insert an empty group, so at least it should be checked for !=0
	if (BufLin.GetTotalPositions()>=0)
	{
		//		BufLin.SetSeedPoint(SourceContour.GetSeedPoint()); // added by edr@mip.sdu.dk v. 0.53
		//		FindMaxGradientForLineSeg(BufLin); // added by edr@mip.sdu.dk v. 0.53
		AddToLineSegmentVector(LinearSegments,BufLin);
		ArcSegments.push_back(BufArc);
	}
    if (SourceContour.GetClosed())
	{
		//      Joining arc or linear segments on both sides of zero index
		//		of closed contours
		P0=SourceContour[0];
		PM=SourceContour[SourceContour.GetTotalPositions()-1];
		if (fabs(P0.GetCurvature())<CurvatureLimitLin&&
			fabs(PM.GetCurvature())<CurvatureLimitLin)
		{
			if (LinearSegments.size()>1)
				for (i=0;i<LinearSegments[0].GetTotalPositions();i++)
					LinearSegments.back().AddPositionFast(LinearSegments[0][i]);
				LinearSegments.erase(LinearSegments.begin());
		}
		
		if (fabs(P0.GetCurvature())>=CurvatureLimitLin&&
			fabs(PM.GetCurvature())>=CurvatureLimitLin)
		{
			if (ArcSegments.size()<1)
			{
				for (i=0;i<ArcSegments[0].GetTotalPositions();i++)
					ArcSegments.back().AddPositionFast(ArcSegments[0][i]);
				ArcSegments.erase(ArcSegments.begin());
			}
			else ArcSegments[0].SetClosed(true);
		}
	}
}
void CLevelContours::DeriveArcDescriptors(CContour &Src,
										  double &sTotal,double &sMedian,int &iMedian, double &q0,double &q1, double &q2)
{
	float s=0,ds;
	double q=0,dq;
	int i;
	for (i=0;i<Src.GetTotalPositions();i++)
	{
		ds=Src[i].GetArcLength();
		if (i==1||i==Src.GetTotalPositions()-1)
			ds*=0.5;
		s+=ds;
		dq=Src[i].GetCurvature()*ds;
		q+=dq;
		//	   z=-Src[i].GetDirectionAngle()+Src[i+1].GetDirectionAngle();
	}
	sTotal=s;
	q0=q;
	s=0;
	q=0;
	i=0;
	while (q/q0<0.5)
	{
		ds=Src[i].GetArcLength();
		if (i==0) ds*=0.5;
		s+=ds;
		q+=Src[i].GetCurvature()*ds;
		i++;
	}
	sMedian=s-ds*(q/q0-0.5);
	iMedian=i-1;
	s=0;
	q1=0;
	q2=0;
    for (i=0;i<Src.GetTotalPositions();i++)
	{
		ds=Src[i].GetArcLength();
	   	   if (i==0||i==Src.GetTotalPositions()-1)
			   ds*=0.5;
		   s+=ds;
		   q1+=(s-sMedian)*Src[i].GetCurvature()*ds;
		   q2+=(s-sMedian)*(s-sMedian)*Src[i].GetCurvature()*ds;
	}
	q1/=sTotal;
	q2/=(sTotal*sTotal);
}
void CLevelContours::FindMaxGradientForLineSeg(CContour& LineSeg)
{
	int MaxGradient=0;
	CPoint2DEdge SeedPoint;
	for(int i=0; i<LineSeg.GetTotalPositions(); i++)
	{
		const CPoint2DContour& P=LineSeg[i];
		if (P.GetGradient()>MaxGradient)
		{
			MaxGradient=P.GetGradient();
			SeedPoint.Set(P.GetX(),P.GetY());
			SeedPoint.m_Gradient=MaxGradient;
		}
	}
	LineSeg.SetSeedPoint(SeedPoint);
}

// added by edr@mip.sdu.dk v. 0.53
void CLevelContours::RemoveGhostLines(vector<CContour> &LinearSegments) const
{
	ostringstream ost;
	RemoveGhostLines(LinearSegments,ost);
}

// added by edr@mip.sdu.dk v. 0.53
void CLevelContours::RemoveGhostLines(vector<CContour> &LinearSegments,ostringstream& ost) const
{
	// lines with less than this angle difference are parallel
	const float DIR_DEVIATION=ipl::DegreeToRad(15);
	const float MIN_DIST=2; // in pixels
	// if fraction between gradients are bigger than this value, they are considered
	// significantly different and the line segment with lowest gradient is removed
	const float FRACTION_GRADIENT_DIFF=(float)0.1;
	set<unsigned int> IndexesToRemove;
	for(int i=0; i<LinearSegments.size(); i++)
	{
		CContour& L1=LinearSegments[i];
		unsigned int L1LastIndex=L1.GetTotalPositions()-1;
		for(int j=i+1; j<LinearSegments.size(); j++)
		{
			CContour& L2=LinearSegments[j];
			unsigned int L2LastIndex=L2.GetTotalPositions()-1;
			// 1) Check if lines are parallel
			float Dir1=L1[L1.GetTotalPositions()/2].GetDirectionAngle();
			float Dir2=L2[L2.GetTotalPositions()/2].GetDirectionAngle();
			// note: we don't remove line segments with opposite directions
			if ((fabs(Dir1-Dir2)<DIR_DEVIATION) || (fabs(Dir1+ipl::PI*2-Dir2)<DIR_DEVIATION) ||
				(fabs(Dir2+ipl::PI*2-Dir1)<DIR_DEVIATION))
			{
				// 2) Check if at least one line end "overlaps" the other line
				//    and is close to the other line
				//    A special case here is, when one of the lines has both endpoints very close to the other
				//    line, in this case we remove this shorter line no matter what the max gradient difference
				//    for each line is
				CLineSegment2D<float> LSeg1(L1[0],L1[L1LastIndex]);
				CLineSegment2D<float> LSeg2(L2[0],L2[L2LastIndex]);
				// two first special cases
				if (((LSeg1.ProjectionOnSegment(LSeg2.GetStartPoint()) && LSeg1.Distance(LSeg2.GetStartPoint())<MIN_DIST)) &&
					((LSeg1.ProjectionOnSegment(LSeg2.GetEndPoint()) && LSeg1.Distance(LSeg2.GetEndPoint())<MIN_DIST)))
				{
					IndexesToRemove.insert(j);
				}
				else if (((LSeg2.ProjectionOnSegment(LSeg1.GetStartPoint()) && LSeg2.Distance(LSeg1.GetStartPoint())<MIN_DIST)) &&
					((LSeg2.ProjectionOnSegment(LSeg1.GetEndPoint()) && LSeg2.Distance(LSeg1.GetEndPoint())<MIN_DIST)))
				{
					IndexesToRemove.insert(i);
				}
				else
				{
					// normal case
					// DEBUG begin
					//					if (LSeg1.ProjectionOnSegment(LSeg2.GetStartPoint()) && LSeg1.Distance(LSeg2.GetStartPoint())<m_MinDistGhostRemoval)
					//						std::cout << "WAIT";
					//					if (LSeg1.ProjectionOnSegment(LSeg2.GetEndPoint()) && LSeg1.Distance(LSeg2.GetEndPoint())<m_MinDistGhostRemoval)
					//						std::cout << "WAIT";
					//					if (LSeg2.ProjectionOnSegment(LSeg1.GetStartPoint()) && LSeg2.Distance(LSeg1.GetStartPoint())<m_MinDistGhostRemoval)
					//						std::cout << "WAIT";
					//					if (LSeg2.ProjectionOnSegment(LSeg1.GetEndPoint()) && LSeg2.Distance(LSeg1.GetEndPoint())<m_MinDistGhostRemoval)
					//						std::cout << "WAIT";
					// DEBUG end
					if ((LSeg1.ProjectionOnSegment(LSeg2.GetStartPoint()) && LSeg1.Distance(LSeg2.GetStartPoint())<m_MinDistGhostRemoval) ||
						(LSeg1.ProjectionOnSegment(LSeg2.GetEndPoint()) && LSeg1.Distance(LSeg2.GetEndPoint())<m_MinDistGhostRemoval) ||
						(LSeg2.ProjectionOnSegment(LSeg1.GetStartPoint()) && LSeg2.Distance(LSeg1.GetStartPoint())<m_MinDistGhostRemoval) ||
						(LSeg2.ProjectionOnSegment(LSeg1.GetEndPoint()) && LSeg2.Distance(LSeg1.GetEndPoint())<m_MinDistGhostRemoval))
					{
						// If significant different start gradient - throw away the one with lowest gradient
						float MinGradient=(float)(ipl::Min(L1.GetSeedPoint().m_Gradient,L2.GetSeedPoint().m_Gradient));
						float MaxGradient=(float)(ipl::Max(L1.GetSeedPoint().m_Gradient,L2.GetSeedPoint().m_Gradient));
						if (((MaxGradient-MinGradient)/MaxGradient)>FRACTION_GRADIENT_DIFF)
						{
							// remove the line segment with lowest gradient
							if (L1.GetSeedPoint().m_Gradient<L2.GetSeedPoint().m_Gradient)
								IndexesToRemove.insert(i);
							else
								IndexesToRemove.insert(j);
						}
					}
				}
			}
		}
	}
	//	ost.str("");
	//	ost << "CLevelContours::RemoveGhostLines() Indexes removed: ";
	CPoint2D<float> P;
	for(std::set<unsigned int>::reverse_iterator it=IndexesToRemove.rbegin(); it!=IndexesToRemove.rend(); it++)
	{
		//		ost << *it << " ";
		P=LinearSegments[*it][0];
		CContour& Last=LinearSegments[LinearSegments.size()-1];
		if (&Last!=&LinearSegments[*it])
			LinearSegments[*it]=Last;
		LinearSegments.pop_back();
	}
}


bool CLevelContours::CContour::Load(const char* pPathAndFileName)
{
	ifstream s(pPathAndFileName);
	if (s.is_open()==true)
	{
		string str;
		SkipSpaceAndComments(s);
		s >> str;
		if (str!=string("CLevelContours::CContour"))
		{
			ostringstream ost;
			ost << "CLevelContours::CContour::Load() Did not find the token "
				"CLevelContours::CContour in beginning of file" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			s.close();
			return false;
		}
		SkipSpaceAndComments(s);
		s >> str;
		if (str==string("Empty"))
		{
			// this is an empty object
			return true;
		}
		
		
		
		if (str!=string("Color:"))
		{
			ostringstream ost;
			ost << "CLevelContours::CContour::Load() Did not find the token 'Color'" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			s.close();
			return false;
		}
		SkipSpaceAndComments(s);
		s >> m_Color;
		SkipSpaceAndComments(s);
		s >> str;
		if (str!=string("Closed:"))
		{
			ostringstream ost;
			ost << "CLevelContours::CContour::Load() Did not find the token 'Closed'" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			s.close();
			return false;
		}
		SkipSpaceAndComments(s);
		s >> m_Closed;
		return LoadInternal(s);
	}
	else
	{
		ostringstream ost;
		ost << "CLevelContours::CContour::Load() Failed opening file: " << pPathAndFileName << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
}

bool CLevelContours::CContour::Save(const char* pPathAndFileName)
{
	ofstream s(pPathAndFileName);
	if (s.is_open()==true)
	{
		s << "# Data from a CLevelContours::CContour object in the Videndo Library �" << endl;
		s << "CLevelContours::CContour" << endl;
		if (IsEmpty()==false)
		{
			s << "Color:  " << m_Color << endl;
			s << "Closed: " << m_Closed << endl;
			SaveInternal(s,5);
		}
		else
		{
			s << "Empty" << endl;
		}
		return true;
	}
	else
	{
		ostringstream ost;
		ost << "CLevelContours::CContour::Save() Failed opening file: " << pPathAndFileName << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
}

ostream& operator<<(ostream& s,const CLevelContours::CContour& Contour)
{
	s << "**************** CContour info *****************\n";
	if (Contour.IsEmpty()==false)
	{
		s << "Color:  " << Contour.GetColor() << endl;
		s << "Closed: " << Contour.GetClosed() << endl;
		s << *dynamic_cast<const CPositionGroup2D<CLevelContours::CPoint2DContour>* >(&Contour);
	}
	else
		s << " Empty" << endl;
	return s;
}

void CLevelContours::DeriveStraightnessDevation(CContour &Source,float &Delta,int &Index)
{
	CPoint2DContour P0,P1,P;
	float max,d,phi,phi0,x,y;
	int i,i0,imax;
	max=0;
	imax=Source.GetTotalPositions()-1;
	if (imax<6)
	{
		Delta=0;
		Index=0;
	}
	else
	{
		P0=Source[0];
		P1=Source[imax];
		phi0=atan2(P1.GetY()-P0.GetY(),P1.GetX()-P0.GetX());
		for (i=2;i<imax-2;i++)
		{
			P=Source[i];
			x=P.GetX()-P0.GetX();
			y=P.GetY()-P0.GetY();
			phi=atan2(y,x);
			d=fabs(sqrt(x*x+y*y)*sin(phi-phi0));
			if (d>max)
			{
				max=d;
				i0=i;
			}
		}
		Delta=max;
		Index=i0;
	}
}

void CLevelContours::SplitContour(CContour &Source, CContour &C0,CContour &C1, int Index)
{
	C0=Source;
	C0.ReAllocPositions(Index-1);
	C1.Empty();
	for (int i=Index;i<Source.GetTotalPositions();i++)
		C1.AddPosition(Source[i]);
}

void CLevelContours::AddToLineSegmentVector(vector<CContour> &LineSegments,CContour &Source)
{
	//	// DEBUG begin
	//	// the following three lines disables the functionality of this function!
	//	FindMaxGradientForLineSeg(Source); // added by edr@mip.sdu.dk v. 0.53
	//	LineSegments.push_back(Source);
	//	return;
	//	// DEBUG end
	
	vector<CContour> Stack;
	CContour Buf,C0,C1;
	float Delta;
	int Index;
	Stack.push_back(Source);
	while (Stack.size()>0)
	{ 
		Buf=Stack.back();
		Stack.pop_back();
		DeriveStraightnessDevation(Buf,Delta,Index);
		if (Delta<m_MaxStraightnessDevation)
		{
			FindMaxGradientForLineSeg(Buf); // added by edr@mip.sdu.dk v. 0.53
			LineSegments.push_back(Buf);
		}
		else
		{
			SplitContour(Buf,C0,C1,Index);
			Stack.push_back(C0);
			Stack.push_back(C1);
		}
	} 
}

} // end namespace videndo
