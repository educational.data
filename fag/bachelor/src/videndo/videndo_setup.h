#ifndef _VIDENDO_SETUP_H
#define _VIDENDO_SETUP_H

// If compiling with Visual C++, we remove some warnings
#ifdef _MSC_VER
#pragma warning(disable: 4786)
#endif /* _MSC_VER */

/************************************************/
/************************************************/
/********      Videndo Setup area          ********/
/************************************************/
/************************************************/

/** \defgroup macrodefines IPL98 Macro definitions with the #define directive (global)
	@{ */
#ifndef VIDENDO_VERSION
/** This is the overall version of the Videndo library. You can always print this number
	in your programs if you want to know the version number of Videndo that were compiled
	with your program. Note that functions, methods, classes etc. can have different
	version numbers but will always be less or equal to this number. */
#define VIDENDO_VERSION 1.20
#endif /* VIDENDO_VERSION */
/*@}*/ /* end group "macrodefines" */


#endif /* _VIDENDO_SETUP_H */
