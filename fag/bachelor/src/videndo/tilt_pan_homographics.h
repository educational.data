#ifndef _VIDENDO_TILTPANHOMOGRAPHICS_H
#define _VIDENDO_TILTPANHOMOGRAPHICS_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <points/point2d.h>
#include <ipl98/cpp/error.h>
#include <sstream>

namespace videndo{ // use namespace if C++

using std::ostringstream;
using ipl::CError;
using ipl::IPL_WARNING;

/** This class can be used for homographic transformations
    consisting of a tilt operation followed by a pan operation
    The 3d rotation matrix is:
	\verbatim 
	
	    (cosp	  0	-sinp) 	 ( 1	  0	  0 )
	R = (  0	  1	  0  ) * ( 0	 cost	sint)
	    (sinp	  0	 cosp)	 ( 0	-sint	cost)
	\endverbatim

	The class deals with the 2D-2D transformation:
	\verbatim
	
	(u,v) -> (u',v')
	(cu',cv',c)^T = K*R*K(-1)*(u,v,1)^T
	\endverbatim

	where K is the ideal camera matrix with a focal length m_f.   
	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CTiltPanHomographics videndo/tilt_pan_homographics.h
	@version 0.63
	@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
class CTiltPanHomographics
{
public:
	/** Default constructor. Initialize() must be called before
		an instance of this class can be used. */
	CTiltPanHomographics(); 

	/** Constructor with the focal length as constant parameter.
		Calls Initialize(), hence IsInitialized() will return true.
		@param f The focal length in pixels.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	CTiltPanHomographics(float f); 
	
	/// Default destructor
	~CTiltPanHomographics(){};

	/** Initializes this class by setting the focal length.
		@param f The focal length in pixels.
		@return False, if f is <=0. */
	bool Initialize(float f);

	/** Returns true, if Initialize() has been successfully called. */
	inline bool IsInitialized() const;

	/** Returns true if DeriveAngles() has been called. */
	inline bool IsEmpty() const;

	/** Resets information about a previous call to DeriveAngles(). That is,
		a following call to DeriveAngles() is needed in order to use this
		class again. */
	inline void Empty();

	/** Calculates the member variables m_t, m_p giving the transformation P0->(0,0)
		@param P0  is the point which is transformed to the focal center.
		@return False, if this class is not initialized.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool DeriveAngles(const CPoint2D<float> &P0);
	
	/** Calculates the member variables m_t,m_p giving the transformation (u0,v0)->(0,0)
		@param u0 is the u-coordinate of the point which is transformed to the focal center.
		@param v0 is the v-coordinate of the point which is transformed to the focal center.
		@return False, if this class is not initialized.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */ 
	bool DeriveAngles(float u0, float v0);  
	
	/** Gives as output parameters tilt and pan angles in radians.
		@param t Tilt value in radians.
		@param p Pan value in radians.
		@return False, if tilt and pan not available. */
	inline bool GetTiltPanAngles(float& t, float& p) const;

	/** Transforms a point In to the point Out using current coefficients.
		@param In Is the input point of the transformation.
		@param Out Is the transformed point.
		@return Returns false if no coefficients for the forward transformation 
			are calculated
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */   
	inline bool Transform(const CPoint2D<float> &In, CPoint2D<float> &Out) const;
	
	/** Transforms a point (u,v) into (um,vm) using current coefficients.
		@param u Is the u-coordinate of the input point of the transformation.
		@param v Is the v-coordinate of the input point of the transformation.
		@param um Is the v-coordinate of the transformed point.
		@param vm Is the c-coordinate of the transformed point.
		@return Returns false if no coefficients for the forward transformation 
			are not calculated.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool Transform(float u, float v, float &um, float &vm) const;
	
	/** Transforms (u,v) to (um,vm) using a transformation characterized by
		(u0,v0)->(0,0). Class must be initialized by a call to Initialize().
		This method is completely independent on other settings in this 
		class by a previous call to DeriveAngles(), it is also less efficient.
		@param u Is the u-coordinate of the input point of the transformation.
		@param v Is the v-coordinate of the input point of the transformation.
		@param um Is the u-coordinate of the transformed point.
		@param vm Is the v-coordinate of the transformed point.
		@param u0 Is the u-coordinate of the point which would be transformed to (0,0)
		@param v0 Is the v-coordinate of the point which would be transformed to (0,0)
		@return False, if IsInitialize() has not been successfully called.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */  
	bool Transform(float u, float v,float u0,float v0, float &um, float &vm) const;
	
	/** Inverse transforms a point (um,vm) into (u,v)In using current coefficients
		for backtransformation. 
		@param um Is the u-coordinate of the input point of the backtransformation.
		@param vm Is the v-coordinate of the input point of the backtransformation.
		@param u Is the u-coordinate of the backtransformed point.
		@param v Is the v-coordinate of the backtransformed point.
		@return False if no coefficients for the inverse transformation are not calculated.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */  
	bool BackTransform(float um, float vm, float &u, float &v) const;
	
	/** Transforms a point In to the point Out using current coefficients.
		@param In Is the input point of the backtransformation.
		@param Out Is the backtransformed point.
		@return Returns false if no coefficients for the forward transformation 
			are calculated
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */         
	bool BackTransform(const CPoint2D<float> &In, CPoint2D<float> &Out) const;
	
	/** Backtransforms (um,vm) to (u,v) using a (forward) transformation 
		characterized by (u0,v0)->(0,0). Class must be initialized by a call to 
		Initialize(). This method is completely independent on other settings in
		this class by a previous call to DeriveAngles(), it is also less efficient.
		@param um Is the u-coordinate of the input point of the backtransformation.
		@param vm Is the u-coordinate of the input point of the backtransformation.
		@param u Is the v-coordinate of the backtransformed point.
		@param v Is the v-coordinate of the backtransformed point.
		@param u0 Is the u-coordinate of the point which would be transformed to (0,0)
		@param v0 Is the v-coordinate of the point which would be transformed to (0,0)
		@return False, if IsInitialize() has not been successfully called.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool BackTransform(float um, float vm,float u0,float v0,float &u, float &v) const;
	
public:
	/** Set to true when DeriveAngles() has been called, if true, thenthat InUse()
		returns true. */
	bool m_TiltPanAvailable;
	/** Set to true when Initialize() is called. */
	bool m_IsInitialized;
	/** The focal length in the same units as u,v */
	float m_f;
	/** The tilt angle in radians of the transformation */
	float m_t;
	/** The pan angle in radians of the transformation */
	float m_p;
	/** Private coeficients in the transformation
		(um,vm) = (Tuu*u+Tuv*v+Tuf,Tvu*u+Tvv*v+Tvf)/(Nu*u+Nv*v+Nf)
		and the back transformation
		(u,v) = (tuu*um+tuv*vm+tuf,tvu*um+tvv*vm+tvf)/(nu*um+nv*vm+nf)
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	mutable float m_Tuu,m_Tuv,m_Tvv,m_Tuf,m_Tvf,m_Nu,m_Nv,m_Nf;
	mutable float m_tuu,m_tvu,m_tvv,m_tuf,m_tvf,m_nu,m_nv,m_nf; 
	/** Set to true by CalculateConstants(). */
	mutable bool m_ConstantsCalculated;
	/** Set to true by CalculateInverseConstants(). */
	mutable bool m_InverseConstantsCalculated;
	/** Calculates relevant coefficients of the transformation (u0,v0)->(0,0) 
		previously defined by a call to DeriveAngles().
		@param u0 is the u-coordinate of the point which is transformed to the
			focal center.
		@param v0 is the v-coordinate of the point which is transformed to the 
			focal center.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool CalculateConstants() const;
	/** Calculates relevant coefficients of the transformation (0,0)->(u0,v0) 
		previously defined
		@param u0 is the u-coordinate of the point which is transformed to the 
			focal center.
		@param v0 is the v-coordinate of the point which is transformed to the 
			focal center.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */   
	bool CalculateInverseConstants() const;
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

inline bool CTiltPanHomographics::IsInitialized() const
{
	return m_IsInitialized;
}

inline bool CTiltPanHomographics::IsEmpty() const
{
	return m_TiltPanAvailable;
}

inline void CTiltPanHomographics::Empty()
{
	m_TiltPanAvailable=false;
}

inline bool CTiltPanHomographics::GetTiltPanAngles(float& t, float& p) const
{
	if (m_TiltPanAvailable==false)
	{
		ostringstream ost;
		ost << "CTiltPanHomographics::GetTiltPanAngles() Tilt-pan angles not available"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	t=m_t;
	p=m_p;
	return true;
}

inline bool CTiltPanHomographics::Transform(const CPoint2D<float> &In, CPoint2D<float> &Out) const
{
	if ((!m_ConstantsCalculated) && (CalculateConstants()==false))
			return false;
	float um,vm;
	Transform(In.GetX(),In.GetY(),um,vm);
	Out.Set(um,vm);
	return true;
}

} // end namespace videndo

#endif //_VIDENDO_TILTPANHOMOGRAPHICS_H

