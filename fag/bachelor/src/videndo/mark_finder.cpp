#include "mark_finder.h"
// #include <mfc/fileutils.h> 
#include <sstream>
#include <fstream>
#include <iomanip>
#include <ios>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/geometry/plane.h> 
#include <ipl98/cpp/geometry/line3d.h> 
#include <ipl98/cpp/pixelgroup.h> 
#include <ipl98/cpp/algorithms/graphics.h> 
#include <time/time_date.h>

namespace videndo{ // use namespace if C++

using std::ostringstream;
using std::endl;
using std::ofstream;
using std::ifstream;
using std::setprecision;
using std::ios_base;
using ipl::FLOAT32;
using ipl::CPlane;
using ipl::CLine3D;
using ipl::CPixelGroup;
using ipl::Round;
using ipl::CGraphics;

// declaration of static members
const double CMarkFinder::m_Version=0.3;

CMarkFinder::CMarkFinder() : m_Perspective(640,480), m_CalibrationFileName(""), m_CameraSetupFileName("")
{
	m_Initialized=false;
	m_AutoSearchEnabled=false;
}

CMarkFinder::~CMarkFinder()
{
}

bool CMarkFinder::Initialize(const string& SetupPath, const string& FilePrefix)
{
	string SetupPath2(SetupPath);
	ipl::AddTrailingSlashToPath(SetupPath2);
	if (CFileUtils::PathFileExists(SetupPath2.c_str())==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::Initialize() Setup path '" << SetupPath2 << "' does not exist";
		m_ErrorMessage=ost.str();
		return false;
	}
	if (FilePrefix==string(""))
	{
		ostringstream ost;
		ost << "CMarkFinder::Initialize() File prefix is empty";
		m_ErrorMessage=ost.str();
		return false;
	}
	m_SetupPath=SetupPath2;
	m_FilePrefix=FilePrefix;
	if (LoadSetup()==true)
	{
		m_Initialized=true;
		return true;
	}
	else
		return false;
}

bool CMarkFinder::IsInitialized() const
{
	return m_Initialized;
}

bool CMarkFinder::IsCamInitialized() const
{
	if (m_Initialized==false)
		return false;
	else
		return m_Cam.IsInitialized();
}

bool CMarkFinder::IsCalibrationInitialized() const
{
	if (m_Initialized==false)
		return false;
	else
		return m_Perspective.RadCalibrationPerformed();
}

double CMarkFinder::GetVersion()
{
	return m_Version;
}

string CMarkFinder::GetSetupPath() const
{
	if (IsInitialized()==true)
		return m_SetupPath;
	else
		return string("");
}

string CMarkFinder::GetFilePrefix() const
{
	if (IsInitialized()==true)
		return m_FilePrefix;
	else
		return string("");
}

string CMarkFinder::GetSetupPathFileName() const
{
	if (IsInitialized()==true)
		return GetSetupFileName(m_SetupPath, m_FilePrefix);
	else
		return string("");
}

bool CMarkFinder::SetCameraSetupFile(const string& CamSetupFile)
{
	if (CamSetupFile==string(""))
	{
		ostringstream ost;
		ost << "CMarkFinder::SetCameraSetupFile() Provided string is empty";
		m_ErrorMessage=ost.str();
		return false;
	}
	else
	{
		if (m_Cam.Load(CamSetupFile.c_str(),true)==false)
		{
			ostringstream ost;
			ost << "CMarkFinder::SetCameraSetupFile() Failed loading camera setup file";
			m_ErrorMessage=ost.str();
			return false;
		}
		m_CameraSetupFileName=CamSetupFile;
		return true;
	}
}

string CMarkFinder::GetCameraSetupPathFileName() const
{
	if (m_CameraSetupFileName==string(""))
		return string("");
	else
		return GetCameraSetupPathFileName(m_SetupPath);
}

bool CMarkFinder::SetCalibrationSetupFile(const string& CalibSetupFile)
{
	if (CalibSetupFile==string(""))
	{
		ostringstream ost;
		ost << "CMarkFinder::SetCalibrationSetupFile() Provided string is empty";
		m_ErrorMessage=ost.str();
		return false;
	}
	else
	{
		if (m_Perspective.Load(CalibSetupFile.c_str())==false)
		{
			ostringstream ost;
			ost << "CMarkFinder::SetCalibrationSetupFile() Failed loading calibration setup file";
			m_ErrorMessage=ost.str();
			return false;
		}
		m_CalibrationFileName=CalibSetupFile;
		return true;
	}
}

string CMarkFinder::GetCalibrationPathFileName() const
{
	if (m_CalibrationFileName==string(""))
		return string("");
	else
		return GetCalibrationPathFileName(m_SetupPath);
}

bool CMarkFinder::StartCamera()
{
	if (m_Cam.IsInitialized()==false)
	{
		// initalize cameras
		m_Cam.QueryCameras();
		if (m_Cam.GetTotalCameras()!=1)
		{
			ostringstream ost;
			ost << "CMarkFinder::StartCamera() Found " << m_Cam.GetTotalCameras() << " cameras, make sure only 1 is connected";
			m_ErrorMessage=ost.str();
			return false;
		}
		// select first camera
		if (m_Cam.SelectCamera(0)==false)
		{
			ostringstream ost;
			ost << "CMarkFinder::StartCamera() Found camera but failed initializing it";
			m_ErrorMessage=ost.str();
			return false;
		}
		if (m_Cam.Initialize()==false)
		{
			ostringstream ost;
			ost << "CMarkFinder::StartCamera() Found camera but failed initializing it";
			m_ErrorMessage=ost.str();
			return false;
		}
//		if (m_Cam.StartImageAcquisition()==false)
//		{
//			ostringstream ost;
//			ost << "CMarkFinder::StartCamera() Found camera and initialized it, but failed calling StartImageAcquisition()";
//			m_ErrorMessage=ost.str();
//			return false;
//		}
//		m_Cam.InitializeAllCameras();
//		if (m_Cam.GetTotalCameras()!=1)
//		{
//			ostringstream ost;
//			ost << "CMarkFinder::StartCamera() Found " << m_Cam.GetTotalCameras() << " cameras, make sure only 1 is connected";
//			m_ErrorMessage=ost.str();
//			return false;
//		}
//		// select first camera
//		FIREi_CAMERA_HANDLE& hCamera=m_Cam.GetCameraHandle(0);
//		if (m_Cam.Initialize(hCamera)==false)
//		{
//			ostringstream ost;
//			ost << "CMarkFinder::StartCamera() Found camera but failed initializing it";
//			m_ErrorMessage=ost.str();
//			return false;
//		}
		std::cout << m_Cam << endl;
	}
	return true;
}

bool CMarkFinder::FindMarks(vector<CMarkPose>& MarkPoses,double& Time)
{
	if (IsInitialized()==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::FindMarks() Must call Initialize() first";
		m_ErrorMessage=ost.str();
		return false;
	}
	if (m_Perspective.RadCalibrationPerformed()==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::FindMarks() No calibration available - cannot find poses";
		m_ErrorMessage=ost.str();
		return false;
	}
	CTimeDate Timer;
	Timer.StartTimer();
	m_MarkPoses.clear();
	m_MarkCenters.clear();
	m_CSCenters.clear();
//	if (m_Img.Load("d:/temp/flipvision/edr/color_test/img1.bmp")==false)
	if (m_Cam.Acquire(m_Img)==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::FindMarks() Failed grabbing image";
		m_ErrorMessage=ost.str();
		return false;
	}
	CImage* pImg=&m_Img;
	bool DeleteTempImage=false;
	if (m_Img.GetBits()!=8)
	{
		pImg=new CImage;
		pImg->CopyConvert(8,m_Img);
		DeleteTempImage=true;
	}
	// need to make sure that 2D points in the pointset are empty (not the case if this method has been called before */
	m_MarkRetrieval.m_PointSets.SetAll2DPointsToNotUsed();
	m_MarkRetrieval.FindMarksAdaptive(*pImg,GetThresholdFileName(m_SetupPath,m_FilePrefix),
										m_RefMarkTypes,false,m_MarkRetrieval.GetThresholdStep());
	
	if (DeleteTempImage==true)
		delete(pImg);

	if (IsAutoSearching()==true)
	{
		// finding new marks is enabled - add these new marks to m_MarkRetrieval.m_PointSets
		unsigned int NewTotal=m_MarkRetrieval.AddNewRefMarksToExisting();
		// we set the 3D point to (0,0,m_Height) for the new points
		unsigned TotalAll=m_MarkRetrieval.m_PointSets.GetTotalPointSets();
		for(int i=TotalAll-NewTotal; i<TotalAll; i++)
		{
			m_MarkRetrieval.m_PointSets.SetPoint3D(CPoint3D<float>(0,0,m_Height),i);
		}
	}
	
	unsigned int Total=m_MarkRetrieval.m_PointSets.GetTotalPointSetsInUse();
	MarkPoses.resize(Total);
	m_MarkCenters.resize(Total);
	m_CSCenters.resize(Total);
		if (Total!=0)
	{
		if (GetPoses(MarkPoses)==false)
			return false;
	}
	Time=Timer.StopTimer();
	return true;
}

bool CMarkFinder::FindMarks(CImage& Source, vector<CMarkPose>& MarkPoses,double& Time)
{
	if (IsInitialized()==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::FindMarks() Must call Initialize() first";
		m_ErrorMessage=ost.str();
		return false;
	}
	if (m_Perspective.RadCalibrationPerformed()==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::FindMarks() No calibration available - cannot find poses";
		m_ErrorMessage=ost.str();
		return false;
	}
	CTimeDate Timer;
	Timer.StartTimer();
	m_MarkPoses.clear();
	m_MarkCenters.clear();
	m_CSCenters.clear();
	m_Img=Source;
	// need to make sure that 2D points in the pointset are empty (not the case if this method has been called before */
	m_MarkRetrieval.m_PointSets.SetAll2DPointsToNotUsed();
	m_MarkRetrieval.FindMarksAdaptive(m_Img,GetThresholdFileName(m_SetupPath,m_FilePrefix),
										m_RefMarkTypes,true/*false*/,m_MarkRetrieval.GetThresholdStep());


	//std::cout << "BEFORE: " << endl << m_MarkRetrieval.m_PointSets << endl;
	if (IsAutoSearching()==true)
	{
		// finding new marks is enabled - add these new marks to m_MarkRetrieval.m_PointSets
		unsigned int NewTotal=m_MarkRetrieval.AddNewRefMarksToExisting();
		// we set the 3D point to (0,0,m_Height) for the new points
		unsigned TotalAll=m_MarkRetrieval.m_PointSets.GetTotalPointSets();
		for(int i=TotalAll-NewTotal; i<TotalAll; i++)
		{
			m_MarkRetrieval.m_PointSets.SetPoint3D(CPoint3D<float>(0,0,m_Height),i);
		}
	}
	//std::cout << "AFTER: " << endl << m_MarkRetrieval.m_PointSets << endl;
	
	
	unsigned int Total=m_MarkRetrieval.m_PointSets.GetTotalPointSetsInUse();
	MarkPoses.resize(Total);
	m_MarkCenters.resize(Total);
	m_CSCenters.resize(Total);
	if (Total!=0)
	{
		if (GetPoses(MarkPoses)==false)
			return false;
	}
	Time=Timer.StopTimer();
	return true;
}

bool CMarkFinder::GetPoses(vector<CMarkPose>& MarkPoses)
{
	CCorresponding3D2DPoints& PointSets=m_MarkRetrieval.m_PointSets;
	unsigned int Count=0;
	vector<CPoint2D<double> > CSCenters; // code start centers
	m_MarkRetrieval.GetCodeStartCenters(CSCenters);
	for(int i=0; i<PointSets.GetTotalPointSets(); i++)
	{
		if (PointSets.Point2DInUse(i))
		{
			// First find 3D center of reference mark
			CPoint3D<FLOAT32> Position; // used for recognized position
			CPoint2D<float> Pos2D=PointSets.GetPoint2D(i);
			// insert mark center in attribute m_MarkCenters before radial correction
			m_MarkCenters[Count]=Pos2D;
			Pos2D=m_Perspective.GetPosRadRemoved(Pos2D);
			CPoint3D<float> Pos3D=PointSets.GetPoint3D(i); // (0,0,z)
			CPlane<FLOAT32> Plane(Pos3D,CPoint3D<FLOAT32>(1,0,0), CPoint3D<FLOAT32>(0,1,0));
			CPoint3D<FLOAT32> ReferenceDir;
			m_Perspective.Direction(Pos2D, ReferenceDir);
			CLine3D<FLOAT32> Line(m_Perspective.GetPinHole(),ReferenceDir);
			Plane.Intersect(Line,Position);
			// Now find the direction
			// 2D point of code start circle
			CPoint3D<FLOAT32> CSPosition;
			Pos2D=CSCenters[i]; // contains code start center
			// insert code start center in attribute m_CSCenters before radial correction
			m_CSCenters[Count]=Pos2D;
			Pos2D=m_Perspective.GetPosRadRemoved(Pos2D);
			m_Perspective.Direction(Pos2D, ReferenceDir);
			Line.Set(m_Perspective.GetPinHole(),ReferenceDir);
			Plane.Intersect(Line,CSPosition);
			CVector3D<double> Dir(CSPosition-Position);
			Dir.Normalize();
//			MarkPoses[Count].m_Direction=Dir;
//			MarkPoses[Count].m_MarkCenter=Position;
//			// Finally insert the id
//			MarkPoses[Count].m_Id=PointSets.GetId(i);
			MarkPoses[Count].Set(PointSets.GetId(i),Position,Dir);
			Count++;
		}
	}
	m_MarkPoses=MarkPoses;
	return true;
}

bool CMarkFinder::SaveLastImage(const string& PathAndFileName, bool FoundMarksToImage)
{
	string Path, FileNameExt, Ext;
	string PathAndFileName3(PathAndFileName);
	ipl::ConvertBackslashToSlash(PathAndFileName3);
	ipl::SplitFileName(PathAndFileName3.c_str(), Path, FileNameExt, Ext);
	if ((Path!=string("")) && (CFileUtils::PathFileExists(Path.c_str())==false))
	{
		ostringstream ost;
		ost << "CMarkFinder::SaveLastImage() Save image path '" << Path << "' does not exist";
		m_ErrorMessage=ost.str();
		return false;
	}
	if (Ext!=string("bmp"))
		Ext += string(".bmp");
	else
		Ext.assign("");
	string PathAndFileName2;
	PathAndFileName2 = Path + FileNameExt + Ext;
	if (FoundMarksToImage==true)
	{
		CImage Img;
		PlotInfoInImage(Img);
		return Img.Save(PathAndFileName2);
	}
	else
		return m_Img.Save(PathAndFileName2);
}

void CMarkFinder::PlotInfoInImage(CImage& Img)
{
	// create a color copy of image
	Img.CopyConvert(24,m_Img);
	// plot in image Img
	ipl::UINT32 CenterColor,CodeStartColor,TextColor;
	CenterColor=ipl::CPalette::CreateRGB(0,255,0); // green
	CodeStartColor=ipl::CPalette::CreateRGB(255,255,0); // yellow
	TextColor=ipl::CPalette::CreateRGB(255,0,0); // red
	// plot in image
	for(int i=0;i<m_MarkPoses.size();i++)
	{
		// plot mark centers
		CPixelGroup Grp;
		CPoint2D<FLOAT32> Pf2d=m_MarkCenters[i];
		CPoint2D<ipl::INT16> P(Round(Pf2d.GetX()),Round(Pf2d.GetY()));
		CGraphics::Cross(P,5,Grp);
		Grp.AddToImage(Img,CenterColor);
		// plot code start centers
		Grp.Empty();
		Pf2d=m_CSCenters[i];
		CPoint2D<ipl::INT16> P2=CPoint2D<ipl::INT16>(Round(Pf2d.GetX()),Round(Pf2d.GetY()));
		CGraphics::Cross(P2,5,Grp);
		Grp.AddToImage(Img,CodeStartColor);
		// write text in image
		CGraphics::FONTALIGNMENT Alignment;
		if (P.GetX()<Img.GetWidth()/2.0)
		{
			Alignment=CGraphics::TOPLEFT;
			P.AddX(3);
		}
		else
		{
			Alignment=CGraphics::TOPRIGHT;
			P.SubX(3);
		}
		CGraphics GraphText(TextColor,true,Alignment,CGraphics::Courier8x12);
		ostringstream ost;
		ost.setf(ios_base::fixed,ios_base::floatfield);
		ost << setprecision(1); // output with 1 decimals
		ost << "Id=" << m_MarkPoses[i].GetId() << " 3D pos=" << m_MarkPoses[i].GetMarkCenter();
		GraphText.PlotString(ost.str().c_str(),P,Img);
		ost.str("");
		ost << "  Dir=" << setprecision(2) << m_MarkPoses[i].GetDirection();
		P.AddY(14);
		GraphText.PlotString(ost.str().c_str(),P,Img);
	}
}

bool CMarkFinder::LoadSetup()
{
	try
	{
		string PathAndFileName=GetSetupFileName(m_SetupPath,m_FilePrefix);
		ifstream is(PathAndFileName.c_str());
		if (!is)
		{
			ostringstream ost;
			ost << "CMarkFinder::LoadSetup() Failed opening file: " << PathAndFileName;
			m_ErrorMessage=ost.str();
			return false;
		}
		else
		{
			m_Initialized=true;
			is >> *this;
		}
	}
	catch(string Str)
	{
		m_Initialized=false;
		m_ErrorMessage=Str;
		return false;
	}
	return true;
}

bool CMarkFinder::SaveSetup()
{
	string PathAndFileName;
	PathAndFileName=GetSetupFileName(m_SetupPath, m_FilePrefix);
	ofstream os(PathAndFileName.c_str());
	if (!os)
	{
		ostringstream ost;
		ost << "CMarkFinder::SaveSetup() Failed opening file: " << PathAndFileName;
		m_ErrorMessage=ost.str();
		return false;
	}
	else
	{
		os << *this;
	}
	return true;
}

bool CMarkFinder::StopCamera()
{
	m_Cam.Close();
	return true;
}

bool CMarkFinder::AddMark(int Id, double Height)
{
	if (m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,Height),Id)==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::AddMark() Id (" << Id << ") not unique";
		m_ErrorMessage=ost.str();
		return false;
	}
	return true;
}

bool CMarkFinder::RemoveMark(int Id)
{
	if (m_MarkRetrieval.m_PointSets.RemovePointSet(Id)==false)
	{
		ostringstream ost;
		ost << "CMarkFinder::RemoveMark() Given id (" << Id << ") not found";
		m_ErrorMessage=ost.str();
		return false;
	}
	return true;
}

vector<int> CMarkFinder::SearchNewId(double Height, double& Time)
{
	// remember class settings to be restored after this method
	double HeightOriginal=m_Height;
	bool AutoSearchEnabledOriginal=m_AutoSearchEnabled;
	CCorresponding3D2DPoints PointSetsOriginal;
	PointSetsOriginal=m_MarkRetrieval.m_PointSets;
	m_Height=Height;
	SetAutoSearch(true, Height);
	// we add a temporary point to the m_MarkRetrieval.m_PointSets that doesn't exists
	// this ensures that the FindMarks algorithms will search all thresholds 
	m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,m_Height),128);

	// delete threshold file so we will iterate thresholds based on lighting in actual image
	DeleteFile(GetThresholdFileName(m_SetupPath,m_FilePrefix).c_str());


	vector<CMarkPose> MarkPoses;
	vector<int> NewIds;
	// this call will now add new Id's to m_MarkRetrieval.m_PointSets
	if (FindMarks(MarkPoses,Time)==true)
	{
		for(int i=0; i<m_MarkRetrieval.m_NewPointSets.GetTotalPointSets(); i++)
		{
			NewIds.push_back(m_MarkRetrieval.m_NewPointSets.GetId(i));
		}
	}

	// before returning we need to reset this class' state
	SetAutoSearch(AutoSearchEnabledOriginal, HeightOriginal);
	m_MarkRetrieval.m_PointSets=PointSetsOriginal;
	return NewIds;
}

vector<int> CMarkFinder::SearchNewId(CImage& Source, double Height, double& Time)
{
	// remember class settings to be restored after this method
	double HeightOriginal=m_Height;
	bool AutoSearchEnabledOriginal=m_AutoSearchEnabled;
	CCorresponding3D2DPoints PointSetsOriginal;
	PointSetsOriginal=m_MarkRetrieval.m_PointSets;
	m_Height=Height;
	SetAutoSearch(true, Height);
	// we add a temporary point to the m_MarkRetrieval.m_PointSets that doesn't exists
	// this ensures that the FindMarks algorithms will search all thresholds 
	m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,m_Height),128);

	// delete threshold file so we will iterate thresholds based on lighting in actual image
	DeleteFile(GetThresholdFileName(m_SetupPath,m_FilePrefix).c_str());


	vector<CMarkPose> MarkPoses;
	vector<int> NewIds;
	// this call will now add new Id's to m_MarkRetrieval.m_PointSets
	if (FindMarks(Source, MarkPoses,Time)==true)
	{
		for(int i=0; i<m_MarkRetrieval.m_NewPointSets.GetTotalPointSets(); i++)
		{
			NewIds.push_back(m_MarkRetrieval.m_NewPointSets.GetId(i));
		}
	}

	// before returning we need to reset this class' state
	SetAutoSearch(AutoSearchEnabledOriginal, HeightOriginal);
	m_MarkRetrieval.m_PointSets=PointSetsOriginal;
	return NewIds;
}

void CMarkFinder::SetAutoSearch(bool Enable, double Height)
{
	m_MarkRetrieval.SetNewRefMarks(Enable);
	m_Height=Height;
	m_AutoSearchEnabled=Enable;
}

bool CMarkFinder::IsAutoSearching() const
{
	return m_AutoSearchEnabled;
}

string CMarkFinder::GetLastError()
{
	return m_ErrorMessage;
}

ostream& operator<<(ostream& os, const CMarkFinder& MarkFinder) throw(string)
{
	if (MarkFinder.IsInitialized()==false)
	{
		ostringstream ost;
		ost << "operator<<(ostream&, const MarkFinder&) Must call Initialize() first." << IPLAddFileAndLine;
		std::cerr << ost.str() << endl;
		throw(ost.str());
	}
	os << "# CMarkFinder setup written by CMarkFinder class from the Videndo library" << endl;
	os << "#\n# CMarkFinder class version" << endl;
	os << "CMarkFinder " <<  MarkFinder.GetVersion() << endl << endl;
	MarkFinder.WriteSetup(os);
	return os;
}

istream& operator>>(istream& is, CMarkFinder& MarkFinder) throw(string)
{
	string Token;
	ipl::SkipSpaceAndComments(is);
	// read 'CMarkFinder' token
	is >> Token;
	if (Token!=string("CMarkFinder"))
	{
		ostringstream ost;
		ost << "operator>>(istream, CMarkFinder) Token CMarkFinder not found";
		MarkFinder.m_ErrorMessage=ost.str();
		throw(ost.str());
		return is;
	}
	double Version;
	is >> Version;
	if (Version!=MarkFinder.GetVersion())
	{
		ostringstream ost;
		ost << "operator>>(istream, MarkFinder) File version is " << Version << " but this class "
			" versions is " << MarkFinder.GetVersion();
		MarkFinder.m_ErrorMessage=ost.str();
		throw(ost.str());
		return is;
	}
	try
	{
		MarkFinder.ReadSetup(is);
	}
	catch(string str)
	{
		throw(str);
	}
	return is;
}

bool CMarkFinder::WriteSetup(ostream& os) const throw(string)
{
	os << "# Refmark types can be either REFMARK_ALL, REFMARK_SMALL, or REFMARK_BIG" << endl;
	if ((m_RefMarkTypes & CMarkRetrieval::REFMARK_SMALL) && 
		(m_RefMarkTypes & CMarkRetrieval::REFMARK_BIG))
	{
		os << "CMarkType: REFMARK_ALL" << endl;
	}
	else if (m_RefMarkTypes & CMarkRetrieval::REFMARK_SMALL)
	{
		os << "CMarkType: REFMARK_SMALL" << endl;
	}
	else if (m_RefMarkTypes & CMarkRetrieval::REFMARK_BIG)
	{
		os << "CMarkType: REFMARK_BIG" << endl;
	}
	else
	{
		ostringstream ost;
		ost << "operator<<(ostream&, const MarkFinder&) Unknown reference mark type in "
			"CMarkFinder.m_RefMarkTypes." << IPLAddFileAndLine;
		std::cerr << ost.str() << endl;
		throw(ost.str());
	}
	os << endl;
	os << "# Threshold iteration and limit values" << endl;
	os << "ThresholdLowerBound: " << m_MarkRetrieval.GetThresholdLowerBound() << endl;
	os << "ThresholdUpperBound: " << m_MarkRetrieval.GetThresholdUpperBound() << endl;
	os << "ThresholdStep:       " << m_MarkRetrieval.GetThresholdStep() << endl << endl;
	os << "# Active identification marks" << endl;
	os << "# first value is ID number, second is height above plane in mm." << endl;
	os << "IdentificationMarks:" << endl;
	for(int x=0; x<m_MarkRetrieval.m_PointSets.GetTotalPointSets(); x++)
	{
		
		os << "Mark  " << m_MarkRetrieval.m_PointSets.GetId(x) << "    "
		   << m_MarkRetrieval.m_PointSets.GetPoint3D(x).GetZ() << endl;
	}
	os << "EndOfIdentificationMarks   # DO NOT DELETE THIS LINE!" << endl << endl;
	os << "# Camera setup" << endl;
	if (m_Cam.IsInitialized()==true)
	{
		//string CamSetupFile(MarkFinder.m_CameraSetupFileName);
		os << "CameraSetupFile:  " << m_CameraSetupFileName << endl;
		m_Cam.Save(GetCameraSetupPathFileName().c_str());
		//os << MarkFinder.m_Cam << endl;
	}
	else
	{
		os << "CameraNotInitialized" << endl;
	}
	os << endl << "# Calibration setup" << endl;
	if (m_Perspective.RadCalibrationPerformed()==true)
	{
		os << "CalibrationSetupFile: " << m_CalibrationFileName << endl;
		m_Perspective.Save(GetCalibrationPathFileName().c_str());
	}
	else
		os << "CalibrationNotAvailable" << endl;
	return true;
}

bool CMarkFinder::ReadSetup(istream& is) throw(string)
{
	string Token;
	unsigned int LowerBound, UpperBound, Step;
	try{
		ipl::SkipSpaceAndComments(is);
		// skip 'CMarkType:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> Token;
		if (Token==string("REFMARK_ALL"))
		{
			m_RefMarkTypes=(CMarkRetrieval::REFMARK_SMALL & CMarkRetrieval::REFMARK_BIG);
		}
		else if (Token==string("REFMARK_SMALL"))
		{
			m_RefMarkTypes=CMarkRetrieval::REFMARK_SMALL;
		}
		else if (Token==string("REFMARK_BIG"))
		{
			m_RefMarkTypes=CMarkRetrieval::REFMARK_BIG;
		}
		else
		{
			ostringstream ost;
			ost << "operator>>(istream, MarkFinder) Unknown reference mark type: " 
				<< Token << GetVersion();
			m_ErrorMessage=ost.str();
			throw(ost.str());
		}
		ipl::SkipSpaceAndComments(is);
		// skip 'ThresholdLowerBound:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> LowerBound;
		ipl::SkipSpaceAndComments(is);
		// skip 'ThresholdUpperBound:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> UpperBound;
		ipl::SkipSpaceAndComments(is);
		// skip 'ThresholdStep:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		ipl::SkipSpaceAndComments(is);
		is >> Step;
		if (m_MarkRetrieval.SetThresholdValues(LowerBound, UpperBound, Step)==false)
		{
			ostringstream ost;
			ost << "operator>>(istream, CMarkFinder) Invalid threshold values in setup file";
			m_ErrorMessage=ost.str();
			throw(ost.str());
		}
		// Read active Id's and their z-value
		m_MarkRetrieval.m_PointSets.Empty();
		ipl::SkipSpaceAndComments(is);
		is >> Token;
		if (Token!=string("IdentificationMarks:"))
		{
			ostringstream ost;
			ost << "operator>>(istream, CMarkFinder) Token IdentificationMarks not found";
			m_ErrorMessage=ost.str();
			throw(ost.str());
		}
		ipl::SkipSpaceAndComments(is);
		is >> Token;
		ipl::SkipSpaceAndComments(is);
		while(Token!=string("EndOfIdentificationMarks"))
		{
			unsigned int Id;
			double Height;
			is >> Id;
			is >> Height;
			m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,Height),Id);
			ipl::SkipSpaceAndComments(is);
			is >> Token;
		}
		
		ipl::SkipSpaceAndComments(is);
		char* pStr = new char[128];
		// look for camera token
		is >> Token;
		if (Token==string("CameraSetupFile:"))
		{
			ipl::SkipSpaceAndComments(is);
			is.getline(pStr,128); // reads rest of line since item can consist of spaces
			m_CameraSetupFileName.assign(pStr);
			string CamSetupFile=GetCameraSetupPathFileName(GetSetupPath());
			// starts camera if possible - i.e. no need to call StartCamera()
			m_Cam.Load(CamSetupFile.c_str(),true);
		}
		ipl::SkipSpaceAndComments(is);
		is >> Token;
		if (Token==string("CalibrationSetupFile:"))
		{
			ipl::SkipSpaceAndComments(is);
			is.getline(pStr,128); // reads rest of line since item can consist of spaces
			m_CalibrationFileName.assign(pStr);
			string CalibFile=GetCalibrationPathFileName(GetSetupPath());
			m_Perspective.Load(CalibFile.c_str());
		}
		
		delete [] pStr;
	}
	catch(...) // all exceptions
	{
		throw("Failed reading from data file containing a CMarkFinder object");
	}
	return true;
}

bool CMarkFinder::SaveDefaultSetup(const string& SetupPath, const string& FilePrefix)
{
	try
	{
		string SetupPath2(SetupPath);
		ipl::AddTrailingSlashToPath(SetupPath2);
		if (CFileUtils::PathFileExists(SetupPath2.c_str())==false)
		{
			ostringstream ost;
			ost << "CMarkFinder::SaveDefaultSetup() Setup path '" << SetupPath2 << "' does not exist";
			m_ErrorMessage=ost.str();
			return false;
		}
		if (FilePrefix==string(""))
		{
			ostringstream ost;
			ost << "CMarkFinder::SaveDefaultSetup() File prefix is empty";
			m_ErrorMessage=ost.str();
			return false;
		}
		string PathAndFileName=GetSetupFileName(SetupPath2,FilePrefix);
		if (CFileUtils::PathFileExists(PathAndFileName.c_str())==true)
		{
			ostringstream ost;
			ost << "CMarkFinder::SaveDefaultSetup() File '" << PathAndFileName << "' already exists";
			m_ErrorMessage=ost.str();
			return false;
		}
		// necessary to set the SetupPath and CameraSetupFileName in this class since the operator<< retrieves
		// the camera setup file name based upon these values
		string CameraSetupFileName1=m_CameraSetupFileName;
		string SetupPath1=m_SetupPath;
//		string FilePrefix1=m_FilePrefix;
		m_SetupPath=SetupPath2;
//		m_FilePrefix=FilePrefix;
		ofstream os(PathAndFileName.c_str());
		if (!os)
		{
			ostringstream ost;
			ost << "CMarkFinder::SaveDefaultSetup() Failed opening file: " << PathAndFileName;
			m_ErrorMessage=ost.str();
			return false;
		}
		else
		{
			bool InitializedBefore=m_Initialized; // we temporarely initialize this class if not initialized before
			if (m_Cam.IsInitialized()==false)
			{
				// try starting camera so that camera info can be stored in setup file
				if (StartCamera()==true)
				{
					m_CameraSetupFileName=FilePrefix;
					m_CameraSetupFileName.append("_CameraSetup.cfg");
				}
//				else
//					m_CameraSetupFileName=string("");
			}
			if (m_Initialized==false)
			{
				// create a default initialized version of this class
				m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,21),1);
				m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,31),2);
				m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,25),3);
				m_MarkRetrieval.m_PointSets.AddPoint3DAndId(CPoint3D<float>(0,0,59),4);
				m_MarkRetrieval.SetThresholdValues(20, 210, 10);
				m_Initialized=true;
			}
			os << "#############################################################" << endl;
			os << "# This is a default setup file written by CMarkFinder       #" << endl;
			os << "# Add as many reference marks as needed, but they must have #" << endl;
			os << "# unique id numbers                                         #" << endl;
			os << "# If a camera was found, the current camera settings have   #" << endl;
			os << "# been stored and a line contains CameraSetupFile: followed #" << endl;
			os << "# by a filename.                                            #" << endl;
			os << "# If camera setup is not available, a line containing       #" << endl;
			os << "# CameraNotInitialized will be present. Replace this line   #" << endl;
			os << "# with the token 'CameraSetupFile: ' followed by a filename #" << endl;
			os << "# containing a camera setup                                 #" << endl;
			os << "# If calibration is available replace the token             #" << endl;
			os << "# 'CalibrationNotAvailable' with 'CalibrationSetupFile:'    #" << endl;
			os << "# followed by the file name                                 #" << endl;
			os << "#############################################################" << endl 
			   << endl << endl;
			
			os << *this;
			m_Initialized=InitializedBefore;
		}
		// restore original values
		m_SetupPath=SetupPath1;
		m_CameraSetupFileName=CameraSetupFileName1;
//		m_FilePrefix=FilePrefix1;
		return true;
	}
	catch(string Str)
	{
		m_ErrorMessage=Str;
		return false;
	}
}

string CMarkFinder::GetSetupFileName(const string& SetupPath, const string& FilePrefix) const
{
	ostringstream ost;
	ost << SetupPath << FilePrefix << "_MarkFinderSetup.cfg";
	return ost.str();
}

string CMarkFinder::GetCalibrationPathFileName(const string& SetupPath) const
{
	ostringstream ost;
	ost << SetupPath << m_CalibrationFileName;
	return ost.str();
}

string CMarkFinder::GetCameraSetupPathFileName(const string& SetupPath) const
{
	ostringstream ost;
	ost << SetupPath << m_CameraSetupFileName;
	return ost.str();
}

string CMarkFinder::GetThresholdFileName(const string& SetupPath, const string& FilePrefix) const
{
	ostringstream ost;
	ost << SetupPath << FilePrefix << "_DynamicThresholds.cfg";
	return ost.str();
}

} // end namespace videndo
