#include "hoops_control.h"
#include "HDB.h"
#include "HIOManager.h"
#include "HUtility.h"
#include <sstream>
#include <iomanip>
#include <fstream>
#include <ios>
#include <ipl98/cpp/error.h>
#include <ipl98/cpp/vectors/vector3d.h>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/pixelgroups.h>
#include <ipl98/cpp/algorithms/feature_extraction.h> 
#include <ipl98/cpp/algorithms/segmentate.h>
#include <paintlib_convert/paintlib_convert.h>
#include <videndo/mark_retrieval.h>
#include "CMyHoopsDoc.h"

using std::ostringstream;
using std::endl;
using std::ios;
using std::ios_base;
using std::setw;
using std::setprecision;
using std::setfill;
using std::ofstream;
using ipl::CError;
using ipl::IPL_ERROR;
using ipl::DegreeToRad;
using ipl::CVector3D;
using ipl::CPixelGroups;
using ipl::CSegmentate;
using ipl::CFeatures;

namespace videndo{ // use namespace if C++

const double CHoopsControl::m_Version=0.5;

typedef	struct {float x, y, z;}	Point;

CHoopsControl::CHoopsControl()
{
	m_pHView=NULL;
	m_pHoopsModel=NULL;
	m_Initialized=false;
	m_HoopsToCImageInitalized=false;
	m_FocalFactorInUse=false;
	m_DimensionsInUse=false;
	m_hsra_options=NULL;
	m_hlr_options=NULL;
	m_current_hsra=NULL;
	m_image=NULL;
	m_image_row=NULL;
	m_image_segment=NULL;
	m_driver_segment=NULL;
}

CHoopsControl::~CHoopsControl()
{
	HoopsToCImageReset();
}

void CHoopsControl::HoopsToCImageReset()
{
	if (m_HoopsToCImageInitalized==true)
	{
		free(m_hsra_options);
		free(m_hlr_options);
		free(m_current_hsra);
		delete m_image;
		delete m_image_row;
		free(m_image_segment);
		free(m_driver_segment);
		//HC_Delete_Segment(m_driver_segment);
		//HC_Delete_Segment(m_image_segment);
	}
}

bool CHoopsControl::Initialize(HBaseView* pHView, HBaseModel* pHoopsModel, CMyHoopsDoc* pDoc)
{
	m_pHView=pHView;
	m_pHoopsModel=pHoopsModel;
	m_pDoc=pDoc;
	m_Initialized=true;
	return true;
}

bool CHoopsControl::SetFocalFactor(double FocalFactor)
{
	if (FocalFactor<=0)
	{
		ostringstream ost;
		ost << "CHoopsControl::SetFocalFactor() FocalFactor <= 0" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_FocalFactor=FocalFactor;
	m_FocalFactorInUse=true;
	return true;
}

bool CHoopsControl::SetDimensions(unsigned int Width, unsigned int Height)
{
	if ((Width==0) || (Height==0))
	{
		ostringstream ost;
		ost << "CHoopsControl::SetDimensions() Width or Height is 0" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_Width=Width;
	m_Height=Height;
	m_ImageWidth=Width;
	m_ImageHeight=Height;
	m_DimensionsInUse=true;
	return true;
}

bool CHoopsControl::SetImageDimensions(unsigned int Width, unsigned int Height)
{
	if ((Width==0) || (Height==0))
	{
		ostringstream ost;
		ost << "CHoopsControl::SetDimensions() Width or Height is 0" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_ImageWidth=Width;
	m_ImageHeight=Height;
	m_DimensionsInUse=true;
	return true;
}

bool CHoopsControl::SetCameraPose(const TCameraParameters& Par, unsigned int Width, unsigned int Height)
{
	return SetCameraPose(CPoint3D<double>(Par.dx,Par.dy,Par.dz),Par.a,Par.b,Par.c,Par.FocalLength,Width,Height);
}

bool CHoopsControl::SetCameraPose(const TCameraParameters& Par)
{
	return SetCameraPose(CPoint3D<double>(Par.dx,Par.dy,Par.dz),Par.a,Par.b,Par.c,Par.FocalLength);
}

bool CHoopsControl::SetCameraPose(const CPoint3D<double>& Pos, double a, double b, double c, double FocalLength)
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::SetCameraPose() This class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (DimensionsInUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::SetCameraPose() Dimensions not set" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	return SetCameraPose(Pos,a,b,c,FocalLength,m_Width,m_Height);
}

//bool CHoopsControl::SetCameraPose(const CPoint3D<double>& Pos, double a, double b, double c, double FocalLength,
//					unsigned int Width, unsigned int Height)
//{
//	if (InUse()==false)
//	{
//		ostringstream ost;
//		ost << "CHoopsControl::SetCameraPose() This class not initialized" << IPLAddFileAndLine;
//		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
//		return false;
//	}
//	// Setup virtual camera
//	HC_Open_Segment_By_Key(m_pHView->GetSceneKey());
//
//	Point position, target, up;
//	// Set up vector
//	double sina=sin(a);
//	double cosa=cos(a);
//	double sinc=sin(c);
//	double cosc=cos(c);
//	double sinb=sin(b);
//	up.x=-sinc*cos(b);
//	up.y=-sinc*sinb*sina+cosc*cosa;
//	up.z=sinc*sinb*cosa+cosc*sina;
//	// Calculate target
//	CVector3D<ipl::FLOAT32> TargetDir(0,0,1);
//	TargetDir.Rotate(a,b);
//	// set camera pose
//	position.x=Pos.GetX()-2*TargetDir.GetX();
//	position.y=Pos.GetY()-2*TargetDir.GetY();
//	position.z=Pos.GetZ()-2*TargetDir.GetZ();
//
//	if (m_FocalFactorInUse==true)
//		TargetDir *= (FocalLength*m_FocalFactor);
//	else
//		TargetDir *= (FocalLength);
//
////	//CPoint2D<ipl::FLOAT32> FCOld(m_Width/2.0,m_Height/2.0);
////	CPoint2D<ipl::FLOAT32> FC(m_pCam->m_Par.xh,m_pCam->m_Par.yh);
////	m_pCam->Direction(FC, TargetDir); // resulting TargetDir is already normalized
////	TargetDir *= m_pCam->m_Par.FocalLength;
//
//	CVector3D<double> TargetPos;
//	TargetPos.Set(position.x+TargetDir.GetX(),position.y+TargetDir.GetY(),position.z+TargetDir.GetZ());
//	target.x=TargetPos.GetX();
//	target.y=TargetPos.GetY();
//	target.z=TargetPos.GetZ();
//	HC_Set_Camera(&position, &target, &up, Width, Height, "perspective");
//	HC_Close_Segment();
//	m_pHView->Update();
//	return true;
//}

bool CHoopsControl::SetCameraPose(const CPoint3D<double>& Pos, double a, double b, double c, double FocalLength,
					unsigned int Width, unsigned int Height)
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::SetCameraPose() This class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	// Setup virtual camera
	HC_Open_Segment_By_Key(m_pHView->GetSceneKey());

	Point position, target, up;
	position.x=Pos.GetX();
	position.y=Pos.GetY();
	position.z=Pos.GetZ();
	// Set up vector
	double sina=sin(a);
	double cosa=cos(a);
	double sinc=sin(c);
	double cosc=cos(c);
	double sinb=sin(b);
	up.x=-sinc*cos(b);
	up.y=-sinc*sinb*sina+cosc*cosa;
	up.z=sinc*sinb*cosa+cosc*sina;
	// Calculate target
	CVector3D<ipl::FLOAT32> TargetDir(0,0,1);
	TargetDir.Rotate(a,b);
	if (m_FocalFactorInUse==true)
		TargetDir *= (FocalLength*m_FocalFactor);
	else
		TargetDir *= (FocalLength);

//	//CPoint2D<ipl::FLOAT32> FCOld(m_Width/2.0,m_Height/2.0);
//	CPoint2D<ipl::FLOAT32> FC(m_pCam->m_Par.xh,m_pCam->m_Par.yh);
//	m_pCam->Direction(FC, TargetDir); // resulting TargetDir is already normalized
//	TargetDir *= m_pCam->m_Par.FocalLength;

	CVector3D<double> TargetPos;
	TargetPos.Set(position.x+TargetDir.GetX(),position.y+TargetDir.GetY(),position.z+TargetDir.GetZ());
	target.x=TargetPos.GetX();
	target.y=TargetPos.GetY();
	target.z=TargetPos.GetZ();
	HC_Set_Camera(&position, &target, &up, Width, Height, "perspective");
	HC_Close_Segment();
	m_pHView->Update();
	return true;
}

CVector3D<double> CHoopsControl::GetUpVector() const
{
	float x,y,z;
	HC_Open_Segment_By_Key(m_pHView->GetSceneKey());
	HC_Show_Camera_Up_Vector(&x, &y, &z);
	HC_Close_Segment();
	return CVector3D<double>(x,y,z);
}

CVector3D<double> CHoopsControl::GetTarget() const
{
	float x,y,z;
	HC_Open_Segment_By_Key(m_pHView->GetSceneKey());
	HC_Show_Camera_Target(&x, &y, &z);
	HC_Close_Segment();
	return CVector3D<double>(x,y,z);
}

CPoint3D<double> CHoopsControl::GetCameraPosition() const
{
	float x,y,z;
	HC_Open_Segment_By_Key(m_pHView->GetSceneKey());
	HC_Show_Camera_Position(&x, &y, &z);
	HC_Close_Segment();
	return CPoint3D<double>(x,y,z);
}

bool CHoopsControl::HoopsToCImage(CImage& Img) 
{
	if (DimensionsInUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::HoopsToCImage() Dimensions not set" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	return HoopsToCImage(Img,m_ImageWidth,m_ImageHeight);
}

bool CHoopsControl::HoopsToCImage(CImage& Img, unsigned int Width, unsigned int Height) 
{
	// find handler for type
	HOutputHandler * handler = HDB::GetHIOManager()->GetOutputHandler("tif");

	HFileOutputResult result = OutputNotHandled;

	char hsra_options[1024];
	char hlr_options[1024];
	char current_hsra[1024];

	/* first find out the relevant options associated with the view */
	HC_Open_Segment_By_Key(m_pHView->GetViewKey());
//	HC_Open_Segment_By_Key(GetViewKey());
	HC_Show_One_Net_Rendering_Optio("hidden surface removal algorithm", current_hsra);
	
	HRenderMode rndrmode = m_pHView->GetRenderMode();
	if (rndrmode == HRenderHiddenLine || rndrmode == HRenderHiddenLineFast)
	{
		
		HC_Show_One_Net_Rendering_Optio("hidden line removal options", hlr_options);
		sprintf(hsra_options, "hsra = hidden line, hidden line removal options = (%s)", hlr_options);
	}
	else
	{
		
		if(handler->GetOutputDefaultHSRA())
			sprintf(hsra_options, "hsra = %s", handler->GetOutputDefaultHSRA());
		else
			sprintf(hsra_options, "hsra = szb, technology = software frame buffer");
	}
	HC_Close_Segment();
	
	char image_segment[4096];
	char driver_segment[4096];
	
	sprintf(image_segment,"?driver/null/hbaseview_%p", (void*)this);
	sprintf(driver_segment,"?driver/image/hbaseview_%p", (void*)this);
	
	// prepare image for rendering
	HC_Open_Segment(image_segment);
	HPixelRGB *image = new HPixelRGB[Width * Height];
	HC_KEY image_key = HC_KInsert_Image(0.0, 0.0, 0.0, "rgb", Width, Height, image);
	HC_Close_Segment();
	
	// prepare data to render
	HC_Open_Segment(driver_segment);
	char buffer[4096];
	sprintf (buffer, "use window id = %s%p, subscreen = (-1, 1, -1, 1)", H_EXTRA_POINTER_FORMAT, (void*)image_key);
	HC_Set_Rendering_Options("attribute lock = (color = (window))");
	HPoint WinColor;
	m_pHView->GetWindowColor(WinColor);
	HC_Set_Color_By_Value("windows", "RGB", WinColor.x, WinColor.y, WinColor.z);
	HC_Set_Rendering_Options(hsra_options);
	HC_Set_Driver_Options (buffer);
	HC_Include_Segment_By_Key (m_pHView->GetSceneKey());
	HC_Set_Handedness("right");
//	HC_Include_Segment_By_Key (GetSceneKey());
	HC_Close_Segment ();
	
	HC_Update_One_Display(driver_segment);

	// Code below taken from HTiffInternal::ImageToTiff (file HIOUtilityTiff.cpp)
	HPixelRGB *image_row;
	image_row = new HPixelRGB[Width];

	// Copy to IPL98 image
	Img.Alloc(Width,Height,24);
	ipl::TImage* pImage=Img.GetTImagePtr();
	int x,y;
	ipl::CImage::RowIterator24bp It=Img.Row24bp(0);
	for(y=0;y<Height;y++)
	{
		HPixelRGB* pPix=image_row;
		It=Img.Row24bp(y);
		HC_Show_Partial_Image (image_key, 0, y, Width, 1, image_row);
		for(x=0;x<Width;x++)
		{
			*It=ipl::CPalette::CreateRGB(pPix->r,pPix->g,pPix->b);
			++pPix;
			++It;
		}
	}
	
	delete image_row;
	HC_Delete_Segment(driver_segment);
	HC_Delete_Segment(image_segment);
	delete[] image;	
	return true;
}

bool CHoopsControl::HoopsToCImageFast(CImage& Img)
{
	if (m_HoopsToCImageInitalized==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::HoopsToCImageFast() Must call HoopsToCImageInit() first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}

	HC_Update_One_Display(m_driver_segment);

	// Code below taken from HTiffInternal::ImageToTiff (file HIOUtilityTiff.cpp)

	// Copy to IPL98 image
	if ((Img.GetWidth()!=m_ImageWidth) || (Img.GetHeight()!=m_ImageHeight) || (Img.GetBits()!=24))
		Img.Alloc(m_ImageWidth,m_ImageHeight,24);
	ipl::TImage* pImage=Img.GetTImagePtr();
	int x,y;
	ipl::CImage::RowIterator24bp It=Img.Row24bp(0);
	for(y=0;y<m_ImageHeight;y++)
	{
		HPixelRGB* pPix=m_image_row;
		It=Img.Row24bp(y);
		HC_Show_Partial_Image (m_image_key, 0, y, m_ImageWidth, 1, m_image_row);
		for(x=0;x<m_ImageWidth;x++)
		{
			*It=ipl::CPalette::CreateRGB(pPix->r,pPix->g,pPix->b);
			++pPix;
			++It;
		}
	}
	return true;
}

bool CHoopsControl::HoopsToCImageInit()
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::HoopsToCImageInit() This object not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (DimensionsInUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::HoopsToCImageInit() Dimensions not set" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}

	HoopsToCImageReset();
	m_hsra_options=(char*)malloc(1024);
	m_hlr_options=(char*)malloc(1024);
	m_current_hsra=(char*)malloc(1024);
	m_image = new HPixelRGB[m_ImageWidth * m_ImageHeight];
	m_image_row = new HPixelRGB[m_ImageWidth];

	m_image_segment=(char*)malloc(4096);
	m_driver_segment=(char*)malloc(4096);
	sprintf(m_image_segment,"?driver/null/hbaseview_%p", (void*)this);
	sprintf(m_driver_segment,"?driver/image/hbaseview_%p", (void*)this);

	if (m_HoopsToCImageInitalized==true)
	{
		// delete old segment if this method has been called before
		HC_Delete_Segment(m_driver_segment);
	}
	m_HoopsToCImageInitalized=true;

	// find handler for type
	HOutputHandler * handler = HDB::GetHIOManager()->GetOutputHandler("tif");

	HFileOutputResult result = OutputNotHandled;

	/* first find out the relevant options associated with the view */
	HC_Open_Segment_By_Key(m_pHView->GetViewKey());
	HC_Show_One_Net_Rendering_Optio("hidden surface removal algorithm", m_current_hsra);
	
	HRenderMode rndrmode = m_pHView->GetRenderMode();
	if (rndrmode == HRenderHiddenLine || rndrmode == HRenderHiddenLineFast)
	{
		
		HC_Show_One_Net_Rendering_Optio("hidden line removal options", m_hlr_options);
		sprintf(m_hsra_options, "hsra = hidden line, hidden line removal options = (%s)", m_hlr_options);
	}
	else
	{
		
		if(handler->GetOutputDefaultHSRA())
			sprintf(m_hsra_options, "hsra = %s", handler->GetOutputDefaultHSRA());
		else
			sprintf(m_hsra_options, "hsra = szb, technology = software frame buffer");
	}
	HC_Close_Segment();

	// prepare image for rendering
	HC_Open_Segment(m_image_segment);
	
	m_image_key = HC_KInsert_Image(0.0, 0.0, 0.0, "rgb", m_ImageWidth, m_ImageHeight, m_image);
	HC_Close_Segment();
	
	// prepare data to render
	HC_Open_Segment(m_driver_segment);
	char buffer[4096];
	sprintf (buffer, "use window id = %s%p, subscreen = (-1, 1, -1, 1)", H_EXTRA_POINTER_FORMAT, (void*)m_image_key);
	HC_Set_Rendering_Options("attribute lock = (color = (window))");
	HPoint WinColor;
	m_pHView->GetWindowColor(WinColor);
	HC_Set_Color_By_Value("windows", "RGB", WinColor.x, WinColor.y, WinColor.z);
	HC_Set_Rendering_Options(m_hsra_options);
	HC_Set_Driver_Options (buffer);
	HC_Set_Handedness("right");
	HC_Include_Segment_By_Key (m_pHView->GetSceneKey());
	HC_Close_Segment ();
	return true;
}

double CHoopsControl::GetVersion() const
{
	return m_Version;
}


/* Note: since this method uses m_FocalFactor as adjustment variable, calling SetCameraPose()
   always results in a view with the new focal length to be tested! */
bool CHoopsControl::CalibrateVirtualFocalLength(CImage& CalibImg, const string& WorldCoordFile, 
												const string& ThresholdFile)
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::CalibrateVirtualFocalLength() This class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	unsigned int Width,Height;
	// Get reference mark 2D positions in ideal image
	CCorresponding3D2DPoints PointSets;
	TCameraParameters Par;
	if (GetIdealPoints(PointSets,Par,Width,Height,CalibImg, WorldCoordFile, ThresholdFile)==false)
	{
		return false;
	}

	// iterate focal length values to find best match
	ostringstream ost;
	CImage Img;
	unsigned int Count=0;
	double Step=0.02; // if not all points are found in beginning step upper and lower bound with this value
	double Upper=1.2;
	double Lower=0.8;
	double UpperError,LowerError, MaxError;

	bool Continue=true;
	m_FocalFactorInUse=true; // make sure SetCameraPose uses the m_FocalFactor to scale with
	// start with lower bound
	m_FocalFactor=Lower;
	while(Continue==true)
	{
		SetCameraPose(Par,Width,Height);
		HoopsToCImage(Img,Width,Height);
		if (GetErrors(PointSets,Img,LowerError,MaxError)==false)
		{
			Lower+=Step;
			Continue=true;
			if (Lower>Upper)
				return false;
		}
		else
			Continue=false;
	}
	Continue=true;
	// then upper bound
	while(Continue==true)
	{
		m_FocalFactor=Upper;
		SetCameraPose(Par,Width,Height);
		HoopsToCImage(Img,Width,Height);
		if (GetErrors(PointSets,Img,UpperError,MaxError)==false)
		{
			Upper-=Step;
			Continue=true;
			if (Upper<Lower)
				return false;
		}
		else
			Continue=false;
	}

	ost.str("");
	ost << "Start conditions: LowerFactor=" << Lower << " UpperFactor=" << Upper << endl
		<< "     LowerError=" << LowerError << " UpperError=" << UpperError << endl;
	ost << "Searching for a bracketed minimum" << endl;
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	// iterate for best value
	double NewError,FinalError;
	while(Count<20)
	{
		ost.str("");
		ost << "Iteration " << Count << ":" << endl;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		m_FocalFactor=(Upper+Lower)/2;
		SetCameraPose(Par,Width,Height);
		HoopsToCImage(Img,Width,Height);
		if (GetErrors(PointSets,Img,NewError,MaxError)==false)
			return false;
		if ((NewError>LowerError) && (NewError<UpperError))
		{
			Upper=m_FocalFactor;
			UpperError=NewError;
		}
		else if ((NewError<LowerError) && (NewError>UpperError))
		{
			Lower=m_FocalFactor;
			LowerError=NewError;
		}
		else
		{
			/* if we end up here, we have a bracketed minimum, 
				that is: NewError<LowError and NewError<UpperError */
			double Tolerance=1e-5;
			ost.str("");
			ost << "Found bracked minimum - starting golden search" << endl;
			m_pDoc->m_History.push_back(ost.str());
			m_pDoc->UpdateViewsNow();
            FinalError=CalibGoldenSearch(Lower,m_FocalFactor,Upper,Tolerance,&m_FocalFactor,
					  PointSets,Par,Width,Height);
			break;
		}
		ost.str("");
		ost << "LowerFactor=" << Lower << " UpperFactor=" << Upper << endl
			<< "     LowerError=" << LowerError << " UpperError=" << UpperError << endl;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		Count++;
	}

	SetCameraPose(Par,Width,Height);
	HoopsToCImage(Img,Width,Height);
	Img.Save("d:/temp/test_cad.bmp");
	m_FocalFactorInUse=true;

	ost.str("");
	ost << "Results:    Best factor=" << m_FocalFactor << "   Mean error=" << FinalError << endl << endl;
	ost << "Ideal image saved as: d:/temp/test_ideal.bmp" << endl 
		<< "Cad   image saved as: d:/temp/test_cad.bmp" << endl;
	ost << "Choose 'SCAPE | Save configuration file' in menu bar to save new configuration file!" << endl;
	ost << "Finished" << endl;
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	return true;
}


bool CHoopsControl::TestVirtualVirtual(const TCameraParameters& Par, const CCorresponding3D2DPoints& PointSets,
									   unsigned int Width, unsigned int Height)
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::CalibrateVirtualFocalLength() This class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}

	// iterate focal length values to find best match
	ostringstream ost;
	CImage Img;
	unsigned int Count=0;
	double Step=0.02; // if not all points are found in beginning step upper and lower bound with this value
	double Upper=1.2;
	double Lower=0.8;
	double UpperError,LowerError, MaxError;
	bool Continue=true;
	m_FocalFactorInUse=true; // make sure SetCameraPose uses the m_FocalFactor to scale with
	// start with lower bound
	m_FocalFactor=Lower;
	while(Continue==true)
	{
		SetCameraPose(Par,Width,Height);
		HoopsToCImage(Img,Width,Height);
		if (GetErrors(PointSets,Img,LowerError,MaxError)==false)
		{
			Lower+=Step;
			Continue=true;
			if (Lower>Upper)
				return false;
		}
		else
			Continue=false;
	}
	Continue=true;
	// then upper bound
	while(Continue==true)
	{
		m_FocalFactor=Upper;
		SetCameraPose(Par,Width,Height);
		HoopsToCImage(Img,Width,Height);
		if (GetErrors(PointSets,Img,UpperError,MaxError)==false)
		{
			Upper-=Step;
			Continue=true;
			if (Upper<Lower)
				return false;
		}
		else
			Continue=false;
	}

	ost.str("");
	ost << "Start conditions: LowerFactor=" << Lower << " UpperFactor=" << Upper << endl
		<< "     LowerError=" << LowerError << " UpperError=" << UpperError << endl;
	ost << "Searching for a bracketed minimum" << endl;
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	// iterate for best value
	double NewError,FinalError;
	while(Count<20)
	{
		ost.str("");
		ost << "Iteration " << Count << ":" << endl;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		m_FocalFactor=(Upper+Lower)/2;
		SetCameraPose(Par,Width,Height);
		HoopsToCImage(Img,Width,Height);
		if (GetErrors(PointSets,Img,NewError,MaxError)==false)
			return false;
		if ((NewError>LowerError) && (NewError<UpperError))
		{
			Upper=m_FocalFactor;
			UpperError=NewError;
		}
		else if ((NewError<LowerError) && (NewError>UpperError))
		{
			Lower=m_FocalFactor;
			LowerError=NewError;
		}
		else
		{
			/* if we end up here, we have a bracketed minimum, 
				that is: NewError<LowError and NewError<UpperError */
			double Tolerance=1e-6;
			ost.str("");
			ost << "Found bracked minimum - starting golden search" << endl;
			m_pDoc->m_History.push_back(ost.str());
			m_pDoc->UpdateViewsNow();
            FinalError=CalibGoldenSearch(Lower,m_FocalFactor,Upper,Tolerance,&m_FocalFactor,
					  PointSets,Par,Width,Height);
			break;
		}
		ost.str("");
		ost << "LowerFactor=" << Lower << " UpperFactor=" << Upper << endl
			<< "     LowerError=" << LowerError << " UpperError=" << UpperError << endl;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		Count++;
	}

	SetCameraPose(Par,Width,Height);
	HoopsToCImage(Img,Width,Height);
	Img.Save("d:/temp/test_cad.bmp");
	m_FocalFactorInUse=true;

	ost.str("");
	ost << "Results:    Best factor=" << m_FocalFactor << "   Mean error=" << FinalError << endl << endl;
	ost << "Ideal image saved as: d:/temp/test_ideal.bmp" << endl 
		<< "Cad   image saved as: d:/temp/test_cad.bmp" << endl;
	ost << "Choose 'SCAPE | Save configuration file' in menu bar to save new configuration file!" << endl;
	ost << "Finished" << endl;
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	return true;
}



#define HOOPS_CALIB_R 0.61803399
#define HOOPS_CALIB_C (1.0-HOOPS_CALIB_R)
#define HOOPS_CALIB_SHFT2(a,b,c) (a)=(b);(b)=(c);
#define HOOPS_CALIB_SHFT3(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
double CHoopsControl::CalibGoldenSearch(double ax,double bx,double cx,double tol,double *xmin,
						  const CCorresponding3D2DPoints& PointSets, const TCameraParameters& Par,
						  unsigned int Width, unsigned int Height)
{
	ostringstream ost;
	unsigned int Count=1;
	ost << "Golden search iteration: ";
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	CImage Img;
	double f1,f2,x0,x1,x2,x3,MaxError;
	double Val;
	x0=ax;
	x3=cx;
	if (fabs(cx-bx)>fabs(bx-ax))
	{
		x1=bx;
		x2=bx+HOOPS_CALIB_C*(cx-bx);
	}
	else
	{
		x2=bx;
		x1=bx-HOOPS_CALIB_C*(bx-ax);
	}
	m_FocalFactor=x1;
	SetCameraPose(Par,Width,Height);
	HoopsToCImage(Img,Width,Height);
	if (GetErrors(PointSets,Img,f1,MaxError)==false)
		return false;

	m_FocalFactor=x2;
	SetCameraPose(Par,Width,Height);
	HoopsToCImage(Img,Width,Height);
	if (GetErrors(PointSets,Img,f2,MaxError)==false)
		return false;
	while (fabs(x3-x0) > tol*(fabs(x1)+fabs(x2)))
	{
		ost.str("");
		ost << "Iteration " << Count++ << " ";
		if (f2<f1)
		{
			HOOPS_CALIB_SHFT3(x0,x1,x2,HOOPS_CALIB_R*x1+HOOPS_CALIB_C*x3)

			m_FocalFactor=x2;
			SetCameraPose(Par,Width,Height);
			HoopsToCImage(Img,Width,Height);
			if (GetErrors(PointSets,Img,Val,MaxError)==false)
				return false;
			HOOPS_CALIB_SHFT2(f1,f2,Val)
		}
		else
		{
			HOOPS_CALIB_SHFT3(x3,x2,x1,HOOPS_CALIB_R*x2+HOOPS_CALIB_C*x0)

			m_FocalFactor=x1;
			SetCameraPose(Par,Width,Height);
			HoopsToCImage(Img,Width,Height);
			if (GetErrors(PointSets,Img,Val,MaxError)==false)
				return false;
			HOOPS_CALIB_SHFT2(f2,f1,Val)
		}
		ost << " Focal factor=" << m_FocalFactor << " Mean error=" << Val << " Max error=" << MaxError;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
	}
	ost.str("");
	ost << endl;
	m_pDoc->m_History.push_back(ost.str());
	if (f1<f2)
	{
		*xmin=x1;
		return f1;
	}
	else
	{
		*xmin=x2;
		return f2;
	}
}

bool CHoopsControl::GetErrorsFocalFactor(CImage& CalibImg, const string& WorldCoordFile, 
												const string& ThresholdFile, double FocalFactor)
{
	if (InUse()==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::CalibrateVirtualFocalLength() This class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	unsigned int Width,Height;
	// Get reference mark 2D positions in ideal image
	CCorresponding3D2DPoints PointSets;
	TCameraParameters Par;
	if (GetIdealPoints(PointSets,Par,Width,Height,CalibImg, WorldCoordFile, ThresholdFile)==false)
	{
		return false;
	}

	m_FocalFactorInUse=true;
	m_FocalFactor=FocalFactor;
	// iterate focal length values to find best match
	CImage Img;
	double Error,MaxError;
	SetCameraPose(Par,Width,Height);
	HoopsToCImage(Img,Width,Height);
	if (GetErrors(PointSets,Img,Error,MaxError)==false)
	{
		m_FocalFactorInUse=false;
		return false;
	}
	else
	{
		ostringstream ost;
		ost << " Focal factor=" << m_FocalFactor << " Mean error=" << Error << " Max error=" << MaxError;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		m_FocalFactorInUse=false;
		return true;
	}
}

bool CHoopsControl::GetErrors(const CCorresponding3D2DPoints& PointSets,const CImage& Img,double &MeanError, double& MaxError)
{
	CImage Img2(Img);
	Img2.CopyConvert(8,Img2);
	CPixelGroups Grps;
	CSegmentate::DeriveBlobs(Img2,Grps,ipl::HIGHCOLOR,10,ipl::EIGHTCONNECTED);
	unsigned int TotalGrps=Grps.GetTotalGroups();
	if (TotalGrps!=PointSets.GetTotalPointSetsInUse())
	{
		ostringstream ost;
		ost << "CHoopsControl::GetErrors() Found " << TotalGrps << " groups but there are only"
			<< PointSets.GetTotalPointSetsInUse() << " point sets in use" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	int x,y;
	vector<CPoint2D<FLOAT32> > CM;
	for(x=0; x<TotalGrps; x++)
	{
		CPoint2D<FLOAT32> Pnt=CFeatures::DeriveCenterOfMassBinary(Grps[x]);
		CM.push_back(Pnt);
	}
	// Calculate mean error
	double Error=0;
	MaxError=0;
	for(x=0; x<TotalGrps; x++)
	{
		CPoint2D<FLOAT32> Pnt1=PointSets.GetPoint2D(x);
		// use the closest point to measure error from
		double TempError=10000;
		for(y=0; y<TotalGrps; y++)
		{
			CPoint2D<FLOAT32> Pnt2=CM[y];
			double Dist=Pnt1.GetDist(Pnt2);
			if (Dist<TempError)
				TempError=Dist;
		}
		Error+=TempError;
		if (TempError>MaxError)
			MaxError=TempError;
	}
	MeanError = Error/TotalGrps;
	return true;
}

bool CHoopsControl::GetIdealPoints(CCorresponding3D2DPoints& PointSets, TCameraParameters& CamParams,
								   unsigned int& Width, unsigned int& Height, CImage& CalibImg, 
								   const string& WorldCoordFile, const string& ThresholdFile)
{
	// Calibrating image
	ostringstream ost;
	ost << "Calibrating real image...";
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	CMarkRetrieval RefMarks;
	if (RefMarks.ReadWorldCoordinates(WorldCoordFile.c_str())==false)
	{
		ostringstream ost;
		ost << "CHoopsControl::GetIdealPoints() Failed reading world coordinates" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	CCameraModel Cam(CalibImg.GetWidth(),CalibImg.GetHeight());
	if ((RefMarks.FindMarksAdaptive(CalibImg, ThresholdFile, CMarkRetrieval::REFMARK_BIG,false)==true) &&
		(Cam.CalibrateWithRadialDist(RefMarks.m_PointSets)==true))
	{
		ost.str("");
		ost << "Found reference marks in original image at: " << endl; 
		ost << RefMarks.m_PointSets << endl;
		ost << "Camera parameters: " << endl;
		ost << "k=" << Cam.GetRadDistParameter() << endl;
		Cam.CamParametersToStream(ost);
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
	}
	else
	{
		ost.str("");
		ost << "Failed calibrating real image" << endl; 
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		return false;
	}

	// Creating ideal image
	CImage IdealImg;
	ost.str("");
	ost << "Creating ideal image";
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	Cam.CreateIdealImage(IdealImg,CalibImg,false);
	ost.str("");
	ost << "Ideal image info:" << endl;
	ost << IdealImg << endl;
	ost << endl << "Finding reference marks 2D positions in ideal image...";
	m_pDoc->m_History.push_back(ost.str());
	m_pDoc->UpdateViewsNow();
	// set return values
	Width=IdealImg.GetWidth();
	Height=IdealImg.GetHeight();
	CamParams=Cam.m_Par;
	CamParams.p=0;
	CamParams.beta=1;
	CamParams.FocalLength=Cam.GetFocalLengthIdeal();
	CamParams.f=1/CamParams.FocalLength;
	CamParams.xh=Width/2.0; // shouldn't really use this value, doesn't make sence
	CamParams.yh=Height/2.0; // shouldn't really use this value, doesn't make sence
	// Save ideal image - just for debug purposes
	IdealImg.Save("d:/temp/test_ideal.bmp");
	// Finding reference marks in ideal image
	string Path,FileNameExt,Ext;
	ipl::SplitFileName(ThresholdFile.c_str(),Path,FileNameExt,Ext);
	ostringstream IdealThresholdFile;
	IdealThresholdFile << Path << "threshold_list_ideal." << Ext;
	IdealImg.SetOrigo(0,0);
	RefMarks.m_PointSets.SetAll2DPointsToNotUsed();
	if (RefMarks.FindMarksAdaptive(IdealImg, IdealThresholdFile.str(), CMarkRetrieval::REFMARK_BIG, false)==true)
	{
		PointSets=RefMarks.m_PointSets;
		ost.str("");
		ost << "Found reference marks in ideal image at (origo reset to (0,0)): " << endl; 
		ost << RefMarks.m_PointSets << endl;
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
	}
	else
	{
		ost.str("");
		ost << "Failed finding reference marks in ideal image" << endl; 
		m_pDoc->m_History.push_back(ost.str());
		m_pDoc->UpdateViewsNow();
		return false;
	}
////	Cam.SetImageDimensions(IdealImg.GetWidth(),IdealImg.GetHeight());
//	if (Cam.CalibrateWithRadialDist(RefMarks.m_PointSets)==true)
//	{
//		CamParams=Cam.m_Par;
//		ost.str("");
//		ost << "Calibration of ideal image:" << endl;
//		ost << "Camera parameters: " << endl;
//		ost << "k=" << Cam.GetRadDistParameter() << endl;
//		Cam.CamParametersToStream(ost);
//		m_pDoc->m_History.push_back(ost.str());
//		m_pDoc->UpdateViewsNow();
//	}
//	else
//		return false;
	return true;
}

ostream& operator<<(ostream& os, const CHoopsControl& HoopsCtrl) throw(string)
{
	os << "# CHoopsControl setup file by edr@mip.sdu.dk" << endl << endl;
	os << "CHoopsControlVersion:   " <<  HoopsCtrl.GetVersion() << endl << endl;
	os << "# Image size. If Dimensions not in use, Width and Height should not be present" << endl;
	os << "DimensionsInUse:       " << HoopsCtrl.m_DimensionsInUse << endl;
	if (HoopsCtrl.m_DimensionsInUse==true)
	{
		os << "Width:       " << HoopsCtrl.m_Width << endl;
		os << "Height:      " << HoopsCtrl.m_Height << endl << endl;
	}
	os << "# Factor between real focal length and the virtual one - a calibration" << endl
	   << "# which for some reason is needed to make correspondance between real and virtual world" << endl
	   << "# If FocalFactor not in use, FocalFactor should not be present" << endl;
	os << "FocalFactorInUse: " << HoopsCtrl.m_FocalFactorInUse << endl;
	if (HoopsCtrl.m_FocalFactorInUse==true)
		os << "FocalFactor: " << HoopsCtrl.m_FocalFactor << endl << endl;

	return os;
}

istream& operator>>(istream& is, CHoopsControl& HoopsCtrl) throw(string)
{
	string Token;
	ipl::SkipSpaceAndComments(is);
	// read 'CHoopsControlVersion' token
	is >> Token;
	if (Token!=string("CHoopsControlVersion:"))
	{
		ostringstream ost;
		ost << "operator>>(istream, CHoopsControl) Token CHoopsControlVersion not found"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		throw(ost.str());
		return is;
	}
	double Version;
	is >> Version;
	if (Version!=HoopsCtrl.GetVersion())
	{
		ostringstream ost;
		ost << "operator>>(istream, CHoopsControl) File version is " << Version << " but this class "
			" versions is " << HoopsCtrl.GetVersion() << " - trying to read rest of file anyway" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
	}
	try
	{
		char* pStr = new char[300];
		string Token;
		ipl::SkipSpaceAndComments(is);
		// skip 'DimensionsInUse:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> HoopsCtrl.m_DimensionsInUse;
		if (HoopsCtrl.m_DimensionsInUse==true)
		{
			ipl::SkipSpaceAndComments(is);
			// skip 'Width:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> HoopsCtrl.m_Width;
			ipl::SkipSpaceAndComments(is);
			// skip 'Height:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> HoopsCtrl.m_Height;
		}
		ipl::SkipSpaceAndComments(is);
		// skip 'FocalFactorInUse:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> HoopsCtrl.m_FocalFactorInUse;
		if (HoopsCtrl.m_FocalFactorInUse==true)
		{
			ipl::SkipSpaceAndComments(is);
			// skip 'FocalFactor:' token
			is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
			is >> HoopsCtrl.m_FocalFactor;
		}
	}
	catch(...) // all exceptions
	{
		throw("Failed reading from data file containing a CHoopsControl object");
	}
	return is;
}

} // end namespace videndo
