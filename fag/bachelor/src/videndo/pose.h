#ifndef _VIDENDO_POSE_H
#define _VIDENDO_POSE_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <sstream>
#include <points/point3d.h>
#include <ipl98/cpp/vectors/vector3d.h>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/error.h>
#include <cmath>
#include <ios>
#include <string>
#include <ipl98/cpp/error.h>

namespace videndo{ // use namespace if C++

using std::ostringstream;
using ipl::CError;
using ipl::IPL_WARNING;
using ipl::CVector3D;

/** This template class can use and transform matrices
	describing spatial 3D rotations and translations.
	The template type should be a floating point type,
	such as float or double.
	The tranformation of the coordinates of a point P is:
	\verbatim

	PLocal=m_R*(PGlobal-LocalOrigin)

	PGlobal=m_R^(-1)*PLocal+LocalOrigin

	\endverbatim

	PLocal and PGlobal are the coordinates of P expressed in the local
	and global frame, respectively.

	The variable m_R is a 3x3 matrix refering to XYZ coordinate systems
	LocalOrigin is the position vector of the origin
	of the local origin in expressed in the global frame.

	The variables m_R and LocalOrigin (m_LocalOrigin) are member variables 
	of the class.

	Attribute m_R can be considered as a product of 3 rotations of the global frame 
	so that it has axes parallel to those of the local frame.
	If (AxisOrderIndex ==0) the order of the rotations is:
	\verbatim
	
	(Rotate the angle -a about the x-axis (in the global frame)),
	(Rotate the angle -b about the y-axis in new frame),
	(Rotate the angle -c about the z-axis in new frame)
	\endverbatim
	If (AxisOrderIndex !=0) the order of the rotations is:
	\verbatim
	
	(Rotate the angle -a about the z-axis in global frame),
	(Rotate the angle -b about the y-axis in new frame),
	(Rotate the angle -c about the x-axis in new frame)
	\endverbatim
	The angles a, b and c defines the rotation and can be derived 
	from the rotation matrix m_R, provided that the rotation order is known.
	A safe way of finding the correspondence between a,b and c and the angles
	of other notations in robotics and computer vision (tilt,pan,roll, A,B,C, 
	or Roll,Pitch,Yaw) is to compare the matrix m_R or m_R^(-1)	
	expressed by the angles. For example the ABC-notation of the 
	robot manufacturer Reis coorsponds to A=c,B=-b and C=a and
	axis order index 1. The relevant expressions is found in the method SetAngularMatrix.
	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CPose videndo/pose.h
	@version 0.97
	@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
template<class T>
class CPose
{
protected:
	/** Reprents to set state of the matrix and the
		displacement vector. */
	bool m_Empty;
	
	/** Represents the 3x3 rotation matrix */
	T m_R[3][3];
	
	/** Represents the displacemente vector in the old frame. */
	CPoint3D<T> m_LocalOrigin;
	
public:
	/** Constructs object with a default identity matrix (diagonal elements are 1) and origin to (0,0,0)
		and sets m_Empty to true */
	CPose();
	
	/** Constructs a new instance and copy Pose into it. */
	CPose(const CPose &Pose);
	
	/** Constructs an instance and assigns datamember m_R and
		m_LocalOrigin according to the input
		@param a Is the angle difining the a-rotation
		@param b Is the angle defining the b rotation
		@param c Is the angle defining the c rotation
		@param LocalOrigin Is assigned to the member variable m_LocalOrigin
		@param AxisOrderIndex Determines the order of the rotations
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	CPose(T a,T b,T c,CPoint3D<T> &LocalOrigin,int AxisOrderIndex);

	/** Constructs an instance and assigns datamember m_R according to
	    input. m_LocalOrigin is set to zero
		@param a Is the angle difining the a-rotation
		@param b Is the angle defining the b rotation
		@param c Is the angle defining the c rotation
		@param AxisOrderIndex Determines the order of the rotations
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	CPose(T a,T b,T c,int AxisOrderIndex);


	/** Returns the state of the matrix and the displacement vector.
		@returns False if not initialized by a previous call to copy 
			constructor or SetMatrix() */
	inline bool IsEmpty() const;

	/** Resets previous information about pose information contained in 
		this class. That is, the copy constructor or SetMatrix() must be
		called to make this class functional again. */
	inline void Empty();

	/** Standard operator 'equal to'.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	CPose& operator = (const CPose &Pose);

	/** Assigns datamember m_R and m_LocalOrigin according to the instance.
		@param a Is the angle defining the a-rotation
		@param b Is the angle defining the b rotation
		@param c Is the angle defining the c rotation
		@param LocalOrigin Is assigned to the member variable m_LocalOrigin
		@param AxisOrderIndex Determines the order of the rotations
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	void SetMatrix(T a,T b,T c,CPoint3D<T> &LocalOrigin,int AxisOrderIndex);

		/** Assigns datamember m_R to the instance. m_LocalOrigin is unchanged.
		@param a Is the angle defining the a-rotation
		@param b Is the angle defining the b rotation
		@param c Is the angle defining the c rotation
		@param AxisOrderIndex Determines the order of the rotations
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline void SetAngularMatrix(T a,T b,T c,int AxisOrderIndex);
	
    /**	Assigns datamember m_R and m_LocalOrigin according to the input. The input
		LocalX and LocalY are the first two row vectors in m_R
		@param LocalX Is a vector along the x-axis in local frame expressed in the global frame.
		@param LocalY Is a vector along the y-axis in local frame expressed in the global frame.
		@param LocalOrigin Is assigned to the member variable m_LocalOrigin
		@return False if any of the direction vectors are zero or not perpendicular
			to each other within 1e-7 radians
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool SetMatrix(const CVector3D<T>& LocalX,const CVector3D<T>&LocalY,
							const CPoint3D<T> &LocalOrigin);
	
	/** Returns the translational vector, i.e. the local origin expressed in the global frame
		@return The new origin (of frame B) expressed in the old frame (A).
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk)*/
	inline const CPoint3D<T>& GetTranslationalPose() const;
	
	/** Calculates the three rotation angles a, b, c. If b
		is + or - pi/2, then c is set to zero and a is calculated to
		give the correct matrix.
		@param a Is the angle defining the a-rotation in the interval -pi to pi
		@param b Is the angle defining the b rotation in the interval -pi/2 to pi/2
		@param c Is the angle defining the c rotation in the interval -pi to pi
		@param AxisOrderIndex determines the order of the rotations.
		@return False if the direction vectors are zero or not perpenticular
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline void DeriveAngularPose(T &a,T &b,T &c,int AxisOrderIndex) const;
	
	/** Calculates the three rotation angles a, b, c. If b is
		+ or - pi/2, then c is set to zero and a is calculated to give
		the correct matrix.
		@param a (Pan) Is the angle defining the a-rotation in the interval -pi to pi
		@param b (Tilt) Is the angle defining the b rotation in the interval pi/2 to 3pi/2.
		@param c Is the angle defining the c rotation in the interval -pi to pi
		@param AxisOrderIndex determines the order of the rotations.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline void DeriveAltAngularPose(T &a,T &b,T &c, int AxisOrderIndex) const;
	
	/** Calculates the unit vectors in the directions of the local frame expressed in the
		old frame. These vectors are row vectors of m_R
		@param X is the unit vector in the direction of the x-axis of local frame
		@param Y is the unit vector in the direction of the y-axis of local frame
		@param Z is the unit vector in the direction of the z-axis of local frame
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline void GetLocalDirections(CVector3D<T> &X,CVector3D<T> &Y, CVector3D<T> &Z) const;
	
	/** Calculates the unit vectors in the directions of the global frame
		expressed in the local frame. These vectors are column vectors of m_R
		@param X is the unit vector in the direction of the x-axis of global frame
		@param Y is the unit vector in the direction of the y-axis of global frame
		@param Z is the unit vector in the direction of the z-axis of global frame
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline void GetGlobalDirections(CVector3D<T> &X,CVector3D<T> &Y, CVector3D<T> &Z) const;
	
	/** Transforms all member variables so that the inverse transformation
		is obtained.
		@return The inverted transformation.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline CPose GetInverted() const;

	/** Transforms the angular member variables so that the inverse 
	    angular transformation is obtained. 
		In this operation, m_LocalOrigin is set to (0,0,0).
		@return The inverted transformation.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline CPose GetAngularInverted() const;
	
	/** Transforms a point from global to local coordinates.
		@param Point Is the point to be transformed.
		@return The transformed point.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	CPoint3D<T> TransformToLocal(const CPoint3D<T> & Point) const;
	
	/** Transforms a point from local to global frame coordinates.
		@param Point Is the point to be backtransformed.
		@return The transformed point.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	CPoint3D<T> TransformToGlobal(const CPoint3D<T> &Point) const;

	/** Derives the minimum rotation angle for transforming Pose1 into Pose2
	    where Pose1 and Pose2 are given by the angular poses (a1,b1,c1)
		and (a2,b2,c2), respectively
		@param a1 Is the a-angle of Pose1
        @param b1 Is the b-angle of Pose1
		@param c1 Is the c-angle of Pose1
		@param a2 Is the a-angle of Pose2.
        @param b2 Is the b-angle of Pose2
		@param c2 Is the c-angle of Pose2
		@param AxisOrderIndex Defines the order of rotations.
		@return The minimum angle.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	static T GetAngleChange(T a1,T b1, T c1, T a2,T b2, T c2, int AxisOrderIndex);
	
	/** Returns the elements of the 3x4 matrix defining the local-to-global
	    transformation. The left 3x3 matrix is m_R^(-1), and the 4th column 
		is the local origin.
		@param i Is row index.
        @param j Is the column index.
		@return The matrix element.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline T GetMatrixElementLocalToGlobal(int i,int j) const;
 
	/** Derives the angular pose of the product Pose2.GetInverted()*Pose1
	    where Pose1 and Pose2 are given by the angular poses (a1,b1,c1)
		and (a2,b2,c2), respectively
		@param a Is the a-angle of the resulting pose
        @param b Is the b-angle of the resulting pose
		@param c Is the c-angle of the resulting pose
		@param a1 Is the a-angle of Pose1
        @param b1 Is the b-angle of Pose1
		@param c1 Is the c-angle of Pose1
		@param a2 Is the a-angle of Pose2.
        @param b2 Is the b-angle of Pose2
		@param c2 Is the c-angle of Pose2
		@param AxisOrderIndex Defines the order of rotations.
		@return The minimum angle.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	static void HomographicTransformation(T &a,T &b,T &c,T a1,T b1, T c1,
									T a2,T b2, T c2, int AxisOrderIndex);

	/** Derives the angular pose of the product Pose2.GetInverted()*this
		where Pose2 is given by the angular poses (a2,b2,c2)
		@param a Is the a-angle of the resulting pose
		@param b Is the b-angle of the resulting pose
		@param c Is the c-angle of the resulting pose
		@param a2 Is the a-angle of Pose2.
		@param b2 Is the b-angle of Pose2
		@param c2 Is the c-angle of Pose2
		@param AxisOrderIndex Defines the order of rotations.
		@return The minimum angle.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */	
	inline void HomographicTransformation(T &a,T &b,T &c,T a2,T b2, T c2, int AxisOrderIndex) const;

	/** Calculates the transformation defined by
		two successive transformations. Thus T1*T2
		is the transformation obtained by T2 followed by T1
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	inline CPose operator * (const CPose &Pose);

	/**@name streaming operators */
	//@{
	/** writes object to stream, format is:
		\verbatim
		
		CPose IsEmpty: 1/0 
		Local-to-global matrix:
		x00 x01 x02 x03 
		x10 x11 x12 x13
		x20 x21 x22 x23	

		\endverbatim
		where IsEmpty is either true (1) or false (0).
		If this object is empty, only the first line is written.
		@author Implementation by Ren� Dencker Eriksen (edr@mip.sdu.dk)
		@version 0.2 */
	friend std::ostream& operator<<(std::ostream& s, const CPose& Pose);

	/** Reads input from given stream into this class. Stream must be in correct format
		according to how the << ostream operator formats output.
		@author Implementation by Ren� Dencker Eriksen (edr@mip.sdu.dk)
		@version 0.2 */
	friend std::istream& operator>>(std::istream& is, CPose& Pose);
	//@}
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

template <class T>
inline bool CPose<T>::IsEmpty() const
{
	return m_Empty;
}

template <class T>
inline void CPose<T>::Empty()
{
	m_Empty=true;
}

template <class T>
inline const CPoint3D<T>& CPose<T>::GetTranslationalPose() const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::GetTranslationalPose() This object not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	return m_LocalOrigin;
}

template <class T>
inline void CPose<T>::GetLocalDirections(CVector3D<T> &X,CVector3D<T> &Y, CVector3D<T> &Z) const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::GetLocalDirections() This object not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	X.Set(m_R[0][0],m_R[0][1],m_R[0][2]);
	Y.Set(m_R[1][0],m_R[1][1],m_R[1][2]);
	Z.Set(m_R[2][0],m_R[2][1],m_R[2][2]);
}

template <class T>
inline void CPose<T>::GetGlobalDirections(CVector3D<T> &X,CVector3D<T> &Y, CVector3D<T> &Z) const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::GetGlobalDirections() This object not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	X.Set(m_R[0][0],m_R[1][0],m_R[2][0]);
	Y.Set(m_R[0][1],m_R[1][1],m_R[2][1]);
	Z.Set(m_R[0][2],m_R[1][2],m_R[2][2]);
}

template <class T>
inline CPose<T> CPose<T>::operator * (const CPose<T> &Pose)
{
	if ((Pose.IsEmpty()) || (m_Empty))
	{
		ostringstream ost;
		ost << "CPose<T>::operator*() One of the operands not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	double sum;
	CPose<T> Buf;
	Buf.m_LocalOrigin.Set(
		Pose.m_R[0][0]*m_LocalOrigin.GetX()+
		Pose.m_R[1][0]*m_LocalOrigin.GetY()+
		Pose.m_R[2][0]*m_LocalOrigin.GetZ(),
		Pose.m_R[0][1]*m_LocalOrigin.GetX()+
		Pose.m_R[1][1]*m_LocalOrigin.GetY()+
		Pose.m_R[2][1]*m_LocalOrigin.GetZ(),
		Pose.m_R[0][2]*m_LocalOrigin.GetX()+
		Pose.m_R[1][2]*m_LocalOrigin.GetY()+
		Pose.m_R[2][2]*m_LocalOrigin.GetZ());
	Buf.m_LocalOrigin+=Pose.m_LocalOrigin;
	for (int i=0;i<3;i++)
	{
		for (int j=0;j<3;j++)
		{
			sum=0;
			for (int k=0;k<3;k++)
				sum+=m_R[j][k]*Pose.m_R[k][i];
			Buf.m_R[j][i]=sum;
		}
	}
	Buf.m_Empty=false;
	return Buf;
}
template <class T>
inline void CPose<T>::SetMatrix(T a,T b,T c,CPoint3D<T> &LocalOrigin,int AxisOrderIndex)
{
  SetAngularMatrix(a,b,c,AxisOrderIndex);
  m_LocalOrigin=LocalOrigin;
}

template <class T>
inline CPose<T> CPose<T>::GetInverted() const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::GetInverted() This object not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	CPose Buf;
	T x=m_LocalOrigin.GetX();
	T y=m_LocalOrigin.GetY();
	T z=m_LocalOrigin.GetZ();
	Buf.m_LocalOrigin.SetX(-m_R[0][0]*x-m_R[0][1]*y-m_R[0][2]*z);
	Buf.m_LocalOrigin.SetY(-m_R[1][0]*x-m_R[1][1]*y-m_R[1][2]*z);
	Buf.m_LocalOrigin.SetZ(-m_R[2][0]*x-m_R[2][1]*y-m_R[2][2]*z);
	for (int i=0;i<3;i++)
	{
		for (int j=0;j<3;j++)
			Buf.m_R[i][j]=m_R[j][i];
		Buf.m_Empty=false;
	}
	return Buf;
}

template <class T>
inline void CPose<T>::HomographicTransformation(T &a,T &b,T &c,T a2,T b2, T c2, int AxisOrderIndex) const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::HomographicTransformation() This object not initialized" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return;
	}
	CPose<T> PR(a2,b2,c2,AxisOrderIndex);
	if (AxisOrderIndex==0)
	{
		c =atan2(-PR.m_R[0][1]*m_R[0][0]
			     -PR.m_R[1][1]*m_R[1][0]
				 -PR.m_R[2][1]*m_R[2][0],
				  PR.m_R[0][0]*m_R[0][0]
				 +PR.m_R[1][0]*m_R[1][0]
				 +PR.m_R[2][0]*m_R[2][0]);	
		b=   asin(PR.m_R[0][2]*m_R[0][0]+
			     +PR.m_R[1][2]*m_R[1][0]+
		   	     +PR.m_R[2][2]*m_R[2][0]);
		a =atan2(-PR.m_R[0][2]*m_R[0][1]
				 -PR.m_R[1][2]*m_R[1][1]
				 -PR.m_R[2][2]*m_R[2][1],
			      PR.m_R[0][2]*m_R[0][2]
				 +PR.m_R[1][2]*m_R[1][2]
				 +PR.m_R[2][2]*m_R[2][2]);
	} 
	else
	{
		c = atan2(PR.m_R[0][1]*m_R[0][2]
			     +PR.m_R[1][1]*m_R[1][2]
				 +PR.m_R[2][1]*m_R[2][2],
			      PR.m_R[0][2]*m_R[0][2]
				 +PR.m_R[1][2]*m_R[1][2]
				 +PR.m_R[2][2]*m_R[2][2]);	
		b=  asin(-PR.m_R[0][0]*m_R[0][2]
			     -PR.m_R[1][0]*m_R[1][2]
				 -PR.m_R[2][0]*m_R[2][2]);
		a = atan2(PR.m_R[0][0]*m_R[0][1]
			     +PR.m_R[1][0]*m_R[1][1]
				 +PR.m_R[2][0]*m_R[2][1],
			      PR.m_R[0][0]*m_R[0][0]
				 +PR.m_R[1][0]*m_R[1][0]
				 +PR.m_R[2][0]*m_R[2][0]);
	}
}

template <class T>
inline CPose<T> CPose<T>::GetAngularInverted() const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::GetInverted() This object not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	CPose Buf;
    Buf.m_LocalOrigin.Set(0,0,0);
	for (int i=0;i<3;i++)
	{
		for (int j=0;j<3;j++)
			Buf.m_R[i][j]=m_R[j][i];
		Buf.m_Empty=false;
	}
	return Buf;
}

template <class T>
inline T CPose<T>::GetMatrixElementLocalToGlobal(int i,int j) const
{
	if (j<4)
		return m_R[j][i];
	else if (i==0)
		return LocalOrigin.GetX(); 
	else if (i==1)
		return LocalOrigin.GetY();
	else
		return LocalOrigin.GetZ(); 
}

template <class T>
inline void CPose<T>::DeriveAngularPose(T &a,T &b,
							  T &c, int AxisOrderIndex) const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::DeriveAngularPose() This object not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	if (AxisOrderIndex==0)
	{
		b=asin(m_R[2][0]);
		if (((m_R[2][1]==0)&&(m_R[2][2]==0))||
			((m_R[1][0]==0)&&(m_R[0][0]==0))) //b is + or - pi/2
		{
			c=0;
			a=atan2(m_R[1][2],m_R[1][1]);
		}
		else
		{
			c=atan2(-m_R[1][0],m_R[0][0]);
			a=atan2(-m_R[2][1],m_R[2][2]);
		}
	}
	else
	{
		b=asin(-m_R[0][2]);
		if (((m_R[1][2]==0)&&(m_R[2][2]==0))||
			((m_R[0][1]==0)&&(m_R[0][0]==0))) //b is + or - pi/2
		{
			c=0;
			a=atan2(-m_R[1][0],m_R[1][1]);
		}
		else
		{
			c=atan2(m_R[1][2],m_R[2][2]);
			a=atan2(m_R[0][1],m_R[0][0]);
		}
	}
}

template <class T>
inline void CPose<T>::DeriveAltAngularPose(T &a,T &b,
								 T &c, int AxisOrderIndex) const
{
	if (m_Empty)
	{
		ostringstream ost;
		ost << "CPose<T>::DeriveAltAngularPose() This object not initialized - continuing with default values" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
	}
	if (AxisOrderIndex==0)
	{
		b=ipl::PI-asin(m_R[2][0]);
		if (((m_R[2][1]==0)&&(m_R[2][2]==0))||
			((m_R[1][0]==0)&&(m_R[0][0]==0))) //b is + or - pi/2
		{
			c=0;
			a=atan2(m_R[1][2],m_R[1][1]);
		}
		else
		{
			c=atan2(m_R[1][0],-m_R[0][0]);
			a=atan2(m_R[2][1],-m_R[2][2]);
		}
	}
	else
	{
		b=ipl::PI-asin(-m_R[0][2]);
		if (((m_R[1][2]==0)&&(m_R[2][2]==0))||
			((m_R[0][1]==0)&&(m_R[0][0]==0))) //b is + or - pi/2
		{
			c=0;
			a=atan2(-m_R[0][1],m_R[1][1]);
		}
		else
		{
			c=atan2(-m_R[1][2],-m_R[2][2]);
			a=atan2(-m_R[0][1],-m_R[0][0]);
		}
	}
}
#include "pose.cpp"

} // end namespace videndo


#endif //_VIDENDO_POSE_H
