#include "kernel_mark_retrieval.h"
#include <ipl98/kernel_c/kernel_pixelgroup.h>
#include <ipl98/kernel_c/ipl98_types.h>
#include <ipl98/kernel_c/kernel_pixelgroups.h>
#include <ipl98/kernel_c/algorithms/kernel_feature_extraction.h>
#include <ipl98/kernel_c/algorithms/kernel_segmentate.h>
#include <ipl98/kernel_c/kernel_error.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdarg.h>
#include <assert.h>
#include <string.h>

extern void AddIPL98ErrorHistory(const char* str);

namespace videndo{

using ipl::TPixelGroup;
using ipl::COLORTYPE;
using ipl::TMoment;
using ipl::T2DFloat;
using ipl::T3DFloat;
using ipl::TImage;
using ipl::TString;
using ipl::k_InitImage;
using ipl::k_CopyImage;
using ipl::k_InitString;
using ipl::k_AddCharToString;
using ipl::k_ShowMessage;
using ipl::IPL_ERROR;
using ipl::k_EmptyString;
using ipl::k_EmptyImage;
using ipl::TPixelGroups;
using ipl::k_InitGroups;
using ipl::k_AddCharArrayToString;
using ipl::EMPTY;
using ipl::IPL_WARNING;
using ipl::k_DeriveBlobs;
using ipl::HIGHCOLOR;
using ipl::EIGHTCONNECTED;
using ipl::LOWCOLOR;
using ipl::k_AddColorsToGroups;
using ipl::k_AllocImage;
using ipl::k_FlushImage;
using ipl::k_SetPixelFast;
using ipl::k_Save;
using ipl::k_DeriveMomentsGrayTone;
using ipl::T2DInt;
using ipl::k_PosInGroup;
using ipl::k_EmptyGroups;
using ipl::k_GetIndexIn3D2DPoints;
using ipl::k_SetPoint2DIn3D2DPoints;
using ipl::k_Empty3D2DPoints;
using ipl::k_AddPoint3DAndIdTo3D2DPoints;
using ipl::k_SetPixel;
using ipl::k_SplitFileName;
using ipl::k_SkipWhiteSpace;
using ipl::k_SkipLine;
using ipl::k_ReadToken;
using ipl::k_StringToUpper;
using ipl::k_Alloc3D2DPoints;
using ipl::k_CopySubImage;


/********************************************************/
/********    Private functions and structures    ********/
/********************************************************/

/** @name Miscellaneous global variables */
/*@{ */
/** Used by k_FindMarksAutoGlobalThreshold() and k_FindMarksAutoGlobalThreshold(). Defines
	the stepsize to adjust threshold with in each iteration. Normal value: 5 */
const unsigned int GLOBAL_CALIB_THRESH_STEP=5;
/** Upper boundary for threshold to stop at for iteration loop. Normal value: 230 */
const unsigned int GLOBAL_CALIB_THRESH_UPPER_BOUND=230;
/** Upper boundary for threshold to stop at for iteration loop. Normal value: 25 */
const unsigned int GLOBAL_CALIB_THRESH_LOWER_BOUND=10;
/*@} */

typedef struct {
	TPixelGroup* pPixelGroup;
	COLORTYPE Color;
	TMoment Moment;
	T2DFloat Center;
} TLTDetectedBlob;

typedef struct {
	TLTDetectedBlob* pCC0;
	TLTDetectedBlob* pCC1;
	TLTDetectedBlob* pCC2;
	unsigned int Id;
} TLTDetectedRefMark;

void k_IniTLTDetectedRefMark(TLTDetectedRefMark* DetectedRefMark)
{
	DetectedRefMark->pCC0=NULL;
	DetectedRefMark->pCC1=NULL;
	DetectedRefMark->pCC2=NULL;
	DetectedRefMark->Id=0;
}

unsigned int k_LTFindCalibrationMarksAndMatchIds(TImage* pImg,T3D2DPoints* pPointSets,TDirection* pDirs, unsigned int Threshold,
												  TImage* pPlotImg, unsigned int Verbose, unsigned int MinBlobSize);

/************************************************/
/********        Public functions        ********/
/************************************************/

/***********************************************************************************/
/********************* Method for detecting reference marks  ***********************/
/**********************This method finds the three circles and**********************/
/**********************locally finds a threshold from the intensities***************/
/**********************found on the three circles***********************************/
/***********************************************************************************/
bool k_FindMarksAutoLocalThreshold(TImage* pOriginalImg, T3D2DPoints* pPointSets, TDirection* pDirs,
								   unsigned short BeginThreshold, unsigned int Verbose)
{
	unsigned int TotalRefMarks=0,FoundRefMarks;
	bool ReturnValue=true;
	bool FoundAllRefMarks=false;
	TString str;
	TImage TempImg;
	unsigned short up,down;
	k_InitImage(&TempImg);
	k_InitString(&str);
	if (Verbose!=0)
		k_RefMarkPrintf("**************** k_FindMarksAutoLocalThreshold ******************\n");
	/* checking the image type*/
	if (pOriginalImg->Bits!=8)
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"k_FindMarksAutoLocalThreshold() Image must be 8 b/p (current is %d b/p)",pOriginalImg->Bits);
		k_EmptyString(&str);
		return false;
	}
	if (pOriginalImg->Origin==EMPTY)
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_WARNING,&str,"k_FindMarksAutoLocalThreshold() Source image is empty - doing nothing");
		k_EmptyString(&str);
		return false;
	}
	k_CopyImage(&TempImg,pOriginalImg);
	/* find all refmarks by iterating through all threshold values */
	up=BeginThreshold;
	down=(unsigned short)(BeginThreshold-GLOBAL_CALIB_THRESH_STEP);
	while (((up<GLOBAL_CALIB_THRESH_UPPER_BOUND) || (down>GLOBAL_CALIB_THRESH_LOWER_BOUND)) && (FoundAllRefMarks==false))
	{
		/* trying upward threshold */
		if (up<GLOBAL_CALIB_THRESH_UPPER_BOUND)
		{
			if (Verbose!=0)
				k_RefMarkPrintf(" **** Iteration (upwards) with Threshold=%d\n",up);
			/* Finding refmarks with given threshold "up" */
			FoundRefMarks=k_LTFindCalibrationMarksAndMatchIds(&TempImg,pPointSets,pDirs,up,pOriginalImg,Verbose,0);
			TotalRefMarks+=FoundRefMarks;
			if (TotalRefMarks==pPointSets->NumberOfSets)
				FoundAllRefMarks=true;
		}
		/* trying downward threshold */
		if ((FoundAllRefMarks==false) && (down>GLOBAL_CALIB_THRESH_LOWER_BOUND))
		{
			if (Verbose!=0)
				k_RefMarkPrintf(" **** Iteration (downwards) with Threshold=%d\n",down);
			/* Finding refmarks with given threshold "down" */
			FoundRefMarks=k_LTFindCalibrationMarksAndMatchIds(&TempImg,pPointSets,pDirs,down,pOriginalImg,Verbose,0);
			TotalRefMarks+=FoundRefMarks;
			if (TotalRefMarks==pPointSets->NumberOfSets)
				FoundAllRefMarks=true;
		}
		if (up<GLOBAL_CALIB_THRESH_UPPER_BOUND)
			up+=GLOBAL_CALIB_THRESH_STEP;
		if (down>GLOBAL_CALIB_THRESH_LOWER_BOUND)
			down-=GLOBAL_CALIB_THRESH_STEP;
	}
	if (TotalRefMarks!=pPointSets->NumberOfSets)
		ReturnValue=false;
	if (ReturnValue==true)
	{
		/* copy result to parameters - already done */
	}
	/* clean up */
	k_EmptyImage(&TempImg);
	k_EmptyString(&str);
	return ReturnValue;
}

bool k_ReadWorldCoordinates(const char* FileName, T3D2DPoints* pPointSets)
{
	bool returnValue;
	bool WoorldCoordinatesFound=false;
	FILE* fp;
	char* Path=NULL,*FileNameExt=NULL,*Ext=NULL;
	TString str;
	k_InitString(&str);
	if (!k_SplitFileName(FileName, &Path,&FileNameExt,&Ext))
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() Filename not valid");
		k_EmptyString(&str);
		return false;
	}
	else if (strcmp(Ext,"")==0)
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() No extension found");
		k_EmptyString(&str);
		returnValue=false;
	}
	else
	{
		/* booleans to check if necessary tokens were found */
		returnValue=true;
		fp=fopen(FileName,"rb"); /* open file */
		if (fp==NULL)
		{
			k_AddFileAndLine(str);
			k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() Could not open file");
			k_EmptyString(&str);
			returnValue=false;
		}
		else
		{
			/* read from file */
			char c;
			k_SkipWhiteSpace(fp);
			while (((c=(char)fgetc(fp))!=EOF) && (returnValue==true))
			{
				ungetc(c,fp);
				/* if beginning of line is a '#' then skip line */
				if (c=='#')
				{
					k_SkipLine(fp);
				}
				else /* not a '#' character - it must be a token then! */
				{
					char* Token;
					if (k_ReadToken(fp,&Token)==false)
					{
						k_AddFileAndLine(str);
						k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() Failed reading token");
						k_EmptyString(&str);
						returnValue=false;
					}
					else
					{
						k_StringToUpper(Token);
						/* token "PlateName" found */
						if (strcmp(Token,"WORLDCOORDINATES")==0)
						{
							char* CharValue;
							int TotalPoints;
							WoorldCoordinatesFound=true;
							k_SkipWhiteSpace(fp);
							k_ReadToken(fp,&CharValue);
							TotalPoints = atoi(CharValue);
							free(CharValue);
							k_Empty3D2DPoints(pPointSets);
							k_Alloc3D2DPoints(pPointSets,TotalPoints); // allocate memory for the points to be stored
							/* The following values must be in the format: x y z id */
							/* read those positions the number of times given above */
							for(int Count=0; Count<TotalPoints; Count++)
							{
								T3DFloat Pnt3D;
								int Id;
								k_SkipWhiteSpace(fp);
								c=(char)fgetc(fp);
								ungetc(c,fp);
								/* if beginning of line is a '#' then skip line */
								if (c=='#')
								{
									k_SkipLine(fp);
									Count--; // decrement count since we did not read a coordinate set
								}
								else
								{
									if (k_ReadToken(fp,&CharValue)==false)
									{
										k_AddFileAndLine(str);
										k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() Failed reading x value");
										k_EmptyString(&str);
									}	
									Pnt3D.x=(float)atof(CharValue);
									free(CharValue);
									if (k_ReadToken(fp,&CharValue)==false)
									{
										k_AddFileAndLine(str);
										k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() Failed reading y value");
										k_EmptyString(&str);
									}	
									Pnt3D.y=(float)atof(CharValue);
									free(CharValue);
									if (k_ReadToken(fp,&CharValue)==false)
									{
										k_AddFileAndLine(str);
										k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() Failed reading z value");
										k_EmptyString(&str);
									}	
									Pnt3D.z=(float)atof(CharValue);
									free(CharValue);
									if (k_ReadToken(fp,&CharValue)==false)
									{
										k_AddFileAndLine(str);
										k_ShowMessage(IPL_ERROR,&str,"k_ReadWorldCoordinates() Failed reading ID value");
										k_EmptyString(&str);
									}	
									Id=atoi(CharValue);
									k_AddPoint3DAndIdTo3D2DPoints(&Pnt3D,Id,pPointSets);
									free(CharValue);
								}
							}
						}
						free(Token);
						k_SkipWhiteSpace(fp);
					}
				}
				k_SkipWhiteSpace(fp);
			}
		}
		if (WoorldCoordinatesFound==false)
		{
			k_AddFileAndLine(str);
			k_ShowMessage(IPL_ERROR,&str,
				"k_ReadWorldCoordinates() One of the necessary tokens not found");
			k_EmptyString(&str);
			returnValue=false;
		}
		if (fp!=NULL)
		{
			fclose(fp);
		}
	}
	if (Path!=NULL)
		free(Path);
	if (FileNameExt!=NULL)
		free(FileNameExt);
	if (Ext!=NULL)
		free(Ext);
	k_EmptyString(&str);
	return returnValue;
}

void k_RefMarkPrintf(char* fmt, ...)
{
	va_list args; /* argument pointer */
	TString StrOut;
	char* p;
	p=(char*)malloc(sizeof(char)*REF_MARK_PRINT_MAX_MESSAGE_SIZE);
	k_InitString(&StrOut);

	va_start(args,fmt);
	if (vsprintf(p,fmt,args)>=REF_MARK_PRINT_MAX_MESSAGE_SIZE)
	{
		k_AddCharArrayToString("IPL_ERROR: k_RefMarkPrintf() Error message to long "
			"- increase the REF_MARK_PRINT_MAX_MESSAGE_SIZE definition",&StrOut);
		k_AddFileAndLine(StrOut);
		fprintf(stderr,"%s\n",StrOut.pChars);
		assert(0);
	}
	::AddIPL98ErrorHistory(p);

	va_end(args);
	free(p);
	k_EmptyString(&StrOut);
}

/************************************************/
/********        Private functions       ********/
/************************************************/


float k_LTDistancePB(T2DFloat pos,const TLTDetectedBlob* pB)
{
	return (float)sqrt((pos.x-pB->Center.x)*(pos.x-pB->Center.x)
		+(pos.y-pB->Center.y)*(pos.y-pB->Center.y));
}

float k_LTDistanceBB(const TLTDetectedBlob* pB1,const TLTDetectedBlob* pB2)
{
	return (float)sqrt((pB1->Center.x-pB2->Center.x)*(pB1->Center.x-pB2->Center.x)
		+(pB1->Center.y-pB2->Center.y)*(pB1->Center.y-pB2->Center.y));
}

float k_LTAngle(T2DFloat Center,const TLTDetectedBlob* pB2)
{
	return (float)(atan2(pB2->Center.y-Center.y, pB2->Center.x-Center.x)+ipl::PI);
}

void k_LTRemoveRefMarksFromImage(TLTDetectedRefMark* pRefMarks,unsigned int FoundRefMarks,TImage* pImg)
{
	if (FoundRefMarks==0)
		return;
	else
	{
		/* assumes same size of all ref marks */
		double RadiusCC0=sqrt((double)(pRefMarks[0].pCC0->pPixelGroup->NumberOfPixels)/ipl::PI);
		double RadiusCC2=sqrt(5*(ipl::PI*RadiusCC0*RadiusCC0)/ipl::PI);
		unsigned int i;
		int x,y;
		/*k_SetBorderSize((UINT16)k_Round(RadiusCC2+1),pImg);*/
		for(i=0;i<FoundRefMarks;i++)
		{
			T2DInt Center;
			Center.x=(short)k_Round(pRefMarks[i].pCC0->Center.x);
			Center.y=(short)k_Round(pRefMarks[i].pCC0->Center.y);
			int LowX=(int)(Center.x-RadiusCC2);
			int LowY=(int)(Center.y-RadiusCC2);
			int HighX=(int)(Center.x+RadiusCC2);
			int HighY=(int)(Center.y+RadiusCC2);
			if (LowX<0)
				LowX=0;
			if (LowY<0)
				LowY=0;
			if (HighX>(int)pImg->Width)
				HighX=pImg->Width;
			if (HighY>(int)pImg->Height)
				HighY=pImg->Height;
			for(y=LowY;y<HighY;y++)
			{
				for(x=LowX;x<HighX;x++)
				{
					k_SetPixel8bp(x,y,255,*pImg);
				}
			}
		}
	}
}

void k_LTAddPlusMark(const TLTDetectedBlob* pBlob, TImage* pImg,unsigned int XOffSet,unsigned int YOffSet)
{
	if(pBlob!=NULL)
	{
		int cx,cy;
		int color;
		color = 255;
		cx=k_Round(pBlob->Center.x+XOffSet);
		cy=k_Round(pBlob->Center.y+YOffSet);
		k_SetPixel(cx,   cy-1, color, pImg);
		k_SetPixel(cx-1, cy,   color, pImg);
		k_SetPixel(cx,   cy,   color, pImg);
		k_SetPixel(cx+1, cy,   color, pImg);
		k_SetPixel(cx,   cy+1, color, pImg);
	}
}

void k_LTAddCrossAtCC0(const TLTDetectedRefMark* pRefMark, TImage* pImg)
{
	int cx,cy;
	int color;
	color = 255;
	/* add 'x'-mark to the image at the mass center of te refmark */
	cx=k_Round((pRefMark->pCC0->Center.x+pRefMark->pCC1->Center.x+pRefMark->pCC2->Center.x)/3.0);
	cy=k_Round((pRefMark->pCC0->Center.y+pRefMark->pCC1->Center.y+pRefMark->pCC2->Center.y)/3.0);
	k_SetPixel(cx-2, cy-2, color, pImg);
	k_SetPixel(cx-1, cy-1, color, pImg);
	k_SetPixel(cx+2, cy-2, color, pImg);
	k_SetPixel(cx+1, cy-1, color, pImg);
	k_SetPixel(cx,   cy,   color, pImg);
	k_SetPixel(cx-1, cy+1, color, pImg);
	k_SetPixel(cx-2, cy+2, color, pImg);
	k_SetPixel(cx+1, cy+1, color, pImg);
	k_SetPixel(cx+2, cy+2, color, pImg);
}

/* does not change pImg! */
bool k_LTCalibDeriveBlobsAddColors(TPixelGroups* pPixelGroupsWhite,TPixelGroups* pPixelGroupsBlack,
								   TImage* pImg, UINT8 Threshold,unsigned int Verbose, unsigned int MinBlobSize)
{
	if(k_DeriveBlobs(pImg, pPixelGroupsWhite, HIGHCOLOR, Threshold, EIGHTCONNECTED,MinBlobSize)==false)
		return false;
	if(k_DeriveBlobs(pImg, pPixelGroupsBlack, LOWCOLOR, Threshold, EIGHTCONNECTED,MinBlobSize)==false)
		return false;
	if (Verbose!=0)
		k_RefMarkPrintf(" Total blobs found: White blobs=%d  Black blobs=%d\n",pPixelGroupsWhite->NumberOfGroups,pPixelGroupsBlack->NumberOfGroups);
	k_AddColorsToGroups(pImg, pPixelGroupsWhite);
	k_AddColorsToGroups(pImg, pPixelGroupsBlack);
	return true;
}

/* allocates an array for "ppBlobs" equal to total white and black blobs in image,
	calculates the moments for each blob. */
unsigned int k_LTAllocAndInitDetectedBlobs(TLTDetectedBlob** ppBlobs, TPixelGroups* pPixelGroupsWhite,
						  TPixelGroups* pPixelGroupsBlack, unsigned int Verbose)
{
	unsigned int TotalBlobs,i;
	if ((*ppBlobs)!=NULL)
		free(*ppBlobs);
	(*ppBlobs)=(TLTDetectedBlob*)calloc((pPixelGroupsWhite->NumberOfGroups+pPixelGroupsBlack->NumberOfGroups),sizeof(TLTDetectedBlob));
	for(i=0; i<pPixelGroupsWhite->NumberOfGroups;i=i+1)
	{
		(*ppBlobs)[i].pPixelGroup=&(pPixelGroupsWhite->pGroups[i]);
		(*ppBlobs)[i].Color=HIGHCOLOR;
	}
	TotalBlobs=pPixelGroupsWhite->NumberOfGroups;
	for(i=0; i<pPixelGroupsBlack->NumberOfGroups;i=i+1)
	{
		(*ppBlobs)[TotalBlobs+i].pPixelGroup=&(pPixelGroupsBlack->pGroups[i]);
		(*ppBlobs)[TotalBlobs+i].Color=LOWCOLOR;
	}
	TotalBlobs+=pPixelGroupsBlack->NumberOfGroups;
	/* calculate moments and mass center for each blob */
	for(i=0; i<TotalBlobs; i=i+1)
	{
		k_DeriveMomentsGrayTone((*ppBlobs)[i].pPixelGroup, 0, 2, &((*ppBlobs)[i].Moment));
		if((*ppBlobs)[i].Moment.m00!=0)
		{
			(*ppBlobs)[i].Center.x=(float)((*ppBlobs)[i].Moment.m10/(*ppBlobs)[i].Moment.m00);
			(*ppBlobs)[i].Center.y=(float)((*ppBlobs)[i].Moment.m01/(*ppBlobs)[i].Moment.m00);
		}
	}
	return TotalBlobs;
}

/* finds the Id for a reference mark located at "center". Done by first estimating the
	threshold value from the pixel values stored in "pRefMark". It then thresholds a small
	part of the image around the reference mark and searches for the Id in this new subimage */
/* Finds code circles from pBlobs. four different checks must be fulfilled in order to accept
   a blob as a code circle:
   1) must be located in a distance from the center position of pRefMark to an interval between
      searchDistMin and searchDistMax where searchDistMin=(17/5)*radiusCC0-(CODE_CIRCLES_SEARCH_AREA*radiusCC0/2)
	  and searchDistMax=(17/5)*radiusCC0+(CODE_CIRCLES_SEARCH_AREA*radiusCC0/2).
   2) the ratio between width and height of the blob must be 1+-BLOB_RATIO_TOLERANCE
   3) the width and height of the blob must be less than 2*radiusCC0 in the reference mark
   4) the ratio of the blobs rectangle area to the biggest circle inside that rectangle must fulfill:
             AreaRatio > (PI/4-AREA_RATIO_TOLERANCE)
			 where AreaRatio NumberOfPixels/(BlobWidth*BlobHeight)
   5) code circle blob size (ccbs) must fulfill:
			 PI * ((2/5) * radiusCC0)^2 * (1 - CODE_CIRCLE_SIZE_FRACTION) < ccbs <  PI * ((3/5) * radiusCC0)^2 * (1 + CODE_CIRCLE_SIZE_FRACTION) */
bool k_LTFindCodeCircles(unsigned int* pId,TDirection* pDir, TImage* pImg,TLTDetectedRefMark* pRefMark,TImage* pPlotImg,
						  unsigned int Verbose)
{
	unsigned int Threshold,i;
	T2DFloat center;
	float searchDist,searchDistMin,searchDistMax, SubImgDim, RadiusCC0;
	float BlackVal=0,WhiteVal=0;
	int xmin,ymin,xmax,ymax;
	const float SIZE_OF_WHOLE_MARK=10; /* square sub image copied with SIZE_OF_WHOLE_MARK*RadiusCC0 size */
	TImage SubImg;
	TPixelGroups CodeGroups;
	TLTDetectedBlob* pBlobs=NULL;
	TPixelGroup* pGroup;
	TString str;
	bool ReturnValue=true;
	k_InitString(&str);
	k_InitImage(&SubImg);
	k_InitGroups(&CodeGroups);
	/* find center for reference mark, radius for CC0 and the search area to look for code circles */
	center.x=(float)((pRefMark->pCC0->Center.x+pRefMark->pCC1->Center.x+pRefMark->pCC2->Center.x)/3.0);
	center.y=(float)((pRefMark->pCC0->Center.y+pRefMark->pCC1->Center.y+pRefMark->pCC2->Center.y)/3.0);
	/* find radius calculated from the total area of the reference mark center circles */
	RadiusCC0=(float)sqrt((pRefMark->pCC0->pPixelGroup->NumberOfPixels+
						   pRefMark->pCC1->pPixelGroup->NumberOfPixels+
						   pRefMark->pCC2->pPixelGroup->NumberOfPixels)/(5.0*ipl::PI));
	searchDist=(float)((17.0/5.0)*RadiusCC0);
	searchDistMin=(float)(searchDist-(CODE_CIRCLES_SEARCH_AREA*RadiusCC0/2.0));
	searchDistMax=(float)(searchDist+(CODE_CIRCLES_SEARCH_AREA*RadiusCC0/2.0));
	/* find the optimal local threshold */
	/* first estimate the average black value */
	pGroup=pRefMark->pCC2->pPixelGroup;
	for(i=0;i<pGroup->NumberOfPixels; i++)
	{
		BlackVal+=pGroup->pColor[i];
	}
	BlackVal /= pGroup->NumberOfPixels;
	/* next estimate the average white value */
	pGroup=pRefMark->pCC1->pPixelGroup;
	for(i=0;i<pGroup->NumberOfPixels; i++)
	{
		WhiteVal+=pGroup->pColor[i];
	}
	WhiteVal /= pGroup->NumberOfPixels;
	Threshold=k_Round((WhiteVal+BlackVal)/2);
	SubImgDim=RadiusCC0*SIZE_OF_WHOLE_MARK;
	if (Verbose>=2)
	{
		k_RefMarkPrintf("  In k_LTFindCodeCircles (verbose=%d)",Verbose);
		k_RefMarkPrintf("    Average black=%.1f   Average white=%.1f  Local threshold=%d",BlackVal,WhiteVal,Threshold);
	}

	/* copy sub image */
	xmin = ((center.x-(SubImgDim/2)) <= 0) ? 0 : k_Round(center.x-(SubImgDim/2));
	xmax = ((center.x+(SubImgDim/2)) >= pImg->Width) ? (pImg->Width-1) : k_Round(center.x+(SubImgDim/2));
	ymin = ((center.y-(SubImgDim/2)) <= 0) ? 0 : k_Round(center.y-(SubImgDim/2));
	ymax = ((center.y+(SubImgDim/2)) >= pImg->Height) ? (pImg->Height-1) : k_Round(center.y+(SubImgDim/2));
	k_CopySubImage(xmin,ymin,xmax,ymax,&SubImg,pImg);
	if (Verbose>=2)
	{
		k_RefMarkPrintf("    Copying subimage (%d,%d) -> (%d,%d)",xmin,ymin,xmax,ymax);
	}
	if(k_DeriveBlobs(&SubImg, &CodeGroups, LOWCOLOR, (UINT8)Threshold, EIGHTCONNECTED,0)==true)
	{
		if (Verbose>=2)
		{
			k_RefMarkPrintf("    Total blobs in subimage: %d",CodeGroups.NumberOfGroups);
		}
		TLTDetectedBlob* pTempBlobs[7];
		float TempAngles[7];
		unsigned int TotalTempBlobs=0;
		unsigned int CSSize;
		unsigned int CSIndex;
		float CSAngle;
		for(i=0; i<7; i=i+1)
		{
			pTempBlobs[i]=NULL;
		}
		k_AddColorsToGroups(&SubImg, &CodeGroups);
		/* initialize a corresponding list with momentvalues */
		pBlobs=(TLTDetectedBlob*)calloc((CodeGroups.NumberOfGroups),sizeof(TLTDetectedBlob));
		for(i=0; i<CodeGroups.NumberOfGroups;i=i+1)
		{
			pBlobs[i].pPixelGroup=&(CodeGroups.pGroups[i]);
			pBlobs[i].Color=LOWCOLOR; /* not used in this algorithm */
			/* calculate moments and mass center */
			k_DeriveMomentsGrayTone(pBlobs[i].pPixelGroup, 0, 2, &(pBlobs[i].Moment));
			if(pBlobs[i].Moment.m00!=0)
			{
				pBlobs[i].Center.x=(float)(pBlobs[i].Moment.m10/pBlobs[i].Moment.m00);
				pBlobs[i].Center.y=(float)(pBlobs[i].Moment.m01/pBlobs[i].Moment.m00);
			}
		}
		/* update center of reference mark to be in subimage coordinatesystem */
		center.x -= xmin;
		center.y -= ymin;
		/* locate the blobs and calculate the Id */
		for(i=0; i<CodeGroups.NumberOfGroups; i=i+1)
		{
			/* checking first condition (see documention in top of this file) */
			if ((k_LTDistancePB(center,&pBlobs[i]) > searchDistMin)
				&& (k_LTDistancePB(center,&pBlobs[i]) < searchDistMax))
			{
				if (Verbose>=2)
				{
					k_RefMarkPrintf("    Blob with center (%.1f,%.1f) passed the distance from refmark center test",
						pBlobs[i].Center.x,pBlobs[i].Center.y);
				}

				/* checking next four conditions (see documention in top of this file) */
				int BlobWidth=pBlobs[i].pPixelGroup->Right.x-pBlobs[i].pPixelGroup->Left.x+1;
				int BlobHeight=pBlobs[i].pPixelGroup->Bottom.y-pBlobs[i].pPixelGroup->Top.y+1;
				/*float BlobCircleArea=(float)PI*(BlobWidth/2)*(BlobWidth/2);*/
				float BlobRatio=(float)BlobWidth/BlobHeight;
				float AreaRatio=(float)pBlobs[i].pPixelGroup->NumberOfPixels/(BlobWidth*BlobHeight);

				if ((BlobRatio>1-BLOB_RATIO_TOLERANCE) && (BlobRatio<1+BLOB_RATIO_TOLERANCE) && 
					(BlobWidth<(float)2*RadiusCC0) && (BlobHeight<(float)2*RadiusCC0) &&
					(AreaRatio > (ipl::PI/4-AREA_RATIO_TOLERANCE)) &&
					(ipl::PI*pow((2.0/5.0)*RadiusCC0,2)*(1.0-CODE_CIRCLE_SIZE_FRACTION) <	pBlobs[i].pPixelGroup->NumberOfPixels) &&
					(pBlobs[i].pPixelGroup->NumberOfPixels < ipl::PI*pow((3.0/5.0)*RadiusCC0,2)*(1.0+CODE_CIRCLE_SIZE_FRACTION)))
				{
					if(TotalTempBlobs<7)
					{
						/* identification blob found */
						pTempBlobs[TotalTempBlobs]=&(pBlobs[i]);
						TempAngles[TotalTempBlobs]=k_LTAngle(center,&(pBlobs[i]));
						TotalTempBlobs=TotalTempBlobs+1;
						if (Verbose>=2)
						{
							k_RefMarkPrintf("    Identification blob found!!!");
						}
					}
					else
					{
						k_AddFileAndLine(str);
						k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Too many code circles detected");
						k_EmptyString(&str);
					}
				}
			}
		}
		/* arrange the order of the code circles
		   find Code Start Circle */
		CSSize=0;
		CSIndex=0;
		for(i=0; i<TotalTempBlobs; i=i+1)
		{
			if(pTempBlobs[i]->pPixelGroup->NumberOfPixels>CSSize)
			{
				CSSize=pTempBlobs[i]->pPixelGroup->NumberOfPixels;
				CSIndex=i; /* code start index */
				/* Set center of mass in the direction parameter */
				pDir->MarkCM.x=center.x + xmin; /* update center to be global in image */
				pDir->MarkCM.y=center.y + ymin; /* update center to be global in image */
				pDir->CodeStartCM.x=pTempBlobs[i]->Center.x + xmin; /* update center to be global in image */
				pDir->CodeStartCM.y=pTempBlobs[i]->Center.y + ymin; /* update center to be global in image */
			}
		}
		if(pTempBlobs[CSIndex]!=NULL)
		{
			CSAngle=TempAngles[CSIndex];
			/* find Id */
			(*pId)=0;
			for(i=0; i<TotalTempBlobs; i=i+1)
			{
				/* calculate angles relative to the Code Start Circle angle */
				TempAngles[i]=TempAngles[i]-CSAngle;
				if(TempAngles[i]<0)
				{
					TempAngles[i]=(float)(TempAngles[i]+2.0*ipl::PI);
				}
				if(i!=CSIndex)
				{
					float delta=(float)(CALIB_ALPHA/2.0);
					if((TempAngles[i]>1.0*CALIB_ALPHA-delta)&&(TempAngles[i]<1.0*CALIB_ALPHA+delta))
					{
						if (*pId & 0x00000001)
						{
							k_AddFileAndLine(str);
							k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Two code circles located in same bit position 0");
							k_EmptyString(&str);
						}
						(*pId)+=1;
					}
					else if((TempAngles[i]>2.0*CALIB_ALPHA-delta)&&(TempAngles[i]<2.0*CALIB_ALPHA+delta))
					{
						if (*pId & 0x00000010)
						{
							k_AddFileAndLine(str);
							k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Two code circles located in same bit position 1");
							k_EmptyString(&str);
						}
						(*pId)+=2;
					}
					else if((TempAngles[i]>3.0*CALIB_ALPHA-delta)&&(TempAngles[i]<3.0*CALIB_ALPHA+delta))
					{
						if (*pId & 0x00000100)
						{
							k_AddFileAndLine(str);
							k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Two code circles located in same bit position 2");
							k_EmptyString(&str);
						}
						(*pId)+=4;
					}
					else if((TempAngles[i]>4.0*CALIB_ALPHA-delta)&&(TempAngles[i]<4.0*CALIB_ALPHA+delta))
					{
						if (*pId & 0x00001000)
						{
							k_AddFileAndLine(str);
							k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Two code circles located in same bit position 3");
							k_EmptyString(&str);
						}
						(*pId)+=8;
					}
					else if((TempAngles[i]>5.0*CALIB_ALPHA-delta)&&(TempAngles[i]<5.0*CALIB_ALPHA+delta))
					{
						if (*pId & 0x00010000)
						{
							k_AddFileAndLine(str);
							k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Two code circles located in same bit position 4");
							k_EmptyString(&str);
						}
						(*pId)+=16;
					}
					else if((TempAngles[i]>6.0*CALIB_ALPHA-delta)&&(TempAngles[i]<6.0*CALIB_ALPHA+delta))
					{
						if (*pId & 0x00100000)
						{
							k_AddFileAndLine(str);
							k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Two code circles located in same bit position 5");
							k_EmptyString(&str);
						}
						(*pId)+=32;
					}
					else
					{
						k_AddFileAndLine(str);
						k_ShowMessage(IPL_WARNING,&str,"k_FindCalibrationMarks() Bad position of Code Circle ");
						k_EmptyString(&str);
					}
				}
				if (Verbose!=0)
				{
					/* plot the found code circles */
					k_LTAddPlusMark(pTempBlobs[i],pPlotImg,xmin,ymin);
				}
			}
			if (Verbose>=2)
			{
				k_RefMarkPrintf("    Id found as %d",*pId);
			}
			ReturnValue=true;
		}
		else
		{
			if (Verbose!=0)
			{
				k_RefMarkPrintf("center of mass: CC0=(%.1f,%.1f)  CC1=(%.1f,%.1f)  CC2=(%.1f,%.1f)\n",pRefMark->pCC0->Center.x,
					pRefMark->pCC0->Center.y,pRefMark->pCC1->Center.x,pRefMark->pCC1->Center.y,
					pRefMark->pCC2->Center.x,pRefMark->pCC2->Center.y);
				k_AddFileAndLine(str);
				k_ShowMessage(IPL_ERROR,&str,"k_FindCalibrationMarks() Code Start Circle not found for "
					"reference mark with center of mass at (%.1f,%.1f)",pRefMark->pCC0->Center.x,pRefMark->pCC0->Center.y);
			}
			ReturnValue=false;
		}
	}
	/* clean up */
	if (pBlobs!=NULL)
		free(pBlobs);
	k_EmptyImage(&SubImg);
	k_EmptyString(&str);
	k_EmptyGroups(&CodeGroups);
	return ReturnValue;
}

/* Find a single corresponding Id. If it turns out that the found Id has not been found before the
   following is performed: The mass centers for CC0, CC1, and CC2 are copied
   to the "pCalib"-parameter and the ImagePosInUse is set to true in the same parameter. */
unsigned int k_LTFindCorrespondingId(TLTDetectedRefMark* pRefMark, unsigned int RefMarkId, const TDirection* pDir, 
									 TDirection* pDirs, T3D2DPoints* pPointSets,unsigned int Verbose)
{
	/*unsigned int x=0,y=0;*/
	/*bool IdFound=false;*/
	unsigned int Index;
	if (k_GetIndexIn3D2DPoints(&Index,RefMarkId,pPointSets)==true)
	{
		if (pPointSets->pSet[Index].Pnt2DInUse==false) /* maybe it has been found with another threshold */
		{
			/* corresponding id's found - copy information to the T3D2DPoints structure */
			int TotalIdMatched=0;
			T2DFloat P2d;
			/* insert center of mass for the center circles CC0 only */
			P2d.x=(float)(pRefMark->pCC0->Center.x);
			P2d.y=(float)(pRefMark->pCC0->Center.y);
			/* insert a mean center of mass for the three center circles CC0, CC1 and CC2 */
			/*T2DFloat old;
			old.x=(float)((pRefMark->pCC0->Center.x+pRefMark->pCC1->Center.x+pRefMark->pCC2->Center.x)/3.0);
			old.y=(float)((pRefMark->pCC0->Center.y+pRefMark->pCC1->Center.y+pRefMark->pCC2->Center.y)/3.0);
			float diffx=P2d.x-old.x;
			float diffy=P2d.y-old.y;*/
			k_SetPoint2DIn3D2DPoints(&P2d, Index, pPointSets);
			/** Also update the direction array */
			pDirs[Index]=*pDir;
			TotalIdMatched++;
			if (Verbose!=0)
			{
				k_RefMarkPrintf(" Id %d matched at: WorldPos=(%.2f,%.2f,%.2f)  ImagePos=(%.2f,%.2f)\n",
					RefMarkId,pPointSets->pSet[Index].Pnt3D.x,pPointSets->pSet[Index].Pnt3D.y,
					pPointSets->pSet[Index].Pnt3D.z,pPointSets->pSet[Index].Pnt2D.x,
					pPointSets->pSet[Index].Pnt2D.y);
			}
			return 1;
		}
		else
		{
			if (Verbose>=2)
			{
				k_RefMarkPrintf(" Reference mark already found - skipping");
			}
			return 0;
		}
	}
	else
	{
		TString str;
		k_InitString(&str);
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"k_LTFindCorrespondingId() Id=%d found in image with ref mark center "
			"(CC0) at (%.1f,%.1f) has no corresponding id in setup file",
			RefMarkId,pRefMark->pCC0->Center.x,pRefMark->pCC0->Center.y);
		k_EmptyString(&str);
		return 0;
	}
}


/* Finds the reference marks with given threshold. Done by finding the three center circles
   of a reference marks in image with given threshold, after that the function k_LTFindCodeCircles()
   is called, it finds an optimal local threshold to find the codecircles. A found ref mark is deleted
   in the image by setting a white square.
   returns the total number of found reference marks */
unsigned int k_LTFindCalibrationMarksAndMatchIds(TImage* pImg,T3D2DPoints* pPointSets,TDirection* pDirs, unsigned int Threshold,
												  TImage* pPlotImg, unsigned int Verbose, unsigned int MinBlobSize)
{
	unsigned int FoundRefMarks=0;
	unsigned int TotalBlobs,i,j,k;
	TPixelGroups PixelGroupsWhite;
	TPixelGroups PixelGroupsBlack;
	TLTDetectedBlob* pBlobs=NULL;
	TLTDetectedRefMark* pRefMarks=NULL;
	k_InitGroups(&PixelGroupsWhite);
	k_InitGroups(&PixelGroupsBlack);
	if (k_LTCalibDeriveBlobsAddColors(&PixelGroupsWhite,&PixelGroupsBlack,pImg,(UINT8)Threshold,Verbose,MinBlobSize)==true)
	{
		pRefMarks=(TLTDetectedRefMark*)calloc(XDIR_HOLES+YDIR_HOLES, sizeof(TLTDetectedRefMark));
		for(i=0; i<XDIR_HOLES+YDIR_HOLES; i=i+1)
		{
			k_IniTLTDetectedRefMark(&(pRefMarks[i]));
		}
		TotalBlobs=k_LTAllocAndInitDetectedBlobs(&pBlobs,&PixelGroupsWhite,&PixelGroupsBlack,Verbose);
		for(i=0; i<TotalBlobs; i=i+1)
		{
			/* find an unused white mid circle */
			if(pBlobs[i].Color==HIGHCOLOR)
			{
				/* find the matching black inner and outer circles by similar center of masses, 
					checking that the outer circle has a bigger radius and the inner circle has 
					a smaller radius */
				for(j=0; j<TotalBlobs; j=j+1)
				{
					if((pBlobs[j].Color==LOWCOLOR) &&
						(k_LTDistanceBB(&pBlobs[i],&pBlobs[j]) < CMTOLERANCE))
					{
						for(k=j+1; k<TotalBlobs; k=k+1)
						{
							if((pBlobs[k].Color==LOWCOLOR) &&
								(k_LTDistanceBB(&pBlobs[i],&pBlobs[k])<CMTOLERANCE))
							{
								if (Verbose>=2)
								{
									k_RefMarkPrintf("\nFound three circles at (%.1f,%.1f) with mass centers "
										"accepted by CMTOLERANCE\n",
										(float)((pBlobs[i].Center.x+pBlobs[j].Center.x+pBlobs[k].Center.x)/3.0),
										(float)((pBlobs[i].Center.y+pBlobs[j].Center.y+pBlobs[k].Center.y)/3.0));
								}
								/* assign CC0 and CC2 to the correct blob */
								/* also check if both the center of mass for CC1 and CC2 are contained in CC0 
								   and: "radius CC2 > radius CC1 > radius CC0" */
								unsigned int Index; /* just a dummy variable used with k_PosInGroup() */
								T2DInt IntCenterCC1,IntCenterCC2;
								TPixelGroup* pGroupCC0;
								float radiusI,radiusJ,radiusK;
								bool RadiusOK=true;
								pRefMarks[FoundRefMarks].pCC1=&(pBlobs[i]);
								radiusI=(float)((pBlobs[i].pPixelGroup->Right.x-pBlobs[k].pPixelGroup->Left.x+1)/2.0);
								radiusJ=(float)((pBlobs[j].pPixelGroup->Right.x-pBlobs[j].pPixelGroup->Left.x+1)/2.0);
								radiusK=(float)((pBlobs[k].pPixelGroup->Right.x-pBlobs[k].pPixelGroup->Left.x+1)/2.0);
								if(radiusJ>radiusK)
								{
									pRefMarks[FoundRefMarks].pCC2=&(pBlobs[j]);
									pRefMarks[FoundRefMarks].pCC0=&(pBlobs[k]);
									if ((radiusJ<radiusI) || (radiusK>radiusI))
										RadiusOK=false;
								}
								else
								{
									pRefMarks[FoundRefMarks].pCC0=&(pBlobs[j]);
									pRefMarks[FoundRefMarks].pCC2=&(pBlobs[k]);
									if ((radiusJ>radiusI) || (radiusK<radiusI))
										RadiusOK=false;
								}
								pGroupCC0=pRefMarks[FoundRefMarks].pCC0->pPixelGroup;
								IntCenterCC1.x=(short)k_Round(pRefMarks[FoundRefMarks].pCC1->Center.x);
								IntCenterCC1.y=(short)k_Round(pRefMarks[FoundRefMarks].pCC1->Center.y);
								IntCenterCC2.x=(short)k_Round(pRefMarks[FoundRefMarks].pCC2->Center.x);
								IntCenterCC2.y=(short)k_Round(pRefMarks[FoundRefMarks].pCC2->Center.y);

								if (Verbose>=2)
								{
									if (RadiusOK==false)
									{
										k_RefMarkPrintf("  Radius for the three blobs not accepted (r0=%.1f, r1=%.1f, r2=%.1f)",
											radiusI,radiusJ,radiusK);
										k_RefMarkPrintf("        additional info: CC1 center=(%d,%d) CC2 center=(%d,%d)",
											IntCenterCC1.x,IntCenterCC1.y,IntCenterCC2.x,IntCenterCC2.y);
									}
								}

								if ((k_PosInGroup(pGroupCC0,IntCenterCC1,&Index)) &&
									(k_PosInGroup(pGroupCC0,IntCenterCC2,&Index)) &&
									(RadiusOK==true))
								{
									/* three circles with same center of mass found */
									unsigned int Id;
									TDirection Dir;
									if(k_LTFindCodeCircles(&Id,&Dir,pImg,&(pRefMarks[FoundRefMarks]),pPlotImg,Verbose)==true)
									{
										/* check if the found id matches one of the id's given in setup file
										   if successful the function also updates pPointSets at the correct place. */
										if (k_LTFindCorrespondingId(&(pRefMarks[FoundRefMarks]),Id,&Dir,pDirs,pPointSets,Verbose)==1)
										{
											if (Verbose!=0)
												k_LTAddCrossAtCC0(&(pRefMarks[FoundRefMarks]),pPlotImg);
											FoundRefMarks += 1;
										}
									}
								}
								else if (Verbose>=2)
								{
									if (RadiusOK==true) // then one of the pos in group check must have failed
									{
										k_RefMarkPrintf("  One of the refmark circles center of mark not inside CC0: "
											"CC1 center=(%d,%d) CC2 center=(%d,%d)",IntCenterCC1.x,IntCenterCC1.y,IntCenterCC2.x,IntCenterCC2.y);
									}
								}
							}
						}					
					}
				}
			}
		}
	}
	/* Remove found refmarks from image */
	k_LTRemoveRefMarksFromImage(pRefMarks,FoundRefMarks,pImg);
	/* clean up */
	k_EmptyGroups(&PixelGroupsWhite);
	k_EmptyGroups(&PixelGroupsBlack);
	if (pBlobs!=NULL)
		free(pBlobs);
	free(pRefMarks);
	return FoundRefMarks;
}

} // end namespace videndo
