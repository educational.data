#ifndef _VIDENDO_KERNEL_MARK_RETRIEVAL_H
#define _VIDENDO_KERNEL_MARK_RETRIEVAL_H

#include "../videndo_setup.h" /* always include the videndo setup file */
#include <ipl98/kernel_c/image/kernel_functions.h>
#include <ipl98/kernel_c/kernel_corresponding_3d2d_points.h>

namespace videndo { /* use namespace if C++ */

using ipl::TImage;
using ipl::T3D2DPoints;
using ipl::UINT8;

#if __cplusplus == 1
//extern "C" { /* prevent name mangling for C functions */
#endif

/** Used by mark retrieval functions to hold directions for each reference mark */
typedef struct
{
	ipl::T2DFloat MarkCM;
	ipl::T2DFloat CodeStartCM;
} TDirection;

/**	@name Mark retrieval
	@memo Kernel ANSI C reference mark retrieval routines, last updated 5/1/2003.
	This set of functions provides a system for finding reference
	marks in an image containing a picture of a calibration plate.
	At present there are two functions available, future experience will show
	which one turns out to be
	the best. The k_FindMarksAutoLocalThreshold() method should be the best of the two.
	Calibration functions are found in the IPL98 perspective functions section.
	Note 1: There are many definitions in the header file that can be adjusted to optimize the
		search criterias for reference marks.
	@version 0.93
	@author Brian Kirstein Ramsgaard (kirstein@protana.com) & Ren� Dencker Eriksen (edr@mip.sdu.dk) */
/*@{ */


/** @name Definitions for detection criteria
	This area defines/describes the definitions used to search for reference marks.
	Several criteria are used when searching for the reference marks in an image, and most
	of those creteria have a factor associated which can be adjusted for better performance.
	For instance one criteria is a tolerance named CMTolerance which handles a small error in
	identical center of mass positions for the three main circles CC0, CC1 and CC2. Some of
	the definitions are default values and can be changed by defining them in the setup file
	for this system. In general these definitions should not be changed when using the standard
	calibration marks created to be used with this system. But certain circumstances may lead
	to necessary changes, it could be that the values given here are not always satisfying with
	every camera. */
/*@{ */
/** A version value, don't change it. */
#define IPL98_MARKRETRIEVAL_VERSION 0.93
/** Maximum number of holes on plate in x-direction, max 9; Value: 7 */
#define XDIR_HOLES 7
/** Maximum number of holes on plate in y-direction, max 9; Value: 7 */
#define YDIR_HOLES 7 
/**	Tolerance on variations of center of mass (CM) for cirkles. If two blobs
	have a common CM within this tolerance they are treated as having the
	same CM. Set to a defualt value by k_InitCalibration.
	Can be set by k_ReadSetupAndCalcWorldCoord(). */
#define CMTOLERANCE 4
/** A search belt to look for code circles. The belt has a width 
	(RadiusCC0 * CODE_CIRCLES_SEARCH_AREA). Value: 1.0 */
#define CODE_CIRCLES_SEARCH_AREA 1.0
//#define CODE_CIRCLES_SEARCH_AREA 1.5
/** The angle between two successive circles in the binary code; Value: PI/7 */
//#define alpha PI/7.0
#define CALIB_ALPHA ipl::PI/7.0
/** Tolerance of blobratio used to accept a blob as a code circle. The ratio between width
	and height of the blob must be 1+-BLOB_RATIO_TOLERANCE. Normal value: 0.7 */
#define BLOB_RATIO_TOLERANCE (float)0.7
//#define BLOB_RATIO_TOLERANCE (float)0.5
/** Tolerance of "rectangle area of blob"/"circle-area of blob". The ratio of the 
	blobs rectangle area to the biggest circle inside that rectangle must fulfill:
	AreaRatio > (PI/4-AREA_RATIO_TOLERANCE) where 
	AreaRatio=NumberOfPixels/(BlobWidth*BlobHeight). Normal value: 0.2 */
#define AREA_RATIO_TOLERANCE (float)0.25
/** A code circle must have a size related to the size of CC0.
	Code circle size (ccs) must fulfill:
	PI * ((2/5) * radiusCC0)^2 * (1 - CODE_CIRCLE_SIZE_FRACTION) < ccs <  
	PI * ((3/5) * radiusCC0)^2 * (1 + CODE_CIRCLE_SIZE_FRACTION); Normal value: 0.3 */
#define CODE_CIRCLE_SIZE_FRACTION (float)0.4
/*@} */

/** @name k_FindMarksAutoLocalThreshold
	Finds reference marks by using different thresholds. For each global threshold it searches
	for the three center circles. If a set of three circles with same center of mass are found,
	it finds a local threshold according to the graytone values in the two outer circles
	(CC1 and CC2). A subimage around the refmark is then thresholded with the new value and
	the code circles are found. The global threshold is caculated by: Starts from the CALIB_THRESH
	value defined earlier in this header file (kernel_calibration.h), if not all of the
	reference marks are found, it continues with the value (CALIB_THRESH-CALIB_THRESH_STEP) and
	(CALIB_THRESH+CALIB_THRESH_STEP) and so on.
	@param pOriginalImg Pointer to the source image. Must be 8 b/p.
	@param pPointSets (T3D2DPointSets) The structure to match id's with. The 3D positions and
		id's must be initialized for instance by calling k_ReadCalibrationPlateSetup(). If an
		id is matched the 2D position from the found calibration mark is stored in the
		T3D2DPoints structure at the Index corresponding to the Id and 3D position.
	@param pDirs Contains center of mass for code start circle and refmark center for all reference marks.
		Must be allocated manually with same number of indexes as available points in pPointSets.
	@param BeginThreshold The beginning threshold for algorithm, the better this guess
		is the faster the function finishes.
	@param Verbose If not 0, important information is written to stdout, and the
		center of all circles in a reference mark are drawn directly in "pOriginalImg".
		Higher numbers results in more detailed output.
    @return False, if source/target image is empty or not 8 b/p, or if
		the found id's does not match the id's present in the pPointSets parameter.
	@see k_FindMarksAutoGlobalThreshold */
bool k_FindMarksAutoLocalThreshold(TImage* pOriginalImg, T3D2DPoints* pPointSets, TDirection* pDirs,
								   unsigned short BeginThreshold, unsigned int Verbose);

/** @name k_ReadWorldCoordinates
	Reads a set of world coordinates.
	General remarks: The file is constructed
	from a set of tokens with one or more appropriate values seperated by whitespace. Comments can 
	be written only at whole lines, but placed anywhere in the file, begins with the character '\#'. 
	The order of tokens is not important.
	Specific information for the world coordinate file:
		WorldCoordinates token followed by the total number of world coordinates in this file.
		After this follows the coordinates and an id like this: x y z id
	@return False, if world coordinate file is illegal. */
bool k_ReadWorldCoordinates(const char* FileName, T3D2DPoints* pPointSets);

#define REF_MARK_PRINT_MAX_MESSAGE_SIZE 400

/** @name k_RefMarkPrintf
	Routine to print messages, errors etc. All messages should be printed using this function,
	a redirection of messages can then be done by changing this function only. Works as the
	normal printf function */
void k_RefMarkPrintf(char* fmt, ...);

/*@} */

#if __cplusplus == 1
//} /* end of extern "C" */
#endif

} // end namespace videndo

#endif /* _VIDENDO_KERNEL_MARK_RETRIEVAL_H */
