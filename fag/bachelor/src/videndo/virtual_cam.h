#ifndef _CVIRTUALCAM_H
#define _CVIRTUALCAM_H

// Necessary to include the MFC afxwin.h to make the gl.h and glu.h files compile!
#include <afxwin.h>         // MFC core and standard components

#include <points/point3d.h>
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/vectors/vector3d.h>
#include <vector>
#include <fstream>
#include <ostream>
#include <istream>

// OpenGL headers
#include "gl\gl.h"
#include "gl\glu.h"

namespace videndo{

using std::vector;
using std::ifstream;
using std::ostream;
using std::istream;
using ipl::CImage;

/** CVirtualCam implements a virtual camera, where the camera pose, image dimensions
	(in pixels), and the focal length (in pixels) can be controlled. It uses
	OpenGL to construct images.

    The class can load the CAD STL-format. Note, that the STL-file must be in ASCII
	format, the binary version cannot be loaded at this moment.

	At the moment a light source is placed at the pinhole positions in order
	to imitate a ring light. There is no anti aliasing since I'm having problems
	making it work together with a light source. It works fine with just default
	ambient light.

	Note: In order to compile this class, the MFC file afxwin.h must be included. 
	Otherwise the OpenGL library (gl.h and glu.h) will not compile.
	To do this, this file includes the afxwin.h file from the general include path, 
	You need to include the files virtual_cam.h and virtual_cam.cpp in your project
	when you need to use them, and you need to ensure that your project supports
	MFC, otherwise the afxwin.h will conflict with other libraries. 
	You must also link with the library files opengl32.lib and glu32.lib!
	The file afxwin.h from the MFC core library is included in top of this file, 
	otherwise the OpenGL files gl.h and glu.h will not compile.

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CVirtualCam videndo/virtual_cam.h
	@version 0.14
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CVirtualCam
{
// Attributes
public:
	// Pose
	double m_x;
	double m_y;
	double m_z;
	double m_a;
	double m_b;
	double m_c;
	// Skewness
	double m_Beta;
	// Pixel aspect ratio
	double m_p;
	// Size
	double m_dNearPlane; 
	double m_dFarPlane; 

	// Clear colors
	float m_ClearColorRed;
	float m_ClearColorGreen;
	float m_ClearColorBlue;
	// Object colors
	float m_ObjectColorRed;
	float m_ObjectColorGreen;
	float m_ObjectColorBlue;

	
protected:
	/// Width in pixels
	unsigned int m_Width;
	/// Height in pixels
	unsigned int m_Height;
	double m_WidthHalf;
	double m_HeightHalf;
	/// Focal length in pixels
	double m_f;
	/// Set to true when Initialize() is called
	bool m_IsInitialized;
	bool m_IsCameraDefined;
	/// Struct for holding triangles defined in STL CAD file
	struct CTriangle{
	public:
		static CPoint3D<double> m_CamPos;
		double m_Normal[3];
		// first index is vertice, next is (x,y,z)
		double m_Vertice[3][3];
		// m_CamPos must be set before using this operator!
		inline bool operator<(const CTriangle& Tr) const
		{
			CPoint3D<double> P1((m_Vertice[0][0]+m_Vertice[1][0]+m_Vertice[2][0])/3.0,
								(m_Vertice[0][1]+m_Vertice[1][1]+m_Vertice[2][1])/3.0,
								(m_Vertice[0][2]+m_Vertice[1][2]+m_Vertice[2][2])/3.0);
			CPoint3D<double> P2((Tr.m_Vertice[0][0]+Tr.m_Vertice[1][0]+Tr.m_Vertice[2][0])/3.0,
								(Tr.m_Vertice[0][1]+Tr.m_Vertice[1][1]+Tr.m_Vertice[2][1])/3.0,
								(Tr.m_Vertice[0][2]+Tr.m_Vertice[1][2]+Tr.m_Vertice[2][2])/3.0);
			return (P1.GetDist(m_CamPos)<P2.GetDist(m_CamPos));
		}					
	};
	/// Vector of trangles, holds the complete CAD object when LoadSTLFile has been called
	vector<CTriangle> m_Triangles;

// Public methods
public:
	/// Constructor
	CVirtualCam();

	/// Default destructor
	~CVirtualCam();

	/** Must be called prior to using other methods in this class. It initializes
		the OpenGL setup regarding lighting, rendering method etc.
		@return True, always. */
	bool Initialize();

	/** Defines the camera by setting image dimensions and focal length. */
	bool DefineCamera(unsigned int Width, unsigned int Height, double FocalLength);

	/** Sets pose of camera.
		@param x X-coordinate value
		@param y Y-coordinate value
		@param z Z-coordinate value
		@param a Tilt of camera in radians
		@param b Pan of camera in radians
		@param c Roll of camera in radians
		@return False, if Initialize() has not been called. */
	bool SetPose(double x, double y, double z, double a, double b, double c);

	/** Returns rendered image of CAD model.
		@param Img Image returned in this parameter.
		@return False, if Initialize() has not been called. */
	bool GetImage(CImage& Img);

	/** Loads an ASCII based STL file. */
	bool LoadSTLFile(const char* pPathAndFileName);

	/** Sets the background color for the projected image.
		@param R Red component value in range [0;1], where 0 is darkest. 
		@param G Red component value in range [0;1], where 0 is darkest. 
		@param B Red component value in range [0;1], where 0 is darkest. 
		@return False, if R, G, or B is out of range. */
	bool SetBackgroundColor(float R, float G, float B);
	/** Sets the object color for the projected image.
		@param R Red component value in range [0;1], where 0 is darkest. 
		@param G Red component value in range [0;1], where 0 is darkest. 
		@param B Red component value in range [0;1], where 0 is darkest. 
		@return False, if R, G, or B is out of range. */
	bool SetObjectColor(float R, float G, float B);

	/** Returns true, if Initialize() has been called, otherwise false.
		@return True, if class is initialized. */
	bool IsInitialized() const{return m_IsInitialized;}
	/** Returns true, if DefineCamera() has been called.
		@return True, if camera is defined. */
	bool IsCameraDefined() const{return m_IsCameraDefined;}
	/** Returns width of camera set by a previous call to DefineCamera().
		@return Width in pixels of camera. If not defined return values is undefined. */
	unsigned int GetWidth() const{return m_Width;}
	/** Returns height of camera set by a previous call to DefineCamera().
		@return Height in pixels of camera. If not defined return values is undefined. */
	unsigned int GetHeight() const{return m_Height;}
	/** Returns the image center as (GetWidth()/2,GetHeight/2). */
	inline CPoint2D<float> GetImageCenter() const;
	/** Returns focal length of camera set by a previous call to DefineCamera().
		@return Focal length in pixels of camera. If not defined return values is undefined. */
	double GetFocalLength() const{return m_f;}
	/** Returns total triangles that the current CAD model is made of. */
	inline unsigned int GetTotalTriangles(){return m_Triangles.size();}
	/** Returns x-position of camera. If SetPose() has not been previously called, the
		return value is undefined.
		@return X-position of camera. */
	inline double GetX() const;
	/** Returns y-position of camera. If SetPose() has not been previously called, the
		return value is undefined.
		@return Y-position of camera. */
	inline double GetY() const;
	/** Returns z-position of camera. If SetPose() has not been previously called, the
		return value is undefined.
		@return Z-position of camera. */
	inline double GetZ() const;
	/** Returns tilt (a) of camera. If SetPose() has not been previously 
		called, the return value is undefined.
		@param InRadians If true (default), then returned value is in radians,
			otherwise in degrees.
		@return Tilt of camera. */
	inline double GetTilt(bool InRadians=true) const;
	/** Returns pan (b) of camera. If SetPose() has not been previously called,
		the return value is undefined.
		@param InRadians If true (default), then returned value is in radians,
			otherwise in degrees.
		@return Pan of camera. */
	inline double GetPan(bool InRadians=true) const;
	/** Returns roll (c) of camera. If SetPose() has not been previously called,
		the return value is undefined.
		@param InRadians If true (default), then returned value is in radians,
			otherwise in degrees.
		@return Roll of camera. */
	inline double GetRoll(bool InRadians=true) const;

	/** Reverses all surface normals, i.e. rotates all surface normal vectors 180 degrees.
		This is sometimes necessary, if a CAD model has opposite normal compared to what
		is used here. */
	void InverseSurfaceNormals();

	/**@name streaming operators */
	//@{
	/** writes object to stream, format is:
		\verbatim
		
		CVirtualCam IsCameraDefined: 1/0 
		 Width:       640
		 Height:      480
		 FocalLength: 1500.0
		\endverbatim
		where IsCameraDefined is either true (1) or false (0). Only
		the camera definitions will be saved, not the actual pose and CAD data.
		Note: Initialize() is automatically called by this streaming operator.
		@author Implementation by Ren� Dencker Eriksen (edr@mip.sdu.dk)
		@version 0.1 */
	friend ostream& operator<<(ostream& s, const CVirtualCam& Cam);

	/** Reads input from given stream into this class. Stream must be in correct format
		according to how the << ostream operator formats output.
		@author Implementation by Ren� Dencker Eriksen (edr@mip.sdu.dk)
		@version 0.1 */
	friend std::istream& operator>>(std::istream& is, CVirtualCam& Cam);
	//@}


// Implementation
protected:
//	void CreateDisplayList(UINT nList);

	/** This is simply a copy of the function gluLookAt from the GLUT library. */
//	void LookAt(double eyex, double eyey, double eyez,
//		  double centerx, double centery, double centerz,
//		  double upx, double upy, double upz);
	/** Should be private - but due to some design issues not yet solved this method is
		at the momemt public. User should ignore this method! */
//	bool SetDCPixelFormat(HDC hDC, DWORD dwFlags);

	void SetProjectionMatrix();
	void SetModelMatrix();
	PBITMAPINFO CreateBitmapInfoStruct(HBITMAP hBmp);
	/** Should be private - but due to some design issues not yet solved this method is
		at the momemt public. User should ignore this method! */
	virtual void SetOpenGLState();
	/** Renders the OpenGL scene */
	virtual void RenderScene();
	void SetLight();
	inline void SkipToken(ifstream& is);
	/** Sorts the triangles from front to back in order to make the blending 
		(antialiasing) work correct. */
	void SortTriangles();
	void GetGLError();
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

inline double CVirtualCam::GetX() const
{
	return m_x;
}

inline double CVirtualCam::GetY() const
{
	return m_y;
}

inline double CVirtualCam::GetZ() const
{
	return m_z;
}

inline double CVirtualCam::GetTilt(bool InRadians) const
{
	if (InRadians==true)
		return m_a;
	else
		return ipl::RadToDegree(m_a);
}

inline double CVirtualCam::GetPan(bool InRadians) const
{
	if (InRadians==true)
		return m_b;
	else
		return ipl::RadToDegree(m_b);
}

inline double CVirtualCam::GetRoll(bool InRadians) const
{
	if (InRadians==true)
		return m_c;
	else
		return ipl::RadToDegree(m_c);
}

inline CPoint2D<float> CVirtualCam::GetImageCenter()const
{
	return CPoint2D<float>(GetWidth()/2.0f,GetHeight()/2.0f);
}

inline void CVirtualCam::SkipToken(ifstream& is)
{
//	char c;
//	while(isspace(c=is.get()))
//	{
//	}
//	is.putback(c);
	while(isspace(is.peek()))
		is.get();
	is.ignore(20,' ');
}

} // end namespace videndo

/////////////////////////////////////////////////////////////////////////////

#endif//_CVIRTUALCAM_H
