#ifndef _VIDENDO_HOOPSCONTROL_H
#define _VIDENDO_HOOPSCONTROL_H

#include "HBaseView.h"
#include "HBaseModel.h"
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/corresponding_3d2d_points.h>
#include <ipl98/cpp/vectors/vector3d.h>
#include <points/point3d.h>
#include <videndo/camera_model.h>
#include <string>
#include <istream>
#include <ostream>

using ipl::CImage;
using ipl::CPerspective;
using ipl::CCorresponding3D2DPoints;
using ipl::TCameraParameters;
using ipl::CVector3D;
using std::string;
using std::istream;
using std::ostream;

class CMyHoopsDoc;

namespace videndo{ // use namespace if C++

/** CHoopsControl must be used with the Hoops reference application framework,
	located in HOOPS-700\demo\mfc\hoopsrefapp.

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	@version 0.50
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CHoopsControl{
	public: // attributes
		/// Set to true when Set() or operator>> has been successfully called
		bool m_Initialized;
		/// Set by Initialize
		HBaseView* m_pHView;
		/// Set by Initialize
		HBaseModel* m_pHoopsModel;
		/// Set by Initialize. Used to write and update text output window
		CMyHoopsDoc* m_pDoc;
		/// Set by SetDimensions() or operator>>
		bool m_DimensionsInUse;
		/// By default set to m_Width, changed by SetImageDimensions()
		unsigned int m_ImageWidth;
		/// By default set to m_Height, changed by SetImageDimensions()
		unsigned int m_ImageHeight;
		/// Set by Set() or operator>>, see also m_DimensionsInUse
		unsigned int m_Width;
		/// Set by Set() or operator>>, see also m_DimensionsInUse
		unsigned int m_Height;
		/** A focal length factor which is needed to make correspondance between the real focal length
			and the virtual focal length. */
		double m_FocalFactor;
		/** Set to false if given m_FocalFactor was never estimated. */
		bool m_FocalFactorInUse;
		/// Version of this class
		const static double m_Version;

		bool m_HoopsToCImageInitalized;
		char* m_hsra_options;
		char* m_hlr_options;
		char* m_current_hsra;
		HPixelRGB *m_image;
		HPixelRGB *m_image_row;
		char* m_image_segment;
		char* m_driver_segment;
		HC_KEY m_image_key;

	protected: // attributes
	public: // methods
		/// Constructor
		CHoopsControl();
		/// Destructor
		~CHoopsControl();
		/// Initializes members
		bool Initialize(HBaseView* pHView, HBaseModel* pHoopsModel, CMyHoopsDoc* pDoc);
		/** Sets m_FocalFactorInUse to true
			@param FocalFactor A factor to make the correspondance between virtual and real
				focal length correct, see CalibrateVirtualFocalLength() for more info. */
		bool SetFocalFactor(double FocalFactor);
		/** Sets image dimensions in this class, hence methods without dimension parameters 
			can be called, for instance SetCameraPose(Pos,a,b,c,FocalLength) and HoopsToCImage(Img). */
		bool SetDimensions(unsigned int Width, unsigned int Height);
		/** Sets image dimensions in this class, hence methods without dimension parameters 
			can be called, for instance SetCameraPose(Pos,a,b,c,FocalLength) and HoopsToCImage(Img). */
		bool SetImageDimensions(unsigned int Width, unsigned int Height);
		/// Returns true if Set() or operator>> has been successfully called
		bool InUse(){return m_Initialized;}
		/// Returns true if SetDimensions() or operator>> has been successfully called
		bool DimensionsInUse(){return m_DimensionsInUse;}
		/** Sets camera pose to given values and image dimensions. */
		bool SetCameraPose(const TCameraParameters& Par, unsigned int Width, unsigned int Height);
		/// Sets camera pose to given values, SetDimensions() must have been successfully called.
		bool SetCameraPose(const TCameraParameters& Par);
		/** Sets camera pose to given values and image dimensions.
			Angular values in radians. */
		bool SetCameraPose(const CPoint3D<double>& Pos, double a, double b, double c, double FocalLength,
							unsigned int Width, unsigned int Height);
		/** Sets camera pose to given values, SetDimensions() must have been successfully called.
			Angular values in radians. */
		bool SetCameraPose(const CPoint3D<double>& Pos, double a, double b, double c, double FocalLength);
		/// Return HOOPS camera up-vector
		CVector3D<double> GetUpVector() const;
		/// Return HOOPS camera target-vector
		CVector3D<double> GetTarget() const;
		/// Return HOOPS camera position
		CPoint3D<double> GetCameraPosition() const;

		/// Copies actual shown Hoops image to a CImage with given dimensions
		bool HoopsToCImage(CImage& Img, unsigned int Width, unsigned int Height);
		/// Copies actual shown Hoops image to a CImage, SetDimensions() must have been successfully called.
		bool HoopsToCImage(CImage& Img);
		/// Fast version of HoopsToCImage, must call HoopsToCImageInit() first
		bool HoopsToCImageFast(CImage& Img);
		/// Initializes variables used by HoopsToCImageFast
		bool HoopsToCImageInit();
		/// Returns version of this class
		double GetVersion() const;
		/** Calibrates the focal length to be used by HOOPS. For some reason it differes a few per mille
			compared to the real one. Don't need to load correct calibration since this method will
			calibrate the CalibImg image and use this result.
			@param CalibImg The real acquired image, the program will convert it to an ideal image. */
		bool CalibrateVirtualFocalLength(CImage& CalibImg, const string& WorldCoordFile,
										const string& ThresholdFile);

		/** Essentially the same as CalibrateVirtualFocalLength(), except the input is now directly a
			camera parameter set and a set of 2D/3D corresponding points. This is used to test the CAD
			pose with an ideal constructed camera setup. */
		bool TestVirtualVirtual(const TCameraParameters& Par, const CCorresponding3D2DPoints& PointSetsunsigned, 
								unsigned int Width, unsigned int Height);
		/// Writes errors for given focal factor - used for tests
		bool GetErrorsFocalFactor(CImage& CalibImg, const string& WorldCoordFile, 
												const string& ThresholdFile, double FocalFactor);

		/** Writes all data to stream. Format is:
			\verbatim
			CHoopsControlVersion: 0.5 

			# Image size. If Dimensions not in use, Width and Height should not be present
			DimensionsInUse: true
			Width: 768
			Height: 576

			# Factor between real focal length and the virtual one - a calibration
			# which for some reason is needed to make correspondance between real and virtual world
			# If FocalFactor not in use, FocalFactor should not be present
			FocalFactorInUse: 1
			FocalFactor: 0

			\endverbatim

			@version 0.3 */
		friend ostream& operator<<(ostream& os, const CHoopsControl& HoopsCtrl) throw(string);

		/** Reads input from given stream into this class. Stream must be in correct format
			according to how the << ostream operator formats output.
			@version 0.3 */
		friend istream& operator>>(istream& is, CHoopsControl& HoopsCtrl) throw(string);	
	private: // methods
		/** Calibrates given image, sets m_pCam->m_Par to ideal calibration results. That is, the 
			pose from real calibration (dx,dy,dz,a,b,c) is used, FocalLength is set to the
			current ideal value, beta=1 and p=0.
			Then creates an ideal image and returns the reference marks 2D positions. */
		bool GetIdealPoints(CCorresponding3D2DPoints& PointSets, TCameraParameters& CamParams,
						unsigned int& Width, unsigned int& Height,CImage& CalibImg, 
						const string& WorldCoordFile, const string& ThresholdFile);
		/// Returns the error between cad image of calibration plate contained in Img and the PointSets
		bool GetErrors(const CCorresponding3D2DPoints& PointSets,const CImage& Img,double &MeanError, double& MaxError);
		/** Golden search method to find best focal factor, used by CalibrateVirtualFocalLength */
		double CalibGoldenSearch(double ax,double bx,double cx,double tol,double *xmin,
					const CCorresponding3D2DPoints& PointSets, const TCameraParameters& Par,
					unsigned int Width, unsigned int Height);
		/// Releases memory if HoopsToCImageInit() has been called before, internal use only
		void HoopsToCImageReset();
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

} // end namespace videndo

#endif //_VIDENDO_HOOPSCONTROL_H
