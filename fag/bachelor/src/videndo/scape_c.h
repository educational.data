#ifndef _VIDENDO_SCAPEC_H
#define _VIDENDO_SCAPEC_H

#pragma warning(disable: 4786)

#include "hoops_control.h"
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/pixelgroup.h>
#include <videndo/camera_model.h>
#include <scape/core/scape_vec3dim.h>
#include <string>
#include <istream>
#include <ostream>
#include <vector>

using ipl::CImage;
using ipl::TCameraParameters;
using ipl::CPixelGroup;
using scape::CVec3Dim;
using std::string;
using std::istream;
using std::ostream;
using std::vector;

class CMyHoopsDoc;

class DBCell
{
	public:
		// in degrees
		double m_Rho;
		double m_Phi;
		double m_Theta;
		double m_Features[13];
		// used for determining roll
		bool m_UseEta21;
		// used for determining roll
		bool m_SignPositive;
		// used for determining roll
		double m_PrimaryAxisAngle;
		// used for determining distance
		double m_m00;
		DBCell& operator=(const DBCell& Source)
		{
			for(int x=0; x<13; x++)
				m_Features[x]=Source.m_Features[x];
			m_Rho=Source.m_Rho;
			m_Phi=Source.m_Phi;
			m_Theta=Source.m_Theta;
			m_UseEta21=Source.m_UseEta21;
			m_SignPositive=Source.m_SignPositive;
			m_PrimaryAxisAngle=Source.m_PrimaryAxisAngle;
			m_m00=Source.m_m00;
			return *this;
		}
};

class CGuess
{
	public:
		DBCell m_Rec;
		double m_DistSq;
		CGuess& operator=(const CGuess& Source)
		{
			m_Rec=Source.m_Rec;
			m_DistSq=Source.m_DistSq;
			return *this;
		}
};

namespace videndo{ // use namespace if C++

/** CScapeC must be used with the Hoops reference application framework,
	located in HOOPS-700\demo\mfc\hoopsrefapp.

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	@version 0.50
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CScapeC{
	public: // attributes
		CHoopsControl m_HoopsControl;
	private: // attributes
		/// Set to true when Initialize() has been successfully called
		bool m_Initialized;
		/// Set to true when SetTraining() or operator>> has been successfully called
		bool m_TrainingInitialized;
		/// Set by Initialize(). Used to write and update text output window
		CMyHoopsDoc* m_pDoc;
		/// By default set to m_Width, changed by SetImageDimensions()
		unsigned int m_ImageWidth;
		/// By default set to m_Height, changed by SetImageDimensions()
		unsigned int m_ImageHeight;
		/// Set by Set() or operator>> 
		unsigned int m_Width;
		/// Set by Set() or operator>> 
		unsigned int m_Height;
		/// Set by Set() or operator>> 
		double m_FocalLengthIdeal;
		/// Set to true when SetCamera() or operator>> has been successfully called
		bool m_CameraInitialized;
		/// Camera object, initialized by SetCamera() or operator>> 
		CCameraModel* m_pCam;
		/** Set to true if cam params was read from a calibration file,
			in this case the m_CalibFile member will hold the file name. */
		bool m_CalibFileInUse;
		/// Set by Set() or operator>> 
		string m_CalibFile;
		/// Set by Set() or operator>> 
		double m_PhiMin;
		/// Set by Set() or operator>> 
		double m_PhiMax;
		/// Set by Set() or operator>> 
		double m_PhiStepSize;
		/// Set by Set() or operator>> 
		double m_ThetaMin;
		/// Set by Set() or operator>> 
		double m_ThetaMax;
		/// Set by Set() or operator>> 
		double m_ThetaStepSize;
		/// Set by Set() or operator>> 
		string m_SavePath;
		/// Set by Set() or operator>> 
		string m_FilePrefix;
		/// Number of 3x3 blurs to perform on original CAD image before deriving features
		unsigned int m_BlurFactor;
		/// Compression level for the PNG genereated files (9 is highest compression)
		unsigned short m_CompressionLevel;
		/// Version of this class
		const static double m_Version;
		/// Total number of Hu moment features
		const static int m_TotalFeatures;
		/// Set to true when a feature database has been read
		bool m_FeaturesInUse;
		/// Feature database, set by ReadFeatures().
		CVec3Dim<DBCell> m_Features;
		/// Variance of each feauture, calculated by ReadFeatures().
		vector<double> m_Variance;
		/** Mean size of all derived pixelgroups in feature database, just for statistical use,
			only set if m_FeaturesInUse is true. */
		double m_MeanBlobSize;
	protected: // attributes
	public: // methods
		/// Constructor
		CScapeC();
		/// Destructor
		~CScapeC();
		/// Initializes members
		bool Initialize(CMyHoopsDoc* pDoc);
		/// Returns true if Initialize() or operator>> has been successfully called
		bool InUse() const{return m_Initialized;}
		/** Sets up camera in m_pCam, m_CameraInitialized to true.
			@param CalibFile A CPerspective calibration file. To set camera pose directly as input
				see other version of SetCamera() in this class. */
		bool SetCamera(unsigned int Width, unsigned int Height, double FocalLengthIdeal, unsigned int BlurFactor, const string& CalibFile);
		/// Same as other Set(), except the camera pose and f is provided in CamParams instead of a calibration file
		bool SetCamera(unsigned int Width, unsigned int Height, double FocalLengthIdeal, unsigned int BlurFactor, const TCameraParameters& CamParams);
		/// Returns true if SetCamera() or operator>> has been called
		bool CameraInUse() const{return m_CameraInitialized;}
		/// Returns current camera parameters in m_pCam. Note: Camera must be in use
		bool GetCameraParams(TCameraParameters& Par) const;
		/// Sets training parameters. Angular values in degrees
		bool SetTraining(double PhiMin, double PhiMax, double PhiStepSize, double ThetaMin, double ThetaMax, 
			  double ThetaStepSize, const string& SavePath, const string& FilePrefix, unsigned short CompressionLevel);
		/// Returns true if Initialize() or operator>> has been successfully called
		bool TrainingInUse() const{return m_TrainingInitialized;}
		/// Returns version of this class
		double GetVersion() const;
		/** Returns name of training file with data as: SavePath/FilePrefix_training_Dim_ImageWidth_ImageHeight.txt */
		string GetTrainingFileName(unsigned int Width, unsigned int Height) const;
		/** Returns filename as Path/images/FilePrefix_rho_phi_theta_Dim_768,576.png, where the 768*576 will
			be the current width and height set for this class. */
		string GetFileName(const string& Path, const string& FilePrefix,double Rho, double Phi, double Theta) const;
		/** Returns filename as SavePath/images/FilePrefix_rho_theta_phi_Dim_768_576.png, where the 768*576 will
			be the current width and height set for this class. Training must be initialized. */
		string GetFileName(double Rho, double Phi, double Theta) const;
		/// Returns the current save path for training images - training must be initialized
		string GetSavePath() const{return m_SavePath;}
		/// Returns the current file prefix for training images - training must be initialized
		string GetFilePrefix() const{return m_FilePrefix;}
		/// Returns the file name of feature file, SavePath/FilePrefix_training_features_Blur_3_Dim_768_576.txt
		string GetFeatureFileName(unsigned int BlurFactor, unsigned int Width, unsigned int Height);
		/// Same as other GetFeatureFileName(), except Path and FilePrefix are supplied
		string GetFeatureFileName(const string& Path, const string& FilePrefix, unsigned int BlurFactor, 
			unsigned int Width, unsigned int Height);
		/// Returns ideal focal length, used for training
		inline double GetFocalLenghtIdeal(){return m_FocalLengthIdeal;}
		/// Returns the blur factor, class must be in use
		inline unsigned int GetBlurFactor(){return m_BlurFactor;}
		/// Sets the blur factor, class should be in use - will be overwritten if loading settings
		inline void SetBlurFactor(unsigned int Factor){m_BlurFactor=Factor;}
		/// Set new dimensions for images - does not change the fundamental image size related to camera
		bool SetImageDimensions(unsigned int Width, unsigned int Height);
		/// Returns image width
		inline GetImageWidth(){return m_ImageWidth;}
		/// Returns image height
		inline GetImageHeight(){return m_ImageHeight;}
		/// Returns current camera distance Rho - at the moment it is constant
		inline double GetRho();
		/// Returns minimum Phi (m_PhiMin)
		inline double GetPhiMin(){return m_PhiMin;}
		/// Returns miximum Phi (m_PhiMax)
		inline double GetPhiMax(){return m_PhiMax;}
		/// Returns Phi training stepsize
		inline double GetPhiStepSize(){return m_PhiStepSize;}
		/// Returns minimum Theta (m_ThetaMin)
		inline double GetThetaMin(){return m_ThetaMin;}
		/// Returns miximum Theta (m_ThetaMax)
		inline double GetThetaMax(){return m_ThetaMax;}
		/// Returns Theta training stepsize
		inline double GetThetaStepSize(){return m_ThetaStepSize;}
		/// Returns average blob sizes when features was derived, only valid if m_FeaturesInUse is true. */
		inline double GetMeanBlobSize(){return m_MeanBlobSize;}
		/** Returns closest values of Rho, Phi, and Theta present in database.
			Note: Rho, Phi, and Theta are all both input and return values.
			Input parameters in degress. */
		inline void GetRoundedValues(double& Rho,double& Phi,double& Theta);
		/** Returns closest indexes in database. Indexes returned in given parameters,
			always integer values.
			Note: Rho, Phi, and Theta are all both input and return values.
			Input parameters in degress. */
		inline unsigned int GetClosestIndex(double& Rho,double& Phi,double& Theta);
		/** Creates a full training set of images, training must be initialized.
			Will use the current image dimensions set in this class.
			Saves a file m_SavePath/m_FilePrefix_training.txt with the following info:
			\verbatim
			# Training data for a CAD model
			HoopsSetupFile: D:/home/edr/projects/SCAPEHoops/TrainingSetup/Lego3Bricks
			SavePath: d:/temp2/TrainingHoops/
			FilePrefix: Lego3Bricks

			TotalFiles: 2025

			# Rho  Phi  Theta  UpVector  Filename
			-760.5    0.0    2.0     (0.0,1.0,0.0)  d:/temp2/TrainingHoops/Lego3Bricks_-760.5_000.0_002.0.png
			-760.5    8.0    2.0     (0.0,1.0,0.0)  d:/temp2/TrainingHoops/Lego3Bricks_-760.5_008.0_002.0.png
			...
			\endverbatim */
		bool TrainObject(const string& HoopsSetupFile) throw(string);
		/** Derives Hu moment features from information in given file, the file format must correspond
			to the one written by TrainObject(). Writes file returned by GetFeatureFileName(), 
			with Width, Height and BlurFactor read from given file.
			@param BlurFactor Features can be derived with a blur factor different from current settings. */
		bool DeriveFeatures(const char* pFilePathName, unsigned int BlurFactor) throw(string);
		/** Reads features from file generated by DeriveFeatures(). If Width, Height, and/or BlurFactor
			has changed, user will be noted and new values will be set in this class. Sets m_FeaturesInUse
			to true. */
		bool ReadFeatures(const char* pFilePathName) throw(string);
		/// Finds best matches between given HuFeatures and database, ReadFeatures() must be called first.
		bool Recognize(const vector<double>& Rec, vector<CGuess>& Guesses, unsigned int TotalGuesses);
		/// Same as Recognize(), but with two cams. Must supply a second feature vector and a cam offset
		bool Recognize2Cams(const vector<double>& Rec, const vector<double>& Cam2Rec, double CamOffset, 
							 vector<CGuess>& Guesses, unsigned int TotalGuesses);
		/// Loads setup by calling friend streaming operator<< for this class
		void LoadSetup(const char* pFilePathName) throw(string);
		/** Writes all data to stream. Format is:
			\verbatim
			# CScapeC setup file by edr@mip.sdu.dk

			CScapeCVersion:   0.5

			# Camera setup. If Camera not in use, all camera tokens should not be present
			CameraInitialized: true

			# Image size 
			Width:       1024
			Height:      768

			# Calibration file
			CalibFile:   D:\home\edr\projects\SCAPEHoops\CalibSetup\ceropa_a2_plate_allimage\calib_matrix_a2_4_ideal1000.cfg

			# Camera parameters - alternative to using a calibration file
			# comment in the following lines and comment out the line with CalibFile
			#CamParameters:
			#dx: 248.688
			#dy: 370.017
			#dz: -614.989
			#a:  0.298234
			#b:  0.0376998
			#c:  -3.14792
			#FocalLength: 969.466	

			# Training setup, If training not in use, all training tokens should not be present
			TrainingInitialized: true

			# Spherical settings - Rho is fixed to cam pose distance
			# values in degrees
			PhiMin:      0
			PhiMax:      360
			PhiStepSize: 8

			ThetaMin:    0
			ThetaMax:    180
			ThetaStepSize: 4

			# Path to save
			SavePath:    d:/temp2/TrainingHoops
			FilePrefix:  Lego3Bricks

			# Compression level for the PNG genereated files (9 is highest compression)
			CompressionLevel: 1
			
			'stream m_HoopsControl'
			
			\endverbatim

			@version 0.3 */
		friend ostream& operator<<(ostream& os, const CScapeC& ScapeC) throw(string);

		/** Reads input from given stream into this class. Stream must be in correct format
			according to how the << ostream operator formats output.
			@version 0.3 */
		friend istream& operator>>(istream& is, CScapeC& ScapeC) throw(string);	
		/// Retrieves the one and only pixelgroup in given CAD image, used by DeriveFeatures()
		bool GetGroupInImage(CPixelGroup& Grp,const CImage& Img);
		/** Returns the smallest 3D angle difference between the two set of angular poses
			All angles in radians! */
		double GetAngleDifference(double a0, double b0, double c0, double a1, double b1, double c1);
	private: // methods
		inline unsigned int GetTotalRhoSteps() const;
		inline unsigned int GetTotalThetaSteps() const;
		inline unsigned int GetTotalPhiSteps() const;
		/** Returns the Phi index in db calculated from given PhiIndex and camera
			offset CamOffset in degrees. It assumes Phi runs whole circle. Takes care
			off roll over at 0 and 360 degrees. */
		unsigned int GetPhiIndexOffset(unsigned int PhiIndex, double CamOffset) throw(string);
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

inline double CScapeC::GetRho()
{
	return -m_pCam->GetPinHole().GetDist();
}

inline unsigned int CScapeC::GetTotalRhoSteps() const
{
	return 1; // fixed to one at the moment
}

inline unsigned int CScapeC::GetTotalThetaSteps() const
{
	double Val=(m_ThetaMax-m_ThetaMin)/m_ThetaStepSize;
	if (Val != (unsigned int)(Val))
		return (unsigned int)(Val+1);
	else
		return (unsigned int)Val;
}

inline unsigned int CScapeC::GetTotalPhiSteps() const
{
	double Val=(m_PhiMax-m_PhiMin)/m_PhiStepSize;
	if (Val != (unsigned int)(Val))
		return (unsigned int)(Val+1);
	else
		return (unsigned int)Val;
}

inline void CScapeC::GetRoundedValues(double& Rho,double& Phi,double& Theta)
{
	Rho=-m_pCam->GetPinHole().GetDist();
	if (((((int)Phi % (int)m_PhiStepSize)) +GetPhiMin()) >= (m_PhiStepSize/2))
		Phi = ((int)((Phi/m_PhiStepSize)+1))*m_PhiStepSize;
	else
		Phi = ((int)(Phi/m_PhiStepSize))*m_PhiStepSize;
	Phi -= m_PhiMin; // sub the half step for phi
	if (Phi>m_PhiMax)
		Phi=m_PhiMin;

	if (((((int)Theta % (int)m_ThetaStepSize))+GetThetaMin()) >= (m_ThetaStepSize/2))
		Theta = ((int)((Theta/m_ThetaStepSize)+1))*m_ThetaStepSize;
	else
		Theta = ((int)(Theta/m_ThetaStepSize))*m_ThetaStepSize;
	Theta -= m_ThetaMin; // sub the half step for theta
	if (Theta>m_ThetaMax)
		Theta=m_ThetaMin;

}

inline unsigned int CScapeC::GetClosestIndex(double& Rho,double& Phi,double& Theta)
{
	Rho=0; // fixed to one distance at the moment
	if (((int)(Phi+GetPhiMin()) % (int)m_PhiStepSize) >= (m_PhiStepSize/2))
		Phi = ((int)((Phi/m_PhiStepSize)+1));
	else
		Phi = ((int)(Phi/m_PhiStepSize));
	if (Phi>GetTotalPhiSteps())
		Phi=0;

	if (((int)(Theta+GetThetaMin()) % (int)m_ThetaStepSize) >= (m_ThetaStepSize/2))
		Theta = ((int)((Theta/m_ThetaStepSize)+1));
	else
		Theta = ((int)(Theta/m_ThetaStepSize));
	if (Theta>GetTotalThetaSteps())
		Theta=0;
}

} // end namespace videndo

#endif //_VIDENDO_SCAPEC_H
