#ifndef _VIDENDO_MARKFINDER_H
#define _VIDENDO_MARKFINDER_H

#pragma warning(disable: 4786)

#include "videndo_setup.h" /* always include the videndo setup file */
//#include "camera_firewire/camera_firewire_unibrain.h"
//#include <camera/camera_firewire/camera_firewire_unibrain.h> 
#include <camera/camera_firewire/camera_firewire_cmu.h> 
#include "mark_retrieval.h"
#include "mark_pose.h"
#include <string>
#include <istream>
#include <ostream>
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/algorithms/perspective.h>
#include <ipl98/cpp/vectors/vector3d.h> 

namespace videndo{ // use namespace if C++

using std::string;
using std::istream;
using std::ostream;
using ipl::CImage;
using ipl::CPerspective;
using ipl::CVector3D;
using ipl::CCameraFirewireCMU;

/** This class is capable of finding reference marks (defined by Videndo) in 
	a given calibrated scene. For each reference mark with a unique identification
	number, the (x,y) position and orientation is returned. 
	
	As input the class needs a setup file containing a few setup parameters 
	for finding the marks and a list of the reference marks identification numbers 
	present in the scene. If the scene does not	contain all reference mark id's 
	present in the setup file, the algorithms will be slower since it keeps 
	searching for the missing marks with all possible parameter variations.

	In addition, the setup file must contain references to two other setup files.
	One file must contain the firewire camera setup, this file is written by
	the class CCameraFirewireUnibrain (can be created by the executable FirewireGUI 
	program). The second file is a calibration file. This setup file is created by
	the class CPerspective in the IPL98 library (can be created by the executable
	Calibration.exe).

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CMarkFinder videndo/mark_finder.h
	@version 0.2
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CMarkFinder{


	private: // attributes
		/** Current version of this class, defined in mark_finder.cpp. */
		static const double m_Version;
		/** Holds the points in the MarkFinder setup file to be found during runtime. */
		CMarkRetrieval m_MarkRetrieval;
		/** Calibration file name. Relative to m_SetupPath. */
		string m_CalibrationFileName;
		/** Camera setup file name. Relative to m_SetupPath. */
		string m_CameraSetupFileName;
		/** Internal data for found reference marks, set by FindMarks(). */
		vector<CMarkPose> m_MarkPoses;
		/** Internal data for found 2D mark centers, set by FindMarks(), */
		vector<CPoint2D<double> > m_MarkCenters;
		/** Internal data for found 2D code start centers, set by FindMarks(), */
		vector<CPoint2D<double> > m_CSCenters;
		/// Contains information about what reference mark types to be used
		unsigned int m_RefMarkTypes;
		/// If true, new Id's are added to the pointsets to be identified. Set by SetAutoSearch()
		bool m_AutoSearchEnabled;
		/// Height for new marks found. Set by SetAutoSearch()
		double m_Height;
	protected: // attributes
		/** Set to true if a call to Initialize() succeeds. */
		bool m_Initialized;
		/** Calibration object, loads calibration from file m_CalibrationFileName. */
		CPerspective m_Perspective;
		/** Camera object. */
		CCameraFirewireCMU m_Cam;
		/** Setup path. */
		string m_SetupPath;
		/** Prefix for all setup files. */
		string m_FilePrefix;
		/** Image stored in this variable. */
		CImage m_Img;
		/** Contains an error message if a method fails. */
		string m_ErrorMessage;
	public: // methods
		/// default constructor
		CMarkFinder();

		/// default destructor
		~CMarkFinder();

		/** Initialize this class with setup file path and prefix for setup file.
			File name and path is "SetupPath/FilePrefix_MarkFinderSetup.cfg", where
			SetupPath and FilePrefix are the two supplied parameters.
			Call this method again if SetupPath and FilePrefix needs to be changed.
			Previous stored information will still be available in this class.
			Loads the setup from given setup file, starts camera if available.
			@param SetupPath Path to setup file (relative or absolute). 
			@param FilePrefix Prefix for setup file.
			@return False, if setup path does not exist or if one of the parameters contains an invalid string. */
		bool Initialize(const string& SetupPath, const string& FilePrefix);
		/** Returns true, if a previous call to Initialize() succeeded. Note: It only says
			if a general setup file was successfully loaded, it does not imply wether the
			camera (m_Cam) and calibration (m_Perspective) attributes are initialized, use 
			IsCamInitialized() and IsCalibrationInitialized() for this purpose.
			@see IsCamInitialized
			@see IsCalibrationInitialized */
		bool IsInitialized() const;
		/** Returns true if camera is initialized.
			@see GetCameraSetupPathFileName */
		bool IsCamInitialized() const;
		/** Returns true if calibration is initialized.
			@see GetCalibrationPathFileName */
		bool IsCalibrationInitialized() const;

		/** Returns the version of this class. */
		static double GetVersion();

		/** Returns setup path provided by call to Initialize(). If not initialized, an empty string is returned. */
		string GetSetupPath() const;
		/** Returns file prefix provided by call to Initialize(). If not initialized, an empty string is returned. */
		string GetFilePrefix() const;
		/** Returns setup path and file name as: SetupPath/FilePrefix_MarkFinderSetup.cfg,
			where SetupPath and FilePrefix are the two parameters supplied to the Initialize() method.
			@return If class not initialized an empty string is returned. */
		string GetSetupPathFileName() const;
		/** Set path and file name for camera setup. Previous information in this class will be overwritten.
			It is not checked whether the file exists or not!
			It is recommended to use extension cfg.
			@param CamSetupFile Relative to this class SetupPath. Must include extension. */
		bool SetCameraSetupFile(const string& CamSetupFile);
		/** Returns camera path and file name.
			@return If not available an empty string is returned. */
		string GetCameraSetupPathFileName() const;
		/** Set path and file name for calibration setup. Previous information in this class will be overwritten.
			It is not checked whether the file exists or not!
			It is recommended to use extension cfg.
			@param CalibSetupFile Relative to this class SetupPath. Must include extension. */
		bool SetCalibrationSetupFile(const string& CalibSetupFile);
		/** Returns calibration path and file name.
			@return If not available an empty string is returned. */
		string GetCalibrationPathFileName() const;

		/** Starts the firewire camera. If camera already started, the method returns
			true without doing anything. Note: If the setup contains a valid camera setup
			file, the camera is automatically loaded when this setup is read, i.e. no need
			for calling this method afterwards.
			@return False, if no camera found or if more than one is connected. */
		bool StartCamera();	

		/** Finds all reference marks in the image.
			@param MarkPoses If method returns true, the size of MarkPoses equals the number of
				found reference marks. Each object in this array contains both mark position and direction.
			@param Time Total time [sec] for finding marks returned in this parameter.
			@return False, if image could not be acquired or if no reference marks are found. */
		bool FindMarks(vector<CMarkPose>& MarkPoses,double& Time);

		/** For test purposes. Same functionality as the other FindMarks(). */
		bool FindMarks(CImage& Source, vector<CMarkPose>& MarkPoses,double& Time);

		/** Saves last image acquired by FindMarks().
			@param FoundMarksToImage If true, the found reference marks are plotted in the image.
			@param PathAndFileName Path and file name of saved image, extension may be left out. This
				method will always append extension 'bmp'. */
		bool SaveLastImage(const string& PathAndFileName, bool FoundMarksToImage);

		/** Saves a default setup file. Useful if configuration files have been corrupted.
			If a single camera is connected, this method connects and retrieves the default
			setup in the camera. This method can always be called without initializing this class.
			If given file already exists, this method fails, i.e. you cannot overwrite existing setups.
			@return False, if setup file already exists. */
		virtual bool SaveDefaultSetup(const string& SetupPath, const string& FilePrefix);

		/** Reads setup files. Usefull if user has changed some settings.
			Uses file information provided by a previous call to Initialize().
			Starts the camera if a valid camera setup file is present.
			@return False, if reading files fails, or if Initialize() has not been called. */
		virtual bool LoadSetup();

		/** Saves setup files.
			Uses file information provided by a previous call to Initialize().
			@return False, if saving files fails, or if Initialize() has not been called. */
		virtual bool SaveSetup();

		/** Stops the firewire camera. Should be called before exit to release the camera for other
			applications. 
			@return False, if stopping the camera fails. */
		bool StopCamera();

		/** Adds mark with given Id to existing marks.
			@param Id Id for mark to be added.
			@return False, if Id is not unique, i.e. it already exists. */
		bool AddMark(int Id, double Height);

		/** Removes mark with given Id from existing marks.
			@param Id Id for mark to be removed.
			@return False, if Id is not present. */
		bool RemoveMark(int Id);

		/** Returns a list of new Id's found in the image. User can for instance call
			AddMark() to add these Id's to be found in future calls to FindMarks().
			This method iterates through all thresholds based on lighting in image,
			this means it is rather slow but will (most likely) find all Id's in the image.
			@param Time Total time [sec] for finding marks returned in this parameter.
			@return A list of new Id's found. */
		vector<int> SearchNewId(double Height, double& Time);

		/** For test purposes. Same functionality as the other SearchNewId(). */
		vector<int> SearchNewId(CImage& Source, double Height, double& Time);

		/** If Enable is true, a call to FindMarks() will add new marks if they are found using
			the set of thresholds needed. It will not search new threshold values in order to find
			new values, i.e. this is a fast way of finding new Id's. To search for new Id's for
			all lighting conditions call SearchNewId().
			@param Enable If true, new Id's found with existing lighting conditions are added.
			@param Height The new reference marks will be assigned this height. */
		void SetAutoSearch(bool Enable, double Height);

		/** Returns true if SetAutoSearch() was called with Enable=true.
			@return True, if auto searching is enabled. */
		bool IsAutoSearching() const;

		/** All functions that can fail, will write a message to an internal string. This string can
			be obtained by a following call to this method. */
		string GetLastError();

		/** Writes all data to stream. Format is:
			\verbatim
			# CMarkFinder setup written by CMarkFinder class from the Videndo library
			#
			# CMarkFinder class version
			CMarkFinder 0.3

			# Refmark types can be either REFMARK_ALL, REFMARK_SMALL, or REFMARK_BIG
			CMarkType: REFMARK_SMALL

			# Threshold iteration and limit values
			ThresholdLowerBound: 110
			ThresholdUpperBound: 126
			ThresholdStep:       2

			# Active identification marks
			# first value is ID number, second is height above plane in mm.
			IdentificationMarks:
			#Mark  7    -140
			Mark	1	-195
			Mark	2	-195
			#Mark	3	-195
			#Mark	4	-177
			#Mark	0	-177
			#Mark	5	-195
			#Mark	6	-177
			EndOfIdentificationMarks   # DO NOT DELETE THIS LINE!

			# Camera setup
			CameraSetupFile:  unibrain_firei400_camera.cfg
			#CameraNotInitialized

			# Calibration setup
			CalibrationSetupFile: A1_FlipCalibration3.cfg
			#CalibrationNotAvailable
			\endverbatim

			The file name following the token CalibrationSetupFile: should be a camera setup
			file written by the class CCameraFirewireCMU from the IPL98 Camera add-on library.
			This camera file can be created by using the camfirewire application.
			For more control about saving to files, see the method CCameraFirewireUnibrain::Save().
			@version 0.1 */
		friend ostream& operator<<(ostream& os, const CMarkFinder& MarkFinder) throw(string);

		/** Reads input from given stream into this class. Stream must be in correct format
			according to how the << ostream operator formats output.
			@version 0.1 */
		friend istream& operator>>(istream& is, CMarkFinder& MarkFinder) throw(string);
	private:
		/// Returns calibration file name as: SetupPath/m_CalibrationFileName
		string GetCalibrationPathFileName(const string& SetupPath) const throw(string);
		/// Returns camera setup file name as: SetupPath/m_CameraSetupFileName
		string GetCameraSetupPathFileName(const string& SetupPath) const throw(string);
		/// Returns threshold setup file name
		string GetThresholdFileName(const string& SetupPath, const string& FilePrefix) const;
		/// Finds the 3D poses and directions for each mark - called by FindMarks()
		bool GetPoses(vector<CMarkPose>& MarkPoses);
	protected:
		/** Plots relevant mark info to m_Img. */
		void PlotInfoInImage(CImage& Img);
		/// Returns setup file name as "SetupPath/FilePrefix_MarkFinderSetup.cfg"
		string GetSetupFileName(const string& SetupPath, const string& FilePrefix) const;
		/** Writes the settings with format as describe in operator<<, except the first
			token CMarkFinder. Note: operator<< simply writes the token CMarkFinder and
			then calls this method. Derived classes may owerwrite this method, call the 
			base method	and add additional setup info. */
		virtual bool WriteSetup(ostream& os) const throw(string);
		/** Reads the settings according to WriteSetup output format. Note: operator>> simply
			calls this method. Derived classes may owerwrite this method, call the base method
			and add code for reading additional setup info. */
		virtual bool ReadSetup(istream& is) throw(string);
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

} // end namespace videndo

#endif //_VIDENDO_MARKFINDER_H
