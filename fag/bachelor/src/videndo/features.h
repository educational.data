#ifndef _VIDENDO_FEATURES_H
#define _VIDENDO_FEATURES_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <vector>
#include <sstream>
#include <ipl98/cpp/pixelgroup.h>
#include <ipl98/cpp/algorithms/feature_extraction.h>
#include <ipl98/cpp/std_image.h>
#include <ipl98/cpp/error.h>

namespace videndo{ // use namespace if C++

using std::vector;
using std::ostringstream;
using ipl::CPixelGroup;
using ipl::TMoment;
using ipl::FLOAT32;
using ipl::UINT8;
using ipl::CStdImage;
using ipl::CError;
using ipl::IPL_ERROR;

/** CFeatures

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CFeatures videndo/features.h
	@version 0.50
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CFeatures{
	public: // attributes
	protected: // attributes
		/** Used by reference objects. New objects have their Id appended here, it's index will correspond
			with same index used in m_UseEta21 and m_SignPositive. */
		vector<unsigned int> m_Id;
		/// Used by reference objects. If value is false, Eta12 should be used instead
		vector<bool> m_UseEta21;
		/// Used by reference objects. Sign for found Eta21/Eta12 stored here
		vector<bool> m_SignPositive;
		/// Used by reference objects. Primary axis in range [0;2PI[ for reference object stored here
		vector<double> m_PrimaryAxisAngle;
		/// An error index returned by GetIndex() if given Id was not found
		static const unsigned int m_IndexError;
		/// Used to check if a moment is considered to be zero
		static const double m_ZeroAdjust;
		/// Used to check if a primary axis exists
		double m_Tolerance;
	public: // methods
		/// Default constructor, Tolerance defaults to 0.2, see PrimaryAxisExists() for more info.
		CFeatures();
		/// Default destructor
		~CFeatures();
		/// Removes all existing reference objects, see AddRefObject().
		void Empty();
		/** Sets a tolerance for the call to PrimaryAxisExists(). Default value in this
			class is 0.2
			@param Tolerance Must be in range [0;infinity]
			@return False, if Tolerance is out of range.
			@see PrimaryAxisExists */
		bool SetPrimaryAxisExistTolerance(double Tolerance);
		/** Add a reference object in order to be able to determine an absolute
			orientation with no ambiguity. 
			Internally this method decides wether Eta12 or
			Eta21 should be used for retrieving orientation for this object, it also
			stores the sign of the chosen Eta moment for this reference position.
			@param CentralMoments Moments calculated by ipl::CFeatures::DeriveCentralMoments(),
					note that moments up to third order must be available.
			@return False, if no primary axis exists. */
		bool AddRefObject(unsigned int Id, const TMoment& CentralMoments);
		/** Same as other version of this method, except input is the raw pixelgroup,
			central moments are calculated internally.
			@return False, if no primary axis exists. */
		bool AddRefObject(unsigned int Id, CPixelGroup& PixelGroup);
		/** Essentially the same as other version of this method, except input is the result
			from moment analasys directly.
			@param PrimaryAxisAngle In radians */
		bool AddRefObject(unsigned int Id, bool UseEta21, bool SignPositive, double PrimaryAxisAngle);
		/** Returns the internally stored values for resolving the PI ambiguity.
			@param PrimaryAxisAngle In radians */
		bool GetRefObject(unsigned int Id, bool& UseEta21, bool& SignPositive, double& PrimaryAxisAngle);
		/** Returns the angle of the primary axis for an object having the given CentralMoments, 
			interval is [0;2PI[. A reference object with a user chosen Id must be supplied by a 
			previous call to AddRefObject(). Note: If you want the angle difference of the
			primary axis for this object and the reference object call GetPrincipalAngleDiff()
			instead.
			@param CentralMoments Moments calculated by ipl::CFeatures::DeriveCentralMoments(),
					note that moments up to third order must be available.
			@return Angle in radians, -1 if no primary axis exists.  */
		double GetPrincipalAngle(unsigned int Id, const TMoment& CentralMoments);
		/** Same as other version of this method, except input is the raw pixelgroup,
			central moments are calculated internally.
			@return Angle in radians, -1 if no primary axis exists. */
		double GetPrincipalAngle(unsigned int Id, const CPixelGroup& PixelGroup);
		/** Returns the angle difference between this object and the reference object
			with given Id, interval is [0;2PI[. A reference object with a user chosen Id 
			must be supplied by a previous call to AddRefObject(). Note: If you want the
			absolute angle of the primary axis for this object call GetPrincipalAngle() instead.
			@return Angle in radians */
		double GetPrincipalAngleDiff(unsigned int Id, const TMoment& CentralMoments);
		/** Same as other version of this method, except input is the raw pixelgroup,
			central moments are calculated internally. */
		double GetPrincipalAngleDiff(unsigned int Id, const CPixelGroup& PixelGroup);
		/** Returns the principal axis angle. The returned interval is [0;PI[, i.e.
			there is a PI ambiguity. To avoid this use GetPrincipalAngle() instead.
			@param CentralMoments Moments calculated by ipl::CFeatures::DeriveCentralMoments(),
					note that moments up to third order must be available.
			@param Tolerance Value to be used for the internal call to PrimaryAxisExists. Defaults to 0.2.
			@return Angle in radians, -1 if no primary axis exists. */
		static double GetPrincipalOrientation(const TMoment& CentralMoments, double Tolerance=0.2);
		/** Same as other version of this method, except input is the raw pixelgroup,
			central moments are calculated internally.
			@param Tolerance Value to be used for the internal call to PrimaryAxisExists. Defaults to 0.2.
			@return Angle in radians, -1 if no primary axis exists. */
		static double GetPrincipalOrientation(const CPixelGroup& PixelGroup, double Tolerance=0.2);
		/** If (L1/L2) < 1+Tolerance, where L1>L2, then no primary 
			axis exists. Here L1 and L2 is the eigen-values of the matrix A with entries
			a00=My20, a10=a01=My11, and a11=My02. Tolerance is a parameter to this method.
			@return True, if (L1/L2) > 1+Tolerance as explained above. */
		static bool PrimaryAxisExists(const TMoment& CentralMoments, double Tolerance);

		/** Finds center of mass with subpixel accuracy by finding interpolated
			edge positions in the blob. Edge positions are found by max gradient.
			This implementation is probably not as fast as it could be.
			The blob cannot be closer to the border than MIN_BORDER_DIST (set 
			to 5 in the algorithm).	This is a safety margin to ensure that the 
			search for max gradient will not pass the border. Probably need a 
			better strategy for this matter.
			@param Grp Pixel group to find center of mass from. Must have top,
				left, right and bottom available.
			@param Img Must be image from which	Grp was derived and must 
				have 8 b/p graytone palette.
			@return Interpolated center of mass. */
		static CPoint2D<double> GetInterpolatedCenterOfMass(const CPixelGroup& Grp, 
															const CStdImage& Img, UINT8 Background);

	private: // methods
		static inline void GetEtaValues(double& Eta02, double& Eta20, double& Eta21, double& Eta12,
									const TMoment& CentralMoments, double Phi);
		/// From a pixel group, this derives the central moments. Given Pixelgroup is treated as binary
		static bool GetMoments(const CPixelGroup& PixelGroup, ipl::CFeatures& Features);
		/// Returns corresponding index for given Id, return m_IndexError if not found
		unsigned int GetIndex(unsigned int Id);
		/// Returns true, if Id already exists
		bool IdExists(unsigned int Id);
		/// Returns the result of atan(2*My11/(My02-My20))/2.0, returns PI/2 if My02-My20==0!
		static inline double GetPrimaryPhi(const double& My11,const double& My20,const double& My02);
		
		/// Find parabolic extremum from the three given points. Used by GetInterpolatedCenterOfMass().
		static inline float GetParabolicExtremum(CPoint2D<float> A, CPoint2D<float> B, CPoint2D<float> C);
		/// Returns x-direction gradient at given position. Used by GetInterpolatedCenterOfMass().
		static inline float GetXGradient(int i, int j, const CStdImage& Img);
		/// Returns y-direction gradient at given position. Used by GetInterpolatedCenterOfMass().
		static inline float GetYGradient(int i, int j, const CStdImage& Img);
		/** Returns interpolated max gradient position in x-direction from given start point.
			Used by GetInterpolatedCenterOfMass().
			@param XPos The integer pixel at which the max gradient was found.
			@return Interpolated x-position with max gradient. */
		static inline float GetXMaxGradInterpolated(int i,int j,const CStdImage& Img, int& XPos);
		/** Returns interpolated max gradient position in y-direction from given start point.
			Used by GetInterpolatedCenterOfMass().
			@param YPos The integer pixel at which the max gradient was found.
			@return Interpolated y-position with max gradient. */
		static inline float GetYMaxGradInterpolated(int i,int j,const CStdImage& Img, int& YPos);
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////
inline void CFeatures::GetEtaValues(double& Eta02, double& Eta20, double& Eta21, double& Eta12, 
									const TMoment& CentralMoments, double Phi)
{
	double sinphi=sin(Phi);
	double cosphi=cos(Phi);
	double sin2phi=sinphi*sinphi;
	double cos2phi=cosphi*cosphi;
	double sin3phi=sin2phi*sinphi;
	double cos3phi=cos2phi*cosphi;
	
	const double& My11=CentralMoments.m11;
	const double& My20=CentralMoments.m20;
	const double& My02=CentralMoments.m02;

	const double& My30=CentralMoments.m30;
	const double& My03=CentralMoments.m03;
	const double& My12=CentralMoments.m12;
	const double& My21=CentralMoments.m21;

	Eta02 = My20*sin2phi + My02*cos2phi + 2*My11*sinphi*cosphi;
	Eta20 = My20*cos2phi + My02*sin2phi - 2*My11*sinphi*cosphi;
	Eta21 = My30*cos2phi*sinphi + My21*(cos3phi-2*cosphi*sin2phi) + 
			My12*(sin3phi-2*cos2phi*sinphi) + My03*cosphi*sin2phi;
	Eta12 = My30*cosphi*sin2phi - My03*cos2phi*sinphi + My12*(cos3phi-2*cosphi*sin2phi) +
			My21*(2*cos2phi*sinphi-sin3phi);;
}

inline double CFeatures::GetPrimaryPhi(const double& My11,const double& My20,const double& My02)
{
	if ((My02-My20)==0)
		return ipl::PI/2.0;
	else
		return atan(2*My11/(My02-My20))/2.0;
}

inline float CFeatures::GetParabolicExtremum(CPoint2D<float> A, CPoint2D<float> B, CPoint2D<float> C)
{
	float bma=B.GetX()-A.GetX();
	float bmc=B.GetX()-C.GetX();
	float fbmfc=B.GetY()-C.GetY();
	float fbmfa=B.GetY()-A.GetY();
	float Div=(bma*fbmfc-bmc*fbmfa);
	if (Div!=0)
	{
		float x=B.GetX()-0.5*(bma*bma*fbmfc-bmc*bmc*fbmfa)/Div;
		return x;
	}
	else
	{
		// we have a straight line - simply use B.x
//		ostringstream ost;
//		ost << "CFeatures::GetParabolicExtremum() Singularity - parabolic ectremum not found" << IPLAddFileAndLine;
//		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return B.GetX();
	}
}

inline float CFeatures::GetXGradient(int i, int j, const CStdImage& Img)
{
	float Grad=((float)((float)Img.GetPixelFast(i+1,j)-Img.GetPixelFast(i-1,j))/2.0);
	return Grad;
}

inline float CFeatures::GetYGradient(int i, int j, const CStdImage& Img)
{
	float Grad=((float)((float)Img.GetPixelFast(i,j+1)-Img.GetPixelFast(i,j-1))/2.0);
	return Grad;
}

// finds the sub-pixel position in x-direction at which the gradient is largest starting
// at (i,j). XPos is asigned the int value at which
// the max-gradient was found
inline float CFeatures::GetXMaxGradInterpolated(int i,int j,const CStdImage& Img, int& XPos)
{
	float Grad0, Grad1, Grad2;
	bool Continue=true;
	// find index i at which we have max gradient
	while(Continue==true)
	{
		Grad0=GetXGradient(i-1,j,Img);
		Grad1=GetXGradient(i,j,Img);
		Grad2=GetXGradient(i+1,j,Img);
		if (((Grad0<=Grad1) && (Grad1>=Grad2)) || ((Grad0>=Grad1) && (Grad1<=Grad2)))
		{
			// we have a maximum at i,j
			Continue=false;
		}
		else if (Grad0*Grad2<0)
		{
			// opposite sign, we are at an extremum
			if (fabs(Grad0)>fabs(Grad2))
				--i;
			else
				++i;
		}
		else
		{
			Grad0=fabs(Grad0);
			Grad1=fabs(Grad1);
			Grad2=fabs(Grad2);
			if ((Grad0>Grad1) && (Grad0>Grad2))
				--i; // Grad0 is biggest - use that one
			else if ((Grad0<Grad2) && (Grad1<Grad2))
				++i; // Grad2 is biggest - use that one
			else
			{
				ostringstream ost;
				ost << "CFeatures::GetParabolicExtremum() Should not be possible to end here: "
					"Grad0>Grad1 and Grad2>Grad1" << IPLAddFileAndLine;
				CError::ShowMessage(IPL_ERROR,ost.str().c_str());
				Continue=false; // we just use this value
			}
		}
	}
	float Extremum=GetParabolicExtremum(CPoint2D<float>(i-1,Grad0),CPoint2D<float>(i,Grad1),CPoint2D<float>(i+1,Grad2));
	Extremum > 0 ? XPos = (int)Extremum : XPos = (int)(Extremum-1);
	return Extremum;
}

// finds the sub-pixel position in y-direction at which the gradient is largest starting
// at (i,j). YPos is asigned the int value at which
// the max-gradient was found
inline float CFeatures::GetYMaxGradInterpolated(int i,int j,const CStdImage& Img, int& YPos)
{
	float Grad0, Grad1, Grad2;
	bool Continue=true;
	// find index j at which we have max gradient
	while(Continue==true)
	{
		Grad0=GetYGradient(i,j-1,Img);
		Grad1=GetYGradient(i,j,Img);
		Grad2=GetYGradient(i,j+1,Img);
		if (((Grad0<=Grad1) && (Grad1>=Grad2)) || ((Grad0>=Grad1) && (Grad1<=Grad2)))
		{
			// we have a maximum at i,j
			Continue=false;
		}
		else if (Grad0*Grad2<0)
		{
			// opposite sign, we are at an extremum
			if (fabs(Grad0)>fabs(Grad2))
				--j;
			else
				++j;
		}
		else
		{
			Grad0=fabs(Grad0);
			Grad1=fabs(Grad1);
			Grad2=fabs(Grad2);
			if ((Grad0>Grad1) && (Grad0>Grad2))
				--j; // Grad0 is biggest - use that one
			else if ((Grad0<Grad2) && (Grad1<Grad2))
				++j; // Grad2 is biggest - try that one
			else
			{
				ostringstream ost;
				ost << "CFeatures::GetParabolicExtremum() Should not be possible to end here: "
					"Grad0>Grad1 and Grad2>Grad1" << IPLAddFileAndLine;
				CError::ShowMessage(IPL_ERROR,ost.str().c_str());
				Continue=false; // we just use this value
			}
		}
	}
	float Extremum=GetParabolicExtremum(CPoint2D<float>(j-1,Grad0),CPoint2D<float>(j,Grad1),CPoint2D<float>(j+1,Grad2));
	Extremum > 0 ? YPos = (int)Extremum : YPos = (int)(Extremum-1);
	return Extremum;
}

} // end namespace videndo

#endif //_VIDENDO_FEATURES_H
