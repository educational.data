#include "telemetrics.h"
#include <ipl98/cpp/arrays/array2d.h> 
#include <ipl98/cpp/algorithms/equation_solver.h>

using std::endl;
using ipl::CArray2D;
using ipl::CEquationSolver;

namespace videndo{ // use namespace if C++

CTelemetrics::CTelemetrics()
{
	m_IsInitialized=false;
}

bool CTelemetrics::	Initialize(vector<CCorresponding3D2DPoints>& VectorOfPointSets, unsigned int FixedId1, 
		unsigned int FixedId2, unsigned int FixedZId, unsigned int Width, unsigned int Height)
{
	if (VectorOfPointSets.size()<5) 
	{
		ostringstream ost;
		ost << "CTelemetrics::Initialize() Number of point sets must be at least 5 (current contains " << 
			VectorOfPointSets.size() << " pointsets)"	<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	// check for same number of points in each pointset, and same 3D points and Id's
	int i,j;
	for(i=1; i<VectorOfPointSets.size(); i++)
	{
		if (VectorOfPointSets[0].GetTotalPointSetsInUse()!=VectorOfPointSets[i].GetTotalPointSetsInUse())
		{
			ostringstream ost;
			ost << "CTelemetrics::Initialize() Not all setup files have same number of pointsets" 
				<< IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			return false;
		}
		for(j=0; j<VectorOfPointSets[0].GetTotalPointSetsInUse(); j++)
		{
			unsigned int Id=VectorOfPointSets[0].GetId(j);
			unsigned int Index;
			if (VectorOfPointSets[i].GetIndex(Index,Id)==false)
			{
				ostringstream ost;
				ost << "CTelemetrics::Initialize() Id=" << Id << " not found in all pointset files" 
					<< IPLAddFileAndLine;
				CError::ShowMessage(IPL_ERROR,ost.str().c_str());
				return false;
			}
			else
			{
				if (VectorOfPointSets[0].GetPoint3D(j)!=VectorOfPointSets[i].GetPoint3D(Index))
				{
					ostringstream ost;
					ost << "CTelemetrics::Initialize() Id=" << Id << 
						" does not have same 3D point in all pointset files" << IPLAddFileAndLine;
					CError::ShowMessage(IPL_ERROR,ost.str().c_str());
					return false;
				}
			}
		}
	}
	if (VectorOfPointSets[0].IdExists(FixedId1)==false)
	{
		ostringstream ost;
		ost << "CTelemetrics::Initialize() FixedId1=" << FixedId1 << 
			" does not does not exist in pointset files" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (VectorOfPointSets[0].IdExists(FixedId2)==false)
	{
		ostringstream ost;
		ost << "CTelemetrics::Initialize() FixedId2=" << FixedId2 << 
			" does not does not exist in pointset files" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (VectorOfPointSets[0].IdExists(FixedZId)==false)
	{
		ostringstream ost;
		ost << "CTelemetrics::Initialize() FixedZId=" << FixedZId << 
			" does not does not exist in pointset files" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	// all checked - now set internal values
	m_VectorOfPointSets.resize(VectorOfPointSets.size());
	for(i=0;i<VectorOfPointSets.size();i++)
		m_VectorOfPointSets[i]=VectorOfPointSets[i];
	m_Cameras.resize(VectorOfPointSets.size());
	m_FixedId1=FixedId1;
	m_FixedId2=FixedId2;
	m_FixedZId=FixedZId;
	m_Width=Width;
	m_Height=Height;
	m_IsInitialized=true;
	return true;
}



bool CTelemetrics::Adjust3DPoints(CCorresponding3D2DPoints& PointSets,bool Verbose,ostringstream& TxtOut)
{
	int i,p,q;
	int k=m_VectorOfPointSets[0].GetTotalPointSets();
	ostringstream ost;
	for(i=0;i<m_Cameras.size();i++)
	{
		m_Cameras[i].SetImageDimensions(m_Width,m_Height);
		m_Cameras[i].CalibrateWithRadialDist(m_VectorOfPointSets[i]);
	}
	float E0,E1;
	for (p=0;p<3;p++)
	{
		E0=10000;
		E1=GetGrandAverageError();
		if (Verbose)
		{
			TxtOut << " p=" << p << " Error=" << E1 << endl;
			TxtOut << " Adjusting 3D points and ext. pose" << endl;
		}
		for (q=0;q<100;q++)
		{
			for (int r=0;r<2;r++)
			{
				for (i=0;i<m_VectorOfPointSets[0].GetTotalPointSets();i++)
				{
					if ((m_VectorOfPointSets[0].GetId(i)!=m_FixedId1)&&
						(m_VectorOfPointSets[0].GetId(i)!=m_FixedId2))
					{
						if (m_VectorOfPointSets[0].GetId(i)==m_FixedZId) 
							UpdateMarkCoordinate(i,true,1.0); 
						else
							UpdateMarkCoordinate(i,false,1.0); 
					}
				}
			}
			for (i=0;i<m_Cameras.size();i++)
			{
				m_Cameras[i].CalibrateWithRadialDist(m_VectorOfPointSets[i]);
				m_Cameras[i].MatrixToParameters();
			}
			EqualizeInternalParameters(m_Cameras);
			for (i=0;i<m_Cameras.size();i++)
			{
				m_Cameras[i].m_Par.xh=512;
				m_Cameras[i].m_Par.yh=384;
				m_Cameras[i].ParametersToMatrix();
				// Update external parameters
				m_Cameras[i].DeriveCameraPose(m_VectorOfPointSets[i],m_Cameras[i],true);
			}
			E1=GetGrandAverageError();
			if (E1<E0) 
				E0=E1; 
			else if (E1<0.5)
				break;
			if (q%10==0)
			{ 
				if (Verbose)
					TxtOut << "  q=" << q << " Error=" << E1 << endl;
			}
		}
		if (Verbose)
			TxtOut << "  q finished: Error=" << E1 << endl;
		for (i=0;i<5;i++) 
			AdjustRadialCenter(m_Cameras[i],m_VectorOfPointSets[i]);
		EqualizeInternalParameters(m_Cameras);
		E1=GetGrandAverageError();
//		float test=GetResidual();
		if (Verbose)
			TxtOut << "Adjusted radial center: Error=" << E1 << endl;
		TxtOut << " New internal parameters:" << endl;
		GetInternalParameters(TxtOut);
	}
	m_ResidualAfter=GetGrandAverageError();
	// Copy the adjusted point set to the returned parameter
	PointSets.Empty();
	for(i=0; i<m_VectorOfPointSets[0].GetTotalPointSets(); i++)
	{
		PointSets.AddPoint3DAndId(m_VectorOfPointSets[0].GetPoint3D(i),m_VectorOfPointSets[0].GetId(i));
	}
	return true;
}

void CTelemetrics::GetInternalParameters(ostringstream& TxtOut)
{
	TxtOut << "  f=" << m_Cameras[0].GetParameter(6) << endl;
	TxtOut << "  xh=" << m_Cameras[0].GetParameter(7) << endl;
	TxtOut << "  yh=" << m_Cameras[0].GetParameter(8) << endl;
	TxtOut << "  p=" << m_Cameras[0].GetParameter(9) << endl;
	TxtOut << "  beta=" << m_Cameras[0].GetParameter(10) << endl;
	TxtOut << "  k=" << m_Cameras[0].GetParameter(11) << endl;
	TxtOut << "  Rad Center=" << CPoint2D<float>(m_Cameras[0].GetParameter(12),
		m_Cameras[0].GetParameter(13)) << endl;
}


float CTelemetrics::GetAverageError(const CPerspectiveAdjust& Camera, const CCorresponding3D2DPoints& PointSets) const
{
	FLOAT32 sum;
	CPoint2D<FLOAT32> P20,Del;
	int jtot=PointSets.GetTotalPointSets();
	sum=0;
	for (int j=0;j<jtot;j++)
	{	
		Camera.Calc3DTo2DRad(PointSets.GetPoint3D(j),P20);
		Del=P20-PointSets.GetPoint2D(j);
		sum+=Del.GetX()*Del.GetX()+Del.GetY()*Del.GetY();
	}
	return sqrt(sum/jtot);
}

float CTelemetrics::GetGrandAverageError() const
{
	FLOAT32 sum;
	CPoint2D<FLOAT32> P20,Del;
	int itot=m_Cameras.size();
	int jtot=m_VectorOfPointSets[0].GetTotalPointSets();
	sum=0;
	for (int i=0;i<itot;i++)
	{
		for (int j=0;j<jtot;j++)
		{	
            m_Cameras[i].Calc3DTo2DRad(m_VectorOfPointSets[i].GetPoint3D(j),P20);
			Del=P20-m_VectorOfPointSets[i].GetPoint2D(j);
			sum+=Del.GetX()*Del.GetX()+Del.GetY()*Del.GetY();
		}
	}
	return sqrt(sum/itot/jtot);

}

void CTelemetrics::AdjustRadialCenter(CPerspectiveAdjust& Camera, const CCorresponding3D2DPoints& PointSets)
{
	int j,StepX,StepY;
	unsigned int CenterX,CenterY;
	CenterX=Camera.GetCenterX();
	CenterY=Camera.GetCenterY();
	float E,E0;
    StepX=5;
    StepY=5;
	int h;
	h=0;
	int k;
	E0=GetAverageError(Camera,PointSets);
    bool Changed;
	for ( j=0;j<2;j++)
	{
		k=0;
		E0=GetAverageError(Camera,PointSets);
		do
		{ 
			Changed=false;
			CenterX+=StepX;
			Camera.SetCenterX(CenterX);
			E=GetAverageError(Camera,PointSets);
			if (E<E0) 
			{
				E0=E;
				Changed=true;
			}
			else
			{
				CenterX-=2*StepX;
				Camera.SetCenterX(CenterX);
				E=GetAverageError(Camera,PointSets);
				if (E<E0)
				{ 
					StepX=-StepX;
					E0=E;
					Changed=true;
				} 
				else 
				{
					CenterX+=StepX;
					Camera.SetCenterX(CenterX);
				}
			}
			CenterY+=StepY;
			Camera.SetCenterY(CenterY);
			E=GetAverageError(Camera,PointSets);
			if (E<E0) 
			{
				E0=E;
				Changed=true;
			}
			else
			{
				CenterY-=2*StepY;
				Camera.SetCenterY(CenterY);
				E=GetAverageError(Camera,PointSets);
				if (E<E0)
				{ 
					StepY=-StepY;
					E0=E;
					Changed=true;
				} 
				else 
				{
					CenterY+=StepY;
					Camera.SetCenterY(CenterY);
				}
			}
			k++;
		}
		while (Changed && k<20);
		StepX/=5;
		StepY/=5;
	}
}

void CTelemetrics::UpdateMarkCoordinate(int MarkIndex,bool XYOnly,FLOAT32 SpatialStep)
{
   int TotalUnknowns;
   if (XYOnly)
	   TotalUnknowns=2; 
   else
	   TotalUnknowns=3;
   int i;
   CPoint2D<FLOAT32> P20;
   CPoint3D<FLOAT32> P3;
   vector<CPoint2D<FLOAT32> > Del;
   int TotalViews=m_Cameras.size();
   int TotalEqs=2*TotalViews;
   CArray2D<double> anm(TotalEqs,TotalUnknowns);
   vector<double> res;
   vector<double> right(TotalEqs);
   for (i=0;i<TotalViews;i++)
   { 
	   Del=GetSpatialPartialDerivative(m_Cameras[i],m_VectorOfPointSets[i],MarkIndex,SpatialStep);	   
	   P3=m_VectorOfPointSets[i].GetPoint3D(MarkIndex);
	   m_Cameras[i].Calc3DTo2DRad(P3,P20);  
	   anm[i*2][0]=Del[0].GetX();
	   anm[i*2+1][0]=Del[0].GetY();
	   anm[i*2][1]=Del[1].GetX();
	   anm[i*2+1][1]=Del[1].GetY();
	   if (!XYOnly) 
	   { 		   
		   anm[i*2][2]=Del[2].GetX();
		   anm[i*2+1][2]=Del[2].GetY();
	   }
	   right[2*i]=P20.GetX()-m_VectorOfPointSets[i].GetPoint2D(MarkIndex).GetX();
	   right[2*i+1]=P20.GetY()-m_VectorOfPointSets[i].GetPoint2D(MarkIndex).GetY();
   }
   res=CEquationSolver::LinearOverDeterminedSolve(anm,right);
   for (i=0;i<TotalViews;i++)
   {
	   P3=m_VectorOfPointSets[i].GetPoint3D(MarkIndex);
	   if (XYOnly) 
		   P3.Set(P3.GetX()-res[0],P3.GetY()-res[1],P3.GetZ());
	   else  
		   P3.Set(P3.GetX()-res[0],P3.GetY()-res[1],P3.GetZ()-res[2]);
	   m_VectorOfPointSets[i].SetPoint3D(P3,MarkIndex);
   }
}

vector<CPoint2D<FLOAT32> > CTelemetrics::GetSpatialPartialDerivative(
		const CPerspectiveAdjust& Camera,const CCorresponding3D2DPoints& PointSets,
		int MarkIndex, FLOAT32 SpatialStep) const
{
	vector<CPoint2D<FLOAT32> > Result(3);
	CPoint2D<FLOAT32> P20,P2;
	CPoint3D<FLOAT32> P30,P3;
	P3=PointSets.GetPoint3D(MarkIndex);
	Camera.Calc3DTo2DRad(P30,P20);
	P3.Set(P30.GetX()+SpatialStep,P30.GetY(),P30.GetZ());
    Camera.Calc3DTo2DRad(P3,P2);
	Result[0]=(P2-P20)/SpatialStep;
	P3.Set(P30.GetX(),P30.GetY()+SpatialStep,P30.GetZ());
    Camera.Calc3DTo2DRad(P3,P2);
	Result[1]=(P2-P20)/SpatialStep;
	P3.Set(P30.GetX(),P30.GetY(),P30.GetZ()+SpatialStep);
    Camera.Calc3DTo2DRad(P3,P2);
	Result[2]=(P2-P20)/SpatialStep;
	return Result;
}

void CTelemetrics::EqualizeInternalParameters(vector<CPerspectiveAdjust>& Cameras) const
{
	int i,j;
	double Param[8]={0,0,0,0,0,0,0,0};
	double x;
	for (i=0;i<Cameras.size();i++)
	{
		for (j=0;j<8;j++)
		{
			Param[j]+=Cameras[i].GetParameter(j+6);
        }
	}
	for (i=0;i<Cameras.size();i++)
	{
		for (j=0;j<8;j++)
		{
			x=Param[j]/Cameras.size();
			Cameras[i].SetParameter(j+6,x);
			Cameras[i].ParametersToMatrix();
        }
	}
}

float CTelemetrics::GetResidualBefore() const
{
	vector<CPerspectiveAdjust> Cameras;
	Cameras=m_Cameras;
	int i;
	for(i=0;i<Cameras.size();i++)
	{
		Cameras[i].SetImageDimensions(m_Width,m_Height);
		Cameras[i].CalibrateWithRadialDist(m_VectorOfPointSets[i]);
	}

	EqualizeInternalParameters(Cameras);

	FLOAT32 sum;
	CPoint2D<FLOAT32> P20,Del;
	int itot=Cameras.size();
	int jtot=m_VectorOfPointSets[0].GetTotalPointSets();
	sum=0;
	for (i=0;i<itot;i++)
	{
		for (int j=0;j<jtot;j++)
		{	
            Cameras[i].Calc3DTo2DRad(m_VectorOfPointSets[i].GetPoint3D(j),P20);
			Del=P20-m_VectorOfPointSets[i].GetPoint2D(j);
			sum+=Del.GetX()*Del.GetX()+Del.GetY()*Del.GetY();
		}
	}
	return sqrt(sum/itot/jtot);
}


} // end namespace videndo
