#ifndef _VIDENDO_LOCALTRENDHOUGH_H
#define _VIDENDO_LOCALTRENDHOUGH_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/geometry/line2d.h> 
#include <ipl98/cpp/int_image.h> 
#include <vector>

namespace videndo{ // use namespace if C++

using ipl::CImage;
using ipl::CLine2D;
using ipl::CIntImage;
using std::vector;

/** This class is used for Hough-like image analyses based on local trends in 
	graytone images. In this version two cases are considered:
	\verbatim

	1) Straight gradient transformation to be used for detecting straight edges
	2) Straight ridge transformation to be used for detecting straight lines or ridges
	\endverbatim

	In both cases an 8 bit graytone image is copied to a 16 bit integer 
	image with graytones 64 times higher. Then subsequent blurring leads
	to images with low discretization noise. This is essential since 
	gradients and higer order differences are very sensitive to 
	discretization noise. The number of blurings (using a 3x3 mask with 
	weights 1/9) is determined by the parameter TotalPreBlur, set by constructor or
	reset by a call to SetTotalPreBlur(). A similar 
	bluring (PostBlur) in the Hough space is performed for noise reasons. 
	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CLocalTrendHough videndo/local_trend_hough.h
	@version 0.50
	@author Implementation by Ivar Balslev (ivb@mip.sdu.dk)*/ 
class CLocalTrendHough
{
  public:
	/** Constructor determining the size of the Hough space. The angle dimension
		of the Hough space is 360/AngleStepDegrees for gradient Hough
		and 180/AngleStepDegrees for ridge Hough. The dimension of the Hough space
		in the rho direction is always 360/AngleStepDegrees
		@param AngleStepDegrees Angular step size in the Hough space.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	CLocalTrendHough(float AngleStepDegrees);

	/// Default destructor
	~CLocalTrendHough();

	/** Sets the number of blurings (using a 3x3 mask with  weights 1/9) in 
		the original image to elliminate noise problems. Must be in the 
		range [1,7]. Defaults to 0.
		@param TotalPreBlur Number of times the mask should be applied.
		@return False, if TotalPreBlur is out of range [1,7]. */
	bool SetTotalPreBlur(unsigned int TotalPreBlur);

	/** Sets the number of blurings (using a 3x3 mask with  weights 1/9) in 
		the Hough space. Must be in the range [0,7]. The default value is 0.
		@param TotalPostBlur Number of times the mask should be applied.
		@return False, if TotalPostBlur is out of range [1,7]. */
	bool SetTotalPostBlur(unsigned int TotalPostBlur);

    /** Detection of straight, high-color lines or ridges. The region
        of interest is defined by UL (uppler left) and LR (lower right)
        corners. Edges can be converted to high-color ridges by
        edge detection on the blurred image.
        The weight used when updating the parameter space is
        the gray-tone times K2, where K2 is the numerically largest
        principal curvature value, i.e. eigenvalue of the 2x2 matrix
        formed by the second order derivatives. The weight is set to
        zero if the trace of the matrix is positive.
        The angle used in the Hough space is derived from the eigenvector
        of this matrix. 
		@param Source Is the source image assumed to be 8 bits per pixel.
		@param UL Upper left corner of the ROI
		@param LR Lower right corner of the ROI
		@param PreEdgeDetect Controls whether the source image should be
			transformed to an image with graytones proportional to the local
			absolute value of the gradient
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool StraightRidgeTransform(const CImage &Source, CPoint2D<int> UL, CPoint2D<int> LR,
           bool PreEdgeDetect=false);

    /** Detection of straight, high-color lines or ridges. 
        Edges can be converted to high-color ridges by
        edge detection on the blurred image.
        The weight used when updating the parameter space is
        the gray-tone times K2, where K2 is the numerically largest
        principal curvature value, i.e. eigenvalue of the 2x2 matrix
        formed by the second order derivatives. The weight is set to
        zero if the trace of the matrix is positive.
        The angle used in the Hough space is derived from the eigenvector
        of this matrix. 
		@param Source Is the source image assumed to be 8 bits per pixel.
		@param PreEdgeDetect Controls whether the source image should be
			transformed to an image with graytones proportional to the local
			absolute value of the gradient
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool StraightRidgeTransform(const CImage &Source, bool PreEdgeDetect=false);

	/** Detection of straight edges in a ROI. The region of interest is
		defined by UL (uppler left) and LR (lower right) corners.
		The gradient angle is derived from the local trend of the graytone.
		The weight used in the Hough space is the absolute value of the
		gradient.
		@param Source Is the source image assumed to be 8 bits per pixel.
		@param UL Upper left corner of the ROI.
		@param LR Lower right corner of the ROI.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */ 
	bool StraightGradientTransform(const CImage &Source,CPoint2D<int> UL, 
								CPoint2D<int> LR);

	/** Detection of straight edges. 
		The gradient angle is derived from the local trend of the graytone.
		The weight used in the Hough space is the absolute value of the
		gradient.
		@param Source Is the source image assumed to be 8 bits per pixel.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */ 
	bool StraightGradientTransform(const CImage &Source);

	/** Finds peeks in the Hough space. First the
		global maximum is found as the first peak. The subsequent peaks
		are the global maximum after the Hough amplitudes in a square
		about the previous peek is set to zero. The sides of the rectangle
		is 2xHalfsizeThetaDegrees times 2xHalfsideRhoPixels. A recommended
		value is 5 for both halfsides.
		The peaks are inserted in PointArray (private member) and also inserted
		as corresponding real space lines in LineArray
		The number of peaks/lines is Total Lines
		@param HalfsideThetaDegrees determines the theta component of the
			blocking window about each found peak in the Hough space.
		@param HalfsideThetaRhoPixels determines the rho component of the
			blocking window about each found peak in the Hough space.
		@param Determines the total number of peaks in the Hough space to be detected.
		@param LineArray is a vector of classes CLine2D<double> in which the
			output is stored. Absolute value of the gradient.
		@return False, if no previous "Transform" methods is called successfully.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool SearchLines(float HalfsideThetaDegrees, float HalfsideRhoPixels,
					int TotalLines, vector<CLine2D<double> > &LineArray);

	/** Draws the Houg amplitudes in an 24 bit per pixel image.
		@param View Destination image for the Hough space
		@param Gamma The gamma value for the relation between Hough amplitudes
		and the graytone of the image. Recommended value: 0.25.
		@return False, if no previous "Transform" methods is called successfully.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool DrawHoughSpace(CImage &View, double Gamma) const;

	/** Draws the Hough amplitudes and the lines found by SearchLines 
		so that lines appear as crosses of specified color.
		@param View Destination image for the Hough space.
		@param Gamma The gamma value for the relation between Hough amplitudes
			and the graytone of the image. Recommended value: 0.25 .
		@return False, if no previous "Transform" methods is called successfully.
		@param Color determines the color or the plotted crosses.*/
	bool DrawHoughSpaceWithPoints(CImage &View, double Gamma, int Color) const;

	/** Draws the lines found by SearchLines in the the image View, usually
		chosen to be the analysed (real) image.
		@param View is the output image assumed to be 8 bpp.
		@param LineArray is the array of lines found previously.
		@param PrintColor is the graytone (0..255) of the lines drawn.
		@return False, if no previous "Transform" methods is called successfully.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk)*/
	bool LinesToImage(CImage &View, vector<CLine2D<double> > &LineArray,int PointColor) const;
private:
    /** Number of 3x3 blur operations on the real image before transformation.
        Default value is 4 
        @author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
    unsigned int m_TotalPreBlur;
	/** Number of 3x3 blur operations in the Hough space after transformation.
		Default value is 2 
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	int m_TotalPostBlur;
	vector<CPoint2D<int> > m_PointArray;
	unsigned long int **hs,**hs1;
	CIntImage m_zInt;
	bool m_TransformationPerformed;
	bool m_Empty;
	bool m_GradientTransform;
	bool m_RidgeTransform;
	bool m_PreEdgeDetect;
	int m_ULX,m_ULY;
	int m_NR,m_NT0,m_NT,m_w,m_h;
	double m_dr,m_dt;
	float m_AngleStepDegrees;
	// methods
	void PrepareHoughTransformation(const CImage &Source);
	bool StraightTransform(const CImage &Source,CPoint2D<int> UL, CPoint2D<int> LR);
	void PointsToLines(vector<CLine2D<double> >&LineArray);
	void EdgeDetect();
	void UpdateHoughSpaceGradient(int x, int y);
	void UpdateHoughSpaceRidge(int x, int y);
	void SmoothHough();
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

} // end namespace videndo

#endif //_VIDENDO_LOCALTRENDHOUGH_H
