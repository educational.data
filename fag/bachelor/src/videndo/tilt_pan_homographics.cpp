#include "tilt_pan_homographics.h"

namespace videndo{ // use namespace if C++

CTiltPanHomographics::CTiltPanHomographics()
{
	m_IsInitialized=false;
	m_ConstantsCalculated=false;
	m_InverseConstantsCalculated=false;
	m_TiltPanAvailable=false;
}

CTiltPanHomographics::CTiltPanHomographics(float f)
{
	m_IsInitialized=Initialize(f);
	m_ConstantsCalculated=false;
	m_InverseConstantsCalculated=false;
	m_TiltPanAvailable=false;
}

bool CTiltPanHomographics::Initialize(float f)
{
	if (f <= 0) 
	{
		ostringstream ost;
		ost << "CTiltPanHomographics::Initialize() Provided focal lenght (" << f << ") is illegal"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	m_IsInitialized=true;
	m_f=f;
	return true;
}

bool CTiltPanHomographics::DeriveAngles(const CPoint2D<float> &P0)
{
	return DeriveAngles(P0.GetX(),P0.GetY());
}

bool CTiltPanHomographics::DeriveAngles(float u0, float v0)
{
	if (IsInitialized()==false) 
	{
		ostringstream ost;
		ost << "CTiltPanHomographics::DeriveAngles() Class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	m_p=atan(u0/sqrt(m_f*m_f+v0*v0));
	m_t=-atan(v0/m_f);
	m_TiltPanAvailable=true;
	m_ConstantsCalculated=false;
	m_InverseConstantsCalculated=false;
	CalculateConstants();
	return true;
}

bool CTiltPanHomographics::CalculateConstants() const
{
	if (!m_TiltPanAvailable) 
	{
		ostringstream ost;
		ost << "CTiltPanHomographics::CalculateConstants() Must call DeriveAngles() first"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	float csp,snp,cst,snt;
	csp=cos(m_p);
	snp=sin(m_p);
	cst=cos(m_t);
	snt=sin(m_t);
	m_Tuu=m_f*csp;
	m_Tuv=m_f*snt*snp;
	m_Tvv=m_f*cst;
	m_Tuf=-m_f*m_f*cst*snp;
	m_Tvf=m_f*m_f*snt;
	m_Nu=snp;
	m_Nv=-snt*csp;
	m_Nf=m_f*cst*csp;
	m_ConstantsCalculated=true;
	return true;
}

bool CTiltPanHomographics::CalculateInverseConstants() const
{
	if (!m_TiltPanAvailable) 
	{
		ostringstream ost;
		ost << "CTiltPanHomographics::CalculateInverseConstants() Must call DeriveAngles() first"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	float csp,snp,cst,snt;
	csp=cos(m_p);
	snp=sin(m_p);
	cst=cos(m_t);
	snt=sin(m_t);
	m_tuu=m_f*csp;
	m_tuf=m_f*m_f*snp;
	m_tvu=m_f*snt*snp;
	m_tvv=m_f*cst;
	m_tvf=-m_f*m_f*snt*csp;
	m_nu=-cst*snp;
	m_nv=snt;
	m_nf=m_f*cst*csp;
	m_InverseConstantsCalculated=true;
	return true;
}

bool CTiltPanHomographics::Transform(float u, float v, float &um, float &vm) const
{
	if ((!m_ConstantsCalculated) && (CalculateConstants()==false))
			return false;
	float Den=m_Nu*u+m_Nv*v+m_Nf;
	um=(m_Tuu*u+m_Tuv*v+m_Tuf)/Den;
	vm=(m_Tvv*v+m_Tvf)/Den;
	return true;
}

bool CTiltPanHomographics::Transform(float u, float v,float u0,float v0, float &um, float &vm) const
{
	if (IsInitialized()==false) 
	{
		ostringstream ost;
		ost << "CTiltPanHomographics::Transform() Class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	float fsq=m_f*m_f;
	float Den=(u0*u+v0*v+fsq)/m_f;
	float sq1=sqrt(fsq+v0*v0);
	float sq2=sqrt(fsq+v0*v0+u0*u0);
	um=(u*sq1-v*u0*v0/sq1-fsq*u0/sq1)/Den;
	vm=m_f*sq2/sq1*(v-v0)/Den;
	return true;
}

bool CTiltPanHomographics::BackTransform(float um, float vm, float &u, float &v) const
{
	if ((!m_InverseConstantsCalculated) && (CalculateInverseConstants()==false))
			return false;
	float Den=m_nu*um+m_nv*vm+m_nf;
	u=(m_tuu*um+m_tuf)/Den;
	v=(m_tvu*um+m_tvv*vm+m_tvf)/Den;
	return true;
}

bool CTiltPanHomographics::BackTransform(const CPoint2D<float> &In, CPoint2D<float> &Out) const
{
	if ((!m_InverseConstantsCalculated) && (CalculateInverseConstants()==false))
			return false;
	float u,v;
	BackTransform(In.GetX(),In.GetY(),u,v);
	Out.Set(u,v);
	return true;
}

bool CTiltPanHomographics::BackTransform(float um, float vm,float u0,float v0, float &u, float &v) const
{
	if (IsInitialized()==false) 
	{
		ostringstream ost;
		ost << "CTiltPanHomographics::BackTransform() Class not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	float fsq=m_f*m_f;
	float sq1=sqrt(fsq+v0*v0);
	float sq2=sqrt(fsq+v0*v0+u0*u0);
	float Den=(-u0*um/sq1-sq2/sq1*v0*vm/m_f+m_f)/m_f;
	u=(sq1*um/m_f+u0)/Den;
	v=(-um*u0*v0/sq1/m_f+sq2/sq1*vm+v0)/Den;
	return true;
}

} // end namespace videndo
