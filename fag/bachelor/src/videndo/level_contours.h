#ifndef _VIDENDO_LEVELCONTOURS_H
#define _VIDENDO_LEVELCONTOURS_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <points/point2d.h>
#include <ipl98/cpp/ipl98_types.h>
#include <ipl98/cpp/positiongroup2d.h>
#include <ipl98/cpp/byte_image.h>
#include <ipl98/cpp/pixelgroup.h>
#include <ipl98/kernel_c/ipl98_types.h>
#include <ipl98/cpp/error.h>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/algorithms/mask_operation.h>
#include <vector>
#include <sstream>

namespace videndo{ // use namespace if C++

using ipl::CPositionGroup2D;
using ipl::CByteImage;
using ipl::CPixelGroup;
using ipl::DIRECTION;
using ipl::CError;
using ipl::IPL_ERROR;
using std::vector;
using std::ostream;
using std::istream;
using std::ostringstream;
using ipl::NORTH;
using ipl::SOUTH;
using ipl::EAST;
using ipl::WEST;

/** The class CLevelContour has a very fast method for finding edge curves
	with subpixel accuracy and without externally given threshold.
	To initialize contour extraction, the user must call InitializeContourSearch(...).
	To initialize a following fix point extraction, the user must call
	InitializeFixPointSearch(...). Finally the user may call the method 
	RemoveGhostLines() to remove parallel lines very close to each other.
	The edge detection runs as follows:
	First the gradient magnitude G(x,y) is found in each point.
	2*G(x,y) is the largest of |f(x-1,y)-f(x+1,y)| and |f(x,y-1)-f(x,y+1)|
	where f(x,y) is the color at pixel position (x,y).
	An array of local maxima of G(x,y) with G values larger than
	a certain minimum (m_MinStartGradient) is found and sorted
	according to gradient magnitude (index 0 corresponds to largest
	gradient magnitude).
	The resulting list L[i] contains potential seeds for generating
	contour curves of interest. L contains (x,y). An instance
	of a binary image B for marks of erased potential seeds is made.
	The next steps runs according to the following pseudo code:
	\verbatim
	i=0
	Repeat
	{
	if (L[i].x,L[i].y) is not marked in the binary image B)
	{
	Follow contour starting from (L[i].x,L[i].y) using the
	threshold f(L[i].x,L[i].y). While treating each
	contour point an interpolation is performed, so
	each contour point contains (x,y) as well as
	(xfloat, yfloat), see later.
	Stop the contour following process until
	(contour is closed) or (image border edge reached) or
	(G is below a value)
	If the contour is not close, follow the contour
	starting from (L[i].x,L[i].y), but now in the opposite
	direction until (image border edge reached) or
	(G is below a value)
	Unless the contour is closed, join the two contour parts
	Mark points 3 pixel wide belt along the contour in a binary image B
	i++;
	}
	Insert contour in the contour list
	(vector of PositionGroup<CPoint2DContour>)
	unles the number of positions is too small
	}
	until list L is exhausted
	\endverbatim

	We assume that the contour points (x,y) have a gray tone
	larger than Threshold, and that at least one fourconntected
	neighbor has graytone lower than Threshold. The coordinates
	(xfloat,yfloat) are then obtained by first finding
	the largest of |f(x,y)-f(x-1,y)|, |f(x,y)-f(x+1,y)|,|f(x,y)-f(x,y-1)|,
	and |f(x,y)-f(x,y+1)|. The interpolation is performed along
	the direction with largest value. For example:
	\verbatim

	if |f(x-1,y)-f(x,y)| is largest then
	xfloat=x-(f(x,y)-Threshold)/(f(x,y)-f(x-1,y));
	yfloat=y;
	\endverbatim
	The codes for the generating L[i] and the contour data (x,y,xfloat,yfloat)
	are optimized. All contour points are ordered: in the direction of 
	incresaing index, the left side of the contour has low graytones, the
	right side has high gray tones.	DeriveCurvature calculates the curve length
	ds, the direction angle, atan2(dy,dx) and the curvature:
	(atan2(dy_plus,dx_plus)-atan2(dy_minus,dx_minus)/ds;
	associated with each contour point.
	Positive peaks in m_Curvature versus curves are convex parts of the contour
	of high-color objects (concave parts of low-color objects).
	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CLevelContours videndo/level_contours.h
	@version 0.67
	@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
class CLevelContours
{
protected:
	/** Container class to be used in the edge detection and sorting of the points 
		of maximum absolute gradient */
	class CPoint2DEdge : public CPoint2D<int>
	{
	public:
		int m_Gradient;
		int m_Graytone;
		bool operator < (const CPoint2DEdge &A)const {return (A.m_Gradient<m_Gradient);}
	};
public:
/** The class CPoint2DContour contains float 2D image positions,
	the color, the threshold, the gradient used in a subpixel
	interpolation, and the index in the position group containing
	the CPoint2DContour. The 2D trend is described by the
	arc lengtht, the curvature, and direction angle of the contour
	associated with the point.
	In addition PixelPos is the position of the the nearest above
	threshold pixel
	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CPoint2DContour videndo/level_contours.h
	@version 0.51
	@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
class CPoint2DContour : public CPoint2D<float>
{
public:
	/// Default constructor
	CPoint2DContour(){};
	/// Default destructor
	~CPoint2DContour(){};
	/// Constructor with initialization of point
	CPoint2DContour(float x,float y):CPoint2D<float>(x,y){};

	/** Gets the value of the gradient
		@return The value of the gradient */
	inline int GetGradient() const {return m_Gradient;}
	
	/** Gets the value of the gradient
		@return The value of the gradient */
	inline float GetArcLength() const {return m_ds;}
	
	/** Gets the index of the contour point
		when it is a member of a position group
		@return The index */
    inline int GetIndex() const {return m_Index;}
	
	/** Gets the value of the curvature
		@return The value of the curvature */
	inline float GetCurvature() const {return m_Curvature;}
	
	/** Gets the value of the direction angle
		@return The value of the direction angle */
	inline float GetDirectionAngle() const {return m_DirectionAngle;}
	
	/** Gets the color of the contour point
		@return The color value. */
	inline ipl::UINT32 GetColor() const {return m_Color;}

	/** Sets the gradient value
		@param x Is the graytone gradient associated with the point */
	inline void SetGradient(float x){m_Gradient=x;}
	
	/** Sets the arc length according to the subpixelpositions of
		the neighbor points in the position group.
		@param x Is the arc length associated with the point */
	inline void SetArcLength(float x){m_ds=x;}
	
	/** Sets the gradieng value
	    @param x Is the index in the position group to wich the point belongs */
	inline void SetIndex(int i){m_Index=i;}
	
	/** Sets the curvature value according to the subpixelpositions of
		the neigbor points in the position group.
		@param x Is the curvature associated with the point */
	inline void SetCurvature(float x){m_Curvature=x;}
	
	/** Sets the direction angle accorrding to the subpixel position
		of the neighbor points in the position group
		@param x Is the direction angle associated with the point */
	inline void SetDirectionAngle(float x){m_DirectionAngle=x;}
	
	/** Sets the color of the contour point */
	inline void SetColor(ipl::UINT32 Color){m_Color=Color;}

	/** Public member containing the position of the nearest pixel.
		Set by DeriveContour(). */
    CPoint2D<int> m_PixelPos;

	/**@name streaming operators */
	//@{
	/** writes a 2D contour point to stream as:
		CPoint2DContour (x,y)
		Gradient: grad 
		Color: color
		ArcLength: arclength
		Index: index
		Curvature: curvature
		DirectionAngle: dirangle */
	friend ostream& operator<<(ostream& s,const CPoint2DContour& P2DContour);
	/** Reads input from given stream into this class. Stream must be in correct
		format according to how the << ostream operator formats output.
		@version 0.2 */
	friend istream& operator>>(istream& is, CPoint2DContour& P2DContour);
	//@}

protected:
	int m_Gradient;
	ipl::UINT32 m_Color;
	float m_ds;
	int m_Index;
	float m_Curvature;
	/// range is [-PI;PI[
	float m_DirectionAngle;
};

/** The class CContour contains a collection of CPoint2DContour plus 
	information on the status closed/unclosed and the common  Color
	of the contours in case of level contours. */
class CContour : public CPositionGroup2D<CPoint2DContour>
{
public:
	/** Gets the closed/nonclosed status of the contour
		@return True if the contour is closed, otherwise false */
	inline bool GetClosed() const {return m_Closed;}
	
	/** Gets the (common) color of the contour
		@return The color of the contour */
    inline int GetColor() const {return m_Color;}
	
	/** Sets the status closed/nonclosed of the contour
		@param State Is the status to be assigned */
    inline void SetClosed(bool State){m_Closed=State;}
	
	/** Sets the common color of the contour
		@param Color is the color to be assigned */
    inline void SetColor(ipl::UINT32 Color){m_Color=Color;}

	/** Sets m_SeedPoint. Used to store the seed point information
		that generated this contour. */
	inline void SetSeedPoint(const CPoint2DEdge& P){m_SeedPoint=P;}

	/** Returns the seed point information. */
	inline const CPoint2DEdge& GetSeedPoint() const{return m_SeedPoint;}

	/** Load positions contained in file given by pPathAndFileName. Format must
		comply with the format described in the Save() method.
		@param pPathAndFileName Name of file including extension and relative
		or absolute path, f.ex. absolut path "c:/temp/positions.txt",
		relative "positions.txt".
		@return False if load failed
		@see Save */
	bool Load(const char* pPathAndFileName);
	/** Saves positions contained in this group. Format is:
		\verbatim
		# Data from a CContour object in the Videndo Library �
		CLevelContours::CContour
		Color: color
		Closed: closed
		TotalPositions: total
		(x0,y0)
		...
		\endverbatim
		Where 'color' is a UINT32 value, 'closed' is true or false, and 'total' is 
		the total number of positions and is followed by 'total' CPoint2DContour objects.
		If this class containes no objects, the CLevelContours::CContour is simply 
		followed by a line with the token "Empty".
		@param pPathAndFileName Name of file including extension and relative
		or absolute path, f.ex. absolut path "c:/temp/positions.txt",
		relative "positions.txt".
		@return False if save failed
		@see Load */
	bool Save(const char* pPathAndFileName);
	/** Writes only part of the group information to stream. Format is:
		\verbatim

		**************** PositionGroup2D info *****************
		Positions: Top=(0,0) Bottom=(0,0) Left=(0,0) Right=(0,0)
		Width=1 Height=1
		Total positions=0 Allocated positions=0
		\endverbatim
		The above is simply an example of an empty group. In order to save
		all information in this class, use method Save() instead. */
	friend ostream& operator<<(ostream& s,const CContour& Contour);
	/** Enumeration defining the two endpoint types for a contour, the
		enumerated names are: UNDEFINED=0, COLLISION=1, BORDER=2, LOWGRAD=3, JOINED=4. */
	enum ENDPOINTTYPE {UNDEFINED=0, COLLISION=1, BORDER=2, LOWGRAD=3, JOINED=4};
	/** Sets the low index end of the contour to given end point type.
		@param Val End point type for the low index position in contour. */
	inline void SetLowIndexType(ENDPOINTTYPE Val){m_LowIndexType=Val;}
	/** Sets the high index end of the contour to given end point type.
		@param Val End point type for the high index position in contour. */
	inline void SetHighIndexType(ENDPOINTTYPE Val){m_HighIndexType=Val;}
	/** Returns the type of the low index end of the contour. 
		@return End point type for the low index position in contour.*/
	inline ENDPOINTTYPE GetLowIndexType(){return m_LowIndexType;};
	/** Returns the type of the high index end of the contour. 
		@return End point type for the high index position in contour*/
	inline ENDPOINTTYPE GetHighIndexType(){return m_HighIndexType;};
protected:
	ENDPOINTTYPE m_LowIndexType, m_HighIndexType;
	int m_Color;
	bool m_Closed;
	CPoint2DEdge m_SeedPoint; // added by edr@mip.sdu.dk v. 0.53
};

	/** Constructor.
		@param TotalBlur Number of initial blur operations, defaults to 1. */
    CLevelContours(unsigned int TotalBlur=1);
	/// Default destructor
    ~CLevelContours(){};

	/** Derives all level contours in the image
		@param Source Is the source image assumed to be 8 bits per pixel
		@param Contours Is a vector of contours containing the level contours
			in the source image.
		@return False, if input image (Source) is empty.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool TreatImage(CByteImage &Source, vector<CContour> &Contours);
	
	/** Derives the level contours in an ROI of the image
		@param Source Is the source image assumed to be 8 bits per pixel
		@param UL Is the upper left point of the ROI
		@param LR Is the lower left point of the ROI
		@param Contours Is a vector of contours containing the level contours
			in the source image.
		@return False, if input image (Source) is empty.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	bool TreatImage(CByteImage &Source, CPoint2D<int> &UL,CPoint2D<int> &LR,
		vector<CContour> &Contours);
	
	/** Sets the number of initial blur operations to be performed on input image.
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeContourSearch() or by the use of the streaming operator.
		@param TotalBlur Number of initial blur operations, defaults to 1. */		
	inline void SetTotalBlur(unsigned int TotalBlur){m_TotalBlur=TotalBlur;}
	/** Returns the value of m_TotalBlur. */
	inline unsigned int GetTotalBlur()const{return m_TotalBlur;}

	/** Sets the member m_MinStartGradient, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeContourSearch() or by the use of the streaming operator.
		@param MinStartGradient See attribute member m_MinStartGradient for more info. */		
	inline void SetMinStartGradient(float MinStartGradient){m_MinStartGradient=MinStartGradient;}
	/** Returns the value of m_MinStartGradient. */
	inline float GetMinStartGradient() const {return m_MinStartGradient;}
	
	/** Sets the member m_MinContourSize, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeContourSearch() or by the use of the streaming operator.
		@param MinContourSize See attribute member m_MinContourSize for more info. */		
	inline void SetMinContourSize(unsigned int MinContourSize){m_MinContourSize=MinContourSize;}
	/** Returns the value of m_MinContourSize. */
	inline unsigned int GetMinContourSize() const{return m_MinContourSize;}
	
	/** Sets the member m_StartStopRatio, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeContourSearch() or by the use of the streaming operator.
		@param StartStopRatio See attribute member m_StartStopRatio for more info. */		
	inline void SetStartStopRatio(float StartStopRatio){m_StartStopRatio=StartStopRatio;}
	/** Returns the value of m_StartStopRatio. */
	inline float GetStartStopRatio() const {return m_StartStopRatio;}

	
	/** Sets the member m_MinStopGradient, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeContourSearch() or by the use of the streaming operator.
		@param MinStopGradient See attribute member m_MinStopGradient for more info. */		
	inline void SetMinStopGradient(float MinStopGradient){m_MinStopGradient=MinStopGradient;}
	/** Returns the value of m_MinStopGradient. */
	inline float GetMinStopGradient() const {return m_MinStopGradient;}

	/** Sets the member m_HalfWidth, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeFixPointSearch() or by the use of the streaming operator.
		@param HalfWidth See attribute member m_HalfWidth for more info. */		
	inline void SetHalfWidth(unsigned int HalfWidth){m_HalfWidth=HalfWidth;}
	/** Returns the value of m_HalfWidth. */
	inline unsigned int GetHalfWidth() const {return m_HalfWidth;}
	
	/** Sets the member m_CurvatureLimitEx, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeFixPointSearch() or by the use of the streaming operator.
		@param CurvatureLimitEx See attribute member m_CurvatureLimitEx for more info. */		
	inline void SetCurvatureLimitEx(float CurvatureLimitEx){m_CurvatureLimitEx=CurvatureLimitEx;}
	/** Returns the value of m_CurvatureLimitEx. */
	inline float GetCurvatureLimitEx() const {return m_CurvatureLimitEx;}

	/** Sets the member m_CurvatureLimitLin, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeFixPointSearch() or by the use of the streaming operator.
		@param CurvatureLimitLin See attribute member m_CurvatureLimitLin for more info. */		
	inline void SetCurvatureLimitLin(float CurvatureLimitLin){m_CurvatureLimitLin=CurvatureLimitLin;}
	/** Returns the value of m_CurvatureLimitLin. */
	inline float GetCurvatureLimitLin() const {return m_CurvatureLimitLin;}
	
	
	/** Sets the member m_MaxStraightnessDevation, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeFixPointSearch() or by the use of the streaming operator.
		@param MaxStraightnessDevation See attribute member m_MaxStraightnessDevation for more info. */
	inline void SetMaxStraightnessDevation(float MaxStraightnessDevation){m_MaxStraightnessDevation=MaxStraightnessDevation;}
	/** Returns the value of m_MaxStraightnessDevation. */
	inline float GetMaxStraightnessDevation() const {return m_MaxStraightnessDevation;}
	
	
	/** Sets the member m_MinDistGhostRemoval, see description of this attribute for more info. 
		This is an alternative way to change this variable, note that in order to
		iniitalize this class, it must at some point be set by a call to 
		InitializeFixPointSearch() or by the use of the streaming operator.
		@param MinDistGhostRemoval See attribute member m_MinDistGhostRemoval for more info. */
	void SetMinDistGhostRemoval(float MinDistGhostRemoval){m_MinDistGhostRemoval=MinDistGhostRemoval;}
	/** Returns the value of m_MinDistGhostRemoval. */
	float GetMinDistGhostRemoval() const {return m_MinDistGhostRemoval;}



	/** Derives the curvature and the arc length assisociated with each point
		The first and last point in each non-closed contours are not treated
		@param Contours Is a vector if contours to be treated. */
	void DeriveCurvature(vector<CContour> &Contours);
	

	/** Image containing the edge detected result obtained in the 
		TreatImage methods */
	CByteImage m_Edge;
	
	/** Initializes all parameters in this class.
		@param TotalBlur See m_TotalBlur for further description. Must be less than 5.
		@param MinStartGradient See m_MinStartGradient for further description. Must be greater than 0.
		@param MinContourSize See m_MinContourSize for further description. Must be greater than 0.
		@param StartStopRatio See m_StartStopRatio for further description. Must be greater than 0.
		@param MinStopGradient Used when deriving contours, ensures that the min.
			gradient is at least the value of this parameter. This prevents the
			risk of following extremely vague contours, typical value is 2.
		@return False, if one of the input parameters is out of range, see
			description for each parameter. */
	bool InitializeContourSearch(unsigned int TotalBlur, int MinStartGradient, int MinContourSize,
								 float StartStopRatio, float MinStopGradient);

	/** Returns true if Initialize() has been successfully called. */
	inline bool IsContourSearchInitialized() const {return m_IsContourSearchInitialized;}
	
	/** Initializes all parameters in this class.
		@param HalfWidth See SmoothCurvature() for further description. Must be >=0.
		@param CurvatureLimitEx Used by DeriveFixpoints, typical value is 0.5.
		@param CurvatureLimitLin Used by DeriveFixpoints, typical value is 0.2.
		@param MaxStraightnessDevation Used to split line segments with low curvature in 
			DeriveFixPoints(), typical value is 2 pixels.
		@param MinDistGhostRemoval Used when calling RemoveGhostLines(). If the
			distance between two lines are greater than this value, they will not
			be considered potential ghost lines, typical value is 3 pixels.
		@return False, if one of the input parameters is out of range, see
			description for each parameter. */
	bool InitializeFixPointSearch(unsigned int HalfWidth, float CurvatureLimitEx,
				float CurvatureLimitLin, float MaxStraightnessDevation,
				float MinDistGhostRemoval);

	/** Returns true if Initialize() has been successfully called. */
	inline bool IsFixPointSearchInitialized() const {return m_IsFixPointSearchInitialized;}

	/** Same as DeriveFixPoints(), except the two curvature parameters are not provided, they
		must be set by a previous call to InitializeFixPointSearch().
		@return False, if InitializeFixPointSearch() has not been successfully called. */
	bool DeriveFixpoints(CContour &SourceContour, vector<CPoint2DContour> &Extrema,
		vector<CContour> &LinearSegments);

	/** This method derives fixpoints and segments according the local variations
		of the curvature. Points in Extrema has local maxiumum absolute curvature
		above the CurvatureLimitEx. LinearSegments
		are characterized by points with absolute curvature lower than
		CurvatureLimitLin. The end points of LinearSegments has
		neighbors with absolute curvature larger than this limit.
		This means that LinarSegments are contour segments with low or
		zero curvature.
		@param CurvatureLimitEx Is the curvature threshold use for points with
		local curvature extrema
		@param CurvatureLimitLin Is the curvature threshold used for points in
		the linear segments
		@param SourceContour Is the contour treated
		@param Extrema Is a vector of contour points with local curvature extremum
		@param LinearSegments is a vector of contours with low absolute curvature.
			Only elements with number of pixels>m_MinContourSize are stored.
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	void DeriveFixpoints(float CurvatureLimitEx, float CurvatureLimitLin, 
		CContour &SourceContour, vector<CPoint2DContour> &Extrema,
        vector<CContour> &LinearSegments);

		/** This method derives fixpoints and segments according the local variations
		of the curvature. Points in Maxima/Minima has local maxiumum/mimimum absolute curvature
		above the CurvatureLimitEx. Points in the vector InflectionPoints has a neighbor with 
		opposite sign of the curvature. LinearSegments	are characterized by points 
		with absolute curvature lower than CurvatureLimitLin. End points of LinearSegments have
		neighbors with absolute curvature larger than this limit or
		are contour end points themselves.
		This means that LinarSegments are contour segments with low or
		zero curvature. Points in InflectionPoints has a neighbor with opposite
		sign of the curvature. Points in ArcSegments have values of the absolute
		curvature above CurvatureLimitLin. The end points are inflection points,
		contour end points, or have absolute curvature below the limit. 		
		@param CurvatureLimitEx Is the curvature threshold use for points with
		    local minimum/maximun of the absolute curvature
		@param CurvatureLimitLin Is the curvature threshold used for points in
		     the linear segments and arc segments
		@param SourceContour Is the contour treated
		@param Maxima Is a vector of contour points with local maximum of the absolute curvature 
		@param Minima Is a vector of contour points with local minimum or the absolute curvature 
		@param InflectionPoints Is a vector of contour points near sign changes of the curvature 
		@param LinearSegments is a vector of contours with low absolute curvature.
		@param ArcSegments is a vector of contours of points points with absolute 
		     curvature above a threshold. Except for the ends each point have the same sign
			 of curvature as its two neighbors.  
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */

	void DeriveFixpointsExtended(float CurvatureLimitEx, float CurvatureLimitLin, 
		CContour &SourceContour, 
		vector<CPoint2DContour> &Maxima,
		vector<CPoint2DContour> &Minima,
		vector<CPoint2DContour> &InflectionPoints,
        vector<CContour> &LinearSegments,
		vector<CContour> &ArcSegments);

	/**	This method operates on an arc segment with points of constant curvature sign.
		It derives the arc length sTotal and the distance sMedian from the low index end to 
		the median point. The median point is defined by the requirement that 
		the integrated curvature from one end to the median point is the 
		same as the integrated curvature from the median point to the other end. qn is given by
		qn=(sTotal)^(-n)*integral of (s-sMedian)^(n)*Curvature(s)ds, where the integral runs over
		all points and s is distance along the arc between the low index end and the running point.
		@param AS Is a contour containing points on an arc segment.
		@param sTotal Is the total arc length
		@param sMedian Is the distance from the low index end to the median point
		@param iMedian Is the the index on AS of the median point
		@param q0 Is the zero order arc moment
		@param q1 Is the first order arc moment
		@param q2 Is the second order arc moment */
	void DeriveArcDescriptors(CContour &AS,
				double &sTotal,double &sMedian,int &iMedian, double &q0,double &q1, double &q2);

	bool DeriveInsertedPoint(CByteImage &Src, CPoint2DContour &P, CPoint2DContour &P0, float Threshold);

	/** Replaces the value of m_Curvature in point P by
		the average of 2*Halfwidth+1 surrounding P. In case of open contours
		the curvature behaviour outside the end points is taken to be the
		mirrored behaviour about the end points.
		@param Contour Is the contour treated
		@param HalfWidth Is the halfwidth of the smoothing
		@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
	void SmoothCurvature(CContour &Contour, unsigned int HalfWidth);

	/** Same as SmoothCurvature(), except the HalfWidth parameter is not provided, it
		must be set by a previous call to InitializeFixPointSearch() or
		by reading the setup using the streaming operator.
		@return False, if InitializeFixPointSearch() has not been successfully called. */
	bool SmoothCurvature(CContour &Contour);

	/** If two line segments are close to each other, parallel and "overlapping",
		the	one with smallest max. gradient is removed. The method InitializeFixPointSearch()
		must be called prior to calling this method. Note, that all linear segments
		should be copied to the LinearSegments parameter provided here, not just segments
		extracted from one contour. Added by edr@mip.sdu.dk v. 0.53. */
	void RemoveGhostLines(vector<CContour> &LinearSegments) const;
	/** Same as other version, except the removed indexes from LinearSegments
		are written to the ost parameter. For DEBUG purposes. */
	void RemoveGhostLines(vector<CContour> &LinearSegments,ostringstream& ost) const;

	/** writes object settings only to stream, format is:
		\verbatim
		
		CLevelContours-Settings
			ContourSearchInitialized: 1
				TotalBlur:        1
				MinStartGradient: 15
				MinContourSize:   10
				StartStopRatio:   50
				MinStopGradient:  2
			FixPointSearchInitialized: 1
				HalfWidth:               3
				CurvatureLimitEx:        0.5
				CurvatureLimitLin:       0.1
				MaxStraightnessDevation: 2
				MinDistGhostRemoval:     3
		\endverbatim
		If ContourSearchInitialized is true, it is followed by the five parameter shown
		above, otherwise they will not be present. The same counts for 
		FixPointSearchInitialized.
		@author Implementation by Ren� Dencker Eriksen (edr@mip.sdu.dk)
		@version 0.2 */
	bool WriteSettings(ostream& os) const;
	/** Reads settings input from given stream into this class. Stream must be 
		in correct format according to how the method WriteSettings() formats 
		output.
		@author Implementation by Ren� Dencker Eriksen (edr@mip.sdu.dk)
		@version 0.2 */
	bool ReadSettings(istream& is);
	protected:
		/** Set to true by InitializeContourSearch(). This class may be used without calling
			initialize, but then the user must ensure to manually set all relevant
			parameters in this class. */
		bool m_IsContourSearchInitialized;
		/** Set by InitializeContourSearch(). Number of initial blur operations. Default = 1. */
		unsigned int m_TotalBlur;
		/** Set by InitializeContourSearch(). Minimum gradient (in graytones per pixel along steepest
			direction) of the members of m_MaxGradient. Default is 25. */
		int m_MinStartGradient;
		/** Set by InitializeContourSearch(). Miniumun size of contour stored. Default is 10. */
		unsigned int m_MinContourSize;
		/** Set by InitializeContourSearch(). Ratio between the gradient at the start of a contour and
			the gradient used in the stop criterion. Default is 30. */
		float m_StartStopRatio;
		/** Set by InitializeContourSearch(). Used by DeriveContours(). If 
			StopGradient = StartGradient/m_StartStopRatio is too small
			there is a risk of finding contours on surfaces. This parameter
			ensures that there will be a minimum stop gradient defined as:
			StopGradient = Max(StartGradient/m_StartStopRatio,m_MinStopGradient
			Default value is 2. */
		float m_MinStopGradient;

		/** Set to true by InitializeFixPointSearch(), or if the streaming operator has
			been used to read a setup for this class. */
		bool m_IsFixPointSearchInitialized;
		/// Set by InitializeFixPointSearch(). Used by one of the versions of DeriveFixPoints().
		unsigned int m_HalfWidth;
		/// Set by InitializeFixPointSearch(). Used by one of the versions of DeriveFixPoints().
		float m_CurvatureLimitEx;
		/// Set by InitializeFixPointSearch(). Used by one of the versions of DeriveFixPoints().
		float m_CurvatureLimitLin;
		/** Set by InitializeFixPointSearch(). Used to split line segments with low curvature in 
			DeriveFixPoints(). Default value is 2 pixels. */
		float m_MaxStraightnessDevation;
		/** Set by InitializeFixPointSearch(). Used when calling RemoveGhostLines(). If the
			distance between two lines are greater than this value, they will not
			be considered potential ghost lines. Default value is 3 pixels */
		float m_MinDistGhostRemoval;

		/** Vector of edge detected points with local maximum absolute gradient
			and higher absolute gradient than m_MinStartGradient. The points in
			this vector are potential seeds for level contours.
			@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
		vector<CPoint2DEdge> m_MaxGradient; //List of potential seed points
		CByteImage m_BinaryImg;
		CPoint2D<int> UL, LR;
		int m_xl, m_xr, m_yt, m_yb;
		CContour m_Contour; //protected container

		// protected methods
		bool DeriveContour(CByteImage &Source, int x0,const int y0, int StopGradient);
		void BlurAndEdgeDetect(CByteImage &Source,int MinGradient);
		inline void DeriveSubpixelPos(float &x1,float &y1,DIRECTION Dir,int x,int y,float f);
		/** Called by DeriveFixpoints(). It sets the max gradient for the new line
			segment LineSeg by calling LineSeg.SetSeedPoint(). This is used for a
			later removal of close line segments when calling RemoveGhostLines()
			@authour Ren� edr@mip.sdu.dk */
		void FindMaxGradientForLineSeg(CContour& LineSeg);

		/** Used to split line segments containing a low curvature. Called by DeriveFixPoints() */
		void DeriveStraightnessDevation(CContour &Source,float &Delta,int &Index);
		/** Used to split line segments containing a low curvature. Called by DeriveFixPoints() */
		void SplitContour(CContour &Source, CContour &C0,CContour &C1, int Index);
		/** Used to split line segments containing a low curvature. Called by DeriveFixPoints() */
		void AddToLineSegmentVector(vector<CContour> &LineSegments,CContour &Source);
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

inline void CLevelContours::DeriveSubpixelPos(float &x1,float &y1,DIRECTION Dir,int x,int y,float f)
{
	switch (Dir)
	{
	case NORTH: 
		x1=x;
		y1=y-f; 
		break;
	case EAST: 
		x1=x+f;
		y1=y;
		break;
	case SOUTH:
		x1=x;
		y1=y+f;
		break;
	case WEST: 
		x1=x-f;
		y1=y;
	}
}

} // end namespace videndo


#endif //_VIDENDO_LEVELCONTOURS_H
