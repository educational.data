#ifndef _VIDENDO_SEGMENTATE_H
#define _VIDENDO_SEGMENTATE_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include "color.h"
#include "level_contours.h"
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/pixelgroups.h>

namespace videndo{ // use namespace if C++

using ipl::CImage;
using ipl::CPixelGroups;
using ipl::CONNECTIVITY;
using ipl::COLORTYPE;

/** Collection of segmentation algorithms. Corresponds to the ipl::CSegmentate class.

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CSegmentate videndo/segmentate.h
	@version 0.60
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CSegmentate
{
	protected:
		class CDynamicBlobDetection: public CLevelContours
		{
		protected: // attributes
			CImage m_BlobImage;
			CImage m_BufImage;
		public:
			/** See explanation for CSegmentate::DeriveBlobsDynamic.
				@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
			bool DeriveBlobs(CByteImage &Source, COLORTYPE Color, CONNECTIVITY C,
				int TotalBlur, int MinStartGradient, CPixelGroups &Blobs);
		protected: // methods
			inline CPoint2D<INT16> GetNeighbour(const CPoint2D<INT16>& P, int i) const;
			bool BlobExpand(CByteImage &Source,const CPoint2D<INT16> &Seed, CONNECTIVITY C,
				int Threshold, CPixelGroup &Blob);
		};

	public: // methods
		/** Extract the blobs from an input image with 24 b/p. The blobs to be extracted
			are defined to have color values in the range [HueLow,HueHigh], colors outside
			this range are considered background.
			@param Source Image to derive blobs from, must have 24 b/p.
			@param PixelGroups Extracted groups are returned in this parameter.
			@param HueLow The lower value of the HUE color range. This refers to 
				the HSI color model, see CColor::RGB2HSI() for more information.
			@param HueHigh The upper value of the HUE color range. This refers to 
				the HSI color model, see CColor::RGB2HSI() for more information.
			@param Connected Decides if the blobs are to be eight- or fourconnected.
			@param MinSize Blobs less than this value are discarded, defaults to 1.
			@return False, if source image is not 24 b/p. */
		static bool DeriveBlobsFromHue(const CImage& Source, CPixelGroups& PixelGroups,
						UINT8 HueLow, UINT8 HueHigh, CONNECTIVITY Connected, unsigned int MinSize=1);


		/** Extract the blobs from an input image with 24 b/p. The blobs to be extracted
			are defined by a truncated cone in the RGB space. The cone itself is defined
			by a point in the RGB-space (Color) and an angle (Angle) defining the cone width.
			In addition, the cone can be truncated by setting lower and upper limits. The
			maximum upper limit can be found by calling CLocalOperation::GetRGBConeUpperLimit.
			Colors outside this cone are considered background.
			@param Source Image to derive blobs from, must have 24 b/p.
			@param PixelGroups Extracted groups are returned in this parameter.
			@param Color The direction of the cone axis is defined by this color.
			@param LowerLimit Truncates the tip of the cone at this value (towards 
				origo of the RGB space). This is the distance from origo to the
				truncated tip of the cone. Value must be greater than 0.
			@param UpperLimit Truncates the base of the cone at this value. This is the
				distance from origo to the truncated base of the cone. The maximum value
				for this parameter can be found by calling GetRGBConeUpperLimit(UINT32 Color).
				If UpperLimit is greater than the value returned by GetRGBConeUpperLimit()
				it will simply be truncated to this value (a warning is produced).
			@param Angle Angle in radians defining the cone width.
			@param Connected Decides if the blobs are to be eight- or fourconnected.
			@param MinSize Blobs less than this value are discarded, defaults to 1.
			@return False, if source image is not 24 b/p.
			@see GetRGBConeUpperLimit */
		static bool SegmentateFromRGBCone(const CImage& Source, CPixelGroups &PixelGroups, 
						UINT32 Color, float LowerLimit, float UpperLimit, float Angle, 
						CONNECTIVITY Connected, unsigned int MinSize=1);

		/** This method derives a series of blobs using dynamical thresholding.
			The gradient image is derived, and local maxima in the gradient image are
			found. Each local maximum is a potential seed for a region growing blob
			detection. The actual seed is the relevant neighbour to the pixel of 
			maximum gradient. The threshold used in the local blob detection is that
			of the point of maximum gradient. Blobs are detected in the order of 
			decreasing value of the gradient. Potential seeds from already detected blobs 
			are erased. If the blob expansion hit an already detected blob, the
			expansion is stopped without generating any blob.
			@param Source Is the source image.
			@param Color Is the choice of forground color: HIGHCOLOR or LOWCOLOR 
			@param C Is the connectivity of the blobs, FOURCONNECTED or EIGHTCONNECTED
			@param TotalBlur Is the number of EightConnecede3x3 preblur operations to be performed
			@param MinStartGradient Is the minimum gradient used in the blob detection.
			@param PixelGroups Is the destination of detected blobs. Note that the colors inserted
			in the Pixelgroup are not those of the source image but set equal to the 
			threshold value used in the relevant blob detection. 
			@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
		bool DeriveBlobsDynamic(CByteImage &Source, COLORTYPE Color, CONNECTIVITY C,
				int TotalBlur, int MinStartGradient, CPixelGroups &PixelGroups);

//		/** Returns the upper limit for the cone defined when calling SegmentateFromRGBCone().
//			@param Color The direction of the cone axis is defined by this color.
//			@return Upper limit for base in the RGB space or 0 if Color=(r,g,b)=(0,0,0). */
//		static float GetRGBConeUpperLimit(UINT32 Color);

	private: // methods
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

inline CPoint2D<INT16> CSegmentate::CDynamicBlobDetection::GetNeighbour(const CPoint2D<INT16>& P, int i) const
{
	CPoint2D<INT16> Q;
	switch(i)
	{
	case 0: Q.Set(P.GetX()+1,P.GetY());return Q;
	case 1: Q.Set(P.GetX()-1,P.GetY());return Q;
	case 2: Q.Set(P.GetX(),P.GetY()+1);return Q;
	case 3: Q.Set(P.GetX(),P.GetY()-1);return Q;
	case 4: Q.Set(P.GetX()+1,P.GetY()+1);return Q;
	case 5: Q.Set(P.GetX()+1,P.GetY()-1);return Q;
	case 6: Q.Set(P.GetX()-1,P.GetY()+1);return Q;
	case 7: Q.Set(P.GetX()-1,P.GetY()-1);return Q;
	default:
		{
			ostringstream ost;
			ost << "CSegmentate::CDynamicBlobDetection::GetNeighbour() Reached a "
				"default-case in a switch statement, should not be possible here "
				"- check code" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			return Q;
		}
	}
}

} // end namespace videndo

#endif //_VIDENDO_SEGMENTATE_H
