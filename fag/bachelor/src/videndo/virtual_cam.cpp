#include "stdafx.h"
#include <afxpriv.h> 
#include "virtual_cam.h"
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/error.h>
#include <algorithm>
#include <string>

namespace videndo{

using std::cerr;
using std::sort;
using std::endl;
using std::ostringstream;
using std::string;
using ipl::CError;
using ipl::IPL_ERROR;

CVirtualCam::CVirtualCam()
{
	m_dNearPlane   = 50; // edr 
	m_dFarPlane    = 5000.0; // edr

	m_x = 0.0f;
	m_y = 0.0f;
	m_z = 0.0f;

	m_a = 0.0f;
	m_b = 0.0f;
	m_c = 0.0f;

	m_Beta=0;
	m_p=1;

	m_ClearColorRed   = 0.0f;
	m_ClearColorGreen = 0.0f;
	m_ClearColorBlue  = 0.0f;

	m_ObjectColorRed = 1.0f;
	m_ObjectColorGreen = 1.0f;
	m_ObjectColorBlue = 1.0f;

	m_IsInitialized=false;
	m_IsCameraDefined=false;
}

CVirtualCam::~CVirtualCam()
{

}

bool CVirtualCam::Initialize()
{
	SetOpenGLState();
	m_IsInitialized=true;
	return true;
}

bool CVirtualCam::DefineCamera(unsigned int Width, unsigned int Height, double FocalLength)
{
	m_Width=Width;
	m_Height=Height;
	m_WidthHalf=Width/2;
	m_HeightHalf=Height/2;
	m_f=FocalLength;
	m_IsCameraDefined=true;
	return true;
}

bool CVirtualCam::SetPose(double x, double y, double z, double a, double b, double c)
{
	if (m_IsInitialized==false)
	{
		AfxMessageBox("CVirtualCam::SetPose() Class not initialized");
		return false;
	}
	else
	{
		m_x=x;
		m_y=y;
		m_z=z;
		m_a=a;
		m_b=b;
		m_c=c;
	}
	return true;
}

void CVirtualCam::RenderScene()
{	
//	SortTriangles();
//	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	std::vector<CTriangle>::const_iterator it=m_Triangles.begin();
	while(it!=m_Triangles.end())
	{
		glColor3f(m_ObjectColorRed,m_ObjectColorGreen,m_ObjectColorBlue);
		glBegin(GL_TRIANGLES);
		glNormal3dv(it->m_Normal);
		glVertex3dv(it->m_Vertice[0]);
		glVertex3dv(it->m_Vertice[1]);
		glVertex3dv(it->m_Vertice[2]);
		glEnd();
		++it;
	}
}

void CVirtualCam::SetProjectionMatrix() 
{
	glViewport(0,0,m_Width,m_Height);
	glMatrixMode(GL_PROJECTION);
	double pdPerspectiveMatrix[] = {
		-2*m_f/m_Width    , 2*m_f*m_p*sin(m_Beta)/m_Height, 0,                          0,
		0,                2*m_f*m_p*cos(m_Beta)/m_Height, 0 ,                         0,
		(2*m_WidthHalf/m_Width)-1, 1-(2*m_HeightHalf/m_Height)         , (m_dFarPlane+m_dNearPlane)/(m_dFarPlane-m_dNearPlane) , 1,
		0               , 0                         , -2*m_dFarPlane*m_dNearPlane/(m_dFarPlane-m_dNearPlane), 0
	};
	glLoadIdentity();
	glMultMatrixd(pdPerspectiveMatrix);
}

void CVirtualCam::SetModelMatrix() 
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotated(ipl::RadToDegree(m_c),  0.0,  0.0, -1.0);
	glRotated(ipl::RadToDegree(m_b),  0.0, -1.0,  0.0);
	glRotated(ipl::RadToDegree(m_a), -1.0,  0.0,  0.0);
	glTranslated(-m_x, -m_y, -m_z);
}

bool CVirtualCam::GetImage(CImage& Img)
{
	if (m_IsInitialized==false)
	{
		AfxMessageBox("CVirtualCam::SetPose() Class not initialized");
		return false;
	}

	// acquire image
	HDC hDc;
	
	HBITMAP hBitmapPrev;
	BITMAPINFO m_BitmapInfo;
	VOID **m_pBitmapBits;
	HBITMAP m_hBitmapImage;
	
	PIXELFORMATDESCRIPTOR pfd;
	HGLRC hRenderContext;

	// TODO: Add your command handler code here
	hDc = ::GetDC( NULL );
	memset( &m_BitmapInfo, 0, sizeof(BITMAPINFO) );
	m_BitmapInfo.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	m_BitmapInfo.bmiHeader.biWidth = GetWidth();
	m_BitmapInfo.bmiHeader.biHeight = GetHeight();
	m_BitmapInfo.bmiHeader.biPlanes = 1;
	m_BitmapInfo.bmiHeader.biBitCount = 24;
	m_BitmapInfo.bmiHeader.biCompression = BI_RGB;
	m_hBitmapImage = CreateDIBSection( hDc, &m_BitmapInfo, DIB_RGB_COLORS,
		(void **)(&m_pBitmapBits), NULL, 0 );
	::ReleaseDC( NULL, hDc );
	
	hDc = CreateCompatibleDC( NULL );
	hBitmapPrev = (HBITMAP)SelectObject( hDc, m_hBitmapImage );
	
	Rectangle( hDc, 0, 0, GetWidth(), GetHeight());
	
	pfd.dwFlags = PFD_DRAW_TO_BITMAP | PFD_SUPPORT_OPENGL | PFD_TYPE_RGBA;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 24;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cDepthBits = 16;
	
	int nPixelFormat = ChoosePixelFormat (hDc, &pfd);
	SetPixelFormat(hDc, nPixelFormat, &pfd);
	
	hRenderContext = wglCreateContext( hDc );
	wglMakeCurrent( hDc, hRenderContext );

	//GetDocument()->Draw();
	// Acquire method from F. C. Hansens project - instead of GetDocument()->Draw()
	SetOpenGLState();
	SetProjectionMatrix();
	SetModelMatrix();
	RenderScene();
	glFinish();
	PBITMAPINFO test = CreateBitmapInfoStruct(m_hBitmapImage);
	Img.AllocFast(GetWidth(), GetHeight(), 24);
	GetDIBits(hDc, m_hBitmapImage, 0, (WORD) GetHeight(), Img.GetImageDataPtr(), test, DIB_RGB_COLORS);

	wglMakeCurrent( NULL, NULL );
//	wglMakeCurrent( hDc, NULL );
	wglDeleteContext( hRenderContext );


//	SelectObject( hDc, hBitmapPrev ); // Unselect the DIB from hDc
	DeleteDC( hDc ); // Delete the Memory Device Context
	DeleteObject(m_hBitmapImage);
	LocalFree(test);

	return true;
}

PBITMAPINFO CVirtualCam::CreateBitmapInfoStruct(HBITMAP hBmp)
{ 
    BITMAP bmp; 
    PBITMAPINFO pbmi; 
    WORD    cClrBits; 
	
    // Retrieve the bitmap's color format, width, and height. 
    GetObject(hBmp, sizeof(BITMAP), (LPSTR)&bmp);
	
    // Convert the color format to a count of bits. 
    cClrBits = (WORD)(bmp.bmPlanes * bmp.bmBitsPixel); 
    if (cClrBits == 1) 
        cClrBits = 1; 
    else if (cClrBits <= 4) 
        cClrBits = 4; 
    else if (cClrBits <= 8) 
        cClrBits = 8; 
    else if (cClrBits <= 16) 
        cClrBits = 16; 
    else if (cClrBits <= 24) 
        cClrBits = 24; 
    else cClrBits = 32; 
	
    // Allocate memory for the BITMAPINFO structure. (This structure 
    // contains a BITMAPINFOHEADER structure and an array of RGBQUAD 
    // data structures.) 
	
	if (cClrBits != 24) 
		pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
		sizeof(BITMAPINFOHEADER) + 
		sizeof(RGBQUAD) * (1<< cClrBits)); 
	
	// There is no RGBQUAD array for the 24-bit-per-pixel format. 
	
	else 
		pbmi = (PBITMAPINFO) LocalAlloc(LPTR, 
		sizeof(BITMAPINFOHEADER)); 
	
    // Initialize the fields in the BITMAPINFO structure. 
	
    pbmi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER); 
    pbmi->bmiHeader.biWidth = bmp.bmWidth; 
    pbmi->bmiHeader.biHeight = bmp.bmHeight; 
    pbmi->bmiHeader.biPlanes = bmp.bmPlanes; 
    pbmi->bmiHeader.biBitCount = bmp.bmBitsPixel; 
    if (cClrBits < 24) 
        pbmi->bmiHeader.biClrUsed = (1<<cClrBits); 
	
    // If the bitmap is not compressed, set the BI_RGB flag. 
    pbmi->bmiHeader.biCompression = BI_RGB; 
	
    // Compute the number of bytes in the array of color 
    // indices and store the result in biSizeImage. 
    // Width must be DWORD aligned unless bitmap is RLE compressed.
    pbmi->bmiHeader.biSizeImage = (pbmi->bmiHeader.biWidth + 15) /16 
		* pbmi->bmiHeader.biHeight 
		* cClrBits * 2;
    // Set biClrImportant to 0, indicating that all of the 
    // device colors are important. 
	pbmi->bmiHeader.biClrImportant = 0; 
	return pbmi; 
} 

bool CVirtualCam::LoadSTLFile(const char* pPathAndFileName)
{
	m_Triangles.clear();
	ifstream is(pPathAndFileName);
	if (is.is_open()==true)
	{
		char pLine1[100],pLine2[100], pTempStr[50];
		is.getline(pLine1,100); // skip first line "solid OBJECT"
		m_Triangles.reserve(5000);
		CTriangle Triangle;
		double* pNormal=Triangle.m_Normal;
		while(true)
		{
			is.getline(pLine1,100);
			is.getline(pLine2,100); // skip "outer loop"
			if (!is)
				break; // eof reached
			sscanf(pLine1,"%s %s %lg %lg %lg",&pTempStr,&pTempStr,&pNormal[0],&pNormal[1],&pNormal[2]);
			for(int i=0; i<3; i++)
			{
				is.getline(pLine1,100);
				sscanf(pLine1,"%s %lg %lg %lg",&pTempStr,&Triangle.m_Vertice[i][0],
								&Triangle.m_Vertice[i][1],&Triangle.m_Vertice[i][2]);
			}				
			m_Triangles.push_back(Triangle);
			is.getline(pLine1,100); // skip endloop
			is.getline(pLine1,100); // skip endfacet
		}
		return true;
	}
	else
	{
		cerr << "CVirtualCam::LoadSTLFile() Failed opening file" << endl;
		return false;
	}
}

bool CVirtualCam::SetBackgroundColor(float R, float G, float B)
{
	if ((R<0) || (R>1) || (G<0) || (G>1) || (B<0) || (B>1))
	{
		ostringstream ost;
		ost << "CVirtualCam::SetBackgroundColor() One of the provided color values are out of "
			"range [0;1] (R=" << R << ", G=" << G << ", B=" << B << ")" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_ClearColorRed=R;
	m_ClearColorGreen=G;
	m_ClearColorBlue=B;
	SetOpenGLState();
	return true;
}

bool CVirtualCam::SetObjectColor(float R, float G, float B)
{
	if ((R<0) || (R>1) || (G<0) || (G>1) || (B<0) || (B>1))
	{
		ostringstream ost;
		ost << "CVirtualCam::SetObjectColor() One of the provided color values are out of "
			"range [0;1] (R=" << R << ", G=" << G << ", B=" << B << ")" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_ObjectColorRed=R;
	m_ObjectColorGreen=G;
	m_ObjectColorBlue=B;
	SetOpenGLState();
	return true;
}

void CVirtualCam::InverseSurfaceNormals()
{
	std::vector<CTriangle>::iterator it=m_Triangles.begin();
	while(it!=m_Triangles.end())
	{
		it->m_Normal[0]=-it->m_Normal[0];
		it->m_Normal[1]=-it->m_Normal[1];
		it->m_Normal[2]=-it->m_Normal[2];
		++it;
	}
}

ostream& operator<<(ostream& s, const CVirtualCam& Cam)
{
	s << "CVirtualCam IsCameraDefined: " << Cam.IsCameraDefined() << endl;
	if (Cam.IsCameraDefined()==true)
	{
		s << " Width:       " << Cam.m_Width << endl
		  << " Height:      " << Cam.m_Height << endl
		  << " FocalLength: " << Cam.m_f << endl;
	}
	return s;
}

std::istream& operator>>(std::istream& is, CVirtualCam& Cam)
{
	// remember to call Initialize() and DefineCamera() after width, height and f has been read!
	string Token;
	ipl::SkipSpaceAndComments(is);
	// read 'CTraining' token
	is >> Token;
	if (Token!=string("CVirtualCam"))
	{
		ostringstream ost;
		ost << "operator>>(istream&, CVirtualCam&) Token CVirtualCam not found" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return is;
	}
	// always initialize camera object
	Cam.Initialize();
	// skip 'IsCameraDefined:' token
	is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
	is >> Cam.m_IsCameraDefined;
	if (Cam.m_IsCameraDefined==true)
	{
		// skip 'Width:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> Cam.m_Width;
		// skip 'Height:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> Cam.m_Height;
		// skip 'Width:' token
		is.ignore(std::numeric_limits<int>::max(),std::char_traits<char>::to_int_type(':'));
		is >> Cam.m_f;
		Cam.DefineCamera(Cam.m_Width,Cam.m_Height,Cam.m_f);
	}
	return is;
}


void CVirtualCam::SetOpenGLState()
{
	int SetState=3;
	if (SetState==0)
	{
		// from NeHe's Tutorial 3
		glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
		glClearColor(0.0, 0.0, 0.0, 0.5);					// Black Background
		glClearDepth(1.0);									// Depth Buffer Setup
		glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
		glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations
	}
	else if (SetState==1)
	{
		////////////////////////////////////////
		///// IMPORTANT: When using this method, you must remember to clear DEPTH BUFFER in RenderScene!
		////////////////////////////////////////
		// from NeHe's Tutorial 3
		glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
		glEnable(GL_NORMALIZE);			// Normalizes the normal vectors (in case they aren't normalized)
//		glClearColor(0.0, 0.0, 0.0, 0.0);					// Black Background
//		glClearDepth(1.0);									// Depth Buffer Setup
		SetLight();
		glEnable(GL_DEPTH_TEST);							// Enables Depth Testing
		glDepthFunc(GL_LEQUAL);								// The Type Of Depth Testing To Do
		glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// Really Nice Perspective Calculations

//		glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);
//		glEnable(GL_BLEND);
////		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
//		glBlendFunc(GL_SRC_ALPHA_SATURATE, GL_ONE);
//		glEnable(GL_POLYGON_SMOOTH);
	}
	else if (SetState==2)
	{
		glShadeModel(GL_SMOOTH);							// Enable Smooth Shading
		glEnable(GL_NORMALIZE);			// Normalizes the normal vectors (in case they aren't normalized)
		glClearColor( 0.0, 0.0, 0.0, 0.0 );
		glDrawBuffer( GL_FRONT_AND_BACK );
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT  );
		glDrawBuffer( GL_BACK );
		
		glPolygonMode( GL_FRONT_AND_BACK, GL_FILL );
		glShadeModel( GL_FLAT );
		
		// specify that back facing polygons are to be culled
		glCullFace( GL_BACK );
		glEnable( GL_CULL_FACE );

		// enable polygon antialiasing
		glBlendFunc( GL_SRC_ALPHA_SATURATE, GL_ONE );
		glEnable( GL_BLEND );
		glEnable( GL_POLYGON_SMOOTH );
		glHint(GL_POLYGON_SMOOTH_HINT,GL_NICEST);
		glDisable( GL_DEPTH_TEST );
		SetLight();
	}
	else if (SetState==3)
	{
		////////////////////////////////////////
		///// IMPORTANT: When using this method, you must remember to clear DEPTH BUFFER in RenderScene!
		////////////////////////////////////////
		glCullFace (GL_BACK);
		glEnable (GL_CULL_FACE);
		glEnable(GL_NORMALIZE);			// Normalizes the normal vectors (in case they aren't normalized)
		glBlendFunc (GL_SRC_ALPHA_SATURATE, GL_ONE);
		glClearColor (m_ClearColorRed, m_ClearColorGreen, m_ClearColorBlue, 0.0);

		glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDisable (GL_BLEND);
		glDisable (GL_POLYGON_SMOOTH);
		glEnable (GL_DEPTH_TEST);
		SetLight();

	}
}


void CVirtualCam::SetLight()
{
	GLfloat LightSpecular[]= { 1.0f, 1.0f, 1.0f, 1.0f }; 
	GLfloat LightAmbient[]= { 0.8f, 0.8f, 0.8f, 1.0f }; 
//	GLfloat LightAmbient[]= { 0.5f, 0.5f, 0.5f, 1.0f }; 
	GLfloat LightDiffuse[]= { 0.9f, 0.9f, 0.9f, 1.0f };
	GLfloat LightPosition[]= { 0.0f, 0.0f, 0.0f, 1.0f }; // light position is in eye coordinates

    // A handy trick -- have surface material mirror the color.
//    glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
//    glEnable(GL_COLOR_MATERIAL);

//	glLightModeli(GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE);
//	glLightfv(GL_LIGHT1, GL_AMBIENT, LightAmbient);  // Setup The Ambient Light
//	glLightfv(GL_LIGHT1, GL_SPECULAR, LightSpecular);  // Setup The Specular Light
	glLightfv(GL_LIGHT1, GL_DIFFUSE, LightDiffuse);  // Setup The Diffuse Light
	glLightfv(GL_LIGHT1, GL_POSITION,LightPosition); // Position The Light
	glEnable(GL_LIGHT1);							 // Enable Light One
	glEnable(GL_LIGHTING);
}

CPoint3D<double> CVirtualCam::CTriangle::m_CamPos;

#include <fstream>
using std::ofstream;

void CVirtualCam::SortTriangles()
{
	m_Triangles[0].m_CamPos.Set(m_x,m_y,m_z);
	sort(m_Triangles.begin(),m_Triangles.end());
}

void CVirtualCam::GetGLError()
{
	GLenum ErrorCode=glGetError();
	if (ErrorCode!=GL_NO_ERROR)
	{
		if (ErrorCode==GL_INVALID_ENUM)
			AfxMessageBox("GL_INVALID_ENUM");
		else if (ErrorCode==GL_INVALID_VALUE)
			AfxMessageBox("GL_INVALID_VALUE");
		else if (ErrorCode==GL_INVALID_OPERATION)
				AfxMessageBox("GL_INVALID_OPERATION");
		else if (ErrorCode==GL_STACK_OVERFLOW)
			AfxMessageBox("GL_STACK_OVERFLOW");
		else if (ErrorCode==GL_STACK_UNDERFLOW)
			AfxMessageBox("GL_STACK_UNDERFLOW");
		else if (ErrorCode==GL_OUT_OF_MEMORY)
			AfxMessageBox("GL_OUT_OF_MEMORY");
		else
			AfxMessageBox("UNKNOWN ERROR");
	}
}

} // end namespace videndo
