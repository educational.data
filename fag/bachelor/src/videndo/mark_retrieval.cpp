#include "mark_retrieval.h"
#include "kernel_c/kernel_mark_retrieval.c"
#include <ipl98/kernel_c/kernel_string.h>
#include <ipl98/cpp/algorithms/statistic.h>
#include <ipl98/cpp/algorithms/segmentate.h>
#include <ipl98/cpp/algorithms/feature_extraction.h> 
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/byte_image.h>
#include <ipl98/cpp/pixelgroup.h>
#include <ipl98/cpp/algorithms/coordinate_transform.h> 
#include <ipl98/cpp/algorithms/graphics.h>
#include "features.h"
#include <fstream>
#include <sstream>
//#include <mfc/fileutils.h>
#include <algorithm>

namespace videndo{

using ipl::TString;
using ipl::k_InitString;
using ipl::CStatistic;
using ipl::Round;
using ipl::k_AddCharToString;
using ipl::k_AddCharArrayToString;
using ipl::k_ShowMessage;
using ipl::IPL_ERROR;
using ipl::k_EmptyString;
using ipl::EMPTY;
using ipl::IPL_WARNING;
using ipl::CByteImage;
using ipl::k_SkipWhiteSpace;
using ipl::k_SkipLine;
using ipl::k_ReadToken;
using ipl::k_StringToUpper;
using ipl::CError;
using ipl::CPixelGroup;
using ipl::CSegmentate;
using ipl::CCoordinateTransform;
using ipl::CGraphics;
using std::ofstream;
using std::ostringstream;
using std::sort;
using std::cout;


using std::endl;

CMarkRetrieval::CMarkRetrieval() : m_ThresholdStep(GLOBAL_CALIB_THRESH_STEP),
								   m_ThresholdLowerBound(GLOBAL_CALIB_THRESH_LOWER_BOUND),
								   m_ThresholdUpperBound(GLOBAL_CALIB_THRESH_UPPER_BOUND),
								   m_AllMarksNeeded(true), m_NotAllMarksFast(false),
								   m_UseUserThresholds(false), m_FindNewRefMarks(false)
{
	ResetAdaptiveReturn();
}

CMarkRetrieval::~CMarkRetrieval()
{
}

bool CMarkRetrieval::FindMarksAdaptive(CStdImage& Img, string ThresholdFile, unsigned int RefMarkType, bool Verbose,
									   unsigned short ThresholdStep, unsigned int MinBlobSize, bool UseSimpleCenter)
{
	if (Img.GetOrigo()!=CPoint2D<int>(0,0))
	{
		ostringstream ost;
		ost << "CMarkRetrieval::FindMarksAdaptive() Origo of input image is not (0,0)" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (m_PointSets.GetTotalPointSets()==0)
	{
		ostringstream ost;
		ost << "CMarkRetrieval::FindMarksAdaptive() No 3D points and Ids available, you need to "
			"load some worldcoordinates before using this method (call ReadWorldCoordinates())" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_Verbose=Verbose;
	m_CSCenters.resize(m_PointSets.GetTotalPointSets());
	unsigned int TotalRefMarks=0,FoundRefMarks;
	vector<unsigned short> ThresholdList; // stores the threshold values where refmarks are found
	bool ReturnValue=true;
	bool FoundAllRefMarks=false;
	TString str;
	k_InitString(&str);
	CImage TempImg;
	unsigned short up,down;
	if (Verbose==true)
		k_RefMarkPrintf("**************** FindMarksAdaptive ******************\n");
	/* find the beginning threshold value from file - if present */
	unsigned short BeginThreshold;
	if (InitializeThreshold(ThresholdFile, Img, ThresholdStep, BeginThreshold)==false)
		return false;
	/* checking the image type*/
	if (Img.GetBits()!=8)
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"FindMarksAdaptive() Image must be 8 b/p (current is %d b/p)",Img.GetBits());
		k_EmptyString(&str);
		return false;
	}
	if (Img.GetOrigin()==EMPTY)
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_WARNING,&str,"FindMarksAdaptive() Source image is empty - doing nothing");
		k_EmptyString(&str);
		return false;
	}
	m_NewPointSets.Empty();
	m_NewCSCenters.clear();
	TempImg=Img;
	m_OriginalImg=Img;
	/* find all refmarks by iterating through all threshold values */
	up=BeginThreshold;
	down=(unsigned short)(BeginThreshold-m_ThresholdStep);
	while (((up<=m_ThresholdUpperBound) || (down>=m_ThresholdLowerBound)) && (FoundAllRefMarks==false))
	{
		/* trying upward threshold */
		if (up<=m_ThresholdUpperBound)
		{
			if (Verbose==true)
				k_RefMarkPrintf(" **** Iteration (upwards) with Threshold=%d\n",up);
			/* Finding refmarks with given threshold "up" */
			FoundRefMarks=FindCalibrationMarksAndMatchIds(TempImg,/*m_PointSets,*/ up, Img,
															MinBlobSize,UseSimpleCenter, RefMarkType);
			if (FoundRefMarks>0)
			{
				TotalRefMarks+=FoundRefMarks;
				if (TotalRefMarks>=m_PointSets.GetTotalPointSets())
					FoundAllRefMarks=true;
				ThresholdList.push_back(up);
			}
		}
		/* trying downward threshold */
		if ((FoundAllRefMarks==false) && (down>=m_ThresholdLowerBound))
		{
			if (Verbose==true)
				k_RefMarkPrintf(" **** Iteration (downwards) with Threshold=%d\n",down);
			/* Finding refmarks with given threshold "down" */
			FoundRefMarks=FindCalibrationMarksAndMatchIds(TempImg,/*m_PointSets, */down, Img, 
															MinBlobSize,UseSimpleCenter, RefMarkType);
			if (FoundRefMarks>0)
			{
				ThresholdList.push_back(down);
				TotalRefMarks+=FoundRefMarks;
				if (TotalRefMarks>=m_PointSets.GetTotalPointSets())
					FoundAllRefMarks=true;
			}
		}
		if (m_AllMarksNeeded==true)
		{
			if (up<=m_ThresholdUpperBound)
				up+=(unsigned short)m_ThresholdStep;
			if (down>=m_ThresholdLowerBound)
				down-=(unsigned short)m_ThresholdStep;
		}
		else // m_AllMarksNeeded is false
		{
			if (m_TotalRefMarks<m_MinMarks)
			{
				// then we keep searching by expanding threshold boundary
				m_ThresholdUpperBound += (unsigned short)m_ThresholdStep;
				if (m_ThresholdUpperBound>GLOBAL_CALIB_THRESH_UPPER_BOUND)
					m_ThresholdUpperBound=GLOBAL_CALIB_THRESH_UPPER_BOUND;
				m_ThresholdLowerBound -= (unsigned short)m_ThresholdStep;
				if (m_ThresholdLowerBound<GLOBAL_CALIB_THRESH_LOWER_BOUND)
					m_ThresholdLowerBound=GLOBAL_CALIB_THRESH_LOWER_BOUND;
				if (m_Up<=m_ThresholdUpperBound)
					m_Up+=(unsigned short)m_ThresholdStep;
				if (m_Down>=m_ThresholdLowerBound)
					m_Down-=(unsigned short)m_ThresholdStep;
//				up+=(unsigned short)m_ThresholdStep;
//				down-=(unsigned short)m_ThresholdStep;
			}
		}

//		if (up<m_ThresholdUpperBound)
//			up+=(unsigned short)m_ThresholdStep;
//		if (down>m_ThresholdLowerBound)
//			down-=(unsigned short)m_ThresholdStep;
	}
	if (TotalRefMarks!=m_PointSets.GetTotalPointSets())
	{
		if ((m_AllMarksNeeded==true) && (TotalRefMarks>=m_MinMarks))
		{
			m_AllMarksFound=true;
			ReturnValue=true;
		}
		else
		{
			m_AllMarksFound=false;
			ReturnValue=false;
		}
		if ((m_AllMarksNeeded==false) && (m_NotAllMarksFast==false))
		{
			// in the case where not all marks are needed but we searched all thresholds
			// we will still write a threshold file
			WriteThresholdFile(ThresholdFile,ThresholdList);
		}
	}
	else
	{
		/* write threshold file */
		WriteThresholdFile(ThresholdFile,ThresholdList);
		m_AllMarksFound=true;
	}
	/* clean up */
	k_EmptyString(&str);
	return ReturnValue;
}

bool CMarkRetrieval::InitAdaptiveReturn(CStdImage& Img, string ThresholdFile, unsigned int RefMarkType, 
										bool Verbose, unsigned short ThresholdStep,
										unsigned int MinBlobSize, bool UseSimpleCenter)
{
	TString str;
	k_InitString(&str);
	if (m_AdaptiveReturnStarted==false)
	{
		if (Img.GetOrigo()!=CPoint2D<int>(0,0))
		{
			ostringstream ost;
			ost << "CMarkRetrieval::InitAdaptiveReturn() Origo of input image is not (0,0)" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			return false;
		}
		if (m_PointSets.GetTotalPointSets()==0)
		{
			ostringstream ost;
			ost << "CMarkRetrieval::InitAdaptiveReturn() No 3D points and Ids available, you need to "
				"load some worldcoordinates before using this method (call ReadWorldCoordinates())" 
				<< IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			return false;
		}
		m_CSCenters.resize(m_PointSets.GetTotalPointSets());
		m_ThresholdList.clear();
		// find initial threshold values and step size
		if (Verbose==true)
			k_RefMarkPrintf("**************** FindMarksAdaptive initializing ******************\n");
		unsigned short BeginThreshold;
		/* find the beginning threshold value from file - if present */
		if (InitializeThreshold(ThresholdFile, Img, ThresholdStep, BeginThreshold)==false)
			return false;
		/* checking the image type*/
		if (Img.GetBits()!=8)
		{
			k_AddFileAndLine(str);
			k_ShowMessage(IPL_ERROR,&str,"InitAdaptiveReturn() Image must be 8 b/p (current is %d b/p)",Img.GetBits());
			k_EmptyString(&str);
			return false;
		}
		if (Img.GetOrigin()==EMPTY)
		{
			k_AddFileAndLine(str);
			k_ShowMessage(IPL_WARNING,&str,"InitAdaptiveReturn() Source image is empty - doing nothing");
			k_EmptyString(&str);
			return false;
		}
		m_NewPointSets.Empty();
		m_NewCSCenters.clear();
		m_AdaptiveReturnStarted=true;
		m_TotalRefMarks=0;
		m_ThresholdFile=ThresholdFile;
		m_OriginalImg=Img;
		m_TempImg=Img;
		m_Verbose=Verbose;
		m_RefMarkType=RefMarkType;
		m_MinBlobSize=MinBlobSize;
		m_UseSimpleCenter=UseSimpleCenter;

		// set initial threshold values
		m_Up=BeginThreshold;
		m_Down=(unsigned short)(BeginThreshold-m_ThresholdStep);
		return true;
	}
	else
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"InitAdaptiveReturn() Already initiated");
		k_EmptyString(&str);
		return false;
	}
}

void CMarkRetrieval::ResetAdaptiveReturn()
{
	m_AdaptiveReturnStarted=false;
	m_AllMarksFound=false;
	m_ThresholdStep=5; // if no file present uses default value set here
	m_OriginalImg.Empty();
	m_TempImg.Empty();
}

bool CMarkRetrieval::FindMarksAdaptiveReturn()
{
	unsigned int FoundRefMarks;
	TString str;
	k_InitString(&str);
	if (m_AdaptiveReturnStarted==false)
	{
		// first time this function is running - find initial threshold values and step size
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"FindMarksAdaptiveReturn() Must run InitAdaptiveReturn");
		k_EmptyString(&str);
		return false;
	}
	else
	{
	}

	if (m_AllMarksFound==true)
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_WARNING,&str,"FindMarksAdaptiveReturn() All marks already found");
		k_EmptyString(&str);
		return true;
	}

	/* find all refmarks by iterating through all threshold values */
	if ((m_Up<=m_ThresholdUpperBound) || (m_Down>=m_ThresholdLowerBound))
	{
		/* trying upward threshold */
		if (m_Up<=m_ThresholdUpperBound)
		{
			if (m_Verbose==true)
				k_RefMarkPrintf(" **** Iteration (upwards) with Threshold=%d\n",m_Up);
			/* Finding refmarks with given threshold "up" */
			FoundRefMarks=FindCalibrationMarksAndMatchIds(m_TempImg,/*m_PointSets,*/ m_Up, m_OriginalImg, 
															m_MinBlobSize,m_UseSimpleCenter, m_RefMarkType);
			if (FoundRefMarks>0)
			{
				m_TotalRefMarks+=FoundRefMarks;
				if (m_TotalRefMarks==m_PointSets.GetTotalPointSets())
					m_AllMarksFound=true;
				m_ThresholdList.push_back(m_Up);
			}
		}
		/* trying downward threshold */
		if ((m_AllMarksFound==false) && (m_Down>=m_ThresholdLowerBound))
		{
			if (m_Verbose==true)
				k_RefMarkPrintf(" **** Iteration (downwards) with Threshold=%d\n",m_Down);
			/* Finding refmarks with given threshold "down" */
			FoundRefMarks=FindCalibrationMarksAndMatchIds(m_TempImg,/*m_PointSets,*/ m_Down, m_OriginalImg, 
															m_MinBlobSize,m_UseSimpleCenter, m_RefMarkType);
			if (FoundRefMarks>0)
			{
				m_ThresholdList.push_back(m_Down);
				m_TotalRefMarks+=FoundRefMarks;
				if (m_TotalRefMarks==m_PointSets.GetTotalPointSets())
					m_AllMarksFound=true;
			}
		}
	}
	else
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_WARNING,&str,"FindMarksAdaptiveReturn() All thresholds used without finding all marks");
		k_EmptyString(&str);
		/* Return true when all threshold values have been used. */
		return true;
	}
	if ((m_AllMarksNeeded==false) && (m_TotalRefMarks>=m_MinMarks))
	{
		// not all marks needs to be found and we found >= m_MinMarks
		m_AllMarksFound=true;
		if (m_NotAllMarksFast==false)
		{
			// in the case where not all marks are needed but we searched all thresholds
			// we will still write a threshold file
			WriteThresholdFile(m_ThresholdFile,m_ThresholdList);
		}

		return true;
	}
	if (m_TotalRefMarks==m_PointSets.GetTotalPointSets())
	{
		/* write threshold file */
		WriteThresholdFile(m_ThresholdFile,m_ThresholdList);
	}
	else
	{
		// update threshold values for next iteration
		if (m_AllMarksNeeded==true)
		{
			if (m_Up<=m_ThresholdUpperBound)
				m_Up+=(unsigned short)m_ThresholdStep;
			if (m_Down>=m_ThresholdLowerBound)
				m_Down-=(unsigned short)m_ThresholdStep;
		}
		else // m_AllMarksNeeded is false
		{
			if (m_TotalRefMarks<m_MinMarks)
			{
				// then we keep searching by expanding threshold boundary
				m_ThresholdUpperBound += (unsigned short)m_ThresholdStep;
				if (m_ThresholdUpperBound>GLOBAL_CALIB_THRESH_UPPER_BOUND)
					m_ThresholdUpperBound=GLOBAL_CALIB_THRESH_UPPER_BOUND;
				m_ThresholdLowerBound -= (unsigned short)m_ThresholdStep;
				if (m_ThresholdLowerBound<GLOBAL_CALIB_THRESH_LOWER_BOUND)
					m_ThresholdLowerBound=GLOBAL_CALIB_THRESH_LOWER_BOUND;
				if (m_Up<=m_ThresholdUpperBound)
					m_Up+=(unsigned short)m_ThresholdStep;
				if (m_Down>=m_ThresholdLowerBound)
					m_Down-=(unsigned short)m_ThresholdStep;
			}
		}
	}
	/* clean up */
	k_EmptyString(&str);
	/* Return false when not all threshold values have been used. */
	return false;
}

bool CMarkRetrieval::ReadWorldCoordinates(const char* FileName)
{
	if (k_ReadWorldCoordinates(FileName,m_PointSets.GetT3D2DPointsPtr())!=true)
	{
		TString str;
		k_InitString(&str);
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadWorldCoordinates() Failed");
	    k_EmptyString(&str);
		return false;
	}
	else
	{
		return true;
	}
}

ostream& operator<<(ostream& s,const CMarkRetrieval& Marks)
{
	s << "# CMarkRetrieval setup written by CMarkRetrieval class from the Videndo library" << endl;
	s << "#\n# CMarkRetrieval class version" << endl;
	s << "CMarkRetrieval " <<  Marks.GetVersion() << endl << endl;
	s << "# Threshold iteration and limit values" << endl;
	s << "ThresholdLowerBound: " << GLOBAL_CALIB_THRESH_LOWER_BOUND << endl;
	s << "ThresholdUpperBound: " << GLOBAL_CALIB_THRESH_UPPER_BOUND << endl;
	s << "ThresholdStep:       " << GLOBAL_CALIB_THRESH_STEP << endl << endl;

	s << " # Point set contained in CMarkRetrieval" << endl;
	s << Marks.m_PointSets << endl;
	return s;
}

bool CMarkRetrieval::WriteWorldCoordinates(const char* FileName)
{
	bool returnValue=true;
	if (m_PointSets.GetTotalPointSets()<6)
	{
		ostringstream ost;
		ost << "CMarkRetrieval::WriteWorldCoordinates() Only " 
			<< m_PointSets.GetTotalPointSetsInUse() << " 3D points available" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
//	TString str;
//	k_InitString(&str);
	ofstream OutStream; // file to write all info to
	OutStream.open(FileName);
	if (!OutStream)
	{
		ostringstream ost;
		ost << "CMarkRetrieval::WriteWorldCoordinates() Failed creating file: " << FileName << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
//		k_AddFileAndLine(str);
//		k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() Failed opening threshold value file: %s",ThresholdFile.c_str());
//		k_EmptyString(&str);
		returnValue=false;
	}
	else
	{
		OutStream << "# World coordinate file written by CMarkRetrieval" << endl;
		OutStream << "# Author: Ren� Dencker Eriksen (edr@mip.sdu.dk)" << endl << endl;
		OutStream << "# After one line with information on the" << endl;
		OutStream << "# number of 3D coordinates, comes:" << endl;
		OutStream << "#    first column: X coordinate" << endl;
		OutStream << "#    second column: Y coordinate" << endl;
		OutStream << "#    third column: Z coordinate" << endl;
		OutStream << "#    fourth column: the Id-index" << endl << endl;
		OutStream << "WorldCoordinates " << m_PointSets.GetTotalPointSets() << endl << endl;
		for(unsigned short i=0; i<m_PointSets.GetTotalPointSets();i++)
		{
			CPoint3D<float> P=m_PointSets.GetPoint3D(i);
			OutStream << P.GetX() << " " << P.GetY() << " " << P.GetZ() << " " 
						<< m_PointSets.GetId(i) << endl;
		}
		OutStream.close();
	}
	return returnValue;
}

bool CMarkRetrieval::ReadThresholdFile(string ThresholdFile, double& Mean, double& StdDev,
									   double& Min, double& Max)
{
	bool returnValue=true;
	TString str;
	k_InitString(&str);
	if (CFileUtils::PathFileExists(ThresholdFile.c_str()))
	{
		FILE* fp=fopen(ThresholdFile.c_str(),"r"); /* open file */
		bool TotalValuesFound=false;
		unsigned int TotalValues; // total threshold values in file
		unsigned short Value;
		CByteImage Img; // used to store threshold values in, easiest method because we have
						// the statistic class which can derive the Mean and StdDev
		if (fp==NULL)
		{
			k_AddFileAndLine(str);
			k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() Failed opening file");
			k_EmptyString(&str);
			return false;
		}
		/* read from file */
		char c;
		k_SkipWhiteSpace(fp);
		while (((c=(char)fgetc(fp))!=EOF) && (returnValue==true))
		{
			ungetc(c,fp);
			if (c=='#')
			{
				k_SkipLine(fp);
			}
			else /* it must be a token then! */
			{
				char* Token;
				if (k_ReadToken(fp,&Token)==false)
				{
					k_AddFileAndLine(str);
					k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() Failed reading token");
					k_EmptyString(&str);
					returnValue=false;
				}
				else
				{
					k_StringToUpper(Token);
					if (strcmp(Token,"TOTALVALUES")==0)
					{
						TotalValuesFound=true;
						if (fscanf(fp,"%d",&TotalValues)!=1)
						{
							k_AddFileAndLine(str);
							k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() Failed reading total threshold values");
							k_EmptyString(&str);
							returnValue=false;
						}
						Img.Alloc(TotalValues,1);
						for(int i=0; i<(int)TotalValues; i++)
						{
							if (fscanf(fp,"%d",&Value)!=1)
							{
								k_AddFileAndLine(str);
								k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() Failed reading value");
								k_EmptyString(&str);
								returnValue=false;
							}
							else if (Value>255)
							{
								k_AddFileAndLine(str);
								k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() value out of range");
								k_EmptyString(&str);
								returnValue=false;
							}
							else
							{
								Img.SetPixel(i,0,Value);
							}
						}

					}
					free(Token);
				}
			}
			k_SkipWhiteSpace(fp);
		}
		if (TotalValuesFound==false)
		{
			k_AddFileAndLine(str);
			k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() One of the necessary tokens not found");
			k_EmptyString(&str);
			returnValue=false;
		}
		else
		{
			// derive statistics
			CStatistic Stats;
			Stats.DeriveStats(Img);
			Mean=Stats.GetMean();
			StdDev=Stats.GetStdDev();
			Min=Stats.GetMin();
			Max=Stats.GetMax();
		}
		fclose(fp);
	}
	else
	{
		returnValue=false;
	}
	k_EmptyString(&str);
	return returnValue;
}

bool CMarkRetrieval::WriteThresholdFile(const string& ThresholdFile, const vector<unsigned short>& Thresholds)
{
	bool returnValue=true;
	TString str;
	k_InitString(&str);
	ofstream OutStream; // file to write all info to
	OutStream.open(ThresholdFile.c_str());
	if (!OutStream)
	{
		k_AddFileAndLine(str);
		k_ShowMessage(IPL_ERROR,&str,"CMarkRetrieval::ReadThresholdFile() Failed opening threshold value file: %s",ThresholdFile.c_str());
		k_EmptyString(&str);
		returnValue=false;
	}
	else
	{
		OutStream << "# Threshold value file written by CMarkRetrieval" << endl;
		OutStream << "# Author: Ren� Dencker Eriksen (edr@mip.sdu.dk)" << endl << endl;

		OutStream << "TotalValues " << Thresholds.size() << endl << endl;

		for(unsigned short it=0;it<Thresholds.size();it++)
		{
			OutStream << Thresholds[it] << endl;
		}

		k_EmptyString(&str);
		OutStream.close();
	}
	return returnValue;
}

unsigned int CMarkRetrieval::GetThresholdLowerBound() const
{
	if (m_UseUserThresholds==true)
		return m_ThresholdLowerBound;
	else
		return GLOBAL_CALIB_THRESH_LOWER_BOUND;
}

unsigned int CMarkRetrieval::GetThresholdUpperBound() const
{
	if (m_UseUserThresholds==true)
		return m_ThresholdUpperBound;
	else
		return GLOBAL_CALIB_THRESH_UPPER_BOUND;
}

unsigned int CMarkRetrieval::GetThresholdStep() const
{
	if (m_UseUserThresholds==true)
		return m_ThresholdStep;
	else
		return GLOBAL_CALIB_THRESH_STEP;
}

bool CMarkRetrieval::SetThresholdValues(unsigned int LowerBound, unsigned int UpperBound, unsigned int Step)
{
	if (LowerBound>254)
	{
		ostringstream ost;
		ost << "CMarkRetrieval::SetThresholdValues() LowerBound>254!" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		throw(ost.str());
	}
	else if ((UpperBound<1) || (UpperBound>255))
	{
		ostringstream ost;
		ost << "CMarkRetrieval::SetThresholdValues() UpperBound out of range!" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		throw(ost.str());
	}
	else if (LowerBound>=UpperBound)
	{
		ostringstream ost;
		ost << "CMarkRetrieval::SetThresholdValues() LowerBound>=UpperBound!" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		throw(ost.str());
	}
	else if ((Step>255) || (Step>(UpperBound-LowerBound)))
	{
		ostringstream ost;
		ost << "CMarkRetrieval::SetThresholdValues() Step>255 or Step>(UpperBound-LowerBound)!" 
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		throw(ost.str());
	}
	m_UserThresholdLowerBound=LowerBound;
	m_UserThresholdUpperBound=UpperBound;
	m_UseUserThresholds=true;
	if (Step!=0)
		m_ThresholdStep=Step;
	return true;
}

unsigned int CMarkRetrieval::AddNewRefMarksToExisting()
{
	unsigned int TotalNew=m_NewPointSets.GetTotalPointSets();
	for(int i=0; i<TotalNew; i++)
	{
		m_PointSets.AddPoint2DAndId(m_NewPointSets.GetPoint2D(i),m_NewPointSets.GetId(i));
		m_CSCenters.push_back(m_NewCSCenters[i]);
	}
	return TotalNew;
}

bool CMarkRetrieval::GetCodeStartCenters(vector<CPoint2D<double> >& CSCenters)

{
	CSCenters.resize(m_CSCenters.size());
	for(int i=0; i<m_CSCenters.size();i++)
	{
		CSCenters[i]=m_CSCenters[i];
	}
	return true;
}

//bool CMarkRetrieval::GetCodeStartCenters(vector<CPoint2D<double> >& CSCenters)
//
//{
//	CSCenters.resize(m_PointSets.GetTotalPointSets());
//	for(int i=0; i<m_PointSets.GetTotalPointSets();i++)
//	{
//		if (m_PointSets.Point2DInUse(i)==true)
//		{
//			CPoint2D<double> P(m_pDirs[i].CodeStartCM.x,m_pDirs[i].CodeStartCM.y);
//			CSCenters[i]=P;
//		}
//	}
//	return true;
//}

bool CMarkRetrieval::InitializeThreshold(const string& ThresholdFile, const CStdImage& Img, 
										 unsigned short ThresholdStep, unsigned short& BeginThreshold)
{
	const unsigned short MIN_STEP_SIZE=5;
	m_ThresholdStep=MIN_STEP_SIZE; // if no file present uses default value set here
	// will in all cases hold mean and std, if threshold file not found it is derive from the image
	double Mean, StdDev, Min, Max;
	if (ReadThresholdFile(ThresholdFile,Mean,StdDev,Min,Max)==true)
	{
		if (StdDev>15)
			StdDev=15;
		else if (StdDev<2)
			StdDev=2;
		if (m_Verbose==true)
		{
			k_RefMarkPrintf(" Threshold file read: %s\n   Mean=%.1f StdDev=%.1f",
				ThresholdFile.c_str(),Mean, StdDev);
		}
		BeginThreshold=(unsigned short)Round(Mean);
		if (ThresholdStep==0)
			m_ThresholdStep=ipl::Max((unsigned short)Round(StdDev),MIN_STEP_SIZE);
		else
			m_ThresholdStep=ThresholdStep;
	}
	else
	{
		/* no threshold file found - starting out with mean value from current image */
		CStatistic Stats;
		Stats.DeriveStats(Img);
		if (m_Verbose==true)
		{
				k_RefMarkPrintf(" no threshold file found - starting out with mean value from current image");
		}
		BeginThreshold=(unsigned short)Round(Stats.GetMean());
		Min=Stats.GetMin();
		Max=Stats.GetMax();
		if (ThresholdStep!=0)
			m_ThresholdStep=ThresholdStep; // threshold supplied but no threshold file available
	}	
	if (m_AllMarksNeeded==false)
	{
		// not all marks are needed
		if (m_NotAllMarksFast==true)
		{
			m_ThresholdLowerBound=Min-MIN_STEP_SIZE;
			m_ThresholdUpperBound=Max+MIN_STEP_SIZE;
		}
		else
		{
			m_ThresholdLowerBound=GLOBAL_CALIB_THRESH_LOWER_BOUND;
			m_ThresholdUpperBound=GLOBAL_CALIB_THRESH_UPPER_BOUND;
		}
	}
	else // all marks are needed, use either user settings or global lower and upper bound
	{
		if (m_UseUserThresholds==true)
		{
			m_ThresholdLowerBound=m_UserThresholdLowerBound;
			m_ThresholdUpperBound=m_UserThresholdUpperBound;
		}
		else
		{
			m_ThresholdLowerBound=GLOBAL_CALIB_THRESH_LOWER_BOUND;
			m_ThresholdUpperBound=GLOBAL_CALIB_THRESH_UPPER_BOUND;
		}
	}
	return true;
}

////////////////////////////////////////////////////////
/////     Adapative threshold methods      /////////////
////////////////////////////////////////////////////////

/**	Tolerance on variations of center of mass (CM) for circles. The reference circles'
	radius times this value is the allowed tolerance. If two blobs
	have a common CM within this tolerance they are treated as having the
	same CM. */
#define CMTOLERANCE_NEW 0.8
//#define CMTOLERANCE_NEW 13
/** The width of the ring in which code circles are allowed to be present. The exact search
	range is [DistToCodeCircles*(1+CODE_CIRCLES_SEARCH_FRACTION,DistToCodeCircles*(1-CODE_CIRCLES_SEARCH_FRACTION] */
#define CODE_CIRCLES_SEARCH_FRACTION 0.25
///** The smalles blob size which can be considered part of a reference mark. */
//#define MIN_BLOB_SIZE_IN_A_MARK 7

class CPixelGroupExt
{
	private: // attributes
		bool m_InUse; // set to true when Set() has been called
		CPoint2D<double> m_CM; // center of mass
	public: // attributes
		const CPixelGroup* m_pGrp;
		ipl::CFeatures m_Mom;
	public: // members
		/// Default constructor
		CPixelGroupExt():m_pGrp(NULL), m_InUse(false){};
		/** Sets m_pGrp, calculates moments in m_Mom and sets m_CM
			@param Background Moments are calculated with pixelvalues equal to PixVal-Background, 
				where PixVal is the actual value. So for a black pixel group, the value should be 255! */
		bool Set(CPixelGroup* pGrp, unsigned int Background)
		{
			m_pGrp=pGrp; 
			if (m_Mom.DeriveMomentsGrayTone(*pGrp,Background,2)==true)
			{
				m_CM.Set(m_Mom.m_Moments.m10/m_Mom.m_Moments.m00,m_Mom.m_Moments.m01/m_Mom.m_Moments.m00);
				m_InUse=true;
				return true;
			}
			else
				return false;
		}
		inline bool InUse() const{return m_InUse;}
		inline const CPoint2D<double>& GetCM() const{return m_CM;}
		inline void SetCM(const CPoint2D<double>& CM){m_CM=CM;}
};

class CRefMark
{
	protected: // attributes
		bool m_IsInitialized;
		const vector<CPixelGroupExt>* m_pGroupsExtWhite;
		const vector<CPixelGroupExt>* m_pGroupsExtBlack;
		CStdImage* m_pImg;

		bool m_CenterInUse;
		CPoint2D<double> m_Center;

		bool m_CodeCirclesInfoInUse;
		double m_DistToCodeCircles;
		double m_CodeStartCircleRadius;
		double m_BinaryCodeCircleRadius;

		bool m_IgnorePrimaryAxisAngle; // if no significant primary axis exists, this value is false
		double m_PrimaryAxisAngle; // range [0;Pi[

		bool m_CodeStartCenterInUse;
		CPoint2D<double> m_CodeStartCenter;
		bool m_IdInUse;
		unsigned int m_Id;

		bool m_UseCC0Center;
		unsigned int m_MinBlobSize;

		// index in blob array from last time a valid white blob was found - set to 0 from beginning
		unsigned int m_WhiteBlobSearchIndex;
		/** The angle between two successive circles in the binary code; Value must be set in 
			derived class' constructor. */
		double m_Alpha;
		/** Color type of center circle CC0. Value must be set in derived class' constructor. */
		COLORTYPE m_ColorTypeCC0;
		/** If true, extra internal information is added to the IPL98 history. */
		bool m_Verbose;
		/** Set by CheckForRotateSubImage(). The rotation angle in radians 
			when finding Id for reference mark. */
		double m_Rotation;
		/** Set by CheckForRotateSubImage(). The stretching factor when finding 
			Id for reference mark. */
		double m_Stretch;
		/** Center of mass in image for Code Start circle */
		CPoint2D<double> m_CSCenter;
	public: // members
		/// constructor
		CRefMark() : m_CenterInUse(false), m_CodeCirclesInfoInUse(false), m_IsInitialized(false),
							m_CodeStartCenterInUse(false), m_IdInUse(false), m_WhiteBlobSearchIndex(0){}
		
		/** @param GroupsExtWhite A vector of all white pixel groups, must have their moments available.
			@param GroupsExtBlack A vector of all black pixel groups, must have their moments available.
			@return False, if at least one of the pixelgroups contains no groups. */
		bool Initialize(const vector<CPixelGroupExt>& GroupsExtWhite,const vector<CPixelGroupExt>& GroupsExtBlack, 
						CStdImage& Image,bool UseCC0Center,unsigned int MinBlobSize,bool Verbose)
		{
			if ((GroupsExtWhite.size()==0) || (GroupsExtBlack.size()==0))
				return false;
			m_pGroupsExtWhite=&GroupsExtWhite;
			m_pGroupsExtBlack=&GroupsExtBlack;
			m_pImg=&Image;
			m_UseCC0Center=UseCC0Center;
			m_MinBlobSize=MinBlobSize;
			m_IsInitialized=true;
			m_Verbose=Verbose;
			return true;
		}
		inline bool IsInitialized()const{return m_IsInitialized;}

		inline bool CenterInUse() const{return m_CenterInUse;}
		inline CPoint2D<double> GetCenter() const{return m_Center;}
		inline void SetCenter(const CPoint2D<double>& Center){m_Center=Center;m_CenterInUse=true;}

		inline bool CodeCirclesInfoInUse() const{return m_CodeCirclesInfoInUse;}
		inline void SetCodeCircleInfo(double DistToCodeCircles,double CodeStartCircleRadius,double BCCRadius)
		{
			m_DistToCodeCircles=DistToCodeCircles;
			m_CodeStartCircleRadius=CodeStartCircleRadius;
			m_BinaryCodeCircleRadius=BCCRadius;
			m_CodeCirclesInfoInUse=true;
		}
		inline double GetDistToCodeCircles() const{return m_DistToCodeCircles;}
		inline double GetCodeStartCircleRadius() const{return m_CodeStartCircleRadius;}
		inline double GetBinaryCodeCircleRadius() const{return m_BinaryCodeCircleRadius;}

		inline bool GetIgnorePrimaryAxisAngle() const{return m_IgnorePrimaryAxisAngle;}
		inline double GetPrimaryAxisAngle() const{return m_PrimaryAxisAngle;}

		inline bool CodeStartCenterInUse() const{return m_CodeStartCenterInUse;}
		inline CPoint2D<double> GetCodeStartCenter() const{return m_CodeStartCenter;}
		inline void SetCodeStartCenter(const CPoint2D<double>& CSCenter){m_CodeStartCenter=CSCenter;m_CodeStartCenterInUse=true;}

		inline bool IdInUse() const{return m_IdInUse;}
		inline int GetId() const{return m_Id;}
		inline void SetId(unsigned int Id){m_Id=Id;m_IdInUse=true;}

		/** Returns direction of reference mark in radians. */
		inline CPoint2D<double> GetCSCenter(){return m_CSCenter;}
		/** Finds an approximated mark center using the actual threshold, must set m_Center, 
			m_DistToCodeCircles, m_CodeStartCircleRadius, m_BinaryCodeCircleRadius, 
			m_IgnorePrimaryAxisAngle and m_PrimaryAxisAngle. Implement in derived class. */
		virtual bool FindMarkCenter()=0;

		/** The approximate reference mark center and distance to code circles must be available by a previous
			successfull call to FindMarkCenter(). The corrected center is found by finding an optimal threshold
			value and using this threshold on a sub image around the approximate reference mark center. The 
			derived method should call FindId() with the blobs derived with optimal threshold. */
		virtual bool CorrectCenterAndFindId()=0;

		/** If both FindMarkCenter() and FindId() succeeds, this method must be called in order
			to avoid finding the same mark twice. */
		bool DeleteFromImage() const;
		/** Derives blobs and adds colors to all blobs. */
		static bool DeriveBlobsAddColors(CPixelGroups& GroupsWhite,CPixelGroups& GroupsBlack,
								   CImage& Img, UINT8 Threshold, unsigned int MinBlobSize);
		/** Derives blobs, adds colors, and derives center of mass and initializes GroupsExtWhite 
			and GroupsExtBlack. */
		static bool DeriveBlobsExtAddColors(CPixelGroups& GroupsWhite,CPixelGroups& GroupsBlack,
							vector<CPixelGroupExt>& GroupsExtWhite,vector<CPixelGroupExt>& GroupsExtBlack,
							CImage& Img, UINT8 LowThreshold, UINT8 HighThreshold, unsigned int MinBlobSize);
	protected:
		/** Called by CorrectCenterAndFindId() which finds blobs with correct threshold.
			CenterInUse() and CodeCirclesInfoInUse() must return true, i.e. mark center, id distance,
			different radius' for id's must be available.
			@param Grps Black pixel groups derived from subimage with optimal threshold. All groups must contain CM.
			@param pGrpCC0 Pointer to the group with CC0. This is needed to calculate central moments in order
				to find out if the mark should be rotated.
			@param SubImage If it turns out that the reference mark needs to be rotated, the original 
				subimage is needed.
			@param Threshold The optimal threshold to be used in case the subimage must be rotated and 
				new pixelgroups derived. */
		bool FindId(const CPixelGroupExt* pGrpCC0, CImage& SubImage,unsigned int Threshold);
		/** Copy sub image containing one reference mark. Code circle infor and an approximate CM must
			be available by a previous successfull call to FindMarkCenter(). This method is normally called
			from the derived class' CorrectCenterAndFindId() method.
			@param Img Resulting sub image will be placed in Img.
			@return False, if CenterInUse() or CodeCirclesInfoInUse() returns false. */
		bool CopySubImage(CImage& Img);
		/** This method actually changes the m_pGrpCCx members, center etc. */
		bool CheckForRotateSubImage(const CPixelGroupExt* pGrpCC0,CImage& SubImage,unsigned int Threshold);
		virtual bool GetMarkCenter(const CPixelGroupExt* WhiteGrp, const vector<CPixelGroupExt>* BlackGrps,
			bool bSetCenter, bool bSetBlobs, bool bSetCodeCircleInfo)=0;
		// linear regression
		void Fit(const vector<double>& x, const vector<double>& y, double& a, double& b) const;
		/** Returns width of center blob. Used to find stretching factor for ref mark. */
		virtual unsigned int GetCenterWidth() const=0;
		/** Returns height of center blob. Used to find stretching factor for ref mark. */
		virtual unsigned int GetCenterHeight() const=0;
		/** If m_Verbose is true, this method is called the relevant places to output messages.
			It prints available information from reference marks if available. */
		void PrintMessage(const char* str) const;
		/** If m_Verbose is true, this method is called the relevant places to output messages.
			It prints the supplied center. This method is needed in addition to PrintMessage()
			since some algorithms temporarely stores center and code information in this class
			in a subimage with its own origo, hence this information is not relevant to output.
			@param Center The center of reference mark in the global image. */
		void PrintMessage(const CPoint2D<double>& Center, const char* str) const;
};

void CRefMark::PrintMessage(const char* str) const
{
	ostringstream ost;
	ost << str;
	if (CenterInUse()==true)
		ost << endl << "(CC0 center=" << GetCenter() << " ";
	if (CodeCirclesInfoInUse()==true)
	{
		ost << "DistToCodeCircles=" << GetDistToCodeCircles() << " ";
		ost << "CodeStartCircleRadius=" << GetCodeStartCircleRadius() << " ";
		ost << "GetBinaryCodeCircleRadius=" << GetBinaryCodeCircleRadius() << " ";
	}
	ost << ")" << endl;
	::AddIPL98ErrorHistory(ost.str().c_str());
}

void CRefMark::PrintMessage(const CPoint2D<double>& Center, const char* str) const
{
	ostringstream ost;
	ost << str << endl << "(CC0 Center=" << Center << ")" << endl;
	::AddIPL98ErrorHistory(ost.str().c_str());
}

bool CRefMark::CheckForRotateSubImage(const CPixelGroupExt* pGrpCC0,CImage& SubImage,unsigned int Threshold)
{
	const double PRIMARY_AXIS_THRESHOLD=0.07;
	ipl::CFeatures Features;
	unsigned int CC0Threshold;
	m_ColorTypeCC0 == LOWCOLOR ? CC0Threshold=255 : CC0Threshold=0;
	Features.DeriveMomentsGrayTone(*(pGrpCC0->m_pGrp),CC0Threshold,2);
	Features.DeriveCentralMoments();
	if (videndo::CFeatures::PrimaryAxisExists(Features.m_CentrMom, PRIMARY_AXIS_THRESHOLD)==true)
	{
		m_Rotation=-videndo::CFeatures::GetPrincipalOrientation(Features.m_CentrMom,PRIMARY_AXIS_THRESHOLD);

//		cout << "Principal orientation: " << ipl::RadToDegree(m_Rotation) << " degrees" << endl;
		// rotate but don't stretch
//		SubImage.Save("d:/temp/test.bmp");
		CCoordinateTransform::ScaleAndRotateAuto(SubImage,SubImage,1,1,m_Rotation,0);
		// find new black blobs in rotated sub-image
		unsigned int i;
		vector<CPixelGroupExt> GrpsExtBlack, GrpsExtWhite;
		CPixelGroups GrpsBlack, GrpsWhite;
		if (DeriveBlobsExtAddColors(GrpsWhite,GrpsBlack,GrpsExtWhite,GrpsExtBlack,
								SubImage, Threshold, Threshold,m_MinBlobSize)==false)
								return false;
		for(i=0; i<GrpsExtWhite.size(); i++)
		{
			if (GetMarkCenter(&GrpsExtWhite[i], &GrpsExtBlack,true, true, true)==true)
			{
				// now we need to find Id's
				break;
			}
		}
		if (i==GrpsExtWhite.size())
		{
			if (m_Verbose==true)
				PrintMessage("CRefMark::CheckForRotateSubImage() Failed finding CC0 in rotated subimage");
			return false;
		}
		m_Stretch=(double)(GetCenterWidth())/(GetCenterHeight());
//		cout << Stretch << endl;
		CCoordinateTransform::ScaleAndRotateAuto(SubImage,SubImage,1,m_Stretch,0,0);

		// must retrieve new marks after scaling to ensure we get correct size of center mark
		if (DeriveBlobsExtAddColors(GrpsWhite,GrpsBlack,GrpsExtWhite,GrpsExtBlack,
								SubImage, Threshold, Threshold,m_MinBlobSize)==false)
								return false;
		for(i=0; i<GrpsExtWhite.size(); i++)
		{
			if (GetMarkCenter(&GrpsExtWhite[i], &GrpsExtBlack,true, true, true)==true)
				break;
		}
		if (i==GrpsExtWhite.size())
		{
			if (m_Verbose==true)
				PrintMessage("CRefMark::CheckForRotateSubImage() Failed finding CC0 in rotated subimage");
			return false;
		}
		//SubImage.Save("d:/temp/test3.bmp");
	}
	else
	{
		m_Rotation=0;
		m_Stretch=1;
	}
	return true;
}

/* Finds code circles from GrpsExt (or an internal new set if rotation and stretching is performed). 
   Four different checks must be fulfilled in order to accept a blob as a code circle:
   1) must be located in a distance from the mark center to an interval
      [DistToCodeCircles*(1+CODE_CIRCLES_SEARCH_FRACTION,DistToCodeCircles*(1-CODE_CIRCLES_SEARCH_FRACTION]
   2) the ratio between width and height of the blob must be 1+-BLOB_RATIO_TOLERANCE
   3) the width and height of the blob must be less than 3*GetCodeStartCircleRadius() in the reference mark
   4) the ratio of the blobs rectangle area to the biggest circle inside that rectangle must fulfill:
             AreaRatio > (PI/4-AREA_RATIO_TOLERANCE)
			 where AreaRatio NumberOfPixels/(BlobWidth*BlobHeight)
   5) code circle blob size (ccbs) must fulfill:
			 BCSize*(1.0-CODE_CIRCLE_SIZE_FRACTION) < ccbs <  CSSize*(1.0+CODE_CIRCLE_SIZE_FRACTION) */
bool CRefMark::FindId(const CPixelGroupExt* pGrpCC0, CImage& SubImage, unsigned int Threshold)
{
	if (CenterInUse()==false)
	{
		ostringstream ost;
		ost << "CRefMark::FindId() No center detected - cannot find an id" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (CodeCirclesInfoInUse()==false)
	{
		ostringstream ost;
		ost << "CRefMark::FindId() No info about code circles dist and radius available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	CPoint2D<double> OriginalCenter(GetCenter());
	unsigned int i;
	vector<CPixelGroupExt> GrpsExtBlack, GrpsExtWhite;
	CPixelGroups GrpsBlack, GrpsWhite;
	// This method rotates and stretches image if needed. In that case SubImage will hold the new image
//	SubImage.Save("d:/temp/test3.bmp");
	CheckForRotateSubImage(pGrpCC0,SubImage,Threshold);
	CImage TempImg;
	TempImg.CopyConvertThreshold(Threshold,SubImage);
//	TempImg.Save("d:/temp/test4.bmp");
//	SubImage.Save("d:/temp/test.bmp");
	if (DeriveBlobsExtAddColors(GrpsWhite,GrpsBlack,GrpsExtWhite,GrpsExtBlack,
							SubImage, Threshold, Threshold,m_MinBlobSize)==false)
							return false;
	for(i=0; i<GrpsExtWhite.size(); i++)
	{
		if (GetMarkCenter(&GrpsExtWhite[i], &GrpsExtBlack,true, true, true)==true)
		{
			// now we need to find Id's
			break;
		}
	}
	if (i==GrpsExtWhite.size())
	{
		if (m_Verbose==true)
			PrintMessage(OriginalCenter,"CRefMark::FindId() Failed finding CC0 in rotated subimage");
		return false;
	}
	// We are now ready to find Id for this reference mark. pGrpsExt contains the groups to be searched
	// IndexCC0 contains the CC0 index in Grps2 and GrpsExt
	float searchDistMin=(float)(GetDistToCodeCircles()*(1-CODE_CIRCLES_SEARCH_FRACTION));
	float searchDistMax=(float)(GetDistToCodeCircles()*(1+CODE_CIRCLES_SEARCH_FRACTION));

	const CPixelGroupExt* pTempBlobs[7];
	float TempAngles[7];
	unsigned int TotalTempBlobs=0;
	unsigned int CSSize,CSIndex;
	float CSAngle;
	for(i=0; i<7; i=i+1)
		pTempBlobs[i]=NULL;
//	/* update center of reference mark to be in subimage coordinatesystem */
//	center.x -= xmin;
//	center.y -= ymin;
	/* locate the blobs and calculate the Id */
	for(i=0; i<GrpsExtBlack.size(); i++)
	{
		const CPixelGroupExt* pGrp=&GrpsExtBlack[i];
		/* checking first condition (see documention in top of this file) */
		float TempDist=GetCenter().GetDist(pGrp->GetCM());
		if ((TempDist> searchDistMin) && (TempDist < searchDistMax))
		{

			/* checking next four conditions (see documention in top of this file) */
			int BlobWidth=pGrp->m_pGrp->GetRight().GetX()-pGrp->m_pGrp->GetLeft().GetX()+1;
			int BlobHeight=pGrp->m_pGrp->GetBottom().GetY()-pGrp->m_pGrp->GetTop().GetY()+1;
			/*float BlobCircleArea=(float)PI*(BlobWidth/2)*(BlobWidth/2);*/
			float BlobRatio=(float)BlobWidth/BlobHeight;
			float AreaRatio=(float)pGrp->m_pGrp->GetTotalPositions()/(BlobWidth*BlobHeight);

			float MaxBlobDim=(float)3.0*GetCodeStartCircleRadius();
			float CSSize=ipl::PI*GetCodeStartCircleRadius()*GetCodeStartCircleRadius();
			float BCSize=ipl::PI*GetBinaryCodeCircleRadius()*GetBinaryCodeCircleRadius();
			if ((BlobRatio>1-BLOB_RATIO_TOLERANCE) && (BlobRatio<1+BLOB_RATIO_TOLERANCE) && 
				(BlobWidth<MaxBlobDim) && (BlobHeight<MaxBlobDim) &&
				(AreaRatio > (ipl::PI/4-AREA_RATIO_TOLERANCE)) &&
				(BCSize*(1.0-CODE_CIRCLE_SIZE_FRACTION) <	pGrp->m_pGrp->GetTotalPositions()) &&
				(pGrp->m_pGrp->GetTotalPositions() < CSSize*(1.0+CODE_CIRCLE_SIZE_FRACTION)))
			{
				if(TotalTempBlobs<7)
				{
					/* identification blob found */
					pTempBlobs[TotalTempBlobs]=pGrp;
					TempAngles[TotalTempBlobs]=CVector2D<double>(pGrp->GetCM()-GetCenter()).GetAngle();
					double TestDegree=ipl::RadToDegree(TempAngles[TotalTempBlobs]);
					TotalTempBlobs=TotalTempBlobs+1;
				}
			}
		}
	}
	/* arrange the order of the code circles
	   find Code Start Circle */
	bool ReturnValue=true;
	CSSize=0;
	CSIndex=0;
	for(i=0; i<TotalTempBlobs; i=i+1)
	{
		const CPixelGroupExt* pGrp=pTempBlobs[i];
		if(pGrp->m_pGrp->GetTotalPositions()>CSSize)
		{
			CSSize=pGrp->m_pGrp->GetTotalPositions();
			CSIndex=i; /* code start index */
			/* Set center of mass in the CSCenter member - inverse adjust 
				with the scaling and rotation */
			CVector2D<double> Vec(pGrp->GetCM()-GetCenter());
			Vec.SetY(Vec.GetY()*(1.0/m_Stretch));
			Vec.Rotate(-m_Rotation);
			m_CSCenter=OriginalCenter+Vec;
		}
	}
	// check if the biggest code circle fulfills requirements to be a code start circle
	const float CODE_CS_TOLERANCE=(float)1.4;
	const float CODE_BC_TOLERANCE=(float)1.1;
	float AreaCS=ipl::PI*GetCodeStartCircleRadius()*GetCodeStartCircleRadius();
	float AreaBC=ipl::PI*GetBinaryCodeCircleRadius()*GetBinaryCodeCircleRadius();
	if ((CSSize<AreaBC*CODE_BC_TOLERANCE) || (CSSize>AreaCS*CODE_CS_TOLERANCE))
	{
		if (m_Verbose==true)
		{
			PrintMessage(OriginalCenter,"CRefMark::FindId() No code start circle found");
		}
		return false;
	}



	UINT8 Id;
	if(pTempBlobs[CSIndex]!=NULL)
	{
		CSAngle=TempAngles[CSIndex];
		/* find Id */
		Id=0;
		for(i=0; i<TotalTempBlobs; i=i+1)
		{
			/* calculate angles relative to the Code Start Circle angle */
			TempAngles[i]=TempAngles[i]-CSAngle;
			if(TempAngles[i]<0)
			{
				TempAngles[i]=(float)(TempAngles[i]+2.0*ipl::PI);
			}
			if(i!=CSIndex)
			{
				float delta=(float)(m_Alpha/2.0);
				if((TempAngles[i]>1.0*m_Alpha-delta)&&(TempAngles[i]<1.0*m_Alpha+delta))
				{
					if (Id & 1)
					{
						if (m_Verbose==true)
							PrintMessage(OriginalCenter,"CRefMark::FindId() Two code circles located at bit position 0");
						return false;
					}
					Id+=1;
				}
				else if((TempAngles[i]>2.0*m_Alpha-delta)&&(TempAngles[i]<2.0*m_Alpha+delta))
				{
					if (Id & 2)
					{
						if (m_Verbose==true)
							PrintMessage(OriginalCenter,"CRefMark::FindId() Two code circles located at bit position 1");
						return false;
					}
					Id+=2;
				}
				else if((TempAngles[i]>3.0*m_Alpha-delta)&&(TempAngles[i]<3.0*m_Alpha+delta))
				{
					if (Id & 4)
					{
						if (m_Verbose==true)
							PrintMessage(OriginalCenter,"CRefMark::FindId() Two code circles located at bit position 2");
						return false;
					}
					Id+=4;
				}
				else if((TempAngles[i]>4.0*m_Alpha-delta)&&(TempAngles[i]<4.0*m_Alpha+delta))
				{
					if (Id & 8)
					{
						if (m_Verbose==true)
							PrintMessage(OriginalCenter,"CRefMark::FindId() Two code circles located at bit position 3");
						return false;
					}
					Id+=8;
				}
				else if((TempAngles[i]>5.0*m_Alpha-delta)&&(TempAngles[i]<5.0*m_Alpha+delta))
				{
					if (Id & 16)
					{
						if (m_Verbose==true)
							PrintMessage(OriginalCenter,"CRefMark::FindId() Two code circles located at bit position 4");
						return false;
					}
					Id+=16;
				}
				else if((TempAngles[i]>6.0*m_Alpha-delta)&&(TempAngles[i]<6.0*m_Alpha+delta))
				{
					if (Id & 32)
					{
						if (m_Verbose==true)
							PrintMessage(OriginalCenter,"CRefMark::FindId() Two code circles located at bit position 5");
						return false;
					}
					Id+=32;
				}
				else
				{
					if (m_Verbose==true)
						PrintMessage(OriginalCenter,"CRefMark::FindId() Bad position of Code Circle - ignores this blob");
				}
			}
		}
		SetId(Id);
		SetCenter(OriginalCenter);
		ReturnValue=true;
	}
	else
		ReturnValue=false;

	return ReturnValue;
}

bool CRefMark::DeleteFromImage() const
{
	if (CenterInUse()==false)
	{
		ostringstream ost;
		ost << "CRefMark::DeleteFromImage() No mark detected - cannot delete from image" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (CodeCirclesInfoInUse()==false)
	{
		ostringstream ost;
		ost << "CRefMark::DeleteFromImage() No id detected for this mark - refusing to delete" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	const float SUB_IMAGE_FRACTION=(float)0.8;
	float SubImgDim=2*(GetDistToCodeCircles()+GetCodeStartCircleRadius())*SUB_IMAGE_FRACTION;
	float xmin,xmax,ymin,ymax;
	CPoint2D<double> Center=GetCenter();
	xmin = ((Center.GetX()-(SubImgDim/2)) <= 0) ? 0 : ipl::Round(Center.GetX()-(SubImgDim/2));
	xmax = ((Center.GetX()+(SubImgDim/2)) >= m_pImg->GetWidth()) ? (m_pImg->GetWidth()-1) : ipl::Round(Center.GetX()+(SubImgDim/2));
	ymin = ((Center.GetY()-(SubImgDim/2)) <= 0) ? 0 : ipl::Round(Center.GetY()-(SubImgDim/2));
	ymax = ((Center.GetY()+(SubImgDim/2)) >= m_pImg->GetHeight()) ? (m_pImg->GetHeight()-1) : ipl::Round(Center.GetY()+(SubImgDim/2));

	for(int x=xmin;x<xmax; x++)
	{
		for(int y=ymin; y<ymax; y++)
			m_pImg->SetPixelFast(x,y,0);
	}
//	m_pImg->Save("d:/temp/test.bmp");
	return true;
}

bool CRefMark::CopySubImage(CImage& Img)
{
	if (CenterInUse()==false)
	{
		ostringstream ost;
		ost << "CRefMark::CopySubImage() No mark detected - cannot copy subimage" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	if (CodeCirclesInfoInUse()==false)
	{
		ostringstream ost;
		ost << "CRefMark::CopySubImage() No code circle information available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	const float SUB_IMAGE_FRACTION=(float)1.5;
	float SubImgDim=2*(GetDistToCodeCircles()+GetCodeStartCircleRadius())*SUB_IMAGE_FRACTION;
	float xmin,xmax,ymin,ymax;
	CPoint2D<double> Center=GetCenter();
	/* copy sub image */
	xmin = ((Center.GetX()-(SubImgDim/2)) <= 0) ? 0 : ipl::Round(Center.GetX()-(SubImgDim/2));
	xmax = ((Center.GetX()+(SubImgDim/2)) >= m_pImg->GetWidth()) ? (m_pImg->GetWidth()-1) : ipl::Round(Center.GetX()+(SubImgDim/2));
	ymin = ((Center.GetY()-(SubImgDim/2)) <= 0) ? 0 : ipl::Round(Center.GetY()-(SubImgDim/2));
	ymax = ((Center.GetY()+(SubImgDim/2)) >= m_pImg->GetHeight()) ? (m_pImg->GetHeight()-1) : ipl::Round(Center.GetY()+(SubImgDim/2));
	Img.CopySubImage(*m_pImg,xmin,ymin,xmax,ymax);
	Img.SetOrigo(-xmin,-ymin);
	return true;
}

class CRefMarkBig : public CRefMark
{
	private: // attributes
		bool m_BlobsAvailable;
		const CPixelGroupExt* m_pGrpCC0;
		const CPixelGroupExt* m_pGrpCC1;
		const CPixelGroupExt* m_pGrpCC2;
	public: // members
		/// Default constructor
		CRefMarkBig() : m_BlobsAvailable(false),CRefMark(){m_Alpha=ipl::PI/7.0;m_ColorTypeCC0=LOWCOLOR;}
		bool FindMarkCenter();
		/** Finds the optimal threshold, corrects the found center with this threshold used and
			calls base class' FindId() with the blobs derived from this threshold. */
		bool CorrectCenterAndFindId();
	private: // methods
		/// Constructor
		bool BlobsAvailable() const{return m_BlobsAvailable;}
		void SetBlobs(const CPixelGroupExt& GrpCC0,const CPixelGroupExt& GrpCC1,const CPixelGroupExt& GrpCC2)
		{
			m_pGrpCC0=&GrpCC0;
			m_pGrpCC1=&GrpCC1;
			m_pGrpCC2=&GrpCC2;
			m_BlobsAvailable=true;
		}
		bool GetOptimalThreshold(unsigned int& LowThreshold, unsigned int& HighThreshold) const;
		/** Searches for a mark center from a given white blob. Depending on the boolean
			values, the Center, CC0, CC1 and CC2 blobs, and code circle info can be set.
			CM must be available for all supplied CPixelGroupExt types.
			@param WhiteGrp White blob to be checked.
			@param BlackGrps Black groups to be searched.
			@param bSetCenter If true, SetCenter() is called if a mark is detected.
			@param bSetBlobs If true, the members m_pGrpCC0, m_pGrpCC1, and m_pGrpCC2 are set.
			@param bSetCodeCircleInfo If true, SetCodeCircleInfo() is called if mark is detected.
			@return True, if a mark center is detected. */
		bool GetMarkCenter(const CPixelGroupExt* WhiteGrp, const vector<CPixelGroupExt>* BlackGrps,
			bool bSetCenter, bool bSetBlobs, bool bSetCodeCircleInfo);
		/** Checks if the requirements to accept the three center circles CC0, CC1, CC2 are met.
			@param pGrpCC0 Since we don't know at the time of calling this function, which really is CC0 and CC2,
				the returned pointer in pGrpCC0 will point to the real CC0 chosen from GrpCC0 and GrpCC2.
			@param RadiusCC0 Since this radius is calculated inside the function we return it, since it is used later.
			@return True, if requirements are fulfilled. */
		bool CheckCCRequirements(const CPixelGroup& GrpCC0, const CPixelGroup& GrpCC1,const CPixelGroup& GrpCC2,
								double& RadiusCC0) const;
		/// Finds the corrected refmark center by taking perspective effect into account
		void CorrectCenter(const CPoint2D<double>& CM0, const CPoint2D<double>& CM1, 
								const CPoint2D<double>& CM2);
		/** Returns width of center blob. Used to find stretching factor for ref mark. m_BlobsAvailable must be true! */
		unsigned int GetCenterWidth() const;
		/** Returns height of center blob. Used to find stretching factor for ref mark. m_BlobsAvailable must be true! */
		unsigned int GetCenterHeight()  const;
};

bool CRefMarkBig::FindMarkCenter()
{
	if (IsInitialized()==false)
	{
		ostringstream ost;
		ost << "CRefMarkBig::FindMarkCenter() CRefMarkBig not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	unsigned int i;
	for(i=m_WhiteBlobSearchIndex; i<m_pGroupsExtWhite->size(); i++)
	{
		if (GetMarkCenter(&(*m_pGroupsExtWhite)[i], m_pGroupsExtBlack,true, true, true)==true)
		{
			// start at this index next time
			m_WhiteBlobSearchIndex=i+1;
			// return if success
			return true;
		}
	}
	return false;
}

bool CRefMarkBig::GetMarkCenter(const CPixelGroupExt* WhiteGrp, const vector<CPixelGroupExt>* BlackGrps,
	bool bSetCenter, bool bSetBlobs, bool bSetCodeCircleInfo)
{
	unsigned int j,k;
	const CPoint2D<double>& CMWhite=WhiteGrp->GetCM();
	double RadiusWhite=sqrt((2.0*WhiteGrp->m_pGrp->GetTotalPositions())/ipl::PI);
	// find two black blobs with same center of mass
	for(j=0; j<BlackGrps->size(); j++)
	{
		// Check for concentric circles
		if (CMWhite.GetDist((*BlackGrps)[j].GetCM()) < RadiusWhite*CMTOLERANCE_NEW)
//		if (CMWhite.GetDist((*BlackGrps)[j].GetCM()) < CMTOLERANCE_NEW)
		{
			// find second black blob
			for(k=j+1; k<BlackGrps->size(); k++)
			{
				if (CMWhite.GetDist((*BlackGrps)[k].GetCM()) < RadiusWhite*CMTOLERANCE_NEW)
//				if (CMWhite.GetDist((*BlackGrps)[k].GetCM()) < CMTOLERANCE_NEW)
				{

					const CPixelGroupExt* pGrpCC0=&((*BlackGrps)[j]);
					const CPixelGroupExt* pGrpCC2=&((*BlackGrps)[k]);
					if (pGrpCC0->m_pGrp->GetRight().GetX()-pGrpCC0->m_pGrp->GetLeft().GetX() > 
						pGrpCC2->m_pGrp->GetRight().GetX()-pGrpCC2->m_pGrp->GetLeft().GetX())
						ipl::Swap(pGrpCC0,pGrpCC2);
					double RadiusCC0;
					if (CheckCCRequirements(*pGrpCC0->m_pGrp, *(WhiteGrp->m_pGrp), *pGrpCC2->m_pGrp, RadiusCC0)==true)
					{
						// our criterias for finding a reference center are fulfilled
							if (bSetCenter==true)
							SetCenter(pGrpCC0->GetCM());
						if (bSetBlobs==true)
							SetBlobs(*pGrpCC0,*WhiteGrp,*pGrpCC2);
						if (bSetCodeCircleInfo==true)
						{
							// must also set code circle (id) info
							SetCodeCircleInfo(RadiusCC0*17.0/5.0,RadiusCC0*3.0/5.0,RadiusCC0*2.0/5.0);
						}
						return true;
					}
				}
			}
		}
	}
	// if we end here, no mark were found
	return false;
}

bool CRefMarkBig::CheckCCRequirements(const CPixelGroup& GrpCC0, const CPixelGroup& GrpCC1,
									  const CPixelGroup& GrpCC2, double& RadiusCC0) const
{
	// now check requirements for radius - we find radius as an average of width and height
	RadiusCC0=(float)((GrpCC0.GetRight().GetX()-GrpCC0.GetLeft().GetX()+1) +
								 (GrpCC0.GetBottom().GetY()-GrpCC0.GetTop().GetY()+1))/4.0;
	float RadiusCC2=(float)((GrpCC2.GetRight().GetX()-GrpCC2.GetLeft().GetX()+1) +
								 (GrpCC2.GetBottom().GetY()-GrpCC2.GetTop().GetY()+1))/4.0;
	float RadiusCC1=(float)((GrpCC1.GetRight().GetX()-GrpCC1.GetLeft().GetX()+1) +
								 (GrpCC1.GetBottom().GetY()-GrpCC1.GetTop().GetY()+1))/4.0;
	if ((RadiusCC1>RadiusCC0) && (RadiusCC1<RadiusCC2))
	{
		// Check requirements for area for center black blob
		unsigned int CC0Width=(GrpCC0.GetRight().GetX()-GrpCC0.GetLeft().GetX()+1);
		unsigned int CC0Height=GrpCC0.GetBottom().GetY()-GrpCC0.GetTop().GetY()+1;
		float AreaRatio=(float)GrpCC0.GetTotalPositions()/(CC0Width*CC0Height);
		if (AreaRatio > (ipl::PI/4-AREA_RATIO_TOLERANCE))
		{
			return true;
		}
	}
	// if we end up here, the found center circles were not accepted as a reference mark
	return false;
}

bool CRefMarkBig::CorrectCenterAndFindId()
{
	unsigned int HighThreshold, LowThreshold,i;
	if (GetOptimalThreshold(LowThreshold,HighThreshold)==false)
		return false;
	CImage SubImg;
	if (CopySubImage(SubImg)==false)
		return false;

//	SubImg.Save("d:/temp/test2.bmp");
//	CImage TempImg;
//	TempImg.CopyConvertThreshold(LowThreshold,SubImg);
//	TempImg.Save("d:/temp/test5.bmp");
	CPixelGroups GroupsWhite, GroupsBlack;
	vector<CPixelGroupExt> GroupsExtWhite, GroupsExtBlack;
	if (CRefMark::DeriveBlobsExtAddColors(GroupsWhite,GroupsBlack,GroupsExtWhite,GroupsExtBlack,
							SubImg, LowThreshold, HighThreshold,m_MinBlobSize)==false)
							return false;
	for(i=0; i<GroupsExtWhite.size(); i++)
	{
		if (GetMarkCenter(&GroupsExtWhite[i], &GroupsExtBlack,true, true, true)==true)
		{
			if (m_UseCC0Center==false)
			{
				CPoint2D<double> CM0, CM1, CM2;
				CM0=videndo::CFeatures::GetInterpolatedCenterOfMass(*(m_pGrpCC0->m_pGrp),SubImg,255);
				CM1=videndo::CFeatures::GetInterpolatedCenterOfMass(*(m_pGrpCC1->m_pGrp),SubImg,0);
				CM2=videndo::CFeatures::GetInterpolatedCenterOfMass(*(m_pGrpCC2->m_pGrp),SubImg,255);

				// blobs are set in m_pGrpCC0, m_pGrpCC1, and m_pGrpCC2
				// we can now find a corrected center of mass for the reference mark
				CorrectCenter(CM0, CM1, CM2);
			}
			// now we need to find Id's - calling base class FindId()
			return FindId(m_pGrpCC0,SubImg,ipl::Round((float)(HighThreshold+LowThreshold)/2.0));
		}
	}
	return false;
}

void CRefMarkBig::CorrectCenter(const CPoint2D<double>& CM0, const CPoint2D<double>& CM1, 
								const CPoint2D<double>& CM2)
{
	vector<double> x;
	vector<double> y;
	double a,b;

	double RadiusCC0=1;
	double AreaCC0 = ipl::PI*RadiusCC0*RadiusCC0;
	double RadiusCC1 = sqrt((3*AreaCC0)/ipl::PI);
	double RadiusCC2 = sqrt((5*AreaCC0)/ipl::PI);

	x.push_back(RadiusCC0*RadiusCC0*0.5);
	x.push_back((RadiusCC1*RadiusCC1 + RadiusCC0*RadiusCC0)*0.5);
	x.push_back((RadiusCC2*RadiusCC2 + RadiusCC1*RadiusCC1)*0.5);

	// correct x-direction
	y.push_back(CM0.GetX());
	y.push_back(CM1.GetX());
	y.push_back(CM2.GetX());
	Fit(x,y,a,b);
	double DeltaX=b;

	y.clear();
	// correct y-direction
	y.push_back(CM0.GetY());
	y.push_back(CM1.GetY());
	y.push_back(CM2.GetY());
	Fit(x,y,a,b);
	double DeltaY=b;
	
	CPoint2D<double> Center(GetCenter());
	Center-=CPoint2D<double>(DeltaX,DeltaY);
	SetCenter(Center);
}

bool CRefMarkBig::GetOptimalThreshold(unsigned int& LowThreshold,unsigned int& HighThreshold) const
{
	if (BlobsAvailable()==false)
	{
		ostringstream ost;
		ost << "CRefMarkBig::GetOptimalThreshold() Blobs not available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	// The number of pixels to find an average from. Uses the most white and most black pixels
	const unsigned int AVERAGE_TOTAL_PIXELS=20;
	// The average of AVERAGE_TOTAL_PIXELS in the white pixel group times this value is used as high threshold
	const double FRACTION_OF_WHITE_LEVEL=0.65;
	vector<ipl::UINT32> Vec;
	int i;
	// white group
	unsigned int Total=m_pGrpCC1->m_pGrp->GetTotalPositions();
	if (Total<=AVERAGE_TOTAL_PIXELS)
	{
		if (m_Verbose==true)
		{
			ostringstream ost;
			ost << "CRefMarkBig::GetOptimalThreshold() White blob only contains " << Total << " Pixels, needs " 
				<< AVERAGE_TOTAL_PIXELS << IPLAddFileAndLine;
			PrintMessage(ost.str().c_str());
		}
		return false;
	}
	Vec.resize(Total);
	for(i=0;i<Total;i++)
		Vec[i]=m_pGrpCC1->m_pGrp->GetColor(i);
	sort(Vec.begin(),Vec.end());
	double Temp=0.0;
	for(i=Vec.size()-AVERAGE_TOTAL_PIXELS;i<Vec.size(); i++)
		Temp+=Vec[i];
	double AvWhite=Round(Temp/AVERAGE_TOTAL_PIXELS);
	HighThreshold=Round((double)(AvWhite*FRACTION_OF_WHITE_LEVEL));

	// black group
	Total=m_pGrpCC2->m_pGrp->GetTotalPositions();
	if (Total<=AVERAGE_TOTAL_PIXELS)
	{
		if (m_Verbose==true)
		{
			ostringstream ost;
			ost << "CRefMarkBig::GetOptimalThreshold() White blob only contains " << Total << " Pixels, needs " 
				<< AVERAGE_TOTAL_PIXELS << IPLAddFileAndLine;
			PrintMessage(ost.str().c_str());
		}
		return false;
	}
	Vec.resize(Total);
	for(i=0;i<Total;i++)
		Vec[i]=m_pGrpCC2->m_pGrp->GetColor(i);
	sort(Vec.begin(),Vec.end());
	Temp=0.0;
	for(i=0;i<AVERAGE_TOTAL_PIXELS; i++)
		Temp+=Vec[i];
	double AvBlack=Round(Temp/AVERAGE_TOTAL_PIXELS);
	LowThreshold=Round(AvBlack+(double)(AvWhite*(1.0-FRACTION_OF_WHITE_LEVEL)));
	if (LowThreshold>=HighThreshold)
	{
		double Mean=((double)LowThreshold+(double)HighThreshold)/2.0;
		LowThreshold=(int)(Mean);
		HighThreshold=(int)(Mean+1);
	}
	return true;
}

unsigned int CRefMarkBig::GetCenterWidth() const
{
	if (m_BlobsAvailable==false)
	{
		ostringstream ost;
		ost << "CRefMarkBig::GetCenterWidth() Blobs not available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return 0;
	}
	const CPixelGroup& Grp=*(m_pGrpCC0->m_pGrp);
	return (Grp.GetRight().GetX()-Grp.GetLeft().GetX()+1);
}

unsigned int CRefMarkBig::GetCenterHeight() const
{
	if (m_BlobsAvailable==false)
	{
		ostringstream ost;
		ost << "CRefMarkBig::GetCenterHeight() Blobs not available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return 0;
	}
	const CPixelGroup& Grp=*(m_pGrpCC0->m_pGrp);
	return (Grp.GetBottom().GetY()-Grp.GetTop().GetY()+1);
}

class CRefMarkSmall : public CRefMark
{
	private: // attributes
		bool m_BlobsAvailable;
		const CPixelGroupExt* m_pGrpCC0;
		const CPixelGroupExt* m_pGrpCC1;
	public: // members
		/// Default constructor
		CRefMarkSmall() : m_BlobsAvailable(false),CRefMark(){m_Alpha=ipl::PI/5.0;m_ColorTypeCC0=HIGHCOLOR;}
		bool FindMarkCenter();
		/** Finds the optimal threshold, corrects the found center with this threshold used and
			calls base class' FindId() with the blobs derived from this threshold. */
		bool CorrectCenterAndFindId();
	private: // methods
		/// Constructor
		bool BlobsAvailable() const{return m_BlobsAvailable;}
		void SetBlobs(const CPixelGroupExt& GrpCC0,const CPixelGroupExt& GrpCC1)
		{
			m_pGrpCC0=&GrpCC0;
			m_pGrpCC1=&GrpCC1;
			m_BlobsAvailable=true;
		}
		bool GetOptimalThreshold(unsigned int& LowThreshold, unsigned int& HighThreshold) const;
		/** Searches for a mark center from a given white blob. Depending on the boolean
			values, the Center, CC0, and CC1 blobs, and code circle info can be set.
			CM must be available for all supplied CPixelGroupExt types.
			@param WhiteGrp White blob to be checked.
			@param BlackGrps Black groups to be searched.
			@param bSetCenter If true, SetCenter() is called if a mark is detected.
			@param bSetBlobs If true, the members m_pGrpCC0, and m_pGrpCC1 are set.
			@param bSetCodeCircleInfo If true, SetCodeCircleInfo() is called if mark is detected.
			@return True, if a mark center is detected. */
		bool GetMarkCenter(const CPixelGroupExt* WhiteGrp, const vector<CPixelGroupExt>* BlackGrps,
			bool bSetCenter, bool bSetBlobs, bool bSetCodeCircleInfo);
		/** Checks if the requirements to accept the two center circles CC0, CC1 are met.
			@param RadiusCC0 Since this radius is calculated inside the function we return it, since it is used later.
			@return True, if requirements are fulfilled. */
		bool CheckCCRequirements(const CPixelGroup& GrpCC0, const CPixelGroup& GrpCC1, double& RadiusCC0) const;
		/// Finds the corrected refmark center by taking perspective effect into account
		void CorrectCenter(const CPoint2D<double>& CM0, const CPoint2D<double>& CM1);
		/** Returns width of center blob. Used to find stretching factor for ref mark. m_BlobsAvailable must be true! */
		unsigned int GetCenterWidth() const;
		/** Returns height of center blob. Used to find stretching factor for ref mark. m_BlobsAvailable must be true! */
		unsigned int GetCenterHeight() const;
};

bool CRefMarkSmall::FindMarkCenter()
{
	if (IsInitialized()==false)
	{
		ostringstream ost;
		ost << "CRefMarkSmall::FindMarkCenter() CRefMarkBig not initialized" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	unsigned int i;
	for(i=m_WhiteBlobSearchIndex; i<m_pGroupsExtWhite->size(); i++)
	{
		if (GetMarkCenter(&(*m_pGroupsExtWhite)[i], m_pGroupsExtBlack,true, true, true)==true)
		{
			// start at this index next time
			m_WhiteBlobSearchIndex=i+1;
			// return if success
			return true;
		}
	}
	return false;
}

bool CRefMarkSmall::GetMarkCenter(const CPixelGroupExt* WhiteGrp, const vector<CPixelGroupExt>* BlackGrps,
	bool bSetCenter, bool bSetBlobs, bool bSetCodeCircleInfo)
{
	unsigned int j;
	const CPoint2D<double>& CMWhite=WhiteGrp->GetCM();
	double RadiusWhite=sqrt(((double)WhiteGrp->m_pGrp->GetTotalPositions())/ipl::PI);
	// find two blobs with same center of mass, one white and one black
	for(j=0; j<BlackGrps->size(); j++)
	{
		// Check for concentric circles
		if (CMWhite.GetDist((*BlackGrps)[j].GetCM()) < RadiusWhite*CMTOLERANCE_NEW)
		{
			const CPixelGroupExt* pGrpCC1=&((*BlackGrps)[j]);
			double RadiusCC0;
			if (CheckCCRequirements(*(WhiteGrp->m_pGrp),*pGrpCC1->m_pGrp, RadiusCC0)==true)
			{
				// our criterias for finding a reference center are fulfilled
				if (bSetCenter==true)
					SetCenter(WhiteGrp->GetCM());
				if (bSetBlobs==true)
					SetBlobs(*WhiteGrp,*pGrpCC1);
				if (bSetCodeCircleInfo==true)
				{
					// must also set code circle (id) info
					SetCodeCircleInfo(RadiusCC0*16.0/5.0,RadiusCC0*4.0/5.0,RadiusCC0*3.0/5.0);
				}
				return true;
			}
		}
	}
	// if we end here, no mark were found
	return false;
}

bool CRefMarkSmall::CheckCCRequirements(const CPixelGroup& GrpCC0, const CPixelGroup& GrpCC1,
									  double& RadiusCC0) const
{
	// now check requirements for radius - we find radius as an average of width and height
	RadiusCC0=(float)((GrpCC0.GetRight().GetX()-GrpCC0.GetLeft().GetX()+1) +
								 (GrpCC0.GetBottom().GetY()-GrpCC0.GetTop().GetY()+1))/4.0;
	float RadiusCC1=(float)((GrpCC1.GetRight().GetX()-GrpCC1.GetLeft().GetX()+1) +
								 (GrpCC1.GetBottom().GetY()-GrpCC1.GetTop().GetY()+1))/4.0;
	if (RadiusCC1>RadiusCC0)
	{
		// Check requirements for area for center white blob
		unsigned int CC0Width=(GrpCC0.GetRight().GetX()-GrpCC0.GetLeft().GetX()+1);
		unsigned int CC0Height=GrpCC0.GetBottom().GetY()-GrpCC0.GetTop().GetY()+1;
		float AreaRatio=(float)GrpCC0.GetTotalPositions()/(CC0Width*CC0Height);
		if (AreaRatio > (ipl::PI/4-AREA_RATIO_TOLERANCE))
		{
			return true;
		}
	}
	// if we end up here, the found center circles were not accepted as a reference mark
	return false;
}

bool CRefMarkSmall::CorrectCenterAndFindId()
{
	unsigned int HighThreshold, LowThreshold,i;
	if (GetOptimalThreshold(LowThreshold,HighThreshold)==false)
		return false;
	CImage SubImg;
	if (CopySubImage(SubImg)==false)
		return false;

	CPixelGroups GroupsWhite, GroupsBlack;
	vector<CPixelGroupExt> GroupsExtWhite, GroupsExtBlack;
	if (CRefMark::DeriveBlobsExtAddColors(GroupsWhite,GroupsBlack,GroupsExtWhite,GroupsExtBlack,
							SubImg, LowThreshold, HighThreshold,m_MinBlobSize)==false)
							return false;
	//SubImg.Save("d:/temp/test2.bmp");
	for(i=0; i<GroupsExtWhite.size(); i++)
	{
		if (GetMarkCenter(&GroupsExtWhite[i], &GroupsExtBlack,true, true, true)==true)
		{
			if (m_UseCC0Center==false)
			{
				CPoint2D<double> CM0, CM1;
				CM0=videndo::CFeatures::GetInterpolatedCenterOfMass(*(m_pGrpCC0->m_pGrp),SubImg,0);
				CM1=videndo::CFeatures::GetInterpolatedCenterOfMass(*(m_pGrpCC1->m_pGrp),SubImg,255);
				// blobs are set in m_pGrpCC0, and m_pGrpCC1
				// we can now find a corrected center of mass for the reference mark
				CorrectCenter(CM0, CM1);
			}
			// now we need to find Id's - calling base class FindId()
			return FindId(m_pGrpCC0,SubImg,ipl::Round((float)(HighThreshold+LowThreshold)/2.0));
			//return FindId(m_pGrpCC0,SubImg,HighThreshold);
		}
	}
	return false;
}

void CRefMarkSmall::CorrectCenter(const CPoint2D<double>& CM0, const CPoint2D<double>& CM1)
{
	vector<double> x;
	vector<double> y;
	double a,b;

	double RadiusCC0=1;
	double AreaCC0 = ipl::PI*RadiusCC0*RadiusCC0;
	double RadiusCC1 = sqrt((3*AreaCC0)/ipl::PI);

	x.push_back(RadiusCC0*RadiusCC0*0.5);
	x.push_back((RadiusCC1*RadiusCC1 + RadiusCC0*RadiusCC0)*0.5);

	// correct x-direction
	y.push_back(CM0.GetX());
	y.push_back(CM1.GetX());
	Fit(x,y,a,b);
	double DeltaX=b;

	y.clear();
	// correct y-direction
	y.push_back(CM0.GetY());
	y.push_back(CM1.GetY());
	Fit(x,y,a,b);
	double DeltaY=b;
	
	CPoint2D<double> Center(GetCenter());
	Center-=CPoint2D<double>(DeltaX,DeltaY);
	SetCenter(Center);
}

bool CRefMarkSmall::GetOptimalThreshold(unsigned int& LowThreshold,unsigned int& HighThreshold) const
{
	if (BlobsAvailable()==false)
	{
		ostringstream ost;
		ost << "CRefMarkSmall::GetOptimalThreshold() Blobs not available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	// The number of pixels to find an average from. Uses the most white and most black pixels
	const unsigned int AVERAGE_TOTAL_PIXELS=15;
	// The average of AVERAGE_TOTAL_PIXELS in the white pixel group times this value is used as high threshold
	const double FRACTION_OF_WHITE_LEVEL=0.65;
	vector<ipl::UINT32> Vec;
	int i;
	// white group
	unsigned int Total=m_pGrpCC0->m_pGrp->GetTotalPositions();
	if (Total<=AVERAGE_TOTAL_PIXELS)
	{
		if (m_Verbose==true)
		{
			ostringstream ost;
			ost << "CRefMarkSmall::GetOptimalThreshold() White blob only contains " << Total << " Pixels, needs " 
				<< AVERAGE_TOTAL_PIXELS << IPLAddFileAndLine;
			PrintMessage(ost.str().c_str());
		}
		return false;
	}
	Vec.resize(Total);
	for(i=0;i<Total;i++)
		Vec[i]=m_pGrpCC0->m_pGrp->GetColor(i);
	sort(Vec.begin(),Vec.end());
	double Temp=0.0;
	for(i=Vec.size()-AVERAGE_TOTAL_PIXELS;i<Vec.size(); i++)
		Temp+=Vec[i];
	double AvWhite=Round(Temp/AVERAGE_TOTAL_PIXELS);
	HighThreshold=Round((double)(AvWhite*FRACTION_OF_WHITE_LEVEL));

	// black group
	Total=m_pGrpCC1->m_pGrp->GetTotalPositions();
	if (Total<=AVERAGE_TOTAL_PIXELS)
	{
		if (m_Verbose==true)
		{
			ostringstream ost;
			ost << "CRefMarkSmall::GetOptimalThreshold() White blob only contains " << Total << " Pixels, needs " 
				<< AVERAGE_TOTAL_PIXELS << IPLAddFileAndLine;
			PrintMessage(ost.str().c_str());
		}
		return false;
	}
	Vec.resize(Total);
	for(i=0;i<Total;i++)
		Vec[i]=m_pGrpCC1->m_pGrp->GetColor(i);
	sort(Vec.begin(),Vec.end());
	Temp=0.0;
	for(i=0;i<AVERAGE_TOTAL_PIXELS; i++)
		Temp+=Vec[i];
	double AvBlack=Round(Temp/AVERAGE_TOTAL_PIXELS);
	LowThreshold=Round(AvBlack+(double)(AvWhite*(1.0-FRACTION_OF_WHITE_LEVEL)));
	return true;
}

unsigned int CRefMarkSmall::GetCenterWidth() const
{
	if (m_BlobsAvailable==false)
	{
		ostringstream ost;
		ost << "CRefMarkSmall::GetCenterWidth() Blobs not available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return 0;
	}
	const CPixelGroup& Grp=*(m_pGrpCC0->m_pGrp);
	return (Grp.GetRight().GetX()-Grp.GetLeft().GetX()+1);
}

unsigned int CRefMarkSmall::GetCenterHeight() const
{
	if (m_BlobsAvailable==false)
	{
		ostringstream ost;
		ost << "CRefMarkBig::GetCenterHeight() Blobs not available" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return 0;
	}
	const CPixelGroup& Grp=*(m_pGrpCC0->m_pGrp);
	return (Grp.GetBottom().GetY()-Grp.GetTop().GetY()+1);
}


unsigned short CMarkRetrieval::FindCalibrationMarksAndMatchIds(CImage& TempImg, /*CCorresponding3D2DPoints& PointSets,*/
								unsigned int Threshold, CStdImage& ImgPlot, unsigned int MinBlobSize, bool UseSimpleCenter, unsigned int RefMarkType)
{
	CPixelGroups GroupsWhite, GroupsBlack;
	vector<CPixelGroupExt> GroupsExtWhite, GroupsExtBlack;
	if (CRefMark::DeriveBlobsExtAddColors(GroupsWhite,GroupsBlack,GroupsExtWhite,GroupsExtBlack,TempImg, 
		Threshold, Threshold,MinBlobSize)==false)
		return false;
							
	if ((GroupsExtWhite.size()==0) || (GroupsExtBlack.size()==0))
		return false;
	// Find reference marks
	unsigned short FoundMarks=0;
	if (RefMarkType & REFMARK_BIG)
	{
		CRefMarkBig RefMark;
		RefMark.Initialize(GroupsExtWhite,GroupsExtBlack,TempImg,UseSimpleCenter,MinBlobSize,m_Verbose);
		FoundMarks += FindCalibrationMarksAndMatchIds(RefMark, /*PointSets,*/ ImgPlot);
	}
	if (RefMarkType & REFMARK_SMALL)
	{
		CRefMarkSmall RefMark;
		RefMark.Initialize(GroupsExtWhite,GroupsExtBlack,TempImg,UseSimpleCenter,MinBlobSize,m_Verbose);
		FoundMarks += FindCalibrationMarksAndMatchIds(RefMark, /*PointSets,*/ ImgPlot);
	}
	return FoundMarks;
}

unsigned short CMarkRetrieval::FindCalibrationMarksAndMatchIds(CRefMark& RefMark, /*CCorresponding3D2DPoints& PointSets, */
															   CStdImage& ImgPlot)
{
	unsigned short FoundMarks=0;
	bool Continue=true;
	while(Continue)
	{
		if (RefMark.FindMarkCenter()==true)
		{
			if (RefMark.CorrectCenterAndFindId()==true)
				if (RefMark.DeleteFromImage()==true)
				{
					unsigned int Index;
//					if (m_PointSets.GetIndex(Index, RefMark.GetId())==false)
					if (m_PointSets.IdExists(RefMark.GetId())==false)
					{
						if (m_FindNewRefMarks==true)
						{
//							if (m_NewPointSets.GetIndex(Index,RefMark.GetId())==true)
							if (m_NewPointSets.IdExists(RefMark.GetId())==true)
							{
								if (m_Verbose==true)
								{
									ostringstream ost;
									ost << "CMarkRetrieval::FindCalibrationMarksAndMatchIds() Found Id " << RefMark.GetId() 
										<< " already found as a new Id - ignoring" << IPLAddFileAndLine;
									CError::ShowMessage(IPL_WARNING,ost.str().c_str());
								}
							}
							else
							{
								m_NewPointSets.AddPoint2DAndId(RefMark.GetCenter(),RefMark.GetId());
								m_NewCSCenters.push_back(RefMark.GetCSCenter());
							}
						}
						else
						{
							if (m_Verbose==true)
							{
								ostringstream ost;
								ost << "CMarkRetrieval::FindCalibrationMarksAndMatchIds() Found Id " << RefMark.GetId() 
									<< " which is not present in setup - ignoring" << IPLAddFileAndLine;
								CError::ShowMessage(IPL_WARNING,ost.str().c_str());
							}
						}
					}
					else
					{
						m_PointSets.GetIndex(Index, RefMark.GetId());
						if (m_PointSets.Point2DInUse(Index)==true)
						{
							if (m_Verbose==true)
							{
								ostringstream ost;
								ost << "CMarkRetrieval::FindCalibrationMarksAndMatchIds() Found Id " << RefMark.GetId() 
									<< " already found - ignoring" << IPLAddFileAndLine;
								CError::ShowMessage(IPL_WARNING,ost.str().c_str());
							}
						}
						else
						{
							//m_PointSets.GetIndex(Index, RefMark.GetId());
							m_PointSets.SetPoint2D(RefMark.GetCenter(),Index);
							m_CSCenters[Index]=RefMark.GetCSCenter();
							//cout << "Id=" << RefMark.GetId() << " Dir=" << ipl::RadToDegree(m_Directions[Index]) << endl;
							FoundMarks++;
//							CPixelGroup Grp;
//							CGraphics::Cross(CPoint2D<ipl::INT16>(ipl::Round(RefMark.GetCenter().GetX()),
//								ipl::Round(RefMark.GetCenter().GetY())),5,Grp);
//							Grp.AddToImage(ImgPlot,255);
//							CPoint2D<int> P(RefMark.GetCenter());
//							P -= CPoint2D<int>(-20,-20);
//							ostringstream IdStr;
//							IdStr << RefMark.GetId();
//							CGraphics Graphics;
//							Graphics.SetFontTransparent(false);
//							Graphics.PlotString(IdStr.str().c_str(),P,ImgPlot);
//							ImgPlot.Save("d:/temp/test.bmp");
							if (m_Verbose==true)
							{
								k_RefMarkPrintf("Found Id=%d at position (%.1f,%.1f)",RefMark.GetId(),
											RefMark.GetCenter().GetX(),RefMark.GetCenter().GetY());
							}
						}
					}
				}
				else
					Continue=false;
		}
		else
			Continue=false;
	};
	return FoundMarks;
}

void CRefMark::Fit(const vector<double>& x, const vector<double>& y, double& a, double& b) const
{
	int i;
	int ndata=x.size();
	double t,sx=0.0,sy=0.0,st2=0.0,ss,sxoss;
	b=0.0;
	for(i=0;i<ndata;i++)
	{
		sx += x[i];
		sy += y[i];
	}
	ss=ndata;
	sxoss=sx/ss;
	for(i=0;i<ndata;i++)
	{
		t=x[i]-sxoss;
		st2 += t*t;
		b+=t*y[i];
	}
	b /= st2;
	a=(sy-sx*b)/ss;
}

bool CRefMark::DeriveBlobsAddColors(CPixelGroups& GroupsWhite,CPixelGroups& GroupsBlack,
								   CImage& Img, UINT8 Threshold, unsigned int MinBlobSize)
{
	if (CSegmentate::DeriveBlobs(Img,GroupsWhite, HIGHCOLOR, Threshold, ipl::FOURCONNECTED,MinBlobSize)==false)
		return false;
	if (CSegmentate::DeriveBlobs(Img,GroupsBlack, LOWCOLOR, Threshold, ipl::FOURCONNECTED,MinBlobSize)==false)
		return false;
	GroupsWhite.AddColors(Img);
	GroupsBlack.AddColors(Img);
	return true;
}

bool CRefMark::DeriveBlobsExtAddColors(CPixelGroups& GroupsWhite,CPixelGroups& GroupsBlack,
							vector<CPixelGroupExt>& GroupsExtWhite,vector<CPixelGroupExt>& GroupsExtBlack,
							CImage& Img, UINT8 LowThreshold, UINT8 HighThreshold,unsigned int MinBlobSize)
{
	unsigned int i;
	if (DeriveBlobsAddColors(GroupsWhite,GroupsBlack,Img,HighThreshold,MinBlobSize)==false)
		return false;

	// remove blobs touching the border of the image
	int MinX=Img.GetMinX();
	int MaxX=Img.GetMaxX()-1;
	int MinY=Img.GetMinY();
	int MaxY=Img.GetMaxY()-1;
	i=0;
	while(i<GroupsBlack.GetTotalGroups())
	{
		CPixelGroup& Grp=GroupsBlack[i];
		if ((Grp.GetLeft().GetX()==MinX) || (Grp.GetRight().GetX()==MaxX) ||
			(Grp.GetTop().GetY()==MinY) || (Grp.GetBottom().GetY()==MaxY))
		{
			GroupsBlack.RemoveGroup(i);
		}
		else
			i++;
	}

	GroupsExtBlack.resize(GroupsBlack.GetTotalGroups());
	for(i=0; i<GroupsBlack.GetTotalGroups(); i++)
		GroupsExtBlack[i].Set(&GroupsBlack[i],255); // background 255 for a black group to get correct moments	

	if (LowThreshold!=HighThreshold)
	{
		// only derive blobs again if two different thresholds are used
		// black blobs are not used in this case
		// this could be more efficient if DeriveBlobsAddColors only derived high or lowcolor based on a parameter
		CPixelGroups GroupsBlackTemp;
		if (DeriveBlobsAddColors(GroupsWhite,GroupsBlackTemp,Img,LowThreshold,MinBlobSize)==false)
			return false;
	}

	i=0;
	while(i<GroupsWhite.GetTotalGroups())
	{
		CPixelGroup& Grp=GroupsWhite[i];
		if ((Grp.GetLeft().GetX()==MinX) || (Grp.GetRight().GetX()==MaxX) ||
			(Grp.GetTop().GetY()==MinY) || (Grp.GetBottom().GetY()==MaxY))
		{
			GroupsWhite.RemoveGroup(i);
		}
		else
			i++;
	}
	GroupsExtWhite.resize(GroupsWhite.GetTotalGroups());
	for(i=0; i<GroupsWhite.GetTotalGroups(); i++)
		GroupsExtWhite[i].Set(&GroupsWhite[i],LOWCOLOR); // background 0 for a white group to get correct moments

	return true;
}

} // end videndo namespace
