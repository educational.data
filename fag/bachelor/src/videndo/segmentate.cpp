#include "segmentate.h"
#include <ipl98/cpp/error.h>
#include <ipl98/cpp/algorithms/segmentate.h>
#include <ipl98/cpp/palette.h>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/algorithms/local_operation.h>
#include <sstream>
#include <algorithm>

namespace videndo{ // use namespace if C++

using ipl::CError;
using ipl::IPL_ERROR;
using ipl::IPL_WARNING;
using ipl::CPalette;
using ipl::CLocalOperation;
using ipl::HIGHCOLOR;
using ipl::EIGHTCONNECTED;
using std::ostringstream;

bool CSegmentate::DeriveBlobsFromHue(const CImage& Source, CPixelGroups& PixelGroups,
				UINT8 HueLow, UINT8 HueHigh, CONNECTIVITY Connected, unsigned int MinSize)
{
	if (Source.GetBits()!=24)
	{
		ostringstream ost;
		ost << "CSegmentate::DeriveBlobsFromHue() Input image must be 24 b/p (current is " << 
			Source.GetBits() << ")" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	CImage BWImage(Source.GetWidth(),Source.GetHeight(),8,255);
	int y;
	UINT8 r,g,b;
	INT16 H,S,I;
	UINT32 Col;
	for(y=Source.GetMinY(); y<Source.GetMaxY(); y++)
	{
		CImage::ConstRowIterator24bp It=Source.ConstRow24bp(y);
		CImage::RowIterator8bp It2=BWImage.Row8bp(y);
		while(It!=It.End())
		{
			Col=*It;
			r=CColor::GetRedVal(Col);
			g=CColor::GetGreenVal(Col);
			b=CColor::GetBlueVal(Col);
			CColor::RGB2HSI(r,g,b,H,S,I);
			if ((H>=HueLow) && (H<HueHigh))
				*It2=0;
			else
				*It2=255;
			++It;
			++It2;
		}
	}
	return ipl::CSegmentate::DeriveBlobs(BWImage,PixelGroups,ipl::LOWCOLOR,128,Connected,MinSize);
}

bool CSegmentate::SegmentateFromRGBCone(const CImage& Source, CPixelGroups &PixelGroups, 
				UINT32 Color, float LowerLimit, float UpperLimit, float Angle, CONNECTIVITY Connected, unsigned int MinSize)
{
	if (Source.GetBits()!=24)
	{
		ostringstream ost;
		ost << "CSegmentate::SegmentateFromRGBCone() Input image must be 24 b/p (current is " << 
			Source.GetBits() << ")" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	CImage BWImage;
	if (CLocalOperation::RGBConeTransformation(Source, BWImage, Color, LowerLimit, UpperLimit, Angle)==false)
		return false;
//	BWImage.Save("d:/temp/test3.bmp");
	return ipl::CSegmentate::DeriveBlobs(BWImage,PixelGroups,ipl::HIGHCOLOR,128,Connected,MinSize);
}

bool CSegmentate::DeriveBlobsDynamic(CByteImage &Source, COLORTYPE Color, CONNECTIVITY C,
		int TotalBlur, int MinStartGradient, CPixelGroups &PixelGroups)
{
	CDynamicBlobDetection DynBlobDetect;
	return DynBlobDetect.DeriveBlobs(Source, Color,C,TotalBlur, 
											  MinStartGradient, PixelGroups);
}

bool CSegmentate::CDynamicBlobDetection::DeriveBlobs(CByteImage &Source, COLORTYPE Color,
												CONNECTIVITY C,	int TotalBlur, 
												int MinStartGradient, CPixelGroups &Blobs)
{
	int x,y,Threshold;
	CPixelGroup Blob;
	InitializeContourSearch(TotalBlur,MinStartGradient,100,2,2);
	m_xl=0;
	m_xr=Source.GetWidth();
	m_yt=0;
	m_yb=Source.GetHeight();
	BlurAndEdgeDetect(Source,m_MinStartGradient);
	if (Color==HIGHCOLOR)
		Source.SetBorder(2,0);
	else 
		Source.SetBorder(2,255);
	m_BlobImage.Alloc(Source.GetWidth(),Source.GetHeight(),1,0);
	m_BlobImage.SetBorder(1,0);
	m_BufImage.Alloc(Source.GetWidth(),Source.GetHeight(),1);
	std::sort(m_MaxGradient.begin(),m_MaxGradient.end());
	for (int i=0;i<m_MaxGradient.size();i++)
	{
		x=m_MaxGradient[i].GetX();
		y=m_MaxGradient[i].GetY();
		CPoint2D<INT16> P(x,y);
		Threshold=Source.GetPixelFast(x,y);
		if (Color==HIGHCOLOR)
			Threshold--;
		else
			Threshold++;
		if (m_BlobImage.GetPixelFast(x+1,y)==0 &&
			m_BlobImage.GetPixelFast(x-1,y)==0 &&
			m_BlobImage.GetPixelFast(x,y+1)==0 &&
			m_BlobImage.GetPixelFast(x,y-1)==0)
		{
			if(BlobExpand(Source,P,C,Threshold,Blob))
			{
				for (int j=0;j<Blob.GetTotalPositions();j++)
				{
					Blob.SetColor((UINT32)Threshold,j);
					m_BlobImage.SetPixelFast(Blob.GetPosition(j),1);			   	
				}
				Blobs.AddGroup(Blob);
			}
		}
	}
	return true;
}

bool CSegmentate::CDynamicBlobDetection::BlobExpand(CByteImage &Source,	const CPoint2D<INT16> &Seed, 
													CONNECTIVITY C,	int Threshold,
													CPixelGroup &Blob)
{
	vector<CPoint2D<INT16> > Untreated;
	Untreated.reserve(100);
    m_BufImage.Flush(0);
    Blob.Empty();
	CPoint2D<INT16> Q,P;
	Untreated.push_back(Seed);
	if (Source.GetPixelFast(Seed)>Threshold)
	{
		while (Untreated.size()>0)
		{
			P=Untreated.back();
			Untreated.pop_back();
			for (int i=0;i<4;i++)
			{
				Q=GetNeighbour(P,i);
				if (Source.GetPixelFast(Q)>Threshold)
				{
					if (m_BlobImage.GetPixelFast(Q)==1) 
						return false;
					if (m_BufImage.GetPixelFast(Q)==0) 
					{
						Untreated.push_back(Q);
						m_BufImage.SetPixelFast(Q,1);
					}
				}
			}
			if (C==EIGHTCONNECTED)
			{
				for (int i=4;i<7;i++)
				{
					Q=GetNeighbour(P,i);
					if (Source.GetPixelFast(Q)>Threshold)
					{
						if (m_BlobImage.GetPixelFast(Q)==1) 
							return false;
						if (m_BufImage.GetPixelFast(Q)==0) 
						{
							Untreated.push_back(Q);
							m_BufImage.SetPixelFast(Q,1);
						}
					}
				}
			}//EIGHTCONNECTED
			Blob.AddPosition(P);
		} //WHILE
	}
	else
	{
        while (Untreated.size()>0)
		{
			P=Untreated.back();
			Untreated.pop_back();
			for (int i=0;i<4;i++)
			{
				Q=GetNeighbour(P,i);
				if (Source.GetPixelFast(Q)<Threshold)
				{
					if (m_BlobImage.GetPixelFast(Q)==1) 
						return false;
					if (m_BufImage.GetPixelFast(Q)==0) 
					{
						Untreated.push_back(Q);
						m_BufImage.SetPixelFast(Q,1);
					}
				}
			}
			if (C==EIGHTCONNECTED)
			{
				for (int i=4;i<7;i++)
				{
					Q=GetNeighbour(P,i);
					if (Source.GetPixelFast(Q)<Threshold)
					{
						if (m_BlobImage.GetPixelFast(Q)==1) 
							return false;
						if (m_BufImage.GetPixelFast(Q)==0) 
						{
							Untreated.push_back(Q);
							m_BufImage.SetPixelFast(Q,1);
						}
					}
				}
			}//EIGHTCONNECTED
			Blob.AddPosition(P);
			
		} //WHILE
	}
	Blob.AllocColors();
	return true;
}

} // end namespace videndo
