#ifndef _SCAPE_POLAR3D_H
#define _SCAPE_POLAR3D_H

#include <iostream>
#include <points/point3d.h>

namespace videndo{ // use namespace if C++

/** CPolar3D template class with 3-dimensional polar points.
	This class is part of the Image Processing Library <a href="http://www.mip.sdu.dk/ipl98">IPL98</a>.
	\class CPolar3D videndo/polar3d.h
	@see CPolar3D
	@version 0.1,  by edr@mip.sdu.dk 
	@author Ren� Dencker Eriksen */
template<class T>
class CPolar3D : public CPoint3D<T>
{
	protected: // attributes
	public: // methods
		 /// default constructor, init to zero
		CPolar3D(){m_x=0;m_y=0;m_z=0;};
		/// constr. with init. of polar point
		CPolar3D(T Theta,T Phi,T Rho){m_x=Theta;m_y=Phi;m_z=Rho;}

		/// cast to POLY in mixed expressions
		template<class POLY> operator CPolar3D<POLY>(){return CPolar3D<POLY>(m_x,m_y,m_z);}

		/** Returns cartesian coordinates of this polar value. */
		inline CPoint3D<T> GetCartesian() const;
		// other methods
		/// add "Theta", "Phi" and "Rho" to current position
		inline void Add(const T Theta, const T Phi, const T Rho);
		/// add "Theta" to current Theta-value
		inline void AddTheta(const T Theta);
		/// add "Phi" to current Phi-value
		inline void AddPhi(const T Phi);
		/// add "Rho" to current Rho-value
		inline void AddRho(const T Rho);
		/// sub "Theta", "Phi" and "z" from current position
		inline void Sub(const T Theta, const T Phi, const T Rho);
		/// sub "Theta" from current Theta-value
		inline void SubTheta(const T Theta);
		/// sub "Phi" from current Phi-value
		inline void SubPhi(const T Phi);
		/// sub "Rho" from current Rho-value
		inline void SubRho(const T Rho);
		/// returns the Theta, Phi and Rho values in given parameters
		inline void Get(T& Theta, T& Phi, T& Rho) const;
		/// return Theta-value
		inline T GetTheta() const;
		/// return Phi-value
		inline T GetPhi() const;
		/// return Rho-value
		inline T GetRho() const;
		/// set Theta-, Phi- and Rho-value
		inline void Set(const T Theta, const T Phi, const T Rho);
		/// set Theta-value
		inline void SetTheta(const T Theta);
		/// set Phi-value
		inline void SetPhi(const T Phi);
		/// set Rho-value
		inline void SetRho(const T Rho);
		/** return Euclidian distance (length) between two points.
			@param P3D The point to calculate distance to, defaults to (0,0,0).
			@return Euclidian distance between this object and the given 3D point. */
		inline double GetDist(const CPolar3D<T>& P3D=CPolar3D<T>(0,0,0)) const;

		/** Converts cartesian point (x,y,z) to polar point (Theta,Phi,Rho).
			The polar coordinates are defined as:
			\verbatim

			x = -Rho*sin(Phi)
			y =  Rho*cos(Phi)*sin(Theta)
			z = -Rho*cos(Phi)*cos(Theta)
			\endverbatim
			@param x Input of cartesian x-value
			@param y Input of cartesian y-value
			@param z Input of cartesian z-value
			@param Theta Returned polar theta-value, in radians.
			@param Phi Returned polar phi-value, in radians.
			@param Rho Returned polar rho-value, in radians.
			@return True, always. */
		static inline bool CartesianToPolar(T x, T y, T z, 
											T& Theta, T& Phi, T& Rho);
		/** Same as CartesianToPolar(x,y,z,Theta,Phi,Rho), except the polar point
			is now given as a CPolar3D object. */
		static inline bool CartesianToPolar(T x, T y, T z, CPolar3D<T>& Polar);
		
		/** Converts polar point (Theta,Phi,Rho) to cartesian point (x,y,z).
			For definition of polar points, see CartesianToPolar().
			@param Theta Input of polar theta-value, in radians.
			@param Phi Input of polar phi-value, in radians.
			@param Rho Input of polar rho-value, in radians.
			@param x Returned cartesian x-value
			@param y Returned cartesian y-value
			@param z Returned cartesian z-value
			@return True, always. */
		static inline bool PolarToCartesian(T Theta, T Phi, T Rho,
											T& x, T& y, T& z);
		/** Same as PolarToCartesian(Theta,Phi,Rho,x,y,z), except the cartesian 
			point is now given as a CPoint3D object. */
		static inline bool PolarToCartesian(T Theta, T Phi, T Rho, CPoint3D<T>& P);

		// info methods
		/// prints value of point to stdout
		void Print() const;
		/**@name streaming operators */
		//@{
		/** writes point to stream as '(Theta,Phi,Rho)' */
		friend std::ostream& operator<<(std::ostream& s,const CPolar3D<T>& P3D){return s << "(" << P3D.m_x << "," << P3D.m_y << "," << P3D.m_z << ")";}

		/** Reads input from given stream into this class. Stream must be in correct format
			according to how the << ostream operator formats output. Both paranteses are removed
			from stream.
			Note: Beacuause of a bug in the Borland C++ 4.0 compiler, the definition of this
			method is placed in the header file, otherwise the Borland compiler can't link.
			@version 0.2 */
		friend std::istream& operator>>(std::istream& is, CPolar3D<T>& P3D)
		{
			SkipSpaceAndComments(is);
			is.get(); // eat '('
			SkipSpaceAndComments(is);
			is >> P3D.m_x;
			SkipSpaceAndComments(is);
			is.get(); // eat ','
			SkipSpaceAndComments(is);
			is >> P3D.m_y;
			SkipSpaceAndComments(is);
			is.get(); // eat ','
			SkipSpaceAndComments(is);
			is >> P3D.m_z;
			SkipSpaceAndComments(is);
			is.get(); // eat ')'
			return is;
		}
		//@}
	protected: // methods
		/** used for loading. Skips all whitespace, newline, tab characters and
			comment lines, i.e. rest of line from a '#' character. */
		static inline void SkipSpaceAndComments(std::istream& s)
		{
			while(isspace(s.peek()))
				s.get();
			while (s.peek()=='#')
			{
				s.ignore(std::numeric_limits<int>::max(),'\n');
				while (isspace(s.peek()))
					s.get();
			}
		}
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

// other methods
template <class T>
inline void CPolar3D<T>::Add(const T Theta, const T Phi, const T Rho)
{
	m_x +=Theta;
	m_y +=Phi;
	m_z +=Rho;
}

template <class T>
inline void CPolar3D<T>::AddTheta(const T Theta)
{
	// add "Theta" to current Theta-value
	m_x += Theta;
}

template <class T>
inline void CPolar3D<T>::AddPhi(const T Phi)
{
	// add "Phi" to current Phi-value
	m_y += Phi;
}

template <class T>
inline void CPolar3D<T>::AddRho(const T Rho)
{
	// add "Rho" to current Rho-value
	m_z += Rho;
}

template <class T>
inline void CPolar3D<T>::Sub(const T Theta, const T Phi, const T Rho)
{
	m_x -= Theta;
	m_y -= Phi;
	m_z -= Rho;
}

template <class T>
inline void CPolar3D<T>::SubTheta(const T Theta)
{
	// sub "Theta" from current Theta-value
	m_x -= Theta;
}

template <class T>
inline void CPolar3D<T>::SubPhi(const T Phi)
{
	// sub "Phi" from current Phi-value
	m_y -= Phi;
}

template <class T>
inline void CPolar3D<T>::SubRho(const T Rho)
{
	// sub "Rho" from current Rho-value
	m_z -= Rho;
}

template <class T>
inline void CPolar3D<T>::Get(T& Theta, T& Phi, T& Rho) const
{
	Theta=m_x;
	Phi=m_y;
	Rho=m_z;
}

template <class T>
inline T CPolar3D<T>::GetTheta() const
{
	// return Theta-value
	return m_x;
}

template <class T>
inline T CPolar3D<T>::GetPhi() const
{
	// return Phi-value
	return m_y;
}

template <class T>
inline T CPolar3D<T>::GetRho() const
{
	// return Rho-value
	return m_z;
}

template <class T>
inline void CPolar3D<T>::Set(const T Theta, const T Phi, const T Rho)
{
	m_x=Theta;
	m_y=Phi;
	m_z=Rho;
}

template <class T>
inline void CPolar3D<T>::SetTheta(const T Theta)
{
	// set Theta-value
	m_x=Theta;
}

template <class T>
inline void CPolar3D<T>::SetPhi(const T Phi)
{
	// set Phi-value
	m_y=Phi;
}

template <class T>
inline void CPolar3D<T>::SetRho(const T Rho)
{
	// set Rho-value
	m_z=Rho;
}

template <class T>
inline double CPolar3D<T>::GetDist(const CPolar3D<T>& P3D) const
{
	// return distance between two points
	CPoint3D<T> P1(GetCartesian());
	CPoint3D<T> P2(P3D.GetCartesian());
	return P1.GetDist(P2);
}

template <class T>
void CPolar3D<T>::Print() const
{
	// prints value of point to stdout
	std::cout << "polar point=(Theta=" << m_x << ", Phi=" << m_y << ", Rho=" 
		      << m_z << ")" << std::endl;
}

template <class T>
inline CPoint3D<T> CPolar3D<T>::GetCartesian() const
{
	CPoint3D<T> P;
	PolarToCartesian(GetTheta(),GetPhi(),GetRho(),P);
	return P;
//	return CPoint3D<T>(-GetRho()*sin(GetPhi()),
//					   GetRho()*cos(GetPhi())*sin(GetTheta()),
//					   -GetRho()*cos(GetPhi())*cos(GetTheta()));
}

template<class T>
inline bool CPolar3D<T>::CartesianToPolar(T x, T y, T z, 
							 T& Theta, T& Phi, T& Rho)
{
	Theta=atan2(-y,z);
	Rho=sqrt(x*x+y*y+z*z);
	Phi=asin(-x/Rho);
	return true;
}

template<class T>
inline bool CPolar3D<T>::CartesianToPolar(T x, T y, T z, 
									CPolar3D<T>& Polar)
{
	Polar.SetTheta(atan2(-y,z));
	Polar.SetRho(sqrt(x*x+y*y+z*z));
	Polar.SetPhi(asin(-x/Polar.GetRho()));
	return true;
}

template<class T>
inline bool CPolar3D<T>::PolarToCartesian(T Theta, T Phi, T Rho,
									T& x, T& y, T& z)
{
	x=-Rho*sin(Phi);
	y=Rho*cos(Phi)*sin(Theta);
	z=-Rho*cos(Phi)*cos(Theta);
	return true;
}

template<class T>
inline bool CPolar3D<T>::PolarToCartesian(T Theta, T Phi, T Rho,
									CPoint3D<T>& P)
{
	P.SetX(-Rho*sin(Phi));
	P.SetY(Rho*cos(Phi)*sin(Theta));
	P.SetZ(-Rho*cos(Phi)*cos(Theta));
	return true;
}

} // end namespace videndo

#endif //_SCAPE_POLAR3D_H
