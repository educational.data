#include "features.h"
#include <ipl98/cpp/image.h>

namespace videndo{ // use namespace if C++

using std::cout;
using std::endl;
using ipl::CImage;

const unsigned int CFeatures::m_IndexError=9999;
const double CFeatures::m_ZeroAdjust=10e-4;

CFeatures::CFeatures()
{
	m_Tolerance=0.2;
}

CFeatures::~CFeatures()
{
}

void CFeatures::Empty()
{
	m_Id.clear();
	m_UseEta21.clear();
	m_SignPositive.clear();
	m_PrimaryAxisAngle.clear();
}

bool CFeatures::SetPrimaryAxisExistTolerance(double Tolerance)
{
	if (Tolerance<0)
	{
		ostringstream ost;
		ost << "CFeatures::SetPrimaryAxisExistTolerance() Tolerance=" << Tolerance << " is invalid" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_Tolerance=Tolerance;
	return true;
}

bool CFeatures::AddRefObject(unsigned int Id, const TMoment& CentralMoments)
{
	if (IdExists(Id)==true)
	{
		ostringstream ost;
		ost << "CFeatures::AddRefObject() Id=" << Id << " already exists" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	double Phi0,Phi1;
	const double& My11=CentralMoments.m11;
	const double& My20=CentralMoments.m20;
	const double& My02=CentralMoments.m02;
	if (PrimaryAxisExists(CentralMoments,m_Tolerance)==false)
	{
		ostringstream ost;
		ost << "CFeatures::AddRefObject() No primary axis exists" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_Id.push_back(Id);
	Phi0=GetPrimaryPhi(My11,My20,My02);
	Phi1=Phi0+ipl::PI/2.0;
	double Eta02, Eta20, Eta21, Eta12;
	double PhiNew;
	// remove two solutions by requiring Eta02-Eta20 > 0
	GetEtaValues(Eta02,Eta20,Eta21,Eta12, CentralMoments, Phi0);
	if (Eta20-Eta02>0)
		PhiNew=Phi0;
	else
		PhiNew=Phi1;

	// Choose Max(|Eta21|,|Eta12|) and remember sign
	GetEtaValues(Eta02,Eta20,Eta21,Eta12, CentralMoments, PhiNew);
	if (fabs(Eta21)>fabs(Eta12))
	{
		m_UseEta21.push_back(true);
		Eta21<0 ? m_SignPositive.push_back(false) : m_SignPositive.push_back(true);
	}
	else
	{
		m_UseEta21.push_back(false);
		Eta12<0 ? m_SignPositive.push_back(false) : m_SignPositive.push_back(true);
	}
	if (PhiNew<0)
		m_PrimaryAxisAngle.push_back(PhiNew+2*ipl::PI);
	else
		m_PrimaryAxisAngle.push_back(PhiNew);
	return true;
}

bool CFeatures::AddRefObject(unsigned int Id, CPixelGroup& PixelGroup)
{
	ipl::CFeatures Features;
	if (GetMoments(PixelGroup,Features)==false)
		return 0;
	return AddRefObject(Id,Features.m_CentrMom);
}

bool CFeatures::AddRefObject(unsigned int Id, bool UseEta21, bool SignPositive, double PrimaryAxisAngle)
{
	if (IdExists(Id)==true)
	{
		ostringstream ost;
		ost << "CFeatures::AddRefObject() Id=" << Id << " already exists" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_Id.push_back(Id);
	m_UseEta21.push_back(UseEta21);
	m_SignPositive.push_back(SignPositive);
	m_PrimaryAxisAngle.push_back(PrimaryAxisAngle);
	return true;
}

bool CFeatures::GetRefObject(unsigned int Id, bool& UseEta21, bool& SignPositive, double& PrimaryAxisAngle)
{
	unsigned int Index=GetIndex(Id);
	if (Index==m_IndexError)
	{
		ostringstream ost;
		ost << "CFeatures::GetRefObject() Id=" << Id << " not found" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	UseEta21=m_UseEta21[Index];
	SignPositive=m_SignPositive[Index];
	PrimaryAxisAngle=m_PrimaryAxisAngle[Index];
	return true;
}

double CFeatures::GetPrincipalAngle(unsigned int Id, const TMoment& CentralMoments)
{
	unsigned int Index=GetIndex(Id);
	if (Index==m_IndexError)
	{
		ostringstream ost;
		ost << "CFeatures::GetPrincipalAngle() Id=" << Id << " not found" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	double Phi0,Phi1;
	const double& My11=CentralMoments.m11;
	const double& My20=CentralMoments.m20;
	const double& My02=CentralMoments.m02;
	if (PrimaryAxisExists(CentralMoments,m_Tolerance)==false)
	{
		ostringstream ost;
		ost << "CFeatures::GetPrincipalAngle() No primary axis exists" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return -1;
	}
	Phi0=GetPrimaryPhi(My11,My20,My02);
	Phi1=Phi0+ipl::PI/2.0;
	double Eta02, Eta20, Eta21, Eta12;
	double PhiNew;
	// remove two solutions by requiring Eta02-Eta20 > 0
	GetEtaValues(Eta02,Eta20,Eta21,Eta12, CentralMoments, Phi0);
	if (Eta20-Eta02>0)
		PhiNew=Phi0;
	else
		PhiNew=Phi1;

	// remove last solution by using reference objects Eta21/Eta12 and add PI if sign is different
	GetEtaValues(Eta02,Eta20,Eta21,Eta12, CentralMoments, PhiNew);
	if (m_UseEta21[Index]==true)
	{
		if ((Eta21>0) != m_SignPositive[Index])
			PhiNew += ipl::PI;
	}
	else
	{
		if ((Eta12>0) != m_SignPositive[Index])
			PhiNew += ipl::PI;
	}
	if (PhiNew<0)
		PhiNew+=2*ipl::PI;
	return PhiNew;
}

double CFeatures::GetPrincipalAngle(unsigned int Id, const CPixelGroup& PixelGroup)
{
	ipl::CFeatures Features;
	if (GetMoments(PixelGroup,Features)==false)
		return 0;
	return GetPrincipalAngle(Id,Features.m_CentrMom);
}

double CFeatures::GetPrincipalAngleDiff(unsigned int Id, const TMoment& CentralMoments)
{
	unsigned int Index=GetIndex(Id);
	if (Index==m_IndexError)
	{
		ostringstream ost;
		ost << "CFeatures::GetPrincipalAngleDiff() Id=" << Id << " not found" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	double AbsoluteAngle=GetPrincipalAngle(Id,CentralMoments);
	double RelativeAngleAbs=fabs(m_PrimaryAxisAngle[Index]-AbsoluteAngle);
	double Result=RelativeAngleAbs;
	if (m_PrimaryAxisAngle[Index] > AbsoluteAngle)
		Result=2*ipl::PI-RelativeAngleAbs;
	return Result;
}

double CFeatures::GetPrincipalAngleDiff(unsigned int Id, const CPixelGroup& PixelGroup)
{
	ipl::CFeatures Features;
	if (GetMoments(PixelGroup,Features)==false)
		return 0;
	return GetPrincipalAngleDiff(Id,Features.m_CentrMom);
}

double CFeatures::GetPrincipalOrientation(const TMoment& CentralMoments, double Tolerance)
{
	double Phi0,Phi1;
	const double& My11=CentralMoments.m11;
	const double& My20=CentralMoments.m20;
	const double& My02=CentralMoments.m02;
	if (PrimaryAxisExists(CentralMoments,Tolerance)==false)
	{
		ostringstream ost;
		ost << "CFeatures::GetPrincipalOrientation() No primary axis exists" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return -1;
	}
	Phi0=GetPrimaryPhi(My11,My20,My02);
	Phi1=Phi0+ipl::PI/2.0;
	double Eta02, Eta20, Eta21, Eta12;
	double PhiNew;
	// remove two solutions by requiring Eta02-Eta20 > 0
	GetEtaValues(Eta02,Eta20,Eta21,Eta12, CentralMoments, Phi0);
	if (Eta20-Eta02>0)
		PhiNew=Phi0;
	else
		PhiNew=Phi1;
	if (PhiNew<0)
		PhiNew += ipl::PI;
	// PhiNew is the secondary axis - convert it to primary in range [0;Pi[
	PhiNew-=ipl::PI/2.0;
	if (PhiNew<0)
		PhiNew+=ipl::PI;
	return PhiNew;
}

double CFeatures::GetPrincipalOrientation(const CPixelGroup& PixelGroup, double Tolerance)
{
	ipl::CFeatures Features;
	if (GetMoments(PixelGroup,Features)==false)
		return 0;
	return GetPrincipalOrientation(Features.m_CentrMom,Tolerance);
}

bool CFeatures::GetMoments(const CPixelGroup& PixelGroup, ipl::CFeatures& Features)
{
	if (PixelGroup.GetTotalPositions()==0)
	{
		ostringstream ost;
		ost << "CFeatures::GetMoments() Pixelgroup is empty" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	Features.DeriveMomentsBinary(PixelGroup,3);
	Features.DeriveCentralMoments();
	return true;
}

unsigned int CFeatures::GetIndex(unsigned int Id)
{
	for(int x=0; x<m_Id.size(); x++)
	{
		if (m_Id[x]==Id)
			return x;
	}
	// if we end up here, the Id did not exist
	return m_IndexError;
}

bool CFeatures::IdExists(unsigned int Id)
{
	for(int x=0; x<m_Id.size(); x++)
	{
		if (m_Id[x]==Id)
			return true;
	}
	// if we end up here, the Id was not found
	return false;
}

bool CFeatures::PrimaryAxisExists(const TMoment& CentralMoments, double Tolerance)
{
	const double& My11=CentralMoments.m11;
	const double& My20=CentralMoments.m20;
	const double& My02=CentralMoments.m02;

	double d=sqrt(My20*My20+My02*My02 - 2*My20*My02 + 4*My11*My11);

	double Lambda1=(My20 + My02 + d)/2.0;
	double Lambda2=(My20 + My02 - d)/2.0;
	if (fabs(Lambda1)<fabs(Lambda2))
		ipl::Swap(Lambda1,Lambda2);

	// to be removed
	double Check1=Lambda1*Lambda1-(My20+My02)*Lambda1+My20*My02-My11*My11;
	double Check2=Lambda2*Lambda2-(My20+My02)*Lambda2+My20*My02-My11*My11;

	double Frac=(fabs(Lambda1)/fabs(Lambda2));
	if (Frac>1+Tolerance)
		return true;
	else
		return false;

}

//bool CFeatures::PrimaryAxisExists(const double& My11,const double& My20,const double& My02)
//{
//	double Diff=My02-My20;
//	if ((fabs(Diff)<m_ZeroAdjust) && (fabs(My11)<m_ZeroAdjust))
//	{
//		return false;
//	}
//	return true;
//}

CPoint2D<double> CFeatures::GetInterpolatedCenterOfMass(const CPixelGroup& Grp, const CStdImage& Img, UINT8 Background)
{
	if (Img.GetBits()!=8)
	{
		ostringstream ost;
		ost << "CFeatures::GetInterpolatedCenterOfMass() Input image must be 8 b/p" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return CPoint2D<double>(-1,-1);
	}
	const unsigned int MIN_BORDER_DIST=5;
	if (((Grp.GetLeft().GetX()-MIN_BORDER_DIST)<Img.GetMinX()) || ((Grp.GetRight().GetX()+MIN_BORDER_DIST)>=Img.GetMaxX()) ||
		((Grp.GetTop().GetY()-MIN_BORDER_DIST)<Img.GetMinY()) || ((Grp.GetBottom().GetY()+MIN_BORDER_DIST)>=Img.GetMaxY()))
	{
		ostringstream ost;
		ost << "CFeatures::GetInterpolatedCenterOfMass() Pixel group to close to border (min: " 
			<< MIN_BORDER_DIST << " pixels)" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return CPoint2D<double>(-1,-1);
	}
	ipl::CFeatures Features;
	CPixelGroup GrpTemp(ipl::Round(1.5*Grp.GetTotalPositions()),true);
	CImage BinaryBlobImg;
	Grp.CopyToImage(1,0,1,BinaryBlobImg);
//	Grp.CopyToImage(NULL,1,BinaryBlobImg);
//	BinaryBlobImg.Save("d:/temp/test2.bmp");

	int i,j,k;
	// find mean X
	for(j=BinaryBlobImg.GetMinY(); j<BinaryBlobImg.GetMaxY(); j++)
	{
		i=BinaryBlobImg.GetMinX();
		while (i<BinaryBlobImg.GetMaxX())
		{
			while((i<BinaryBlobImg.GetMaxX()) && (BinaryBlobImg.GetPixelFast(i,j)!=0))
			{
				i++;
			}
			if (i==BinaryBlobImg.GetMaxX())
				break; // we reach the end without finding more pixels

			// we are now at the first black position (left edge of blob)
			int XPosL;
			float fXPosL=GetXMaxGradInterpolated(i,j,Img,XPosL);
			float GrayToneL=(XPosL+1-fXPosL)*Img.GetPixelFast(XPosL,j);
			GrpTemp.AddPositionColorFast(XPosL,j,GrayToneL);

			while((i<BinaryBlobImg.GetMaxX()) && (BinaryBlobImg.GetPixelFast(i,j)==0))
			{
				i++;
			}
			// we are now at the first white position (right edge of blob)
			int XPosR;
			float fXPosR=GetXMaxGradInterpolated(i,j,Img,XPosR);
			float GrayToneR=(fXPosR-XPosR)*Img.GetPixelFast(XPosR,j);
			GrpTemp.AddPositionColorFast(XPosR,j,GrayToneR);
		
			for(k=XPosL+1; k<XPosR; k++)
				GrpTemp.AddPositionColorFast(k,j,Img.GetPixelFast(k,j));
		}

	}
	//GrpTemp.PrintInfo(true);
	Features.DeriveMomentsGrayTone(GrpTemp,Background,2);
	CPoint2D<double> CM;
	CM.SetX(Features.m_Moments.m10/Features.m_Moments.m00);

//	CImage Img2;
//	GrpTemp.UpdateBoundaries();
//	GrpTemp.CopyToImage(1,0,1,Img2);
//	Img2.Save("d:/temp/test3.bmp");

	GrpTemp.EmptyNoDeallocation();
//	GrpTemp.AllocColors();
	// find mean Y
	for(j=BinaryBlobImg.GetMinX(); j<BinaryBlobImg.GetMaxX(); j++)
	{
		i=BinaryBlobImg.GetMinY();
		while (i<BinaryBlobImg.GetMaxY())
		{
			while((i<BinaryBlobImg.GetMaxY()) && (BinaryBlobImg.GetPixelFast(j,i)!=0))
			{
				i++;
			}
			if (i==BinaryBlobImg.GetMaxY())
				break; // we reach the end without finding more pixels

			// we are now at the first black position (top edge of blob)
			int YPosT;
			float fYPosT=GetYMaxGradInterpolated(j,i,Img,YPosT);
			float GrayToneT=(YPosT+1-fYPosT)*Img.GetPixelFast(j,YPosT);
			GrpTemp.AddPositionColorFast(j,YPosT,GrayToneT);

			while((i<BinaryBlobImg.GetMaxY()) && (BinaryBlobImg.GetPixelFast(j,i)==0))
			{
				i++;
			}
			// we are now at the first white position (bottom edge of blob)
			int YPosB;
			float fYPosB=GetYMaxGradInterpolated(j,i,Img,YPosB);
			float GrayToneB=(fYPosB-YPosB)*Img.GetPixelFast(j,YPosB);
			GrpTemp.AddPositionColorFast(j,YPosB,GrayToneB);
		
			for(k=YPosT+1; k<YPosB; k++)
				GrpTemp.AddPositionColorFast(j,k,Img.GetPixelFast(j,k));
		}
	}
//	GrpTemp.UpdateBoundaries();
//	GrpTemp.CopyToImage(1,0,1,Img2);
//	Img2.Save("d:/temp/test4.bmp");
//	GrpTemp.PrintInfo(true);
	Features.DeriveMomentsGrayTone(GrpTemp,Background,2);
	CM.SetY(Features.m_Moments.m01/Features.m_Moments.m00);
	
	return CM;
}

} // end namespace videndo
