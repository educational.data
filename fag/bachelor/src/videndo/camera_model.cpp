#include "camera_model.h"
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/error.h>
#include <ipl98/cpp/palette.h>
#include <fstream>
#include <iomanip>
#include <ios>

namespace videndo{

using std::ios;
using std::ios_base;
using std::scientific;
using std::setprecision;
using std::setiosflags;
using std::setw;
using std::ofstream;
using std::ostringstream;
using std::cout;
using std::endl;
using std::string;
using ipl::CError;
using ipl::IPL_ERROR;
using ipl::IPL_WARNING;
using ipl::CPalette;
using ipl::Round;
using ipl::TString;

CCameraModel::CCameraModel(const CStdImage& Img, double FocalLengthIdeal) : CPerspective(Img)
{
	m_FocalLengthIdeal=FocalLengthIdeal;
    m_RollAdjustment=false;
}

CCameraModel::CCameraModel(unsigned int Width, unsigned int Height, double FocalLengthIdeal) : CPerspective(Width,Height)
{
	m_FocalLengthIdeal=FocalLengthIdeal;
    m_RollAdjustment=false;
}

CCameraModel::CCameraModel(CPoint2D<unsigned int> Dimensions, double FocalLengthIdeal) : CPerspective(Dimensions)
{
	m_FocalLengthIdeal=FocalLengthIdeal;
    m_RollAdjustment=false;
}

CCameraModel::~CCameraModel()
{
}

CCameraModel& CCameraModel::operator=(const CCameraModel& Source)
{
	// overloading of assignment operator
	if (this != &Source)
	{
		// ok to do assignment
		m_CamParametersAvailable=Source.m_CamParametersAvailable;
		if (m_CamParametersAvailable==true)
			m_Par=Source.m_Par; // only need to copy Par structure if data available
		m_Cam=Source.m_Cam;
		m_CamMatrixCalibrated=Source.m_CamMatrixCalibrated;
		m_RadCalibrationPerformed=Source.m_RadCalibrationPerformed;
		m_k=Source.m_k;
		m_CenterX=Source.m_CenterX;
		m_CenterY=Source.m_CenterY;
		m_FocalLengthIdeal=Source.m_FocalLengthIdeal;
	}
	else
	{
		ostringstream ost;
		ost << "CCameraModel::operator=() Cannot assign CCameraModel object to itself" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
	}
	return *this;
}

bool CCameraModel::Set(const TCameraMatrix& Cam, double k, unsigned int Width, unsigned Height, 
		double FocalLengthIdeal)
{
	m_Cam=Cam;
	m_k=k;
	m_CenterX=Width/2.0;
	m_CenterY=Height/2.0;
	m_FocalLengthIdeal=FocalLengthIdeal;
	m_CamMatrixCalibrated=true;
	m_RadCalibrationPerformed=true;
	//m_CamParametersAvailable=false;
	MatrixToParameters();
	CalcRealIdealConstants();
	return true;
}

bool CCameraModel::Set(const CPoint3D<FLOAT32>& Pinhole, FLOAT32 a, FLOAT32 b, FLOAT32 c, FLOAT32 FocalLength, 
			const CPoint2D<FLOAT32>& FocalPoint, FLOAT32 p, FLOAT32 Beta, double k, 
			unsigned int Width, unsigned Height, double FocalLengthIdeal)
{
	if (k<0)
	{
		ostringstream ost;
		ost << "CCameraModel::Set() k<0 is illegal" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_Par.dx=Pinhole.GetX();
	m_Par.dy=Pinhole.GetY();
	m_Par.dz=Pinhole.GetZ();
	m_Par.a=a;
	m_Par.b=b;
	m_Par.c=c;
	m_Par.FocalLength=FocalLength;
	m_Par.f=1/FocalLength;
	m_Par.xh=FocalPoint.GetX();
	m_Par.yh=FocalPoint.GetY();
	m_Par.p=p;
	m_Par.beta=Beta;
	m_k=k;
	m_CenterX=Width/2.0;
	m_CenterY=Height/2.0;
	m_FocalLengthIdeal=FocalLengthIdeal;
	//m_CamMatrixCalibrated=true;
	m_RadCalibrationPerformed=true;
	m_CamParametersAvailable=true;
	ParametersToMatrix();
	CalcRealIdealConstants();
	return true;
}

bool CCameraModel::Set(const CPoint3D<FLOAT32>& Pinhole, FLOAT32 a, FLOAT32 b, FLOAT32 c, FLOAT32 FocalLength, 
			const CPoint2D<FLOAT32>& FocalPoint, FLOAT32 p, FLOAT32 Beta, double k)
{
	return Set(Pinhole,a,b,c,FocalLength, FocalPoint, p, Beta, k, m_CenterX*2, m_CenterY*2,m_FocalLengthIdeal);
}

//CPoint2D<int> CCameraModel::GetMaxIdealDisplacement(const CStdImage& Source) const
//{
//	if (m_RadCalibrationPerformed!=true)
//	{
//		ostringstream ost;
//		ost << "CCameraModel::GetMaxIdealDisplacement() Calibration not performed yet, call CalibrateWithRadialDist() first" << IPLAddFileAndLine;
//		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
//		return CPoint2D<int>(-1,-1);;
//	}
//	CPoint2D<FLOAT32> UL(GetPosIdeal(Source.GetMinX(),Source.GetMinY()));
//	CPoint2D<FLOAT32> UR(GetPosIdeal(Source.GetMaxX(),Source.GetMinY()));
//	CPoint2D<FLOAT32> LL(GetPosIdeal(Source.GetMinX(),Source.GetMaxY()));
//	CPoint2D<FLOAT32> LR(GetPosIdeal(Source.GetMaxX(),Source.GetMaxY()));
//	double MinX,MaxX,MinY,MaxY;
//	if (UL.GetX()<LL.GetX())
//		MinX=UL.GetX();
//	else
//		MinX=LL.GetX();
//	if (UR.GetX()>LR.GetX())
//		MaxX=UR.GetX();
//	else
//		MaxX=LR.GetX();
//	if (UL.GetY()<UR.GetY())
//		MinY=UL.GetY();
//	else
//		MinY=UR.GetY();
//	if (LL.GetY()>LR.GetY())
//		MaxY=LL.GetY();
//	else
//		MaxY=LR.GetY();
//	unsigned MaxDispX,MaxDispY;
//	CPoint2D<int> Origo(Source.GetOrigo());
//	if ((abs(MinX-Source.GetMinX())) > (abs(MaxX-Source.GetMaxX())))
//		MaxDispX=abs(MinX-Source.GetMinX());
//	else
//		MaxDispX=abs(MaxX-Source.GetMaxX());
//	if ((abs(MinY-Source.GetMinY())) > (abs(MaxY-Source.GetMaxY())))
//		MaxDispY=abs(MinY-Source.GetMinY());
//	else
//		MaxDispY=abs(MaxY-Source.GetMaxY());
//
//	return CPoint2D<int>(MaxDispX,MaxDispY);
//}

bool CCameraModel::AllocIdealImage(CImage& Dest, ipl::UINT16 Bits, ipl::UINT32 Color, const CStdImage& Source) const
{
	if (m_RadCalibrationPerformed!=true)
	{
		ostringstream ost;
		ost << "CCameraModel::AllocIdealImage() Calibration not performed yet, call CalibrateWithRadialDist() first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	CPoint2D<FLOAT32> UL(GetPosIdeal(Source.GetMinX(),Source.GetMinY()));
	CPoint2D<FLOAT32> UR(GetPosIdeal(Source.GetMaxX(),Source.GetMinY()));
	CPoint2D<FLOAT32> LL(GetPosIdeal(Source.GetMinX(),Source.GetMaxY()));
	CPoint2D<FLOAT32> LR(GetPosIdeal(Source.GetMaxX(),Source.GetMaxY()));
	double MinX,MaxX,MinY,MaxY;
	if (UL.GetX()<LL.GetX())
		MinX=UL.GetX();
	else
		MinX=LL.GetX();
	if (UR.GetX()>LR.GetX())
		MaxX=UR.GetX();
	else
		MaxX=LR.GetX();
	if (UL.GetY()<UR.GetY())
		MinY=UL.GetY();
	else
		MinY=UR.GetY();
	if (LL.GetY()>LR.GetY())
		MaxY=LL.GetY();
	else
		MaxY=LR.GetY();
	Dest.Alloc(MaxX-MinX,MaxY-MinY,Bits,Color);
	CPoint2D<int> NewOrigo(Round(-MinX),Round(-MinY));
	Dest.SetOrigo(NewOrigo);
	return true;
}

bool CCameraModel::CreateIdealImage(CImage& Dest, CStdImage& Source, bool PreserveImageSize, double f)
{
	if (m_RadCalibrationPerformed!=true)
	{
		ostringstream ost;
		ost << "CCameraModel::CreateIdealImage() Calibration not performed yet, call CalibrateWithRadialDist() first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	else if ((Source.GetBits()!=8) && (Source.GetBits()!=24))
	{
		ostringstream ost;
		ost << "CCameraModel::CreateIdealImage() Image must be 8 or 24 b/p (current is " << Source.GetBits() << 
			" b/p)" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	else
	{
		if (m_CamParametersAvailable==false)
		{
			MatrixToParameters();
			ostringstream ost;
			ost << "CCameraModel::CreateIdealImage() Parameters not calculated - performing calculation..." << IPLAddFileAndLine;
			CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		}
		double OriginalFLIdeal;
		if (f!=0)
		{
			// temporarely set m_FocalLengthIdeal=f, remember to set it back before leaving method
			OriginalFLIdeal=m_FocalLengthIdeal;
			m_FocalLengthIdeal=f;
			// also need to recalculate constants with new ideal f
			CalcRealIdealConstants();
		}

		CPoint2D<FLOAT32> Center(Source.GetWidth()/2.0,Source.GetHeight()/2.0);
		if (PreserveImageSize==true)
		{
			if (Source.GetBits()==8)
				Dest.Alloc(Source.GetWidth(),Source.GetHeight(),8,255);
			else // must be 24 b/p
				Dest.Alloc(Source.GetWidth(),Source.GetHeight(),24,CPalette::CreateRGB(255,255,255));
			//Dest.Alloc(Source.GetWidth(),Source.GetHeight(),8);
			Dest.SetOrigo(m_Par.xh,m_Par.yh);
			for(int y=Dest.GetMinY(); y<Dest.GetMaxY(); y++)
			{
				for(int x=Dest.GetMinX(); x<Dest.GetMaxX(); x++)
				{
					CPoint2D<FLOAT32> PosReal(GetPosReal(x,y));
					if ((PosReal.GetX()>Source.GetMinX()) && (PosReal.GetX()<Source.GetMaxX()-1) &&
						(PosReal.GetY()>Source.GetMinY()) && (PosReal.GetY()<Source.GetMaxY()-1))
					{
						double Val=Source.GetPixelInterpolated(PosReal);
						Dest.SetPixel(x,y,Val);
					}
				}
			}
		}
		else // scale image to whatever needed
		{
			CPoint2D<FLOAT32> UL(GetPosIdeal(Source.GetMinX(),Source.GetMinY()));
			CPoint2D<FLOAT32> UR(GetPosIdeal(Source.GetMaxX(),Source.GetMinY()));
			CPoint2D<FLOAT32> LL(GetPosIdeal(Source.GetMinX(),Source.GetMaxY()));
			CPoint2D<FLOAT32> LR(GetPosIdeal(Source.GetMaxX(),Source.GetMaxY()));
			double MinX,MaxX,MinY,MaxY;
			if (UL.GetX()<LL.GetX())
				MinX=UL.GetX();
			else
				MinX=LL.GetX();
			if (UR.GetX()>LR.GetX())
				MaxX=UR.GetX();
			else
				MaxX=LR.GetX();
			if (UL.GetY()<UR.GetY())
				MinY=UL.GetY();
			else
				MinY=UR.GetY();
			if (LL.GetY()>LR.GetY())
				MaxY=LL.GetY();
			else
				MaxY=LR.GetY();
			unsigned int Width,Height;
			// Set width to twice the maximum horisontal displacement
			fabs(MaxX) > fabs(MinX) ? Width=2*fabs(MaxX) : Width=2*fabs(MinX);
			// Set height to twice the maximum vertical displacement
			fabs(MaxY) > fabs(MinY) ? Height=2*fabs(MaxY) : Height=2*fabs(MinY);
			if (Source.GetBits()==8)
				Dest.Alloc(Width,Height,8,255);
			else // must be 24 b/p
				Dest.Alloc(Width,Height,24,CPalette::CreateRGB(255,255,255));
			CPoint2D<int> NewOrigo(Round(Width/2),Round(Height/2));
//			if (Source.GetBits()==8)
//				Dest.Alloc(MaxX-MinX,MaxY-MinY,8,255);
//			else // must be 24 b/p
//				Dest.Alloc(MaxX-MinX,MaxY-MinY,8,CPalette::CreateRGB(255,255,255));
//			CPoint2D<int> NewOrigo(Round(-MinX),Round(-MinY));
			Dest.SetOrigo(NewOrigo);
			for(int y=Dest.GetMinY(); y<Dest.GetMaxY(); y++)
			{
				for(int x=Dest.GetMinX(); x<Dest.GetMaxX(); x++)
				{
					CPoint2D<FLOAT32> PosReal(GetPosReal(x,y));
					if ((PosReal.GetX()>Source.GetMinX()) && (PosReal.GetX()<Source.GetMaxX()-1) &&
						(PosReal.GetY()>Source.GetMinY()) && (PosReal.GetY()<Source.GetMaxY()-1))
					{
						ipl::UINT32 Val;
						if (Source.GetBits()==8)
							Val=Source.GetPixelInterpolated(PosReal);
						else // must be 24 b/p
							Val=Source.GetColorInterpolated(PosReal);
						Dest.SetPixel(x,y,Val);
					}
				}
			}
		}

		if (f!=0)
		{			
			m_FocalLengthIdeal=OriginalFLIdeal;
			// need to recalculate original constants with original ideal f
			CalcRealIdealConstants();
		}
		return true;
	}
}

bool CCameraModel::CreateIdealView(CImage& Dest, CStdImage& Source, unsigned int Width, unsigned int Height, 
								   ipl::UINT32 Color, double f)
{
	if (m_RadCalibrationPerformed!=true)
	{
		ostringstream ost;
		ost << "CCameraModel::CreateIdealView() Calibration not performed yet, call CalibrateWithRadialDist() first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	else if ((Source.GetBits()!=8) && (Source.GetBits()!=24))
	{
		ostringstream ost;
		ost << "CCameraModel::CreateIdealView() Image must be 8 or 24 b/p (current is " << Source.GetBits() << 
			" b/p)" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	else
	{
		if (m_CamParametersAvailable==false)
		{
			MatrixToParameters();
			ostringstream ost;
			ost << "CCameraModel::CreateIdealView() Parameters not calculated - performing calculation..." 
				<< IPLAddFileAndLine;
			CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		}
		double OriginalFLIdeal;
		if (f!=0)
		{
			// temporarely set m_FocalLengthIdeal=f, remember to set it back before leaving method
			OriginalFLIdeal=m_FocalLengthIdeal;
			m_FocalLengthIdeal=f;
			// also need to recalculate constants with new ideal f
			CalcRealIdealConstants();
		}

//		CPoint2D<int> Center(m_Par.xh,m_Par.yh);
		CPoint2D<int> Center(Round(Width/2.0),Round(Height/2.0));
		Dest.Alloc(Width,Height,Source.GetBits(),Color);
		Dest.SetOrigo(Center);
//		CPoint2D<FLOAT32> PosReal(GetPosReal(0,0));
		for(int y=Dest.GetMinY(); y<Dest.GetMaxY(); y++)
		{
			for(int x=Dest.GetMinX(); x<Dest.GetMaxX(); x++)
			{
				CPoint2D<FLOAT32> PosReal(GetPosReal(x,y));
				if ((PosReal.GetX()>Source.GetMinX()) && (PosReal.GetX()<Source.GetMaxX()-1) &&
					(PosReal.GetY()>Source.GetMinY()) && (PosReal.GetY()<Source.GetMaxY()-1))
				{
					ipl::UINT32 Val;
					if (Source.GetBits()==8)
						Val=Source.GetPixelInterpolated(PosReal);
					else // must be 24 b/p
						Val=Source.GetColorInterpolated(PosReal);
					Dest.SetPixel(x,y,Val);
				}
			}
		}
		if (f!=0)
		{			
			m_FocalLengthIdeal=OriginalFLIdeal;
			// need to recalculate original constants with original ideal f
			CalcRealIdealConstants();
		}
		return true;
	}
}

bool CCameraModel::SetFocalLengthIdeal(double FocalLengthIdeal)
{
	if (FocalLengthIdeal<=0)
	{
		ostringstream ost;
		ost << "CCameraModel::SetFocalLengthIdeal() FocalLengthIdeal<=0 not allowed" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	m_FocalLengthIdeal=FocalLengthIdeal;
	return CalcRealIdealConstants();
}

ostream& operator<<(ostream& s,const CCameraModel& Persp)
{
	ios_base::fmtflags Flags=s.flags();
	s << scientific << setprecision(6) << setiosflags(ios_base::right);
	s << "**************** TCameraMatrix info ********************" << endl;
	s << setw(15) << Persp.GetCamMatrix(1,1);
	s << setw(15) << Persp.GetCamMatrix(1,2); 
	s << setw(15) << Persp.GetCamMatrix(1,3);
	s << setw(15) << Persp.GetCamMatrix(1,4) << endl;
	s << setw(15) << Persp.GetCamMatrix(2,1);
	s << setw(15) << Persp.GetCamMatrix(2,2);
	s << setw(15) << Persp.GetCamMatrix(2,3);
	s << setw(15) << Persp.GetCamMatrix(2,4) << endl;
	s << setw(15) << Persp.GetCamMatrix(3,1);
	s << setw(15) << Persp.GetCamMatrix(3,2);
	s << setw(15) << Persp.GetCamMatrix(3,3);
	s << setw(15) << Persp.GetCamMatrix(3,4) << endl;
	s << setw(15) << Persp.GetCamMatrix(4,1);
	s << setw(15) << Persp.GetCamMatrix(4,2);
	s << setw(15) << Persp.GetCamMatrix(4,3);
	s << setw(15) << Persp.GetCamMatrix(4,4) << endl;
	s.flags(Flags);

	if (Persp.RadCalibrationPerformed()==true)
	{
		s << "  Radial distortion parameter: k=" << Persp.GetRadDistParameter() << endl;
	}
	else
	{
		s << "  No radial distortion parameter available" << endl;
	}

	Persp.CamParametersToStream(s);
	return s;
}

bool CCameraModel::MatrixToParameters()
{
	if (m_CamMatrixCalibrated==false)
	{
		ostringstream ost;
		ost << "CCameraModel::MatrixToParameters() Camera matrix not yet calculated" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	else
	{
        if (ipl::k_MatrixToParameters(&m_Cam, &m_Par)==false)
			return false;
        m_CamParametersAvailable=true;
		CalcRealIdealConstants();
		return true;
	}

}

bool CCameraModel::Load(const char* pPathAndFileName)
{
	/* booleans to check if necessary tokens were found */
	bool PerspectiveVersionFund=false;
	bool CameraMatrixFound=false;
	bool kFound=false, FLIdealFound=false, WidthFound=false, HeightFound=false;
	bool returnValue=true;
	FILE* fp=fopen(pPathAndFileName,"rb"); /* open file */
	if (fp==NULL)
	{
		ostringstream ost;
		ost << "CCameraModel::Load() Could not open file" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		returnValue=false;
	}
	else
	{
		/* read from file */
		char c;
		ipl::k_SkipWhiteSpace(fp);
		while (((c=fgetc(fp))!=EOF) && (returnValue==true))
		{
			ungetc(c,fp);
			if (c=='#')
			{
				ipl::k_SkipLine(fp);
			}
			else /* it must be a token then! */
			{
				char* Token;
				if (ipl::k_ReadToken(fp,&Token)==false)
				{
					ostringstream ost;
					ost << "CCameraModel::Load(): Failed reading token" << IPLAddFileAndLine;
					CError::ShowMessage(IPL_ERROR,ost.str().c_str());
					returnValue=false;
				}
				else
				{
					ipl::k_StringToUpper(Token);
					if (strcmp(Token,"PERSPECTIVEVERSION")==0)
					{
						PerspectiveVersionFund=true;
						double FileVersion;
						if (fscanf(fp,"%lf",&FileVersion)!=1)
						{
							ostringstream ost;
							ost << "CCameraModel::Load() Failed reading calibration version" << IPLAddFileAndLine;
							CError::ShowMessage(IPL_ERROR,ost.str().c_str());
							returnValue=false;
						}
						else if (m_Version!=FileVersion)
						{
							ostringstream ost;
							ost << "CCameraModel::Load() File version not same as this class version" << IPLAddFileAndLine;
							CError::ShowMessage(IPL_ERROR,ost.str().c_str());
						}
					}
					else if (strcmp(Token,"CAMERAMATRIX")==0)
					{
						CameraMatrixFound=true;
						/* The following 16 values must be the camera parameters */
						if (fscanf(fp,"%f %f %f %f"
									  "%f %f %f %f"
									  "%f %f %f %f"
									  "%f %f %f %f"
									  ,&m_Cam.a11,&m_Cam.a12, &m_Cam.a13, &m_Cam.a14
									  ,&m_Cam.a21,&m_Cam.a22, &m_Cam.a23, &m_Cam.a24
									  ,&m_Cam.a31,&m_Cam.a32, &m_Cam.a33, &m_Cam.a34
									  ,&m_Cam.a41,&m_Cam.a42, &m_Cam.a43, &m_Cam.a44) != 16)
						{
							ostringstream ost;
							ost << "CCameraModel::Load() Failed reading camera matrix values" << IPLAddFileAndLine;
							CError::ShowMessage(IPL_ERROR,ost.str().c_str());
							returnValue=false;
						}
					}
					else if (strcmp(Token,"K")==0)
					{
						kFound=true;
						if (fscanf(fp,"%lf",&m_k) != 1)
						{
							ostringstream ost;
							ost << "CCameraModel::Load() Failed reading k radial dist parameter" << IPLAddFileAndLine;
							CError::ShowMessage(IPL_ERROR,ost.str().c_str());
							returnValue=false;
						}
					}
					else if (strcmp(Token,"FOCALLENGTHIDEAL")==0)
					{
						FLIdealFound=true;
						if (fscanf(fp,"%lf",&m_FocalLengthIdeal) != 1)
						{
							ostringstream ost;
							ost << "CCameraModel::Load() Failed reading ideal focal length parameter" << IPLAddFileAndLine;
							CError::ShowMessage(IPL_ERROR,ost.str().c_str());
							returnValue=false;
						}
					}
					else if (strcmp(Token,"WIDTH")==0)
					{
						int CenterX;
						WidthFound=true;
						if (fscanf(fp,"%d",&CenterX) != 1)
						{
							ostringstream ost;
							ost << "CCameraModel::Load() Failed reading width parameter" << IPLAddFileAndLine;
							CError::ShowMessage(IPL_ERROR,ost.str().c_str());
							returnValue=false;
						}
						m_CenterX = (FLOAT32)CenterX/2.0; // convert from image dimension to center
					}
					else if (strcmp(Token,"HEIGHT")==0)
					{
						int CenterY;
						HeightFound=true;
						if (fscanf(fp,"%d",&CenterY) != 1)
						{
							ostringstream ost;
							ost << "CCameraModel::Load() Failed reading height parameter" << IPLAddFileAndLine;
							CError::ShowMessage(IPL_ERROR,ost.str().c_str());
							returnValue=false;
						}
						m_CenterY = (FLOAT32)CenterY/2.0; // convert from image dimension to center
					}
					free(Token);
				}
			}
			ipl::k_SkipWhiteSpace(fp);
		}
		if ((PerspectiveVersionFund==false) || (CameraMatrixFound==false) || (kFound==false))
		{
			ostringstream ost;
			ost << "CCameraModel::Load() One of the necessary tokens not found" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_ERROR,ost.str().c_str());
			returnValue=false;
		}
		else if (FLIdealFound==false)
		{
			ostringstream ost;
			ost << "CCameraModel::Load() Ideal focal length not found - setting to deafult value 1000" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		}
		if ((WidthFound==false) || (HeightFound==false))
		{
			ostringstream ost;
			ost << "CCameraModel::Load() Width and/or height not found - using image dimensions (" <<
				m_CenterX*2 << "," << m_CenterY*2 << ")" << IPLAddFileAndLine;
			CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		}
		if (returnValue==true)
		{
			m_CamMatrixCalibrated=true;
			if (kFound==true)
				m_RadCalibrationPerformed=true;
			MatrixToParameters();
		}
	}
	return returnValue;
}

bool CCameraModel::Save(const char* pPathAndFileName,const char* pComments) const
{
	if (m_CamMatrixCalibrated==false)
	{
		ostringstream ost;
		ost << "CCameraModel::Save() Need to calibrate camera first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_ERROR,ost.str().c_str());
		return false;
	}
	else
	{
		// check for extension, if no extension add ".cfg"
		string FilePathName(pPathAndFileName); // resulting filename
		ipl::TFileInfo FileInfo;
		ipl::k_InitFileInfo(&FileInfo);
		char* Path=NULL,*FileNameExt=NULL,*Ext=NULL;
		if (ipl::k_SplitFileName(pPathAndFileName,&Path, &FileNameExt, &Ext)==true)
		{
			if (strcmp(Ext,"")==0)
				FilePathName.append(".cfg");

			ofstream OutStream(FilePathName.c_str());
			if (!OutStream)
			{
				ostringstream ost;
				ost << "CCameraModel::Save() Failed to write file" << IPLAddFileAndLine;
				CError::ShowMessage(IPL_ERROR,ost.str().c_str());
				return false;
			}
			else
			{
				OutStream << "# Calibration data file written by the CCameraModel class from IPL98" << endl;
				// output comments given in the parameter "Comments"
				if (pComments!=NULL)
				{
					int Pos=0;
					string ComStr(pComments);
					while((Pos=ComStr.find_first_of("\n"))!=-1)
					{
						OutStream << "# " << ComStr.substr(0,Pos) << endl;
						ComStr=ComStr.substr(Pos+1,ComStr.size()-Pos-1);
					}
					OutStream << "# " << ComStr.c_str() << endl;
				}				
				OutStream << "#\n# CCameraModel class version" << endl;
				OutStream << "PerspectiveVersion " <<  m_Version << endl << endl;
				OutStream << "FocalLengthIdeal " << m_FocalLengthIdeal << endl << endl;
				OutStream << "# Image dimensions " << endl;
				OutStream << "Width " << m_CenterX*2 << endl << "Height " << m_CenterY*2 << endl << endl;
				OutStream << "# Camera matrix" << endl;
				OutStream << "CameraMatrix" << endl;
				OutStream << m_Cam.a11 << "   " << m_Cam.a12 << "   " << m_Cam.a13 << "   " << m_Cam.a14 << endl;
				OutStream << m_Cam.a21 << "   " << m_Cam.a22 << "   " << m_Cam.a23 << "   " << m_Cam.a24 << endl;
				OutStream << m_Cam.a31 << "   " << m_Cam.a32 << "   " << m_Cam.a33 << "   " << m_Cam.a34 << endl;
				OutStream << m_Cam.a41 << "   " << m_Cam.a42 << "   " << m_Cam.a43 << "   " << m_Cam.a44 << endl;
				if (m_RadCalibrationPerformed==true)
				{
					OutStream << "\n# k distortion parameter" << endl;
					OutStream << "k " << m_k << endl;
				}
				OutStream << endl;
				// output camera parameters (if available)
				ostringstream ParamsStr;
				CamParametersToStream(ParamsStr);
				string Str(ParamsStr.str());
				OutStream << ipl::ConvertLinesToComments(Str);
				//OutStream << Str;
			}		
		}
		ipl::k_EmptyFileInfo(&FileInfo);
		if (Path!=NULL)
			free(Path);
		if (FileNameExt!=NULL)
			free(FileNameExt);
		if (Ext!=NULL)
			free(Ext);
		return true;
	}	
}

bool CCameraModel::CalcRealIdealConstants()
{
	if (m_CamParametersAvailable!=true)
	{
		ostringstream ost;
		ost << "CCameraModel::CalcRealIdealConstants() Calibration not performet yet, "
			"call CalibrateWithRadialDist() first" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	double cosbeta=cos(m_Par.beta);
	double sinbeta=sin(m_Par.beta);
	m_C1=m_Par.FocalLength/m_FocalLengthIdeal;
	m_C2=m_C1*m_Par.p*sinbeta;
	m_C3=m_C1*m_Par.p*cosbeta;
	m_C4=m_FocalLengthIdeal/m_Par.FocalLength;
	m_C5=m_C4*m_Par.xh;
	m_C6=-(m_C4*sinbeta)/cosbeta;
	m_C7=m_C4/(m_Par.p*cosbeta);
	m_C8=(m_C4*(m_Par.p*sinbeta*m_Par.xh-m_Par.yh))/(m_Par.p*cosbeta);
	return true;
}

void CCameraModel::SetRollAdjustment(CCameraModel &RefCamera)
{
	m_RollAdjustment=true;
	MatrixToParameters();RefCamera.MatrixToParameters();
	m_cosC=cos(RefCamera.m_Par.c-m_Par.c);
	m_sinC=sin(RefCamera.m_Par.c-m_Par.c);
}

void CCameraModel::RemoveRollAdjustment()
{
	m_RollAdjustment=false;
}

} // end videndo namespace

