#ifndef _VIDENDO_TELEMETRICS_H
#define _VIDENDO_TELEMETRICS_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <ipl98/cpp/algorithms/perspective.h>
#include <ipl98/cpp/corresponding_3d2d_points.h>
#include <ipl98/cpp/error.h>
#include <vector>
#include <sstream>

using ipl::CPerspective;
using ipl::FLOAT32;
using ipl::CCorresponding3D2DPoints;
using ipl::CError;
using ipl::IPL_ERROR;
using std::vector;
using std::ostringstream;

namespace videndo{ // use namespace if C++

/** This class 
	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CTelemetrics videndo/telemetrics.h
	@version 0.12
	@author Implementation by Ivar Balslev (ivb@mip.sdu.dk) */
class CTelemetrics
{
public:
	/** Default constructor. Initialize() must be called before
		an instance of this class can be used. */
	CTelemetrics();

	/// Default destructor
	~CTelemetrics(){};

	/** Initializes this class by setting
		@param VectorOfPointSets Must contain at least 5 pointsets from 5 
			images  taken with different poses. Each set must have exactly 
			same 3D positions.
		@param FixedId1 Id of first reference mark used as a fixed point.
		@param FixedId2 Id of second reference mark used as a fixed point.
		@param FixedZId Id of reference mark, where only the height is fixed.
		@param Width Image width in pixels.
		@param Height Image height in pixels.
		@return False, if PointSets does not contain at least 5 sets or if not
			all sets have same 3D values and Id's, or if any of the fixed Id 
			values are not present in file. */
	bool Initialize(vector<CCorresponding3D2DPoints>& VectorOfPointSets, unsigned int FixedId1, 
		unsigned int FixedId2, unsigned int FixedZId, unsigned int Width, unsigned int Height);

	/** Returns true, if Initialize() has been successfully called. */
	inline bool IsInitialized() const;

	/** Main method, adjust the 3D points for the calibration plate. Initialize()
		must have been successfully called.
		@PointSets The corrected 3D points with corresponding Id's are returned 
			in this parameter. 2D points will not be in use.
		@return False, if Initialize() has not been called. */ 
	bool Adjust3DPoints(CCorresponding3D2DPoints& PointSets,bool Verbose,ostringstream& TxtOut);

	/** Only call after Initialize() has been successfully called. */ 
	float GetResidualBefore() const;//{return m_ResidualBefore;}
	/** Only call after Adjust3DPoints() has been successfully called. */ 
	float GetResidualAfter() const{return m_ResidualAfter;}
private:

	class CPerspectiveAdjust : public CPerspective
	{
	public:
		void SetCenterX(FLOAT32 CenterX){m_CenterX=CenterX;}
		FLOAT32 GetCenterX() const{return m_CenterX;}
		void SetCenterY(FLOAT32 CenterY){m_CenterY=CenterY;}
		FLOAT32 GetCenterY() const{return m_CenterY;}
		void SetRadDistortion(double k){m_k=k;}
		inline FLOAT32 GetParameter(int Index) const{return CPerspective::GetParameter(Index);}
		inline bool SetParameter(int Index, FLOAT32 Value){return CPerspective::SetParameter(Index,Value);}
	};

	/** Set to true when Initialize() is called. */
	bool m_IsInitialized;

	vector<CPerspectiveAdjust> m_Cameras;

	vector<CCorresponding3D2DPoints> m_VectorOfPointSets;

	unsigned int m_FixedId1;
	unsigned int m_FixedId2;
	unsigned int m_FixedZId;
	unsigned int m_Width;
	unsigned int m_Height;

	// Set by Adjust3DPoints
	float m_ResidualAfter;
	
    float GetGrandAverageError() const;

    float GetAverageError(const CPerspectiveAdjust& Camera, const CCorresponding3D2DPoints& PointSets) const;

	void UpdateMarkCoordinate(int MarkIndex,bool XYOnly,FLOAT32 SpatialStep);

    void Update(unsigned int FreeInternalParameterCombination,CCorresponding3D2DPoints& PointSets);

	void AdjustRadialCenter(CPerspectiveAdjust& Camera, const CCorresponding3D2DPoints& PointSets);

	float GetResidualInternal() const;

	vector<CPoint2D<FLOAT32> > GetSpatialPartialDerivative(
		const CPerspectiveAdjust& Camera,const CCorresponding3D2DPoints& PointSets,
		int MarkIndex,FLOAT32 SpatialStep) const;

	/** Sets the average internal parameters in Cameras */
    void EqualizeInternalParameters(vector<CPerspectiveAdjust>& Cameras) const;

	void GetInternalParameters(ostringstream& TxtOut);
};

/////////////////////////////////////////////////
//// Inline methods
////////////////////////////////////////////////


} // end namespace videndo

#endif //_VIDENDO_TELEMETRICS_H

