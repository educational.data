#include "local_trend_hough.h"

#include <ipl98/cpp/algorithms/mask_operation.h> 
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/error.h>
#include <sstream>

namespace videndo{ // use namespace if C++

using ipl::CMaskOperation;
using ipl::Round;
using ipl::CError;
using ipl::IPL_WARNING;
using std::ostringstream;

CLocalTrendHough::CLocalTrendHough(float AngleStepDegrees)
{
	m_NT0=2*(int)(180.0/AngleStepDegrees);
	m_NR=m_NT0;
	m_AngleStepDegrees=AngleStepDegrees;
	hs=(unsigned long int**)calloc(m_NT0+1,sizeof(unsigned long int*));
	hs1=(unsigned long int**)calloc(m_NT0+1,sizeof(unsigned long int*));
	for (int i=0;i<m_NT0+1;i++)
	{
		hs[i]=(unsigned long int*)calloc(m_NR+1,sizeof(unsigned long int));
		hs1[i]=(unsigned long int*)calloc(m_NR+1,sizeof(unsigned long int));
	}
	m_TransformationPerformed=false;
	m_GradientTransform=false;
	m_RidgeTransform=false;
	m_Empty=false;
	m_PreEdgeDetect=false;
	SetTotalPreBlur(0);
}

CLocalTrendHough::~CLocalTrendHough()
{
	for (int i=0;i<m_NT0;i++)
	{
		free(hs[i]);
		free(hs1[i]);
	}
	free(hs);
	free(hs1);
}

bool CLocalTrendHough::SetTotalPreBlur(unsigned int TotalPreBlur)
{
	if ((TotalPreBlur<1) || (TotalPreBlur>7))
	{
		ostringstream ost;
		ost << "CLocalTrendHough::SetTotalPreBlur() TotalPreBlur=" << TotalPreBlur
			<< " is out of range [1,7]" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	m_TotalPreBlur=TotalPreBlur;
	return true;
}

bool CLocalTrendHough::SetTotalPostBlur(unsigned int TotalPostBlur)
{
	if (TotalPostBlur>7)
	{
		ostringstream ost;
		ost << "CLocalTrendHough::SetTotalPostBlur() TotalPostBlur=" << TotalPostBlur
			<< " is out of range [0,7]" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	m_TotalPostBlur=TotalPostBlur;
	return true;
}

void CLocalTrendHough::EdgeDetect()
{
	unsigned int max=0;
	CMaskOperation MO;
	MO.MaxGradientFast(m_zInt,1);
	for (int x=0;x<m_zInt.GetWidth();x++)
	{
		for (int y=0;y<m_zInt.GetHeight();y++)
		{
			if (max<m_zInt.GetPixel(x,y)) max=m_zInt.GetPixel(x,y);
		}
	}
	for (int y=0;y<m_zInt.GetHeight();y++)
		for (int x=0;x<m_zInt.GetWidth();x++)
		{
			m_zInt.SetPixel(x,y,(1<<14)*m_zInt.GetPixel(x,y)/(double)max);
		}
}

bool CLocalTrendHough::StraightRidgeTransform(const CImage &Source,
											  CPoint2D<int> UL, CPoint2D<int> LR,bool PreEdgeDetect)
{
	m_NT=m_NT0/2;
	m_RidgeTransform=true;
	m_GradientTransform=false;
	m_PreEdgeDetect=PreEdgeDetect;
	return StraightTransform(Source,UL,LR);
}

bool CLocalTrendHough::StraightRidgeTransform(const CImage &Source,bool PreEdgeDetect)
{
	CPoint2D<int> UL(0,0);
	CPoint2D<int> LR(Source.GetWidth(),Source.GetHeight());
	return StraightRidgeTransform(Source,UL,LR,PreEdgeDetect);
}

bool CLocalTrendHough::StraightGradientTransform(const CImage &Source,
												 CPoint2D<int> UL, CPoint2D<int> LR)
{
	m_NT=m_NT0;
	m_PreEdgeDetect=false;
	m_RidgeTransform=false;
	m_GradientTransform=true;
	return StraightTransform(Source,UL,LR);
}

bool CLocalTrendHough::StraightGradientTransform(const CImage &Source)
{
	CPoint2D<int> UL(0,0);
	CPoint2D<int> LR(Source.GetWidth(),Source.GetHeight());
	return StraightGradientTransform(Source,UL,LR);
}

void CLocalTrendHough::UpdateHoughSpaceGradient(int x, int y)
{
	int a,b,f00,f10,f01,f11,ir,it;
	double grad,theta;
	f00=m_zInt.GetPixel(x,y);
	f10=m_zInt.GetPixel(x-1,y);
	f01=m_zInt.GetPixel(x,y-1);
	f11=m_zInt.GetPixel(x-1,y-1);
	a=f10-f00+f11-f01;
	b=f01-f00+f11-f10;
	grad=sqrt(a*a+b*b); //Gradient magnitude
	theta=atan2(b,a+1e-20);  //Angle defining the gradient direction
	ir=Round((-(x-m_ULX)*cos(theta)-(y-m_ULY)*sin(theta))/m_dr);
	ir+=m_NR/2;
	it=Round(theta/m_dt);
	it+=m_NT/2;
	hs[it][ir]+=(int)(grad);
}

void CLocalTrendHough::UpdateHoughSpaceRidge(int x, int y)
{
	int f0,f1,f2,f3,f4,f5,f6,f7,f8,it,ir;
	long int m11,m20,m02;
	double theta,K1,K2,s,c,buf;
	f0=m_zInt.GetPixelFast(x,y);
	f1=m_zInt.GetPixelFast(x+1,y);
	f2=m_zInt.GetPixelFast(x+1,y+1);
	f3=m_zInt.GetPixelFast(x,y+1);
	f4=m_zInt.GetPixelFast(x-1,y+1);
	f5=m_zInt.GetPixelFast(x-1,y);
	f6=m_zInt.GetPixelFast(x-1,y-1);
	f7=m_zInt.GetPixelFast(x,y-1);
	f8=m_zInt.GetPixelFast(x+1,y-1);
	m20=f1+f5-2*f0;
	m02=f3+f7-2*f0;
	m11=f2+f6-f4-f8;
	if (m02+m20<0)
	{
        if (fabs(m02-m20)<1e-20)
			theta=ipl::PI/4;
        else
			theta=0.5*atan(-(float)m11/(m02-m20)/2);
        c=cos(theta);
        s=sin(theta);
        K1=-m20*c*c-m11/2*s*c-m02*s*s;
        K2=-m02-m20-K1;
        if (K1>fabs(K2)){theta+=ipl::PI/2;buf = K1;K1=K2;K2=buf;}
        if (theta<0) theta+=ipl::PI;
        ir=(int)(((x-m_ULX)*sin(theta)-(y-m_ULY)*cos(theta))/m_dr+0.5);
        ir+=m_NR/2;
        it=(int)(theta/m_dt+0.5);
        if (K2>0)
			hs[it][ir]+=(int)(K2-K1);
	}
}

void CLocalTrendHough::SmoothHough()
{
	int ir, it;
	if ((m_TotalPostBlur>0)&&(m_TotalPostBlur<5))
		for (int w=0;w<m_TotalPostBlur;w++)
		{
			for (ir=1;ir<m_NR-1;ir++)
			{
				for (it=1;it<m_NT-1;it++)
				{
					hs1[it][ir]=(unsigned long int)(57*
						(hs[it][ir]+hs[it+1][ir]+hs[it-1][ir]+
						hs[it][ir+1]+hs[it][ir-1]+hs[it+1][ir+1]+
						hs[it-1][ir+1]+hs[it+1][ir-1]+hs[it-1][ir-1])>>9);
				}
				if (m_GradientTransform)
				{
					hs1[0][ir]=(unsigned long int)(57*
						(hs[0][ir]+hs[0][ir+1]+hs[0][ir-1]+
						hs[1][ir]+hs[1][ir+1]+hs[1][ir-1]+
						hs[m_NT-1][ir]+hs[m_NT-1][ir+1]+hs[m_NT-1][ir-1])>>9);
					hs1[m_NT-1][ir]=(unsigned long int)(57*
						(hs[m_NT-1][ir]+hs[m_NT-1][ir+1]+hs[m_NT-1][ir-1]+
						hs[m_NT-2][ir]+hs[m_NT-2][ir+1]+hs[m_NT-2][ir-1]+
						hs[0][ir]+hs[0][ir+1]+hs[0][ir-1])>>9);
				} else
				{
					hs1[0][ir]=(unsigned long int)(57*
						(hs[0][ir]+hs[0][ir+1]+hs[0][ir-1]+
						hs[1][ir]+hs[1][ir+1]+hs[1][ir-1]+
						hs[m_NT-1][m_NR-ir]+hs[m_NT-1][m_NR-ir-1]+hs[m_NT-1][m_NR-ir+1])>>9);
					hs1[m_NT-1][ir]=(unsigned long int)(57*
						(hs[m_NT-1][ir]+hs[m_NT-1][ir+1]+hs[m_NT-1][ir-1]+
						hs[m_NT-2][ir]+hs[m_NT-2][ir+1]+hs[m_NT-2][ir-1]+
						hs[0][m_NR-ir]+hs[0][m_NR-ir+1]+hs[0][m_NR-ir-1])>>9);
				}
				hs1[0][0]=hs[0][0];
				hs1[0][m_NR-1]=hs[0][m_NR-1];
				hs1[m_NT-1][0]=hs[m_NT-1][0];
				hs1[m_NT-1][m_NR-1]=hs[0][m_NR-1];
			}
			for (ir=0;ir<m_NR;ir++)
				for (it=0;it<m_NT;it++) 
					hs[it][ir]=hs1[it][ir];
		}
}

void CLocalTrendHough::PrepareHoughTransformation(const CImage &Source)
{
	int it,ir;
	m_zInt=CIntImage(Source.GetWidth(),Source.GetHeight());
	for (int x=0;x<Source.GetWidth();x++)
		for (int y=0;y<Source.GetHeight();y++)
		{
			unsigned int g=Source.GetPixel(x,y);
			m_zInt.SetPixel(x,y,g<<6);
		}
		CMaskOperation MO;
		if (m_PreEdgeDetect)EdgeDetect();
		if ((m_TotalPreBlur>0)&&(m_TotalPreBlur<8))
			for (int i=0;i<m_TotalPreBlur;i++)MO.BlurEightConnected3x3(m_zInt);
			for (ir=0;ir<m_NR;ir++)
				for (it=0;it<m_NT;it++)
					hs[it][ir]=0;
}

bool CLocalTrendHough::StraightTransform(const CImage &Source, CPoint2D<int> UL, CPoint2D<int> LR)
{
	if (Source.IsEmpty()) 
	{
		ostringstream ost;
		ost << "CLocalTrendHough::StraightTransform() Source image is empty" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	if (Source.GetBits()!=8) 
	{
		ostringstream ost;
		ost << "CLocalTrendHough::StraightTransform() Source image is " 
			<< Source.GetBits() << " b/p, must be 8 b/p" << IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	double rmax;
	int x,y;
	PrepareHoughTransformation(Source);
	m_w=LR.GetX()-UL.GetX();//Source.GetWidth();
	m_h=LR.GetY()-UL.GetY();//Source.GetHeight();
	m_ULX=UL.GetX();
	m_ULY=UL.GetY();
	if (m_GradientTransform)
	{
		rmax=sqrt(m_w*m_w+m_h*m_h);
		m_dr=2*rmax/m_NR;
		m_dt=8*atan(1)/m_NT;
		for (y=m_ULY+1;y<m_ULY+m_h;y++)
			for (x=m_ULX+1;x<m_ULX+m_w;x++)
			{
				UpdateHoughSpaceGradient(x,y);
			}
	}
	else if (m_RidgeTransform)
	{
		rmax=sqrt(m_w*m_w+m_h*m_h);
		m_dr=2*rmax/m_NR;
		m_dt=4*atan(1)/m_NT;
		for (y=m_ULY+1;y<m_ULY+m_h-1;y++)
			for (x=m_ULX+1;x<m_ULX+m_w-1;x++)
			{
				UpdateHoughSpaceRidge(x,y);
			}
			for (int ir=0;ir<m_NR;ir++)
				hs[0][ir]=hs[m_NT-1][m_NR-ir];
	}
	SmoothHough();
	m_TransformationPerformed=true;
	return true;
}

void CLocalTrendHough::PointsToLines(vector<CLine2D<double> > &LineArray)
{
	double theta;
	double r;
	LineArray.resize(m_PointArray.size());
	if (!m_TransformationPerformed) 
		return;
	for (int k=0;k<m_PointArray.size();k++)
	{
		if (m_GradientTransform)
		{
			theta=(m_PointArray[k].GetX()-m_NT/2.0)*m_dt;
			r=(m_PointArray[k].GetY()-m_NR/2.0)*m_dr;
			LineArray[k].m_Dir.SetY(cos(theta));
			LineArray[k].m_Dir.SetX(-sin(theta));
			LineArray[k].m_P.SetX(-r*cos(theta)+m_ULX);
			LineArray[k].m_P.SetY(-r*sin(theta)+m_ULY);
		} 
		else
		{
			theta=m_PointArray[k].GetX()*m_dt;
			r=(m_PointArray[k].GetY()-m_NR/2.0)*m_dr;
			LineArray[k].m_Dir.SetX(cos(theta));
			LineArray[k].m_Dir.SetY(sin(theta));
			LineArray[k].m_P.SetX(r*sin(theta)+m_ULX);
			LineArray[k].m_P.SetY(-r*cos(theta)+m_ULY);
		}
	}
}

bool CLocalTrendHough::SearchLines(float HalfsideThetaDegrees, float HalfsideRhoPixels,
								   int TotalLines, vector<CLine2D<double> > &LineArray)
{
	if (!m_TransformationPerformed) 
	{
		ostringstream ost;
		ost << "CLocalTrendHough::SearchLines() Must call a Transform-method first"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	
	int ir,it,itmax,irmax;
	for (ir=0;ir<m_NR;ir++)
	{
		for (it=0;it<m_NT;it++) 
			hs1[it][ir]=hs[it][ir];
		unsigned long int Peak,q;
		int ThetaHalfsideSteps=(int)(HalfsideThetaDegrees/m_AngleStepDegrees);
		int RhoHalfsideSteps=(int)(HalfsideRhoPixels*m_dr);
		for (int k=0;k<TotalLines;k++)
		{
			Peak=0;
			for (ir=0;ir<m_NR;ir++)
			{
				for (it=0;it<m_NT;it++)
				{
					q=hs1[it][ir];
					if (q>Peak)
					{
						Peak=q;
						irmax=ir;itmax=it;
					}
				}
			}
			for (it=itmax-ThetaHalfsideSteps;it<=itmax+ThetaHalfsideSteps;it++)
			{
				for (ir=irmax-RhoHalfsideSteps;ir<=irmax+RhoHalfsideSteps;ir++)
				{
					int it0=it;
					int ir0=ir;
					if (it0>=m_NT)it0-=m_NT;
					if (it0<0)it0+=m_NT;
					if ((it!=it0)&&(m_RidgeTransform)) 
						ir0=m_NR-ir;
					else 
						ir0=ir;
					if (ir0<0)
						ir0=0;
					if (ir0>=m_NR)
						ir0=m_NR-1;
					//   q=hs1[it0][ir0];
					hs1[it0][ir0]=0;
				}
			}
			CPoint2D<int> Pt;
			Pt.Set(itmax,irmax);
			m_PointArray.push_back(Pt);
		}
		PointsToLines(LineArray);
	}
	return true;
}

bool CLocalTrendHough::DrawHoughSpace(CImage &View, double Gamma) const
{
    if (!m_TransformationPerformed) 
	{
		ostringstream ost;
		ost << "CLocalTrendHough::DrawHoughSpace() Must call a Transform-method first"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
    int ir,it,irmax,itmax;
    View.Alloc(m_NT,m_NR,24);
    long int Peak,q;
	Peak=0;
    for (ir=0;ir<m_NR;ir++)
	{
		for (it=0;it<m_NT;it++)
		{
			q=hs[it][ir];
			if (q>Peak)
			{
				Peak=q;
				irmax=ir;
				itmax=it;
			}
		}
	}
	double logden=log(Peak+1);
	for (ir=0;ir<m_NR;ir++)
		for (it=0;it<m_NT;it++)
			View.SetPixel(it,ir,0x10101*(int)(255.0*pow(1.0*hs[it][ir]/Peak,Gamma)));
	return true;
}

bool CLocalTrendHough::DrawHoughSpaceWithPoints(CImage &View,double Gamma, int PointColor) const
{
	if (!m_TransformationPerformed) 
	{
		ostringstream ost;
		ost << "CLocalTrendHough::DrawHoughSpaceWithPoints() Must call a Transform-method first"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	DrawHoughSpace(View,Gamma);
	int itmax,irmax;
	for (int k=0;k<m_PointArray.size();k++)
	{
		itmax=m_PointArray[k].GetX();
		irmax=m_PointArray[k].GetY();
		View.SetPixel(itmax,irmax,PointColor);
		View.SetPixel(itmax-1,irmax,PointColor);
		View.SetPixel(itmax,irmax+1,PointColor);
		View.SetPixel(itmax,irmax-1,PointColor);
		View.SetPixel(itmax+1,irmax,PointColor);
	}
	return true;
}

bool CLocalTrendHough::LinesToImage(CImage &View, vector<CLine2D<double> > &LineArray,
									int color) const
{
	if (!m_TransformationPerformed) 
	{
		ostringstream ost;
		ost << "CLocalTrendHough::LinesToImage() Must call a Transform-method first"
			<< IPLAddFileAndLine;
		CError::ShowMessage(IPL_WARNING,ost.str().c_str());
		return false;
	}
	for (int k=0;k<LineArray.size();k++)
	{
		double Slope;
		if (LineArray[k].m_Dir.GetX()==0) 
			Slope = 1e20; 
		else
			Slope=LineArray[k].m_Dir.GetY()/LineArray[k].m_Dir.GetX();
		if (fabs(Slope)<1)
			
			for (int x=0;x<View.GetWidth();x++)
			{
				double y=LineArray[k].m_P.GetY()+(x-LineArray[k].m_P.GetX())*Slope;
				View.SetPixel(x,(int)(y+0.5),color);
			}
			else
				for (int y=0;y<View.GetHeight();y++)
				{
					double x;
					if (Slope>0.9e20) x=LineArray[k].m_P.GetX(); 
					else
						x=LineArray[k].m_P.GetX()+(y-LineArray[k].m_P.GetY())/Slope;
					View.SetPixel((int)(x+0.5),y,color);
				}
	}
	return true;
}

} // end namespace videndo
