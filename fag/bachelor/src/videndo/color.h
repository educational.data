#ifndef _VIDENDO_COLOR_H
#define _VIDENDO_COLOR_H

#include "videndo_setup.h" /* always include the videndo setup file */
#include <ipl98/cpp/ipl98_types.h>
#include <ipl98/cpp/ipl98_general_functions.h>
#include <ipl98/cpp/palette.h>

namespace videndo{ // use namespace if C++

using ipl::UINT8;
using ipl::INT16;
using ipl::UINT16;
using ipl::UINT32;
using ipl::Max;
using ipl::Min;

/** CColor 

	This class is part of the <a href="http://vejleby/videndo">Videndo Library �</a>.
	\class CColor videndo/color.h
	@version 0.50
	@author Ren� Dencker Eriksen (edr@mip.sdu.dk) */
class CColor{
	public: // attributes
	protected: // attributes
	public: // methods
		/**	Create a UINT32 RGB-component from the three 
			components r, g and b.
			@return A palette value containing all three r,b and b components. */
		inline static UINT32 CreateRGB(UINT8 r, UINT8  g, UINT8 b);
		/** Swaps the R and B value in a RGB, i.e. RGB -> BGR. */
		inline static void SwapRB(UINT32& Color);
		/** Extracts the R-value from a UINT32 RGB-component (returned by 
			the GetColor() method in this class).
			@return UINT8 value of the red component. */
		inline static UINT8 GetRedVal(UINT32 rgb);
		/** Extracts the G-value from a UINT32 RGB-component (returned by 
			the GetColor() method in this class).
			@return UINT8 value of the green component. */
		inline static UINT8 GetGreenVal(UINT32 rgb);
		/** Extracts the B-value from a UINT32 RGB-component (returned by 
			the GetColor() method in this class).
			@return UINT8 value of the blue component. */
		inline static UINT8 GetBlueVal(UINT32 rgb);
		/** Converts and RGB value to HSI (hue, saturation and intensity).
			@param r Red component input.
			@param g Green component input.
			@param b Blue component input.
			@param H Hue output.
			@param S Saturation output.
			@param I Intensity output.
			@version 0.70
			@author Implementation by Ren� Dencker (edr@mip.sdu.dk) */
		inline static void RGB2HSI(UINT8 r, UINT8 g, UINT8 b, INT16 &H, INT16 &S, INT16 &I);
	private: // methods
};

/////////////////////////////////////////////////
//// Inline methods
/////////////////////////////////////////////////

inline UINT32 CColor::CreateRGB(UINT8 r, UINT8  g, UINT8 b)
{
	return k_PalCreateRGB(r, g ,b);
}

inline void CColor::SwapRB(UINT32& Color)
{
	Color = ((Color << 16) & 0x00ff0000) | ((Color >> 16) & 0x000000ff) | (Color & 0x0000ff00);
}

inline UINT8 CColor::GetRedVal(UINT32 rgb)
{
	return k_PalGetRVal(rgb);
}

inline UINT8 CColor::GetGreenVal(UINT32 rgb)
{
	return k_PalGetGVal(rgb);
}

inline UINT8 CColor::GetBlueVal(UINT32 rgb)
{
	return k_PalGetBVal(rgb);
}

inline void CColor::RGB2HSI(UINT8 r, UINT8 g, UINT8 b, INT16& H, INT16& S, INT16& I)
{
	int nMin, nMax,nSum,nDiff;
	nMax = Max(r,Max(g,b));
	nMin = Min(r,Min(g,b));
	I=(nMin+nMax+1)>>1;
	if (nMin==nMax) 
	{
		H=0;
		S=0;
	}
	else
	{
		nDiff = nMax - nMin;
		if (I<=128) 
			nSum = nMax + nMin; 
		else
			nSum = 511 - nMax - nMin;
		S = (255*nDiff)/nSum;
		if (r==nMax)
			H = (85*(g-b))/(nDiff<<1);
		else if (g==nMax)
            H = 85 + (85*(b-r))/(nDiff<<1);
		else
            H = 170+ (85*(r-g))/(nDiff<<1);
		if (H<0)
			H+=256;
	}
}

} // end namespace videndo

#endif //_VIDENDO_COLOR_H
