// CameraDocument.h: interface for the CCameraDocument class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CAMERADOCUMENT_H__73DB05C6_025C_4449_A2C2_1F786883A243__INCLUDED_)
#define AFX_CAMERADOCUMENT_H__73DB05C6_025C_4449_A2C2_1F786883A243__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <camera/camera_firewire/camera_firewire_cmu.h>
#include <sstream>
#include "../camera/CameraSettings.h"
#include <ipl98/cpp/image.h>

using namespace std;
using namespace ipl;

class CCameraDocument  
{
	CCameraFirewireCMU theCamera;
public:
	void setCameraSettings(const CCameraSettings &set);
	string getName();
	bool isInitialized();
	void SetUp();
	CImage AcquireImage();
	CCameraDocument();
	virtual ~CCameraDocument();

private:
	string m_name;
	bool initialized;
};

#endif // !defined(AFX_CAMERADOCUMENT_H__73DB05C6_025C_4449_A2C2_1F786883A243__INCLUDED_)
