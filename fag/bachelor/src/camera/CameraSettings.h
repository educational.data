// CameraSettings1.h: interface for the CCameraSettings class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _CAMERASETTINGS_H__INCLUDED_
#define _CAMERASETTINGS_H__INCLUDED_

class CCameraSettings  
{
public:
  int exposure, shutter, gain, brightness, gamma;

  CCameraSettings(const int exposure,
		  const int shutter, 
		  const int gain, 
		  const int brightness, 
		  const int gamma);

  virtual ~CCameraSettings();

};

#endif 
