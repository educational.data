// CameraDocument.cpp: implementation of the CCameraDocument class.
//
//////////////////////////////////////////////////////////////////////


//#include "RobotHandler.h"
#include "CameraDocument.h"
#include <iostream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

using namespace std;

CCameraDocument::CCameraDocument()
{
	initialized=false;
}

CCameraDocument::~CCameraDocument()
{
	
}

CImage CCameraDocument::AcquireImage()
{
	theCamera.SetFormatModeFrameRate(0,5,15);

	CImage Img;
	if(initialized)
		theCamera.Acquire(Img);
	return Img;
}

void CCameraDocument::SetUp()
{
	theCamera.QueryCameras();

	bool selected = theCamera.SelectCamera(0);
	
	if(selected){
		initialized = theCamera.Initialize();
		if(initialized)
		{		
			cout<< theCamera.GetCameraVendor() << " " << theCamera.GetCameraName() << endl; 
			m_name = theCamera.GetCameraName();

			if (!theCamera.AutoExposureAvailable())
				cout << "AutoExposure not available"<< "\r\n";
			else{
				cout << "AutoExposure available"<< endl;
				cout << "GetAutoExposure(): " << theCamera.GetAutoExposure()<< "\r\n";
				cout << "GetAutoExposureMin(): " <<  theCamera.GetAutoExposureMin()<< "\r\n";
				cout << "GetAutoExposureMax(): " <<  theCamera.GetAutoExposureMax()<< "\r\n";
				cout << "SetAutoExposure(117) " << theCamera.SetAutoExposure(117)<< "\r\n";
				cout << "GetAutoExposure(): " << theCamera.GetAutoExposure()<< "\r\n";
			}

			cout << "GetAllModes:" << "\r\n";
			ostringstream os;
			theCamera.GetAllFormatsAndModes(os);
			cout << os.str() << "\r\n";

			vector<string> CamInfo;
			cout << "GetAllCameraInfo(CamInfo);" << "\r\n";
			theCamera.GetAllCameraInfo(CamInfo);
			for(int i=0; i<theCamera.GetTotalCameras(); i++)
			{
				cout << CamInfo[i] << "\r\n";
			}
		}
		else
			cout << "Could not initialise camera 0\r\n";
	} else
		cout << "No camera detected\r\n";

}

bool CCameraDocument::isInitialized()
{
	return initialized;
}

string CCameraDocument::getName()
{
	return m_name;
}

void CCameraDocument::setCameraSettings(const CCameraSettings &set)
{
	if(initialized)
	{
		theCamera.SetAutoExposure(set.exposure);
		theCamera.SetBrightness(set.brightness);
		theCamera.SetGain(set.gain);
		theCamera.SetGamma(set.gamma);
		theCamera.SetShutter(set.shutter);

	}
}
