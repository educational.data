// CameraSettings1.cpp: implementation of the CCameraSettings class.
//
//////////////////////////////////////////////////////////////////////

#include "CameraSettings.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCameraSettings::~CCameraSettings()
{

}

CCameraSettings::CCameraSettings(const int exposure, 
				 const int shutter, 
				 const int gain, 
				 const int brightness, 
				 const int gamma)
{
  this->exposure = exposure;
  this->shutter = shutter;
  this->gain = gain;
  this->brightness = brightness;
  this->gamma = gamma;
}
