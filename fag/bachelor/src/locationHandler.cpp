#include "locationHandler.h"
#include "locater.h"
#include "win32_2/exceptions.h"
#include "videndo/camera_model.h"
//#include "win32_2/OffsetCalculator.h"

static CalibrateHandler p_handler;
ICalibrateHandler *getCalibrateHandler() {
  return &p_handler;
}

CalibrateHandler::CalibrateHandler() {
  camera.SetUp();
}

const CCorresponding3D2DPoints 
CalibrateHandler::findPointsInSystem() {
  CCorresponding3D2DPoints PointSets;

  if(locater==NULL) {
    on_error("Cannot use a file to calibrate yet");
    return CCorresponding3D2DPoints();
  }

  if(robot_track.size() < 6) {
    on_error("Not enough points to perform calibration.\nHave you loaded any?");
    return CCorresponding3D2DPoints();
  }

  try {
    robot.connect();
  } catch(const std::exception &ex) {
    on_error(ex.what());
    return PointSets;
  }
  location l;
  int cnt = 0;
  int tSize = robot_track.positions.size();
  for(CRobotPoint::itcpos it= robot_track.positions.begin();it != robot_track.positions.end();++it) {
    try {
      l = *it;

      int res = (100*++cnt)/(tSize+6);
      if(!on_calibrate_status(res)) {
        robot.disconnect();
        throw user_abort_exception();
      }
		
      CPoint3D<float> threeDP(l.x,l.y,l.z);
      try {
	      robot.moveTo(l.x,l.y,l.z,l.yaw, l.pitch,l.roll,50);
      } catch(...) {
	      cout << "Failed to move robot to pose: " << threeDP << endl;
	      continue;
      }

      if(!acquireImage()) {
	      robot.disconnect();
	      return CCorresponding3D2DPoints();
      }
      
      			
      locater->locate(img);
      if(!locater->hasPoint()) {
	      cout << "could not locate point." << endl;
	      continue;
      }
      drawMark(img,locater->getPoint().GetX(),locater->getPoint().GetY());

      on_image_update();

      CPoint2D<float> twoDP = locater->getPoint();
      //cout << "Point " << twoDP << " <-> " << threeDP << endl;

      PointSets.AddPointSet(threeDP,twoDP);
    } catch(const user_abort_exception &ex) {
      //rethrow so it will be handled outside
      throw ex;
    } catch(const exception &ex) {
      on_error(ex.what());
      return CCorresponding3D2DPoints();      
    }
  }
  robot.disconnect();
  return PointSets;
}

//DEL const CCorresponding3D2DPoints
//DEL CalibrateHandler::findAngularPoints(const location &l) {
//DEL   CCorresponding3D2DPoints PointSets;
//DEL   cout << "Now doing angular pose" << endl;
//DEL 
//DEL   CCorresponding3D2DPoints poses;
//DEL   std::vector<location> angulars;
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,17,90,0)); // base pose
//DEL 
//DEL   angulars.push_back(location(l.x,l.y,l.z,120,17,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,60,17,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,47,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,-27,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,17,120,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,17,60,0));
//DEL /*
//DEL   angulars.push_back(location(l.x,l.y,l.z,100,17,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,80,17,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,27,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,7,90,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,17,100,0));
//DEL   angulars.push_back(location(l.x,l.y,l.z,90,17,80,0));
//DEL */
//DEL   try {
//DEL     robot.connect();
//DEL   } catch(const std::exception &ex) {
//DEL     on_error(ex.what());
//DEL     return CCorresponding3D2DPoints();
//DEL   }
//DEL   for(std::vector<location>::iterator it= angulars.begin();it != angulars.end();++it) {
//DEL     try {
//DEL       location l = *it;
//DEL       //int res = (100*++cnt)/(tSize+6);
//DEL       if(!on_calibrate_status(100)) {
//DEL         robot.disconnect();
//DEL         throw user_abort_exception();
//DEL       }
//DEL 	  
//DEL       CPoint3D<float> threeDP(l.yaw,l.pitch,l.roll);
//DEL       try {
//DEL 	      robot.moveTo(l,25);
//DEL       } catch(...) {
//DEL 	      cout << "Failed to move robot to pose" << endl;
//DEL 	      continue;
//DEL       }
//DEL 
//DEL       if(!acquireImage()) {
//DEL 	      robot.disconnect();
//DEL 	      return CCorresponding3D2DPoints();
//DEL       }
//DEL     
//DEL       		  
//DEL       locater->locate(img);
//DEL       if(!locater->hasPoint()) {
//DEL 	      cout << "could not locate point." << endl;
//DEL 	      continue;
//DEL       }
//DEL       drawMark(img,locater->getPoint().GetX(),locater->getPoint().GetY());
//DEL 
//DEL       on_image_update();
//DEL 
//DEL       CPoint2D<float> twoDP = locater->getPoint();
//DEL       
//DEL       cout << "Point " << twoDP << " <-> " << threeDP << endl;
//DEL 
//DEL       PointSets.AddPointSet(threeDP,twoDP);
//DEL     } catch(const user_abort_exception &ex) {
//DEL       //rethrow so it will be handled outside
//DEL       throw ex;
//DEL     } catch(const exception &ex) {
//DEL       on_error(ex.what());
//DEL       return CCorresponding3D2DPoints();      
//DEL     }
//DEL   }
//DEL   robot.moveTo(angulars[0],100);
//DEL   robot.disconnect();
//DEL 
//DEL   return PointSets;
//DEL }

void 
CalibrateHandler::doCalibrate() {

  try {
    CCorresponding3D2DPoints PointSets = findPointsInSystem();
    if(PointSets.GetTotalPointSetsInUse() == 0) 
      return;

    videndo::CCameraModel pers(img.GetWidth(), img.GetHeight());

    
    if(pers.CalibrateWithRadialDist(PointSets) == false) {
      cout << ">>> System is NOT calibrated <<<" << endl;
      on_error("Not enough points to perform valid calibration");
      return;
    }

    // perform second angular calibration
    /*CPoint3D<float> lastP = PointSets.GetPoint3D(PointSets.GetTotalPointSetsInUse()-1);
	  location lastLoc(lastP.GetX(),lastP.GetY(),lastP.GetZ(),0,0,0,0);

    CCorresponding3D2DPoints AngularPoints = findAngularPoints(lastLoc);

    vector<CPoint3D<float> > poses;
    vector<CPoint2D<float> > points;
    for(int i = 0; i < AngularPoints.GetTotalPointSetsInUse();++i) {
      poses.push_back(AngularPoints.GetPoint3D(i));
      points.push_back(AngularPoints.GetPoint2D(i));
    }

    COffsetCalculator calc(pers,CPoint3D<float>(70,-15,-20),lastP,40,1,points,poses);

    CCameraModel model = calc.getModel();
    CPoint2D<float> np;
    model.Calc3DTo2DRad(lastP,np);
    cout << "New tcp at: " << np << endl;
    img = camera.AcquireImage();
    drawMark(img,np.GetX(),np.GetY());
    on_image_update();
    
    */
    cout << pers << endl << endl;

    pers.ErrorsToStream(PointSets,cout);

    
    cout << "*** Resultat efter angul�r kalibrering" << endl;
    
    // cout << model << endl;
    //cout << "Offset: " << calc.getOffset() << endl;
    //cout << "Fejl: " << calc.getFejl() << endl;
  } catch(const std::exception &ex) {
    on_error(ex.what());
  }

  //findErrors(pers,PointSets);
	
  // UpdateAllViews(NULL);
}

void 
CalibrateHandler::setMethod(int method) {
  if(locater) 
    delete locater;
  switch(method) {
  case 2:
    locater = NULL;
    on_error("Sorry, we havn't implemented the method to load 2D<->3D pointsets yet");
    break;
  case 1:
    locater = new leo::DiodeDetector();
    camera.setCameraSettings(CCameraSettings(511,7,0,0,1));
    break;
  case 0:
  default:
    locater = new tob_vision::LandmarkLocator();
    camera.setCameraSettings(CCameraSettings(254,7,139,261,0));
    break;
  }
}

void 
CalibrateHandler::drawMark(CImage & img, const int x, const int y) {
  typedef CPoint2D<int> cp;
  
  cp p = locater->getPoint();
  
  int radius=15;
  int innerPoint = 5;
  int outherPoint = 20;
  
  img.DrawLine(cp(x-outherPoint,y+1),cp(x-innerPoint,y+1),0xFF);
  img.DrawLine(cp(x-outherPoint,y-1),cp(x-innerPoint,y-1),0xFF);
  
  img.DrawLine(cp(x+outherPoint,y+1),cp(x+innerPoint,y+1),0xFF);
  img.DrawLine(cp(x+outherPoint,y-1),cp(x+innerPoint,y-1),0xFF);
  
  img.DrawLine(cp(x-1,y+outherPoint),cp(x-1,y+innerPoint),0xFF);
  img.DrawLine(cp(x+1,y+outherPoint),cp(x+1,y+innerPoint),0xFF);
  
  img.DrawLine(cp(x-1,y-outherPoint),cp(x-1,y-innerPoint),0xFF);
  img.DrawLine(cp(x+1,y-outherPoint),cp(x+1,y-innerPoint),0xFF);
  //img.DrawLine(p-x,p+x,0xFF);
  
  /*
  img.DrawCircle(p,radius+1,0xFF);
  img.DrawCircle(p,radius-1,0xFF);
  // img.DrawCircle(p,16,0xFF);
  // img.DrawCircle(p,14,0xFF);
  
  img.DrawCircle(p,radius,0);*/
  // img.DrawCircle(p,15,0);
  img.DrawLine(cp(x-outherPoint,y),cp(x-innerPoint,y),0);
  img.DrawLine(cp(x+outherPoint,y),cp(x+innerPoint,y),0);
  img.DrawLine(cp(x,y+outherPoint),cp(x,y+innerPoint),0);
  img.DrawLine(cp(x,y-outherPoint),cp(x,y-innerPoint),0);
}

bool 
CalibrateHandler::acquireImage() {
  if(!camera.isInitialized()) {
    on_error("Cannot acquire image as no camera is currently attached to this computer.");
    cout << "Camera was not found" << endl;
    return false;
  }
  img = camera.AcquireImage();
  on_image_update();
  return true;
}

const ipl::CImage &
CalibrateHandler::getImage() const {
  return img;
}

bool CalibrateHandler::loadImage(const std::string &filename)
{
  bool result = img.Load(filename);
  cout << "*** Loaded image: " << filename << " ***" << endl << endl;
  on_image_update();
  return result;
}

bool CalibrateHandler::loadRobotPath(const std::string &filename)
{
  on_error("Not implemented");
  return true;
}

bool CalibrateHandler::loadScenario(const std::string &filename)
{
  on_error("Not implemented");
  return true;
}

bool CalibrateHandler::saveScenario(const std::string &filename)
{
  on_error("Not implemented");
  return true;
}

bool CalibrateHandler::saveImage(const std::string &filename)
{
  on_error("Not implemented");
  return true;
}

bool CalibrateHandler::saveRobotPath(const std::string &filename)
{
  on_error("Not implemented");
  return true;
}

void 
CalibrateHandler::findPunkt()
{
  if(locater) {
    locater->locate(img);
    if(locater->hasPoint()) {
      cout << "Point found at " << locater->getPoint() << endl;
      drawMark(img,locater->getPoint().GetX(),locater->getPoint().GetY());
      on_image_update();
    } else {
      on_error("No point found in image");
    }
  } else on_error("No locater method selected");
}

bool CalibrateHandler::hasCamera()
{
  return camera.isInitialized();
}

bool CalibrateHandler::randomizePoints(const int numPoints)
{
  // Generer punkter til path. 
  robot_track.clean();

  int offsetX = 350;
  int offsetY = 0;
  int offsetZ = 300;

  int maxX = 550;
  int maxY = 400;
  int maxZ = 500;

  int spanX = offsetX-maxX;
  int spanY = offsetY-maxY;
  int spanZ = offsetZ-maxZ;
  
  int X,Y,Z;
  for(int cnt = 0; cnt < numPoints;++cnt) {
    X = offsetX + rand() % spanX;
    Y = offsetY + rand() % spanY;
    Z = offsetZ + rand() % spanZ;

    robot_track.add(location( X, Y, Z, 90, 17, 90, 0 ));
  }
  //robot_track.add(location( 350, 400, 400, 90, 17, 90, 0 ));

  cout << robot_track.toString() << endl;
  return true;
}

void CalibrateHandler::setImage(const CImage &newimg)
{
  img = newimg;
  on_image_update();
}
