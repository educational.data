// OffsetCalculator.h: interface for the COffsetCalculator class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_OFFSETCALCULATOR_H__1540A371_60F8_496A_B531_D655F501EF48__INCLUDED_)
#define AFX_OFFSETCALCULATOR_H__1540A371_60F8_496A_B531_D655F501EF48__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <points/point3d.h>
#include <points/point2d.h>
#include "../videndo/pose.h"
#include "../videndo/camera_model.h"
#include <vector>


namespace ipl {
  class CCorresponding3D2DPoints;
  /*template<class T> class CPoint3D;
  template<class T> class CPoint2D;*/
}

using namespace videndo;
using namespace ipl;
using namespace std;

class COffsetCalculator  
{
public:
	virtual ~COffsetCalculator();
  COffsetCalculator(
    const CCameraModel &source,
    const CPoint3D<int> &StartGuess, 
    const CPoint3D<float> &CenterPointOfTranspose, 
    const int HalfWidthOfSearchCube,
    const float step,
    const vector<CPoint2D<float> > &Points,
    const vector<CPoint3D<float> > &Pose
  );
  
  float getFejl() {
    return residualFejl;
  }

  CCameraModel getModel() {
    return destination;
  }
  CPoint3D<float> getOffset() {
    return offset;
  }
private:
	float residualFejl;
	CCameraModel destination;
  CPoint3D<float> offset;
  
  void ModifyOffset(
    const CCameraModel &Source, 
    const CCorresponding3D2DPoints &CorrThreeToTwooDim, 
    const vector<CPose<float> > Poses,
		const CPoint3D<int> OffsetStartGuess, 
    const int HalfWidthOfSearchCube,
    const float Step,
    CPoint3D<float> &Offset,
		CCameraModel &Destination,float &ResidualError
  );
  float DeriveError(
    const CCameraModel &Cam, 
    const CCorresponding3D2DPoints &C32
  );

};

#endif // !defined(AFX_OFFSETCALCULATOR_H__1540A371_60F8_496A_B531_D655F501EF48__INCLUDED_)
