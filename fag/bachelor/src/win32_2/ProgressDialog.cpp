// ProgressDialog.cpp : implementation file
//

#include "stdafx.h"
#include "RobotHandler.h"
#include "ProgressDialog.h"
#include "../ihandler.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProgressDialog dialog


CProgressDialog::CProgressDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CProgressDialog::IDD, pParent)
{
  abort = false;
  Create(CProgressDialog::IDD);
	//{{AFX_DATA_INIT(CProgressDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CProgressDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgressDialog)
	DDX_Control(pDX, IDC_PROG, m_progress);
	DDX_Control(pDX, IDC_BTN_ABORT, m_abortbutton);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProgressDialog, CDialog)
	//{{AFX_MSG_MAP(CProgressDialog)
	ON_BN_CLICKED(IDC_BTN_ABORT, onBtnAbort)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgressDialog message handlers

void CProgressDialog::onBtnAbort() 
{
  m_abortbutton.EnableWindow(false);
  abort=true;
}

void CProgressDialog::perform()
{

  m_abortbutton.EnableWindow(true);
  getCalibrateHandler()->on_calibrate_status.connect(sig::slot(this,&CProgressDialog::setState));
  ShowWindow(SW_SHOW);
  DoEvents();
  getCalibrateHandler()->doCalibrate();
  DoEvents();
  ShowWindow(SW_HIDE);
}



bool CProgressDialog::setState(int state)
{
  DoEvents();
  if(abort) return false;
  m_progress.SetRange(0,100);
  m_progress.SetPos(state);
  return true;
}

void CProgressDialog::DoEvents()
{
  MSG oMSG;
  while(::PeekMessage(&oMSG, NULL, 0, 0, PM_NOREMOVE))
  { 
    if(::GetMessage(&oMSG, NULL, 0, 0)) 
    {
      //::TranslateMessage(&oMSG);
      ::DispatchMessage(&oMSG);
    }
    else
    {
      break;
    }
  }
}
