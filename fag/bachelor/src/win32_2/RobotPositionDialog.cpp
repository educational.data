// RobotPositionDialog.cpp : implementation file
//

#include "stdafx.h"
#include "RobotHandler.h"
#include "RobotPositionDialog.h"
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern CRobotHandlerApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CRobotPositionDialog dialog


CRobotPositionDialog::CRobotPositionDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CRobotPositionDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRobotPositionDialog)
	m_ed_pitch = 0.0;
	m_ed_roll = 0.0;
	m_ed_x = 0.0;
	m_ed_y = 0.0;
	m_ed_yaw = 0.0;
	m_ed_z = 0.0;
	m_ed_speed = 0.0;
	m_ed_grip = 0.0;
	m_msg = _T("");
	m_trackMsg = _T("");
	//}}AFX_DATA_INIT
	connected = false;
}


void CRobotPositionDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRobotPositionDialog)
	DDX_Control(pDX, IDC_BTN_ADD, m_spinner);
	DDX_Control(pDX, IDC_ED_GRIP, m_Ed_grip);
	DDX_Control(pDX, IDC_ED_SPEED, m_Ed_speed);
	DDX_Control(pDX, IDC_ED_Z, m_Ed_z);
	DDX_Control(pDX, IDC_ED_YAW, m_Ed_yaw);
	DDX_Control(pDX, IDC_ED_Y, m_Ed_y);
	DDX_Control(pDX, IDC_ED_X, m_Ed_x);
	DDX_Control(pDX, IDC_ED_ROLL, m_Ed_roll);
	DDX_Control(pDX, IDC_ED_PITCH, m_Ed_pitch);
	DDX_Control(pDX, IDC_ST_POS8, m_a9);
	DDX_Control(pDX, IDC_ST_POS9, m_a8);
	DDX_Control(pDX, IDC_ST_POS7, m_a7);
	DDX_Control(pDX, IDC_ST_POS6, m_a6);
	DDX_Control(pDX, IDC_ST_POS5, m_a5);
	DDX_Control(pDX, IDC_ST_POS3, m_a4);
	DDX_Control(pDX, IDC_ST_POS2, m_a3);
	DDX_Control(pDX, IDC_ST_POS1, m_a2);
	DDX_Control(pDX, IDC_ST_POS0, m_a1);
	DDX_Control(pDX, IDC_BTN_SAVE, m_btn_save);
	DDX_Control(pDX, IDC_BTN_PLAY, m_btn_play);
	DDX_Control(pDX, IDC_BTN_NEW, m_btn_new);
	DDX_Control(pDX, IDC_BTN_MOVE, m_btn_move);
	DDX_Control(pDX, IDC_BTN_LOAD, m_btn_load);
	DDX_Control(pDX, IDC_BTN_CONNECT, m_btn_connect);
	DDX_Control(pDX, IDC_SL_Z, m_sl_z);
	DDX_Control(pDX, IDC_SL_YAW, m_sl_yaw);
	DDX_Control(pDX, IDC_SL_Y, m_sl_y);
	DDX_Control(pDX, IDC_SL_X, m_sl_x);
	DDX_Control(pDX, IDC_SL_ROLL, m_sl_roll);
	DDX_Control(pDX, IDC_SL_PITCH, m_sl_pitch);
	DDX_Text(pDX, IDC_ED_PITCH, m_ed_pitch);
	DDX_Text(pDX, IDC_ED_ROLL, m_ed_roll);
	DDX_Text(pDX, IDC_ED_X, m_ed_x);
	DDX_Text(pDX, IDC_ED_Y, m_ed_y);
	DDX_Text(pDX, IDC_ED_YAW, m_ed_yaw);
	DDX_Text(pDX, IDC_ED_Z, m_ed_z);
	DDX_Control(pDX, IDC_SL_SPEED, m_sl_speed);
	DDX_Control(pDX, IDC_SL_GRIP, m_sl_grip);
	DDX_Text(pDX, IDC_ED_SPEED, m_ed_speed);
	DDX_Text(pDX, IDC_ED_GRIP, m_ed_grip);
	DDX_Text(pDX, IDC_MSG, m_msg);
	DDX_Text(pDX, IDC_TRACK_TEXT, m_trackMsg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRobotPositionDialog, CDialog)
	//{{AFX_MSG_MAP(CRobotPositionDialog)
	ON_BN_CLICKED(IDC_BTN_CONNECT, OnBtnConnect)
	ON_BN_CLICKED(IDC_BTN_LOAD, OnBtnLoad)
	ON_BN_CLICKED(IDC_BTN_MOVE, OnBtnMove)
	ON_BN_CLICKED(IDC_BTN_NEW, OnBtnNew)
	ON_BN_CLICKED(IDC_BTN_PLAY, OnBtnPlay)
	ON_BN_CLICKED(IDC_BTN_SAVE, OnBtnSave)
	ON_EN_CHANGE(IDC_ED_PITCH, OnChangeEd)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_PITCH, OnReleasedSlider)
	ON_BN_CLICKED(IDC_BTN_ADD, OnBtnAdd)
	ON_BN_CLICKED(IDC_BTN_DELETE, OnBtnDelete)
	ON_EN_CHANGE(IDC_ED_ROLL, OnChangeEd)
	ON_EN_CHANGE(IDC_ED_X, OnChangeEd)
	ON_EN_CHANGE(IDC_ED_Y, OnChangeEd)
	ON_EN_CHANGE(IDC_ED_YAW, OnChangeEd)
	ON_EN_CHANGE(IDC_ED_Z, OnChangeEd)
	ON_EN_CHANGE(IDC_ED_SPEED, OnChangeEd)
	ON_EN_CHANGE(IDC_ED_GRIP, OnChangeEd)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_ROLL, OnReleasedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_X, OnReleasedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_Y, OnReleasedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_YAW, OnReleasedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_Z, OnReleasedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_GRIP, OnReleasedSlider)
	ON_NOTIFY(NM_RELEASEDCAPTURE, IDC_SL_SPEED, OnReleasedSlider)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPIN_POS, OnSpinPos)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRobotPositionDialog message handlers

void CRobotPositionDialog::OnBtnConnect() 
{
	// TODO: Add your control notification handler code here
	setProcess(true);
	if(!connected) {
		/*try {
			setProcess(true);
			m_msg = "Connecting to robot...";
			UpdateData(false);
			m_doc.robot.connect();
			connected = true;
			location loc = m_doc.robot.getPosition();
			m_ed_x = loc.x;
			m_ed_y = loc.y;
			m_ed_z = loc.z;
			m_ed_yaw = loc.yaw;
			m_ed_pitch = loc.pitch;
			m_ed_roll = loc.roll;
			m_ed_speed = 25;
			m_ed_grip = loc.grip;
			UpdateData(false);
			OnChangeEd();

		} catch(const std::exception &ex) {
			setProcess(false);
			AfxMessageBox(ex.what(),MB_OK | MB_APPLMODAL | MB_ICONSTOP );
			connected = false;
		}*/
    setProcess(false);
		m_msg = "";
		UpdateData(false);
		
	} 
	else {
		//m_doc.robot.disconnect();
		connected = false;
	}
	setProcess(false);
	update_state();
	
}

void CRobotPositionDialog::OnBtnLoad() 
{
	// TODO: Add your control notification handler code here

	CFileDialog dlg(true,".crs",NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "CRS tracks (*.crs)|*.crs|All files|*.*|");
	dlg.m_ofn.lpstrTitle = "Open robot track";
	

	if(dlg.DoModal() == IDOK)
    positions = CRobotPoint::load(std::cout,dlg.GetPathName().GetBuffer(0));	

	
	updateTrackInfo();
}

void CRobotPositionDialog::OnBtnMove() 
{
	m_msg = "Moving....";
	UpdateData(false);
	setProcess(false);
	try {
		// m_doc.robot.moveTo(currentLocation,m_ed_speed);
	} catch(const exception &ex) {
		AfxMessageBox(ex.what(),MB_OK | MB_APPLMODAL | MB_ICONSTOP );
	}
	setProcess(false);
	m_msg = "";
	UpdateData(false);
	
}

void CRobotPositionDialog::OnBtnNew() 
{
	
	positions.positions.resize(0);
	updateTrackInfo();
}

void CRobotPositionDialog::OnBtnPlay() 
{
	setProcess(true);
	
	for(CRobotPoint::itcpos it = positions.positions.begin();it != positions.positions.end();++it) {
		currentLocation = *it;
		OnChangeEd();
		// m_doc.robot.moveTo(currentLocation,m_ed_speed);
		updateTrackInfo();
	}
	setProcess(false);
	
}

void CRobotPositionDialog::OnBtnSave() 
{
	// TODO: Add your control notification handler code here
	
}

void CRobotPositionDialog::OnChangeEd() 
{
	UpdateData();

	m_sl_pitch.SetPos(180+m_ed_pitch);
	m_sl_yaw.SetPos(180+m_ed_yaw);
	m_sl_roll.SetPos(180+m_ed_roll);
	m_sl_x.SetPos(600+m_ed_x);
	m_sl_y.SetPos(500+m_ed_y);
	m_sl_z.SetPos(500+m_ed_z);
	m_sl_speed.SetPos(m_ed_speed);
	m_sl_grip.SetPos(m_ed_grip);

	currentLocation.x = m_ed_x;
	currentLocation.y = m_ed_y;
	currentLocation.z = m_ed_z;
	currentLocation.yaw = m_ed_yaw;
	currentLocation.pitch = m_ed_pitch;
	currentLocation.roll = m_ed_roll;
	currentLocation.grip = m_ed_grip;
	
}

void CRobotPositionDialog::OnReleasedSlider(NMHDR* pNMHDR, LRESULT* pResult) 
{
	UpdateData();
	m_ed_x = m_sl_x.GetPos()-600;	
	m_ed_y = m_sl_y.GetPos()-500;	
	m_ed_z = m_sl_z.GetPos()-500;
	m_ed_yaw = m_sl_yaw.GetPos()-180;
	m_ed_pitch = m_sl_pitch.GetPos()-180;
	m_ed_roll = m_sl_roll.GetPos()-180;
	m_ed_speed = m_sl_speed.GetPos();
	m_ed_grip = m_sl_grip.GetPos();
	UpdateData(false);

	currentLocation.x = m_ed_x;
	currentLocation.y = m_ed_y;
	currentLocation.z = m_ed_z;
	currentLocation.yaw = m_ed_yaw;
	currentLocation.pitch = m_ed_pitch;
	currentLocation.roll = m_ed_roll;
	currentLocation.grip = m_ed_grip;

	*pResult = 0;
}



BOOL CRobotPositionDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_sl_pitch.SetRangeMax(360);
	m_sl_yaw.SetRangeMax(360);
	m_sl_roll.SetRangeMax(360);
	m_sl_x.SetRangeMax(1200);
	m_sl_y.SetRangeMax(1000);
	m_sl_z.SetRangeMax(1000);
	m_sl_speed.SetRangeMax(100);
	m_sl_grip.SetRangeMax(50);

	m_sl_pitch.SetPos(180);
	m_sl_yaw.SetPos(180);
	m_sl_roll.SetPos(180);
	m_sl_x.SetPos(500);
	m_sl_y.SetPos(500);
	m_sl_z.SetPos(500);
	
	update_state();

	UpdateData(false);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CRobotPositionDialog::update_state()
{
	m_btn_move.EnableWindow(connected);
	m_btn_load.EnableWindow(connected);
	m_btn_new.EnableWindow(connected);
	m_a1.EnableWindow(connected);
	m_a2.EnableWindow(connected);
	m_a3.EnableWindow(connected);
	m_a4.EnableWindow(connected);
	m_a5.EnableWindow(connected);
	m_a6.EnableWindow(connected);
	m_a7.EnableWindow(connected);
	m_a8.EnableWindow(connected);
	m_a9.EnableWindow(connected);
	m_sl_pitch.EnableWindow(connected);
	m_sl_yaw.EnableWindow(connected);
	m_sl_roll.EnableWindow(connected);
	m_sl_x.EnableWindow(connected);
	m_sl_y.EnableWindow(connected);
	m_sl_z.EnableWindow(connected);
	m_sl_grip.EnableWindow(connected);
	m_sl_speed.EnableWindow(connected);

	m_Ed_pitch.EnableWindow(connected);
	m_Ed_yaw.EnableWindow(connected);
	m_Ed_roll.EnableWindow(connected);
	m_Ed_x.EnableWindow(connected);
	m_Ed_y.EnableWindow(connected);
	m_Ed_z.EnableWindow(connected);
	m_Ed_grip.EnableWindow(connected);
	m_Ed_speed.EnableWindow(connected);

	m_btn_connect.SetWindowText(connected ? "Connected!" : "Connect");
}

void CRobotPositionDialog::OnOK() 
{
	//m_doc.robot.disconnect();
	
	CDialog::OnOK();
}

void CRobotPositionDialog::setProcess(bool state)
{
	if(state) {
		EnableWindow(false);
		SetCursor(theApp.LoadStandardCursor(IDC_WAIT));
		m_btn_connect.EnableWindow(false);
	} else {
		SetCursor(theApp.LoadStandardCursor(IDC_ARROW));
		EnableWindow(true);
		m_btn_connect.EnableWindow(true);
	}
	update_state();
}

void CRobotPositionDialog::OnBtnAdd() 
{
	// TODO: Add your control notification handler code here
	
}

void CRobotPositionDialog::OnBtnDelete() 
{
	// TODO: Add your control notification handler code here
	
}

void CRobotPositionDialog::OnSpinPos(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	// TODO: Add your control notification handler code here
	
	*pResult = 0;
}

void CRobotPositionDialog::updateTrackInfo()
{
	if(positions.size() > 0) {
		currentLocation = positions[0];
		currentPos = 0;
		OnChangeEd();
	}
	m_trackMsg.Format("%d of %d points.",currentPos,positions.size());
	UpdateData(false);
}
