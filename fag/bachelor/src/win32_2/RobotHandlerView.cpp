// RobotHandlerView.cpp : implementation of the CRobotHandlerView class
//

#include "stdafx.h"
#include "RobotHandler.h"

#include "RobotHandlerDoc.h"
#include "RobotHandlerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerView

IMPLEMENT_DYNCREATE(CRobotHandlerView, CHtmlView)

BEGIN_MESSAGE_MAP(CRobotHandlerView, CHtmlView)
	//{{AFX_MSG_MAP(CRobotHandlerView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CHtmlView::OnFilePrint)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerView construction/destruction

CRobotHandlerView::CRobotHandlerView()
{
	// TODO: add construction code here

}

CRobotHandlerView::~CRobotHandlerView()
{
}

BOOL CRobotHandlerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CHtmlView::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerView drawing

void CRobotHandlerView::OnDraw(CDC* pDC)
{
	CRobotHandlerDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	// TODO: add draw code for native data here
}

void CRobotHandlerView::OnInitialUpdate()
{
	CHtmlView::OnInitialUpdate();

	// TODO: This code navigates to a popular spot on the web.
	//  change the code to go where you'd like.
	Navigate2(_T("http://www.mip.sdu.dk/~tobibobi"),NULL,NULL);
}

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerView printing


/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerView diagnostics

#ifdef _DEBUG
void CRobotHandlerView::AssertValid() const
{
	CHtmlView::AssertValid();
}

void CRobotHandlerView::Dump(CDumpContext& dc) const
{
	CHtmlView::Dump(dc);
}

CRobotHandlerDoc* CRobotHandlerView::GetDocument() // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CRobotHandlerDoc)));
	return (CRobotHandlerDoc*)m_pDocument;
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerView message handlers
