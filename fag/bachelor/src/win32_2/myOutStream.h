#ifndef __MY_OUT_STREAM_H_INCLUDED__
#define __MY_OUT_STREAM_H_INCLUDED__

#include <iostream>
#include <streambuf>
#include <string>
#include "../sig.h"

class MyOutStream : public std::basic_streambuf<char>
{
public:
  sig::signal1<int,std::string> on_message;

  MyOutStream(std::ostream &stream) : m_stream(stream)
  {
    m_old_buf = stream.rdbuf();
    stream.rdbuf(this);
  }
  ~MyOutStream()
  {
    // output anything that is left
    if (!m_string.empty())
      on_message(m_string);

    m_stream.rdbuf(m_old_buf);
  }

protected:
  virtual int_type overflow(int_type v)
  {
    if (v == '\n') {
	    on_message(m_string + "\r\n");
	    m_string = "";
    }
    else
      m_string+=v;
    
    return v;
  }

  virtual std::streamsize xsputn(const char *p, std::streamsize n)
  {
    m_string.append(p, p + n);

    int pos = 0;
    while (pos != std::string::npos) {
	    pos = m_string.find('\n');
	    if (pos != std::string::npos) {
	      std::string tmp(m_string.begin(), m_string.begin() + pos);
	      on_message(tmp + "\r\n");
	      m_string.erase(m_string.begin(), m_string.begin() + pos + 1);
      }
    }

    return n;
  }

private:

  std::ostream &m_stream;
  std::streambuf *m_old_buf;
  std::string m_string;
};

#endif
