// CImageView.cpp : implementation file
//

#include "stdafx.h"
#include "RobotHandler.h"
#include "RobotHandlerDoc.h"
#include "CImageView.h"
#include "RobotPositionDialog.h"
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCImageView

IMPLEMENT_DYNCREATE(CCImageView, CFormView)

CCImageView::CCImageView()
 : 
  CFormView(CCImageView::IDD), 
  mImgFront(640,480,8,0),
  image_gui(&mImgFront)
{
	ZoomFactor = 1.0f;
  handler = getCalibrateHandler();
  handler->on_image_update.connect(sig::slot(this,&CCImageView::forceRedraw));
}

CCImageView::~CCImageView()
{

}


BEGIN_MESSAGE_MAP(CCImageView, CFormView)
	//{{AFX_MSG_MAP(CCImageView)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CCImageView drawing

void CCImageView::OnDraw(CDC* pDC)
{
	CBrush black_brush;black_brush.CreateSolidBrush(0);
	
	if(mImgFront.GetWidth() > 0) {
		// calculate the position of the image

		CImage &pImg = mImgFront;
		CRect rc;
		m_pict.GetWindowRect(&rc);
		ScreenToClient(&rc);
		m_pict.MoveWindow(rc.left,rc.top,pImg.GetWidth()+4,pImg.GetHeight()+4);

		m_pict.GetClientRect(&rc);
		CRgn rgn;rgn.CreateRectRgnIndirect(&rc);
		//pDC->FillRect(&rc,&black_brush);
		CDC *cDC= m_pict.GetDCEx(&rgn,DCX_CACHE);

		image_gui.Show(cDC,true,1,CPoint(0,0));
		
		// cDC->MoveTo(pImg.GetWidth()/2,0);
		// cDC->LineTo(pImg.GetWidth()/2,pImg.GetHeight());
		image_gui.SetImage(&(mImgFront));
		m_pict.GetWindowRect(&rc);
		ScreenToClient(&rc);

		SetScrollSizes(MM_TEXT,CSize((int)(rc.right),
			(int)(rc.bottom)));
		// make the window the size of the main dialog
		//GetParentFrame()->RecalcLayout();
		//ResizeParentToFit( /*FALSE*/ );
		//GetDocument()->image_gui.Show(pDC,true,ZoomFactor,pt);

		ReleaseDC(cDC);
	} else {
		CRect rc;
		m_pict.GetClientRect(&rc);
		CRgn rgn;rgn.CreateRectRgnIndirect(&rc);
		//pDC->FillRect(&rc,&black_brush);
		CDC *cDC= m_pict.GetDCEx(&rgn,DCX_CACHE);
		cDC->FillRect(&rc,&black_brush);
		ReleaseDC(cDC);
	}
	CFormView::OnDraw(pDC);
	/* CRect rc;

	m_cameraFrame.GetWindowRect(&rc);
	CString title;
	m_cameraFrame.GetWindowText(title);
	drawTopBorder(*pDC,rc,title); */
	
}



/////////////////////////////////////////////////////////////////////////////
// CCImageView message handlers

BOOL CCImageView::PreCreateWindow(CREATESTRUCT& cs) 
{
	// TODO: Add your specialized code here and/or call the base class

	cs.dwExStyle |= WS_EX_RIGHTSCROLLBAR;
	cs.style |= WS_VSCROLL;

	return CFormView::PreCreateWindow(cs);
}



void CCImageView::OnInitialUpdate() 
{
	CFormView::OnInitialUpdate();
	CRect rc;
	m_pict.GetWindowRect(&rc);
	ScreenToClient(&rc);
	m_pict.MoveWindow(rc.left,rc.top,640,480);

  
}

void CCImageView::DoDataExchange(CDataExchange* pDX) 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCImageView)
	DDX_Control(pDX, IDC_PICTURE, m_pict);
	//}}AFX_DATA_MAP
}



void CCImageView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
	//bool camInit = GetDocument()->camera.isInitialized();
	//m_cameraAcquire.EnableWindow(camInit);
	//Invalidate(false);
	DoEvents();
}



void CCImageView::OnBtnSetupRobot() 
{
	// TODO: Add your control notification handler code here
	//CRobotPositionDialog dlg(*(GetDocument()));
	//dlg.DoModal();
}








extern CRobotHandlerApp theApp;

void CCImageView::blockWindow(bool state)
{
	if(state) {
		EnableWindow(false);
		SetCursor(theApp.LoadStandardCursor(IDC_WAIT));
	} else {
		SetCursor(theApp.LoadStandardCursor(IDC_ARROW));
		EnableWindow(true);
	}
}

void CCImageView::DoEvents()
{
	MSG oMSG;
  while(::PeekMessage(&oMSG, NULL, 0, 0, PM_NOREMOVE))
  { 
    if(::GetMessage(&oMSG, NULL, 0, 0)) 
    {
      //::TranslateMessage(&oMSG);
      ::DispatchMessage(&oMSG);
    }
    else
    {
      break;
    }
  }
}



int CCImageView::forceRedraw()
{
  mImgFront = getCalibrateHandler()->getImage();
  Invalidate();
  //InvalidateRect(CRect(0,0,mImgFront.GetWidth(),mImgFront.GetHeight()));
  DoEvents();
  return 0;
}
