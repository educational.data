#if !defined(AFX_ROBOTPOSITIONDIALOG_H__4E53DB26_936D_4FD2_ADAB_C4121F33BD40__INCLUDED_)
#define AFX_ROBOTPOSITIONDIALOG_H__4E53DB26_936D_4FD2_ADAB_C4121F33BD40__INCLUDED_

//#include "RobotHandlerDoc.h"
#include "../robot/location.h"
#include "../ROBOT/RobotPoint.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RobotPositionDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRobotPositionDialog dialog

class CRobotPositionDialog : public CDialog
{
// Construction
//	CRobotHandlerDoc & m_doc;
public:
	void updateTrackInfo();
	void setProcess(bool state);
	CRobotPositionDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRobotPositionDialog)
	enum { IDD = IDD_DIALOG_ROBOT };
	CButton	m_spinner;
	CEdit	m_Ed_grip;
	CEdit	m_Ed_speed;
	CEdit	m_Ed_z;
	CEdit	m_Ed_yaw;
	CEdit	m_Ed_y;
	CEdit	m_Ed_x;
	CEdit	m_Ed_roll;
	CEdit	m_Ed_pitch;
	CStatic	m_a9;
	CStatic	m_a8;
	CButton	m_a7;
	CStatic	m_a6;
	CStatic	m_a5;
	CStatic	m_a4;
	CStatic	m_a3;
	CStatic	m_a2;
	CStatic	m_a1;
	CButton	m_btn_save;
	CButton	m_btn_play;
	CButton	m_btn_new;
	CButton	m_btn_move;
	CButton	m_btn_load;
	CButton	m_btn_connect;
	CSliderCtrl	m_sl_z;
	CSliderCtrl	m_sl_yaw;
	CSliderCtrl	m_sl_y;
	CSliderCtrl	m_sl_x;
	CSliderCtrl	m_sl_roll;
	CSliderCtrl	m_sl_pitch;
	double	m_ed_pitch;
	double	m_ed_roll;
	double	m_ed_x;
	double	m_ed_y;
	double	m_ed_yaw;
	double	m_ed_z;
	CSliderCtrl	m_sl_speed;
	CSliderCtrl	m_sl_grip;
	double	m_ed_speed;
	double	m_ed_grip;
	CString	m_msg;
	CString	m_trackMsg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRobotPositionDialog)
	public:
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CRobotPositionDialog)
	afx_msg void OnBtnConnect();
	afx_msg void OnBtnLoad();
	afx_msg void OnBtnMove();
	afx_msg void OnBtnNew();
	afx_msg void OnBtnPlay();
	afx_msg void OnBtnSave();
	afx_msg void OnChangeEd();
	afx_msg void OnReleasedSlider(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnBtnAdd();
	afx_msg void OnBtnDelete();
	afx_msg void OnSpinPos(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	int currentPos;
	CRobotPoint positions;
	location currentLocation;
	void update_state();
	bool connected;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROBOTPOSITIONDIALOG_H__4E53DB26_936D_4FD2_ADAB_C4121F33BD40__INCLUDED_)
