//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by RobotHandler.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_CAMERASETTINGS_TMPL         102
#define IDD_DIALOGBAR                   103
#define IDR_MAINFRAME                   128
#define IDR_ROBOTHTYPE                  129
#define IDD_LEFTVIEW                    130
#define IDD_DIALOG_ROBOT                132
#define IDD_DLG_CAMERA_SETUP            134
#define IDR_MNU_POPUP                   139
#define IDD_DLG_PROGRESS                144
#define IDC_BUTTON_ACQUIRE_IMAGE        1000
#define IDC_SLIDER1                     1001
#define IDC_SL_X                        1001
#define IDC_SLIDER2                     1002
#define IDC_SL_Y                        1002
#define IDC_SL_Z                        1003
#define IDC_COMBO1                      1004
#define IDC_CMB_CAMERAS                 1004
#define IDC_SL_YAW                      1004
#define IDC_CMB_VISION                  1004
#define IDC_SL_PITCH                    1005
#define IDC_CMB_TOOL                    1005
#define IDC_SL_ROLL                     1006
#define IDC_SL_GRIP                     1007
#define IDC_PICTURE                     1008
#define IDC_SL_SPEED                    1008
#define IDC_BTN_SETUP_CAMERA            1026
#define IDC_BTN_SETUP_ROBOT             1027
#define IDC_ED_X                        1028
#define IDC_BTN_FIND_POINT              1028
#define IDC_ED_Y                        1029
#define IDC_ED_Z                        1030
#define IDC_ED_YAW                      1031
#define IDC_ED_PITCH                    1032
#define IDC_ED_ROLL                     1033
#define IDC_BTN_MOVE                    1034
#define IDC_BTN_NEW                     1035
#define IDC_BTN_PLAY                    1036
#define IDC_BTN_LOAD                    1037
#define IDC_BTN_SAVE                    1038
#define IDC_BTN_CONNECT                 1039
#define IDC_ST_POS0                     1040
#define IDC_ST_POS1                     1041
#define IDC_ST_POS2                     1042
#define IDC_ST_POS3                     1043
#define IDC_ST_POS5                     1044
#define IDC_ST_POS6                     1045
#define IDC_ST_POS7                     1046
#define IDC_ST_POS8                     1047
#define IDC_ED_GRIP                     1048
#define IDC_ST_POS9                     1049
#define IDC_ED_SPEED                    1050
#define IDC_MSG                         1051
#define IDC_BTN_ADD                     1052
#define IDC_BTN_DELETE                  1053
#define IDC_BTN_CALIBRATE               1054
#define IDC_SPIN_POS                    1056
#define IDC_TRACK_TEXT                  1057
#define IDC_CMB_POINTS                  1061
#define IDC_BTN_FIND                    1064
#define IDC_BTN_EXECUTE                 1065
#define IDC_BTN_ACQUIRE                 1066
#define IDC_PROG                        1067
#define IDC_BTN_ABORT                   1068
#define IDC_CURRENT                     1069
#define ID_FILE_OPENROBOTPATH           32771
#define ID_FILE_OPEN_PICTURE            32772
#define ID_OPENROBOTPATH                32774
#define ID_OPEN2D3DPOINTSET             32778

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        145
#define _APS_NEXT_COMMAND_VALUE         32779
#define _APS_NEXT_CONTROL_VALUE         1070
#define _APS_NEXT_SYMED_VALUE           103
#endif
#endif
