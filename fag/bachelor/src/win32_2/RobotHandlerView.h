// RobotHandlerView.h : interface of the CRobotHandlerView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROBOTHANDLERVIEW_H__5B3220C9_808D_4C0F_A57A_0E0DBB4C6AD9__INCLUDED_)
#define AFX_ROBOTHANDLERVIEW_H__5B3220C9_808D_4C0F_A57A_0E0DBB4C6AD9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


class CRobotHandlerView : public CHtmlView
{
protected: // create from serialization only
	CRobotHandlerView();
	DECLARE_DYNCREATE(CRobotHandlerView)

// Attributes
public:
	CRobotHandlerDoc* GetDocument();

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRobotHandlerView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual void OnInitialUpdate(); // called first time after construct
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CRobotHandlerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRobotHandlerView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

#ifndef _DEBUG  // debug version in RobotHandlerView.cpp
inline CRobotHandlerDoc* CRobotHandlerView::GetDocument()
   { return (CRobotHandlerDoc*)m_pDocument; }
#endif

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROBOTHANDLERVIEW_H__5B3220C9_808D_4C0F_A57A_0E0DBB4C6AD9__INCLUDED_)
