// MethodBar.cpp : implementation file
//

#include "stdafx.h"
#include "RobotHandler.h"
#include "MethodBar.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// MethodBar dialog


MethodBar::MethodBar(CWnd* pParent /*=NULL*/)
	: CDialog(MethodBar::IDD, pParent)
{
	//{{AFX_DATA_INIT(MethodBar)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void MethodBar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(MethodBar)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(MethodBar, CDialog)
	//{{AFX_MSG_MAP(MethodBar)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// MethodBar message handlers
