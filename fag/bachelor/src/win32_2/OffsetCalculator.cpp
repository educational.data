// OffsetCalculator.cpp: implementation of the COffsetCalculator class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "RobotHandler.h"
#include "OffsetCalculator.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

COffsetCalculator::~COffsetCalculator()
{

}

COffsetCalculator::COffsetCalculator(
    const CCameraModel &source,
    const CPoint3D<int> &StartGuess, 
    const CPoint3D<float> &CenterPointOfTranspose, 
    const int HalfWidthOfSearchCube,
    const float step,
    const vector<CPoint2D<float> > &Points,
    const vector<CPoint3D<float> > &pose
    ) : destination(source) {
    	 vector<videndo::CPose<float> > Poses(pose.size());
       CPoint3D<float> P0 = CenterPointOfTranspose;//Last Calibration 3Dpoint
       CPoint2D<float> P2(0,0);
       CPoint3D<float> P3(0,0,0);
       CCorresponding3D2DPoints C32;

       cout << "Start: " << StartGuess << endl;

       for(int i=0; i<pose.size(); i++) {
         Poses[i].SetMatrix(- ipl::DegreeToRad(pose[i].GetX()),
		               ipl::DegreeToRad(pose[i].GetY()),
					   ipl::DegreeToRad(pose[i].GetZ()),
					   P0,0);
         cout << Points[i] << endl;
         C32.AddPointSetAndId (P3,Points[i],i);
       }
       ModifyOffset(source,C32,Poses,StartGuess,HalfWidthOfSearchCube,step,offset,destination,residualFejl);
}


void COffsetCalculator::ModifyOffset(
   const CCameraModel &Source, 
   const CCorresponding3D2DPoints &CorrThreeToTwooDim, 
   const vector<CPose<float> > Poses,
	 const CPoint3D<int> OffsetStartGuess, 
   const int HalfWidthOfSearchCube,
   const float Step,
   CPoint3D<float> &Offset,
	 CCameraModel &Destination,
   float &ResidualError) 
{

  videndo::CCameraModel Camera = Source;
	CPoint3D<float> P3,P30;
	CPoint3D<float>P0;
	P0=Poses[0].GetTranslationalPose();

  CCorresponding3D2DPoints C32 = CorrThreeToTwooDim;
  
	float xx,yy,zz,x0,y0,z0,min=1000000;

	float d=HalfWidthOfSearchCube;
	/**
	 * Afs�g hele 3D rummet for det punkt med mindst fejl
	 */
  for (xx=OffsetStartGuess.GetX()-d;xx<OffsetStartGuess.GetX()+d;xx+=Step) {
    for (yy=OffsetStartGuess.GetY()-d;yy<OffsetStartGuess.GetY()+d;yy+=Step) {
	    for (zz=OffsetStartGuess.GetZ()-d;zz<OffsetStartGuess.GetZ()+d;zz+=Step)
	    {
		    Offset.Set(xx,yy,zz);
        P3=Poses[0].TransformToGlobal(Offset);
		    P30=P3;
		    C32.SetPoint3D(P3,0);
   		  P3=Poses[1].TransformToGlobal(Offset);
		    C32.SetPoint3D(P3,1);
		    P3=Poses[2].TransformToGlobal(Offset);
		    C32.SetPoint3D(P3,2);
		    P3=Poses[3].TransformToGlobal(Offset);
		    C32.SetPoint3D(P3,3);
		    P3=Poses[4].TransformToGlobal(Offset);
		    C32.SetPoint3D(P3,4);
		    P3=Poses[5].TransformToGlobal(Offset);
		    C32.SetPoint3D(P3,5);
		    P3=Poses[6].TransformToGlobal(Offset);
		    C32.SetPoint3D(P3,6);
		    Camera=Source;
		    Camera.MatrixToParameters();
		    Camera.m_Par.dx += (P30-P0).GetX();
		    Camera.m_Par.dy += (P30-P0).GetY();
		    Camera.m_Par.dz += (P30-P0).GetZ();
		    Camera.ParametersToMatrix();

		    float Error= DeriveError(Camera,C32); 
        //cout << "Error: " << Error << endl;
		    if (Error<min){ 
          min=Error; 
          x0=xx;
          y0=yy;
          z0=zz; 
          //cout << "Error: " << min << " at: " << x0 << "," << y0 << "," << z0 << endl;
          //cout << "P30: " << (P30) << endl;
          //cout << "P0:  " << (P0) << endl;
        }
        // cout << "at: " << xx << "," << yy << "," << zz << endl;
	    }
    }
  }
	Camera=Source;
	Offset.Set(x0,y0,z0);
	P3=Poses[0].TransformToGlobal(Offset);
	P30=P3;
	C32.SetPoint3D(P3,0);	
	P3=Poses[1].TransformToGlobal(Offset);
	C32.SetPoint3D(P3,1);
	P3=Poses[2].TransformToGlobal(Offset);
	C32.SetPoint3D(P3,2);
	P3=Poses[3].TransformToGlobal(Offset);
	C32.SetPoint3D(P3,3);
	P3=Poses[4].TransformToGlobal(Offset);
	C32.SetPoint3D(P3,4);
	P3=Poses[5].TransformToGlobal(Offset);
	C32.SetPoint3D(P3,5);
	P3=Poses[6].TransformToGlobal(Offset);
	C32.SetPoint3D(P3,6);
	Camera.MatrixToParameters();
	Camera.m_Par.dx+=(P30-P0).GetX();
	Camera.m_Par.dy+=(P30-P0).GetY();	 
	Camera.m_Par.dz+=(P30-P0).GetZ();
	Camera.ParametersToMatrix();
	ResidualError = DeriveError(Camera,C32);
	Destination = Camera;
  cout << "P30: " << (P30) << endl;
  cout << "P0:  " << (P0) << endl;

}


float COffsetCalculator::DeriveError(
    const CCameraModel &Cam, 
    const CCorresponding3D2DPoints &C32) 
{
  int i,n=C32.GetTotalPointSets();
	float d=0,x,y;//,xx,yy,zz;
	int count=0;
	CPoint2D<float> P2;

	for(i=0;i<n;i++)
	{
		if (i!=6)
		{
		  Cam.Calc3DTo2DRad(C32.GetPoint3D(i),P2);
		  x=P2.GetX()-C32.GetPoint2D(i).GetX();
		  y=P2.GetY()-C32.GetPoint2D(i).GetY();
		  d+=x*x + y*y;
		  count++;
		}
	}
	return sqrt(d/count);
}
