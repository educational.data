#if !defined(AFX_CIMAGEVIEW_H__F9BDD398_7100_4AE5_AA76_4199EA7C3DCB__INCLUDED_)
#define AFX_CIMAGEVIEW_H__F9BDD398_7100_4AE5_AA76_4199EA7C3DCB__INCLUDED_

#include "VisualGUI.h"	// Added by ClassView
#include "../ihandler.h"
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CImageView.h : header file
//
using namespace ipl;
/////////////////////////////////////////////////////////////////////////////
// CCImageView view

class CCImageView : public CFormView
{
protected:
	CCImageView();           // protected constructor used by dynamic creation
	DECLARE_DYNCREATE(CCImageView)

		// Form Data
public:
	//{{AFX_DATA(CCImageView)
	enum { IDD = IDD_LEFTVIEW };
	//CComboBox	m_cmbCameras;
	CStatic	m_pict;
	//}}AFX_DATA

// Attributes
public:
  CImage mImgFront;
// Operations
public:
	ICalibrateHandler * handler;
	CVisualGUI image_gui;
	int forceRedraw();
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCImageView)
	public:
	virtual void OnInitialUpdate();
	protected:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void DoDataExchange(CDataExchange* pDX);
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	//}}AFX_VIRTUAL

// Implementation
protected:
	void blockWindow(bool state=true);
	virtual ~CCImageView();
	void OnDraw(CDC* pDC);
#ifdef _DEBUG
#endif

	// Generated message map functions
protected:
	//{{AFX_MSG(CCImageView)
	afx_msg void OnBtnSetupRobot();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
private:

	void DoEvents();
	float ZoomFactor;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CIMAGEVIEW_H__F9BDD398_7100_4AE5_AA76_4199EA7C3DCB__INCLUDED_)
