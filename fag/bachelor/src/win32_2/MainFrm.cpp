// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "RobotHandler.h"

#include "MainFrm.h"
#include "LeftView.h"
#include "CImageView.h"
#include "CameraSettings.h"
#include "RobotHandlerView.h"
#include "ProgressDialog.h"
#include <iostream>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_CBN_SELCHANGE(IDC_CMB_POINTS, OnSelchangeCmbPoints)
	ON_BN_CLICKED(IDC_BTN_FIND, onFindPunkt)
	ON_BN_CLICKED(IDC_BTN_EXECUTE, onExecuteCalibration)
	ON_CBN_SELCHANGE(IDC_CMB_VISION, onChangeVisionMethod)
	ON_BN_CLICKED(IDC_BTN_ACQUIRE, onAcquireImage)
	ON_COMMAND(ID_FILE_OPEN_PICTURE, OnFileOpenPicture)
	ON_COMMAND(ID_FILE_OPEN, onTBOpen)
	ON_COMMAND(ID_OPENROBOTPATH, onOpenRobotPath)
	ON_COMMAND(ID_OPEN2D3DPOINTSET, onOpen2D3DPointset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	// TODO: add member initialization code here
	
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{

	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
	if (!m_wndToolBar.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
		| CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}

  //m_wndMethodBar.LoadToolBar(IDR_MAINFRAME);

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	// TODO: Delete these three lines if you don't want the toolbar to
	//  be dockable
  EnableDocking(CBRS_ALIGN_ANY);

	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	

  m_wndMethodBar.Create(this,IDD_DIALOGBAR,CBRS_GRIPPER | CBRS_TOP|CBRS_TOOLTIPS|CBRS_FLYBY,IDD_DIALOGBAR);
  m_wndMethodBar.EnableDocking(CBRS_ALIGN_TOP|CBRS_ALIGN_BOTTOM);

  
  DockControlBar(&m_wndToolBar);
  DockControlBarLeftOf((CToolBar *) &m_wndMethodBar, &m_wndToolBar);

  getCalibrateHandler()->setMethod(0);
  ((CComboBox *)m_wndMethodBar.GetDlgItem( IDC_CMB_VISION ))->SetCurSel(0);
  ((CComboBox *)m_wndMethodBar.GetDlgItem( IDC_CMB_POINTS ))->SetCurSel(0);

  m_wndMethodBar.GetDlgItem( IDC_BTN_ACQUIRE )->EnableWindow(getCalibrateHandler()->hasCamera());
    
	return 0;
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext)
{
	// CFrameWnd::OnCreateClient(lpcs,pContext);
	 // create splitter window
	if (!m_wndSplitter.CreateStatic(this, 1, 2))
		return FALSE;


	if (!m_wndSplitter.CreateView(0, 1, RUNTIME_CLASS(CCImageView), CSize(680, 500), pContext) ||
		!m_wndSplitter.CreateView(0, 0, RUNTIME_CLASS(CLeftView), CSize(400, 500), pContext))
	{
		m_wndSplitter.DestroyWindow();
		return FALSE;
	}

	/*m_wndSplitter.SetRowInfo(0,500,500);
	m_wndSplitter.SetRowInfo(1,100,100);*/
	//m_wndSplitter.RecalcLayout();

	return TRUE;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	cs.style |= WS_MAXIMIZE;

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnSelchangeCmbPoints() 
{
	// TODO: Add your control notification handler code here

  CComboBox *m_Points = (CComboBox *)m_wndMethodBar.GetDlgItem(IDC_CMB_POINTS);

  if(m_Points == NULL) {
     cerr << "No access to control" << endl;
     return;
  }

  switch(m_Points->GetCurSel()) {
  case 0:
    cout << "To be implemented!!!!" << endl;
    break;
  case 1:
    getCalibrateHandler()->randomizePoints(6);
    break;
  case 2:
    getCalibrateHandler()->randomizePoints(7);
    break;
  case 3:
    getCalibrateHandler()->randomizePoints(8);
    break;
  case 4:
    getCalibrateHandler()->randomizePoints(9);
    break;
  case 5:
    getCalibrateHandler()->randomizePoints(10);
    break;
  case 6:
    getCalibrateHandler()->randomizePoints(15);
    break;
  case 7:
    getCalibrateHandler()->randomizePoints(20);
    break;
  case 8:
    getCalibrateHandler()->randomizePoints(25);
    break;
  case 9:
    getCalibrateHandler()->randomizePoints(30);
    break;
  case 10:
    getCalibrateHandler()->randomizePoints(50);
    break;
  case 11:
    getCalibrateHandler()->randomizePoints(100);
    break;
  default: 
    cout << "Not able to randomize point. Does the data model fit your implementation?" << endl;
    cout << "Current selection is: " << m_Points->GetCurSel() << endl;
    break;
  }	
	
}

void CMainFrame::onFindPunkt() 
{
	getCalibrateHandler()->findPunkt();
}

void CMainFrame::onExecuteCalibration() 
{
  setProgress(true);
	CProgressDialog dlg;
  dlg.perform();
  setProgress(false);
  
}

void CMainFrame::onChangeVisionMethod() 
{
	// TODO: Add your control notification handler code here

  CComboBox *m_Vision = (CComboBox *)m_wndMethodBar.GetDlgItem(IDC_CMB_VISION);

  if(m_Vision == NULL) {
     cerr << "No access to control" << endl;
     return;
  }

  getCalibrateHandler()->setMethod(m_Vision->GetCurSel());
}

void CMainFrame::DockControlBarLeftOf(CToolBar *Bar, CToolBar *LeftOf)
{
  CRect rect;
	DWORD dw;
	UINT n;
	
	// get MFC to adjust the dimensions of all docked ToolBars
	// so that GetWindowRect will be accurate
	RecalcLayout(TRUE);
	
	LeftOf->GetWindowRect(&rect);
	rect.OffsetRect(1,0);
	dw=LeftOf->GetBarStyle();
	n = 0;
	n = (dw&CBRS_ALIGN_TOP) ? AFX_IDW_DOCKBAR_TOP : n;
	n = (dw&CBRS_ALIGN_BOTTOM && n==0) ? AFX_IDW_DOCKBAR_BOTTOM : n;
	n = (dw&CBRS_ALIGN_LEFT && n==0) ? AFX_IDW_DOCKBAR_LEFT : n;
	n = (dw&CBRS_ALIGN_RIGHT && n==0) ? AFX_IDW_DOCKBAR_RIGHT : n;
	
	// When we take the default parameters on rect, DockControlBar will dock
	// each Toolbar on a seperate line. By calculating a rectangle, we
	// are simulating a Toolbar being dragged to that location and docked.
	DockControlBar(Bar,n,&rect);
}

void CMainFrame::onAcquireImage() 
{
	getCalibrateHandler()->acquireImage();
	
}

void CMainFrame::OnFileOpenPicture() 
{
	// TODO: Add your command handler code here
  CFileDialog dlg(true,NULL,NULL,OFN_ENABLESIZING | OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_LONGNAMES|
    OFN_PATHMUSTEXIST|OFN_HIDEREADONLY,"bmp image (*.bmp)|*.bmp|jpeg image (*.jpeg;*.jpg)|*.jpeg;*.jpg|png internet image (*.png)|*.png||",NULL);

  dlg.m_ofn.lpstrTitle = "Open image for position detection";

  if(dlg.DoModal() == IDOK) {
    getCalibrateHandler()->loadImage(dlg.m_ofn.lpstrFile);
  } else cout << "Aborted image load" << endl;
	
}

void CMainFrame::onTBOpen() 
{
	CMenu men;
  men.LoadMenu(IDR_MNU_POPUP);
  CToolBar &bar = m_wndToolBar;
  CRect rc,wrc;
  

  bar.GetWindowRect(&wrc);
  bar.GetItemRect(0,&rc);
  int x =wrc.left + rc.left+5;
  int y =wrc.top + rc.bottom;
  CMenu &mn = *men.GetSubMenu(0);
  mn.TrackPopupMenu(TPM_LEFTALIGN,x,y,this);
  men.DestroyMenu();
	
}

void CMainFrame::onOpenRobotPath() 
{
	// TODO: Add your command handler code here
	CFileDialog dlg(true,NULL,NULL,OFN_ENABLESIZING | OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_LONGNAMES|
    OFN_PATHMUSTEXIST|OFN_HIDEREADONLY,"CRSRobot path file (*.crs)|*.crs|All files (*.*)|*.*||",NULL);

  dlg.m_ofn.lpstrTitle = "Open robot path";

  if(dlg.DoModal() == IDOK) {
    getCalibrateHandler()->loadRobotPath(dlg.m_ofn.lpstrFile);
  } else cout << "Aborted path load" << endl;
}

void CMainFrame::onOpen2D3DPointset() 
{
	CFileDialog dlg(true,NULL,NULL,OFN_ENABLESIZING | OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_LONGNAMES|
    OFN_PATHMUSTEXIST|OFN_HIDEREADONLY,"PointSet file (*.3d2d)|*.3d2d|All files (*.*)|*.*||",NULL);

  dlg.m_ofn.lpstrTitle = "Open 3D <-> 2D pointset";

  if(dlg.DoModal() == IDOK) {
    getCalibrateHandler()->loadScenario(dlg.m_ofn.lpstrFile);
  } else cout << "Aborted path load" << endl;
	
}

void CMainFrame::setProgress(bool state)
{
  if(state) {
		EnableWindow(false);
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_WAIT));
	} else {
		SetCursor(AfxGetApp()->LoadStandardCursor(IDC_ARROW));
		EnableWindow(true);
	}
}
