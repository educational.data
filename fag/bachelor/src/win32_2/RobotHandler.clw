; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CProgressDialog
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "RobotHandler.h"
LastPage=0

ClassCount=10
Class1=CRobotHandlerApp
Class2=CRobotHandlerDoc
Class3=CRobotHandlerView
Class4=CMainFrame

ResourceCount=11
Resource1=IDD_ABOUTBOX
Resource2=IDD_DLG_PROGRESS
Resource3=IDD_ABOUTBOX (English (U.S.))
Resource4=IDR_MNU_POPUP
Class5=CLeftView
Class6=CAboutDlg
Class7=CCImageView
Resource5=IDD_DIALOG_ROBOT
Resource6=IDD_LEFTVIEW
Resource7=IDR_MAINFRAME
Class8=CRobotPositionDialog
Resource8=IDD_DIALOGBAR (English (U.S.))
Class9=MethodBar
Resource9=IDD_DLG_CAMERA_SETUP
Resource10=IDR_CAMERASETTINGS_TMPL
Class10=CProgressDialog
Resource11=IDR_MAINFRAME (English (U.S.))

[CLS:CRobotHandlerApp]
Type=0
HeaderFile=RobotHandler.h
ImplementationFile=RobotHandler.cpp
Filter=N

[CLS:CRobotHandlerDoc]
Type=0
HeaderFile=RobotHandlerDoc.h
ImplementationFile=RobotHandlerDoc.cpp
Filter=N
BaseClass=CDocument
VirtualFilter=DC

[CLS:CRobotHandlerView]
Type=0
HeaderFile=RobotHandlerView.h
ImplementationFile=RobotHandlerView.cpp
Filter=C


[CLS:CMainFrame]
Type=0
HeaderFile=MainFrm.h
ImplementationFile=MainFrm.cpp
Filter=T
BaseClass=CFrameWnd
VirtualFilter=fWC
LastObject=CMainFrame



[CLS:CLeftView]
Type=0
HeaderFile=LeftView.h
ImplementationFile=LeftView.cpp
Filter=T
BaseClass=CEditView
VirtualFilter=VWC
LastObject=CLeftView

[CLS:CAboutDlg]
Type=0
HeaderFile=RobotHandler.cpp
ImplementationFile=RobotHandler.cpp
Filter=D
LastObject=IDOK

[DLG:IDD_ABOUTBOX]
Type=1
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889
Class=CAboutDlg

[MNU:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_OPEN_PICTURE
Command2=ID_OPENROBOTPATH
Command3=ID_OPEN2D3DPOINTSET
Command4=ID_APP_EXIT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_VIEW_TOOLBAR
Command10=ID_VIEW_STATUS_BAR
Command11=ID_APP_ABOUT
CommandCount=11

[ACL:IDR_MAINFRAME]
Type=1
Class=CMainFrame
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_PRINT
Command5=ID_EDIT_UNDO
Command6=ID_EDIT_CUT
Command7=ID_EDIT_COPY
Command8=ID_EDIT_PASTE
Command9=ID_EDIT_UNDO
Command10=ID_EDIT_CUT
Command11=ID_EDIT_COPY
Command12=ID_EDIT_PASTE
CommandCount=14
Command13=ID_NEXT_PANE
Command14=ID_PREV_PANE


[TB:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_FILE_OPEN
Command2=ID_FILE_SAVE
Command3=ID_EDIT_CUT
Command4=ID_EDIT_COPY
Command5=ID_EDIT_PASTE
Command6=ID_FILE_PRINT
Command7=ID_APP_ABOUT
CommandCount=7

[ACL:IDR_MAINFRAME (English (U.S.))]
Type=1
Class=?
Command1=ID_EDIT_COPY
Command2=ID_FILE_NEW
Command3=ID_FILE_OPEN
Command4=ID_FILE_OPENROBOTPATH
Command5=ID_FILE_PRINT
Command6=ID_FILE_SAVE
Command7=ID_EDIT_PASTE
Command8=ID_EDIT_UNDO
Command9=ID_EDIT_CUT
Command10=ID_NEXT_PANE
Command11=ID_PREV_PANE
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_EDIT_CUT
Command15=ID_EDIT_UNDO
CommandCount=15

[DLG:IDD_ABOUTBOX (English (U.S.))]
Type=1
Class=CAboutDlg
ControlCount=4
Control1=IDC_STATIC,static,1342177283
Control2=IDC_STATIC,static,1342308480
Control3=IDC_STATIC,static,1342308352
Control4=IDOK,button,1342373889

[DLG:IDD_LEFTVIEW]
Type=1
Class=CCImageView
ControlCount=1
Control1=IDC_PICTURE,static,1342177298

[MNU:IDR_CAMERASETTINGS_TMPL]
Type=1
Class=?
Command1=ID_FILE_NEW
Command2=ID_FILE_OPEN
Command3=ID_FILE_SAVE
Command4=ID_FILE_SAVE_AS
Command5=ID_FILE_PRINT
Command6=ID_FILE_PRINT_PREVIEW
Command7=ID_FILE_PRINT_SETUP
Command8=ID_FILE_MRU_FILE1
Command9=ID_APP_EXIT
Command10=ID_EDIT_UNDO
Command11=ID_EDIT_CUT
Command12=ID_EDIT_COPY
Command13=ID_EDIT_PASTE
Command14=ID_VIEW_TOOLBAR
Command15=ID_VIEW_STATUS_BAR
Command16=ID_APP_ABOUT
CommandCount=16

[CLS:CCImageView]
Type=0
HeaderFile=CImageView.h
ImplementationFile=CImageView.cpp
BaseClass=CFormView
Filter=C
VirtualFilter=VWC
LastObject=IDC_CMB_POINTS

[DLG:IDD_DIALOGBAR (English (U.S.))]
Type=1
Class=MethodBar
ControlCount=6
Control1=IDC_STATIC,static,1342308352
Control2=IDC_CMB_VISION,combobox,1344339971
Control3=IDC_CMB_POINTS,combobox,1344339971
Control4=IDC_BTN_FIND,button,1342242816
Control5=IDC_BTN_EXECUTE,button,1342242816
Control6=IDC_BTN_ACQUIRE,button,1342242816

[DLG:IDD_DIALOG_ROBOT]
Type=1
Class=CRobotPositionDialog
ControlCount=38
Control1=IDOK,button,1342242816
Control2=IDC_SL_X,msctls_trackbar32,1476460568
Control3=IDC_ST_POS0,static,1476526080
Control4=IDC_ED_X,edit,1484849280
Control5=IDC_SL_Y,msctls_trackbar32,1476460568
Control6=IDC_ST_POS1,static,1476526080
Control7=IDC_ED_Y,edit,1484849280
Control8=IDC_SL_Z,msctls_trackbar32,1476460568
Control9=IDC_ST_POS2,static,1476526080
Control10=IDC_ED_Z,edit,1484849280
Control11=IDC_SL_YAW,msctls_trackbar32,1476460568
Control12=IDC_ST_POS3,static,1476526080
Control13=IDC_ED_YAW,edit,1484849280
Control14=IDC_SL_PITCH,msctls_trackbar32,1476460568
Control15=IDC_ST_POS5,static,1476526080
Control16=IDC_ED_PITCH,edit,1484849280
Control17=IDC_SL_ROLL,msctls_trackbar32,1476460568
Control18=IDC_ST_POS6,static,1476526080
Control19=IDC_ED_ROLL,edit,1484849280
Control20=IDC_ST_POS7,button,1476395015
Control21=IDC_BTN_MOVE,button,1476460544
Control22=IDC_BTN_NEW,button,1342242816
Control23=IDC_BTN_PLAY,button,1342242816
Control24=IDC_BTN_LOAD,button,1342242816
Control25=IDC_BTN_SAVE,button,1342242816
Control26=IDC_BTN_CONNECT,button,1342242817
Control27=IDC_SL_GRIP,msctls_trackbar32,1476460568
Control28=IDC_ST_POS8,static,1476526080
Control29=IDC_ED_GRIP,edit,1484849280
Control30=IDC_SL_SPEED,msctls_trackbar32,1476460568
Control31=IDC_ST_POS9,static,1476526080
Control32=IDC_ED_SPEED,edit,1484849280
Control33=IDC_MSG,static,1342308353
Control34=IDC_STATIC,button,1342177287
Control35=IDC_BTN_ADD,button,1342242816
Control36=IDC_BTN_DELETE,button,1342242816
Control37=IDC_SPIN_POS,msctls_updown32,1342177312
Control38=IDC_TRACK_TEXT,static,1342308352

[CLS:CRobotPositionDialog]
Type=0
HeaderFile=RobotPositionDialog.h
ImplementationFile=RobotPositionDialog.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_TRACK_TEXT

[DLG:IDD_DLG_CAMERA_SETUP]
Type=1
Class=?
ControlCount=2
Control1=IDOK,button,1342242817
Control2=IDCANCEL,button,1342242816

[CLS:MethodBar]
Type=0
HeaderFile=MethodBar.h
ImplementationFile=MethodBar.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_APP_ABOUT

[MNU:IDR_MNU_POPUP]
Type=1
Class=CMainFrame
Command1=ID_FILE_OPEN_PICTURE
Command2=ID_OPENROBOTPATH
Command3=ID_OPEN2D3DPOINTSET
CommandCount=3

[DLG:IDD_DLG_PROGRESS]
Type=1
Class=CProgressDialog
ControlCount=3
Control1=IDC_PROG,msctls_progress32,1350565888
Control2=IDC_BTN_ABORT,button,1342242817
Control3=IDC_CURRENT,static,1342308352

[CLS:CProgressDialog]
Type=0
HeaderFile=ProgressDialog.h
ImplementationFile=ProgressDialog.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_BTN_ABORT
VirtualFilter=dWC

