// LeftView.cpp : implementation of the CLeftView class
//

#include "stdafx.h"
#include "RobotHandler.h"

#include "RobotHandlerDoc.h"
#include "LeftView.h"
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif




/////////////////////////////////////////////////////////////////////////////
// CLeftView

IMPLEMENT_DYNCREATE(CLeftView, CEditView)

BEGIN_MESSAGE_MAP(CLeftView, CEditView)
	//{{AFX_MSG_MAP(CLeftView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG_MAP
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CEditView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CEditView::OnFilePrintPreview)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLeftView construction/destruction

CLeftView::CLeftView() : mcout(std::cout) , mcerr(std::cerr)
{

  mcout.on_message.connect(sig::slot(this,&CLeftView::add_message));
  mcerr.on_message.connect(sig::slot(this,&CLeftView::add_message));
}

CLeftView::~CLeftView()
{
}


void CLeftView::OnInitialUpdate()
{
	CEditView::OnInitialUpdate();
	CEdit &ed = GetEditCtrl();
	ed.SetReadOnly();
	/* fnt.CreateFont(20,5,0,0,FW_DONTCARE,false,false,0,
		DEFAULT_CHARSET,OUT_CHARACTER_PRECIS,
		CLIP_DEFAULT_PRECIS,DEFAULT_QUALITY,
		FF_DONTCARE ,"Verdana"); */
	fnt.CreatePointFont(100,"Microsoft Sans Serif");
	ed.SetFont(&fnt);
	ed.ModifyStyle(0,ES_MULTILINE + ES_AUTOVSCROLL + ES_OEMCONVERT);
	
}

////////////////////////////////////////////////////////////////////////////
// CLeftView message handlers

int CLeftView::add_message(std::string msg)
{
  int a,b;
	CString buf;
	GetEditCtrl().GetWindowText(buf);
  GetEditCtrl().GetSel(a,b);
	GetEditCtrl().SetWindowText(buf + msg.c_str());
  GetEditCtrl().SetSel(a,b);
  GetEditCtrl().LineScroll(GetEditCtrl().GetLineCount());
  return 0;
}
