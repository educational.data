// RobotHandlerDoc.cpp : implementation of the CRobotHandlerDoc class
//

#include "stdafx.h"
#include "RobotHandler.h"
#include "../vision/LandMarkLocator.h"
#include "../leovision/DiodeDetector.h"
#include "../robot/RobotPoint.h"
#include <ipl98/cpp/algorithms/perspective.h>
#include <ipl98/cpp/geometry/line3d.h>

#include "RobotHandlerDoc.h"
#include "../locater.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerDoc

IMPLEMENT_DYNCREATE(CRobotHandlerDoc, CDocument)

BEGIN_MESSAGE_MAP(CRobotHandlerDoc, CDocument)
	//{{AFX_MSG_MAP(CRobotHandlerDoc)
	ON_COMMAND(ID_FILE_OPENROBOTPATH, OnFileOpenrobotpath)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerDoc construction/destruction

CRobotHandlerDoc::CRobotHandlerDoc()
 : image_gui(&mImgFront)
{
  handler = getCalibrateHandler();
  handler->on_error.connect(sig::slot(this,&CRobotHandlerDoc::handleError));
	//setLocater(0);
	//camera.SetUp();
}

const CPoint2D<FLOAT32> operator+(const CPoint2D<FLOAT32> &p1, const CPoint2D<FLOAT32> &p2) {
  CPoint2D<FLOAT32> res;
  res.SetX(p1.GetX() + p2.GetX());
  res.SetY(p1.GetY() + p2.GetY());
  return res;
}

const CPoint2D<FLOAT32> operator-(const CPoint2D<FLOAT32> &p1, const CPoint2D<FLOAT32> &p2) {
  CPoint2D<FLOAT32> res;
  res.SetX(p1.GetX() - p2.GetX());
  res.SetY(p1.GetY() - p2.GetY());
  return res;
}


void CRobotHandlerDoc::findErrors(CPerspective &p, const CCorresponding3D2DPoints & PointSets )
{
  std::vector< CPoint2D< FLOAT32 > > Errors;
  std::vector< CPoint3D< FLOAT32 > > Errors3D;
  p.GetErrors(PointSets,Errors);
  p.MatrixToParameters();

/*
  CPoint3D<FLOAT32> focal(p.m_Par.dx,p.m_Par.dy,p.m_Par.dz);

  double f = p.m_Par.FocalLength; // hardcoded.

  CPoint2D<unsigned int> cen = p.GetImageDimensions();
  cen.SetX(cen.GetX()/2);
  cen.SetY(cen.GetY()/2);

  CPoint3D<FLOAT32> cen3D;

  p.Direction(cen,cen3D);
  
  for(int i = 0; i < Errors.size();++i) {
    double a = PointSets.GetPoint3D(i).GetDist(p.GetPinHole());
    double b = 1/(1/f - 1/a);
    //cout << a << ", " << b << endl; 
    cout << PointSets.GetPoint3D(i);
    cout << " Distance: " << a << ";";
    cout << " Errors: " << Errors[i].GetX() << "; " << Errors[i].GetY();
    cout << " -> " << Errors[i].GetX()*a/b << "; " << Errors[i].GetY()*a/b << endl;
  } */

  for(int i = 0; i < Errors.size(); ++i) {
    double a = PointSets.GetPoint3D(i).GetDist(p.GetPinHole());

    CPoint2D<FLOAT32> errLine = PointSets.GetPoint2D(i);
    CLine3D<FLOAT32> line = p.GetRay(errLine);
    cout << "Distance: " << a << " Error Distance: " << line.Distance(PointSets.GetPoint3D(i)) << endl;
  }

}


CRobotHandlerDoc::~CRobotHandlerDoc()
{

}

BOOL CRobotHandlerDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	setLocater(0);
	
	mImgFront = CImage();
	
	SetModifiedFlag(false);

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerDoc commands

BOOL CRobotHandlerDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
	if (!CDocument::OnOpenDocument(lpszPathName))
		return FALSE;

	// TODO: Add your specialized creation code here

	if(!handler->loadImage(lpszPathName)) {
		cout << "Open failed" << endl;
		UpdateAllViews(NULL);

		return FALSE;
	}	
	setLocater(0);
	mImgFront = handler->getImage();
	cout << "------ Opened " << lpszPathName << " succesfully. ---- " << endl;
	//cout << "Image Size is: " << mImgFront.GetWidth() << ", " << mImgFront.GetHeight() << "\r\n\r\n";
	//UpdateAllViews(NULL);
 
	return TRUE;
}



bool CRobotHandlerDoc::AcquireImage()
{
  return handler->acquireImage();
}

void CRobotHandlerDoc::find_punkt()
{
  handler->findPunkt();
}

extern CRobotHandlerApp theApp;
void CRobotHandlerDoc::resolvePoints()
{
  handler->doCalibrate();
}

void CRobotHandlerDoc::setLocater(int id)
{
  switch(id) {
  case 0: 
    handler->setMethod(0);
    break;
  case 1:
    handler->setMethod(1);
    break;
  default:
    handler->setMethod(0);
    break;
  };
}

void CRobotHandlerDoc::OnFileOpenrobotpath() 
{
  CFileDialog dlg(true,".crs",NULL,OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "CRS tracks (*.crs)|*.crs|All files|*.*|");
  dlg.m_ofn.lpstrTitle = "Open robot track";
  
  
  /* if(dlg.DoModal() == IDOK)
    robot_track = CRobotPoint::load(cout,dlg.GetPathName().GetBuffer(0)); */
  UpdateAllViews(NULL);	
}

BOOL CRobotHandlerDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
  CImage img =( handler->getImage() );
  img.Save(lpszPathName);	
  return CDocument::OnSaveDocument(lpszPathName);
}

void CRobotHandlerDoc::randomizePoints(int numPoints)
{
/*
  // Generer punkter til path. 
  robot_track.clean();

  int offsetX = 350;
  int offsetY = -100;
  int offsetZ = 200;

  int maxX = 550;
  int maxY = 300;
  int maxZ = 450;

  int spanX = offsetX-maxX;
  int spanY = offsetY-maxY;
  int spanZ = offsetZ-maxZ;

  for(int cnt = 0; cnt < numPoints;++cnt) {
    int X = offsetX + rand() % spanX;
    int Y = offsetY + rand() % spanY;
    int Z = offsetZ + rand() % spanZ;

    robot_track.add(location( X, Y, Z, 0, 80, 70, 0 ));
  }
  
  cout << robot_track.toString() << endl;
  */
  UpdateAllViews(NULL);
  
}

int CRobotHandlerDoc::handleError(std::string msg)
{
  return AfxMessageBox(msg.c_str(),MB_OK | MB_ICONSTOP);
}
