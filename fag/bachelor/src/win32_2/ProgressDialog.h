#if !defined(AFX_PROGRESSDIALOG_H__8360A962_D94B_462A_B92F_AE8BB00F1110__INCLUDED_)
#define AFX_PROGRESSDIALOG_H__8360A962_D94B_462A_B92F_AE8BB00F1110__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProgressDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProgressDialog dialog

class CProgressDialog : public CDialog
{
// Construction
public:
	void DoEvents();
	bool setState(int state);
	void perform();
	bool abort;
	CProgressDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CProgressDialog)
	enum { IDD = IDD_DLG_PROGRESS };
	CProgressCtrl	m_progress;
	CButton	m_abortbutton;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgressDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CProgressDialog)
	afx_msg void onBtnAbort();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROGRESSDIALOG_H__8360A962_D94B_462A_B92F_AE8BB00F1110__INCLUDED_)
