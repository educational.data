// RobotHandler.h : main header file for the ROBOTHANDLER application
//

#if !defined(AFX_ROBOTHANDLER_H__D7877F16_8441_414C_9183_86517935D1BD__INCLUDED_)
#define AFX_ROBOTHANDLER_H__D7877F16_8441_414C_9183_86517935D1BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols
#include <string>

/////////////////////////////////////////////////////////////////////////////
// CRobotHandlerApp:
// See RobotHandler.cpp for the implementation of this class
//

class CRobotHandlerApp : public CWinApp
{
public:
  int showError(std::string errMsg);
	CRobotHandlerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRobotHandlerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CRobotHandlerApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROBOTHANDLER_H__D7877F16_8441_414C_9183_86517935D1BD__INCLUDED_)
