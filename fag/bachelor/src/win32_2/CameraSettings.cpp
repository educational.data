// CameraSettings.cpp : implementation file
//

#include "stdafx.h"
#include "RobotHandler.h"
#include "CameraSettings.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCameraSettingsView

IMPLEMENT_DYNCREATE(CCameraSettingsView, CFormView)

CCameraSettingsView::CCameraSettingsView()
	: CFormView(CCameraSettingsView::IDD)
{
	//{{AFX_DATA_INIT(CCameraSettingsView)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}

CCameraSettingsView::~CCameraSettingsView()
{
}

void CCameraSettingsView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCameraSettingsView)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCameraSettingsView, CFormView)
	//{{AFX_MSG_MAP(CCameraSettingsView)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCameraSettingsView diagnostics

#ifdef _DEBUG
void CCameraSettingsView::AssertValid() const
{
	CFormView::AssertValid();
}

void CCameraSettingsView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CCameraSettingsView message handlers
