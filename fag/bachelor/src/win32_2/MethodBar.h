#if !defined(AFX_METHODBAR_H__B0075AD4_7B9D_4F3F_B613_5F82A98CC9D0__INCLUDED_)
#define AFX_METHODBAR_H__B0075AD4_7B9D_4F3F_B613_5F82A98CC9D0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MethodBar.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// MethodBar dialog

class MethodBar : public CDialog
{
// Construction
public:
	MethodBar(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(MethodBar)
	enum { IDD = IDD_DIALOGBAR };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(MethodBar)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(MethodBar)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_METHODBAR_H__B0075AD4_7B9D_4F3F_B613_5F82A98CC9D0__INCLUDED_)
