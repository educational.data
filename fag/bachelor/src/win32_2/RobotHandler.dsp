# Microsoft Developer Studio Project File - Name="RobotHandler" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=RobotHandler - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "RobotHandler.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "RobotHandler.mak" CFG="RobotHandler - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "RobotHandler - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "RobotHandler - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "RobotHandler - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "NOMINMAX" /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x406 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x406 /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 ipl98_visualc.lib 1394camera.lib cameras_ipl98.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /pdb:none
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy ..\..\..\..\ipl98\lib\*.dll Release
# End Special Build Tool

!ELSEIF  "$(CFG)" == "RobotHandler - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /I "H:\ipl98\source" /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "NOMINMAX" /FD /GZ /c
# SUBTRACT CPP /YX /Yc /Yu
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x406 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x406 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ipl98_visualc.lib 1394camera.lib cameras_ipl98.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"H:\ipl98\lib"
# Begin Special Build Tool
SOURCE="$(InputPath)"
PostBuild_Cmds=copy ..\..\..\..\ipl98\lib\*.dll Debug
# End Special Build Tool

!ENDIF 

# Begin Target

# Name "RobotHandler - Win32 Release"
# Name "RobotHandler - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\vision\Blob.cpp
# End Source File
# Begin Source File

SOURCE=..\vision\BlobMass.cpp
# End Source File
# Begin Source File

SOURCE=..\vision\Blobs.cpp
# End Source File
# Begin Source File

SOURCE=..\videndo\camera_model.cpp
# End Source File
# Begin Source File

SOURCE=..\camera\CameraDocument.cpp
# End Source File
# Begin Source File

SOURCE=..\camera\CameraSettings.cpp
# End Source File
# Begin Source File

SOURCE=.\CImageView.cpp
# End Source File
# Begin Source File

SOURCE=..\robot\crs_aci.cpp
# End Source File
# Begin Source File

SOURCE=..\leovision\DiodeDetector.cpp
# End Source File
# Begin Source File

SOURCE=..\vision\Histogram.cpp
# End Source File
# Begin Source File

SOURCE=..\vision\LandMarkLocator.cpp
# End Source File
# Begin Source File

SOURCE=.\LeftView.cpp
# End Source File
# Begin Source File

SOURCE=..\robot\location.cpp
# End Source File
# Begin Source File

SOURCE=..\locationHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressDialog.cpp
# End Source File
# Begin Source File

SOURCE=..\robot\robot.cpp
# End Source File
# Begin Source File

SOURCE=..\robot\RobotController.cpp
# End Source File
# Begin Source File

SOURCE=.\RobotHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\RobotHandler.rc
# End Source File
# Begin Source File

SOURCE=..\robot\RobotPoint.cpp
# End Source File
# Begin Source File

SOURCE=.\RobotPositionDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\VisualGUI.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\vision\Blob.h
# End Source File
# Begin Source File

SOURCE=..\vision\BlobMass.h
# End Source File
# Begin Source File

SOURCE=..\vision\Blobs.h
# End Source File
# Begin Source File

SOURCE=..\videndo\camera_model.h
# End Source File
# Begin Source File

SOURCE=..\camera\CameraDocument.h
# End Source File
# Begin Source File

SOURCE=..\camera\CameraSettings.h
# End Source File
# Begin Source File

SOURCE=.\CImageView.h
# End Source File
# Begin Source File

SOURCE=..\videndo\color.h
# End Source File
# Begin Source File

SOURCE=..\robot\CRCRobotBase.h
# End Source File
# Begin Source File

SOURCE=..\robot\crs_aci.h
# End Source File
# Begin Source File

SOURCE=..\leovision\DiodeDetector.h
# End Source File
# Begin Source File

SOURCE=.\exceptions.h
# End Source File
# Begin Source File

SOURCE=..\videndo\features.h
# End Source File
# Begin Source File

SOURCE=..\vision\Histogram.h
# End Source File
# Begin Source File

SOURCE=..\ihandler.h
# End Source File
# Begin Source File

SOURCE=..\vision\LandMarkLocator.h
# End Source File
# Begin Source File

SOURCE=.\LeftView.h
# End Source File
# Begin Source File

SOURCE=..\videndo\level_contours.h
# End Source File
# Begin Source File

SOURCE=..\videndo\local_trend_hough.h
# End Source File
# Begin Source File

SOURCE=..\locater.h
# End Source File
# Begin Source File

SOURCE=..\robot\location.h
# End Source File
# Begin Source File

SOURCE=..\locationHandler.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=..\videndo\mark_finder.h
# End Source File
# Begin Source File

SOURCE=..\videndo\mark_pose.h
# End Source File
# Begin Source File

SOURCE=..\videndo\mark_retrieval.h
# End Source File
# Begin Source File

SOURCE=.\myOutStream.h
# End Source File
# Begin Source File

SOURCE=..\videndo\polar3d.h
# End Source File
# Begin Source File

SOURCE=..\videndo\pose.h
# End Source File
# Begin Source File

SOURCE=.\ProgressDialog.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=..\robot\robot.h
# End Source File
# Begin Source File

SOURCE=..\robot\RobotController.h
# End Source File
# Begin Source File

SOURCE=.\RobotHandler.h
# End Source File
# Begin Source File

SOURCE=..\robot\RobotPoint.h
# End Source File
# Begin Source File

SOURCE=.\RobotPositionDialog.h
# End Source File
# Begin Source File

SOURCE=..\videndo\scape_c.h
# End Source File
# Begin Source File

SOURCE=..\sig.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\vision\tcimage.h
# End Source File
# Begin Source File

SOURCE=..\videndo\telemetrics.h
# End Source File
# Begin Source File

SOURCE=..\vision\tpoint.h
# End Source File
# Begin Source File

SOURCE=..\videndo\videndo_setup.h
# End Source File
# Begin Source File

SOURCE=..\videndo\virtual_cam.h
# End Source File
# Begin Source File

SOURCE=.\VisualGUI.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\RobotHandler.ico
# End Source File
# Begin Source File

SOURCE=.\res\RobotHandler.rc2
# End Source File
# Begin Source File

SOURCE=.\res\RobotHandlerDoc.ico
# End Source File
# Begin Source File

SOURCE=.\sdi.ico
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\robotdata.txt
# End Source File
# End Target
# End Project
