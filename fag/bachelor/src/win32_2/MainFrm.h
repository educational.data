// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__7B57AEEB_0C92_4F89_9ADC_9A8742B68D1A__INCLUDED_)
#define AFX_MAINFRM_H__7B57AEEB_0C92_4F89_9ADC_9A8742B68D1A__INCLUDED_

#include "MethodBar.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRobotHandlerView;

class CMainFrame : public CFrameWnd
{
	
public:
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
protected:
	CSplitterWnd m_wndSplitter;
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
#endif

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	CToolBar    m_wndToolBar;
  CDialogBar m_wndMethodBar;

// Generated message map functions
protected:

	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSelchangeCmbPoints();
	afx_msg void onFindPunkt();
	afx_msg void onExecuteCalibration();
	afx_msg void onChangeVisionMethod();
	afx_msg void onAcquireImage();
	afx_msg void OnFileOpenPicture();
	afx_msg void onTBOpen();
	afx_msg void onOpenRobotPath();
	afx_msg void onOpen2D3DPointset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void setProgress(bool state);
	void DockControlBarLeftOf(CToolBar* Bar, CToolBar *LeftOf);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__7B57AEEB_0C92_4F89_9ADC_9A8742B68D1A__INCLUDED_)
