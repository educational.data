#ifndef __exception_h_included__
#define __exception_h_included__

#include <string>

class user_abort_exception : public std::exception {
private:
	std::string msg;
public:
  user_abort_exception() throw() {
      msg = "User abort";
  }

  user_abort_exception(const std::string str) throw() {
      msg = str;
  }
  const char *what() const throw() {
		return msg.c_str();
	}

};

class runtime_exception : public std::exception {
private:
	std::string msg;
public:
	runtime_exception(const std::string str) throw (){
		msg = str;
	}
	const char *what() const throw() {
		return msg.c_str();
	}
};


#endif