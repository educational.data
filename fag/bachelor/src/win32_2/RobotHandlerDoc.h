// RobotHandlerDoc.h : interface of the CRobotHandlerDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROBOTHANDLERDOC_H__527C2F67_93D7_4EF9_802D_253404A7B305__INCLUDED_)
#define AFX_ROBOTHANDLERDOC_H__527C2F67_93D7_4EF9_802D_253404A7B305__INCLUDED_

#include "VisualGUI.h"	// Added by ClassView
#include <ipl98/cpp/image.h>
#include <sstream>
#include "../ihandler.h"
//#include "../camera/CameraDocument.h"	// Added by ClassView

using namespace ipl;
using namespace std;

namespace ipl {
  class CPerspective;
  class CCorresponding3D2DPoints;
}

class CRobotHandlerDoc : public CDocument
{
protected: // create from serialization only
	CRobotHandlerDoc();
	DECLARE_DYNCREATE(CRobotHandlerDoc)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRobotHandlerDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
	int handleError(std::string msg);
	ICalibrateHandler * handler;
	void setLocater(int location);
	void findErrors(CPerspective &p,const CCorresponding3D2DPoints &  PointSets);
	void randomizePoints(int numPoints);
	void resolvePoints();
	void find_punkt();

	bool AcquireImage();

	ostringstream cout;
	CImage mImgFront;
	
	virtual ~CRobotHandlerDoc();
#ifdef _DEBUG
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CRobotHandlerDoc)
	afx_msg void OnFileOpenrobotpath();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	//CRobotPoint robot_track;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROBOTHANDLERDOC_H__527C2F67_93D7_4EF9_802D_253404A7B305__INCLUDED_)
