// LeftView.h : interface of the CLeftView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_LEFTVIEW_H__0C341216_BAC5_4B92_8E31_CAACB0929C42__INCLUDED_)
#define AFX_LEFTVIEW_H__0C341216_BAC5_4B92_8E31_CAACB0929C42__INCLUDED_

#include "myOutStream.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CRobotHandlerDoc;

class CLeftView : public CEditView
{
	CFont fnt;
protected: // create from serialization only
	CLeftView();
	DECLARE_DYNCREATE(CLeftView)

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLeftView)
	public:
	virtual void OnInitialUpdate();
	//}}AFX_VIRTUAL

// Implementation
public:
	MyOutStream mcout;
  MyOutStream mcerr;
  int add_message(std::string msg);
	virtual ~CLeftView();
#ifdef _DEBUG
#endif

protected:

// Generated message map functions
protected:
	//{{AFX_MSG(CLeftView)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_LEFTVIEW_H__0C341216_BAC5_4B92_8E31_CAACB0929C42__INCLUDED_)
