#ifdef USEGTKMM
#include "window.h"
#include <gdk/gdkpixbuf.h>
#include <gdkmm/pixbuf.h>

#include <gtkmm/drawingarea.h>
#include "vision/tcimage.h"

//Custom drawing area with modified expose_event.
class CustomDrawingArea : public Gtk::DrawingArea
{
public:
  CustomDrawingArea(const tCImage & img) : DrawingArea() { 
    const int rowstride = 3;
    const int w = img.GetWidth();
    const int h = img.GetHeight();

    data = new guchar[w * h * rowstride];
    for(int y = 0; y < h; y++) {
      for(int x = 0; x < w; x++) {
	guchar *pix = & (data[(x + y * w) * rowstride]);
	pix[0] = pix[1] = pix[2] = img.GetPixelFast(x,y);
      }
    }
    image = Gdk::Pixbuf::create_from_data(data,
					  Gdk::COLORSPACE_RGB,
					  false,
					  8,
					  w,
					  h,
					  rowstride * w
					  );
    
    set_size_request(img.GetWidth(),
		     img.GetHeight());
  }
  virtual ~CustomDrawingArea() {
    delete [] data;
  }
  bool on_expose_event(GdkEventExpose* event) {
    image->render_to_drawable(get_window(), get_style()->get_black_gc(),
			      0, 0, 0, 0, image->get_width(), image->get_height(), // draw the whole image (from 0,0 to the full width,height) at 100,80 in the window
			      Gdk::RGB_DITHER_NONE, 0, 0);
 
    return true;
  }
private:
  
  Glib::RefPtr<Gdk::Pixbuf> image;
  guchar *data;
};


myWin::~myWin() {
  delete m_drawing_area;
}

myWin::myWin(const tCImage &img) : Gtk::Window() {
  m_drawing_area = new CustomDrawingArea(img);
  // set_border_width(5);
  add(m_Box);
  m_Box.pack_start(*m_drawing_area);
  show_all_children();
}
#endif // ifdef USEGTKMM
