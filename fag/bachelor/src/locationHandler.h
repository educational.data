#ifndef LOCATION_HANDLER_H_
#define LOCATION_HANDLER_H_

#include "sig.h"
#include <ipl98/cpp/image.h>
#include <ipl98/cpp/algorithms/perspective.h>
#include "vision/LandMarkLocator.h"
#include "leovision/DiodeDetector.h"
#include "robot/RobotPoint.h"
#include "camera/CameraDocument.h"
#include "robot/RobotController.h" 
#include "robot/RobotPoint.h"
#include "ihandler.h"
#include <vector>

class Locater;

class CalibrateHandler : public ICalibrateHandler {
public:
	void setImage(const CImage &newimg);
	bool randomizePoints(const int numPoints);
	bool hasCamera();
  
	void findPunkt();
  virtual bool loadImage(const std::string &filename);
  virtual bool loadRobotPath(const std::string &filename);
  virtual bool loadScenario(const std::string &filename);
  virtual bool saveImage(const std::string &filename);
  virtual bool saveRobotPath(const std::string &filename);
  virtual bool saveScenario(const std::string &filename);
  //virtual string dumpMathematica();

  CalibrateHandler();
  const ipl::CImage &getImage() const;
  void setMethod(int method);
  void doCalibrate();
  bool acquireImage();
private:
  const CCorresponding3D2DPoints findPointsInSystem();
  void drawMark(CImage & img, const int x, const int y);
private:
  CRobotController robot;
  CCameraDocument camera;
  Locater *locater;
  ipl::CImage img;

  CRobotPoint robot_track;
};

#endif
