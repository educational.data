#ifndef __WINDOWSE_H__
#define __WINDOWSE_H__

#ifdef USEGTKMM
#warning using gtkmm

#include <gtkmm/window.h>
#include <gtkmm/box.h>
//#include <gtkmm/uimanager.h>

#include "vision/tcimage.h"

class CustomDrawingArea;

class myWin : public Gtk::Window {
public:
  myWin(const tCImage &img);
  virtual ~myWin();
protected:
  //Child widgets:
  Gtk::VBox m_Box;
  CustomDrawingArea *m_drawing_area;
  
};

#endif // ifdef USEGTKMM
#endif
