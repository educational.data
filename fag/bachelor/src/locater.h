#ifndef __LOCATER_H_INCLUDED__
#define __LOCATER_H_INCLUDED__

#include <ipl98/cpp/image.h>
#include <points/point2d.h>

namespace ipl {
  class CImage;
}

class Locater {
public:
  virtual ~Locater() {}
  virtual bool locate(const ipl::CImage &img) = 0;
  virtual bool hasPoint() const = 0;
  virtual CPoint2D<double> getPoint() const = 0;
};

#endif // __LOCATER_H_INCLUDED__
