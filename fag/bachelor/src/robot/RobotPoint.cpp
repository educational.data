// RobotPoint,cpp: implementation of the CRobotPoint class,
//
//////////////////////////////////////////////////////////////////////

#include "RobotPoint.h"
#include <fstream>
#include <sstream>
#include <iomanip>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRobotPoint CRobotPoint::load(std::ostream &cout, const std::string fil) {
	using namespace std;

	CRobotPoint points;

	std::ifstream fin(fil.c_str());

	if(! fin.is_open() )
		return points;
	int count;
	fin >> count;
	for(int p =0;p<count;++p) {
		float x,y,z,yaw,pitch,roll,grip;
		fin >> x >> y >> z >> yaw >> pitch >> roll >> grip;
		points.positions.push_back(location(x,y,z,yaw,pitch,roll,grip));
	}
	cout << "Loaded " << count << " point(s) from " << fil << endl;
	

	fin.close();

	return points;
}

CRobotPoint::CRobotPoint()
{
	
}

CRobotPoint::~CRobotPoint()
{

}

int CRobotPoint::size() const
{
	return positions.size();
}

const location & CRobotPoint::operator[](const int index)
{
	return positions.at(index);
}

bool CRobotPoint::isZero() const
{
	return positions.size() == 0;	
}

void CRobotPoint::dummy()
{
	positions.push_back(location(500,  100,  250,  0,  80,  70,  0));
	positions.push_back(location(450,  100,  250,  0,  80,  70,  0));
	positions.push_back(location(400,  100,  250,  0,  80,  70,  0));
	positions.push_back(location(400,  200,  250,  0,  80,  70,  0));
	positions.push_back(location(450,  200,  250,  0,  80,  70,  0));
	positions.push_back(location(500,  200,  250,  0,  80,  70,  0));
	positions.push_back(location(500,  200,  350,  0,  80,  70,  0));
	positions.push_back(location(450,  200,  350,  0,  80,  70,  0));
	positions.push_back(location(400,  200,  350,  0,  80,  70,  0));
	positions.push_back(location(400,  100,  350,  0,  80,  70,  0));
	positions.push_back(location(500,  100,  350,  0,  80,  70,  0));
}

bool CRobotPoint::save(const std::string &fil)
{
	using namespace std;
	ofstream of(fil.c_str());

	if(!of.is_open()) return false;
	
	of << size() << endl;
	for(itcpos it = positions.begin();it != positions.end();++it) {
		of << it->x << "  ";
		of << it->y << "  ";
		of << it->z << "  ";
		of << it->yaw << "  ";
		of << it->pitch << "  ";
		of << it->roll << "  ";
		of << it->grip << endl;
	}
	of.close();
	return true;
}

void CRobotPoint::add(location l)
{
  positions.push_back(l);
}

void CRobotPoint::clean()
{
  positions.resize(0);
}

std::string CRobotPoint::toString()
{
  using namespace std;
  stringstream str;

  if(positions.size() == 0)
    return "";
  
  str << "CRobotPoint (" << positions.size() << ") [" << endl;
	for(itcpos it = positions.begin();it != positions.end();++it) {
    str << "\t"<< it->toString() << "" << endl;
  }
  str << "]";
  return str.str();
}
