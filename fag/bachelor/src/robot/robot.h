// robot.h: interface for the robot class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROBOT_H__B57AF0A2_CE32_4ADD_8E5A_7D59DFFC5AA5__INCLUDED_)
#define AFX_ROBOT_H__B57AF0A2_CE32_4ADD_8E5A_7D59DFFC5AA5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000



// forware decalaration of ACI
class ACI;
class robot  
{
public:
  bool Home();
  void CloseSession();
  robot();
  virtual ~robot();
  
  bool Tool(char *loc);
  bool SetSpeed(int speed);
  bool SetLocation(char *loc, float x, float y, float z, float yaw, float pitch, float roll);
  bool Ready();
  bool OpenSession(int port, int rate, int slave_no);
  bool Open(float force);
  bool Move(char *loc);
  bool KillLocation(char *loc);
  bool Joint(int index, float Angle);
  bool Grip(float pos);
  bool GetLocation(char *loc, float &x, float &y, float &z, float &yaw, float &pitch, float &roll);
  bool CopyLocation(char *loc, char *dest);
  bool Close(float force);
  bool AsyncMove(char *loc);
  bool Actual(float &x, float &y, float &z, float &yaw, float &pitch, float &roll);
  bool Abort();
private:
  ACI *bot;
};

#endif // !defined(AFX_ROBOT_H__B57AF0A2_CE32_4ADD_8E5A_7D59DFFC5AA5__INCLUDED_)
