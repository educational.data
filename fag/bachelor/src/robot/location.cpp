// location.cpp: implementation of the location class.
//
//////////////////////////////////////////////////////////////////////


#include "location.h"
#include <sstream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

std::string location::toString()
{
  using namespace std;
  stringstream str;
  
  str << "location [";
  str << x << " ";
  str << y << " ";
  str << z << " ";
  str << yaw << " ";
  str << pitch << " ";
  str << roll << " ";
  str << grip << "]";

  return str.str();
}
