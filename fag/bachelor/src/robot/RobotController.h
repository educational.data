// RobotController.h: interface for the CRobotController class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROBOTCONTROLLER_H__2584BB5C_AB39_48B4_8AAC_3DDC20071C15__INCLUDED_)
#define AFX_ROBOTCONTROLLER_H__2584BB5C_AB39_48B4_8AAC_3DDC20071C15__INCLUDED_

#include "crs_aci.h"
#include "location.h"
#include <exception>

class CRobotController  
{
  ACI bot;
  bool sessionopen;
public:
  
  
public:
  location getPosition();
  void home();
  
  void disconnect();
  void connect() throw(std::exception);
  void grip(const int position) throw(std::exception);
  void moveTo(const int x, const int y, const int z, const int yaw, const int pitch, const int roll, const int speed) throw(std::exception);
  void moveTo(const location &loc, const int speed) throw(std::exception);
  
  CRobotController();
  CRobotController(const int portid) throw(std::exception);
  virtual ~CRobotController();
  
private:
  int m_portId;
  // forcing robot not to be copyable
  CRobotController(const CRobotController &) throw(std::exception);
  void initTools();
};

#endif // !defined(AFX_ROBOTCONTROLLER_H__2584BB5C_AB39_48B4_8AAC_3DDC20071C15__INCLUDED_)
