// RobotPoint.h: interface for the CRobotPoint class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ROBOTPOINT_H__315BEE0C_1C54_4FB7_9D5D_0E680CAF72D6__INCLUDED_)
#define AFX_ROBOTPOINT_H__315BEE0C_1C54_4FB7_9D5D_0E680CAF72D6__INCLUDED_

#include <vector>
#include <string>
#include <ostream>
#include "location.h"

class CRobotPoint  
{
public:
  std::string toString();
	void clean();
	void add(location l);
	bool save(const std::string &fil);
	bool isZero() const;
	int size() const;
	typedef std::vector<location> cpos;
	typedef std::vector<location>::iterator itcpos;

	static CRobotPoint load(std::ostream &cout, const std::string fil);
	const location & operator[](const int index);

	CRobotPoint();
	virtual ~CRobotPoint();
	cpos positions;
private:
	void dummy();
};

#endif // !defined(AFX_ROBOTPOINT_H__315BEE0C_1C54_4FB7_9D5D_0E680CAF72D6__INCLUDED_)
