// location.h: interface for the location class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOCATION_H__A870AA16_4A56_4051_A42E_0303E6614F3E__INCLUDED_)
#define AFX_LOCATION_H__A870AA16_4A56_4051_A42E_0303E6614F3E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <ostream>
#include <string>

class location {
public:
  std::string toString();
	location() { x=y=z=yaw=roll=pitch=grip=0; }
	location(const float _x, const float _y, const float _z, 
		const float _yaw, const float _pitch, const float _roll, const float _grip) :
		x(_x),y(_y),z(_z),yaw(_yaw),pitch(_pitch),roll(_roll),grip(_grip) {}
	float x,y,z,yaw,pitch,roll,grip;
  const location &operator=(const location &l) {
    x = l.x;
    y = l.y;
    z = l.z;
    yaw = l.yaw;
    roll = l.roll;
    pitch = l.pitch;
    grip = l.grip;
    return *this;
  }
};

#endif // !defined(AFX_LOCATION_H__A870AA16_4A56_4051_A42E_0303E6614F3E__INCLUDED_)
