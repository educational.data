// RobotController.cpp: implementation of the CRobotController class.
//
//////////////////////////////////////////////////////////////////////


#include "RobotController.h"
#include <string>
#include "../win32_2/exceptions.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CRobotController::CRobotController(const int portid) {
	m_portId = portid;
	sessionopen = false;
}

CRobotController::CRobotController(const CRobotController &ctrl) throw(exception) {
	 throw runtime_exception("Cannot copy robot object");
}

CRobotController::~CRobotController()
{
	disconnect();
}

CRobotController::CRobotController()
{
	sessionopen = false;
	m_portId = 1;
}

void CRobotController::initTools()
{
	float ToolY=0;//9;
	float ToolZ=0;//7;
	float ToolX=100;//258;
	float ToolRoll=45;

	bot.SetLocation("TOOL",ToolX/25.4,ToolY/24.5,ToolZ/25.4,0,0,ToolRoll);
    bot.Tool("TOOL");

}

void CRobotController::moveTo(const int x, const int y, const int z, const int yaw, const int pitch, const int roll, const int speed) throw(exception)
{
	float inch=25.4f;

	bot.SetLocation("AA",x/inch,y/inch,z/inch,yaw,pitch,roll);
	int spd = speed;
	spd = spd >= 1 ? spd : 1;
	spd = spd <= 100 ? spd : 100; 
	bot.SetSpeed(spd);

	if(!bot.Move("AA")) 
		throw runtime_exception("Could not move to location");
}

void CRobotController::moveTo(const location &loc, const int speed) throw(exception) {
	moveTo(loc.x,loc.y,loc.z,loc.yaw,loc.pitch,loc.roll,speed);
	grip(loc.grip);
}

void CRobotController::grip(const int position) throw(exception)
{
	float inch=25.4f;
	if(!bot.Grip(position/inch))
		throw runtime_exception("Invalid grip");
}


void CRobotController::connect() throw(exception)
{
	sessionopen = bot.OpenSession(m_portId,38400,1);

	if(!sessionopen)sessionopen = bot.OpenSession(m_portId,38400,2);
    if(!sessionopen)throw runtime_exception("Could not connect to robot on either port.");

	initTools();
}

void CRobotController::disconnect()
{
	if(sessionopen) bot.CloseSession();
}

void CRobotController::home()
{
	if(sessionopen) bot.Home();
}

location CRobotController::getPosition()
{
	float inch=25.4f;

	location loc;
	if(sessionopen) {
		bot.GetLocation("AA",loc.x,loc.y,loc.z,loc.yaw,loc.pitch,loc.roll);
		loc.grip=25;
		loc.x*=inch;loc.y*=inch;loc.z*=inch;
	}
	return loc;
}
