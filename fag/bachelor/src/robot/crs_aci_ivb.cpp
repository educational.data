#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include "crs_aci.h"

//#define FALSE   (0)
//#define TRUE    (!FALSE)
#define NO      FALSE
#define YES     TRUE

#define ENQ_ATS    32
#define ENQ        0x05
#define EOT        0x04
#define ETB        0x17
#define ETX        0x03
#define STX        0x02
#define ACK        0x06
#define NAK        0x15
#define SOH        0x01
#define SRCID      0xFF

#define MAXERTRY   32 /* Maximum enquiry attempts */
#define MAXHRTRY   3  /* Maximum header retry limit */
#define MAXDRTRY   3  /* Max data retry limit */

uword aci_pacing=0; /* time interval between characters */
uword aci_channel=2; /* 1=COM1, 2=COM2 */
uword aci_txto = 5000; /* Timeout value for transmit character - in msec */
uword aci_rxto = 5000; /* Timeout value for receive character - in msec */
uword aci_baud = 38400; /* Baud rate for ACI communication - the default
			   is 2400                                                                */
uword aci_slave = 1; /* Default is slave #1. Can assume a value of 1 to
			127                                                                    */
word aci_err = 0; /* Global aci error number. Gets set to non-zero if any ACI
		     error occurs. When this happens, subsequent ACI programming is aborted. */

void error_state( word err )
{
  aci_err = err;
}




/*
 * Empty the input buffer, and clear any pending character in the serial input
 * device. This should initialize the input of characters to a known state.
 */
void IBMACI::clean_aci()
{
  COMMTIMEOUTS timeouts;
  char buf[1];
  DWORD numread;
  int rc;

  timeouts.ReadIntervalTimeout			= MAXDWORD;
  timeouts.ReadTotalTimeoutMultiplier	= 0;
  timeouts.ReadTotalTimeoutConstant	= 0;
  timeouts.WriteTotalTimeoutMultiplier= 0;
  timeouts.WriteTotalTimeoutConstant	= 0;
  SetCommTimeouts(aci_com, &timeouts);

  do {
    rc = ReadFile(aci_com, buf, sizeof(buf), &numread, 0);
  } while(rc && numread);

  aci_timeouts.ReadIntervalTimeout				= aci_rxto;
  aci_timeouts.ReadTotalTimeoutMultiplier	= aci_rxto;
  aci_timeouts.ReadTotalTimeoutConstant		= 0;
  aci_timeouts.WriteTotalTimeoutMultiplier	= aci_txto; 
  aci_timeouts.WriteTotalTimeoutConstant		= 0;
  SetCommTimeouts(aci_com, &aci_timeouts);
}

// gets a char if available and returns
int IBMACI::get_aci_nto(char* c)
{
  COMMTIMEOUTS timeouts;
  DWORD numread;
  int rc;

  timeouts.ReadIntervalTimeout			= MAXDWORD;
  timeouts.ReadTotalTimeoutMultiplier	= 0; 
  timeouts.ReadTotalTimeoutConstant	= 0;
  timeouts.WriteTotalTimeoutMultiplier= 0;
  timeouts.WriteTotalTimeoutConstant	= 0;
  SetCommTimeouts(aci_com, &timeouts);

  rc = ReadFile(aci_com, c, 1, &numread, 0);

  aci_timeouts.ReadIntervalTimeout				= aci_rxto;
  aci_timeouts.ReadTotalTimeoutMultiplier	= aci_rxto;
  aci_timeouts.ReadTotalTimeoutConstant		= 0;
  aci_timeouts.WriteTotalTimeoutMultiplier	= aci_txto;
  aci_timeouts.WriteTotalTimeoutConstant		= 0;
  SetCommTimeouts(aci_com, &aci_timeouts);

  return numread;
}

/*
 * Send a character out the aci interface. CTS must indicate that a slave
 * is available, a transmit timeout would indicate that an interface chip
 * error exists. Error #50 is set if this happens.
 */
void IBMACI::send_aci(char out_char)
{
  DWORD numwrite;
  int rc;

  rc = WriteFile(aci_com, &out_char, 1, &numwrite, 0);
  if(!rc || numwrite==0) {
    error_state( 0x50 ); /* Set timeout error on receive */
    MessageBeep(-1);
  }
}


/*
 * Read the next character from the ACI interface. Character must be avaiable to
 * the ACI within the receive timeout limit. If a timeout occurs, error #40 will
 * be set.
 */
char IBMACI::get_aci(void)
{
  DWORD numread;
  char out_char;
  int rc;

  rc = ReadFile(aci_com, &out_char, 1, &numread, 0);
  if(!rc || numread==0) {
    error_state( 0x40 ); /* Set timeout error on receive */
    out_char = 0; /* Doesn't matter what it is, it won't be used */
    MessageBeep(-1);
  }

  return( out_char );
}


/*
 * Restore communication interface
 */
void IBMACI::res_aci(void)
{
  if(aci_com) CloseHandle(aci_com);
  aci_com = 0;
}



int IBMACI::initrobot(int showdlg) 
{
  char cname[]="COM0";
  cname[3] += aci_channel;

  aci_com = 0;
  aci_cfig.dwSize = sizeof(COMMCONFIG);
  aci_cfig.dwProviderSubType = PST_RS232;
  aci_cfig.dwProviderOffset = 0;
  aci_cfig.dwProviderSize = 0;
  /*********************************************/
  FillMemory(&aci_cfig.dcb,sizeof(DCB),0);
  aci_cfig.dcb.DCBlength = sizeof(DCB);
  //CString buffer;
  //buffer.Format("38400,n,8,1",aci_baud); // baud=1200 parity=N data=8 stop=1 
  if(!BuildCommDCB("38400,n,8,1",&aci_cfig.dcb))
    {
      MessageBox(0, "Error Displaying\nFejl i opsætningen!", "ACI Message", MB_ICONEXCLAMATION|MB_OK);
      return FALSE;
    }
  /**************************************/
  /*	aci_cfig.dcb.BaudRate=aci_baud;
	aci_cfig.dcb.fBinary=TRUE;
	aci_cfig.dcb.fParity=FALSE;
	aci_cfig.dcb.Parity=NOPARITY;
	aci_cfig.dcb.ByteSize=8;
	aci_cfig.dcb.StopBits=ONESTOPBIT;
	aci_cfig.dcb.fOutX=FALSE;
	aci_cfig.dcb.fInX=FALSE;
	aci_cfig.dcb.fDsrSensitivity=FALSE;
	aci_cfig.dcb.fOutxDsrFlow=FALSE;
	aci_cfig.dcb.fOutxCtsFlow=TRUE;
	aci_cfig.dcb.fRtsControl=RTS_CONTROL_HANDSHAKE;*/

  if(showdlg && !CommConfigDialog(cname, 0, &aci_cfig)) {
    MessageBox(0, "Error Displaying\nCommunication Settings Dialog!", "ACI Message", MB_ICONEXCLAMATION|MB_OK);
    return FALSE;
  }

  return TRUE;
}

/*
 * Initialize the communication channel, and prepare for talking.
 */
void IBMACI::inz_aci( void )
{
  char cname[]="COM0";
  cname[3] += aci_channel;

  if(!aci_com && !(aci_com=CreateFile(cname, GENERIC_READ|GENERIC_WRITE, 0, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0))) {
    MessageBox(0, "Couldn't open Communications Device!", "ACI Message", MB_ICONEXCLAMATION|MB_OK);
    return;
  }
  if(!SetCommState(aci_com, &aci_cfig.dcb)) {
    MessageBox(0, "Error Setting Communication State!", "ACI Message", MB_ICONEXCLAMATION|MB_OK);
    res_aci();
    return;
  }
  clean_aci();
}


/*************************ACI00 Class*******************************/
/*
  Return high or low byte in a word quantity
*/
word ACI00::hibyte(word i )
{
  return( i >> 8 );
}


word ACI00::lobyte(word i )
{
  return( i & 0xFF );
}

/*
  Compare a string of bytes - not a character string, since a NULL can
  appear anywhere.
*/
word ACI00::string_cmp( char *s1 , char *s2 , word n )
{
  word  i,good;

  for( good = TRUE , i = 0 ; i < n ; i++ )
    {
      if ( s1[i] != s2[i] )
	good = FALSE;
    }
  return( good );
}


/**
   ACI system procedures
**/

void ACI00::aci_nstrout(char str[] ,word n )
{
  word  i;
  for( i = 0; i < n; ++i )
    send_aci( str[i] );
}


void ACI00::aci_strin(char tempstr[] ,word n )
{
  word  i;
  for( i = 0; i < n; ++i )
    tempstr[i] = get_aci();
}


/*
  ACI enquiry and expected response strings. The slave ID number must be
  encoded into the string before final transmission.
*/
static char enq_string[] = {'R',0,ENQ};
static char slave_response[] = {'R',0,ACK};

/*
  Issue ENQ sequence to slave controller. Encode slave ID number first.
*/
void ACI00::enqout(word slave_no )
{
  enq_string[1] = slave_no + 0x20;
  aci_nstrout( enq_string , 3 );
}


/*
  Issue a header block to the slave. All header information must be supplied
  in the argument list. Checksum is formulated, and the string is sent.
*/
void ACI00::header(uword slave_no ,uword memtype ,uword memofs ,uword memseg ,char rorw ,uword message_length )
{
  uword  i,rw,lrc = 0;
  char header_string[13];
  uword  lastblk_size,full_blks;

  if ( toupper( rorw ) == 'W' )
    rw = 0x00;
  else
    rw = 0xFF;
  full_blks = message_length / 128;
  lastblk_size = message_length % 128;
  if ( lastblk_size == 0 )
    {
      --full_blks;
      lastblk_size = 128;
    }
  header_string[0] = SOH;
  header_string[1] = slave_no + 0x20;
  header_string[2] = (char)SRCID;
  header_string[3] = (char)rw;
  header_string[4] = (char)memtype;
  header_string[5] = (char)full_blks;
  header_string[6] = (char)lastblk_size;
  header_string[7] = (char)lobyte( memofs );
  header_string[8] = (char)hibyte( memofs );
  header_string[9] = (char)lobyte( memseg );
  header_string[10] = (char)hibyte( memseg );
  header_string[11] = ETX;
  for( i = 1; i < 11; ++i )
    lrc += header_string[i];
  header_string[12] = (char)lrc;
  aci_nstrout( header_string , 13 );
}


/*
  Control retry logic for sending header block. Uses header() procedure
  above to actually send the header data. Controls checking the response from
  the slave.
*/
void ACI00::send_header(uword slave ,uword memtype ,uword memofs ,uword memseg ,char rw ,uword data_length )
{
  char response;
  word  done,try1;

  for( try1 = 0, done = FALSE ; !done && !aci_err ; ++try1 )
    {
      header( slave , memtype , memofs , memseg , rw , data_length );
      response = get_aci();
      if ( !aci_err )
	{
	  if ( response == ACK )
	    done = TRUE; /* Done */
	  else if ( response == NAK )
	    {
	      if ( try1 == MAXHRTRY )
		error_state( 12 ); /* All retries attempted */
	    }
	  else {
	    error_state( 16 ); /* Improper response from slave */
	  }
	}
    }
}


/*
  Read data from the slave. The specified number of bytes will be dumped
  into a memory array located by *data. Send the closing EOT character.
*/
void ACI00::data_read(word n_bytes ,ubyte data[] )
{
  word  i, j, k,done;
  uword lastblk_size,full_blks;
  uword tries,index;
  char lrc, in_char;

  /* Establish the number of full and partial block sizes */
  full_blks = n_bytes / 128;
  lastblk_size = n_bytes % 128;
  if ( lastblk_size == 0 )
    {
      --full_blks;
      lastblk_size = 128;
    }

  /* Do all full blocks */
  for( index = i = tries = 0 ; i != full_blks && !aci_err ; )
    {
      in_char = get_aci();
      if ( !aci_err )
	{
	  if ( in_char == STX )
	    {
	      for( lrc = 0 , k = i << 7 , j = 0 ; j != 128 && !aci_err ; )
		{
		  data[k + j] = get_aci();
		  lrc += data[k + j];
		  ++j;
		}
	      if ( !aci_err )
		{
		  in_char = get_aci();
		  if ( !aci_err )
		    {
		      if ( in_char != ETB )
			error_state( 32 );
		      else {
			in_char = get_aci();
			if ( !aci_err )
			  {
			    if ( in_char == lrc )
			      {
				send_aci( ACK );
				++i;
				tries = 0;
			      }
			    else {
			      send_aci( NAK );
			      if ( tries++ == MAXDRTRY )
				error_state( 20 );
			    }
			  }
		      }
		    }
		}
	    }
	  else {
	    error_state( 28 );
	  }
	}
    }
  /* Do the last block */
  for( done = FALSE, tries = 0  ; !done && !aci_err ; )
    {
      in_char = get_aci();
      if ( !aci_err )
	{
	  if ( in_char != STX )
	    error_state( 28 );
	  else {
	    for( i = lrc = 0 , index = full_blks << 7 ; i != lastblk_size && !aci_err ; ++i )
	      {
		data[i + index] = get_aci();
		lrc += data[i + index];
	      }
	    if ( !aci_err )
	      {
		in_char = get_aci();
		if ( !aci_err )
		  {
		    if ( in_char != ETX )
		      error_state( 34 );
		    else {
		      in_char = get_aci();
		      if ( !aci_err )
			{
			  if ( in_char != lrc )
			    {
			      send_aci( NAK );
			      if ( tries++ == MAXDRTRY )
				error_state( 20 );
			    }
			  else {
			    send_aci( ACK );
			    done = TRUE;
			  }
			}
		    }
		  }
	      }
	  }
	}
    }
  if ( !aci_err )
    {
      in_char = get_aci();
      if ( in_char != EOT )
	error_state( 27 );
      send_aci( EOT );
    }
}


/*
  Send data portion to the slave controller. Handle all data block retries.
  Send the closing EOT character if all goes well or not. We must terminate the
  sequence regardless.
*/
void ACI00::data_out(uword n_bytes ,ubyte data[] )
{
  char in_char, lrc;
  uword  i, j, tries, k, index,done, full_blks, lastblk_size;

  full_blks = n_bytes / 128;
  lastblk_size = n_bytes % 128;
  if ( lastblk_size == 0 )
    {
      --full_blks;
      lastblk_size = 128;
    }
  /* For all full blocks */
  for( done = FALSE , index = i = tries = 0 ; i != full_blks && !aci_err ; )
    {
      send_aci( STX );
      for( lrc = 0 , j = 0 , k = i<<7 ; j < 128 ; ++j )
	{
	  index = k + j;
	  send_aci( data[index] );
	  lrc += data[index];
	}
      send_aci( ETB );
      send_aci( lrc );
      in_char = get_aci();
      if ( !aci_err )
	{
	  if ( in_char == ACK )
	    {
	      ++i;
	      tries = 0;
	    }
	  else if ( in_char != NAK )
	    error_state( 24 );
	  else {
	    if ( tries++ == MAXDRTRY )
	      error_state( 22 );
	  }
	}
    }
  if ( !aci_err )
    {
      for( done = FALSE , tries = 0 ; !done && !aci_err ; )
	{
	  send_aci( STX );
	  for( i = lrc = 0, index = full_blks << 7 ; (i != lastblk_size) ; ++i )
	    {
	      send_aci( data[i + index] );
	      lrc += data[i + index];
	    }
	  send_aci( ETX );
	  send_aci( lrc );
	  in_char = get_aci();
	  if ( !aci_err )
	    {
	      if ( in_char == ACK )
		done = TRUE;
	      else if ( in_char == NAK )
		{
		  if ( tries++ == MAXDRTRY )
		    error_state( 22 );
		}
	      else {
		error_state( 24 );
	      }
	    }
	}
    }
  send_aci( EOT );
}


/*
  Sends enquiry to slave controller, and analyzes the response
*/
void ACI00::sendenq(uword slave_no )
{
  uword  finished, i;
  char tempstr[3];

  slave_response[1] = slave_no + 0x20;
  for( i = 0, finished = FALSE ; !finished && !aci_err ; )
    {
      enqout( slave_no );
      aci_strin( tempstr , 3 );
      if ( !aci_err )
	{
	  if ( string_cmp( tempstr , slave_response , 3 ))
	    finished = TRUE;
	  else if ( i++ == MAXERTRY )
	    error_state( 4 );
	}
    }
}


/*
  Control a complete ACI data transfer. Read and writes are handled here.
  Complete ACI communication is NOT initialized at the start, and reset at the
  end of the sequence as in the aci_transfer procedure, since some system
  initializations send garbage down the serial link, causing slave controller
  communication aborts. For systems such as these, aci_xfr1() will not re-
  initialize the interface so it assumes that the interface is in a constant
  state of readiness. This is probably a good guess is a lot of communication
  cycles are required all within a short span of time.
*/
void ACI00::aci_xfr1(uword slave_dev ,uword memtype ,uword memofs ,uword memseg ,char accesstype ,uword bytes ,ubyte* memptr )
{
  clean_aci();
  error_state(0); /* Clear all pending errors */
  sendenq( slave_dev );
  if ( !aci_err )
    {
      send_header( slave_dev , memtype , memofs , memseg , accesstype , bytes );
      if ( !aci_err )
	{
	  if ( ( toupper( accesstype ) ) == 'W' )
	    data_read( bytes , memptr );
	  else if (( toupper( accesstype)) == 'R' )
	    data_out( bytes , memptr );
	}
    }
}


/**
   APPLICATION INTERFACE LEVEL
**/
/*
  Control a complete ACI data transfer. Read and writes are handled here. Complete
  ACI communication is initialized at the start, and reset at the end of the
  sequence.
*/
void ACI00::aci_xfr(uword slave_dev ,uword memtype ,uword memofs ,uword memseg ,char accesstype ,uword bytes ,ubyte* memptr )
{
  inz_aci();
  aci_xfr1( slave_dev , memtype , memofs , memseg , accesstype , bytes , memptr );
  res_aci();
}


/**
   The following procedures assume that we are talking to the slave identifed by
   the global slave_device variable. It also assumes that we are accessing
   standard memory, specified by the '0' memory type qualifier. This is the most
   convenient robot memory access procedure, as it contains the fewest arguments,
   but assumes correct setup of the global parameter 'aci_slave'.
**/

void ACI00::writerobot(uword offset ,uword segment ,ubyte* srcptr,uword cnt )
{
  aci_xfr( aci_slave, 0, offset, segment, 'R', cnt, srcptr );
}


void ACI00::readrobot(uword offset ,uword segment ,ubyte* destptr,uword cnt )
{
  aci_xfr( aci_slave, 0, offset, segment, 'W', cnt, destptr );
}


/**
   This procedure will return the RAPL pointer list contents.
**/

void ACI00::readptrs(ubyte* destptr,word num )
{
  aci_xfr( aci_slave, 0x40, 0, 0, 'W', (num<<2), destptr );
}

/*************************ACI Class*******************************/

ACI::ACI() {
  sessionopen=FALSE;
}


ACI::~ACI() {
  CloseSession();
}


bool ACI::Tool(char *loc) {
  char buf[16];

  sprintf(buf, "TOOL %s\r", loc);
  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}


bool ACI::Home() {
  if(!sendstr("HOME\r")) return FALSE;

  char c;
  while(!get_aci_nto(&c));
  garble();
  return (c == 0x06);	// ACK
}


bool ACI::Abort() {
  if(!sendstr("ABORT\r")) return FALSE;
  garble();
  return TRUE;
}


bool ACI::Ready() {
  if(!sendstr("READY\r")) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}


bool ACI::SetSpeed(int speed) {
  char buf[11];
  sprintf(buf, "SPEED %d\r", speed);

  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}


bool ACI::Actual(float &x, float &y, float &z, float &yaw, float &pitch, float &roll) {
  if(!sendstr("ACTUAL WORLD\r")) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return GetLocation("WORLD", x, y, z, yaw, pitch, roll);
}


bool ACI::KillLocation(char *loc) {
  char buf[16];
  sprintf(buf, "DLOC %s\r", loc);

  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}


bool ACI::CopyLocation(char *loc, char *dest) {
  char buf[24];
  sprintf(buf, "SET %s=%s\r", dest, loc);
  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}

bool ACI::GetLocation(char *loc, float &x, float &y, float &z, float &yaw, float &pitch, float &roll) {
  char buf[100];
  sprintf(buf, "LLOC %s\r", loc);
  if(!sendstr(buf))
    return FALSE;
  if(!recstr(buf, 72))
    return FALSE;
  if(!recstr(buf, 71))
    return FALSE;
  buf[71] = 0;
  if(sscanf(buf, "%s %f %f %f %f %f %f", buf+80, &x, &y, &z, &yaw, &pitch, &roll) != 7)
    return FALSE;
  if(get()!=0x06)
    return FALSE;	// ACK
  garble();
  return TRUE;
}


bool ACI::SetLocation(char *loc, float x, float y, float z, float yaw, float pitch, float roll) {
  char buf[100];
  sprintf(buf, "POINT %s %.4f,%.4f,%.4f,%.4f,%.4f,%.4f\r", loc, x, y, z, yaw, pitch, roll);

  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}


bool ACI::AsyncMove(char *loc) {
  char buf[16];
  sprintf(buf, "MOVE %s\r", loc);

  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) {	// ACK
    swallow();
    return FALSE;
  }
  garble();
  return TRUE;
}


bool ACI::Move(char *loc) {
  char buf[16];
  sprintf(buf, "MOVE %s\r", loc);

  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) {	// ACK
    swallow();
    return FALSE;
  }
  garble();
  if(!sendstr(buf)) return FALSE;

  char c;
  while(!get_aci_nto(&c));
  garble();
  return (c == 0x06);	// ACK
}

bool ACI::Grip(float pos)
{	char buf[26];
 sprintf(buf, "GRIP %f\r", pos);
 if(!sendstr(buf)) return FALSE;
 if(get()!=0x06) return FALSE;	// ACK
 garble();
 return TRUE;
}
bool ACI::Joint(int Index, int Angle)
{	char buf[21];
 sprintf(buf, "JOINT %d %d\r", Index, Angle);
 if(!sendstr(buf)) return FALSE;
 if(get()!=0x06) return FALSE;	// ACK
 garble();
 return TRUE;
}
bool ACI::Open(float force) {
  char buf[11];
  sprintf(buf, "OPEN %f\r", force);
  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}
bool ACI::Close(float force) {
  char buf[11];
  sprintf(buf, "CLOSE %f\r", force);
  if(!sendstr(buf)) return FALSE;
  if(get()!=0x06) return FALSE;	// ACK
  garble();
  return TRUE;
}



bool ACI::OpenSession(int port, int rate, int slave_no) {
  char buf[6];

  aci_baud = rate;
  aci_channel = port;
  aci_slave = slave_no;
  if(!initrobot(FALSE)) return FALSE;
  aci_xfr(aci_slave, 0x48, 0, 0, 'W', 1, (byte*)buf);
  if(aci_err) return FALSE;
  inz_aci();
  send(3);
  garble();

  if(!sendstr("NOH")) return FALSE;
  recstr(buf, 5);
  send('\r');
  get();
  if(get()!=0x06) return FALSE;	// ACK
  garble();

  sessionopen=TRUE;
  return TRUE;
}

void ACI::CloseSession() {
  if(sessionopen) {
    error_state(0);
    clean_aci();
    
    sendstr("HELP\r");
    get();	// ACK
    garble();

    send(26);
    send(26);
    res_aci();
  }
  sessionopen=FALSE;
}

// garble the robot controller prompt (13, 10, ">>")
void ACI::garble() {
  error_state(0);
  if(get()!=13) return;
  if(get()!=10) return;
  if(get()!='>') return;
  if(get()!='>') return;
}

// swallow any excess message chars returned by controller
void ACI::swallow() 
{
  error_state(0);
  do {
    get_aci();
  } while(aci_err!=0x40);
}


bool ACI::sendstr(char *s) {
  error_state(0);
  bool num = FALSE;
  for(int i=0; i<(int)strlen(s); i++) {
    if(!send(s[i])) return FALSE;
    if(aci_err || s[i]!=get()) return FALSE;

    if((s[i]>='0' && s[i]<='9') || s[i]=='.' || s[i]=='-') {
      if(!num) {
	get();
	get();
	get();
	get();
	num = TRUE;
      }
    }
    else num = FALSE;
  }
  return TRUE;
}

bool ACI::recstr(char *buf, int count) 
{
  error_state(0);
  for(int i=0; i<count; i++) {
    buf[i]=get();
    if(aci_err) return FALSE;
  }
  return TRUE;
}


bool ACI::send(char c) 
{
  bool done = FALSE;
  error_state(0);
  send_aci(c);
  return !aci_err;
}

char ACI::get() 
{
  char c = 0;
  error_state(0);
  do {
    c = get_aci();
  } while( c==17 || c==19 );
  return c;
}

