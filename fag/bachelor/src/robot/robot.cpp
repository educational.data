// robot.cpp: implementation of the robot class.
//
//////////////////////////////////////////////////////////////////////

#include "robot.h"
#include "crs_aci.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

robot::robot()// : pImpl(*(new robot::Impl))
{
  bot = new ACI;
}

robot::~robot()
{
  delete bot;
}

bool robot::Abort()
{
  return bot->Abort();
}


bool robot::Actual(float &x, float &y, float &z, float &yaw, float &pitch, float &roll)
{
  return bot->Actual(x,y,z,yaw,pitch,roll);
}

bool robot::AsyncMove(char *loc)
{
  return bot->AsyncMove(loc);
}

bool robot::Close(float force)
{
  return bot->Close(force);
}

bool robot::CopyLocation(char *loc, char *dest)
{
  return bot->CopyLocation(loc,dest);
}

bool robot::GetLocation(char *loc, float &x, float &y, float &z, float &yaw, float &pitch, float &roll)
{
  return bot->GetLocation(loc,x,y,z,yaw,pitch,roll);
}

bool robot::Grip(float pos)
{
  return bot->Grip(pos);
}

bool robot::Joint(int index, float Angle)
{
  return bot->Joint(index,Angle);
}

bool robot::KillLocation(char *loc)
{
  return bot->KillLocation(loc);
}

bool robot::Move(char *loc)
{
  return bot->Move(loc);
}

bool robot::Open(float force)
{
  return bot->Open(force);
}

bool robot::OpenSession(int port, int rate, int slave_no)
{
  return bot->OpenSession(port,rate,slave_no);
}

bool robot::Ready() {
  return bot->Ready();
}

bool robot::SetLocation(char *loc, float x, float y, float z, float yaw, float pitch, float roll)
{
  return bot->SetLocation(loc,x,y,z,yaw,pitch,roll);
}

bool robot::SetSpeed(int speed)
{
  return bot->SetSpeed(speed);
}

bool robot::Tool(char *loc)
{
  return bot->Tool(loc);
}

//DEL robot::robot(const robot &rob) : pImpl(rob.pImpl)
//DEL {
//DEL 	++(pImpl.refCnt);
//DEL }

void robot::CloseSession()
{
  bot->CloseSession();
}

bool robot::Home()
{
  return bot->Home();
}
