LIBFILE:=	${LIBRARY}
BINFILE:=	${BINARY}
_CXXFLAGS:=	${CXXFLAGS} -g -O2
_CFLAGS:=	${CFLAGS}
_LDFLAGS:=	${LDFLAGS}

OBJFILES:=	${shell echo "${SOURCES} "|sed "s/\.cpp \|\.c \|\.c++ \|\.cc /.o /g"}

LIB_OBJFILES:=	${shell echo "${LIB_SOURCES} "|sed "s/\.cpp \|\.c \|\.c++ \|\.cc /.o /g"}

BIN_OBJFILES:=	${shell echo "${BIN_SOURCES} "|sed "s/\.cpp \|\.c \|\.c++ \|\.cc /.o /g"}

DEPSOURCES:= ${shell echo " ${SOURCES} ${LIB_SOURCES} ${BIN_SOURCES} "|sed "s/ [^ ]*.h//g"}


CC=gcc
CXX=g++
LINK=g++

.PHONY: all clean depend dep

ifdef V
  ifeq ("$(origin V)", "command line")
    KBUILD_VERBOSE = $(V)
  endif
endif
ifndef KBUILD_VERBOSE
  KBUILD_VERBOSE = 0
endif

ifeq ($(KBUILD_VERBOSE),0)
  MAKEFLAGS=	--no-print-directory
  define Q
    @
  endef
else
  define SILENT
    >/dev/null
  endef
endif

all:
	@if ! test -r makefile.inc; then \
	  cd ..;${MAKE} $@; \
	else \
	  ${MAKE} bin; \
	fi;

test:
	@if ! test -r makefile.inc; then \
	  cd ..; ${MAKE} $@; \
	else \
	  ${MAKE} bin lib; \
	  if test -x "${BINARY}"; then \
	    ./${BINARY}; \
	  fi; \
	fi;

rebuild:
	${Q}${MAKE} clean
	${Q}${MAKE} lib
	${Q}${MAKE} bin

lib:
	${Q}if test ! -z "${LIBFILE}"; then ${MAKE} ${LIBFILE}; fi

bin:
	${Q}$(foreach dir, ${SUBDIRS}, ${MAKE} -C ${dir} RELDIR=${RELDIR}${dir}/ lib;)
	${Q}if test ! -z "${BINFILE}"; then ${MAKE} ${BINFILE}; fi

obj:${LIB_OBJFILES} ${OBJFILES}
	@sleep 0.1

${BINFILE}:${BIN_OBJFILES} ${OBJFILES} ${LIBADD} makefile
	@echo "   LINK     ${RELDIR}$@" ${SILENT}
	${Q}${LINK} ${_LDFLAGS} -o $@ ${BIN_OBJFILES} ${OBJFILES} ${LIBADD}

${LIBFILE}:${LIB_OBJFILES} ${OBJFILES} makefile
	@echo "   AR       ${RELDIR}$@" ${SILENT}
	${Q}ar cr $@ ${LIB_OBJFILES} ${OBJFILES}
	@echo "   RANLIB   ${RELDIR}$@" ${SILENT}
	${Q}ranlib  $@

%.o:%.cpp
	@echo "   C++      ${RELDIR}$@" ${SILENT}
	${Q}${CXX} ${_CXXFLAGS} -c -o $@ $<

%.o:%.cc
	@echo "   C++      ${RELDIR}$@" ${SILENT}
	${Q}${CXX} ${_CXXFLAGS} -c -o $@ $<

%.o:%.c++
	@echo "   C++      ${RELDIR}$@" ${SILENT}
	${Q}${CXX} ${_CXXFLAGS} -c -o $@ $<

%.o:%.c
	@echo "   CC       ${RELDIR}$@" ${SILENT}
	${Q}${CC} ${_CFLAGS} -c -o $@ $<

clean:
	@$(foreach dir, ${SUBDIRS}, \
	   ${MAKE} -C ${dir} RELDIR=${RELDIR}${dir}/ clean;)

	@echo "   CLEAN    ${RELDIR}" ${SILENT}
	${Q}${RM} *.o	
	${Q}${RM} *~
	${Q}${RM} *.bak
	${Q}${RM} \#*
	${Q}${RM} ${LIBFILE}
	${Q}${RM} ${BINFILE}
	$(Q)$(RM) .depend

dep depend:
	${Q}$(foreach dir, ${SUBDIRS}, ${MAKE} -C ${dir} RELDIR=${RELDIR}${dir}/ depend;)
	@echo "   DEP      ${RELDIR}$@" ${SILENT}
	${Q}${CXX} -MM ${CXXFLAGS} ${DEPSOURCES} >.depend
#	${Q}${CC} -MM ${CFLAGS} ${ccFiles} >>.depend

-include .depend