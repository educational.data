#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <iostream>
#include "System/vision/LandMarkLocator.h"
#include "System/leovision/DiodeDetector.h"
#ifdef USEGTKMM
# include "window.h" 
# include <gtkmm/main.h>
#endif


int main(int argc, char **args) {
  using namespace std;
  string name = "landmark.bmp";

  if (argc > 1)
    name = args[1];

  ipl::CImage img;

  if(!(img.Load(name.c_str()))) {
    cout << "Could not load image: " << name << endl;
    return 1;
  }
  try {
    
    tob_vision::LandmarkLocator loc;
    //leo::DiodeDetector loc;
    loc.locate(img);
#ifdef USEGTKMM
    { 
      Gtk::Main kit(__argc, __args);
      myWin win(blobs.getImage());
      //myWin win(img);
      kit.run(win);
    }
#else
    {
      if(loc.hasPoint()) {
	cout << "Maker has been found in image " << name << endl;
	cout << loc.getPoint() << endl;
      } else {
	cout << "No Marker found in image" << endl;
      }
    }    
#endif
  } catch(const exception &ex) {
    cout << "Error: " << ex.what() << endl;
    return -1;
  }
  
  return 0;
}
