// Blob.h: interface for the CBlob class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AFX_BLOB_H_INCLUDED_
#define AFX_BLOB_H_INCLUDED_

#include <vector>

namespace ipl {
  class CImage;
}
namespace tob_vision {
  template<typename t> class tpoint;

  class CBlob  
  {
  public:
    //typedef std::vector < tpoint < int > > Blob;  
    
    void add_point(const int x, const int y);
    void draw(ipl::CImage &img, int const color) const;
    void draw(ipl::CImage &img, int const color, const int y) const;
    
    //const Blob &get_points() const;
    const tpoint <int> &operator[] (const int pos) const;
    const size_t size() const;
    
    CBlob();
    CBlob(const CBlob &blb);
    virtual ~CBlob();
    const CBlob & operator +=(const CBlob &blb);
   
  private:
    std::vector < tpoint < int > >  points;
  };
}
#endif
