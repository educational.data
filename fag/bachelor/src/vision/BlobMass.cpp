// BlobMass.cpp: implementation of the CBlobMass class.
//
//////////////////////////////////////////////////////////////////////

#include "BlobMass.h"
#include "tpoint.h"
#include "Blob.h"
#include <iostream>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
namespace tob_vision {
  
  using namespace std;
  
  CBlobMass::~CBlobMass()
  {
    delete &blob;
    delete &center;
  }

  CBlobMass::CBlobMass(const CBlobMass &mass)
    :
    blob(*(new CBlob(mass.blob))),
    center(*(new tpoint < double > (mass.center))) 
  {
    
  }
  const CBlobMass &
  CBlobMass::operator=(const CBlobMass &mass)
  {
    delete &blob;
    delete &center;
    blob = *(new CBlob(mass.blob));
    center = *(new tpoint < double > (mass.center));
    return *this;
  }
  
  CBlobMass::CBlobMass(const CBlob &blb)
    : 
    blob(*(new CBlob(blb))),
    center(*(new tpoint <double>()))
  {
    long x,y;
    x = y = 1;
    
    int size=0;
    for(int refId=0;refId < blob.size();++refId) {
      // for(CBlob::Blob::const_iterator it = blob.getPoints().begin();it != blob.getPoints().end();++it) {
      // center += *it;
      if(blob[refId].x == 0 || blob[refId].y == 0) {
        cout << "Err empty point" << endl; 
        continue;
      }
      ++size;

      x += (long)(blob[refId].x);
      y += (long)(blob[refId].y);
    }
    center.x = ((double)x)/size;
    center.y = ((double)y)/size;
  }
  
  const tpoint < double > & 
  CBlobMass::get_center() const
  {
    return center;
  }
  
  const CBlob & 
  CBlobMass::get_blob() const
  {
    return blob;
  }
  
  const int 
  CBlobMass::get_mass() const {
    return blob.size();
  }
  
  
  bool dist(const CBlobMass &m1, const CBlobMass &m2,const double _dist) {
    tpoint < double > c1 = m1.get_center();
    tpoint < double > c2 = m2.get_center();
    
    double dx = c1.x - c2.x;
    double dy = c1.y - c2.y;
    
    return dx*dx + dy*dy < _dist*_dist;
  }
}
