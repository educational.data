// Histogram.h: interface for the CHistogram class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HISTOGRAM_H__919D37E3_7432_4D58_8D13_E9C62C4A0F17__INCLUDED_)
#define AFX_HISTOGRAM_H__919D37E3_7432_4D58_8D13_E9C62C4A0F17__INCLUDED_


namespace ipl {
  class CImage;
}

namespace tob_vision {
  class CHistogram  
  {
  public:
    CHistogram(const CHistogram &hist);
    CHistogram(const ipl::CImage &img, const int center);
    CHistogram(const ipl::CImage &img);
    
    const ipl::CImage & get_image() const;
    virtual ~CHistogram();
  private:
    ipl::CImage *hImage;
  };
}

#endif // !defined(AFX_HISTOGRAM_H__919D37E3_7432_4D58_8D13_E9C62C4A0F17__INCLUDED_)
