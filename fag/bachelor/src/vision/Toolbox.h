// Toolbox.h: interface for the Toolbox class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_TOOLBOX_H__07A683DD_142F_4065_B214_82DC4CD9854A__INCLUDED_)
#define AFX_TOOLBOX_H__07A683DD_142F_4065_B214_82DC4CD9854A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "tcimage.h"
#include <vector>

#include "Blobs.h"
#include "tpoint.h"

using namespace std;




class Toolbox
{
public:
	void drawMasses(tCImage &img, const BlobMasses &masses, const int color);

  Toolbox ();
  virtual ~ Toolbox ();
private:

  //vector < bool > findMaxima (const vector < long >&histBuf, int &mSize);
};

#endif // !defined(AFX_TOOLBOX_H__07A683DD_142F_4065_B214_82DC4CD9854A__INCLUDED_)
