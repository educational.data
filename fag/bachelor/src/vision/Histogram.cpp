// Histogram.cpp: implementation of the CHistogram class.
//
//////////////////////////////////////////////////////////////////////

#ifdef _WIN32_
# include "stdafx.h"
# include "show7.h"
#endif
#include "Histogram.h"
#include <vector>
#include <ipl98/cpp/image.h>

using namespace std;
using namespace ipl;

namespace tob_vision {

  vector < long > __damp_histogram(const vector < long > &histBuf);
  vector < bool > __find_maxima (const vector < long >&histBuf, int &mSize);

  //////////////////////////////////////////////////////////////////////
  // Construction/Destruction
  //////////////////////////////////////////////////////////////////////

  CHistogram::CHistogram(const CHistogram &hist)
    : hImage(0)
  {
    hImage = new CImage(*(hist.hImage));
  }

  CHistogram::~CHistogram() { 
    if(hImage) delete hImage;
  }

  CHistogram::CHistogram(const CImage &img,const int center) 
    : hImage(0)
  {
    hImage = new CImage(img.GetWidth (), img.GetHeight (), 8);
  
    for (int x = 0; x < img.GetWidth (); x++) {
      for (int y = 0; y < img.GetHeight (); y++) {
	if (img.GetPixel (x, y) > center)
	  hImage->SetPixel (x, y, 255);
	else
	  hImage->SetPixel (x, y, 0);
      }
    }
  }

  CHistogram::CHistogram(const CImage &img)
    : hImage(0)
  {
    // this process initiates a histogram
    vector < long >histBuf;
    long histCnt = 0;
    histBuf.resize (256);

    // this is one of the incompatabilities between gcc and msvcc
    int x, y;

    // Reset code
    for (x = 0; x < 256; x++)
      histBuf[x] = 0;
    for (y = 0; y < 256; y++) {
      for (x = 0; x < 256; x++) {
	int index = img.GetPixel (x, y);
	histBuf[index]++;
	if (histCnt < histBuf[index])
	  histCnt = histBuf[index];
      }
    }
    int maximasize = 0;
    vector < bool > maxs = __find_maxima (histBuf, maximasize);
    while (maximasize > 2) {
      histBuf = __damp_histogram (histBuf);
      maxs = __find_maxima (histBuf, maximasize);
    }
    int b1, b2;
    b1 = b2 = 0;
    {
      bool found1 = false;
      for (y = 0; y < 256; y++) {
	if (maxs[y] == true) {
	  if (!found1) {
	    b1 = y;
	    found1 = true;
	  }
	  else {
	    b2 = y;
	  }
	}
      }
    }
    hImage = new CImage(img.GetWidth (), img.GetHeight (), 8);
    int center = ((b1 + b2) / 2);
    for (x = 0; x < img.GetWidth (); x++) {
      for (y = 0; y < img.GetHeight (); y++) {
	if (img.GetPixel (x, y) > center)
	  hImage->SetPixel (x, y, 255);
      	else
	  hImage->SetPixel (x, y, 0);
      }
    }
  }

  const CImage & 
  CHistogram::get_image() const
  {
    return *hImage;
  }

  vector < long >
  __damp_histogram(const vector < long > &hist) {
    vector < long >tempBuf (256);
    tempBuf[0] = (hist[0] + hist[1]) / 2;
    for (int i = 1; i < 255; i++) {
      tempBuf[i] = (hist[i] +	hist[i - 1] + hist[i + 1]) / 3;
    }
    tempBuf[255] = (hist[255] + hist[254]) / 2;
    return tempBuf;
  }


  vector < bool >
  __find_maxima (const vector < long >&histBuf, int &mSize)
  {
    mSize = 0;
    vector < bool > maxs (256);
    if (histBuf[0] > histBuf[1]) {
      mSize++;
      maxs[0] = true;
    }
    for (int x = 1; x < 255; x++) {
      if (histBuf[x] >= histBuf[x - 1] && histBuf[x] > histBuf[x + 1]) {
	mSize++;
	maxs[x] = true;
      }
    }
    if (histBuf[255] > histBuf[254]) {
      mSize++;
      maxs[255] = true;
    }
    return maxs;
  }
}
