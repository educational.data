// Blobs.h: interface for the CBlobs class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __BLOBS_H_INCLUDED_
#define __BLOBS_H_INCLUDED_


#include <vector>

namespace ipl {
  class CImage;
}

namespace tob_vision {
  class CHistogram;
  class CBlob;

  class CBlobs  
  {
  public:
    CBlobs(const CHistogram &hist);
    const ipl::CImage & get_image() const;
    const CBlob &operator[](const int);
    const size_t size() const;
    virtual ~CBlobs();
    
  private: // member variables
    ipl::CImage &hImage;    
    std::vector < CBlob > &blobs;
    
  };
}

#endif // !defined(AFX_BLOBS_H__398B934D_C8BB_4BDC_B752_B48CA5BBCFAE__INCLUDED_)
