// Toolbox.cpp: implementation of the Toolbox class.
//
//////////////////////////////////////////////////////////////////////
#ifdef WIN32
#include "stdafx.h"
#include "show7.h"
#endif // WIN32
#include "Toolbox.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Toolbox::Toolbox()
{

}

Toolbox::~Toolbox()
{

}

//DEL void Toolbox::createHistogram (CImage & img1,
//DEL 			  CImage & img2)
//DEL {
//DEL 
//DEL   vector < long >histBuf;
//DEL   long histCnt = 0;
//DEL   histBuf.resize (256);
//DEL 
//DEL   //Reset code
//DEL   for (int x = 0; x < 256; x++)
//DEL     histBuf[x] = 0;
//DEL 
//DEL   //calculate image
//DEL   img2 = CImage (512, 256, 8);
//DEL 
//DEL   for (int y = 0; y < 256; y++) {
//DEL     for (int x = 0; x < 256; x++) {
//DEL       int index = img1.GetPixel (x, y);
//DEL       histBuf[index]++;
//DEL       if (histCnt < histBuf[index])
//DEL 	histCnt = histBuf[index];
//DEL     }
//DEL   }
//DEL   int maximasize = 0;
//DEL 
//DEL   vector < bool > maxs = findMaxima (histBuf, maximasize);
//DEL 
//DEL   //generate the base histogram
//DEL   generateHistogram (img2, histBuf, histCnt, maxs, 0);
//DEL 
//DEL   int numRuns = 0;
//DEL 
//DEL   while (maximasize > 2) {
//DEL     histBuf = dampHistogram (histBuf);
//DEL     maxs = findMaxima (histBuf, maximasize);
//DEL     numRuns++;
//DEL   }
//DEL   generateHistogram (img2, histBuf, histCnt, maxs, 256);
//DEL 
//DEL   int b1, b2;
//DEL   b1 = b2 = 0;
//DEL   {
//DEL     bool found1 = false;
//DEL     for (int y = 0; y < 256; y++) {
//DEL       if (maxs[y] == true) {
//DEL 	if (!found1) {
//DEL 	  b1 = y;
//DEL 	  found1 = true;
//DEL 	}
//DEL 	else {
//DEL 	  b2 = y;
//DEL 	}
//DEL       }
//DEL     }
//DEL   }
//DEL   int center = ((b1 + b2) / 2);
//DEL   for (x = 0; x < img1.GetWidth (); x++) {
//DEL     for (int y = 0; y < img1.GetHeight (); y++) {
//DEL       int index = img1.GetPixel (x, y);
//DEL       if (index < center)
//DEL 	img1.SetPixelFast (x, y, 0);
//DEL       else
//DEL 	img1.SetPixelFast (x, y, 0xFFFFFF);
//DEL     }
//DEL   }
//DEL }



// vector < long >
//DEL Toolbox::dampHistogram (const vector < long > histBuf)
//DEL {
//DEL   vector < long >tempBuf (256);
//DEL   tempBuf[0] = (histBuf[0] + histBuf[1]) / 2;
//DEL   for (int index = 1; index < 255; index++) {
//DEL     tempBuf[index] = (histBuf[index] +
//DEL 		      histBuf[index - 1] + histBuf[index + 1]) / 3;
//DEL 
//DEL   }
//DEL   tempBuf[255] = (histBuf[255] + histBuf[254]) / 2;
//DEL   return tempBuf;
//DEL }

/*vector < bool > Toolbox::findMaxima (const vector < long >&histBuf,
				     int &mSize)
{
  mSize = 0;
  vector < bool > maxs (256);
  if (histBuf[0] > histBuf[1]) {
    mSize++;
    maxs[0] = true;
  }
  for (int x = 1; x < 255; x++) {
    if (histBuf[x] >= histBuf[x - 1] && histBuf[x] > histBuf[x + 1]) {
      mSize++;
      maxs[x] = true;
    }
  }
  if (histBuf[255] > histBuf[254]) {
    mSize++;
    maxs[255] = true;
  }
  return maxs;
}*/

/* void
Toolbox::generateHistogram (CImage & img,
			    const vector < long >&histBuf,
			    const long histCnt,
			    vector < bool > &maxs, int offset)
{

  int col = 255;

  for (int x = 0; x < 255; x++) {
    int val = (255 * histBuf[x] / histCnt);
    val *= (-1);
    val += 255;
    if (maxs[x] == true)
      col = 200;
    else
      col = 255;

    for (int y = 0; y < val; y++)
      img.SetPixel (offset + x, y, col);
  }
}*/

// CImage
//DEL Toolbox::createBlobs (const CImage &img)
//DEL {
//DEL   //CImage img = adaptiveHistogram (pimg);
//DEL   using namespace std;
//DEL   int cmbs = 0;
//DEL   blbcnt = 0;
//DEL   int w = img.GetWidth ();
//DEL   int h = img.GetHeight ();
//DEL   int x, y;
//DEL 
//DEL   unsigned int A, B;
//DEL 
//DEL   unsigned int state = 0;
//DEL 
//DEL 
//DEL   try {
//DEL     CImage blobIndexMap (w, h, 24, 0xFFFFFF);
//DEL 
//DEL     for (y = 1; y < img.GetHeight (); y++) {
//DEL       for (x = 1; x < img.GetWidth (); x++) {
//DEL 
//DEL 	//optimization...
//DEL 	//if (img.GetPixel (x, y) == 0xFF) continue;
//DEL 	//if(img.GetPixel (x, y) == 0) continue;
//DEL 
//DEL 	state = 0;
//DEL 
//DEL 	A = appendNorth (img, blobIndexMap, x, y);
//DEL 	B = appendWest (img, blobIndexMap, x, y);
//DEL 	
//DEL 	if (A == B && A != 0xFFFFFF) {
//DEL 	  state = 1;
//DEL 	  blobIndexMap.SetPixelFast (x, y, A);
//DEL 	  blobs.at (A).push_back (tPoint(x, y));
//DEL 	}
//DEL 	else if (A != 0xFFFFFF && B != 0xFFFFFF && A != B) {
//DEL 	  state = 20;
//DEL 	  combineBlobs (blobIndexMap, A, B);
//DEL 	  blobIndexMap.SetPixelFast (x, y, A);
//DEL 	  blobs.at (A).push_back (tPoint(x, y));
//DEL 	}
//DEL 	else if (B == 0xFFFFFF && A == 0xFFFFFF) {
//DEL 	  state = 3;
//DEL 	  int index = blobs.size ();
//DEL 	  blobIndexMap.SetPixelFast (x, y, index);
//DEL 	  Blob blob;
//DEL 	  blob.push_back (tPoint(x,y));
//DEL 	  blobs.push_back (blob);
//DEL 	}
//DEL 	else if (A != 0xFFFFFF && B == 0xFFFFFF) {
//DEL 	  state = 4;
//DEL 	  blobIndexMap.SetPixelFast (x, y, A);
//DEL 	  blobs.at (A).push_back (tPoint(x,y));
//DEL 	}
//DEL 	else if (B != 0xFFFFFF && A == 0xFFFFFF) {
//DEL 	  state = 5;
//DEL 	  blobIndexMap.SetPixelFast (x, y, B);
//DEL 	  blobs.at (B).push_back (tPoint(x,y));
//DEL 	}
//DEL       }
//DEL     }
//DEL     /*// opret et lidt mere farverigt billede af blobs'ene
//DEL     int ci = 0;
//DEL     for (Blobs::iterator it = blobs.begin (); it != blobs.end (); it++) {
//DEL       unsigned int col = (ci++) * 0x222222;
//DEL       for (Blob::iterator itb = it->begin (); itb != it->end (); itb++) {
//DEL 	blobIndexMap.SetPixel (itb->x, itb->y, col);
//DEL       }
//DEL     } */
//DEL     return blobIndexMap;	// normal return
//DEL   }
//DEL   catch (exception & ex) {
//DEL     cout << "Error: " << ex.what () << endl;
//DEL     cout << "Data dump:" << endl;
//DEL     cout << "X: " << x << endl;
//DEL     cout << "Y: " << y << endl;
//DEL     cout << "A: " << A << endl;
//DEL     cout << "B: " << B << endl;
//DEL     cout << "Size of blobs: " << blobs.size () << endl;
//DEL     cout << "Current state: " << state << endl;
//DEL     cout << "-------------------------------" << endl;
//DEL     cout << flush;
//DEL     throw ex;
//DEL   }
//DEL   catch (...) {
//DEL     cout << "Unknown exception" << endl;
//DEL     cout << "Data dump:" << endl;
//DEL     cout << "X: " << x << endl;
//DEL     cout << "Y: " << y << endl;
//DEL     cout << "A: " << A << endl;
//DEL     cout << "B: " << B << endl;
//DEL     cout << "Size of blobs: " << blobs.size () << endl;
//DEL     cout << "Current state: " << state << endl;
//DEL     cout << "-------------------------------" << endl;
//DEL     cout << flush;
//DEL   }
//DEL   return CImage ();
//DEL }

//DEL void
//DEL inline Toolbox::combineBlobs (CImage & blobIndex, unsigned int &A, unsigned int B)
//DEL {
//DEL   typedef Blob::iterator vit;
//DEL   for (vit it = blobs.at (B).begin (); it < blobs.at (B).end (); it++)
//DEL     blobs.at (A).push_back (*it);
//DEL 
//DEL   for (it = blobs.at (A).begin (); it < blobs.at (A).end (); it++)
//DEL     blobIndex.SetPixel (it->x, it->y, A);
//DEL 
//DEL   //we must remove B
//DEL   if (B == blobs.size () - 1)
//DEL     blobs.pop_back ();
//DEL   else {
//DEL     //move the blobs from the last into B 's old position
//DEL     blobs[B] = blobs[blobs.size () - 1];
//DEL 
//DEL     //in the special case where A is the last blob, we 
//DEL     // must make sure to reset A to its new state.
//DEL     if (A == (blobs.size () - 1))
//DEL       A = B;
//DEL     blobs.pop_back ();
//DEL     vit vend = blobs.at (B).end ();
//DEL     for (vit it = blobs.at (B).begin (); it < vend; it++) {
//DEL       blobIndex.SetPixel (it->x, it->y, B);
//DEL     }
//DEL   }
//DEL }

//DEL int
//DEL inline Toolbox::appendNorth (const CImage & img, const CImage & blobIndex, int x,
//DEL 		      int y)
//DEL {
//DEL   if (y == 0)
//DEL     return 0xFFFFFF;
//DEL 
//DEL   if (img.GetPixelFast (x, y) == img.GetPixelFast (x, y - 1))
//DEL     return blobIndex.GetPixelFast (x, y - 1);
//DEL   else
//DEL     return 0xFFFFFF;
//DEL }

//DEL int
//DEL inline Toolbox::appendWest (const CImage & img, const CImage & blobIndex, int x,
//DEL 		     int y)
//DEL {
//DEL   if (x == 0)
//DEL     return 0xFFFFFF;
//DEL   if (img.GetPixelFast (x, y) == img.GetPixelFast (x - 1, y))
//DEL     return blobIndex.GetPixelFast (x - 1, y);
//DEL   else
//DEL     return 0xFFFFFF;
//DEL }


//DEL CImage Toolbox::adaptiveHistogram (CImage img)
//DEL {
//DEL   vector < long >histBuf;
//DEL   long histCnt = 0;
//DEL   histBuf.resize (256);
//DEL 
//DEL   //Reset code
//DEL   for (int x = 0; x < 256; x++)
//DEL     histBuf[x] = 0;
//DEL   for (int y = 0; y < 256; y++) {
//DEL     for (int x = 0; x < 256; x++) {
//DEL       int index = img.GetPixel (x, y);
//DEL       histBuf[index]++;
//DEL       if (histCnt < histBuf[index])
//DEL 	histCnt = histBuf[index];
//DEL     }
//DEL   }
//DEL   int maximasize = 0;
//DEL   vector < bool > maxs = findMaxima (histBuf, maximasize);
//DEL   while (maximasize > 2) {
//DEL     histBuf = dampHistogram (histBuf);
//DEL     maxs = findMaxima (histBuf, maximasize);
//DEL   }
//DEL   int b1, b2;
//DEL   b1 = b2 = 0;
//DEL   {
//DEL     bool found1 = false;
//DEL     for (int y = 0; y < 256; y++) {
//DEL       if (maxs[y] == true) {
//DEL 	if (!found1) {
//DEL 	  b1 = y;
//DEL 	  found1 = true;
//DEL 	}
//DEL 	else {
//DEL 	  b2 = y;
//DEL 	}
//DEL       }
//DEL     }
//DEL   }
//DEL   CImage retImg (img.GetWidth (), img.GetHeight (), 8);
//DEL   int center = ((b1 + b2) / 2);
//DEL   for (x = 0; x < img.GetWidth (); x++) {
//DEL     for (int y = 0; y < img.GetHeight (); y++) {
//DEL       if (img.GetPixel (x, y) > center)
//DEL 	retImg.SetPixel (x, y, 255);
//DEL 
//DEL       else
//DEL 	retImg.SetPixel (x, y, 0);
//DEL     }
//DEL   }
//DEL   return retImg;
//DEL }

//DEL void Toolbox::drawBlobs(const Blobs &blobs, CImage &img, const int maxcolor)
//DEL {
//DEL 	int index = 0;
//DEL 	int inc = maxcolor/blobs.size();
//DEL 	for(Blobs::const_iterator it = blobs.begin();it != blobs.end();it++) {
//DEL 		drawBlob(img,*it,index);
//DEL 		index+=inc;
//DEL 	}
//DEL }

//DEL void Toolbox::drawBlob(CImage &img, const Blob &blob, const int color)
//DEL {
//DEL 	for(Blob::const_iterator pix = blob.begin();pix != blob.end();++pix) {
//DEL 		img.SetPixelFast(pix->x,pix->y,color);
//DEL 	}
//DEL }

void Toolbox::drawMasses(tCImage &img, const BlobMasses &masses, const int color)
{
	for(BlobMasses::const_iterator pix = masses.begin();pix != masses.end();++pix) {
		tpoint< double > pt = pix->get_center();
		tpoint< int > p;
		p.x= static_cast<int>(0.5 + pt.x);
		p.y= static_cast<int>(0.5 + pt.y);
		img.SetPixel(p.x-1,p.y-1,color);
		img.SetPixel(p.x,  p.y-1,color);
		img.SetPixel(p.x+1,p.y-1,color);
		img.SetPixel(p.x-1,p.y  ,color);
		img.SetPixel(p.x,  p.y  ,color);
		img.SetPixel(p.x+1,p.y  ,color);
		img.SetPixel(p.x-1,p.y+1,color);
		img.SetPixel(p.x,  p.y+1,color);
		img.SetPixel(p.x+1,p.y+1,color);
		/*img.SetPixel(p.x-1,p.y+2,color);
		img.SetPixel(p.x  ,p.y+2,color);
		img.SetPixel(p.x+1,p.y+2,color);
		img.SetPixel(p.x+2,p.y+2,color);
		img.SetPixel(p.x+2,p.y-1,color);
		img.SetPixel(p.x+2,p.y  ,color);
		img.SetPixel(p.x+2,p.y+1,color); */
		
	}
		
}


