///////////////////////////////////////////////
// compilation interface for CImage
///////////////////////////////////////////////

#include "tcimage.h"
#include <ipl98/cpp/image.h>
#include <points/point2d.h>

class ptCImage::dataHolder {
public:
  ipl::CImage *pImg;
};

ptCImage::ptCImage() {
  d = new dataHolder;
  d->pImg = new ipl::CImage();
}

ptCImage::ptCImage(const ptCImage &img) {
  d = new dataHolder;
  d->pImg = new ipl::CImage(*(img.d->pImg));
}

ptCImage::ptCImage(const int w, const int h, const int dp) {
  d = new dataHolder;
  d->pImg = new ipl::CImage(w,h,dp);
}

ptCImage::ptCImage(const int w, const int h, const int dp, const int def) {
  d = new dataHolder;
  d->pImg = new ipl::CImage(w,h,dp,def);
}

ptCImage::~ptCImage() {
  delete d->pImg;
  delete d;
}

const ptCImage &
ptCImage::operator=(const ptCImage &img) {
  *(d->pImg) = *(img.d->pImg);
  return *this;
}

int
ptCImage::GetWidth() const {
  return d->pImg->GetWidth();
}

int
ptCImage::GetHeight() const {
  return d->pImg->GetHeight();
}

int 
ptCImage::GetPixel(const int x, const int y) const {
  return d->pImg->GetPixel(x,y);
}

int 
ptCImage::GetPixelFast(const int x, const int y) const {
  return d->pImg->GetPixelFast(x,y);
}

void 
ptCImage::SetPixel(const int x, const int y, const int col) {
  d->pImg->SetPixel(x,y,col);
}

void 
ptCImage::SetPixelFast(const int x, const int y, const int col) {
  d->pImg->SetPixelFast(x,y,col);
}

void 
ptCImage::Save(const char * name) {
  d->pImg->Save(name);
}
  
void 
ptCImage::Load(const char * name) {
  d->pImg->Load(name);
}

void
ptCImage::DrawLine(const int x1,
		  const int y1,
		  const int x2,
		  const int y2,
		  const int color) {
  CPoint2D< int > p1(x1,y1), p2(x2,y2);
  d->pImg->DrawLine(p1,p2,color);
}
