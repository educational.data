#ifndef __TCIMAGE_H__
#define __TCIMAGE_H__

// #define FASTCOMPILE

#ifndef FASTCOMPILE
#include <ipl98/cpp/image.h>
#endif

#ifndef _WIN32
class ptCImage {
  class dataHolder;
  dataHolder *d;
public:
  ptCImage();
  ptCImage(const ptCImage &img);
  ptCImage(const int w, const int h, const int dp);
  ptCImage(const int w, const int h, const int dp, const int def);

  virtual ~ptCImage();

  int GetWidth() const;
  int GetHeight() const;

  const ptCImage &operator=(const ptCImage &img);
  
  int GetPixel(const int x, const int y) const;
  int GetPixelFast(const int x, const int y) const;
  void SetPixelFast(const int x, int y, const int col);
  void SetPixel(const int x, int y, const int col);
  void DrawLine(const int x1, 
		const int y1, 
		const int x2, 
		const int y2, 
		const int color);

  void Save(const char *name);
  void Load(const char *name);
};
#endif

#ifdef FASTCOMPILE
typedef ptCImage tCImage;
#else
typedef ipl::CImage tCImage;
#endif
  
#endif // __TCIMAGE_H__
