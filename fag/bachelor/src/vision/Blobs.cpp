#include "Blobs.h"
#include "Blob.h"
#include "Histogram.h"
#include <iostream>
#include <ipl98/cpp/image.h>
#include "tpoint.h"

namespace tob_vision {

  using namespace ipl;
  using namespace std;

  // first define all the inline functions;
  int inline 
  append_north (const CImage & img, const CImage & blobIndex, int x, int y)
  {
    if (y == 0)
      return 0xFFFFFF;

    if (img.GetPixelFast (x, y) == img.GetPixelFast (x, y - 1))
      return blobIndex.GetPixelFast (x, y - 1);
    else
      return 0xFFFFFF;
  }

  inline int 
  append_west (const CImage & img, const CImage & blobIndex, int x,
			     int y)
  {
    if (x == 0)
      return 0xFFFFFF;
    if (img.GetPixelFast (x, y) == img.GetPixelFast (x - 1, y))
      return blobIndex.GetPixelFast (x - 1, y);
    else
      return 0xFFFFFF;
  }

  inline void
  combine_blobs (vector <CBlob> &blobs, 
		 CImage & blobIndex, 
		 unsigned int &A, 
		 unsigned int B, 
		 const int y)
  {
    // CBlob::Blob::iterator it;
    try {
      blobs[B].draw(blobIndex,A);
      blobs[A] += blobs[B];
    
      //we must remove B
      if (B != blobs.size () - 1) {
	//move the blob from the last into B 's old position
	blobs[B] = blobs[blobs.size () - 1];
	blobs[B].draw(blobIndex,B);
      
	//in the special case where A is the last blob, we 
	// must make sure to reset A to its new state.
	if (A == (blobs.size () - 1)) 
	  A = B;
      }
      blobs.pop_back ();
    } catch(const exception &ex) {
      cerr << "Exception thrown: " << ex.what() << endl;
      throw ex;
    }  
  }

  
  CBlobs::~CBlobs()
  {
    if(&hImage) delete &hImage;
    if(&blobs) delete &blobs;
  }
  
  CBlobs::CBlobs(const CHistogram &hist) :
    hImage (*(new CImage(hist.get_image().GetWidth(), 
			 hist.get_image().GetHeight(),
			 24,0xffffff))),
    blobs(*(new vector < CBlob >() ))
  {
    CImage img = hist.get_image();
    int cmbs = 0;
    int w = img.GetWidth ();
    int h = img.GetHeight ();
    unsigned int x, y;

    unsigned int A, B;

    try {
      for (y = 1; y < h; ++y) {
	      for (x = 1; x < w; ++x) {
		      
	        A = append_north (img, hImage, x, y);
	        B = append_west (img, hImage, x, y);

	        if (A == B && A != 0xFFFFFF) {
	          hImage.SetPixelFast (x, y, A);
	          blobs[A].add_point ( x, y );
	        }
	        else if (A != 0xFFFFFF && B != 0xFFFFFF && A != B) {
	          combine_blobs (blobs, hImage, A, B,y);
	          hImage.SetPixelFast (x, y, A);
	          blobs[A].add_point ( x, y );
	        }
	        else if (B == 0xFFFFFF && A == 0xFFFFFF) {
	          int index = blobs.size ();
	          hImage.SetPixelFast (x, y, index);
	          CBlob blob;
	          blob.add_point( x, y );
	          blobs.push_back (blob);
	        }
	        else if (A != 0xFFFFFF && B == 0xFFFFFF) {
	          hImage.SetPixelFast (x, y, A);
	          blobs[A].add_point(x,y);
	        }
	        else if (B != 0xFFFFFF && A == 0xFFFFFF) {
	          hImage.SetPixelFast (x, y, B);
	          blobs[B].add_point(x,y);
	        }
	      }
      }
    }
    catch (exception & ex) {
      cout << "Error: " << ex.what () << endl;
      cout << "Data dump:" << endl;
      cout << "X: " << x << endl;
      cout << "Y: " << y << endl;
      cout << "A: " << A << endl;
      cout << "B: " << B << endl;
      cout << "Size of blobs: " << blobs.size () << endl;
      //cout << "Current state: " << state << endl;
      cout << "-------------------------------" << endl;
      cout << flush;
      throw ex;
    }
    catch (...) {
      cout << "Unknown exception" << endl;
      cout << "Data dump:" << endl;
      cout << "X: " << x << endl;
      cout << "Y: " << y << endl;
      cout << "A: " << A << endl;
      cout << "B: " << B << endl;
      cout << "Size of blobs: " << blobs.size () << endl;
      //cout << "Current state: " << state << endl;
      cout << "-------------------------------" << endl;
      cout << flush;
    }
  }

  const CImage & CBlobs::get_image() const
  {
    return hImage;
  }


  const CBlob & CBlobs::operator [](const int refId)
  {
    return blobs[refId];
  }
  const size_t
  CBlobs::size() const 
  {
    return blobs.size();
  }
}
