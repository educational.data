#ifndef _LANDMARKLOCATER_H_INCLUDED__
#define _LANDMARKLOCATER_H_INCLUDED__

// #include "tcimage.h"
#include "../locater.h"
#include <points/point2d.h>
namespace tob_vision {
  class LandmarkLocator : public Locater {
  public:
    LandmarkLocator();
    virtual ~LandmarkLocator();
    
  public: // overridden functions from locater
    bool locate(const ipl::CImage &img);
    bool hasPoint() const;
    CPoint2D<double> getPoint() const;
    
  private:
    CPoint2D<double> center;
    bool isFound;
  };
}

#endif
