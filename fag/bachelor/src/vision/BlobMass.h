// BlobMass.h: interface for the CBlobMass class.
//
//////////////////////////////////////////////////////////////////////

#ifndef AFX_BLOBMASS_H_INCLUDED_
#define AFX_BLOBMASS_H_INCLUDED_

#include <vector>

namespace tob_vision {
  template<typename t> class tpoint;
  class CBlob;
  class CBlobMass  
  {
    CBlob &blob;
    tpoint < double > &center;
  public:
    const CBlob & get_blob() const;
    const tpoint < double > &get_center() const;
    const int get_mass() const;
    
    CBlobMass(const CBlob &blob);
    CBlobMass(const CBlobMass &mass);
    const CBlobMass &operator=(const CBlobMass &mass);
    virtual ~CBlobMass();
  };
  
  typedef std::vector< CBlobMass > BlobMasses;

  bool dist(const CBlobMass &m1, const CBlobMass &m2, const double); 
}

#endif
