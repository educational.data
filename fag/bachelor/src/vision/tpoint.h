#ifndef __HEADER_TPOINT_H__
#define __HEADER_TPOINT_H__

namespace tob_vision {
  template <typename T>
    class tpoint {
    public:
    tpoint() {
      x=y=0;
    }
    tpoint(T X, T Y) {
      x=X;
      y=Y;
    }
    bool Near(T X, T Y, T max) {
      //return (x-X)+(y-Y) < max;
      
      return (x-X)*(x-X)+(y-Y)*(y-Y) < max*max;
    }
    bool Near(tpoint<T> p, T max) {
      return (x-p.x)*(x-p.x)+(y-p.y)*(y-p.y) < max*max;
    }
    const tpoint <T> operator+(const tpoint < T > &ref) {
      tpoint <T> n;
      n.x = x + ref.x;
      n.y = y + ref.y;
      return n;
    }
    const tpoint <T> operator=(const tpoint < T > &ref) {
      x=ref.x;
      y=ref.y;
      return *this;
    }
    const tpoint <T> operator +=(const tpoint < T > &ref) {
      x += ref.x;
      y += ref.y;
      return *this;
    }
    const tpoint <T> operator/(const T val) {
      tpoint < T > n;
      n.x = x/val;
      n.y = y/val;
      return n;
    }
    const tpoint <T> operator=(const T val) {
      x=val;
      y=val;
      return *this;
    }
    const tpoint <T> operator +=(const T val) {
      x += val;
      y += val;
      return *this;
    }
    T x, y;
  };

  typedef tpoint<int> tPoint;
}

#endif // tpoint_header
