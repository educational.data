// Blob.cpp: implementation of the CBlob class.
//
//////////////////////////////////////////////////////////////////////
#ifdef _WIN32_
# include "../win32/stdafx.h"
#endif

#include "Blob.h"
#include <ipl98/cpp/image.h>
#include "tpoint.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

namespace tob_vision {
  CBlob::CBlob(const CBlob &blob)
    : points(blob.points)
  {

  }
  
  CBlob::CBlob()
  {
    points.reserve(1000);
  }

  CBlob::~CBlob()
  {

  }

  void 
  CBlob::draw(ipl::CImage &img, const int color) const
  {
    std::vector < tpoint < int > >::const_iterator it;
  
    for(it = points.begin();it != points.end();it++)
      img.SetPixelFast(it->x,it->y,color);
  }

  void 
  CBlob::draw(ipl::CImage &img, const int color, const int y) const
  {
    std::vector < tpoint < int > >::const_reverse_iterator it;
    for(it = points.rbegin();it != points.rend() && it->y == y;++it)
      img.SetPixelFast(it->x,it->y,color);
  }

  void 
  CBlob::add_point(const int x, const int y) {
    points.push_back(tpoint< int >(x,y) );
  }

  /* const CBlob::Blob &
     CBlob::get_points() const {
     return points;
     }*/

  const tpoint < int > &
  CBlob::operator[] (const int pos) const {
    return points[pos];
  }


  const CBlob & 
  CBlob::operator +=(const CBlob &blb)
  {
    points.insert(points.begin(),blb.points.begin(),blb.points.end());
    return *this;
  }

  const size_t 
  CBlob::size() const
  {
    return points.size();
  }
}
