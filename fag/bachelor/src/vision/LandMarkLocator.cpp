#include "LandMarkLocator.h"
#include "tpoint.h"
#include "Histogram.h"
#include "BlobMass.h"
#include "Blobs.h"
#include "Blob.h"


#include <iostream>
#include <ipl98/cpp/image.h>


namespace tob_vision {
  using namespace ipl;
  using namespace std;
  
  typedef vector < CBlob > Blobs;
  
  LandmarkLocator::LandmarkLocator() {
    isFound = false;
  }

  bool LandmarkLocator::locate(const ipl::CImage & img) {
    using namespace std;
    isFound = false;
    try { 
      CBlobs cblobs(CHistogram(img,120));
    
      /**
       * Decide which blobs that is reasonally small enough to
       * be eligible to a landmark.
       */
      Blobs blobs;
      vector< CBlobMass > masses;

      int numFound = 0;
      for(int pos_blobs = 0;pos_blobs < cblobs.size();++pos_blobs) {
	      if(cblobs[pos_blobs].size() > 30 && cblobs[pos_blobs].size() < 1000) {
	        blobs.push_back(cblobs[pos_blobs]);
	        masses.push_back(CBlobMass(cblobs[pos_blobs]));
	        ++numFound;
	      }
      }
    
      /**
       * There got to be at least 3 blobs:
       *  - Outher black ring
       *  - Inner White ring
       *  - Center black spot
       */
      if (numFound<3)
	      return isFound = false;
    
      /**
       * Calculations is done based on
       * center of masses.
       * This is a point that is questionably
       * is it the true center of the circles???
       */
      vector<CBlobMass> newMasses;
    
      blobs.resize(0);
      for (vector<CBlobMass>::iterator msit = masses.begin();msit != masses.end();++msit) {
	      vector<CBlobMass> lMass;
	      for (vector<CBlobMass>::iterator mssb = msit;mssb != masses.end();++mssb) {
	        if(dist(*msit, *mssb, 3)) lMass.push_back(*msit);
	      }
	      if(lMass.size()==3) { // more than two blobs are near this one
          for(vector<CBlobMass>::iterator ims = lMass.begin();ims != lMass.end();++ims) {
	          newMasses.push_back(*ims);
          }
	      }
      }
      /**
       * Have we found the needed cells?
       */    
      if(newMasses.size() == 0) {
	      return isFound = false;
      } else {
	      /**
	       * mark the cells as found
	       */
        tpoint<double> tcenter(0,0);
        for (vector<CBlobMass>::iterator mssb = newMasses.begin();mssb != newMasses.end();++mssb) {
          tcenter+=mssb->get_center();
        }
	      center.Set(tcenter.x / newMasses.size(),
		      tcenter.y / newMasses.size()
		    );

	      isFound = true;
	      return true;
      }
    } catch(const exception &ex) {
      cout << "Exception thrown: " << ex.what() << endl;
      return false;
    }
  }

  LandmarkLocator::~LandmarkLocator() {
  
  }

  CPoint2D<double> 
  LandmarkLocator::getPoint() const {
    if(isFound)
      return center;
    else
      return CPoint2D<double> ();
  }

  bool
  LandmarkLocator::hasPoint() const {
    return isFound;
  }
}
