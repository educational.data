#ifndef __SIG_H_INCLUDED__
#define __SIG_H_INCLUDED__

#include <vector>

namespace sig {
  template<typename RT, typename AT1, typename AT2> 
  class Functor2 {
  public:
    virtual RT emit(AT1,AT2) const = 0;
    virtual ~Functor2() { };
  };

  template<typename RT, typename AT1> 
  class Functor1 {
  public:
    virtual RT emit(AT1) const = 0;
    virtual ~Functor1() { };
  };

  template<typename RT> 
  class Functor0 {
  public:
    virtual RT emit() const = 0;
    virtual ~Functor0() { };
  };

  template<class T, typename RT, typename AT1, typename AT2>
  class Functor2C : public Functor2<RT,AT1,AT2> {
    typedef RT (T::*function)(AT1,AT2);
  
    function func;
    T *object;
  public:
    Functor2C(T *obj,function f) {
      object = obj;
      func = f;
    }

    RT emit(AT1 a,AT2 b) const {
      return (object->*func)(a,b);
    };
    ~Functor2C() {};
  };

  template<class T, typename RT, typename AT1>
  class Functor1C : public Functor1<RT,AT1> {
    typedef RT (T::*function)(AT1);
  
    function func;
	  T *object;
  public:
    Functor1C(T *obj,function f) {
      object = obj;
      func = f;
    }

    RT emit(AT1 a) const {
	    return (object->*func)(a);
    };
    ~Functor1C() {};
  };

  template<class T, typename RT>
  class Functor0C : public Functor0<RT> {
    typedef RT (T::*function)();
  
    function func;
    T *object;
  public:
    Functor0C(T *obj,function f) {
      object = obj;
      func = f;
    }

    RT emit() const {
      return (object->*func)();
    };
    ~Functor0C() {};
  };

  template<class RT, class AT1 , class AT2> 
  class signal2 {
    typedef Functor2<RT,AT1,AT2> Functor;
    typedef std::vector<Functor *> FuncVector;
    typedef typename FuncVector::const_iterator FuncIterator;

    FuncVector funcs;
  public:
    void connect(Functor *functor) {
      funcs.push_back(functor);
    }

    RT operator()(AT1 a, AT2 b) const {
      return emit(a,b);
    }

    RT emit(AT1 a, AT2 b) const {
      RT ret;
      FuncIterator it = funcs.begin();
      for(;it!=funcs.end();++it) {
	      Functor *func = *it;
	      ret = func->emit(a,b);
      }
      return ret;
    }
  };

  template<class RT, class AT1> 
  class signal1 {
    typedef Functor1<RT,AT1> Functor;
    typedef std::vector<Functor *> FuncVector;
    typedef typename FuncVector::const_iterator FuncIterator;

    FuncVector funcs;
  public:
    void connect(Functor *functor) {
      funcs.push_back(functor);
    }
    RT operator()(AT1 a) const {
      return emit(a);
    }
    RT emit(const AT1 a) const {
      RT ret;
      FuncIterator it = funcs.begin();
      for(;it!=funcs.end();++it) {
        Functor *func = *it;
        ret = func->emit(a);
      }
      return ret;
    }
  };


  template<class RT> 
  class signal0 {
    typedef Functor0<RT> Functor;
    typedef std::vector<Functor *> FuncVector;
    typedef typename FuncVector::const_iterator FuncIterator;

    FuncVector funcs;
  public:
    void connect(Functor *functor) {
      funcs.push_back(functor);
    }
	  RT operator()() const {
		  return emit();
  	}
    RT emit() const {
      RT ret;
      FuncIterator it = funcs.begin();
      for(;it!=funcs.end();++it) {
		Functor *func = *it;
		ret = func->emit();
      }
      return ret;
    }
  };


  template<typename RT, class AT1, class AT2, class T>
  Functor2<RT,AT1,AT2> *slot(T *object, RT (T::*func)(AT1,AT2)) {
    return new Functor2C<T,RT,AT1,AT2>(object,func);
  }

  template<typename RT, class AT1, class T>
  Functor1<RT,AT1> *slot(T *object, RT (T::*func)(AT1)) {
    return new Functor1C<T,RT,AT1>(object,func);
  }

  template<typename RT, class T>
  Functor0<RT> *slot(T *object, RT (T::*func)()) {
    return new Functor0C<T,RT>(object,func);
  }
}
#endif