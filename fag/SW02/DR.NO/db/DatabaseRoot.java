/*
 * Created on 21-02-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package db;
import java.sql.*;


/**
 * @author tobibobi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class DatabaseRoot {
	public DatabaseRoot() {
		Connection con = null;
		try {
			try {
				Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
			}catch(ClassNotFoundException ex) {
				System.out.println(ex.toString());
			}
			con = DriverManager.getConnection("jdbc:odbc:testdb", "","");
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM Personer");
			while(rs.next()) {
				IPerson pers = new Person(rs,con);
				System.out.println(pers);
			}
		} catch (SQLException ex) {
			System.out.println(ex.toString());
		} finally {
			try {
				if(con != null)
					con.close();
			} catch (SQLException ex) {
				System.out.println(ex.toString());
			}
		}
	}	
}
