
package db;
/*
 * Created on 21-02-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import java.sql.*;
import main.*;

/**
 * @author tobibobi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Person implements IPerson {
	private String _navn;
	private String _adresse;
	private boolean _tainted;
	private Connection _con;
	
	public Person(ResultSet rs,Connection con) {
		try {
		_navn = rs.getString("Navn");
		_adresse = rs.getString("Adresse");
		_tainted = false;
		} catch(SQLException ex) {
			ErrorHandler.instance().dumpError(ex.toString());
		}
	}
	
	public Person(String navn,String adresse) {
		_navn = navn;
		_adresse = adresse;
		_tainted = false;
		_con = null;
	}
	
	private boolean isConnected() {
		if(_con == null)
			return false;
		try { return (! _con.isClosed()); }	
		catch (SQLException ex){ return false; }	
	}
	/**
	 * @see IPerson#getNavn()
	 */
	public String getNavn() {
		return _navn;
	}

	/**
	 * @see IPerson#setNavn(java.lang.String)
	 */
	public void setNavn(String navn) {
		_navn = navn;
		_tainted = true;
	}

	/**
	 * @see IPerson#getAdresse()
	 */
	public String getAdresse() {
		return _adresse;
	}

	/* (non-Javadoc)
	 * @see IPerson#setAdresse(java.lang.String)
	 */
	public void setAdresse(String adresse) {
		_adresse = adresse;
		_tainted = true;
	}
	
	public boolean getTainted() {
		return _tainted;
	}
	public void save() {
		if(isConnected()) {
			
		}
	}
	public String toString() {
		String str = ">>Person Obj<<\n" +
			"Navn: " + _navn + "\n" +
			"Adresse: " + _adresse;
		return str;
	}

}
