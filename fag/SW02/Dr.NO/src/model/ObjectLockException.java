/*
 * Created on 18-03-2005
 * 
 * $Log: ObjectLockException.java,v $
 * Revision 1.1  2005-03-21 19:43:42  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package model;

/**
 * @author tobibobi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ObjectLockException extends Exception {
	public ObjectLockException() {
		super("Object is locked and can only be read. Unlock it to write to it.");
	}
}
