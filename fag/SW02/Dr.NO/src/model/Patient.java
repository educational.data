
/*
 * Patient.java
 *
 * Created on 6. marts 2005, 15:35
 * 
 * $Log: Patient.java,v $
 * Revision 1.1  2005-03-21 19:43:42  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */

package model;
/**
 *
 * @author  Leo
 */

public class Patient implements IPatient{
    /**
     * @author tobibobi
     */
    public class Log {

    }
    private String _fornavn;
    private String _efternavn;
    private String _cpr;
    private String _adresse;
    private String _tlf;
    private String _sex;
    private int _alder;
    private int _id;
    
    public Patient(int id, String fornavn, String efternavn, String cpr, String adresse, String tlf, String k�n, int alder) {
    	_id = id;
        _fornavn = fornavn;
        _efternavn = efternavn;
        _cpr = cpr;
        _adresse = adresse;
        _tlf = tlf;
        _sex = k�n;
        _alder = alder;
    }
    public int get_id(){
    	return _id;
    }
    public String get_adresse() {
    	return _adresse;
    }
    public int get_alder() {
    	return _alder;
    }
	public String get_cpr() {
		return _cpr;
	}
	public String get_efternavn() {
		return _efternavn;
	}
	public String get_fornavn() {
		return _fornavn;
	}
	public String get_sex() {
		return _sex;
	}
	public String get_tlf() {
		return _tlf;
	}
	
	public void set_adresse(String adresse) {
		_adresse = adresse;
	}
	
	public void set_alder(int alder) {
		_alder = alder;
	}

	public void set_cpr(String cpr) {
		_cpr = cpr;
	}
	
	public void set_efternavn(String efternavn) {
		_efternavn = efternavn;
	}

	public void set_fornavn(String fornavn) {
		_fornavn = fornavn;
	}
	
	public void set_sex(String sex) {
		_sex = sex;
	}
	
	public void set_tlf(String tlf) {
		_tlf = tlf;
	}
}
