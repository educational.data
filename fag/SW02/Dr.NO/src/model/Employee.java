/*
 * Created on 2005-03-16
 *
 * $Log: Employee.java,v $
 * Revision 1.1  2005-03-21 19:43:42  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package model;

/**
 * @author Bo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Employee implements IEmployee{
	private int id;
	private String login;
	private String password;
	private String efternavn;
	private String fornavn;
	private int tidsopdeling;
	private int type;
	
	public Employee(int id, String fornavn, String efternavn, String login, String password, int tidsopdeling, int type){
		this.id = id;
		this.login = login;
		this.password = password;
		this.efternavn = efternavn;
		this.fornavn = fornavn;
		this.tidsopdeling = tidsopdeling;
		this.type = type;
	}
	public int get_id(){
		return id;
	}
	public String get_login(){
		return login;
	}
	public String get_password(){
		return password;
	}
	public String get_efternavn(){
		return efternavn;
	}
	public String get_fornavn(){
		return fornavn;
	}
	public int get_tidsopdeling(){
		return tidsopdeling;
	}
	public int get_type(){
		return type;
	}
}