/*
 * Created on 2005-03-17
 *
 * $Log: IEmployee.java,v $
 * Revision 1.1  2005-03-21 19:43:42  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package model;

/**
 * @author Leo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IEmployee {
	abstract public int get_id();
	abstract public String get_login();
	abstract public String get_password();
	abstract public String get_efternavn();
	abstract public String get_fornavn();
	abstract public int get_tidsopdeling();
	abstract public int get_type();
}
