/*
 * Created on 16-03-2005
 *
 * $Log: IPatient.java,v $
 * Revision 1.1  2005-03-21 19:43:42  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package model;

/**
 * @author tobibobi
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public interface IPatient {
	abstract public String get_adresse();
	abstract public int get_alder();
	abstract public String get_cpr();
	abstract public String get_efternavn();
	abstract public String get_fornavn();
	abstract public String get_sex();
	abstract public String get_tlf();
	
	abstract public void set_alder(int alder) throws Exception;
	abstract public void set_cpr(String cpr) throws Exception;
	abstract public void set_efternavn(String efternavn) throws Exception;
	abstract public void set_fornavn(String fornavn) throws Exception;
	abstract public void set_sex(String sex) throws Exception;
	abstract public void set_tlf(String tlf) throws Exception;
}
