/*
 * Created on 18-03-2005
 * This file is created 18-03-2005 and descripes BrokerController
 * 
 * $Log: BrokerController.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package database;

import database.mysql.DBConnection;
import applikation.Logger;

/**
 * @author tobibobi
 */
public class BrokerController {
    private static BrokerController _instance;
    EmployeeBroker _EB;
    PatientBroker _PB;
    DBConnection _con;
    
    private BrokerController() {
        try {
            _con = new DBConnection("root","");
        } catch(Exception ex) {
            ex.printStackTrace(Logger.instance());
            System.exit(-1);
        }
        _EB = new EmployeeBroker(_con);
        _PB = new PatientBroker(_con);
    }
    public static BrokerController instance() {
        if(_instance == null)
            _instance = new BrokerController();
        
        return _instance;
    }
    
    public EmployeeBroker getEmployeeBroker() {
        return _EB;
    }
    
    public PatientBroker getPatientBroker() {
        return _PB;
    }

}
