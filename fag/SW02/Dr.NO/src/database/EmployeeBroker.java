/*
 * Created on 2005-03-16
 *
 * $Log: EmployeeBroker.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package database;

import java.sql.ResultSet;
import java.util.StringTokenizer;
import java.util.Vector;

import database.mysql.DBConnection;

import model.Employee;


/**
 * Description of an database to the Employee object in db.
 * All serialiazation of model objects, is going through the 
 * Broker objects.
 * 
 * @author Bo
 */
public class EmployeeBroker {
	
	DBConnection con;
	
	public EmployeeBroker(DBConnection DBC){
		con = DBC;
	}
	
	public void create_DB() {
		try {
            con.executeUpdate("create table EMPLOYEE (id INTEGER PRIMARY KEY, FORNAVN varchar(10), EFTERNAVN varchar(10), LOGIN varchar(12), PASSWORD varchar(8), TID integer(10), TYPE integer(1))");  
            System.out.println("Table created");
        }
        catch(Exception e){
            System.out.println("Table not created: "+e);
        }
	}

	public void RemoveAll()throws Exception{
        con.executeUpdate("drop table EMPLOYEE");
    }
	
	public void add_employee(Employee e)throws Exception {
		con.executeUpdate("insert into EMPLOYEE values("+e.get_id()+",'"+
        		e.get_fornavn()+"','"+
				e.get_efternavn()+"','"+
				e.get_login()+"','"+
				e.get_password()+"','"+
				e.get_tidsopdeling()+"','"+
				e.get_type()+"')");
        System.out.println("Ny ansat tilf�jet.");
	}


	public Employee get_Employee(int id){
		try {
	    	ResultSet rs = con.executeQuery("select * from EMPLOYEE where id=" + id + ";");
	    	if(rs.next()){
	    		int tid = rs.getInt("id");
	            String fnavn = rs.getString("FORNAVN");
	            String enavn = rs.getString("EFTERNAVN");
	            String login = rs.getString("LOGIN");
	            String password = rs.getString("PASSWORD");
	            int tidsopdeling = rs.getInt("TID");
	            int type = rs.getInt("TYPE");
	            
	            Employee e = new Employee(id,fnavn, enavn, login, password, tidsopdeling, type);
	            return e;
	    	}
    	} catch(Exception e){
            System.out.println("Employee not fetched: "+e);
        }
    	return null;
	}
	
	public Vector get_all() throws Exception{
		Vector all = new Vector();
        ResultSet rs = con.executeQuery("select id from EMPLOYEE");
        
        while(rs.next()){   
        	int id = rs.getInt("id");
                        
            Employee e = get_Employee(id);
            all.addElement(e);
        }
        return all;
	}
	
	public void PrintAll(Vector all){
        if(all.size()>0){
            //System.out.println("Udskriver Database....");
            System.out.println();
            for(int i=0; i<all.size(); i++){
                Employee e = (Employee)all.elementAt(i);
            
                System.out.println("Navn:\t"+e.get_fornavn()+" "+e.get_efternavn());
                System.out.println();
            }
        }
        else{}
    }
	
	public void PrintEmployee(Employee e){
        System.out.println();    
        System.out.println("Navn:\t"+e.get_fornavn()+" "+e.get_efternavn());
        System.out.println();
    }
	
	public Vector SearchByName(String navn)throws Exception{
        Vector v = new Vector();
        StringTokenizer token = new StringTokenizer(navn);
        String fornavn = token.nextToken();
        String efternavn = token.nextToken();  
        ResultSet rs = con.executeQuery("select id from EMPLOYEE where FORNAVN = \""+fornavn+"\" and EFTERNAVN = \""+efternavn+"\"");
        
        while(rs.next()){
        	int tid = rs.getInt("id");
                      
            Employee e = get_Employee(tid);
            v.addElement(e);   
        }
        if(v.size()==0)
            System.out.println("Ansat med navn: "+navn+", eksisterer ikke i Databasen.");
        return v; 
    }

	public void delete_employee(Employee e)throws Exception {
		int id = e.get_id();
		String navn = e.get_fornavn()+" "+e.get_efternavn();
        con.executeUpdate("delete from EMPLOYEE where ID = \""+id+"\"");
        System.out.println("Ansat med navn: "+navn+" er slettet fra databasen.");
	}

	
}