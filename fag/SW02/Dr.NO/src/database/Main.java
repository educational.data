/*
 * Main.java
 *
 * Created on 5. marts 2005, 11:27
 * @author  Leo
 * 
 * $Log: Main.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */

package database;

import java.util.Vector;

import database.mysql.DBConnection;

import model.Patient;



public class Main {
    public void println(String str) {
        System.out.println(str);
    }
    public void perform() throws Exception {
        DBConnection DBC = new DBConnection("root","");
        
        //Create en patient tabel:
        PatientBroker PB = new PatientBroker(DBC);
        PB.RemoveAll();
        PB.CreateDB();
        //Tilf�j testdataer i tabellen:
        
        PB.addPatient(new Patient(0,"Tobias","Nielsen","020470-3448","fuck 1 5240 Odense �","56545456","Nej",50));
        PB.addPatient(new Patient(1,"Leo","Zhou","020382-3743","Kirseb�rgrene 81 5220 Odense S�","26366832","Mand",23));
        PB.addPatient(new Patient(2,"Leo","Zhou","020372-3043","Kirseb�rgrene 89 5220 Odense S�","26366932","Mand",40));
        PB.addPatient(new Patient(3,"Bo","Hansen","020480-3549","fuckfuck 1 5000 Odense c","59345356","Nej",12));
        PB.addPatient(new Patient(4,"Kim","Slot","010389-3343","Luderall� 1 5200 Odense S","24563456","Nej",9));
        //Test: Udskriver Databasen:
        System.out.println("Print Database.");
        Vector v = PB.GetAll();
        PB.PrintAll(v);
        
        //Test: S�g ud fra navne: ikke eksisterer:
        System.out.println("Print search result 1:");
        Vector s = PB.SearchByName("Leo Slot");
        PB.PrintAll(s);
        //Test: s�g ud fra navne: 2 med samme navne:
        System.out.println("Print search result 2:");
        Vector k = PB.SearchByName("Leo Zhou");
        PB.PrintAll(k);
        //Test: S�g ud fra CPR-NR:
        System.out.println("Print search result 3:");
        Patient p = PB.SearchByCPR("010389-3343");
        PB.PrintPatient(p);
        //Test: Slet en patient fra DB:
        PB.DeletePatient(p);
        System.out.println("DB efter opdatering:");
        Vector a = PB.GetAll();
        PB.PrintAll(a);

    }
 
    public static void main(String[] args) throws Exception{
        try{
            new Main().perform();
        }
        catch(Exception e){
            System.out.println("Exception kastet: "+e);
            e.printStackTrace();
        }
    }
    
}
