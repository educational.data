/*
 * Created on 16-03-2005
 *
 * $Log: PatientProxy.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package database;

import model.IPatient;
import model.ObjectLockException;
import model.Patient;

/**
 * @author tobibobi
 *
 */
public class PatientProxy implements IPatient {
	private Patient p;
	private int id;
	private PatientBroker broker;
	private boolean bCanEdit;
	private boolean bIsNew;
	
	public PatientProxy(int id, PatientBroker br) {
		this.id = id;
		p=null;
		broker=br;
	}
	/* (non-Javadoc)
	 * @see model.IPatient#get_adresse()
	 */
	private void getPatient() {
		if(p == null)
			p = broker.getPatient(id);
	}
	
	private void verifyWriteAble() throws ObjectLockException {
		getPatient();
		if(bCanEdit) return;
		else throw new ObjectLockException();
	}
	
	public void set_adresse(String adresse) throws Exception {
		 verifyWriteAble(); // will throw if not writeable
		 p.set_adresse(adresse);
	}
	
	public String get_adresse() {
		getPatient();
		return p.get_adresse();
	}

	public void set_alder(int alder) throws Exception {
		verifyWriteAble();
		p.set_alder(alder); 
	}
	/* (non-Javadoc)
	 * @see model.IPatient#get_alder()
	 */
	public int get_alder() {
		getPatient();
		return p.get_alder();
	}

	
	public void set_cpr(String cpr) throws Exception {
		verifyWriteAble();
		p.set_cpr(cpr);
	}
	/* (non-Javadoc)
	 * @see model.IPatient#get_cpr()
	 */
	public String get_cpr() {
		getPatient();
		return p.get_cpr();
	}

	public void set_efternavn(String efternavn) throws Exception {
		verifyWriteAble();
		p.set_efternavn(efternavn);
	}
	/* (non-Javadoc)
	 * @see model.IPatient#get_efternavn()
	 */
	public String get_efternavn() {
		getPatient();
		return p.get_efternavn();
	}

	public void set_fornavn(String fornavn) throws Exception {
		verifyWriteAble();
		p.set_fornavn(fornavn);
	}
	/* (non-Javadoc)
	 * @see model.IPatient#get_fornavn()
	 */
	public String get_fornavn() {
		getPatient();
		return p.get_fornavn();
	}
	
	public void set_sex(String sex) throws Exception {
		verifyWriteAble();
		p.set_sex(sex);
	}
	/* (non-Javadoc)
	 * @see model.IPatient#get_sex()
	 */
	public String get_sex() {
		getPatient();
		return p.get_sex();
	}

	public void set_tlf(String tlf) throws Exception {
		verifyWriteAble();
		p.set_tlf(tlf);
	}
	/* (non-Javadoc)
	 * @see model.IPatient#get_tlf()
	 */
	public String get_tlf() {
		getPatient();
		return p.get_tlf();
	}

}
