/*
 * PatientBroker.java
 *
 * Created on 15. marts 2005, 14:38
 * 
 * $Log: PatientBroker.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */



package database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.StringTokenizer;
import java.util.Vector;

import database.mysql.DBConnection;

import applikation.Logger;

import model.IPatient;
import model.Patient;

/**
 * @author  Leo
 */

public class PatientBroker {
    
    private String tabel="Patienter";

	/**
	 * 
	 * @uml.property name="con"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	DBConnection con;
	
	private HashMap loadedPatients;
	

    /** Creates a new instance of PatientBroker */
    public PatientBroker(DBConnection DBC) {
        con = DBC;
        // examine if the database is created already.
        try {
            ResultSet rs = con.executeQuery("show tables;");
            boolean isFound = false;
            while(rs.next()) {
                if(rs.getString(1).equals(tabel)) isFound = true;
            }
            rs.close();
            if(! isFound)
                CreateDB();
        } catch(Exception ex) {
            ex.printStackTrace();
        }
        loadedPatients = new HashMap();
    }
     public void CreateDB() throws SQLException{
        try {
            con.executeUpdate("create table " + tabel + " (id INTEGER PRIMARY KEY, fornavn varchar(10), efternavn varchar(10), cpr varchar(11), adresse varchar(50), tlf varchar(8), sex varchar(8), alder integer(3))");  
            //System.out.println("Table \"Patienter\" created");
        }
        catch(Exception e){
            System.out.println("Table \"Patienter\" not created: "+e.getMessage());
        }
    }
    
    public void RemoveAll(){
    	try {
    		con.executeUpdate("drop table " + tabel);
    	} catch(Exception ex) {
    		System.err.println("PatientBroker::RemoveAll():");
    		System.err.println("  Warning: " + ex.getMessage());
    	}
    }
    
    public void addPatient(Patient p) throws Exception{
    	con.executeUpdate("insert into "+ tabel + " values("+p.get_id()+",'"+
        		p.get_fornavn()+"','"+
				p.get_efternavn()+"','"+
				p.get_cpr()+"','"+
				p.get_adresse()+"','"+
				p.get_tlf()+"','"+
				p.get_sex()+"','"+
				p.get_alder()+"')");
    	loadedPatients.put(new Integer(p.get_id()),p);
        Logger.instance().println("Ny Patient tilf�jet.");
    }
    public Patient getPatient(int id) {
        // first examine to see if we already 
        // have a loaded version of the object
        if(loadedPatients.containsKey(new Integer(id))) {
            System.out.println("Retrive object from database");
            return (Patient)loadedPatients.get(new Integer(id)); 
        } else {
	    	try {
		    	ResultSet rs = con.executeQuery("select * from " +tabel + " where id=" + id + ";");
		    	if(rs.next()){
		    		int tid = rs.getInt("id");
		            String fornavn = rs.getString("fornavn");
		            String efternavn = rs.getString("efternavn");
		            String cpr = rs.getString("cpr");
		            String adresse = rs.getString("adresse");
		            String tlf = rs.getString("tlf");
		            String sex = rs.getString("sex");
		            int alder = rs.getInt("alder");
		            Patient p = new Patient(id,fornavn, efternavn, cpr, adresse, tlf, sex, alder);
		            // store the patient in local buffer
		            loadedPatients.put(new Integer(tid),p);
		            return p;
		    	}
	    	} catch(Exception e){
	    	    Logger.instance().println("Patient not fetched: "+e);
	        }
        }
	    return null;
    }
    public Vector GetAll() throws Exception{
        Vector all = new Vector();
        ResultSet rs = con.executeQuery("select id from " + tabel);
        
        while(rs.next()){   
        	int id = rs.getInt("id");
                        
            Patient p = getPatient(id);
            all.addElement(p);
        }
        return all;
    }
    public void PrintAll(Vector all){
        if(all.size()>0){
            //System.out.println("Udskriver Database....");
            System.out.println();
            for(int i=0; i<all.size(); i++){
                Patient p = (Patient)all.elementAt(i);
            
                System.out.println("Navn:\t"+p.get_fornavn()+" "+p.get_efternavn());
                System.out.println("CPR-NR:\t"+p.get_cpr());
                System.out.println("Adr:\t"+p.get_adresse());
                System.out.println("TLF-NR:\t"+p.get_tlf());
                System.out.println("K�n:\t"+p.get_sex());
                System.out.println("Alder:\t"+p.get_alder());
                System.out.println();
            }
        }
        else{}
    }
    
    public void PrintPatient(Patient p){
        Logger.instance().println();    
        Logger.instance().println("Navn:\t"+p.get_fornavn()+" "+p.get_efternavn());
        Logger.instance().println("CPR-NR:\t"+p.get_cpr());
        Logger.instance().println("Adr:\t"+p.get_adresse());
        Logger.instance().println("TLF-NR:\t"+p.get_tlf());
        Logger.instance().println("K�n:\t"+p.get_sex());
        Logger.instance().println("Alder:\t"+p.get_alder());
        Logger.instance().println();
    }
    
    public Vector SearchByName(String navn)throws Exception{
        Vector v = new Vector();
        StringTokenizer token = new StringTokenizer(navn);
        String fornavn = token.nextToken();
        String efternavn = token.nextToken();  
        ResultSet rs = con.executeQuery("select id from " + tabel +"  where FORNAVN = \""+fornavn+"\" and EFTERNAVN = \""+efternavn+"\"");
        
        while(rs.next()){
        	int tid = rs.getInt("id");
                      
            Patient p = getPatient(tid);
            v.addElement(p);   
        }
        if(v.size()==0)
            System.out.println("Patient med navn: "+navn+", eksisterer ikke i Databasen.");
        return v; 
    }
    
    public Patient SearchByCPR(String c)throws Exception{
        ResultSet rs = con.executeQuery("select id from "+tabel+" where CPR like \""+c+"\"");
        if(rs.next()){
        	int tid = rs.getInt("id");
        	
            Patient p = getPatient(tid);
            return p;
        }
        else{
            System.out.println("Patient med CPR-NR: "+c+" findes ikke i databasen.");
            return null;
        }
    }
    
    public void DeletePatient(Patient p)throws Exception{
        String cprnr = p.get_cpr();
        con.executeUpdate("delete from "+tabel+" where CPR = \""+cprnr+"\"");
        System.out.println("Patient med CPR-NR: "+cprnr+" er slettet fra databasen.");
    }
    /**
     * @param patient
     */
    public void lock(IPatient patient) {
        // TODO Auto-generated method stub        
    }
}
