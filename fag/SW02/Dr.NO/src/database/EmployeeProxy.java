/*
 * Created on 2005-03-17
 *
 * $Log: EmployeeProxy.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package database;
import model.Employee;
import model.IEmployee;
/**
 * @author Leo
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class EmployeeProxy implements IEmployee{
	private Employee e;
	private int id;
	private EmployeeBroker broker;
	
	private EmployeeProxy(int id, EmployeeBroker br) {
		this.id = id;
		e=null;
		broker=br;
	}
	
	public int get_id() {
		if(e == null)
			e = broker.get_Employee(id);
		return e.get_id();
	}
	
	public String get_login() {
		if(e == null)
			e = broker.get_Employee(id);
		return e.get_login();
	}

	public String get_password() {
		if(e == null)
			e = broker.get_Employee(id);
		return e.get_password();
	}

	public int get_tidsopdeling() {
		if(e == null)
			e = broker.get_Employee(id);
		return e.get_tidsopdeling();
	}

	public String get_efternavn() {
		if(e == null)
			e = broker.get_Employee(id);
		return e.get_efternavn();
	}

	public String get_fornavn() {
		if(e == null)
			e = broker.get_Employee(id);
		return e.get_fornavn();
	}

	public int get_type() {
		if(e == null)
			e = broker.get_Employee(id);
		return e.get_type();
	}
}
