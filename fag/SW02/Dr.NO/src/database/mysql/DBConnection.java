/*
 * DBConnection.java
 *
 * Created on 5. marts 2005, 14:28
 * 
 * $Log: DBConnection.java,v $
 * Revision 1.1  2005-03-21 19:43:43  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */

/**
 *
 * @author  Leo
 */
package database.mysql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;

public class DBConnection {    
    private Connection con;
    private Statement stmt;
    private String driver = "com.mysql.jdbc.Driver";
    private String connection = "jdbc:mysql://localhost:3306/drno";

	public DBConnection(String user, String password) throws Exception{ 
		try {
	        Class.forName(driver);
	        System.out.println("Database Driver loaded...");
	        
	        con = DriverManager.getConnection(connection,user,password);
	        System.out.println("Database Connection established...");
		} catch (SQLException ex) {
			System.err.println("SQLException: "+ ex.getMessage());
			System.err.println("SQL State: " + ex.getSQLState());
			System.err.println("Vendor code: " + ex.getErrorCode());
			System.exit(-1);
		}
    }
	
	public void close() {
		try {
			con.close();
		} catch(Exception ex) {
			System.out.println("Failed close of connection");
			System.out.println(ex);
		}
	}

	public Connection getConnection() {
		return con;
	}
	/**
	 * To make any non returning updates.
	 * @param sqlstatement
	 * @return index     an index of any eventually generated insert.
	 * @throws Exception
	 */

    public int executeUpdate(String sqlstatement) throws Exception {
        Statement stmt = con.createStatement();
        stmt.executeUpdate(sqlstatement);
        ResultSet rs = stmt.getGeneratedKeys();
        if(rs.next()) {
            return rs.getInt(1);
        } else return -1;
    }
    public ResultSet executeQuery(String s) throws Exception {
        Statement stmt = con.createStatement();
        return stmt.executeQuery(s);
    }
}
