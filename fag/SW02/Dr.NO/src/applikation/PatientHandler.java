/*
 * Created on 18-03-2005
 * This file is created 18-03-2005 and descripes PatientHandler
 * 
 * $Log: PatientHandler.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package applikation;
import java.util.Vector;

import model.*;
import database.*;

/**
 * Use case handler for Patient handling.
 * This class is consideres an facade to the model
 * layer. it works side by side with the 
 * (@link EmployeeHandler) EmployeeHandler.
 * 
 * @author tobibobi
 */
public class PatientHandler {
    PatientBroker _pb;
    public PatientHandler() {
        _pb = BrokerController.instance().getPatientBroker();
    }
    /**
     * 
     * @param name	Name of person to search for 
     * @return	A vector of IPatients objects that is readable only,
     * or in the situation that no objects is found, it 
     * returns an empty vector.
     */
    public Vector SearchPatientsByName(String name) {
        try {
            return _pb.SearchByName(name);
        } catch (Exception ex) {
            return new Vector();
        }
    }
    
    public void editPatient(IPatient patient) throws Exception{
        _pb.lock(patient);
    }
}
