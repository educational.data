/*
 * Created on 18-03-2005
 * This file is created 18-03-2005 and descripes Logger
 * 
 * $Log: Logger.java,v $
 * Revision 1.1  2005-03-21 19:43:41  tobibobi
 * We have added data to CVS controll
 * and dumped subversion.
 *
 * Subversion seemed to unstable
 *
 */
package applikation;

import java.io.OutputStream;
import java.io.PrintStream;

/**
 * Main class to include logger funktionality.
 * To utilize this funktionality, one must override 
 * the (@link Logger.write()) function.
 * 
 * @author tobibobi
 * @see java.io.OutputStream
 */
public class Logger extends OutputStream {
    /**
     * Constructor for Logger class.
     * Creates a logger object 
     * @see Logger 
     */
    private Logger() {
        super();
        _stream = new PrintStream(this);
        
    }
    private static Logger _instance;
    private PrintStream _stream;
    
    /**
     * Singleton implementation of this object.
     * It will be used when there is needed logger knowledge.
     * 
     * @return PrintStream      Object used to write normal data output.
     * @see java.io.PrintStream
     */
    public static PrintStream instance() {
        if(_instance == null)
            _instance = new Logger();
        return _instance._stream;
    }
    
    /**
     * To use OutputStream in conjunction with PrintStream,
     * One must override write.
     * In this implementation its used to write to an 
     * external file.
     * 
     * @see java.io.OutputStream#write(int)
     */
    public void write(int t){
        System.out.print((char)t);
    }

}
