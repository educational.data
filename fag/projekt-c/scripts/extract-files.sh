#!/bin/bash

mkdir -p dosBuild
cd src
find -name "*.cpp" -or -name "*.h" |sed -s "s/\.\(.*\/\)\(.*\)/mkdir -p ..\/dosBuild\1;cp -vu .\1\2 ..\/dosBuild\1\2/" > perform
chmod u+x perform
./perform > /dev/null
cd ../dosBuild
find -name "*test*"|xargs rm -rf
find -regex ".*\/[^\/][^\/][^\/][^\/][^\/][^\/][^\/][^\/][^\/]+\.h" -exec rm {} \;
find -regex ".*\/[^\/][^\/][^\/][^\/][^\/][^\/][^\/][^\/][^\/]+\.cpp" -exec rm {} \;
find -name "*.cpp" |sed -s "s/\(.*\)\.cpp/bcc -o \1.o\1\.cpp/g"
find -name "*.cpp" |sed
cd ..
rm -rf dosBuild


