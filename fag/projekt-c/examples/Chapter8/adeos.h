/**********************************************************************
 *
 * Filename:    adeos.h
 * 
 * Description: The header file that users of the OS include.
 *
 * Notes:       
 *
 * 
 * Copyright (c) 1998 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 **********************************************************************/

#ifndef _ADEOS_H
#define _ADEOS_H


#include "bsp.h"
#include "task.h"
#include "sched.h"
#include "mutex.h"

#ifndef NULL
#define NULL  (void *) 0L
#endif


#endif /* _ADEOS_H */
