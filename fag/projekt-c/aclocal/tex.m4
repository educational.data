AC_DEFUN([AC_PROG_DVI2PDF],[
AC_PROVIDE([$0])
AC_MSG_CHECKING([for a method to generate pdf files])
DVI2PDF=;found=0
path=`echo $PATH|sed "s/\:/ /g"`
cmds="dvipdfm dvips"
for filename in $cmds
do
  if test $found = 0; then
      for pn in $path; do
	  if test -x $pn/$filename; then
	      DVI2PDF=$pn/$filename
	      found=$filename
	  fi;
      done
  fi
done
	
if test $found = 0; then
    AC_MSG_RESULT([No method found])
    DVI2PDF="echo "
else
    case $found in 
	dvips)
	    DVI2PDF="cd \$(<D);$DVI2PDF -f* -q \$(<F) | gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=\$(@F) -";;

	dvi2pdfm)
	    DVI2PDF="$DVI2PDF -o \$$@@ \$<";;
    esac 
    AC_MSG_RESULT($found)
fi
AC_SUBST(DVI2PDF)
])
