import java.util.*;

/**
 * class MicroProcessor
 * an instance of this represents an if to one mp
 * all functions have described below.
 * giving wrong input (according to the language) of the 
 * function, are just ignored - so extensive error check
 * should be provided. 
 */ 
public class MicroProcessor {
    public MicroProcessor() {
	System.out.println("Oprettet MP object");
    }
    
    native public static void init();
    native public static String getGreenHouses();
    native public static int getTemp(int id);    
    native public static int getTempMax(int id);    
    native public static int getTempMin(int id);    
    native public static int getWinState(int id);
    native public static int getAutomata(int id);
    native public static void setTemp(int id, int tempmax, int tempmin);
    native public static void setWinState(int id, int state);
    native public static void enableAutomata(int id, int state);
    static {
	System.out.println("Loader Kernel");
	System.loadLibrary("Kernel");
	System.out.println("done");
    }
}
