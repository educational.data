import java.util.*;
import java.rmi.*;
public interface HeadServerI extends Remote{
   public int GHCount() throws java.rmi.RemoteException;
   public Vector getIDList() throws java.rmi.RemoteException;
   public void receiveMessage(String x) throws java.rmi.RemoteException;
}
