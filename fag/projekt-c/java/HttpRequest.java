import java.io.*;
import java.net.*;
import java.util.*;

public class HttpRequest implements Runnable {
    private Socket socket;
    //Constructor:
    public HttpRequest(Socket socket) throws Exception{
        this.socket = socket;
    }
    //Run metoden:
    public void run() {   
        try{
            //Laver to streams, en input og en output:
            BufferedReader iStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            DataOutputStream oStream = new DataOutputStream(socket.getOutputStream());
            //path angiver hvor filen som klienten requester ligger. Antag at filen ligger p� C:
            String fileName, path="./"; 
            //Henter request fra klienten og angiv starttidspunktet:
            String request = iStream.readLine();
            System.out.println("start "+new Date()+" "+request);
            //Fjerner GET og forme filnavne:
            StringTokenizer tokenizedLine = new StringTokenizer(request);
            if(tokenizedLine.nextToken().equals("GET")){
                fileName = tokenizedLine.nextToken();
                if(fileName.startsWith("/")==true){
                    fileName = path + fileName.substring(1);
                }
                //Finder den requested fil:
                try{
                File file = new File(fileName);
                int numOfBytes = (int) file.length();
                FileInputStream inFile = new FileInputStream(fileName);
                byte[] fileInBytes = new byte[numOfBytes];
                inFile.read(fileInBytes);
                
                //HTTP-svarmeddelelsen:
                oStream.writeBytes("HTTP/1.0 200 Document Follows\r\n");
                oStream.writeBytes("Date: "+new Date()+"\r\n");
                oStream.writeBytes((String) contentType(fileName));
                oStream.writeBytes("Content-Length: "+numOfBytes+"\r\n");
                oStream.writeBytes("\r\n");
                //Overf�rer filen og lukker socket:
                oStream.write(fileInBytes,0, numOfBytes);
                oStream.writeBytes("\r\n");
                socket.close();
                //Angiv sluttidspunktet:
                System.out.println("closed "+new Date()+" "+request);
                }
                //Hvis den requested fil ikke findes i servermaskinen:
                catch(FileNotFoundException e){
                    oStream.writeBytes("Requested File Not Found\r\n");
                    socket.close();
                    System.out.println("closed "+new Date()+" "+request);
                }
            }
            else System.out.println("Bad Request Message!");
            Thread.sleep(500);
        }
        catch(Exception e){
             e.printStackTrace();
            //System.out.println("Error!");
        }
    }
    //Underklasse der unders�ger filtype og returnerer en passende meddelse:
    private static String contentType(String fileName){
        if(fileName.endsWith(".htm") || fileName.endsWith(".html")){
            return "Content-Type: text/html\r\n";
        }
        if(fileName.endsWith(".jpg")){
            return "Content-Type: image/jpeg\r\n";
        }
        if(fileName.endsWith(".gif")){
           return "Content-Type: image/gif\r\n";
        }
        if(fileName.endsWith(".mp3")){
            return "Content-Type: sound/mp3\r\n";
        }
        if(fileName.endsWith(".txt")){
            return "Content-Type: text/txt\r\n";
        }
         if(fileName.endsWith(".log")){
            return "Content-Type: text/log\r\n";
        }
         else{
            return "Content-Type: Unknown\r\n";
        }
    }
}
