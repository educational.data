import java.io.*;
import java.net.*;

public class MultWebServer {
    
    public static void main(String[] args) {
        
        ServerSocket serversocket;
        Socket socket;
        final int port =80;
        final int queue_length = 6;
        try{
            serversocket = new ServerSocket(port,queue_length);
            //Uendelig l�kke der venter p� requester fra port 6789:
            while(true){
            socket = serversocket.accept();
            HttpRequest req = new HttpRequest(socket);
            //Start tr�den:
            new Thread(req).start();
            }
        }
        catch(Exception e){
            e.printStackTrace();
            //System.out.println("Error!");
        }       
    }
}
