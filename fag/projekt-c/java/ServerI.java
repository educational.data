/*
 * Interface.java
 * Her defineres et interface til de metoder p� serverobjektet, som skal v�re 
 * tilg�ngelige for klienten.
 * Created on 20. september 2004, 18:30
 * @author  Silex
 */
import java.util.*;
import java.rmi.*;
public interface ServerI extends Remote{
    public int getTemp(int temp_type) throws java.rmi.RemoteException;
    public int getTempMax() throws java.rmi.RemoteException;
    public int getTempMin() throws java.rmi.RemoteException;
    public int getWinState() throws java.rmi.RemoteException;
    public int getAutomata() throws java.rmi.RemoteException;
    public void setTemp(int tempmax, int tempmin) throws java.rmi.RemoteException;
    public void setWinState(int state) throws java.rmi.RemoteException;
    public void setAutomata(int state) throws java.rmi.RemoteException;
    public void receiveMessage(String x) throws java.rmi.RemoteException;
}
