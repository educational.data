import java.util.*;
import java.rmi.*;
import java.rmi.server.*;
import java.io.*;
import java.util.*;

public class HeadServer extends UnicastRemoteObject implements HeadServerI {
    public Vector idVec;
    /** Creates a new instance of Class */
    public HeadServer(String GHlist) throws java.rmi.RemoteException{
        idVec = new Vector();
        StringTokenizer tokenizer = new StringTokenizer(GHlist);
        while(tokenizer.hasMoreTokens()){
            int id = Integer.parseInt(tokenizer.nextToken());
            try{
                ServerI server = new ServerImpl(id);
                String rmiID = "GreenHouse"+id;
                idVec.addElement(rmiID);
                String ServerObjectName = "//localhost/"+rmiID;
                Naming.rebind(ServerObjectName, server);
                System.out.println(rmiID+" waiting for Client requests");
                
            }
            catch(java.io.IOException e){
                System.out.println("I/O error.");
            }
        }
    }
    
    public void receiveMessage(String x) throws RemoteException
    {
        System.out.println(x);
    }
    
    public int GHCount(){
        return idVec.size();
    }
    
    public Vector getIDList(){
        return idVec;
    }
}