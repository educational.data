/*
 * MPImpl.java
 * P� serversiden skal vi implementere MicroProcessor interfacet og programmere
 * de funktionaliter der skjuler bag det. Et serverobjekt arves fra UnicastRemoteObject.
 * Created on 20. september 2004, 18:39
 * @author  Silex
 */
import java.util.*;
import java.net.*;
import java.rmi.*;
import java.io.*;
import java.rmi.server.*;
import java.rmi.registry.*;
import java.rmi.server.UnicastRemoteObject;
public class ServerImpl extends UnicastRemoteObject implements ServerI{
    
        public int WINSTATE_CURRENT;
        public int WINSTATE_CLOSED;
        public int WINSTATE_OPEN;

        public int TEMP_CURRENT;
        public int TEMP_MIN;
        public int TEMP_MAX;

        public int AUTOMATA_ENABLED;
        public int AUTOMATA_DISABLED;
        public int AUTOMATA_CURRENT;
        
        public int ID;
       
    public ServerImpl(int id) throws java.rmi.RemoteException{
        WINSTATE_CLOSED=0;
        WINSTATE_OPEN=1;
        WINSTATE_CURRENT=0;
       
        TEMP_MIN=20;
        TEMP_MAX=40;
        TEMP_CURRENT=25;

        AUTOMATA_ENABLED=1;
        AUTOMATA_DISABLED=0;
        AUTOMATA_CURRENT=0;
        
        ID = id;
    }
    /**
     * gets the configured temperatures
     * input is either:
     *   TEMP_CURRENT - returns the measured temp at this moment
     *   TEMP_MIN     - min threshold temp before win close
     *   TEMP_MAX     - max threshold temp before win opens
     * 
     * returns temp in cc (integer roundoff)
     */
    public int getTemp(int temp_type) {
	return MicroProcessor.getTemp(ID);
    } 
    
    public int getTempMax(){
        return MicroProcessor.getTempMax(ID);
    }
    public int getTempMin(){
        return MicroProcessor.getTempMin(ID);
    }
    
    /**
     * gets current window position
     * returns either
     *  WINSTATE_CLOSED | WINSTATE_OPEN
     */
    public int getWinState() { 
	//System.out.println(" --> getWinState");
	return MicroProcessor.getWinState(ID); 
    }
    
    /**
     * Determine if the mp's automatics is enabled.
     * returns either:
     *    AUTOMATA_ENABLED | AUTOMATA_DISABLED
     */
    public int getAutomata() { 
	//System.out.println(" --> getAutomata");
	return MicroProcessor.getAutomata(ID);
    }

    /**
     * set the mp's temp registers
     * valid input is (temp type , temp) where 
     * temptype is one of set { TEMP_MIN, TEMP_MAX }
     * temp is the temperature in celcius
     */
    public void setTemp(int tempmax, int tempmin) { 
	MicroProcessor.setTemp(ID,tempmax,tempmin);
    }

    /**
     * Force the window either open or closed
     * using the input:
     *   WINSTATE_CLOSED | WINSTATE_OPEN
     *
     * note: if the mp's automata is enabled,
     *       the automata might change window
     *       state again right away
     */
    public void setWinState(int state) {
	MicroProcessor.setWinState(ID,state);
    }

    /**
     * Enables/Disables the automata steering the
     * window
     * valid input are:
     *   AUTOMATA_ENABLED | AUTOMATA_DISABLED
     */
    public void setAutomata(int state) { 
	MicroProcessor.enableAutomata(ID,state);
    } 
    
    public void receiveMessage(String x) throws RemoteException
    {
        System.out.println(x);
    }
}
