#include "testgroup.h"

namespace test {
  class TestClassItem {
  public:
    TestClassItem(TestClassItem *&list, TestClass &cls) : m_cls(cls){
      if(list) 
	next = list;
      else
	next = 0;
      list = this;
    }
    ~TestClassItem() {
      if(next != 0) delete next;
    }
    TestClass &m_cls;
    TestClassItem *next;
  };

  TestGroup::TestGroup(const char *name) : TestClass(name) {
    testClasses = 0;
  }

  TestGroup::TestGroup() : TestClass("") {
    testClasses = 0;
  }

  TestGroup::TestGroup(const TestGroup &grp) : TestClass(grp.title().c_str()), testClasses(grp.testClasses) {
    
  }
  
  TestGroup::~TestGroup() {
    if (testClasses)
      delete testClasses;
  }
  
  void 
  TestGroup::add(TestClass &tstcls) {
    new TestClassItem(testClasses,tstcls);
  }
  vector<string>
  TestGroup::list() {
    vector<string> listNames;
    TestClassItem *testClass = testClasses;
    while(testClass != 0) {
      vector<string> subLists = testClass->m_cls.list();
      for(vector<string>::iterator it = subLists.begin();it != subLists.end();it++)
	listNames.push_back(*it);
      
      testClass = testClass->next;
    }
    return listNames;
  }

  void 
  TestGroup::run_tests(Visualizer & visi,string testname) {
    TestClassItem *testClass = testClasses;
    while(testClass != 0) {
      testClass->m_cls.run_tests(visi,testname);
      testClass = testClass->next;
    }
  }
  
  int
  TestGroup::run(Visualizer & visi,string testname) {
    run_tests(visi,testname);
    return visi.print_complete();
  }
}
