#ifndef __TEST_VISI_IMPL_H__
#define __TEST_VISI_IMPL_H__

#include "visualizer.h"
#include <iostream>
#include <string>
#include <sstream>

namespace test {
  using namespace std;

  class StreamVisualizer : public Visualizer {
  public:
    StreamVisualizer(ostream &cstream);
    void begin_group(string title);
    void end_group();
    void begin_test(string title);
    void end_test();
    void no_test();
    void mark_failure(string type, string msg);

    int print_complete();
  private:
    ostream &cstr;
    stringstream strbuf;
    string currentTest;
    string currentGroup;
    int numInGroup;
    int numTests;
    int numSuccess;
  };
}

#endif
