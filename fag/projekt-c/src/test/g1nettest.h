#include "protocol/g1.net/datalink.h"
#include "driver/iodriver.h"
#include "protocol/g1.net/protocol.h"

class simpleDriver : public IODriver
{
public:
  simpleDriver ()
  {
    bufPos = 0;
    bufSize = 0;
  }
   ~simpleDriver ()
  {
  }

  byte read (byte * buf, byte size)
  {
    byte pos = 0;
    if (bufSize == 0)
      return 0;
    
    if (size > bufSize - bufPos)
      size = bufSize - bufPos;
    
    byte nPos = size + bufPos;
    
    for (; bufPos < nPos; bufPos++)
      buf[pos++] = buffer[bufPos];
    
    if (nPos == bufSize) {
      bufSize = 0;
      bufPos = 0;
    }
    return size;
  }
  
  byte write (byte * buf, const byte size)
  {
    for (byte pos = 0; pos < size; pos++)
      buffer[pos] = buf[pos];
    
    bufPos = 0;
    bufSize = size;
    return size;
  }
  byte *getBuffer ()
  {
    return buffer;
  }
  byte getSize ()
  {
    return bufSize;
  }
  void reset ()
  {
    bufPos = 0;
    bufSize = 0;
  }
private:
  byte buffer[255];
  byte bufPos;
  byte bufSize;
  
};

namespace tut
{
  struct dl_test_data
  {
  };

  typedef test_group < dl_test_data, 10 > dl_testgroup;
  typedef dl_testgroup::object dltest;
  dl_testgroup dltestgrp ("protocol.g1net.datalink");

  template <> void dltest::test < 1 > ()
  {
    // test the constructor
    simpleDriver drv;
    g1_Datalink dl (&drv);

    ensure ("Constructor is not set correctly", dl.getDriver () == &drv);
  } 
  
  template <> void dltest::test < 2 > ()
  {
    // test byte stopping
    simpleDriver drv;
    g1_Datalink dl (&drv);
    byte data[3] = { 12, SIGNAL_MARKER, 12 };
    Packet pack(data,3);
    dl.write (255, pack);
    ensure ("Protocol dosnt start with correct byte stopping sequence",
	    drv.getBuffer ()[0] == SIGNAL_MARKER &&
	    drv.getBuffer ()[1] == SIGNAL_PACKET_START);
    ensure ("Protocol dosn't end with correct byte stopping sequence",
	    drv.getBuffer ()[drv.getSize () - 2] == SIGNAL_MARKER &&
	    drv.getBuffer ()[drv.getSize () - 1] == SIGNAL_PACKET_END);
    
    ensure ("Protocol dosn't have a byte escape sequence",
	    drv.getBuffer ()[drv.getSize () - 5] == SIGNAL_MARKER &&
	    drv.getBuffer ()[drv.getSize () - 4] == SIGNAL_MARKER);
  }
  template <> void dltest::test < 3 > ()
  {
    // test read method
    simpleDriver drv;
    g1_Datalink dl (&drv);
    byte data[3] = { 12, SIGNAL_MARKER, 13 };
    Packet pack(data,3);
    dl.write (255, pack);
    Packet pack2 = dl.read();

    ensure ("Protocol dosn't fit the first byte", pack2[0] == 12);
    ensure ("Protocol dosn't fit the second byte", pack2[1] == SIGNAL_MARKER);
    ensure ("Protocol dosn't fit the third byte", pack2[2] == 13);
  }

  template <> void dltest::test < 4 > ()
  {
    // test to see if we can fake through a new package

    simpleDriver drv;
    g1_Datalink dl (&drv);
    byte tstdata[3] = { 120, 43, 25 };
    
    drv.write (tstdata, 3);
    byte data[3] = { 11, 1, 1 };
    Packet pack(data,3);
    dl.write (255, pack);
    Packet pack2 =  dl.read ();
    ensure ("we have not read 3 bytes", pack.size() == 3);
    ensure ("The first byte is not correct", pack2[0] == 11);
    ensure ("The second byte is not correct", pack2[1] == 1);
    ensure ("The third byte is not correct", pack2[2] == 1);
  }
  
  template <> void dltest::test < 5 > ()
  {
    // test to see if the packet is dropped if its 
    // not the correct reciever
    simpleDriver drv;
    g1_Datalink dl (&drv);

    dl.setSenderId (10);
    Packet pack1, pack2;

    pack1 << 120;
    pack2 << 210;

    dl.write (12, pack1);	// wrong senderid
    dl.write (10, pack2);	// right senderid
    Packet pack = dl.read ();
    ensure ("We have not recieved the right data", pack[0] == 210);
    // now we send it as a broadcast id.
    dl.write (12, pack1);	// still wrong senderid
    dl.write (255, pack2);	// now send out as a broadcast
    pack = dl.read ();
    ensure ("WE have not recieved the right broadcast data", pack[0] == 210);
  }
  
  struct tc_testdata
  {
    simpleDriver drv;
  };
  typedef test_group < tc_testdata, 10 > tc_testgroup;
  typedef tc_testgroup::object tctest;
  tc_testgroup tctestgrp ("protocol.g1net.control");

  template <> void tctest::test < 1 > ()
  {
    drv.reset ();
    g1_Datalink dl (drv);
    /*     g1_Network &nw (drv);
	   g1_Control cl (nw); */

  }
};


