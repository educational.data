#include "config.h"
#include "kernel.h"
#include "utility/item.h"

namespace tut
{
  struct mailbox_test_data
  {
  };

  typedef test_group < mailbox_test_data > mailbox_testgroup;
  typedef mailbox_testgroup::object mailbox_testobject;
  mailbox_testgroup mailtestgrp ("sync.mailbox");

  template <> void mailbox_testobject::test < 1 > ()
  {
    Kernel *kern = Kernel::instance ();
    
    ensure ("Kernel instance was empty", kern != 0);
    
    Item *ms = new Item;
    kern->mail.box (1).send (ms);
    
    ensure ("The message sent was not the message sent",
	    kern->mail.box (1).receive () == ms);
    delete ms;
  }
};
