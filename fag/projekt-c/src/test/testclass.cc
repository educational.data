#include "testclass.h"
#include <exception>
#include "stdio.h"
namespace test {
  class done_excep_complete : public exception { };
  class done_excep : public exception { };
  
  //void TestClass::test1()  { throw done_excep_complete(); }
  void TestClass::test2()  { throw done_excep_complete(); }
  void TestClass::test3()  { throw done_excep_complete(); }
  void TestClass::test4()  { throw done_excep_complete(); }
  void TestClass::test5()  { throw done_excep_complete(); }
  void TestClass::test6()  { throw done_excep_complete(); }
  void TestClass::test7()  { throw done_excep_complete(); }
  void TestClass::test8()  { throw done_excep_complete(); }
  void TestClass::test9()  { throw done_excep_complete(); }
  void TestClass::test10() { throw done_excep_complete(); }
  void TestClass::test11() { throw done_excep_complete(); }
  void TestClass::test12() { throw done_excep_complete(); }
  void TestClass::test13() { throw done_excep_complete(); }
  void TestClass::test14() { throw done_excep_complete(); }
  void TestClass::test15() { throw done_excep_complete(); }
  void TestClass::set_up() { }
  void TestClass::tear_down() { }

  TestClass::TestClass(const TestClass &cls) {
    m_title = cls.m_title;
    m_visi = cls.m_visi;
  }

  TestClass::TestClass(const char *title) {
    m_title = title;
  }

  vector<string>
  TestClass::list() {
    vector<string> name;
    if(m_title.length() != 0)
      name.push_back(title());
    return name;
  }
  void TestClass::run_tests(Visualizer & visi, string testname) {
    if(testname != "" && testname != title())
      return;
    m_visi = &visi;

    m_visi->begin_group(title());
    set_up();
    try {
      try { begin(1); test1(); end(); } catch ( const done_excep &ex ) { }
      try { begin(2); test2(); end(); } catch ( const done_excep &ex ) { }
      try { begin(3); test3(); end(); } catch ( const done_excep &ex ) { }
      try { begin(4); test4(); end(); } catch ( const done_excep &ex ) { }
      try { begin(5); test5(); end(); } catch ( const done_excep &ex ) { }
      try { begin(6); test6(); end(); } catch ( const done_excep &ex ) { }
      try { begin(7); test7(); end(); } catch ( const done_excep &ex ) { }
      try { begin(8); test8(); end(); } catch ( const done_excep &ex ) { }
      try { begin(9); test9(); end(); } catch ( const done_excep &ex ) { }
      try { begin(10);test10();end(); } catch ( const done_excep &ex ) { }
      try { begin(11);test11();end(); } catch ( const done_excep &ex ) { }
      try { begin(12);test12();end(); } catch ( const done_excep &ex ) { }
      try { begin(13);test13();end(); } catch ( const done_excep &ex ) { }
      try { begin(14);test14();end(); } catch ( const done_excep &ex ) { }
      try { begin(15);test15();end(); } catch ( const done_excep &ex ) { }
    } 
    catch (const done_excep_complete & ex) { m_visi->no_test(); }
    catch (const exception & ex) {
      m_visi->mark_failure("Exception thrown",ex.what());
    }
    tear_down();
    m_visi->end_group();
  }
  void TestClass::end() {
    m_visi->end_test();
  }
  
  void TestClass::begin(int id) {
    char testTekst[10];
    sprintf(testTekst,"test%d", id);
    m_visi->begin_test(testTekst);
  }
  void TestClass::fail() {
    fail("failed");
  }
  void TestClass::fail(string msg) {
    m_visi->mark_failure("fail",msg);
    throw done_excep();
  }
  void TestClass::ensure(bool result) {
    ensure("ensure not true",result);
  }
  void TestClass::ensure(string msg, bool result) {
    if(!result) {
      m_visi->mark_failure("ensure",msg);
      throw done_excep();
    }
  }
}
