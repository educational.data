#ifndef __TEST_VISUALIZER_H__
#define __TEST_VISUALIZER_H__

#include <string>

namespace test {
  using namespace std;
  class Visualizer {
  public:
    virtual ~Visualizer() { }
    virtual void begin_group(string title) = 0;
    virtual void end_group() = 0;
    virtual void begin_test(string title) = 0;
    virtual void end_test() = 0;
    virtual void no_test() = 0;
    virtual void mark_failure(string type, string msg) = 0;
    virtual int print_complete() = 0;
  };
}

#endif // __TEST_VISUALIZER_H__
