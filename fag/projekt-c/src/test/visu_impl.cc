#include "visu_impl.h"

namespace test {
  StreamVisualizer::StreamVisualizer(ostream &cstream) : cstr(cstream) {
    numTests = 0;
    numSuccess = 0;
  }

  void
  StreamVisualizer::begin_group(string title) {
    cstr << title << ": <" << flush;
    currentGroup = title;
    numInGroup = 0;
  }

  void
  StreamVisualizer::end_group() {
    cstr << "> " <<  endl;
  }

  void
  StreamVisualizer::begin_test(string title) {
    ++numTests;
    ++numInGroup;
    currentTest = title;
  }

  void
  StreamVisualizer::end_test() {
    ++numSuccess;
    cstr << "." << flush;
  }
  void
  StreamVisualizer::no_test() {
    --numTests;
  }

  void
  StreamVisualizer::mark_failure(string type, string msg) {
    cstr << "F=" << numInGroup << flush;
    strbuf << endl;
    strbuf << "Test failure in group: " << currentGroup << endl;
    strbuf << "Test: " << currentTest << ", Type: " << type << "." << endl;
    strbuf << "Failure message: " << msg << endl;
  }

  int
  StreamVisualizer::print_complete() {
    string str = strbuf.str();
    cstr << str << endl;
    if(numSuccess != numTests) {
      cstr << "FAILED: passed only " << numSuccess << " out of " << numTests << " tests." << endl;
      return 1;
    } else {
      cstr << "OK: passed all " << numSuccess << " out of " << numTests << " tests." << endl;
      return 0;
    }
  }

}
