#include "config.h"
#include "kernel.h"
#include "kernel/thread.h"
#include "kernel/thrlist.h"

void
dummyFunc2 ()
{
}

namespace tut
{
  struct threadlist_test_data
  {
  };

  typedef test_group < threadlist_test_data, 10 > threadlist_testgroup;
  typedef threadlist_testgroup::object threadlist_testobject;
  threadlist_testgroup threadlisttestgrp ("kernel.threadlist");

  // test ques
  template <> void threadlist_testobject::test < 1 > ()
  {
    ThreadList tlist;
    // Thread dummy1(dummyFunc2,10,"a", 255);
    // Thread dummy2(dummyFunc2,10,"b", 255);
    Thread dummy1 (dummyFunc2, 100, "a");
    Thread dummy2 (dummyFunc2, 100, "b");
    Thread dummy3 (dummyFunc2, 0, "c");

    tlist.push (&dummy3);
    tlist.push (&dummy1);
    
      ensure ("ThreadList does not contain thread a",
	      tlist.top () == &dummy1);
      
      tlist.push (&dummy2);

      ensure
	("Thread 1 is not on top of threadlist after insertion of another thread",
	 tlist.top () == &dummy1);
      
      tlist.calculateNextThread ();
      
      ensure
	("Thread 2 is not on top of threadlist after a calculation",
	 tlist.pop () == &dummy2);
      
      ensure ("Thread 1 has disappered after rotation and pop",
	      tlist.pop () == &dummy1);
  }
};
