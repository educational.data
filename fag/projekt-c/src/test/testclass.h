#ifndef __TEST_TESTCLASS_H__
#define __TEST_TESTCLASS_H__

#include <string>
#include <vector>
#include "visualizer.h"

namespace test {
  using namespace std;
  class TestClass {
    string m_title;
    Visualizer *m_visi;
  public:
    TestClass(const char *title);
    TestClass(const TestClass &cls);

    string title() const 
    { 
      return m_title; 
    }

    virtual vector<string> list();

    virtual ~TestClass() { }

    virtual void run_tests(Visualizer &visi, string testname);
    virtual void set_up();
    virtual void tear_down();

    virtual void test1() = 0;
    virtual void test2();
    virtual void test3();
    virtual void test4();
    virtual void test5();
    virtual void test6();
    virtual void test7();
    virtual void test8();
    virtual void test9();
    virtual void test10();
    virtual void test11();
    virtual void test12();
    virtual void test13();
    virtual void test14();
    virtual void test15();
  protected:
    void ensure(bool result);
    void ensure(string failmsg, bool result);
    void fail();
    void fail(string message);
  private:
    void end();
    void begin(int id);
  };
}
#endif
