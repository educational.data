#ifndef __TESTGROUP_H__
#define __TESTGROUP_H__

#include "testclass.h"
#include "visualizer.h"
#include <vector>
#include <string>

namespace test {
  using namespace std;

  class TestClassItem;
  
  class TestGroup : public TestClass {
  public:
    TestGroup();
    TestGroup(const char *name);
    TestGroup(const TestGroup &grp);

    vector<string> list();

    ~TestGroup();
    void add(TestClass &cls);
    int run(Visualizer & visi, string testname);
    void test1() { }
  protected:
    void run_tests(Visualizer & visi,string testname);
  private:
    TestClassItem *testClasses;
  };
}

#endif
