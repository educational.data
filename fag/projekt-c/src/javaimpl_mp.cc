typedef long long __int64;
#include "funcs/MicroProcessor.h"
#include "funcs/ghserverproxy.h"
#include "funcs/ighsrv.h"
#include "funcs/ghsrvstb.h"
#include "driver/rs232/rs232_unix.h"
#include "protocol/g1.net/datalink.h"
#include "protocol/g1.net/simpleDriver.h"
#include <unistd.h>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

class GHouse : public IGHServer {
  byte min;
  byte max;
  byte cur;
  bool winState;
  bool automata;
public:
  GHouse() {
    min=20;
    cur=25;
    max=35;
    winState = false;
    automata = true;
  }
  byte getTemp(int type) {
    getWinState();

    switch(type) {
    case 0:
      return winState ? --cur : ++cur;
    case 2:
      return max;
    case 1:
      return min;
    default:
      return 0;
    }
  }
  byte getWinState() {
    if(automata)
      if(cur<=min) 
	winState = false;
      else if(cur>=max)
	winState = true;
    return winState ? 1 : 0;
  }
  byte getAutomata() {
    return automata ? 1 : 0;
  }
  void setTemp(byte min, byte max) {
    this->min = min;
    this->max = max;
  }
  void setWinState(byte state) {
    winState = state > 0;
  }
  void enableAutomata(byte state) {
    automata = state > 0;
  }
};

class mthr : public UnixPThread {
  GHServerStub &stub;
public:
  mthr(GHServerStub &stb) : stub(stb) {
    Start();
  }
  void *main() {
    for(;;)stub.parse();
  }
};

class server {
  server() : 
    ser("/dev/ttyS0"), 
    ser1("/dev/ttyS0"), 
    ser2("/dev/ttyS0"),
    dlk1(ser1,40),
    dlk2(ser2,41),
    dlk(ser,0),
    stb1(dlk1,gh1),
    stb2(dlk2,gh2) ,
    th1(stb1),
    th2(stb2) 
  {
    // Initialize a list of 200 greenhouses.
    for(int id=0;id<200;id++)
      proxies.push_back(GHServerProxy(dlk,id));
  }
  Serial_unix ser1;
  Serial_unix ser2;
  
  g1_Datalink dlk1;
  g1_Datalink dlk2;
  GHServerStub stb1;
  GHServerStub stb2;
  GHouse gh1;
  GHouse gh2;

  mthr th1,th2;
	  

public:

  Serial_unix ser;
  // simpleDriver ser;
  g1_Datalink dlk;
  vector<GHServerProxy> proxies;

  static server &instance() {
    static server inst;
    return inst;
  }
};

/*
 * Class:     MicroProcessor
 * Method:    enableAutomata
 * Signature: (I)V
 */
extern "C" JNIEXPORT void JNICALL // __declspec(dllexport)
Java_MicroProcessor_enableAutomata(JNIEnv *e, jclass q, jint id, jint a ) {
  server::instance().proxies[id].enableAutomata((byte)a);
}


/*
 * Class:     MicroProcessor
 * Method:    getAutomata
 * Signature: ()I
 */
extern "C" JNIEXPORT jint JNICALL 
Java_MicroProcessor_getAutomata(JNIEnv *e, jclass q, jint id) {
  return server::instance().proxies[(int)id].getAutomata();
}

/*
 * Class:     MicroProcessor
 * Method:    getGreenHouses
 * Signature: ()Ljava/lang/String;
 */
extern "C" JNIEXPORT jstring JNICALL 
Java_MicroProcessor_getGreenHouses(JNIEnv *env , jclass c) {
  server &sv = server::instance();
  
  vector<g1_term_id> ids = sv.dlk.getConnections();


  ostringstream data;
  for (int id = 0;id<ids.size();id++) {
    data << (int)ids[id];
    if(id!=ids.size()-1) 
      data << " ";
  }
  return env->NewStringUTF(data.str().c_str());
}

/*
 * Class:     MicroProcessor
 * Method:    getTemp
 * Signature: (I)I
 */
extern "C" JNIEXPORT jint JNICALL 
Java_MicroProcessor_getTemp(JNIEnv *e, jclass q, jint id) {
  GHServerProxy &prx = server::instance().proxies[(int)id];
  return prx.getTemp(0);
}

/*
 * Class:     MicroProcessor
 * Method:    getTempMax
 * Signature: ()I
 */
extern "C" JNIEXPORT jint JNICALL 
Java_MicroProcessor_getTempMax(JNIEnv *e, jclass q, jint id) {
  return server::instance().proxies[(int)id].getTemp(1);
}

/*
 * Class:     MicroProcessor
 * Method:    getTempMin
 * Signature: ()I
 */
extern "C" JNIEXPORT jint JNICALL 
Java_MicroProcessor_getTempMin(JNIEnv *e, jclass q, jint id) {
  return server::instance().proxies[(int)id].getTemp(2);
}

/*
 * Class:     MicroProcessor
 * Method:    getWinState
 * Signature: ()I
 */
extern "C" JNIEXPORT jint JNICALL 
Java_MicroProcessor_getWinState(JNIEnv *e, jclass q, jint id) {
  return server::instance().proxies[(int)id].getWinState();
}

/*
 * Class:     MicroProcessor
 * Method:    setTemp
 * Signature: (II)V
 */
extern "C" JNIEXPORT void JNICALL 
Java_MicroProcessor_setTemp(JNIEnv *q, jclass a, jint id, jint type, jint val) {
  server::instance().proxies[(int)id].setTemp(type,val);
}

/*
 * Class:     MicroProcessor
 * Method:    setWinState
 * Signature: (I)V
 */
extern "C" JNIEXPORT void JNICALL 
Java_MicroProcessor_setWinState(JNIEnv *q, jclass a, jint id, jint state) {
  server::instance().proxies[(int)id].setWinState(state);
}
