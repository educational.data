/********************************************************************
 **
 ** Kernel.cpp
 **
 ** Facade object for alle applikationer. denne er den eneste mulighed
 ** den enkelte applikation har for at komme til systemet
 ** yderligere er det ogs� den der opretter selve scheduleren i
 ** systemet.
 **
 ** $Revision: 1.5 $
 ********************************************************************/

#include "kernel.h"
#include "proc/am186er.h"
#include "driver/timer/tmrdrv.h"

Kernel::Kernel()
{
  // Allow timer interrupts
  Am186ER::PCB->icu.timerControl = 1;
  
  // Setup the global TimerDriver
  TimerDriver::instance();
}

Kernel *
Kernel::instance()
{
  static Kernel  *inst = 0;
  if (inst == 0) {
    inst = new Kernel();
  }
  return inst;
}

void 
Kernel::start(bool bPreemptive)
{
  scheduler.start(bPreemptive);
}
