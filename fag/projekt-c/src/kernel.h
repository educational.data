/********************************************************************
 **
 ** Kernel.h
 **
 ** Facade object for alle applikationer. denne er den eneste mulighed
 ** den enkelte applikation har for at komme til systemet
 ** yderligere er det ogs� den der opretter selve scheduleren i
 ** systemet.
 **
 ********************************************************************/

#ifndef __KERNEL_H__
#define __KERNEL_H__

#include "kernel/sched.h"
#include "sync/mbctrl.h"

class Kernel
{
  Kernel ();
public:
  static Kernel *instance ();

  void start (bool bPreemptive = true);

  Scheduler scheduler;
};

#endif // __KERNEL_H__
