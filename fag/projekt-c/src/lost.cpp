#include "kernel.h"
#include "kernel/thread.h"
#include "kernel/scheduler.h"
#include "am186er.h"
#include "sync/mutex.h"
#include "driver/timer.h"

#ifndef __GNUC__
Mutex mut;

void
thread1func()
{
  Am186ER::PCB->timer[0].maxCountA = SCH_TIMEOUT;
  Am186ER::PCB->timer[0].control =
    TIMER_ENABLE | TIMER_INTERRUPT | TIMER_PRESCALE;
  
  Timer           timer;
  
  mut.take();
  timer.start(10000);
  timer.waitfor();
  mut.release();
  
  while (1) {
    outportb(0x500, 237);
  }
}

void
thread2func()
{
  Am186ER::PCB->timer[0].maxCountA = SCH_TIMEOUT;
  Am186ER::PCB->timer[0].control =
    TIMER_ENABLE | TIMER_INTERRUPT | TIMER_PRESCALE;
  
  Timer timer;
  timer.start(1000);
  timer.waitfor();
  
  mut.take();
  
  while (1) {
    outportb(0x500, 112);
  }
}

void
thread3func()
{
  Am186ER::PCB->timer[0].maxCountA = SCH_TIMEOUT;
  Am186ER::PCB->timer[0].control =
    TIMER_ENABLE | TIMER_INTERRUPT | TIMER_PRESCALE;
  
  while (1) {
    outportb(0x500, 97);
  }
}

void
thread4func()
{
  Am186ER::PCB->timer[0].maxCountA = SCH_TIMEOUT;
  Am186ER::PCB->timer[0].control =
    TIMER_ENABLE | TIMER_INTERRUPT | TIMER_PRESCALE;
  
  Timer timer;
  timer.start(5000, TT_PERIODIC);
  
  while (1) {
    timer.waitfor();
    outportb(0x500, 45);
  }
}

void
thread5func()
{
  Am186ER::PCB->timer[0].maxCountA = SCH_TIMEOUT;
  Am186ER::PCB->timer[0].control =
    TIMER_ENABLE | TIMER_INTERRUPT | TIMER_PRESCALE;
  
  Timer timer;
  timer.start(2000, TT_PERIODIC);
  
  while (1) {
    timer.waitfor();
    outportb(0x500, 35);
  }
}

Thread          thread1(thread1func, 100, "dev/thread/1", NULL, 512);
Thread          thread2(thread2func, 100, "dev/thread/2", NULL, 512);
Thread          thread3(thread3func, 100, "dev/thread/3", NULL, 512);
Thread          thread4(thread4func, 100, "dev/thread/4", NULL, 512);
Thread          thread5(thread5func, 100, "dev/thread/5", NULL, 512);

int
main(void)
{
  thread1.run();
  thread2.run();
  // thread3.run();
  thread4.run();
  thread5.run();
  
  Kernel::instance()->start(true);
}
#endif // __GNUC__
