#include "config.h"
#include <memory>

#include "unixthread.h"
#include "/usr/include/sched.h"

/**
 * void *threadFunc(void *)
 *
 * local function used to pass in the call from C to C++
 * standard C cannot always parse to C++ calls and must
 * therefore use a wrapper function to make the correct
 * call
 * It calls the Run function on the thread object.
 */
void* threadFunc(void *pvThis) 
{
  UnixPThread *pThis = static_cast<UnixPThread *>(pvThis);
  void *ret = pThis->main();
  return ret;
}

/**
 * CREATE:
 * Thread()
 *
 * Creates the pimpl object and sets the internal state
 * to closed.
 */
UnixPThread::UnixPThread() {
  state = closed;
}

UnixPThread::~UnixPThread() {

}

/**
 * void Start()
 *
 * Function to call from the parent.
 * It calls the std c pthread library function
 * to initialise a C++ thread
 * Its initiated through the threadFunc function
 */
void 
UnixPThread::Start () {
  if(state != stopped) {
    // Make some type casting that ANSI C++ permits
    void *ths = static_cast<void *>(this);
    state = running;
    pthread_create(&t_id, NULL, &threadFunc,ths);
  }
}

/**
 * void main()
 *
 * The main Thread function of this object;
 */
void *
UnixPThread::main () {
  // empty!!! (as supposed to be)
}

/**
 * void Stop()
 *
 * Stops the running thread from being run
 * the Thread cannot resume from here on
 *
 * TODO: Implement accrding to the pthread
 *       Standard
 */
void 
UnixPThread::Stop () {
  if(state = running) {
    // stop the thread
    state = stopped;
  }
}

/**
 * void Suspend()
 * 
 * If the thread is running it can be suspended
 * while later being resumed. This is a lite 
 * way of stopping the thread
 *
 * TODO: Implement according to standard POSIX
 */
void 
UnixPThread::Suspend () {
  if(state == running) {
    // thr_suspend(pimpl_->t_id);
    state = waiting;
  }
}

/**
 * void Continue()
 *
 * Continue a suspended thread (see Suspend::)
 */
void 
UnixPThread::Continue () {
  if(state == waiting) {
    // thr_continue(pImpl->t_id);
    state = running;
  }
}

/**
 * void Yield()
 *
 * Yield a process in the running queue
 *
 */

void 
UnixPThread::Yield() {
#ifdef HAVE_SCHED_YIELD
  sched_yield();
#else
#warning Not using sched yield is dangerous
#endif
}

/**
 * void Join()
 *
 * Joins a thread to the calling thread
 */
void 
UnixPThread::Join() {
  if(state == running) {
    if(pthread_join(t_id,NULL) != 0)
      cout << "We have not joined - some errors are found" << endl;

    state = joined;
  }
}
