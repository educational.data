/********************************************************************
 **
 ** Scheduler.h
 **
 ** Scheduleren er hjertet i vores ops. den implementerer roundrobin
 ** som task shiftings algoritmen
 **
 ** $Revision: 1.1 $
 **
 ********************************************************************/

#ifndef __SCHEDULER_H__
#define __SCHEDULER_H__

#include "config.h"

#define SCH_TIMEOUT 100

class _sched_data;

class Thread;
class Kernel;

class Scheduler
{
  Scheduler ();
  ~Scheduler ();

  _sched_data *self;

  void contextSwitch (Thread * oldThread, Thread * newThread);
  void restartTimer ();
  static void interrupt __isr__ ();
  static void __idle__ ();

  friend class Kernel;
  friend class Thread;
public:

  void addThread (Thread *);
  void removeThread (Thread *);

  void start (bool bPreemptive = true);

  void lock ();
  void unlock ();

  void __saveregs schedule ();
  Thread *getRunningThread ();

};


#endif // __SCHEDULER_H__
