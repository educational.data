#ifndef __THREADLIST_H__
#define __THREADLIST_H__

#include "utility/item.h"
#include "kernel/thread.h"

class _RRTList_data;

class ThreadList : public Queue
{
public:
  ThreadList ();
  ~ThreadList ();

  // overridden funcs
  void push (Item *);
  void update (Thread *);
  void calculateNextThread ();
};

#endif // __THREADDLIST_H__
