/********************************************************************
 **
 ** thread.cpp
 **
 ** $Revision: 1.4 $
 ********************************************************************/

#include "kernel/thread.h"
#include "kernel.h"
#include "proc/am186er.h"
#include <string.h>

Thread::Thread(void (*fn) (),
	       BYTE priority, 
	       char *name, 
	       void *stack, 
	       UINT stacksize)
{
  state = THR_NOT_INSERTED;
  
  this->priority = priority;
  
  if (stack == NULL) {
    stack = new unsigned[stacksize];
    freeStack = 1;
  } else 
    freeStack = 0;
  
  
  static int cId = 0;
  
  if (name)
    memcpy(threadName, name, sizeof threadName);
  else
    threadName[0] = '\0';
  
  if (priority == 0)
    threadId = 0;
  else
    threadId = ++cId;
  
  this->stacksize = stacksize;
  this->stack = (unsigned int *) stack;
  if (this->stack == NULL) {
    state = THR_DEAD;
    error = THR_ERR_NOSTACK;
    return;
  }
  // set stack
  unsigned int   *tos = &this->stack[stacksize - 1];
  context[0].j_sp = FP_OFF(tos);
  context[0].j_ss = FP_SEG(tos);
  
  // set CS:IP to entrypoint of fn
  context[0].j_ip = FP_OFF(fn);
  context[0].j_cs = FP_SEG(fn);
  
  // set data segment and extra segment
  context[0].j_ds = _DS;
  context[0].j_es = _ES;
  
  // set flags for new task
  context[0].j_flag = 0xF202;
  
  // these registers are not important
  context[0].j_si = 0;
  context[0].j_di = 0;
  context[0].j_bp = 0;
  
  state = THR_NOT_INSERTED;
  error = THR_ERR_OK;
}

Thread::~Thread()
{
  delete[]stack;
}

void
Thread::run()
{
  if (state == THR_NOT_INSERTED)
    Kernel::instance()->scheduler.addThread(this);
}

void
Thread::kill()
{
  // ignore requests to kill dead threads
  if (state != THR_DEAD) {
    // remove the thread permanent
    Kernel::instance()->scheduler.removeThread(this);
    state = THR_DEAD;
  }
}

void
Thread::restoreContext()
{
#ifndef __GNUC__
  longjmp(context, 1);
#endif
}

int
Thread::saveContext()
{
#ifndef __CNUC__
  return setjmp(context);
#else
  return 0;
#endif
}


Thread*
Thread::getCurrentThread()
{
  return Kernel::instance()->scheduler.getRunningThread();
}
