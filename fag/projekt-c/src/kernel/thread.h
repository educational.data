/********************************************************************
 **
 ** thread.h
 **
 ** $Revision: 1.4 $
 **
 ********************************************************************/

#ifndef __THREAD_H__
#define __THREAD_H__

#include "config.h"
#include "utility/queue.h"

#ifndef __GNUG__
#include <setjmp.h>
#endif

class Scheduler;
class ThreadList;
class Condition;
class Mutex;

enum ThreadState
{
  THR_NOT_INSERTED,		// default state;
  THR_READY,
  THR_SUSPENDED,
  THR_RUNNING,
  THR_DEAD
};

enum ThreadError
{
  THR_ERR_OK = 0,
  THR_ERR_NOSTACK = -1
};

/**
 * Alle bruger programmer skal anvende denne klasse til at h�ndtere
 * deres selvst�ndige tr�de.
 */
class Thread : public Item
{
  // Thread identification data
  UINT threadId;
  char threadName[40];
  BYTE priority;

  // stack information
  UINT freeStack;
  UINT *stack;
  UINT stacksize;

  // context data
  jmp_buf context;

  // state and error
  ThreadError error;
  ThreadState state;

  // functions only to be called from scheduler::contextSwitch
  void restoreContext ();
  int saveContext ();

public:
  Thread (void (*fn) (),
	  BYTE priority = 100,
	  char *name = NULL, void *stack = NULL, UINT stacksize = 255);
  virtual ~ Thread ();
  void kill ();

  // S�tter en process i stand til at k�re
  void run ();

  ThreadState getState () const
  {
    return state;
  }
  UINT getId () const
  {
    return threadId;
  }
  BYTE getPriority () const
  {
    return priority;
  }
  const char *getName () const
  {
    return (const char *) threadName;
  }
  // this function will lookup the running thread in the scheduler.
  static Thread *getCurrentThread ();

  // Classes that are known to access all deaper details of the thread
  friend class Scheduler;
  friend class ThreadList;
  friend class Condition;
  friend class Mutex;
};

#endif // __THREAD_H__
