#include "config.h"
#include "kernel.h"
#include "kernel/thread.h"
#include "kernel/thrlist.h"
#include "test/testgroup.h"
#include "sync/mutex.h"
#include "sync/cond.h"

void
dummyFunc ()
{
 lp:
  goto lp;
}

void
dummyFunc2 ()
{
}

class testThreadlist : public test::TestClass {
public:
  testThreadlist() : test::TestClass("kernel.threadlist") {
    
  }

  // test ques
  void test1()
  {
    ThreadList tlist;
    // Thread dummy1(dummyFunc2,10,"a", 255);
    // Thread dummy2(dummyFunc2,10,"b", 255);
    Thread dummy1 (dummyFunc2, 100, "a");
    Thread dummy2 (dummyFunc2, 100, "b");
    Thread dummy3 (dummyFunc2, 0, "c");

    tlist.push (&dummy3);
    tlist.push (&dummy1);
    
      ensure ("ThreadList does not contain thread a",
	      tlist.top () == &dummy1);
      
      tlist.push (&dummy2);

      ensure
	("Thread 1 is not on top of threadlist after insertion of another thread",
	 tlist.top () == &dummy1);
      
      tlist.calculateNextThread ();
      
      ensure
	("Thread 2 is not on top of threadlist after a calculation",
	 tlist.pop () == &dummy2);
      
      ensure ("Thread 1 has disappered after rotation and pop",
	      tlist.pop () == &dummy1);
  }
};

class testScheduler : public test::TestClass {
public:
  testScheduler() : test::TestClass("kernel.scheduler") {
  }

  // test ques
  void test1()
  {
    Thread *thr = new Thread (dummyFunc);
    
    Kernel::instance ()->scheduler.addThread (thr);
    Kernel::instance ()->start ();
    
    ensure ("first thread has not entered running state",
	    thr->getState () == THR_RUNNING);
    
    Kernel::instance ()->scheduler.removeThread (thr);
    
    ensure ("Thread state not set correctly on remove",
	    thr->getState () == THR_NOT_INSERTED);
    Kernel::instance ()->scheduler.removeThread (thr);
    
    Kernel::instance ()->scheduler.schedule ();
    delete thr;
  }
  
  //test mutex 
  void test2()
  {
    // Thread *thr = new Thread (dummyFunc, 10,"dummy", 255);
    Thread *thr = new Thread (dummyFunc);
    Kernel::instance ()->scheduler.addThread (thr);
    Kernel::instance ()->scheduler.start (false);
    ensure ("Thread is not running as it should be in initial state before a mutex",
	    thr->getState () == THR_RUNNING);
    Mutex mut;
    
    mut.take ();
    
    ensure ("Thread is suspended allready after one 'take' in a mutex",
	    thr->getState () == THR_RUNNING);
    
    ensure ("Thread is not the current Thread",
	    thr == Thread::getCurrentThread ());
    
    mut.take ();
    
    ensure ("Thread is NOT suspended after second 'take' in a mutex",
	    thr->getState () != THR_RUNNING);
    
    mut.release ();
    Kernel::instance ()->scheduler.removeThread (thr);
    ensure ("Thread state not set correctly on remove",
	    thr->getState () == THR_NOT_INSERTED);
    
    Kernel::instance ()->scheduler.removeThread (thr);
    
    Kernel::instance ()->scheduler.schedule ();
    
    delete thr;
  }
  
  
  // test condition
  void test3()
  {
    // Thread *thr = new Thread (dummyFunc, 10, "Dummy", 255);
    Thread *thr = new Thread (dummyFunc);
    Kernel::instance ()->scheduler.addThread (thr);
    Kernel::instance ()->scheduler.start ();
    
    Condition cond;
    ensure ("Thread is not running as it should be in initial state",
	    thr->getState () == THR_RUNNING);
    
    cond.wait ();
    
    ensure ("Thread is not sleeping as it should be after 'wait()'",
	    thr->getState () != THR_RUNNING);
    
    cond.signal ();
    
    ensure ("Thread has not been reawaken after 'signal()'",
	    thr->getState () == THR_RUNNING);
    
    Kernel::instance ()->scheduler.removeThread (thr);
    delete thr;
  }
  
  void test4()
  {
    Thread *thr = new Thread (dummyFunc, 100, "1");
    Thread *thr2 = new Thread (dummyFunc, 100, "2");
    
    thr->run ();
    thr2->run ();
    
    Kernel::instance ()->scheduler.start (false);
    Kernel::instance ()->scheduler.schedule ();
    ensure ("Current thread is not thr", thr == Thread::getCurrentThread ());
    Kernel::instance ()->scheduler.schedule ();
    ensure ("Current thread is not thr2",
	    thr2 == Thread::getCurrentThread ());
    Kernel::instance ()->scheduler.schedule ();
    ensure ("Current thread is not thr", thr == Thread::getCurrentThread ());
    
    Kernel::instance ()->scheduler.removeThread (thr);
    delete thr;
    
    Kernel::instance ()->scheduler.removeThread (thr2);
    delete thr2;
    
    Kernel::instance ()->scheduler.schedule ();
    
    ensure ("Last thread is not idle thread",
	    Thread::getCurrentThread ()->getId () == 0);
  }
  
};


namespace test {
  testThreadlist testthreadlist;
  testScheduler testscheduler;
  TestGroup testkernelsection;
}

test::TestClass &prepareKernel() {
  using namespace test;
  testkernelsection.add(testthreadlist);
  testkernelsection.add(testscheduler);
  return testkernelsection;
}
