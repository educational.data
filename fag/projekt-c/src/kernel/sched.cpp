/*************************************************************
 **  scheduler.cpp
 **
 **  implements both Scheduler
 **
 **  $Id: sched.cpp,v 1.2 2004-10-13 10:30:34 tobibobi Exp $
 **************************************************************/

#include "kernel/sched.h"
#include "kernel/thrlist.h"
#include "kernel/thread.h"
#include "driver/timer/timer.h"
#include "kernel.h"
#include "proc/am186er.h"
#include "config.h"

enum _sched_enum_state {
  SCH_INACTIVE,
  SCH_ACTIVE
};

// pimpl idiom
class _sched_data {
public:
  ThreadList threadQueue;
  Thread     *runningThread;
  
  _sched_enum_state state;
  int        iLockLevel;
  bool       bReschedule;
  bool       bPreemptive;
};

/**
 * Constructor Scheduler
 * Adds an emty Thread to the running thread
 * this thread will do nothing than repeat itself when running
 */
Scheduler::Scheduler()
{
  // by using the pimpl idiom, we create a private data area here.
  self = new _sched_data;
  
  // create the idle dummy thread
  Thread         *idleThread =
    new Thread(Scheduler::__idle__, 0, "idle");
  addThread(idleThread);
  
  // set runningThread to a NULL thread
  self->runningThread = (Thread *) NULL;
  
  // make sure scheduling is enabled per default.
  self->iLockLevel = 0;
  self->bReschedule = false;
}

Scheduler::~Scheduler()
{
  Am186ER::PCB->timer[0].control = TIMER_DISABLE;
  delete self;
}

/**
 * void lock()
 *
 * increment the lock count and thereby prevent the scheduler
 * from executing
 */
void
Scheduler::lock()
{
  __enter_cs;
  ++(self->iLockLevel);
  __exit_cs;
}

/**
 * void unlock()
 *
 * decrement the lock count and call the scheduler if
 * there is any pending requests
 */
void
Scheduler::unlock()
{
  __enter_cs;
  
  --(self->iLockLevel);
  if (self->iLockLevel == 0 && self->bReschedule) {
    self->bReschedule = false;
    schedule();
  }
  
  __exit_cs;
}

/**
 * void addThread(thread)
 *
 * add a thread to the ready que and marks it as ready
 */
void
Scheduler::addThread(Thread * thread)
{
  self->threadQueue.push(thread);
  thread->state = THR_READY;
}

/**
 * Removes thread from all ques
 * but user should be aware that the thread is 
 * not automaticly destroyed
 */
void
Scheduler::removeThread(Thread * thread)
{
  if (thread->state == THR_READY || thread->state == THR_RUNNING) {
    self->threadQueue.remove(thread);
    thread->state = THR_NOT_INSERTED;
  }
}

/**
 * void Start()
 *
 * Marks the system ready for 
 * multitasking execution
 */

void
Scheduler::start(bool bPreemptive)
{
  // notice the preemption state of the scheduler
  self->bPreemptive = bPreemptive;

  // If the scheduler was started as a preemptive
  // scheduler, then install the timer0 ISR.
  if (bPreemptive)
    setvect(TIMER0_INT, Scheduler::__isr__);
  
  self->state = SCH_ACTIVE;
  
  // force initial state
  
  /**
   * this is simply a hack that has to be done 
   * to make sure that the compiler doesn't use 
   * the same memory space again.
   * by that the session will fail.
   */
  self->runningThread = NULL;
  schedule();
}

/**
 * void Schedule()
 *
 * Calculate and sets the next running process
 * In this implementation its simple, since its 
 * implementing Round Robin as a whole circular
 * system
 *
 * it might be replaced with a priority based 
 * system later on
 */

void __saveregs
Scheduler::schedule()
{
  __enter_cs;
  
  
  // if the scheduler is not up running, 
  // just return
  if (self->state == SCH_INACTIVE) {
    __exit_cs;
    return;
  }
  // if we are called by the timer interrupt 
  // or other methods while we are in a locked
  // state - flag it for later execution and 
  // return
  if (self->iLockLevel > 0) {
    self->bReschedule = true;
    __exit_cs;
    return;
  }
  
  Thread
    *oldT = NULL;
  
  // put it back in list so it will figure correct
  self->threadQueue.calculateNextThread();
  
  
  if (!self->runningThread) {
    
    // initial state
    self->runningThread = (Thread *) self->threadQueue.top();
    if (!self->runningThread) {
      __exit_cs;
      return;
    }
    
    self->runningThread->state = THR_RUNNING;
  } else {
    // if the running thread is the same as the top thread
    // just return and do nothing more
    UINT rId = self->runningThread->getId();
    UINT fId = ((Thread *) (self->threadQueue.top()))->getId();
    if (rId == fId) {
      __exit_cs;
      return;
    }
    
    oldT = self->runningThread;
    
    if (self->runningThread->state == THR_RUNNING)
      self->runningThread->state = THR_READY;
    
    // change the running thread as it supposed to be.
    self->runningThread = (Thread *) self->threadQueue.top();
    
    self->runningThread->state = THR_RUNNING;
  }
  
  // if the scheduler is preemptive, then
  // restart the timer, that controls preemption
  if (self->bPreemptive)
    restartTimer();
  
  // perform the actual task shift
  contextSwitch(oldT, self->runningThread);
  
  __exit_cs;
  
}

/**
 * static Thread *getRunningThread()
 *
 * Bliver kaldt fra den k�rende process til at 
 * f� adgang til alle running process
 */
Thread *
Scheduler::getRunningThread()
{
  return (Thread *) self->runningThread;
}

/**
 * contextSwitch()
 *
 * Switch from one context to another
 */
void
Scheduler::contextSwitch(Thread * oldThread, Thread * newThread)
{
  // if oldthread exists and it returns a true value, 
  // it signals that a context switch has just happend
  // and therefore a switch is needed.
  if (oldThread) {
    if (oldThread->saveContext()) {
      if (self->bPreemptive)
	restartTimer();
      return;
    }
  }
  // we an now jump to the new thread...
  newThread->restoreContext();
}

/**
 * restartTimer()
 *
 * (Re)setup the timer0, to be prescaled by timer 2 to tick each 10 ms.  
 */
inline void
Scheduler::restartTimer()
{
  Am186ER::PCB->timer[0].maxCountA = SCH_TIMEOUT;
  Am186ER::PCB->timer[0].control =
    TIMER_ENABLE | TIMER_INTERRUPT | TIMER_PRESCALE;
}

/**
 * interrupt __isr__()
 *
 * Am186ER processoren kalder denne routine med SCHED_TIMEOUT
 * mellemrum. 
 *
 * Det er denne metode der g�r at en tr�d bliver preemptivt 
 * bortscheduleret.
 */
void interrupt
Scheduler::__isr__()
{
  Kernel::instance()->scheduler.lock();
  
  Kernel::instance()->scheduler.schedule();
  Am186ER::PCB->icu.eoi = 0x8;
  
  Kernel::instance()->scheduler.unlock();
}

/**
 * static __idle__()
 * our emergency idle func if nobody else is able to execute
 * but it actually get run quite a lot
 * when all devices are waiting for action - 
 * this is performed.
 */
void
Scheduler::__idle__()
{
 loop:goto loop;
}
