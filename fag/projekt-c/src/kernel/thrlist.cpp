/****************************************************************
 * ThreadList.cpp
 *
 * Threadlist er til at h�ndtere tr�dk�er for scheduleren.
 * Det er den alene der bestemmer 100% om det er RR, Priotet,
 * eller et mix af disse der arbejder p� k�en
 * Scheduleren underst�tter preemption,
 * Men hvis vi blot afleverer den samme tr�d til scheduleren,
 * vil den ikke preemte tr�den.
 ****************************************************************/
#include "kernel/thrlist.h"
#include "utility/listitem.h"
#include "config.h"

/**
 * Constructor
 *
 * Resets first and last
 */
ThreadList::ThreadList()
{
  m_first = NULL;
  m_last = NULL;
  m_size = 0;
}

/**
 * Destructor
 *
 * Do not delete any objects.
 */
ThreadList::~ThreadList()
{

}

/**
 * void push(thread)
 *
 * Pushes the thread on the end of the stack
 * This is a specialised version of push since it 
 * will not add a thread to the end of the list if 
 * there exists a thread with id 0. 
 * This is the default idle thread wich should only 
 * be run if there is nothing there can be done anyway.
 *
 * in a priority scheme, this will not be a problem
 * since the idle task will always have the lovest 
 * priority
 */
void
ThreadList::push(Item * thread)
{
  // create the containment object.
  ListItem *obj = new ListItem(thread);
  if (m_first == NULL) {
    // there is none in the list, ignore check for id.
    // just set it as first in list.
    m_first = obj;
    m_last = obj;
  } else {
    if (((Thread *) (m_last->containedObj))->getId() == 0) {
      /*
       * last is idle process so we need to squeesh the item in
       * between these two 
       */
      // start by updating own data
      if (m_first == m_last) {
	// There were only one item in the list before
	// put obj as the first in list
	m_first = obj;
      } else {
	// there was more than one item in the list before this
	// now split up the list and insert this before last
	obj->previous = m_last->previous;
	obj->previous->next = obj;
      }
      
      // this goes for all inserts.
      obj->next = m_last;
      obj->next->previous = obj;
    } else {
      // no last with id == 0
      // so therefore we add it at the end
      m_last->next = obj;
      obj->previous = m_last;
      m_last = obj;
    }
  }
  ++m_size;
}

/**
 * update(thread)
 * 
 * in this implementation the function does nothing
 * but should be used in the case a function
 * changes priority and therefor wants to shift 
 * up in system.
 */
void
ThreadList::update(Thread * thread)
{
  
}

/**
 * calculateNextThread()
 *
 * Find next thread to be executed - in RR its just 
 * a matter of doing a push/pop operation, but it demand
 * a destruction / construction roundtrip which is actually
 * not needed since its just a matter of shifting the object 
 * arround.
 **/

void
ThreadList::calculateNextThread()
{
  if (m_first->next && ((Thread *) (m_first->next->containedObj))->getId() != 0) {
    ListItem *it = m_first;
    m_first = (ListItem *)m_first->next;
    m_first->previous = NULL;
    it->previous = (ListItem *)m_last->previous;
    it->next = (ListItem *)m_last;
    m_last->previous = (ListItem *)it;
    it->previous->next = (ListItem *)it;
  }
}
