#ifndef __UNIXPTHREAD_H__
#define __UNIXPTHREAD_H__

#include <pthread.h>

void *threadFunc(void *);

class UnixPThread {
  friend void *threadFunc(void *);
public:
  UnixPThread();
  virtual ~UnixPThread();
  void Start ();
  void Stop ();
  void Continue ();
  void Suspend ();
  void Join();
protected:
  virtual void *main();
  void Yield();
private:
  pthread_t t_id;
  enum thState {
    closed = 0,
    stopped,
    running,
    waiting,
    joined
  } state;  
};

#endif // __THREAD_H__
