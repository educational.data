#include "ghsrvstb.h"
#include "protocol/net/packet.h"

GHServerStub::GHServerStub(g1_Datalink &dlk,IGHServer &serv) : dl(dlk), srv(serv) {

}

void GHServerStub::parse() {
  cout << "Waiting for data" << endl;
  PacketInfo pack = dl.recieve();
  struct msg_body &msg = *(msg_body *)pack.packet.getBuffer();
  cout << "Data recieved for id=" << (int)(msg.senderId);
  byte retVal = 0;
  switch(msg.funcId) {
  case 1:
    msg.retvar = srv.getTemp(msg.arg1);
    break;
  case 2:
    msg.retvar = srv.getWinState();
    break;
  case 3:
    msg.retvar = srv.getAutomata();
    break;
  case 4:
    srv.setTemp(msg.arg1,msg.arg2);
    break;
  case 5:
    srv.setWinState(msg.arg1);
    break;
  case 6:
    srv.enableAutomata(msg.arg1);
    break;
  default:
    break;
  }
  byte reciever = msg.senderId;
  msg.senderId = dl.getSenderId();
  
  Packet retpack((byte *)&msg,sizeof(struct msg_body));
  cout << "sending reply to " << (int)(reciever) << endl;
  dl.send(reciever,retpack);
}
