#include "config.h"
#include "test/testclass.h"
#include "ghsrvstb.h"
#include "ghserverproxy.h"
#include "driver/rs232/rs232_unix.h"
#include "testghserver.h"



		 
class testFuncs : public test::TestClass {
  Serial_unix *drv1;
  Serial_unix *drv2;
  g1_Datalink *dl1;
  g1_Datalink *dl2;
public:
  testFuncs() : 
    test::TestClass("funcs.translator")

  {
    
  }

  void set_up() {
    drv1 = new Serial_unix("/dev/ttyS0");
    drv2 = new Serial_unix("/dev/ttyS0");
    dl1 = new g1_Datalink(drv1,1);
    dl2 = new g1_Datalink(drv2,2);
  }
  
  void tear_down() {
    delete dl2;
    delete dl1;
    delete drv2;
    delete drv1;
  }

  void test1() {
    vector<g1_term_id> ids = dl1->getConnections();

    ensure("The size of the list is not 1",ids.size() == 1);
    ensure("The first id is not 2",ids[0] == 2);
    
    // test getTemp();
    /* GHServerStub stb(dl1,2);
    TestGHServer tghsrv;
    GHServerProxy prox(dl2,tghsrv);
    stb */

  }
};

namespace test {
  testFuncs testfuncs;
  //  TestGroup testfuncsection
}

test::TestClass &prepareFuncs() {
  using namespace test;
  //testfuncsection.add(testfuncs);
  return testfuncs;
}
