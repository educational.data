#ifndef __GHSVSTB_H__
#define __GHSVSTB_H__

#include "protocol/g1.net/datalink.h"
#include "ighsrv.h"

class GHServerStub {
public:
  GHServerStub(g1_Datalink &dlk,IGHServer &serv);
  void parse();
private:
  g1_Datalink &dl;
  IGHServer &srv;
};

#endif // __GHSRVSTB_H__
