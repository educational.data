#include "ghsrvstb.h"

class TestGHServer : public IGHServer  {
  int mTemp;
  int mMax;
  int mMin;
  int winState;
  int automata;
public:
  TestGHServer() {
    mMax     = 20;
    mMin     = 10;
    mTemp    = 15;
    winState = 0;
    automata = 0;
  }

  byte getTemp (int type) {
    switch(type) {
    case 0:
      return mTemp;
    case 1:
      return mMin;
    case 2:
      return mMax;
    default:
      return 0;
    }
  }

  byte getWinState () {
    return winState;
  }
  byte getAutomata () {
    return automata;
  }
  void setTemp (byte min, byte max) {
    mTemp = min/2 + max/2;
    mMin  = min;
    mMax  = max;
  }

  void setWinState(byte state) {
    winState = state;
  }

  void enableAutomata(byte state) {
    automata = state == 1 ? 1 : 0;
  }
};
