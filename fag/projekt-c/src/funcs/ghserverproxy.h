#include "ighsrv.h"
#include "protocol/g1.net/datalink.h"

class GHServerProxy : public IGHServer{
  g1_Datalink &dl;
  g1_term_id id;
public:
  g1_term_id getId() { return id; }
  GHServerProxy(g1_Datalink &dlk,char rid);
  GHServerProxy(const GHServerProxy &prox);
  GHServerProxy &operator=(const GHServerProxy &prox);

  byte getTemp(int type);
  byte getWinState();
  byte getAutomata();
  void setTemp(byte min, byte max);
  void setWinState(byte state);
  void enableAutomata(byte state);
};


