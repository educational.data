#include "ghserverproxy.h"
#include <iostream>

using namespace std;

GHServerProxy::GHServerProxy(g1_Datalink &dlk,char rid) : dl(dlk) {
  id = rid;
}

GHServerProxy::GHServerProxy(const GHServerProxy &prox) : dl(prox.dl), id(prox.id) {
}

GHServerProxy &GHServerProxy::operator=(const GHServerProxy &prox) {
  dl = prox.dl;
  id = prox.id;
  return *this;
}

byte GHServerProxy::getTemp(int type) {
  msg_body msg;
  msg.funcId = 1;
  msg.senderId = dl.getSenderId();
  msg.arg1 = type;

  Packet pack((byte *)&msg,sizeof(msg_body));
  dl.send(id,pack);

  // ---- mp processing data

  PacketInfo pi = dl.recieve();
  msg_body *retmsg = (msg_body*)pi.packet.getBuffer();
  return retmsg->retvar;
}
byte GHServerProxy::getWinState() {
  msg_body msg;
  msg.senderId = dl.getSenderId();
  msg.funcId = 2;

  Packet pack((byte *)&msg,sizeof(msg_body));
  dl.send(id,pack);

  // ---- mp processing data

  PacketInfo pi = dl.recieve();
  msg_body *retmsg = (msg_body*)pi.packet.getBuffer();
  return retmsg->retvar;

}
byte GHServerProxy::getAutomata() {
  msg_body msg;
  msg.funcId = 3;
  msg.senderId = dl.getSenderId();

  Packet pack((byte *)&msg,sizeof(msg_body));
  dl.send(id,pack);

  // ---- mp processing data

  PacketInfo pi = dl.recieve();
  msg_body *retmsg = (msg_body*)pi.packet.getBuffer();
  return retmsg->retvar;

}
void GHServerProxy::setTemp(byte min, byte max) {
  msg_body msg;
  msg.senderId = dl.getSenderId();
  msg.funcId = 4;
  msg.arg1 = min;
  msg.arg2 = max;

  Packet pack((byte *)&msg,sizeof(msg_body));
  dl.send(id,pack);

  // ---- mp processing data

  PacketInfo pi = dl.recieve();
}
void GHServerProxy::setWinState(byte state) {
  msg_body msg;
  msg.senderId = dl.getSenderId();
  msg.funcId = 5;
  msg.arg1 = state;

  Packet pack((byte *)&msg,sizeof(msg_body));
  dl.send(id,pack);

  // ---- mp processing data

  PacketInfo pi = dl.recieve();
}
void GHServerProxy::enableAutomata(byte state) {
  msg_body msg;
  msg.senderId = dl.getSenderId();
  msg.funcId = 6;
  msg.arg1 = state;

  Packet pack((byte *)&msg,sizeof(msg_body));
  dl.send(id,pack);

  // ---- mp processing data

  PacketInfo pi = dl.recieve();
}
