#ifndef __IGHSRV_H__
#define __IGHSRV_H__

#include "config.h"

struct msg_body {
  byte senderId;
  byte funcId;
  byte arg1;
  byte arg2;
  byte retvar;
};

class IGHServer {
public:
  virtual byte getTemp(int type) = 0;
  virtual byte getWinState() = 0;
  virtual byte getAutomata() = 0;
  virtual void setTemp(byte min, byte max) = 0;
  virtual void setWinState(byte state) = 0;
  virtual void enableAutomata(byte state) = 0;
};

#endif
