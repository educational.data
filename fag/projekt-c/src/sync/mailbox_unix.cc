/*********************************************************************
 * Mailbox_unix.cpp
 *
 * A method for sending Messages between processes.
 * The message is serialised through a gate.
 * 
 *********************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "mailbox_unix.h"

#include "utility/listitem.h"

Mailbox_unix::Mailbox_unix() {
  m_first = NULL;
  m_last = NULL;

  pthread_cond_init(&cond,0);
  pthread_mutex_init(&mut,0);
}

Mailbox_unix::~Mailbox_unix() {
  pthread_cond_destroy(&cond);
  pthread_mutex_destroy(&mut);
}

void
Mailbox_unix::send(Item * msg)
{
  pthread_mutex_lock(&mut);
  // we are about to change the shared contents of the mail box 
  // so we start by locking access to it.

  if (m_first == NULL) {
    m_last = new ListItem(msg);;
    m_first = m_last;
  } else {
    m_last->next = new ListItem(msg);;
    m_last = m_last->next;
  }
  // we have completed our update - release access to any other objects.
  pthread_cond_signal(&cond);
  pthread_mutex_unlock(&mut);
}

bool
Mailbox_unix::hasData() {
  pthread_mutex_lock(&mut);
  bool ret = m_first != NULL;
  pthread_mutex_unlock(&mut);
  return ret;
}

Item *
Mailbox_unix::receive()
{
  pthread_mutex_lock(&mut);
  while (m_first == NULL) {
    // we wait if there is nothing in the queue
    pthread_cond_wait(&cond, &mut);
  }

  ListItem *obj = (ListItem *)m_first;

  m_first = m_first->next;

  Item *msg = (Item *)obj->containedObj;

  if (m_first == NULL)
    m_last = NULL;
  delete obj;
  pthread_mutex_unlock(&mut);
  // done accessing
  return msg;
}
