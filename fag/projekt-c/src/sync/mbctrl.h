/**************************************************************
 * Mailboxcontroller.h
 *
 * this controller handles the instances of free mailboxes in
 * the system.
 *
 * to use a mailbox, one just calls up the mailbox with 
 * its correct number and the controller will create a mail
 * box if its not currently there
 *
 * A mailbox can be freed by calling freeBox 
 * 
 **************************************************************/


#ifndef __MAILBOXCONTROLLER_H__
#define __MAILBOXCONTROLLER_H__
#include "sync/mailbox.h"

class _MailBoxController_data;

class MailBoxController
{
public:
  MailBoxController ();
  ~MailBoxController ();

  Mailbox & box (unsigned id);
  void freeBox (unsigned id);
private:
  _MailBoxController_data * self;
};

#endif // __MAILBOXCONTROLLER_H__
