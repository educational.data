/**********************************
 **
 ** Mutex.cpp
 **
 ** Mutex er en variant af en semaphor
 ** 
 **********************************/

#include "sync/mutex.h"

#include "config.h"
#include "utility/queue.h"
#include "kernel.h"
#include "kernel/thread.h"


enum mutex_state { Available, Held };

class _Mutex_data {
public:
  mutex_state state;
  
  Queue waitingList;
};

Mutex::Mutex()
{
  // mark us ready to accept mutex jobs
  _pimpl = new _Mutex_data;
  _pimpl->state = Available;
}

Mutex::~Mutex()
{
  delete _pimpl;
}

void
Mutex::take(void)
{
  if (_pimpl->state == Available)
    _pimpl->state = Held;
  else {
    Thread *curThread = Thread::getCurrentThread();
    Kernel::instance()->scheduler.removeThread(curThread);
    _pimpl->waitingList.push(curThread);
    
    curThread->state = THR_SUSPENDED;
    
    Kernel::instance()->scheduler.schedule();
  }
}

void
Mutex::release(void)
{
  if (_pimpl->state == Held) {
    Thread *curWaitingThread = (Thread *) _pimpl->waitingList.pop();
    if (curWaitingThread) {
      Kernel::instance()->scheduler.addThread(curWaitingThread);
      Kernel::instance()->scheduler.schedule();
    } else
      _pimpl->state = Available;
  }
}
