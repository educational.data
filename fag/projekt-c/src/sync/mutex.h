/*******************************************************
   Mutex.h

   Mutex.h implements semaphore and monitor like data
   Guard is used with functions to limit access to one
   thread.
   pattern should be this:
   func() { ...
     static Mutex _funcMutex;
     Guard _funcGuard(_funcMutex);
     
     code.....
     .....code

     // normal return makes sure that guard is destroyed
     // and therefore _funcMutex->release() is called
   }
   $Id: mutex.h,v 1.2 2004-09-28 19:16:43 tobibobi Exp $

 *******************************************************/
#ifndef __MUTEX_H__
#define __MUTEX_H__

#include "config.h"

// use FUNC_GUARD to protect entry
// to a function
#define FUNC_GUARD \
        static Mutex _funcMutex; \
        Guard _funcGard(_funcMutex);

class _Mutex_data;

class Mutex
{
public:
  Mutex ();
  virtual ~ Mutex ();

  void take (void);
  void release (void);

private:
  _Mutex_data * _pimpl;
};

class Guard
{
public:
  Guard (Mutex * mut)
  {
    mut->take ();
  } 
  
  ~Guard ()
  {
    mut->release ();
    mut = NULL;
  }
private:
  Mutex * mut;
};

#endif // __MUTEX_H__
