#ifndef __MAILBOX_H__
#define __MAILBOX_H__

class Item;

class Mailbox
{
public:
  Mailbox ();
  ~Mailbox ();
  void send (Item * msg);
  Item *receive ();
  
protected:
  typedef struct priv;
  priv *p;
};

#endif // __MAILBOX_H__
