#ifndef __CONDITION_H__
#define __CONDITION_H__

class _condition_data;
class Condition
{
public:
  Condition ();
  virtual ~ Condition ();
  void wait (void);
  int signal (void);
  void broadcast (void);
private:
  // pimpl idiom
  _condition_data * self;
};

#endif // __CONDITION_H__
