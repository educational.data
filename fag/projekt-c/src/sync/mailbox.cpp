/*********************************************************************
 * Mailbox.cpp
 *
 * A method for sending Messages between processes.
 * The message is serialised through a gate.
 * 
 *********************************************************************/

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "mailbox.h"
#include "cond.h"
#include "mutex.h"
#include "utility/listitem.h"

typedef struct Mailbox::priv {
  Condition cond;
  Mutex mutex;
  ListItem * m_first;
  ListItem *m_last;
};

Mailbox::Mailbox()
{
  p = new priv;
  p->m_first = NULL;
  p->m_last = NULL;
}

Mailbox::~Mailbox() {
  delete p;
}

void
Mailbox::send(Item * msg)
{
  p->mutex.take();
  // we are about to change the shared contents of the mail box 
  // so we start by locking access to it.
  ListItem *obj = new ListItem(msg);
  if (p->m_first == NULL) {
    p->m_first = obj;
    p->m_last = obj;
  } else {
    p->m_last->next = obj;
    p->m_last = obj;
  }
  // we have completed our update - release access to any other objects.
  p->mutex.release();
  
  
  // signalling from the outside to prevent any dead locks
  p->cond.signal();
}

Item *
Mailbox::receive()
{
  if (p->m_first == NULL)
    // we wait if there is nothing in the queue
    p->cond.wait();
  
  // weare about to change data - so lock up access..
  p->mutex.take();
  
  ListItem *obj = p->m_first;
  Item *msg = (Item *)p->m_first->containedObj;
  p->m_first = (ListItem *)p->m_first->next;
  
  if (p->m_first == NULL)
    p->m_last = NULL;
  
  delete obj;
  
  // done accessing
  p->mutex.release();
  
  return msg;
}
