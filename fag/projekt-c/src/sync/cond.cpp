/****************************************************
 *
 * Condition.cpp
 *
 * Bruges af alle systemer som �nsker en 
 * "wait for signal" lignende metode.
 *
 ***************************************************/

#include "config.h"
#include "sync/cond.h"
#include "kernel/thread.h"
#include "kernel.h"
#include "utility/queue.h"

class _condition_data {
public:
  Queue m_waitingThreads;
};

Condition::Condition()
{
  self = new _condition_data;
}

Condition::~Condition()
{
  delete self;
}

// Supends the calling thread
void
Condition::wait()
{
  // remove the running thread from the ready queue
  
  Thread *thr = Thread::getCurrentThread();
  
  self->m_waitingThreads.push(thr);
  
  Kernel::instance()->scheduler.removeThread(thr);
  // remember to set state correctly to resemble state
  thr->state = THR_SUSPENDED;
  
  // yeild the thread so far.
  Kernel::instance()->scheduler.schedule();
}

/**
 * int signal(void)
 * 
 * V�kker en kaldende tr�d fra den ventende stak.
 *
 * returnerer antallet af ventende tr�de tilbage i 
 * stakkern
 */
int
Condition::signal()
{
  Thread *thr = (Thread *) self->m_waitingThreads.pop();

  // handle and yield to any waiting threads.
  if (thr) {
    // mark the ready to the ready Queue.
    thr->state = THR_READY;
    
    // move the top thread in to the ready q.
    
    Kernel::instance()->scheduler.addThread((Thread *) thr);
    
    
    // yield to the new thread. (or any other thread)
    Kernel::instance()->scheduler.schedule();
    
    // return the number of remaining queues.
    return self->m_waitingThreads.size();
  } else
    return 0;		// there is no more waiting threads
}

/**
 * void broadcast(void)
 *
 * L�gger alle ventende tr�de tilbage i ready k�en og 
 * yielder til den n�ste thr�d.
 */

void
Condition::broadcast()
{
  // only perform if there are any threads to wake
  if (self->m_waitingThreads.size() > 0) {
    
    // loop all waiting threads.
    while (self->m_waitingThreads.size() > 0) {
      Thread *thr = (Thread *) self->m_waitingThreads.pop();
      
      // mark the thread ready to run
      thr->state = THR_READY;
      Kernel::instance()->scheduler.addThread(thr);
    }
    // we have restored all threads, yield to the first in line..
    Kernel::instance()->scheduler.schedule();
  }
  return;
}
