/**************************************************************
 * Mailboxcontroller.h
 *
 * this controller handles the instances of free mailboxes in
 * the system.
 *
 * to use a mailbox, one just calls up the mailbox with 
 * its correct number and the controller will create a mail
 * box if its not currently there
 *
 * The system is implemented using a binary search tree
 *
 * A mailbox can be freed by calling freeBox 
 * 
 **************************************************************/

#include "sync/mbctrl.h"

class mailCont {
public:
  mailCont(unsigned _id, Mailbox * _box, mailCont * _parent) {
    left = 0;
    right = 0;
    box = _box;
    id = _id;
    parent = _parent;
  } unsigned      id;
  mailCont       *left;
  mailCont       *right;
  mailCont       *parent;
  
  Mailbox        *box;
};


#define SELF _MailBoxController_data
class SELF {
public:
  SELF() {
    cont = 0;
  } mailCont     *cont;
  // recursive funcs
  Mailbox        *find(unsigned id, mailCont * cont);
  void            insert(unsigned id, Mailbox * box);
  void            remove(unsigned id, mailCont * cont);
  mailCont       *TreeMinimum(unsigned id, mailCont * cont);
  mailCont       *TreeSuccessor(unsigned id, mailCont * cont);
};

Mailbox *
SELF::find(unsigned id, mailCont * cont)
{
  if (!cont)
    return 0;
  if (cont->id == id)
    return cont->box;
  if (id < cont->id)
    return find(id, cont->left);
  else
    return find(id, cont->right);
}

void
SELF::insert(unsigned id, Mailbox * box)
{
  mailCont *y = 0;
  mailCont *x = cont;
  while (x != 0) {
    y = x;
    if (x->id < id)
      x = x->left;
    else
      x = y->right;
  }
  mailCont *z = new mailCont(id, box, y);
  if (y == 0)		// the tree was empty
    cont = z;
  else {
    if (z->id < y->id)
      y->left = z;
    else
      y->right = z;
  }
}

mailCont *
SELF::TreeMinimum(unsigned id, mailCont * cont)
{
  while (cont->left != 0)
    cont = cont->left;
  return cont;
}

mailCont *
SELF::TreeSuccessor(unsigned id, mailCont * cont)
{
  mailCont *y = 0;
  mailCont *x = cont;
  
  if (x->right != 0)
    return TreeMinimum(id, x->right);
  
  y = x->parent;
  while (y != 0 && x == y->right) {
    x = y;
    y = y->parent;
  }
  return y;
}

void
SELF::remove(unsigned id, mailCont * cont)
{
  mailCont *y = 0;
  mailCont *x = 0;
  mailCont *z = cont;
  if (z->left == 0 || z->right == 0)
    y = z;
  else
    y = TreeSuccessor(id, z);
  if (y->left != 0)
    x = y->left;
  else
    x = y->right;
  if (x != 0)
    x->parent = y->parent;
  if (y->parent == 0)
    cont = x;
  else {
    if (y == (y->parent)->left)
      (y->parent)->left = x;
    else
      (y->parent)->right = x;
  }
  if (y != z)
    z->id = y->id;		// copy y's satellite data into z
  // return y;
}

MailBoxController::MailBoxController()
{
  self = new _MailBoxController_data;
}

MailBoxController::~MailBoxController()
{
  delete self;
}

Mailbox & MailBoxController::box(unsigned id)
{
  Mailbox *box;
  box = self->find(id, self->cont);
  if (!box) {
    box = new Mailbox;
    self->insert(id, box);
  }
  return *box;
}

void
MailBoxController::freeBox(unsigned id)
{
  Mailbox *box;
  box = self->find(id, self->cont);
  if (box)
    self->remove(id, self->cont);
}
