#ifndef __MAILBOX_UNIX_H__
#define __MAILBOX_UNIX_H__

class Item;
class ListItem;

#include <sys/types.h>
#include <pthread.h>

class Mailbox_unix
{
public:
  Mailbox_unix ();
  ~Mailbox_unix ();
  void send (Item * msg);
  Item *receive ();

  void operator<<(Item *itm) {
    send(itm);
  }
  void operator>> (Item *&itm) {
    itm = receive();
  }
  
  bool hasData();
  
protected:
  pthread_mutex_t mut;
  pthread_cond_t cond;

  volatile ListItem *m_first;
  volatile ListItem *m_last;
};


#endif // __MAILBOX_H__
