#include "driver/rs232/rs232_unix.h"
#include "protocol/g1.net/datalink.h"
#include "funcs/ghserverproxy.h"
#include <vector>
#include <iostream>

using namespace std;


int main() {
  Serial_unix ser("/dev/com4");
  g1_Datalink dl(ser,0x10);

  /*vector<g1_term_id> vid = dl.getConnections();

  for(vector<g1_term_id>::iterator it = vid.begin();it != vid.end();it++) {
    cout << "ID = " << *it << endl;
  }
  */
  GHServerProxy server(dl,1);

  for(;;) {
    cout << "Enter command: " << endl << "> " << flush;
    int c = getchar();
    getchar();
    switch(c) {
    case '?':
      cout << "Commands: \n\
t:Temparture\n\
i:Min temp\n\
a:Max temp\n\
A:Automata state\n\
w:WinState\n\
f:Flip winstate\n\
\n\
q:Quit value" << endl <<endl;
      break;
    case 't':
      cout << "Temperatur: " <<  (int)server.getTemp(0) << endl;
      break;
    case 'i':
      cout << "Min Temp: " << (int)server.getTemp(1) << endl;
      break;
    case 'a':
      cout << "Max Temp: " << (int)server.getTemp(2) << endl;
      break;
    case 'A':
      cout << "Automata state: " << (server.getAutomata() != 0 ? "true" : "false") << endl;
      break;
    case 'w':
      cout << "WinState: " << (server.getWinState() != 0 ? "open" : "closed") << endl;
      break;
    case 'f':
      cout << "Flipping winstate.." << endl;
      server.setWinState(server.getWinState() == 0 ? 1 : 0);
      break;
    case 'q':
      return 0;
    default:
      cout << "Unknown command \"" << (char)c << "\"." << endl;
      break;
    }
  }
}
