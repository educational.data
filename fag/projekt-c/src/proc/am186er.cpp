 /**********************************************************************
 **
 ** am186er.cpp
 **
 ** $Revision: 1.2 $
 **
 **********************************************************************/

#ifdef __GNUG__
#include <iostream>

using namespace std;
#endif

#include "am186er.h"
#include "bit.h"

#define RELREG 0xFFFE

#ifndef __GNUG__
volatile PeripheralControlBlock *
    Am186ER::PCB = (PeripheralControlBlock *) 0x400;
#else
volatile PeripheralControlBlock *
    Am186ER::PCB = new PeripheralControlBlock;
#endif

Am186ER::Am186ER()
{
  // Reloker PCB blok til hukommelse p� adresse 400h
  outport(RELREG, 0x1004);
}

unsigned short
inport(unsigned short portid)
{
#ifndef __GNUG__
  unsigned short  value;
  asm {
    mov dx, portid 
    in ax, dx 
    mov value, ax
  } 
  return value;
#else
  // thelog << "Read word from port " << portid << endl;
  return 0;
#endif
}

unsigned char
inportb(unsigned short portid)
{
#ifndef __GNUG__
  unsigned char   value;
  asm {
    mov dx,portid 
    in al,dx 
    mov value, al
  } 
  return value;
#else
  // thelog << "Read byte from port " << portid << endl;
  return 0;
#endif
}

void
outport(unsigned short portid, short value)
{
#ifndef __GNUG__
  asm {
    mov  dx, portid 
    mov  ax, value 
    out  dx, ax
  }
#else
  // thelog << "Dumped " << value << " on port " << portid << endl;
#endif
}

void
outportb(unsigned short portid, unsigned char value)
{
#ifndef __GNUG__
  asm {
    mov  dx, portid 
    mov  al, value 
    out  dx, al
  }
#else
  // thelog << "Dumped " << value << " on port " << portid << endl;
#endif
}

void
setvect(int interruptno, void (interrupt * fn) ())
{
#ifndef __GNUG__
  unsigned long  *vector = (unsigned long *) (interruptno << 2);
  *vector = (unsigned long) fn;
#endif
}
