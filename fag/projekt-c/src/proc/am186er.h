/**********************************************************************
 **
 ** am186er.h
 **
 ** processor definitions fil for AMD Am186ER mikroprocessor.
 **
 ** $Revision: 1.2 $
 **
 **********************************************************************/

#ifndef __AM186ER_H__
#define __AM186ER_H__

#include "config.h"

/*
 * #define __PCB_EXPOSE_SYNC_SERIAL_INTERFACE 
 */
#define __PCB_EXPOSE_INTERRUPT_CONTROL_UNIT
#define __PCB_EXPOSE_TIMER_CONTROL_UNIT
/*
 * #define __PCB_EXPOSE_PIO_UNIT #define __PCB_EXPOSE_ASYNC_SERIAL_PORT
 * #define __PCB_EXPOSE_EXT_CS #define __PCB_EXPOSE_INT_CS #define
 * __PCB_EXPOSE_DMA_CONTROLLER #define __PCB_EXPOSE_REFRESH_CONTROL_UNIT
 * #define __PCB_EXPOSE_WATCHDOG_TIMER #define __PCB_EXPOSE_PCB #define
 * __PCB_EXPOSE_PCB 
 */

struct SyncSerialInterface
{
  unsigned short stauts;
  unsigned short enable;
  unsigned short tx1;
  unsigned short tx0;
  unsigned short rx;
};

struct InterruptControlUnit
{
  unsigned short intVector;
  unsigned short eoi;
  unsigned short poll;
  unsigned short pollStatus;
  unsigned short intMask;
  unsigned short priorityMask;
  unsigned short inService;
  unsigned short intRequest;
  unsigned short intStatus;
  unsigned short timerControl;
  unsigned short dma0Control;
  unsigned short dma1Control;
  unsigned short int0Control;
  unsigned short int1Control;
  unsigned short int2Control;
  unsigned short int3Control;
  unsigned short int4Control;
  unsigned short watchdogControl;
  unsigned short serialControl;
};

struct TimerUnit
{
  unsigned short count;
  unsigned short maxCountA;
  unsigned short maxCountB;
  unsigned short control;
};

#define TIMER_ENABLE    0xC000
#define TIMER_DISABLE	0x4000
#define TIMER_INTERRUPT	0x2000
#define TIMER_MAXCOUNT  0x0020
#define TIMER_PRESCALE  0x0008
#define TIMER_PERIODIC	0x0001

#define TIMER0_INT 0x08
#define TIMER1_INT 0x12
#define TIMER2_INT 0x13

struct PIO
{
  unsigned short mode;
  unsigned short direction;
  unsigned short data;
};

struct AsyncSerialPort
{
  unsigned short control;
  unsigned short status;
  unsigned short tx;
  unsigned short rx;
  unsigned short baud;
};

struct DMA
{
  unsigned short srcLo;
  unsigned short srcHi;
  unsigned short destLo;
  unsigned short destHi;
  unsigned short count;
  unsigned short control;
  char reserved[4];
};

struct RefreshControlUnit
{
  unsigned short partition;
  unsigned short prescaler;
  unsigned short enable;
};

struct PeripheralControlBlock
{
  char _reserved0[16];

#ifdef __PCB_EXPOSE_SYNC_SERIAL_INTERFACE
  SyncSerialInterface ssi;
#else
  char _not_exposed0[sizeof (SyncSerialInterface)];
#endif				/* __PCB_EXPOSE_SYNC_SERIAL_INTERFACE */

  char _reserved1[6];

#ifdef __PCB_EXPOSE_INTERRUPT_CONTROL_UNIT
  InterruptControlUnit icu;
#else
  char _not_exposed1[sizeof (InterruptControlUnit)];
#endif				/* __PCB_EXPOSE_INTERRUPT_CONTROL_UNIT */

  char _reserved2[10];

#ifdef __PCB_EXPOSE_TIMER_CONTROL_UNIT
  TimerUnit timer[3];
#else
  char _not_expose2[3 * sizeof (TimerUnit)];
#endif				/* __PCB_EXPOSE_TIMER_CONTROL_UNIT */

  char _reserved3[8];

#ifdef __PCB_EXPOSE_PIO_UNIT
  PIO pio[2];
#else
  char not_exposed3[2 * sizeof (PIO)];
#endif				/* __PCB_EXPOSE_PIO_UNIT */

  char reserved4[4];

#ifdef __PCB_EXPOSE_ASYNC_SERIAL_PORT
  AsyncSerialPort serial;
#else
  char _not_exposed4[sizeof (AsyncSerialPort)];
#endif				/* __PCB_EXPOSE_ASYNC_SERIAL_PORT */

  char _reserved5[22];

#ifdef __PCB_EXPOSE_EXT_CS
  unsigned short ucs;
  unsigned short lcs;
  unsigned short pcs;
  unsigned short mcs;
  unsigned short aux;
#else
  char _not_exposed5[10];
#endif				/* __PCB_EXPOSE_EXT_CS */

  char _reserved7[2];

#ifdef __PCB_EXPOSE_INT_CS
  unsigned short ics;
#else
  char _not_exposed6[2];
#endif				/* __PCB_EXPOSE_INT_CS */

  char reserved8[18];

#ifdef __PCB_EXPOSE_DMA_CONTROLLER
  DMA dma[2];
#else
  char _not_exposed7[2 * sizeof (DMA)];
#endif

#ifdef __PCB_EXPOSE_REFRESH_CONTROL_UNIT
  RefreshControlUnit rcu;
#else
  char _not_exposed8[sizeof (RefreshControlUnit)];
#endif				/* __PCB_EXPOSE_REFRESH_CONTROL_UNIT */

#ifdef __PCB_EXPOSE_WATCHDOG_TIMER
  unsigned short watchdog;
#else
  char _not_exposed9[2];
#endif				/* __PCB_EXPOSE_WATCHDOG_TIMER */

  char _reserved9[8];

#ifdef __PCB_EXPOSE_PCB
  unsigned short power;
  char _reserved10[2];
  unsigned short stepId;
  unsigned short reset;
  char _reserved11[6];
  unsigned short reloc;
#else
  char _not_exposed[16];
#endif				/* __PCB_EXPOSE_PCB */
};

class Am186ER
{
public:
  static volatile PeripheralControlBlock *PCB;

public:
    Am186ER ();
};

/*
 * Globalt Am186ER CPU objekt... 
 */
static Am186ER ___am;

/*
 * x86 support funktioner 
 */
unsigned short inport (unsigned short portid);
unsigned char inportb (unsigned short portid);
void outport (unsigned short portid, short value);
void outportb (unsigned short portid, unsigned char value);
void setvect (int interruptno, void (interrupt * fn) ());

#ifndef __GNUG__
#define __enter_cs asm { pushf; cli }
#define __exit_cs asm { popf }
#else
#define __enter_cs
#define __exit_cs
#endif

#define MK_FP(seg,ofs)((void _seg *)(seg) + (void near *)(ofs))
#define FP_SEG(fp)((unsigned)(void _seg *)(void far *)(fp))
#define FP_OFF(fp)((unsigned)(fp))

#endif // __AM186ER_H__
