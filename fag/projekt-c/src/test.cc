/******************************************************
 *
 * main.cpp
 *
 ******************************************************/
#include "test/visu_impl.h"
#include "test/testgroup.h"
#include <iostream>
#include <exception>

using namespace std;
using namespace test;

// prepare funcs

TestClass &prepareNet();
TestClass &prepareG1Net();
TestClass &prepareUtility();
TestClass &prepareKernel();
TestClass &prepareFuncs();

int
main(int argn, char *argv[])
{
  StreamVisualizer visu(cout);
  TestGroup tstGrp("top");
  
  tstGrp.add(prepareG1Net());
  tstGrp.add(prepareNet());
  tstGrp.add(prepareUtility());
  tstGrp.add(prepareKernel()); 
  tstGrp.add(prepareFuncs());
  
  // set_terminate (__gnu_cxx::__verbose_terminate_handler);  
  if(argn > 1) {
    string arg = argv[1];
    if(arg == "list") {
      cout << "List of available" << endl;
      vector<string>::iterator it;
      vector<string> list = tstGrp.list();
      for(it=list.begin();it != list.end();it++) {
	cout << " " << *it << endl;
      }
      return 0;
    }
    else {
      cout << "Running (specific) Test:" << endl << endl;
      return tstGrp.run(visu,arg);
    }
  } else { 
    cout << "Running Tests:" << endl << endl;
    return tstGrp.run(visu,"");
  }
}







