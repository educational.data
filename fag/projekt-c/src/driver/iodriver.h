#ifndef __DEVICE_H__
#define __DEVICE_H__

#include "config.h"

class IODriver
{
protected:
  IODriver ()
  {
  }
public:
  virtual ~ IODriver ()
  {
  }

  // this is the "normal read write routines"
  virtual byte read (byte * buf, const byte len) = 0;
  virtual byte write (byte * buf, const byte len) = 0;

  // mainly used on devices that have a large buffer
  virtual void flush() { }
};

#endif
