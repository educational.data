#ifndef __DRIVERCONTROLLER_H__
#define __DRIVERCONTROLLER_H__

#include "config.h"

class IODriver;			// forward declaration

class IODriverController
{
  IODriverController ();
public:
  virtual ~ IODriverController ();

  static IODriverController & inst ()
  {
    static IODriverController driv;
    return driv;
  }
  IODriver *getIODriver (const byte drivId);
  void releaseIODriver (const IODriver *);

  const static byte DRIV_RS232PORT = 1;
  const static byte DRIV_NETPORT = 2;
  const static byte DRIV_APORT = 3;
  const static byte DRIV_BPORT = 4;
};

#endif
