#ifndef __SERIALDRIVER_UNIX_H__
#define __SERIALDRIVER_UNIX_H__

#include "driver/iodriver.h"

class __serial;

class Serial_unix : public IODriver {
public:
  Serial_unix (const char *);
  ~Serial_unix ();
  
  byte read (byte *buf, const byte size);
  byte write (byte *buf, const byte size);
  void flush();

  void push_data(const byte *buf, const byte size);
private:
  byte data[4000];
  int datasize;
  int datapos;
  __serial *ser;
};

#endif //__SERIALDRIVER_UNIX_H__
