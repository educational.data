#ifndef __SERIALDEVICE_H__
#define __SERIALDEVICE_H__

#include "driver/iodriver.h"

class SerialDevice : public IODriver
{
public:
  SerialDevice ();
  ~SerialDevice ();
  byte read (byte * buf, const byte size);
  byte write (byte * buf, const byte size);
private:
};

#endif
