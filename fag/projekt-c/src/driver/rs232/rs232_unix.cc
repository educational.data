#include "rs232_unix.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/select.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <exception>
#include <iostream>
#include <errno.h>
#include <unistd.h>
#include <time.h>
#include <map>
#include <vector>
#include <termios.h>

using namespace std;

#define BAUDRATE B57600
#define MODEMDEVICE "/dev/ttyS1"
#define _POSIX_SOURCE 1 /* POSIX compliant source */
#define FALSE 0
#define TRUE 1

void byte_zero(void *ptr,size_t size) {
  char *pt = (char *)ptr;
  for(size_t p=0; p < size; ++p)
    *(pt++) = 0;
}

class no_serial : public exception{
  string sWhat;
public:
  no_serial(const char *str) throw () {
    sWhat = str;
  }
  no_serial(int errcode) throw() {
    sWhat = strerror(errcode);
  }
  no_serial(const char *op, const int errcode) throw () {
    sWhat = "Operation: ";
    sWhat+= op;
    sWhat+= " failed with message: ";
    sWhat+= strerror(errcode);
  }

  ~no_serial() throw() {
  }
  const char* what() const throw() {
    return sWhat.c_str();
  }
};

class __serial {
  class markHolder {
    bool &m_val;
  public:
    markHolder(bool &val) : m_val(val) {
      if(m_val== false) {
	// cout << "Waiting for lock" << endl;
	while(m_val==false);
	// cout << "got it" << endl;
      }
      m_val=false;
      // cout << "Got lock" << endl;
    }
    ~markHolder() {
      // cout << "Release lock" << endl;
      m_val = true;
    }
  };
  __serial(const char *portname) {
    try {
      struct termios newtio;
      
      fd = open(portname, O_RDWR | O_NOCTTY | O_NDELAY);
      
      if(fd == -1)
	throw no_serial("open",errno);
      
      byte_zero(&newtio, sizeof(newtio));
      if(tcgetattr(fd,&oldtio)) {
	int errval = errno;
	close(fd);
	throw no_serial("tcgetattr", errval);
      }
      
      newtio.c_cflag = BAUDRATE | CS8 | CLOCAL | CREAD;
      newtio.c_iflag = IGNPAR;
      newtio.c_oflag = 0;
      
      /* set input mode (non-canonical, no echo,...) */
      newtio.c_lflag = 0;
      
      newtio.c_cc[VTIME]    = 0;   /* inter-character timer unused */
      newtio.c_cc[VMIN]     = 0;   /* blocking read until 5 chars received */
      
      if(tcflush(fd, TCIFLUSH) == -1) {
	int errval = errno;
	close(fd);
	throw no_serial("tcflush",errval);
      }
      
      if(tcsetattr(fd,TCSANOW,&newtio) == -1) {
	int errval = errno;
	close(fd);
	throw no_serial("tcsetattr",errval);
      }
      fcntl(fd,F_SETFL, FNDELAY);
    } catch (const exception &ex) {
      cerr << "Constructor err on port \"" 
	   << portname << "\" - exception thrown: " 
	   << ex.what() << endl;

      throw ex;
    }
    refCnt = 0;
    portId = portname;
    //cout << "Created" << endl;
  }
  typedef map<string,__serial *> serialMap;
  static map<string,__serial *> serials;

  vector<Serial_unix *> listeners;

  int refCnt;
  string portId;

  int fd;
  struct termios oldtio;
public:
  ~__serial() {
    tcsetattr(fd,TCSANOW,&oldtio);
    ::close(fd);
    //cout << "Destroyed" << endl;
  }

  void addListener(Serial_unix *listner) {
    //markHolder mark(canread);
    //cout << "Listener added" << endl;
    listeners.push_back(listner);
  }

  void addref() {
    //markHolder mark(canread);
    ++refCnt;
    // cout << "addref called on " << portId << " with refCnt (post) = " << refCnt << endl;
  }

  void release(Serial_unix *listener) {
    // destroy the listeners
    for(vector<Serial_unix *>::iterator it = listeners.begin();it < listeners.end();++it) {
      if(*it == listener)
	it = listeners.erase(it);
    }
    if(--refCnt == 0) {
      serialMap::iterator it = serials.find(portId);
      serials.erase(it);
      delete this;
    }
  }

  static __serial *instance(const string id) {
    // cout << "instance called" << endl;
    static bool canRead = true;
    markHolder mark(canRead);
    serialMap::iterator it = serials.find(id);
    if(it == serials.end()) {
      serials.insert(pair<string,__serial *>(id,new __serial(id.c_str())));
    }
    return serials[id];
  }

  void publish_data(Serial_unix *unx,byte *buf, const byte size) {
    for(vector<Serial_unix *>::iterator it = listeners.begin();it != listeners.end();it++) {
      if (unx != *it) // makes sure data does not go out to the sender...
	(*it)->push_data(buf,size);
    }
  }

  byte read(Serial_unix *unx,byte *buf, const byte size) {
    publish_data ( unx, buf, size );
    // cout << "." << flush;
    return ::read  ( fd, buf, size );
  }

  byte write(Serial_unix *unx, byte *buf, const byte size) {
    publish_data(unx,buf,size);
    return ::write(fd, buf, size);
  }
};

map<string, __serial *> __serial::serials;


Serial_unix::Serial_unix(const char *portname) {
  ser = __serial::instance(portname);
  ser->addListener(this);
  ser->addref();
  datapos = 0;
  datasize = 0;
}

Serial_unix::~Serial_unix() {
  ser->release(this);
}

void Serial_unix::push_data(const byte *buf, const byte size) {
  memcpy(&data[datasize],buf,size);
  datasize += size;
  // cout << "recieved push data with size = " << (int)size << endl;
}  

byte Serial_unix::read(byte *buf, const byte size) {
  int nsize;
  if(datapos == datasize) {
    datapos = 0;
    datasize = 0;
    nsize = ser->read(this,buf,size);
    
  } else {
    nsize = size;
    if(datapos+nsize > datasize)
      nsize=datasize-datapos;
    memcpy(buf,&data[datapos],nsize);
    datapos+=nsize;
  }
  // cout << "Returning " << nsize << " bytes. " << endl;
  return nsize;
}

void Serial_unix::flush() {
  // resets to empty buffers.
  datapos = 0;
  datasize = 0;
}

byte Serial_unix::write(byte *nbuf, const byte size) {
  //cout << "writing data on size = " << (int)size << endl;
  return ser->write(this,nbuf,size);
}
