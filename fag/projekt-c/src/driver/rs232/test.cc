/*************************************************
 **
 **
 ** $id:$
 **
 ************************************************/
#include "config.h"
#include "rs232_unix.h"
#include <exception>
#include <iostream>

using namespace std;

int main() {

  try {
    byte str[] = "A message from space";
    byte rcv[30];

    Serial_unix drv1("/dev/ttyS1");
    Serial_unix drv2("/dev/ttyS0");
    drv1.write(str,21);
    cout << "Send \"" << str << "\" through /dev/ttyS1" << endl;
    drv2.read(rcv,21);
    cout << "Recieved \"" << rcv << "\" on /dev/ttyS0" << endl;

  } catch (exception &ex) {
    cerr << "Exception thrown: " << ex.what() << endl;
  }
}
