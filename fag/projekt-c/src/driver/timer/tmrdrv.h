/********************************************************************
 **
 ** timerdriver.h
 **
 ********************************************************************/

#ifndef __TIMERDRIVER_H__
#define __TIMERDRIVER_H__

#include "config.h"
#include "driver/timer/tmrlst.h"

class Timer;

class TimerDriver
{
private:
public:
  TimerList timerQueue;

public:
  ~TimerDriver ();

  static TimerDriver *instance ();

  void addTimer (Timer *);
  void removeTimer (Timer *);

private:
    TimerDriver ();
  void tick ();
  static void interrupt far __isr__ ();
};

#endif // __TIMERDRIVER_H__
