/********************************************************************
 **
 ** timerlist.h
 **
 ********************************************************************/

#ifndef __TIMERLIST_H__
#define __TIMERLIST_H__

#include "utility/queue.h"

class TimerList : public Queue
{
public:
  TimerList ();
  ~TimerList ();
  
  // overidden queue functions
  void push (Item *);
};

#endif // __TIMERLIST_H__
