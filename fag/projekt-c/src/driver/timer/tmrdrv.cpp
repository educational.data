/********************************************************************
 **
 ** timerdriver.cpp
 **
 ********************************************************************/

#include "tmrdrv.h"
#include "timer.h"
#include "proc/am186er.h"
#include "kernel.h"
#include "kernel/thread.h"
#include "utility/item.h"


/**
 * Constructor
 *
 */
TimerDriver::TimerDriver()
{
  // install __isr__() as service 
  // for timer #2 interrupt
  setvect(TIMER2_INT, TimerDriver::__isr__);
  
  // setup max count value to 1ms
  Am186ER::PCB->timer[2].maxCountA = 10000;
  
  // start the timer with interrupt 
  // and make i restart by it self
  Am186ER::PCB->timer[2].control =
    TIMER_ENABLE | TIMER_INTERRUPT | TIMER_PERIODIC;
}

/**
 * Destructor
 *
 */
TimerDriver::~TimerDriver()
{
  // disable the timer
  Am186ER::PCB->timer[2].control = TIMER_DISABLE;
}

/**
 * TimerDriver instance()
 *
 */
TimerDriver *
TimerDriver::instance()
{
  static TimerDriver *inst = new TimerDriver();
  return inst;
}

/**
 * addTimer(Timer *)
 *
 */
inline void
TimerDriver::addTimer(Timer * timer)
{
  // add a timer to the queue of active timers
  timerQueue.push(timer);
}

/**
 * removeTimer(Timer *)
 *
 */
inline void
TimerDriver::removeTimer(Timer * timer)
{
  // remove a tiemr from the queue of active timers
  timerQueue.remove(timer);
}

/**
 * tick()
 *
 */
void
TimerDriver::tick()
{
  // get the top most timer
  Timer *
    timer = (Timer *) timerQueue.top();

  if (timer) {
    // decrement the timers count
    timer->count--;
    
    // mark all the expired timers done and
    // remove them from the active timer queue
    while (timer && timer->count <= 0) {
      timer->state = TS_DONE;
      
      // makes the suspended thread come back
      timer->condition.signal();
      
      timerQueue.pop();
      timer = (Timer *) timerQueue.top();
    }
  }
}

/**
 * __isr__()
 *
 */
void interrupt far
TimerDriver::__isr__()
{
  Kernel::instance()->scheduler.lock();
  
  // timer #2 tick
  TimerDriver::instance()->tick();
  
  // EOI
  Am186ER::PCB->icu.eoi = 0x8;
  
  Kernel::instance()->scheduler.unlock();
}
