/********************************************************************
 **
 ** timerlist.cpp
 **
 ********************************************************************/

#include "tmrlst.h"
#include "timer.h"

using namespace std;

/**
 * Constructor
 *
 */
TimerList::TimerList()
{
  m_first = NULL;
  m_last = NULL;
  m_size = 0;
}

/**
 * Destructor
 *
 */
TimerList::~TimerList()
{
}

/**
 * push(timer)
 *
 * Pushes the timer in somewhere in the stack.
 * The stack is an sorted stack with accending
 * count values. When a new timer is created,
 * it's given an absolute length, this stack
 * only contains counts relative to the top
 * timer.
 */
void
TimerList::push(Item * timer)
{
  ListItem *
    obj = new ListItem(timer);
  if (!m_first) {
    // the list is empty
    m_first = obj;
    m_last = obj;
  } else {
    
    ListItem       *
      itt = m_first;
    
    // locate a place where the timer should go, 
    // by comparing count with the other timers, 
    // if the count is to great, we move on deducting the timer count, 
    // 
    // with the just passed timer's count,
    // until we find a suitable spot for the new timer
    while (itt
	   && ((Timer *) timer)->count >=
	   ((Timer *) itt->containedObj)->count) {
      ((Timer *) timer)->count -=
	((Timer *) itt->containedObj)->count;
      itt = (ListItem *)itt->next;
    }
    
    if (itt == m_first) {
      // The itt item should go before the 
      // current first item in the queue
      obj->next = itt;
      obj->previous = itt->previous;
      itt->previous = obj;
      m_first = obj;
    } else {
      if (itt == NULL) {
	// The itt item should go after the 
	// current last item in the queue
	obj->next = NULL;
	obj->previous = m_last;
	m_last->next = obj;
	m_last = obj;
      } else {
	// The itt item should be placed somewhere
	// inside the queue, not first not last
	obj->next = itt;
	obj->previous = itt->previous;
	itt->previous->next = obj;
	itt->previous = obj;
      }
    }
    
    // adjust the next timer count (if any)
    // so the queue is in relative order
    // 
    if (obj->next) {
      ((Timer *) obj->next->containedObj)->count -=
	((Timer *) obj->containedObj)->count;
    }
  }
  ++m_size;
}
