/********************************************************************
 **
 ** timer.h
 **
 ********************************************************************/

#ifndef __TIMER_H__
#define __TIMER_H__

#include "kernel/thread.h"
#include "sync/cond.h"
#include "utility/item.h"

#define MS_PER_TICK 1

enum TimerState
{
  TS_IDLE,
  TS_ACTIVE,
  TS_DONE
};

enum TimerType {
  TT_ONESHOT,
  TT_PERIODIC
};

// Foward friend
class TimerDriver;
class TimerList;

class Timer : public Item
{
private:
  TimerState state;
  TimerType type;
  unsigned long length;
  unsigned long count;
  Condition condition;

  friend class TimerDriver;
  friend class TimerList;

public:
  Timer ();
  ~Timer ();
  
  int start ();
  int start (unsigned long ms, TimerType type = TT_ONESHOT);
  int waitfor ();
  void cancel ();

  bool isDone ()
  {
    return (state == TS_DONE);
  }
};


#endif // __TIMER_H__
