/********************************************************************
 **
 ** timer.cpp
 **
 ********************************************************************/

#include "timer.h"
#include "tmrdrv.h"
#include "proc/am186er.h"

/**
 * Constructor
 *
 */
Timer::Timer()
{
  length = 0;
  count = 0;
  state = TS_IDLE;
}

/**
 * Destructor
 *
 */
Timer::~Timer()
{
    cancel();
}

int
Timer::start()
{
  if (state != TS_DONE)
    return -1;
  
  // Reinitialize software timer
  count = length;
  state = TS_ACTIVE;
  
  // Add timer to queue of timers
  TimerDriver::instance()->addTimer(this);
  
  return 0;
}

int
Timer::start(unsigned long ms, TimerType type)
{
  if (state != TS_IDLE)
    return -1;
  
  // Initialize the software timer
  this->type = type;
  count = length = ms / MS_PER_TICK;
  state = TS_ACTIVE;
  
  // Add timer to queue of timers
  TimerDriver::instance()->addTimer(this);
  
  return 0;
}

int
Timer::waitfor()
{
  if (state != TS_ACTIVE)
    return -1;
  
  // wait for the soft timer to expire
  condition.wait();
  
  if (type == TT_ONESHOT) {
  } else {
    count = length;
    state = TS_ACTIVE;
    
    // Add timer to queue of timers
    TimerDriver::instance()->addTimer(this);
  }
  
  return 0;
}

/**
 *
 */
void
Timer::cancel()
{
  if (state == TS_ACTIVE)
    TimerDriver::instance()->removeTimer(this);
  
  state = TS_IDLE;
}
