/**
 * HashMap
 *
 * Dette er en lokal implementering til erstatning for
 * STL's map struktur.
 * grunden til at vi anvender denne type er at �nsker
 * ikke at anvende templates, da det ofte medf�rer alvorligt
 * meget hukommelses brug.
 *
 * $Revision: 1.7 $
 */

#include "hashmap.h"
#include <iostream>
using namespace std;

HashIterator::HashIterator() {
  _next = NULL;
  _currentItem = NULL;
  _map = NULL;
}

HashIterator::HashIterator(HashMap *hmap,HashItem *item) {
  if(item)
    _next = item->next;
  else
    _next = NULL;

  _currentItem = item;
  _map = hmap;
}

HashIterator::HashIterator(const HashIterator &hash) {
  _currentItem = hash._currentItem;
  if(_currentItem != NULL) 
    _next = _currentItem->next;
  else
    _next = NULL;
  _map = hash._map;
}

Item *HashIterator::remove() {
  /// We must catch the situation if we 
  /// accidently try to destroy an empty 
  /// container
  if(!_currentItem) {
    return NULL;
  }

  //cout << "Destroy " << (int)_currentItem->id << endl;

  HashItem *lastItem = _currentItem->prev;
  // map if there is a lastItem
  if(lastItem)
    lastItem->next = _next;

  // map if there is a next item
  if(_next)
    _next->prev = lastItem;

  if(_currentItem == _map->firstItem)
    _map->firstItem = _next;

  // the item is now actually out of the list
  /// first delete actual item
  Item *mItm = _currentItem->itm;

  // then the container....
  delete _currentItem;
  _currentItem = NULL;
  --(_map->m_size);  

  return mItm;
}

void HashIterator::destroy() {
  Item *itm = remove();
  if(itm)delete itm;
}
HashIterator &
HashIterator::operator ++ () {
  if(_next != NULL) {
    _currentItem = _next;
    _next = _currentItem->next;
  }
  else {
    _currentItem=NULL;
    _next = NULL;
  }
  return *this;
}

HashIterator
HashIterator::operator ++ (int) {
  HashIterator tmp = *this;
  if(_next) {
    _currentItem = _next;
    _next = _currentItem->next;
  }
  else {
    _currentItem=NULL;
    _next = NULL;
  }
  return tmp;
}

const Item *HashIterator::item() const {
  if(_currentItem != NULL)
    return _currentItem->itm;
  else
    return NULL;
}

const byte HashIterator::key() const {
  if(_currentItem != NULL)
    return _currentItem->id;
  else
    return 0;
}

bool 
HashIterator::operator==(const HashIterator &it) const {
  return _currentItem == it._currentItem && _next == it._next;
}

bool 
HashIterator::operator!=(const HashIterator &it) const {
  return _currentItem != it._currentItem || _next != it._next;
}

HashMap::HashMap() {
  set = false;
  m_size = 0;
  firstItem = NULL;
}

HashMap::~HashMap() {
  HashItem *it = firstItem;
  while(it != NULL) {
    HashItem *itp = it;
    it = it->next;
    delete itp;
  }
}


HashIterator
HashMap::begin() {
  return HashIterator(this,firstItem);
}

HashIterator
HashMap::end() {
  return HashIterator(this,NULL);
}

void 
HashMap::insert(int id, Item *item) {
  HashItem *itm = firstItem;
  if(itm) {
    while(itm->next != NULL && itm->id != id)
      itm = itm->next;

    if(itm->id == id)
      itm->itm = item;
    else // itm->next == NULL
      itm->next = new HashItem(itm,item,id);
  } else 
    firstItem = new HashItem(NULL,item,id);
  ++m_size;
}

const Item *HashMap::operator[](int id) {
  HashItem *itm = firstItem;

  if(itm == NULL)
    return NULL;

  while(itm->next != NULL&& itm->id != id)
    itm = itm->next;
  if(itm->id == id)
    return itm->itm;
  else {
    return NULL;
  }
}

void HashMap::remove(int id) {
  HashItem *it1, *it2;
  it1 = firstItem;
  it2 = NULL;
  if(it1 == NULL)
    return;

  //cout << "remove " << id << endl;

  while(it1->next != NULL && it1->id != id) {
    it2 = it1;
    it1 = it1->next;
  }
  if(it1->id == id) {
    if(it2 == NULL)
      firstItem = firstItem->next;
    else 
      it2->next = it1->next;
    
    --m_size;
    
    delete it1;
  }
}
    
