/**************************************************
 * Queue.cpp
 *
 * NOTE: Unsyncroniced queue
 *
 **************************************************/

#include "queue.h"

Queue::Queue()
{
  m_first = NULL;
  m_last = NULL;
  m_size = 0;
}

Item *
Queue::pop(void)
{
  if (m_first) {
    ListItem *it = m_first;
    m_first = (ListItem *)m_first->next;
    
    if (m_first)
      m_first->previous = NULL;
    else
      m_last = NULL;
    
    --m_size;
    Item *obj = (Item *)it->containedObj;
    
    delete it;

    return obj;
  } else
    return NULL;
}

Item *
Queue::top()
{
  return (Item *)m_first->containedObj;
}

void
Queue::push(Item * item)
{
  ListItem * it = new ListItem(item);
  if (m_last) {
    m_last->next = it;
    it->previous = m_last;
  } else {
    m_first = it;
  }
  
  m_last = it;
  ++m_size;
}

Iterator
Queue::head() const
{
  Iterator it = m_first;
  return it;
}

Iterator
Queue::tail() const
{
  Iterator it = m_last;
  return it;
}

unsigned char
Queue::size(void)
{
  return m_size;
}

/**
 * remove(item)
 *
 * Removes the item from the list
 * and makes sure that the size value is set accordingly
 */
void
Queue::remove(Item * item)
{
  // find the item in the list
  ListItem *obj;
  for (obj = m_first;
       obj != NULL && obj->containedObj != item; obj = (ListItem *)obj->next);
  
  // did we find it or did we reach the end
  if (obj == NULL)
    return;
  
  if (m_first == obj) {	// the thread is first in list
    m_first = (ListItem *)obj->next;
    if (m_first)
      m_first->previous = NULL;
    if (m_last == obj) {
      m_last = NULL;
    }
  } else {
    // colapse the object
    if (obj->next)
      obj->next->previous = (ListItem *)obj->previous;
    if (obj->previous)
      obj->previous->next = (ListItem *)obj->next;
    
    // remove last - since we know that obj != first
    // there must be someone before this one
    if (m_last == obj)
      m_last = (ListItem *)obj->previous;
  }
  delete obj;
  --m_size;
}


