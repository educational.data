/************************************************
 ** ListItem class
 ** A Listitem is a container item used internal
 ** in container classes like queue, but are 
 ** also a part of iterators
 **
 ** $Id: listitem.h,v 1.3 2004-11-10 09:30:46 tobibobi Exp $
 ***********************************************/

#ifndef __LISTITEM_H__
#define __LISTITEM_H__

#include "utility/item.h"
#include "config.h"

class ListItem
{
public:
  ListItem (Item * item)
  {
    next = NULL;
    previous = NULL;
    containedObj = item;
  }
  volatile ListItem *next;
  volatile ListItem *previous;
  volatile Item *containedObj;
};

#endif // __LISTITEM_H__
