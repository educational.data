/***********************************************************
 * ListItem.h
 *
 * For alle linkede lister, skal denne klasse nedarves.
 * Derved er den generelle funktionalitet beskrevet.
 *
 ***********************************************************/
#ifndef __ITEM_H__
#define __ITEM_H__

class Item
{
public:
  virtual ~ Item ()
  {
  }
};

#endif // __ITEM_H__
