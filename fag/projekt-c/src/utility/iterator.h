#ifndef __ITERATOR_H__
#define __ITERATOR_H__

#include "utility/item.h"
#include "utility/listitem.h"
#include "config.h"

class Queue;

class Iterator
{
  ListItem *itm;
  Iterator (const ListItem * li);

  friend class Queue;
public:
  Iterator (const Iterator & it);	// copy
  // constructor

  Item *operator* () const;
  Iterator & operator++ ();
  Iterator & operator-- ();
  bool operator== (const Iterator & it) const;
};

#endif // __ITERATOR_H__
