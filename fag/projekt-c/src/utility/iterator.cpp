#include "utility/iterator.h"
#include "utility/queue.h"

Iterator::Iterator(const Iterator & it)
{
  itm = it.itm;
}

Iterator::Iterator(const ListItem * li)
{
  itm = (ListItem *) li;
}

Iterator & 
Iterator::operator++()
{
  itm = (ListItem *)itm->next;
  return *this;
}

Iterator & 
Iterator::operator--()
{
  itm = (ListItem *)itm->previous;
  return *this;
}


Item *
Iterator::operator*() const {
  return (Item *)itm->containedObj;
} 

bool          
Iterator::operator==(const Iterator & it) const {
  return (itm->containedObj == it.itm->containedObj);
}
