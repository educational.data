#ifndef __HASHITEM_H__
#define __HASHITEM_H__

#include "item.h"

class HashItem {
public:
  HashItem(HashItem *last, Item *item, byte ID) {
    if(last) {
      last->next = this;
    } 
    prev = last;
    itm = item;
    id = ID;
    next = NULL;
  }
  Item *itm;
  HashItem *next;
  HashItem *prev;
  int id;
};

#endif
