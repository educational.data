/*********************************************************
 *
 * Queue.h
 *
 * Implementerer en k� struktur med pop og push funktioner.
 *
 * $Log: queue.h,v $
 * Revision 1.4  2004-09-28 19:16:44  tobibobi
 * Auto indented and added a control framework
 *
 * Revision 1.3  2004/09/26 19:04:18  tobibobi
 * Implemented check stream
 *
 * Revision 1.2  2004/06/03 09:18:37  tobibobi
 * Added an itterator for queue class
 *
 * Revision 1.1  2004/05/24 17:11:49  tobibobi
 * Flyttet alle filer over i src i stedet for implementation
 *
 * Revision 1.7  2004/05/23 21:05:40  thomase
 * Fjernet logbeskeder -- til aflevering
 *
 * Revision 1.6  2004/05/21 14:24:34  tobibobi
 *
 *********************************************************/
#ifndef __QUEUE_H__
#define __QUEUE_H__

#include "item.h"
#include "config.h"

#include "listitem.h"

#include "iterator.h"

class Queue
{
public:
  Queue ();

  // peeks at the first item in top of q.
  Item *top (void);
  
  // removes the top item from list and returns it
  Item *pop (void);
  
  // Iterator implementation
  Iterator head () const;
  Iterator tail () const;

  // pushes the item to bottom of the queue
  void push (Item * item);
  
  // directly removes an item from the list
  void remove (Item * item);
  
  // returns the size of the current queue 
  unsigned char size (void);
protected:
  ListItem * m_first;
  ListItem *m_last;
  unsigned char m_size;
};
#endif //
