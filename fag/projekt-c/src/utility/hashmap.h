#ifndef __MHASHMAP_H__
#define __MHASHMAP_H__
#include "config.h"
#include "hashitem.h"
#include "iterator.h"

class HashMap;

class HashIterator {
public:
  HashIterator();
  HashIterator(const HashIterator &hash);
  const byte key() const;
  const Item *item() const;
  HashIterator &operator++();
  HashIterator operator++(int);
  void next();
  void destroy();
  Item *remove();
  bool operator!=(const HashIterator &it) const;
  bool operator==(const HashIterator &it) const;
private:
  HashIterator(HashMap *hmap,HashItem *itm);
  HashItem *_currentItem;
  HashItem *_next;
  HashMap  *_map;
  friend class HashMap;
};

class HashMap {
  bool set;
  byte m_size;
public:
  HashMap();
  ~HashMap();
  const Item *operator[](int id);
  HashIterator begin();
  HashIterator end();
  void insert(int id, Item *itm);
  void remove(int id);
  byte size() const { return m_size; }
private:
  HashItem *firstItem;
  friend class HashIterator;
};

#endif



