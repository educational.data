#include "config.h"
#include "test/testgroup.h"
#include "utility/queue.h"
#include "utility/hashmap.h"

class testQueue : public test::TestClass {
public:
  testQueue() : test::TestClass("utility.queue") {
  }
  
  
  // test ques
  void test1() {
    Queue queue;
    Item dummy1;
    Item dummy2;
    
    queue.push (&dummy1);
    
    ensure ("Queue does not contain dummy1", queue.top () == &dummy1);
    
    queue.push (&dummy2);

    ensure ("dummy1 is not on top of queue after insertion of another item",
	    queue.top () == &dummy1);
    
    queue.push (queue.pop ());
    
    ensure ("dummy2 is not on top of queue after a rotation",
	    queue.pop () == &dummy2);
    
    ensure ("dummy1 has disappered after rotation and pop",
	    queue.pop () == &dummy1);
  } 
  
  void test2()
  {
    Queue queue;
    Item Dummy1;
    Item Dummy2;
    ensure ("Queue size is not 0 when queue is empty.", queue.size () == 0);
    
    queue.push (&Dummy1);
    ensure ("Queue size is not 1 when queue contains an object.",
	    queue.size () == 1);
    
    queue.push (&Dummy2);
    ensure ("Queue size is not 1 when queue contains two objects.",
	    queue.size () == 2);
    
    queue.pop ();
    ensure
      ("Queue size is not decreased to one when its been popped from two",
       queue.size () == 1);
    
    queue.pop ();
    ensure
      ("Queue size is not decreased to zero when its been popped from one",
       queue.size () == 0);
  }

  void test3()
  {
    Queue queue;
    queue.pop ();
    ensure ("Queue size is not 0 after popping an empty queue",
	    queue.size () == 0);
  }
};

class testHash : public test::TestClass {
public:
  testHash() : test::TestClass("utility.hashmap") {
  }
  void test1() {
    HashMap hash;
    ensure("hash size is not 0 after construction",hash.size() == 0);
  }

  void test2() {
    HashMap hash;
    Item itm;
    hash.insert(0,&itm);
    ensure("Operator [ ] not right",hash[0] == &itm);
    
    hash.remove(0);
    ensure("Operator [ ] does not return NULL after object removal", hash[0] == NULL);
  }
  
  void test3() {
    HashMap hash;
    Item itm1, itm12, itm13;
    hash.insert(1,&itm1);
    hash.insert(12,&itm12);
    hash.insert(13,&itm13);
    
    ensure("Size is not 3 after 3 inserts",hash.size() == 3);
    ensure("Item 2 does not resemble [2]",hash[1] == &itm1);
    ensure("Item 12 does not resemble [12]",hash[12] == &itm12);
    ensure("Item 13 does not resemble [13]",hash[13] == &itm13);

    hash.remove(12);
    ensure("After insert, Item 2 does not resemble [2]",hash[1] == &itm1);
    ensure("After insert, NULL does not resemble [12]",hash[12] == NULL);
    ensure("After insert, Item 13 does not resemble [13]",hash[13] == &itm13);  
  }
  void test4() {
    HashMap hash;
    Item itm1, itm2, itm3, itm4;
    hash.insert(1,&itm1);
    hash.insert(2,&itm2);
    hash.insert(22,&itm4);
    hash.insert(3,&itm3);

    hash.remove(22);
    int cnt = 0;
    bool found1, found2, found3;
    for(HashIterator it = hash.begin();it != hash.end();it++) {
      cnt++;
      if(it.key() == 1 && it.item() == &itm1) found1=true;
      if(it.key() == 2 && it.item() == &itm2) found2=true;
      if(it.key() == 3 && it.item() == &itm3 && it.item() != &itm2) found3=true;
    }
    ensure("The amount of loops should be 3", cnt == 3);
    ensure("item1 not found",found1);
    ensure("item2 not found",found2);
    ensure("item3 not found",found3); 
  } 
  void test5() {
    HashMap hash;
    Item it1, it2, it3;
    
    hash.insert(1,&it1);
    hash.insert(2,&it2);
    hash.insert(3,&it3);

    for(HashIterator it = hash.begin();it != hash.end();it++) {
      if(it.key() == 2)
	it.remove();
    }
    ensure("Item 1 is not in hash", hash[1] == &it1);
    ensure("Item 3 is not in hash", hash[3] == &it3);
    ensure("Item 2 is nor empty", hash[2] == NULL);

    for(HashIterator it = hash.begin();it != hash.end();it++) {
      if(it.key() == 1)
	it.remove();
    } 
    ensure("Item 1 is not empty", hash[1] == NULL);
    ensure("Item 3 is not in hash", hash[3] == &it3);

    for(HashIterator it = hash.begin();it != hash.end();it++) {
      if(it.key() == 3)
      it.remove();
    } 
    ensure("Item 3 is not null", hash[3] == NULL);
  }
};

namespace test {
  testQueue testqueue;
  testHash testhash;
  TestGroup utilstests;
}

test::TestClass &prepareUtility() {
  test::utilstests.add(test::testqueue);
  test::utilstests.add(test::testhash);
  return test::utilstests;
}
