/********************************************************************
 **
 ** config.h
 **
 ** $Revision: 1.5 $
 **
 ********************************************************************/

#ifndef __CONFIG_H__
#define __CONFIG_H__

#ifdef HAVE_CONFIG_H
# include "conf.h"
#endif

#ifdef __GNUG__

// typedefs to make it compile under gcc
#define far
#define _seg
#define __saveregs
#define interrupt

inline void __emit__ (...)
{
}

typedef struct __jmp_buf
{
  unsigned j_sp;
  unsigned j_ss;
  unsigned j_flag;
  unsigned j_cs;
  unsigned j_ip;
  unsigned j_bp;
  unsigned j_di;
  unsigned j_es;
  unsigned j_si;
  unsigned j_ds;
} jmp_buf[1];

inline unsigned
setjmp (jmp_buf __jmpb)
{
  return 0;
}
inline void longjmp (jmp_buf __jmpb, int __retval)
{;
}

#define _DS 0
#define _ES 0


#include <iostream>
using namespace std;

#define DUMP(tekst) cout << tekst << endl
#else
#define DUMP(tekst)

typedef unsigned int bool;
#define true 1
#define false 0

#endif

typedef unsigned char BYTE;
typedef unsigned char byte;
typedef unsigned int UINT;

#ifndef NULL
#define NULL (void *)0L
#endif

#endif // __CONFIG_H__
