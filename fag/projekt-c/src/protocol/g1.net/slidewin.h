#ifndef __SLIDE_WINDOW_HEADER__
#define __SLIDE_WINDOW_HEADER__

#include "utility/hashmap.h"
#include "utility/queue.h"


class Packet;

class g1_SlidingWindow:public Item
{
public:
  g1_SlidingWindow (const int size, const int init);
  g1_SlidingWindow (const int size = 8);
  ~g1_SlidingWindow ();

  // called when a client is idented...
  void reset ();

  /// marks if there is a need to send a package.
  /// calling it, makes the flag be reset so next time it will return false
  bool recvwin_unacked ();
  byte recvwin_unacked_id ();
  byte recvwin_expected_id();
  void recvwin_expected_id_inc();
  void recvwin_unacked_mark (byte id);		/// sets the flag accordingly
  void recvwin_unacked_remark ();		/// sets the flag accordingly
  
  bool senderwin_empty ();		/// marks that no packages is unacked;
  byte senderwin_nextsessionid ();	/// use to get id of package to send
  Packet &senderwin_firstunacked ();	/// use to get the first unacked 
  void senderwin_insert (Packet & pack);/// insert and increment sessionId
  void senderwin_acknowledge (byte id);	/// delete packages that is acked

  // every time a loop has been passed
  void tick();
  bool valid();

  // these vals is used to mark startup
  Queue newUnSentPackages;
  byte  step;
  byte  sendIdentCnt;
  bool  connected;
private:
  // time without acknowledge
  byte maxRTT;
  byte lastRTT;
  byte avgRTT;
  byte timeSinceLastSend;
  // algoritem to calculate mean rtt.
  void calcRTT();
  
  bool isFull ();

  int winsize;
  
  int offset;
  /* int heldPacks;
  Queue window; */
  HashMap window;

  int rcvUnacked;
  int rcvExpectedId;
  bool rcvUnackedMark;

  byte mId;
};

#endif
