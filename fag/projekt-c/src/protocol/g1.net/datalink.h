/**
 * @file datalink.h
 *
 * @brief The layer upon the physical layer.
 */

#ifndef __G1_DATALINK_H__
#define __G1_DATALINK_H__

#include "config.h"
#include "utility/queue.h"
#include "kernel/thread.h"
#include "protocol.h"
#include "slidewin.h"
#include "nettrans.h"
#include "utility/hashmap.h"
#include "protocol/net/datalink.h"
#ifdef __GNUG__
#include <vector>
#endif
#if HAVE_LIBPTHREAD == 1
#include "kernel/unixthread.h"
#include "sync/mailbox_unix.h" 
#include <pthread.h>
#else
#include "kernel/thread.h"
#include "sync/mailbox.h"
#endif



/**
 * @struct _dl_header
 *
 * this simple header is all we need to see if the packages are for us.
 * if not we just throw them out.
 */
typedef struct _dl_header
{
  g1_term_id recvId;
  g1_term_id sendId;
  byte control;
  byte seq;
  byte ack;
  byte step;
  byte crc;
  byte size;
} dl_header;

class IODriver;

/**
 * @class g1_Datalink
 *
 * The Datalink layer of the g1 protocol.
 * It will probally be changed later.
 */
class g1_Datalink: public Datalink, 
#if HAVE_LIBPTHREAD == 1
  public UnixPThread
#else
  public Thread
#endif
{
public:
  g1_Datalink (IODriver * driver, byte id);
  g1_Datalink (IODriver & driver, byte id);

  virtual ~ g1_Datalink ();

  IODriver *getDriver ()
  {
    return translator.getDriver ();
  }
  const g1_term_id getSenderId ()
  {
    return translator.senderId;
  }

#ifdef __GNUG__
  std::vector<g1_term_id> getConnections();
#endif

  void send (const byte recvId, Packet & pack);
  PacketInfo recieve ();

private:
  volatile bool stop; // marker flag to make sure that the thread is running;
  const static byte CTRL_ACK = 1 << 1;
  const static byte CTRL_HAS_DATA = 1 << 2;
  const static byte CTRL_IDENT = 1 << 0;
  const static byte CTRL_RESEND = 1 << 7;

  // thread func
  void *main ();

# ifdef __GNUG__
  volatile bool send_broadcast_ident;
  volatile bool holding;
  void dumpPacket(Packet & pack);
# endif

  static bool vrfyPacket (Packet & pack);

  static byte getCRC (Packet & pack);
  void send_packet(Packet & pack);

  void sendIdent(const g1_term_id identId = 255);
  void send_ident_ack(const g1_term_id identId);

  void readTranslator();
  void resendPackets(byte step);
  bool sendNewPackets(byte step);
  void sendRemainingAck(byte step);
  bool sendQueuedPackages(byte step);
  
  Packet *curPack;

#if HAVE_LIBPTHREAD == 1
  Mailbox_unix mbin;
  Mailbox_unix mbout;
#else
  Mailbox mbin;
  Mailbox mbout;
#endif

  Nettranslator translator;
  HashMap mapWindows;

  byte step;
  bool runAsThread;
};

#endif // __G1_DATALINK_H__
