#include "slidewin.h"
#include "protocol/net/packet.h"
#include <iostream>
using namespace std;


g1_SlidingWindow::g1_SlidingWindow (const int size, const int init)
{
  winsize        = size;
  offset         = init;
  rcvUnacked     = 0;
  rcvUnackedMark = false;
  rcvExpectedId  = 0;
  lastRTT        = 0;
  avgRTT         = 8;
  maxRTT         = 10;
}

g1_SlidingWindow::g1_SlidingWindow (const int size)
{
  winsize        = size;
  offset         = 0;
  rcvUnacked     = 0;
  rcvUnackedMark = false;
  rcvExpectedId  = 0;
  lastRTT        = 0;
  avgRTT         = 8;
  maxRTT         = 10;
}

g1_SlidingWindow::~g1_SlidingWindow ()
{
}

void g1_SlidingWindow::reset() {
  rcvUnacked     = 0;
  rcvUnackedMark = false;
  rcvExpectedId  = 0;
  lastRTT        = 0;
  offset         = 0;
  for(HashIterator it = window.begin();it!=window.end();it++)
    it.destroy();
}

bool g1_SlidingWindow::isFull ()
{
  //return heldPacks >= winsize;
  return window.size() >= winsize;
}

bool
g1_SlidingWindow::senderwin_empty() {
  // return heldPacks == 0;
  return window.size() == 0;
}

Packet & 
g1_SlidingWindow::senderwin_firstunacked() {
  // return *(Packet *) window.top();
  return *(Packet *) window.begin().item();
}

/**
 * acknowledge a package in sequence.
 *
 * @fixme: The calculation of lastRTT is somewhat unprecise
 */
void 
g1_SlidingWindow::senderwin_acknowledge (byte id)
{
  /// ved en acknowledge lidt l�ngere fremme i vinduet
  /// end den seneste, skal alle pakker indtil nu ack'es
  HashIterator it;
  for ( it = window.begin();
	it != window.end() && it.key() != id;
	++it)it.destroy();
  if(it.key() == id) // should always happen
    it.destroy();

  lastRTT = timeSinceLastSend > 8 ? timeSinceLastSend : 8;
  calcRTT();

}

void
g1_SlidingWindow::senderwin_insert (Packet & pack)
{
  window.insert(offset++,(Item *) new Packet(pack));
  if(offset == winsize)offset = 0;
}

byte
g1_SlidingWindow::senderwin_nextsessionid ()
{
  return offset;
}

bool 
g1_SlidingWindow::recvwin_unacked () {
  if(rcvUnackedMark) {
    rcvUnackedMark = false;
    return true;
  }
  return false;
}


byte
g1_SlidingWindow::recvwin_unacked_id () {
  return rcvUnacked;
}

void 
g1_SlidingWindow::recvwin_unacked_mark(byte id) {
  rcvUnackedMark = true;
  rcvUnacked = id;  
}

void
g1_SlidingWindow::recvwin_unacked_remark() {
  rcvUnackedMark = true;
}

void 
g1_SlidingWindow::recvwin_expected_id_inc() {
  rcvExpectedId++;
 
  if(rcvExpectedId >= winsize)
    rcvExpectedId=0;
}


void
g1_SlidingWindow::tick() {
  if(window.size() == 0)
    /// if we dont have any packets in store,
    /// we must keep reseting to 0.
    timeSinceLastSend = 0;
  else
    ++ timeSinceLastSend;  
}

bool
g1_SlidingWindow::valid() {
  if(window.size()==0)
    return true;
  else {
    return (timeSinceLastSend < maxRTT);
  }
}

void 
g1_SlidingWindow::calcRTT() {
  if(lastRTT == 0) {
    avgRTT = 20;
    maxRTT = 255;
  } else {
    avgRTT = ((3 * avgRTT) + lastRTT)/4;
    maxRTT = avgRTT * 4;
  }
}

byte
g1_SlidingWindow::recvwin_expected_id() {
  return rcvExpectedId;
}
