#include "driver/iodriver.h"

class simpleDriver : public IODriver
{
  class markHolder {
    bool &m_val;
  public:
    markHolder(bool &val) : m_val(val) {
      while(m_val==false);
      m_val=false;
    }
    ~markHolder() {
      m_val = true;
    }
  };
  struct itm {
    itm(itm *&list, simpleDriver& drv) : driv(drv) {
      next=list;
      list = this;
    }
    ~itm () {
      if (next) delete next;
    }
    itm *next;
    simpleDriver &driv;
  };
  class buffer {
    bool canread;
    byte *data;
    itm *list;
    buffer() {
      canread = true;
      list = NULL;
      data = NULL;
    }
  public:
    ~buffer() {
      if (list) delete list;
      if (data) free(data);
    }
    void addListener(simpleDriver &driv) {
      markHolder mark(canread);
      new itm(list,driv);
    }

    void clean() {
      if (list) {
	delete list;
	list = NULL;
      }
    }

    void sendData(byte *data, int size) {
      markHolder mark(canread);
      itm *it = list;
      while(it) {
	memcpy(it -> driv.pBuffer,data,size);
	it -> driv.bufSize = size;
	it -> driv.bufPos = 0;
	it = it -> next;
      }
    }

    static buffer &instance() {
      static buffer buf;
      return buf;
    }
  };
  friend class buffer;
  friend class markholder;
  friend class itm;

  bool canread;
public:
  simpleDriver () 
  {
    canread = true;
    buffer::instance().addListener ( *this );
    bufPos = 0;
    bufSize = 0;
  }

  ~simpleDriver ()
  {
    buffer::instance().clean();
  }

  byte read (byte * buf, byte size)
  {
    markHolder mark(canread);
    byte pos = 0;
    if (bufSize == 0)
      return 0;
    
    if (size > bufSize - bufPos)
      size = bufSize - bufPos;
    
    byte nPos = size + bufPos;
    
    for (; bufPos < nPos; bufPos++)
      buf[pos++] = pBuffer[bufPos];
    
    if (nPos == bufSize) {
      bufSize = 0;
      bufPos = 0;
    }
    return size;
  }
  
  byte write (byte * buf, const byte size)
  {
    markHolder mark(canread);
    buffer::instance().sendData(buf, size);
    return size;
  }
  byte *getBuffer ()
  {
    return pBuffer;
  }
  byte getSize ()
  {
    return bufSize;
  }
  void reset ()
  {
    bufPos = 0;
    bufSize = 0;
  }
private:
  byte pBuffer[255];
  byte bufPos;
  byte bufSize;
  
};
