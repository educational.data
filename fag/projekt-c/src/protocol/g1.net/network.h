#ifndef __G1NET_NETWORK_H__
#define __G1NET_NETWORK_H__


#include "utility/queue.h"
#include "kernel/thread.h"
#include "protocol.h"
#include "datalink.h"

class IODriver;

// the network listener layer does not have any 
// read funtion, it has only got a write function that
// is thread transparent
class g1_Network
{
  g1_Network (IODriver * device, byte id);
public:
  static g1_Network &instance();
  ~g1_Network();

  void send (const g1_term_id reciever, const Packet & pack);
  
  
  g1_term_id getSenderId ()
  {
    return mDL.getSenderId ();
  }
private:
  static void doWork();
  
  g1_Datalink mDL;
  IODriver *mDriver;
};

#endif // __G1NET_NETWORK_H__
