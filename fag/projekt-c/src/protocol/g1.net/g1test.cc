#include "config.h"
#include "datalink.h"
#include "driver/rs232/rs232_unix.h"
#include "protocol/net/packet.h"
#include <iostream>
#include <fstream>
#include <string>

#include "protocol/g1.net/datalink.h"
#include "simpleDriver.h"
#include "protocol/g1.net/protocol.h"
#include "test/testclass.h"

using namespace std;

class testg1dl : public test::TestClass {
  /*Serial_unix drv1;
    Serial_unix drv2; */
  IODriver *drv1;
  IODriver *drv2;
  IODriver *drv3;
public:
  testg1dl() : test::TestClass("protocol.g1net.datalink")/*  ,
	       drv1("/dev/ttyS0"),
	       drv2("/dev/ttyS1") */
  {
    // drv1 = NULL;
    // drv2 = NULL;
    // drv1.faulty = true;
    // drv2.faulty = true;
  }
  
  void set_up() {
    drv1 = new simpleDriver;
    drv2 = new simpleDriver;
    drv3 = new simpleDriver;
    // drv1 = new Serial_unix("/dev/ttyS0");
    // drv2 = new Serial_unix("/dev/ttyS1");
  }

  void tear_down() {
    delete drv1;
    delete drv2;
    delete drv3;
  }

   
  void test1 ()
  {
    drv1->write(0L,0); // forces a clean data block
    // test the constructor
    
    g1_Datalink dl (drv1,1); 
    
    ensure ("Constructor is not setting driver to any non NULL value",dl.getDriver() != NULL);
    ensure ("Constructor is not set correctly", dl.getDriver () == drv1); 
  } 
  
  void test2()
  {
    drv1->write(0L,0); // forces a clean data block

    // test recieve method
    try {
      g1_Datalink dl1(drv1,1);
      g1_Datalink dl2(drv2,2);
      byte data[3] = { 12, SIGNAL_MARKER, 13 };
      Packet pack(data,3);
      dl1.send (2, pack);

      PacketInfo PI = dl2.recieve();

      ensure ("Protocol dosn't fit the first byte", PI.packet[0] == 12);
      ensure ("Protocol dosn't fit the second byte", PI.packet[1] == SIGNAL_MARKER);
      ensure ("Protocol dosn't fit the third byte", PI.packet[2] == 13);       
    } 
    catch (const exception &ex) {
      fail(ex.what());
    } 
  }
  
  
  void test3() 
  {
    drv1->write(0L,0); // forces a clean data block

    // test to see if the packet is dropped if its 
    // not the correct reciever
    
    g1_Datalink dl (drv1,2);
    g1_Datalink dl2 (drv2,10);
    
    Packet pack1, pack2;
    
    pack1 << 120;
    pack2 << 210;
    
    dl.send (12, pack1);	// wrong recieverid
    dl.send (10, pack2);	// right recieverid
    PacketInfo pi = dl2.recieve ();
    ensure ("We have not recieved the right data", pi.packet == pack2);
  }
  
  void test4() {
    // examine to see if we can handle multiple recievers
    drv1->write(0L,0); // forces a clean data block

    g1_Datalink dl1 (drv1,1);
    g1_Datalink dl2 (drv2,2);
    g1_Datalink dl3 (drv3,3);
    Packet pack2, pack3;

    pack2 << 2;
    pack3 << 3;

    dl1.send(2,pack2);
    dl1.send(3,pack3);

    ensure("Packet 2 send is not 2 recieved", dl2.recieve().packet == pack2);
    ensure("Packet 3 send is not 3 recieved", dl3.recieve().packet == pack3);
  }
};

namespace test {
  testg1dl TestG1DL;
}

test::TestClass &prepareG1Net() {
  return test::TestG1DL;
}
