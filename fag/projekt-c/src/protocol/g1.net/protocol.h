#ifndef __G1_PROTOCOLS_H__
#define __G1_PROTOCOLS_H__

// address types used on the g1.net
typedef byte g1_term_id;

// this is the byte stopping signal
#define SIGNAL_MARKER       170	// 10101010
#define SIGNAL_PACKET_START 219	// 11011011
#define SIGNAL_PACKET_END   182	// 10110110

#endif
