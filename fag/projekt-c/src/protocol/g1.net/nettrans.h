#ifndef __NET_TRANSLATER_H__
#define __NET_TRANSLATER_H__

#include "utility/queue.h"
#include "protocol.h"
#include "protocol/net/packet.h"

class IODriver;

class Nettranslator
{
public:
  Nettranslator(IODriver *driver);
  Queue incomming;		// queue of incoming packets.
  g1_term_id senderId;
  
  void wait_for_clear_to_send ();

  IODriver *getDriver ()
  {
    return pDriver;
  }
  void send_packet (Packet & pack);
private:
  Packet add_byte_stopping (Packet & oldBuf);
  void read_data_from_stream ();

  IODriver *pDriver;

  bool clear_to_send;

  Packet *curPack;
  bool rcv_signal_marker;
};

#endif
