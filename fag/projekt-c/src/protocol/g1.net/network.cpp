#include "network.h"
#include "protocol/net/packet.h"
#include "driver/iodriver.h"
#ifdef __GNUG__
#include "driver/rs232/rs232_unix.h"
#endif

g1_Network::g1_Network(IODriver *driver):mDL(driver)
{
  mDriver = driver;
#ifdef __GNUG__
  
#endif
}

g1_Network::~g1_Network() {
  delete mDriver;
}

void
g1_Network::write(g1_term_id reciever, const Packet & pack)
{
  mDL.write(reciever, pack);
}

g1_Network &g1_Network::instance() {
#ifdef __GNUG__
  static g1_Network inst(new Serial_unix("/dev/ttyS0"));
#endif
  return inst;
}

void g1_Network::doWork() {
  // nothing so far...
}
