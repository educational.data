#include "nettrans.h"
#include "driver/iodriver.h"
#ifdef __GNUG__
#include <iostream>
# define D(s) std::cout << s << " " << std::flush;
#else
# define D(s)
#endif

/**
 * read_data_from_stream();
 *
 * This is one of the magick functions
 * it read data in from the serial device, removes and
 * any byte stuffing, and handles it of to vrfyPacket
 * to check if data is valid.
 *
 * @todo read_data_from_strem is to complex and should be rewritten
 */

Nettranslator::Nettranslator(IODriver *driver) {
  pDriver = driver;
  curPack = 0;
  rcv_signal_marker = false;
  clear_to_send=true;
}
void
Nettranslator::read_data_from_stream ()
{
  byte data;
  int size = pDriver->read (&data, 1);
  if (size > 0) {
    if (!curPack) {
      // we are not reading any data yet
      
      // test to see if we are starting to recieve a signal marker
      if (rcv_signal_marker == false && data == SIGNAL_MARKER) {
	rcv_signal_marker = true;
	
	// we are now assumeably in progress of a package
	clear_to_send = false;
      }
      else if (rcv_signal_marker == true && data == SIGNAL_PACKET_START) {
	rcv_signal_marker = false;
	clear_to_send = false;
	curPack = new Packet;
      }
      else {
	// eeeh somethings rotten, lets just skip 
	// data and destroy sending data - since it must be noise
	rcv_signal_marker = false;
	clear_to_send = true;
      }
    }
    else {
      // we have an active packet 
      if (rcv_signal_marker == false && data != SIGNAL_MARKER) {
	*curPack << data;
      }
      else if (rcv_signal_marker == false && data == SIGNAL_MARKER) {
	// we have recieved a mark - we hold input to see if its 
	// a packet marker.

	rcv_signal_marker = true;
      }
      else if (rcv_signal_marker == true && data == SIGNAL_MARKER) {
	// we have an escape mark
	*curPack << SIGNAL_MARKER;
	rcv_signal_marker = false;
      }
      else if (rcv_signal_marker == true && data != SIGNAL_MARKER) {
	rcv_signal_marker = false;

	if (data != SIGNAL_PACKET_END) {
	  // data must be corrupt since the
	  // data MUST be a marker end!!!

	  delete curPack;
	  curPack = 0;
	  clear_to_send = true;
	}
	else {
	  // EOF package 

	  // we push the data on the stack to prevent using 
	  // to much time on package validation now.

      
	  incomming.push (curPack);

	  // we say that there must be no more than five unhandled
	  // packages in line - therefore we delete if there is to many
	  if (incomming.size () > 5)
	    delete (Packet *) incomming.pop ();

	  curPack = 0;
	  clear_to_send = true;

	  /**
	   * we sleep enough random time for a new packet to 
	   * enter the line.
	   * if the sender needs one send
	   * making it random will make the chance that 
	   * someone is not starting to send at the exact same time
	   * smaller.
	   *
	   * @todo make delay function fit with both gnu and MP
	   */
#         ifdef __GNUG__
	  // sleep (1 + (random () % 2));
#         endif
	  read_data_from_stream ();	// perform a last check
	}
      }
    }
  }
}


void
Nettranslator::send_packet (Packet & pack)
{
  /// perform byte flipping.
  Packet net_pack = add_byte_stopping (pack);

  /// make sure that there is nothing preventing us 
  /// from sending at this very moment.

  wait_for_clear_to_send ();


  /// now dPos contains the size of the translated package
  /// so send it over the wire.
  pDriver->write (net_pack.getBuffer (), net_pack.size ());
}



/**
 * wait_for_clear_to_send();
 *
 * We examine if there is anything in the buffer that needs being read
 * and reads it in before sending a new package.
 */
void
Nettranslator::wait_for_clear_to_send ()
{
  //cout << "<" << flush;
  // call to make sure that stream is clear before sending
  read_data_from_stream ();

  // if clear, this will not call and we will return
  while (!clear_to_send)
    read_data_from_stream ();

  //cout << ">" << flush;

  return;
}





/**
 * add_byte_stopping(old_pack) := new_packet;
 *
 * Add byte stopping and escaping sequences to the buffer.
 * to use it you must provide it with a new buffer
 * and you will get the new size in the size value
 */
Packet Nettranslator::add_byte_stopping (Packet & oldPack)
{
  Packet newPack;
  // set packetstart
  newPack << SIGNAL_MARKER;
  newPack << SIGNAL_PACKET_START;

  byte
    size = oldPack.size ();

  // convert data
  for (byte sPos = 0; sPos < size; sPos++) {
    /**
     * If we find a value equal to the signal marker, 
     * we enter an extra marker as escape marker
     * just in front of it.
     */
    if (oldPack[sPos] == SIGNAL_MARKER)
      newPack << SIGNAL_MARKER;

    // append data to buffer
    newPack << oldPack[sPos];
  }
  // set bytestop
  newPack << SIGNAL_MARKER;
  newPack << SIGNAL_PACKET_END;

  return newPack;
}
