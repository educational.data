/**
 * Implementation of the g1_Datalink class.
 *
 * $Id: datalink.cpp,v 1.21 2005-01-17 10:32:31 thomase Exp $
 */

#define DUMP_PACKAGE 1
#define _ID_ 100
#include "datalink.h"
#include "driver/iodriver.h"
#include "protocol/net/packet.h"

const byte ident_resend_cnts = 20;
const byte time_tick_step    = 10;

#ifdef __GNUG__
#include <unistd.h>
#include <iostream>
using namespace std;
#endif

g1_Datalink::g1_Datalink (IODriver * driver, byte id) : translator (driver)
{
  // reset buffers if previously used...
  driver->flush();

  curPack              = NULL;
  stop                 = false;
  step                 = 0;
  translator.senderId  = id;
#ifdef __GNUG__
  // actually only needed on server
  send_broadcast_ident = false;
  holding = false;
#endif // __GNUG__
  Start ();
}

g1_Datalink::g1_Datalink (IODriver & driver, byte id) : translator (&driver)
{
  // reset buffers if previously used...
  driver.flush();

  curPack              = NULL;
  stop                 = false;
  step                 = 0;
  translator.senderId  = id;
#ifdef __GNUG__
  // actually only needed on server
  send_broadcast_ident = false;
  holding = false;
#endif
  Start ();
}

#ifdef __GNUG__
/**
 * getConnections()
 *
 * Special server version to query all clients
 * will not be present on any gh clients.
 */
vector<g1_term_id> g1_Datalink::getConnections() {
  vector<g1_term_id> ids;
  holding = true;
  Yield();
  send_broadcast_ident = true;
  holding = false;
  Yield();

  /* for(int cnt=0;cnt<50;cnt++)
     Yield(); */
  sleep(1); // 1 second to reply should be enough

  // now data should be available

  holding = true;
  Yield();

  // its now safe to touch data

  for(HashIterator it = mapWindows.begin();it != mapWindows.end();it++)
    ids.push_back((g1_term_id)it.key());
  // we are now done accessing data
  holding = false;
  Yield();
  return ids;
}

#endif // __GNUG__


g1_Datalink::~g1_Datalink ()
{
  stop = true;
  holding = false;
  Join();
#ifdef __GNUG__

#endif // __GNUG__
}



/**
 * @breif Examines a packet to see if the data is valid.
 *
 * A packet is first examined to see if it contains any errors,
 * and after that it's examined to see if it contains any 
 * controll information
 */
bool g1_Datalink::vrfyPacket (Packet & pack)
{
  dl_header & hdr = *((dl_header *) pack.getBuffer ());

  if (hdr.size != pack.size ()) {
    return false;
  }
  // do CRC calculation
  long
    crc = hdr.crc;
  hdr.crc = 0;
  if (getCRC (pack) != crc) {
    return false;
  }
  hdr.crc = crc;

  return true;
}



/**
 * getCRC(packet)
 * 
 * @brief calculates CRC over a single package
 *
 * @param Packet the packet to be CRC calculated.
 *
 * The value is supposed to be 8 bits precise.
 */
byte
g1_Datalink::getCRC (Packet & pack)
{
  int checkvar = 0;

  byte *buf = pack.getBuffer ();

  for (int pos = 0; pos < pack.size (); pos++)
    checkvar += buf[pos];

  return checkvar % 1223;
}




/**
 * recieve();
 *
 * @brief The read function is used for retrieving a package
 *        in the sequence.
 *
 * it does not examine any of the header data as it 
 * rely on read_data_from_stream performs a clean 
 * check of all data.
 */
PacketInfo g1_Datalink::recieve() {
  PacketInfo *pkg = (PacketInfo *)(mbout.receive());
  PacketInfo pack = *pkg;
  delete pkg;
  return pack;
}

void
g1_Datalink::readTranslator ()
{
  // We must make sure that data is ready for us.
  translator.wait_for_clear_to_send ();
  // if the translator hasn't read any data, continue;
  if (translator.incomming.size () == 0) 
    return;
  // load the first package and examine it.
  Packet *pkg = (Packet *) translator.incomming.pop ();
  dumpPacket(*pkg);
  if(!vrfyPacket(*pkg)) {
    delete pkg;
    cout << "Package dropped" << endl;
    return;
  }

  // move to local scope package
  Packet pack = *pkg;
  delete pkg;

  dl_header &
    hdr = *(dl_header *) pack.getBuffer ();

  // if the data is from myself, destroy it.
  if(hdr.sendId == getSenderId()) {
    cout << "private drop (" << (int)getSenderId() << ")" << endl;
    return;
  }
  
  // Verify that the reciever is correct
  if (getSenderId () != hdr.recvId && hdr.recvId != 255) {
    cout << "Not for me (" << (int)getSenderId() << ") "  << (int)(hdr.recvId) << endl;
    return;
  }

  g1_SlidingWindow * _win = (g1_SlidingWindow *) mapWindows[hdr.sendId];
  // if its an ident - we need to reset buffers.
  if(hdr.control & CTRL_IDENT) {
    if(_win != NULL) {
      if(_win->connected == true) {
	cout << "a reset has been requested" << endl;
	_win->reset();
      }
    }
    else {
      _win = new g1_SlidingWindow ();
      mapWindows.insert (hdr.sendId, (Item *) _win);
    }
    if(! (hdr.control & CTRL_ACK)) {
      send_ident_ack(hdr.sendId);
      //cout << "(" << (int)getSenderId() << ") ident from " << (int)(hdr.sendId) << " to " << (int)(hdr.recvId) << endl;
    } // else cout << "(" << (int)getSenderId() << ") ack ident from " << (int)(hdr.sendId) << " to " << (int)(hdr.recvId) << endl;
    _win -> connected = true;
    return;
  }
  // if there is any old packets running around the net,
  // there will not be any window allocated for them.
  // so here we just force a reset of both peers
  if(_win == NULL) {
    sendIdent(hdr.sendId);
    cout << "no knowledge of peer - send a reset to him(" << (int)hdr.sendId << ")" <<  endl;
    return;
  }

    
  g1_SlidingWindow & window = *(g1_SlidingWindow*)_win;
  
  // examine acks first
  if(hdr.control & CTRL_ACK)
    window.senderwin_acknowledge(hdr.ack);
  
  // if the packet contains data, it must be acked.
  if(hdr.control & CTRL_HAS_DATA) 
    window.recvwin_unacked_mark(hdr.seq);
  else
    return; // if no data is in package - drop it


  // the packet must be the right in sequenxe
  if(hdr.seq != window.recvwin_expected_id()) {
    window.recvwin_unacked_remark();
    return;
  }
  
  window.recvwin_expected_id_inc();

  // byte copy the remaining package 
  // in to the new buffer
  // find out how much data is left in the package
  byte nsize = hdr.size - sizeof (dl_header);

  // the buffer needs to be null based on the data of
  // the package
  byte * recvBuf = pack.getBuffer ();
  PacketInfo *PI = new PacketInfo( Packet(&recvBuf[sizeof (dl_header)], nsize),
				   hdr.sendId );

  mbout.send((Item *)PI);
}


/**
 * @fn send
 *
 * @brief sends a single package to the specified reviever
 *
 * We perform all byte conversion in the 
 * buffer directly since it saves us for any 
 * c++ lea lookup loops (which takes a lot of
 * time to perform in the "big" memory model
 */
void
g1_Datalink::send (const g1_term_id recvId, Packet & data_pack)
{
  dl_header hdr;
  hdr.recvId = recvId;
  hdr.sendId = getSenderId();
  hdr.control = CTRL_HAS_DATA;

  Packet *net_pack = new Packet ();
  net_pack->add ((byte *) & hdr, sizeof (dl_header));
  net_pack->add (data_pack);
  
  mbin.send(net_pack);
}

/// The packets are crc translated and lastly send
/// to the net translator.
void
g1_Datalink::send_packet (Packet & pack)
{
  // add crc
  ((dl_header *) (pack.getBuffer ()))->crc = 0;
  ((dl_header *) (pack.getBuffer ()))->crc = getCRC (pack);
  //dumpPacket(pack);
  //  translator.wait_for_clear_to_send ();
  translator.send_packet (pack);
}

/**
 *
 */
void 
g1_Datalink::sendRemainingAck(byte step) {
  for(HashIterator it = mapWindows.begin(); it != mapWindows.end(); it++) {
    g1_SlidingWindow * win = (g1_SlidingWindow*)it.item();

    if(win->recvwin_unacked()) {
      dl_header hdr;
      hdr.control = CTRL_ACK;
      hdr.step = step;
      int d = it.key();
      hdr.recvId = it.key();
      hdr.sendId = getSenderId();
      hdr.ack = win->recvwin_unacked_id();
      hdr.seq = 0;
      hdr.size = sizeof(dl_header);
      Packet pack((byte *)&hdr,hdr.size);
      send_packet(pack);
    }
  }
}

void g1_Datalink::resendPackets(byte step) {
  Item *last = NULL;
  for(HashIterator it = mapWindows.begin();it != mapWindows.end();it++) {
    g1_SlidingWindow & win = *(g1_SlidingWindow*)it.item();
    if (!win.senderwin_empty ()) {
      // even though we have a fast cp const in packet, references is still faster.
      Packet &packet = win.senderwin_firstunacked();
      dl_header &hdr = *((dl_header *)packet.getBuffer());

      // we must only send when we have passed the amount of steps since last send
      if(step == hdr.step) {
	//cout << "Package resent" << endl;
      // the acknowledgement might have changed, so reeavaluate acks
	byte ctrl = hdr.control & CTRL_HAS_DATA;
	ctrl += CTRL_RESEND;
	if(win.recvwin_unacked()) {
	  ctrl += CTRL_ACK;
	  hdr.ack = win.recvwin_unacked_id();
	}
	hdr.control = ctrl;
	send_packet (packet);
      }
    }
  }
}

bool 
g1_Datalink::sendQueuedPackages(byte step) {
  // examine all windows to see if they are waiting for a connection.
  bool hasDoneSomething = false;
  for (HashIterator it = mapWindows.begin (); it != mapWindows.end (); it++) {
    g1_SlidingWindow &window = *(g1_SlidingWindow *)it.item ();
    
    // We cannot send any data if we do not have a verified
    // connection first.
    if (window.connected == false) {
      // there is no known connection so we must 
      // send an ident to reciever to mark that 
      // we want to send data.
      

      // we wait until our step time has arrived,
      // and then we (re)send the ident.
      if (window.step == step && window.sendIdentCnt < ident_resend_cnts) {
	sendIdent (it.key ());
	++(window.sendIdentCnt);
	hasDoneSomething = true;
      }

      // if we have send (time_resend_cnts) ident packages
      // and we have still not recieved ant data, we now assume 
      // that the connection is broken.
      if(window.sendIdentCnt == ident_resend_cnts) {
	cerr << "No connection to " << (int)it.key() << endl;
	it.destroy();
      }
    } else {
      if (window.newUnSentPackages.size() > 0) {
	
	Packet *pkg = (Packet *)window.newUnSentPackages.pop();
	Packet pack = *pkg;
	delete pkg;
	dl_header &hdr = *((dl_header *)pack.getBuffer());
	hdr.seq = window.senderwin_nextsessionid ();
	
	window.senderwin_insert (pack);
	hdr.size = pack.size ();
	hdr.step = step;
	if(window.recvwin_unacked()) {
	  hdr.control += CTRL_ACK;
	  hdr.ack = window.recvwin_unacked_id();
	} else
	  hdr.ack = 0;

	send_packet (pack);
	hasDoneSomething = true;
      }
    }
  }
  return hasDoneSomething;
}



/**
 * @fn sendNewPackets
 *
 * Examines all packets that are placed in the mbin
 * Mailbox and sends them out.
 *
 * as well as sending all data out, its placed in the slidewin
 * buffer to make sure it can be resend in the case of package loss.
 * resending them again is the job for resendPackets.
 */
bool 
g1_Datalink::sendNewPackets(byte step) {
  if (mbin.hasData ()) {
    // transfer to local scope
    Packet *pkg = (Packet *)mbin.receive();
    Packet pack = *pkg;
    delete pkg;
    dl_header &hdr = *((dl_header *)pack.getBuffer());
    g1_SlidingWindow *_win = (g1_SlidingWindow *)mapWindows[hdr.recvId];

    // if we have no window representing the reciever, 
    // we have no knowledge of any sequence.
    if(_win == NULL ) {
      // We now setup an initial sliding window in which
      // we store the pending packages and mark the window
      // as unconnected.

      _win =  new g1_SlidingWindow();
      _win -> connected = false;
      _win -> step = step;
      _win -> sendIdentCnt = 0;
      _win -> newUnSentPackages.push(new Packet(pack));
      mapWindows.insert (hdr.recvId, _win);

      // Finally we send the first ident, which we hope 
      // will be the one replied on.
      sendIdent (hdr.recvId);

      // Return true to mark that we have 
      // performed net transport.
      return true;
    }
    g1_SlidingWindow &window = *_win;

    // if there is no connection, we continue to queue packages.
    // we also know that all packages in the newUnSentPackages
    // queue are send first, so therefore we must add them here
    if(window.connected == false || window.newUnSentPackages.size () > 0) {

      // store for later send
      _win -> newUnSentPackages.push (new Packet(pack));

      // we have not performed any net transport...
      return false;
    }
    // here we have normal connection, and we can send 
    // data
    hdr.seq = _win -> senderwin_nextsessionid ();
    
    // we insert the package in to the sliding window
    // as we may loose it on the way.
    _win -> senderwin_insert (pack);

    // Update final data storege.
    hdr.size = pack.size ();
    hdr.step = step;

    // if we have any other packages that needs
    // to be acknowledged, we just send it along...
    if (_win -> recvwin_unacked ()) {
      // mark the package as an acknowledge packet
      hdr.control += CTRL_ACK;
      hdr.ack = _win -> recvwin_unacked_id ();
    } else
      hdr.ack = 0;

    send_packet (pack);
    // we have made data transfer.
    return true;
  }
  else // mbin is empty
    return false;
}


/**
 * sendIdent(id = 255)
 *
 * Sends an ident package to the reciever.
 * if one sends a broadast (id = 255), it can be seen 
 * as an ARP request.
 */
void
g1_Datalink::sendIdent(const g1_term_id identId) {
  dl_header hdr;
  hdr.control = CTRL_IDENT;
  hdr.recvId = identId; //Normally broadcast - but can be different
  hdr.sendId = getSenderId();
  hdr.size = sizeof (dl_header);
  hdr.seq = 0;
  hdr.ack = 0;
  Packet pack((byte *)&hdr,hdr.size);
  send_packet (pack);  
}

void g1_Datalink::send_ident_ack(const g1_term_id identId) {
  dl_header hdr;
  hdr.control = CTRL_IDENT | CTRL_ACK;
  hdr.recvId = identId;
  hdr.sendId = getSenderId();
  hdr.size = sizeof (dl_header);
  hdr.seq = 0;
  hdr.ack = 0;
  Packet pack((byte *)&hdr,hdr.size);
  send_packet (pack);  
}


#ifdef __GNUG__
void g1_Datalink::dumpPacket(Packet & pack) {
# if DUMP_PACKAGE==1
  using namespace std;
  dl_header & hdr = *((dl_header *) pack.getBuffer ());
  cerr << "============== Package ==============" << endl;
  cerr << "  Package type: ";
  if(hdr.control & CTRL_ACK) cerr << "acknowledgement ";
  if(hdr.control & CTRL_HAS_DATA) cerr << "data ";
  if(hdr.control & CTRL_IDENT) cerr << "ident ";
  if(hdr.control & CTRL_RESEND) cerr << "resend ";
  cerr << endl;
  cerr << "  Reciever:     " << (int)hdr.recvId << endl;
  cerr << "  Sender:       " << (int)hdr.sendId << endl;
  cerr << "  Sequence:     " << (int)hdr.seq << endl;
  cerr << "  Acknowledge:  " << (int)hdr.ack << endl;
  cerr << "  CRC:          " << (int)hdr.crc << endl;
  cerr << "  Size:         " << (int)hdr.size << endl;
  cerr << "  Step:         " << (int)hdr.step << endl;
  cerr << "-------------------------------------" << endl;
# endif
}
#endif



void *
g1_Datalink::main () {
  while(stop == false) {
    // we try to tell the scheduler that we do 
    // not need to use the processor any more.

    usleep(100000);
#ifdef __GNUG__    
    while(holding) Yield();
    
    // this will only happen on the server
    // as it's the only one that needs to know these details

    if(send_broadcast_ident) {
      sendIdent(255);
      send_broadcast_ident = false;
    }
#endif
    readTranslator();

    // we dont need to process any outgoing data if there is no
    // know participants and any data to send.
    if(mapWindows.size() != 0 || mbin.hasData()) {

      resendPackets(step);

      sendQueuedPackages (step);
      
      if (!sendNewPackets(step))
	sendRemainingAck(step);
      
      step++;
      if (step > time_tick_step){  // each timetick must be long enough 
	step=0;
	
	for (HashIterator it = mapWindows.begin(); it != mapWindows.end(); it++) {
	  g1_SlidingWindow & win = *(g1_SlidingWindow*)it.item ();
	  win.tick ();
	  if(! win.valid ()) {
	    cout << "destroying invalid window" << endl;
	    it.destroy ();
	  }
	}
      }
    }
#ifdef __GNU__
    holding = false;
#endif // __GNU__
  }
  sendRemainingAck(step);
  return 0;
}
