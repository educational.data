#ifndef __DATALINK_H__
#define __DATALINK_H__

#include "packet.h"

class PacketInfo : public Item{
public:
  PacketInfo(Packet pack,g1_term_id id) : packet(pack), sender(id) {
  }

  PacketInfo(const PacketInfo &PI) {
    packet = PI.packet;
    sender = PI.sender;
  }

  Packet packet;
  g1_term_id sender;
};

class Datalink {
public:
  virtual ~Datalink() { }
  virtual PacketInfo recieve() = 0;
  virtual void send(const byte reciever, Packet &pack) = 0;
};

#endif // __DATALINK_H__
