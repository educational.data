#include "packet.h"
#ifdef __GNUG__
# include <iostream>
# define D(s) std::cout << s << " " << std::flush;
#else
# define D(s)
#endif

Packet::Packet()
{
  p = new priv;
  p->ref_cnt=1;

  p->buf_pos = 0;
  p->buf_size = 0;
}
Packet::~Packet() {
  if(--(p->ref_cnt) == 0)
    delete p;
  else
    p=NULL;
}

Packet::Packet(const byte *data, const byte size) 
{
  p = new priv;
  p->ref_cnt=1;

  for (byte pos = 0; pos < size; pos++)
    p->buffer[pos] = data[pos];
  
  p->buf_size = size;
  p->buf_pos = size;
}

const Packet &Packet::operator=(const Packet &pack) {
  if(--(p->ref_cnt) == 0)
    delete p;
  p = pack.p;
  ++(p->ref_cnt);
  return *this;
}

Packet::Packet(const Packet &pack) {
  p = pack.p;
  ++(p->ref_cnt);
}

Packet Packet::copy() {
  return Packet(*p);
}

/// faster copy func since we dont 
/// need to copy unneeded data.
Packet::Packet(const priv & pPriv)
{
  p = new priv;
  p->ref_cnt=1;

  /// copying foreign data in to local buffers
  /// is an optimization since lookup in a 
  /// large memory model takes a lot of time
  /// and that will be dangerous.
  const byte * buf = pPriv.buffer;
  byte upto = pPriv.buf_size;
  byte pos = 0;

  do {
    p->buffer[pos] = buf[pos];
    ++pos;
  }while (pos < upto);
  
  
  p->buf_size = pPriv.buf_size;
  p->buf_pos = pPriv.buf_pos;
}

const Packet &
Packet::add(const Packet & pack)
{
  byte nSize;
  // calculate how much space we have left in this package
  if ((int) (pack.p->buf_size + p->buf_size) > 255)
    nSize = 255;
  else
    nSize = pack.p->buf_size + p->buf_size;
  
  byte far_pos = 0;
  const byte *far_buffer = pack.p->buffer;

  byte buf_pos = p->buf_pos;
  byte *buffer = p->buffer;

  /// a little bit faster approach instead of doing for loops
  do {
    buffer[buf_pos] = far_buffer[far_pos];
    ++far_pos;
    ++buf_pos;
  } while(buf_pos < nSize);
  
  p->buf_size = nSize;
  
  return *this;
}

const Packet &
Packet::add(const byte * srcbuf, const byte size)
{
  p->buf_size += size;
  byte pos = 0;
  byte buf_pos = p->buf_pos;
  byte buf_size = p->buf_size;
  byte *buffer = p->buffer;
  for (; buf_pos < buf_size; buf_pos++) {
    buffer[buf_pos] = srcbuf[pos++];
  }
  
  p->buf_pos = buf_pos;

  return *this;
}

const Packet &
Packet::add(const byte src)
{
  p->buffer[p->buf_pos++] = src;
  p->buf_size++;
  
  return *this;
}

void
Packet::reset()
{
  p->buf_size = 0;
  p->buf_pos = 0;
}

bool
Packet::operator == (const Packet &pack) {
  // test if the package contains pointer to the same data
  if(pack.p == p)
    return true;
  // we can also easily assume that if the bodys size are not the 
  // same, it is not the same.
  if(pack.p->buf_size != p->buf_size)
    return false;

  // if we are here, there is only bytewide compare left;
  byte pos = 0;
  byte *buf = p->buffer;
  byte *far_buf = pack.p->buffer;
  bool result = true;
  while(buf[pos] == far_buf[pos] && pos < p->buf_size) 
    ++pos;
  return pos == p->buf_size && buf[pos-1] == far_buf[pos-1];
}
