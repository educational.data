#ifndef __SOCKET_H__
#define __SOCKET_H__

#include "config.h"

class Socket
{
public:
  Socket ()
    {
    }
  virtual ~ Socket ()
    {
    };
  virtual byte read (byte * buf, const byte size) = 0;
  virtual void write (byte * buf, const byte size) = 0;
};

#endif // __SOCKET_H__
