#include "test/testclass.h"
#include "protocol/net/packet.h"

using namespace test;

class testPacket : public TestClass {
public:
  testPacket() : TestClass("protocol.net.packet") {
  }

  void test1()
  {
    Packet pack;
    ensure ("Package size is not 0 at construction", pack.size () == 0);
  } 
  
  void test2()
  {
    Packet pack;
    byte data[] = { 1, 2, 3, 4 };
    pack.add (data, 4);
    ensure ("Package size is not 4 after adding four bytes",
	    pack.size () == 4);
    pack.reset ();
    ensure ("Package size is not 0 after reset", pack.size () == 0);
  }

  void test3()
  {
    Packet pack;
    ensure ("Packet does not add itself after completion,",
	    pack.add ('a').size () == 1);
  }

  void test4() {
    Packet pack;
    pack << 120;
    pack << 22;
    pack << 121;
    ensure("Pack operator == does not respond correctly when packages is equal", pack == pack.copy());

    Packet pack2;
    pack << 120;
    pack << 121;
    pack << 22;

    ensure("Pack operator == does not respond correctly when packages is unequal", ! (pack == pack2));

  }
  void test5() {
    Packet *pack = new Packet();
    Packet pack2 = *pack;
    delete pack;
  }
};

namespace test {
  testPacket testpacket;
}

TestClass &prepareNet() {
  return testpacket;
}
