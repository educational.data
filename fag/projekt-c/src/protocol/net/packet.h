#ifndef __PACKET_H__
#define __PACKET_H__

#include "config.h"
#include "utility/item.h"

class Packet:public Item
{
  typedef struct priv {
    byte buffer[255];
    byte buf_size;
    byte buf_pos;
    volatile byte ref_cnt;
  };
  priv *p;

  Packet (const priv & pPriv);
public:
  Packet ();
  Packet (const Packet & pack);
  Packet (const Packet * pack);
  Packet (const byte *data, const byte size);

  ~Packet ();

  const Packet & add (const Packet & pack);
  const Packet & add (const byte * data, const byte size);
  const Packet & add (const byte data);

  Packet copy();
  
  const byte operator[](const byte pos) {
    return p->buffer[pos];
  }
  const Packet &operator << (const Packet &pack) {
    return add(pack);
  }
  const Packet &operator << (const byte data) {
    return add(data);
  }

  const Packet &operator = (const Packet &pack);
  bool operator == (const Packet & pack);

  void reset ();
  byte *getBuffer ()
  {
    return (byte *)p->buffer;
  }
  const byte size () const
  {
    return p->buf_size;
  }
};

#endif
