import java.util.*;
import java.lang.*;
import java.io.*;

public class Main {
    private Hashtable hCodes;
    private String sGens = new String();
    private String sProtein = new String();
    
    public Main() { }

    public Hashtable testValues(int offSet)  throws Exception {
	Hashtable hTmp = new Hashtable();
	int iPos = 0;
	int sPos;

	// when we get above the loft of the gen.lenght - we must terminate
	if (offSet >= sGens.length())
	    throw new Exception("Above limit");
	
	for(sPos = offSet;sPos+2<sGens.length();sPos+=3) {
	    String sek = sGens.substring(sPos,sPos+3);
	    // method:
	    //  In case we find a sequence not known we just add it
	    //  otherwise we examine if the proteins are equal
	    //  if they are not, we return a failure... (null value)
	    String sProtval = new String();
	    sProtval += sProtein.charAt(iPos);
	    if(hTmp.get(sek) == null) {
		hTmp.put(sek,sProtval);
	    } else {
		if(hTmp.get(sek) != sProtein) {
		    return null;
		}
		else {
		    // do nothing - data is there allready
		}
	    }
	    
	    if((iPos++)==sProtein.length()-1)
		return hTmp;

	}
	return null;
    }
    public void sortVals() 
    {
	int iOffset = 0;
	try {
	    for(;;) {
		hCodes = null;
		while (hCodes == null) 
		    hCodes = testValues(iOffset++);

		printSequence(iOffset-1);
	    }
	}
	catch(IndexOutOfBoundsException e) {
	    System.out.println(e);
	}
	catch(Exception e) {
      	    System.out.println("Completed run");
	}

    }

    public void printSequence(int offs) {
	System.out.println("Found a possible match at: " + offs);
	for(Enumeration en = hCodes.keys();en.hasMoreElements();) {
	    String elem = (String)en.nextElement();
	    System.out.println(" \"" + elem + 
			       "\" -> " + 
			       hCodes.get(elem));
	}

	System.out.println("");
	
    }

    public void loadProtein(String filename) throws Exception {
	FileReader file = new FileReader(filename);
	BufferedReader brStream = new BufferedReader(file);
	String line;

	String sTmp = new String();
	
	while((line = brStream.readLine() ) != null)
	    sTmp += line;

	// catch all chars from a to Z
	for ( int sPos = 0; sPos<sTmp.length(); sPos++ ) 
	    if((sTmp.charAt(sPos) >= 'a' && sTmp.charAt(sPos) <= 'z' ) ||
	       (sTmp.charAt(sPos) >= 'A' && sTmp.charAt(sPos) <= 'Z' ))
		// char catched - add to sequence
		sProtein += sTmp.charAt(sPos);
    }

    public void loadGenSequence(String filename) throws Exception{
	FileReader file = new FileReader(filename);
	BufferedReader brStream = new BufferedReader(file);
	String line;

	String sTmp = new String();
	
	while((line = brStream.readLine() ) != null)
	    sTmp += line;

	// catch all chars from a to z
	for ( int sPos = 0; sPos<sTmp.length(); sPos++ ) 
	    if((sTmp.charAt(sPos) >= 'a' && sTmp.charAt(sPos) <= 'z' ) ||
	       (sTmp.charAt(sPos) >= 'A' && sTmp.charAt(sPos) <= 'Z' ))
		// char catched - add to sequence
		sGens += sTmp.charAt(sPos);
    }
		
    public static void main(String[] Args) {
	Main mn = new Main();
	try {
	    mn.loadProtein("protein2.txt");
	    mn.loadGenSequence("gensekvens2.txt");
	} catch (Exception e) {
	    System.out.println(e.getMessage());
	    return;
	}
	mn.sortVals();
    }
}
