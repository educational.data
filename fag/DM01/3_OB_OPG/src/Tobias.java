public class Tobias extends Player 
{
    private boolean[] lstSolutions = {true, false, false, true, false, false,false,true,false};
    private int iSolutionsPos = 0;
    
    public Tobias(String na) 
    {
	super(na);
    }
    
    public char getAction() 
    {
	if(iSolutionsPos == lstSolutions.length)
	    iSolutionsPos = 0;

	return lstSolutions[iSolutionsPos++] ? Meet.SAM : Meet.UDN;
    }
    
}

	

