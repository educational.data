import cs1.Keyboard;

public class Interaktiv extends Player {
    public Interaktiv(String name) {
	super(name);
    }
    
    public char getAction() {
	char key = ' ';
	System.out.print("Indtast konfrontations valg (u,s): ");
	while(key != 's' && key != 'u') {
	    key = Keyboard.readChar();
	    
	    // dump errors to stderr
	    if(key != 's' && key != 'u') 
		System.err.print("Det er kun tilladt at taste s eller u - pr�v igen \n-> ");
	}
	return key;
    }
}
