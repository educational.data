import cs1.Keyboard;
import java.util.Vector;

public class Driver {
    public static void main(String[] args) {
	Vector Players = new Vector();
	System.out.println("Velkommen til spillet Konfrontation");

	// Opretter fire playere og lader dem spille imod hverandre
	
	//Players.add(new NormalPlayer());
	Players.add(new Interaktiv("Interaktiv"));
	Players.add(new Samarbejde("Samarbejde"));
	Players.add(new Udnytte("Udnytte"));
	Players.add(new RandomPlayer());
	Players.add(new Edmund("Edmund"));
	Players.add(new Tobias("Tobias"));

	for(int outher=0;outher< Players.size();outher++) {
	    Player pl1 = (Player)Players.get(outher);
	    for(int inner=0;inner< Players.size();inner++) {
		Player pl2 = (Player)Players.get(inner);

		char actA = pl1.getAction();
		char actB = pl2.getAction();
		
		pl1.saveMeet(actA,actB);
		pl2.saveMeet(actB,actA);
	    }
	}

	// udprintning af status
	System.out.println("");
	for(int outher=0;outher< Players.size();outher++) {
	    Player pl1 = (Player)Players.get(outher);
	    pl1.recall();
	}
	System.out.println("\nKlasse             1    2    3    4    5    6");
	System.out.println("---------------------------------------------");
	for(int outher=0;outher< Players.size();outher++) {
	    Player pl1 = (Player)Players.get(outher);
	    System.out.print(leftJustify(15,pl1.getName()));
	    for(int inner=0;inner< Players.size();inner++) {
		System.out.print(rightJustify(5,Integer.toString(pl1.getScore(inner))));
	    }
	    System.out.print("\n");
	}
	System.out.println("---------------------------------------------");
    }
    static private String rightJustify(int size, String tekst) {
	String str = new String();
	while ( str.length() + tekst.length() < size ) str += " ";
	return str + tekst;
    }
    static private String leftJustify(int size, String tekst) {
	String str = new String();
	while ( str.length() + tekst.length() < size ) str += " ";
	return tekst + str;
    }

    
}
