import java.util.ArrayList;

// Klassen definerer en generel spiller. 
// Den konkrete spiller inkl. strategi defineres i en afledt klasse.

abstract public class Player
{
   protected String name;
   protected ArrayList memory;    // her huskes tidligere m�der
   private int total = 0;         // spillerens totale score

   public Player(String na)
   {
      name = na;
      memory = new ArrayList();
   }

   public String getName() {
      return name;
   }

   abstract public char getAction();
   // implementeres i den afledte klasse
   
   public void clearMemory() { 
      memory.clear();
   }

   public void saveMeet(char a, char b) {
      memory.add(new Meet(a,b));
   }

   // F�lgende metode beregner scoren ud fra hukommelsen.
   // Den skal kaldes pr�cis 1 gang for hver modspiller
   // af hensyn til beregning af total score.
   public int score() 
   {
      int sum=0;
      for (int i=0; i<memory.size(); i++) 
  	 sum += ((Meet)memory.get(i)).value(); 
      total += sum;              // den totale score
      return sum;
   }

   public int getTotal() {
      return total;
   }
}
