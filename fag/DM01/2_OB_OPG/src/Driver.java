import cs1.Keyboard;
import java.util.Vector;

public class Driver {
    public static void main(String[] args) {
	Vector Players = new Vector();
	System.out.println("Velkommen til spillet Konfrontation");

	// Opretter fire playere og lader dem spille imod hverandre
	
	//Players.add(new NormalPlayer());
	Players.add(new Samarbejde());
	Players.add(new Udnytte());
	Players.add(new RandomPlayer());
	Players.add(new Edmund("Edmund strategi"));
	Players.add(new TobiasStrategi());



	for(int outher=0;outher< Players.size();outher++) {
	    for(int inner=0;inner< Players.size();inner++) {
		Player pl1 = (Player)Players.get(outher);
		Player pl2 = (Player)Players.get(inner);

		char actA = pl1.getAction();
		char actB = pl2.getAction();
		
		pl1.saveMeet(actA,actB);
		pl2.saveMeet(actB,actA);
	    }
	}
    }
}
