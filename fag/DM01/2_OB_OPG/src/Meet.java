public class Meet
{
   public static final char SAM='s', UDN='u';
   private char Aaction, Baction;

   public Meet(char a, char b)
   {
      Aaction=a;
      Baction=b;
   }

   public char getA() {
      return Aaction;
   }

   public char getB() {
      return Baction;
   }

   public int value()
   { 
      int result = -100000;      // �ndres med sikkerhed
      if (Aaction==SAM && Baction==SAM) result =  2;
      if (Aaction==SAM && Baction==UDN) result = -1;
      if (Aaction==UDN && Baction==SAM) result =  4;
      if (Aaction==UDN && Baction==UDN) result =  0;
      return result; 
   }

   public String toString() {
      return "(" + Aaction + "," + Baction + ")";
   }
}
