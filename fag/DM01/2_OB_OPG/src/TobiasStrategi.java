public class TobiasStrategi extends Player 
{
    private boolean[] lstSolutions = {true, true, false, true, false, false};
    private int iSolutionsPos = 0;
    
    public TobiasStrategi() 
    {
	super("TobiasStrategi");
    }
    
    public char getAction() 
    {
	if(iSolutionsPos == lstSolutions.length)
	    iSolutionsPos = 0;

	return lstSolutions[iSolutionsPos++] ? Meet.SAM : Meet.UDN;
    }
    
}

	

