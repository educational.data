import java.util.Random;

public class RandomPlayer extends Player
{
    public RandomPlayer() 
    {
	super("RandomPlayer");
    }
    public char getAction() 
    {
	Random rnd = new Random();

	// 50% chance for hvert udfald
	return rnd.nextBoolean() ? Meet.SAM : Meet.UDN;
    }
}

	
