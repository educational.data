 

import java.lang.*;
import java.util.*;

/**
 * Calculates mean, min and man values
 *
 * @author Tobias G�nge Nielsen
 * @version $Revision: 1.2 $
 */
public class MathList {
    private Vector m_numbers;

    /** 
     * Constructor setting the internel array with an array list
     *
     * @param numbers An array of double numbers
     */
    public MathList(double[] numbers) {
	m_numbers = new Vector();
	for (int pos=0;pos<numbers.length;pos++)
	    m_numbers.add(new Double(numbers[pos]));
    }

    /**
     * constructor setting the array with an vector
     *
     * @param vc input vector array
     */
    public MathList(Vector vc) {
	m_numbers = vc;
    }


    /**
     * Return the size of the internal buffer
     *
     * @return size of internal buffer 
     */
    public int size() {
	return m_numbers.size();
    }

    /** 
     * calculate mean value of a series of numbers
     *
     * @return mean value of internal list
     */
    public double mean() {
	int pos = 0;
	double retVal = 0;

	// traverse list as long as there is numbers
	while(pos<m_numbers.size()) {
	    // add each number to retval
	    retVal+= ((Double)m_numbers.get(pos++)).doubleValue();
	}
	// calculate mean value and return data
	return retVal / m_numbers.size(); 
    }

    /**
     * find max number in list
     *
     * @return max value of internal list
     */
    public double max() {
	int pos = 0;
	double retval = -1*Double.MAX_VALUE;

	// traverse list
	for ( pos=0; pos < m_numbers.size(); pos++ ) {
	    // select the largest number
	    retval = ((Double)m_numbers.get(pos)).doubleValue() > retval ? 
		((Double)m_numbers.get(pos)).doubleValue() : 
		retval;
	}

	return retval;
    }
    
    /**
     * find smallest number in list
     *
     * @return minimum value of internal list
     */
    public double min() {
	int pos;
	double retval = Double.MAX_VALUE;

	// traverse list
	for ( pos=0; pos < m_numbers.size(); pos++ ) {
	    // select the smallest number
	    retval = ((Double)m_numbers.get(pos)).doubleValue() < retval ? 
		((Double)m_numbers.get(pos)).doubleValue() : 
		retval;
	}

	return retval;
    }
}
