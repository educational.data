 

import junit.framework.*;
import java.util.*;
import java.io.*;

/**
 * Main Application Entry point <p> 
 * includes normal runtime info, and details regarding running
 * normalized tests
 *
 * @author Tobias G. Nielsen
 * @version $Revision: 1.8 $
 */
public class Main {
    /** 
     * Main application entry point 
     */
    public static void main(String[] args) {
	// handle situation where user forgets to enter anything
	if(args.length == 0) {
	    System.err.println("You need to enter a filename to import");
	    System.exit(1);
	}
	TextFile file = GetFile(args[0]);
	MathList mt = new MathList(ReadData(file));
	System.out.println("Array size: " + mt.size());
	System.out.println("Input max size: " + mt.max());
	System.out.println("Input mean size: " + mt.mean());
	System.out.println("Input min size: " + mt.min());
	System.out.println("Done");
	
    }

    /**
     * ReadData uses a TextFile object to import all data in to a 
     * vector <p>
     *
     * It also makes sure that all errors in the form of diffrent
     * exceptions are handled safely
     *
     * @param TextFile file - an open textfile object containing
     *                 double values
     * @return a Vector containing a list of Double values
     */     
    private static Vector ReadData(TextFile file) {
	// create Vector to hold data
	Vector vc = new Vector();

	// enter loop and run until eof
	while(!file.EOF()){
	    try {
		Double db = file.readDouble();
		vc.add(db);
	    }
	    catch (NumberFormatException e2) {
		System.err.println("Cannot read line - its a non double value");
		System.exit(0);
	    }
	    catch (IOException e3) {
		System.err.println("Unknown Error");
		System.exit(0);
	    }
	}
	return vc;
    }

    /**
     * GetFile in a successfull state opens a TextFile Object
     * 
     * @param sFileName a complete link to a file (String)
     * @return a valid TextFile object
     */
    private static TextFile GetFile(String sFileName) {
	try {
	    // Open File
	    TextFile file = new TextFile(sFileName);
	    return file;
	} 
	catch(FileNotFoundException e) {
	    System.err.println("Cannot open file " + sFileName);
	    System.exit(1);
	}
	catch(IOException e2) {
	    System.err.println("Unexpected error");
	    System.exit(1);
	}
	// this should newer happen - but needed to compile
	return null;
    }

    /**
     * Main Test suite setup - called by the JUnit framework
     * Read on @see www.junit.org
     */
    public static Test suite() {
	// top suite
	TestSuite suite = new TestSuite("Main Test Suite");

	// i work with one suite per class basis
	suite.addTest(new TestSuite(MathTest.class, "Math Calculation"));
	suite.addTest(new TestSuite(TextTest.class, "Text reader input"));

	return suite;
    }
}
