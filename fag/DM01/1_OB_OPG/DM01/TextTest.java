 

import java.io.*;
import java.util.*;
import java.text.*;
import junit.framework.*;

/**
 * Tests the reading of the TextFile class
 *
 * @author Tobias G. Nielsen
 * @version $Revision: 1.9 $
 */

public class TextTest extends TestCase {

    public TextTest(String boot){
	super(boot);
    }

    // failed to open this file
    public void testFailureToOpenFile() throws IOException {
	try {
	    TextFile buf = new TextFile("AFileThatSurelyIsNotThere");
	}
	catch (FileNotFoundException e1) {
	    assertTrue(true);
	    return;
	}
	assertTrue("File do not exists, but sys do not cast exception", false);
    }

    // should open this file
    public void testSuccesfullOpenFile() throws IOException{
	try {
	    new TextFile("taldata");
	}
	catch (FileNotFoundException e1) {
	    // file shold have been found
	    assertTrue("File Should exits, but exception casted",false);
	    return;
	}
	assertTrue(true);
    }
}
