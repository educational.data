 

import java.io.*;
import java.util.*;
import java.text.*;

/**
 * 
 * FileObject representing a list of Doubles
 * can only return double values at the time
 *
 * @author Tobias G. Nielsen
 * @version $Revision: 1.4 $
 */

public class TextFile {
    private StringTokenizer stToken,stTokenNext;
    private BufferedReader brFile;
    private boolean bEOF;
    
    /** 
     * Constructor opening a filestream
     *
     * @param filename A url to a valid text file
     */
    public TextFile(String filename)
	throws FileNotFoundException,IOException
    {
	bEOF=false;
	brFile = new BufferedReader( new FileReader( filename ) );
	refresh(); // read the first token
    }
    
    /**
     * Reads a new String from the tokenizer object and then parses 
     * as a Double<p>
     *
     * If it finds a value not able to be interpreted as a double, 
     * it dumps an error and goes on to the next line <p>
     *
     * if an EOF is expected, it rethrows an EOFException
     *
     * @param in BufferedReader acts as a stream handle
     * @return a double result, exception on eof.
     */
    public Double readDouble() 
	throws IOException, NumberFormatException 
    {
	if ( stToken == null ) refresh ();

	// keeps on looping until either it finds a double
	// or an exception occour
	while(true) {
	    try {
		String item = stToken.nextToken();
		return Double.valueOf ( item.trim() );

	    }
	    catch ( NoSuchElementException e1 ) {
		// no more tokens
		refresh ();
	    }
	}
    }
    
    /**
     * flag set to indikate an EOF
     */
    public boolean EOF() {
	return bEOF;
    }

    /**
     * Reads a new line in to the Tokenizer object
     */
    private void refresh() 
	throws IOException 
    {
	String sLine;
	// must only execute if there is more data
	if(!bEOF) {
	    sLine = brFile.readLine();
	    stToken = stTokenNext; // wait until next state
	    if ( sLine == null ) {
		bEOF = true; // set flag
		return;
	    }	
	    stTokenNext = new StringTokenizer (sLine);
	}
    }
}	
