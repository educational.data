 
import junit.framework.*;

/**
 * Tests the math class
 *
 * @author Tobias G. Nielsen
 * @version $Revision: 1.7 $
 */
public class MathTest extends TestCase {
    private MathList mt;
    
    public MathTest(String str) {
	super(str);
    }

    protected void setUp() {
	double[] vals = {1,2,3,4,5,6};

	mt = new MathList(vals);
    }

    public void testSize() {
	assertEquals(6,mt.size(),0);
    }

    public void testMean() {
	assertEquals (3.5, mt.mean(), 0.0);
    }

    public void testMax() {
	assertEquals (6.0 ,mt.max(), 0.0);
    }

    public void testMin() {
	assertEquals (1.0, mt.min(), 0.0);
    }
}
