// Toolbox.h: interface for the Toolbox class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __TOOLBOX_H__
#define __TOOLBOX_H__
#include <vector>
#include "tcimage.h"


using namespace std;

class tPoint {
 public:
  tPoint() {
    x=y=0;
  }
  tPoint(int X, int Y) {
    x=X;
    y=Y;
  }
  int x, y;
};
typedef vector < tPoint > Blob;
typedef vector < Blob > Blobs;

class nail {
public:
  tPoint center;
  float angle;
};

class Toolbox
{
public:
  tCImage adaptiveHistogram (tCImage img);
  Blobs blobs;
  tCImage createBlobs (tCImage img);

  vector< nail > findNails(tCImage &img);
  vector< nail > findNailsNew(tCImage &img);
  int blbcnt;
  Toolbox ();
  virtual ~ Toolbox ();
  void createHistogram (tCImage & img1, tCImage & img2);
private:

  void combineBlobs (tCImage & blobIndex, unsigned int &A, unsigned int B);
  int appendWest (const tCImage & img, const tCImage & blobIndex, int x, int y);
  int appendNorth (const tCImage & img, const tCImage & blobIndex, int x,
		   int y);
  vector < bool > findMaxima (const vector < long >&histBuf, int &mSize);
  vector < long > dampHistogram (const vector < long >histBuf);
  void generateHistogram (tCImage & img,
			  const std::vector < long >&histBuf,
			  const long histCnt, std::vector < bool > &maxs,
			  int offset);
};

#endif
