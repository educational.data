///////////////////////////////////////////////
// compilation interface for CImage
///////////////////////////////////////////////

#include "tcimage.h"
#include <cpp/image.h>
#include <points/point2d.h>

using namespace ipl;

tCImage::tCImage() {
  pImg = new CImage();
}

tCImage::tCImage(const tCImage &img) {
  pImg = new CImage(*(img.pImg));
}

tCImage::tCImage(const int w, const int h, const int dp) {
  pImg = new CImage(w,h,dp);
}

tCImage::tCImage(const int w, const int h, const int dp, const int def) {
  pImg = new CImage(w,h,dp,def);
}

tCImage::~tCImage() {
  delete pImg;
}

const tCImage &
tCImage::operator=(const tCImage &img) {
  *pImg = *(img.pImg);
  return *this;
}

int
tCImage::GetWidth() const {
  return pImg->GetWidth();
}

int
tCImage::GetHeight() const {
  return pImg->GetHeight();
}

int 
tCImage::GetPixel(const int x, const int y) const {
  return pImg->GetPixel(x,y);
}

void 
tCImage::SetPixel(const int x, const int y, const int col) {
  pImg->SetPixel(x,y,col);
}

void 
tCImage::Save(const char * name) {
  pImg->Save(name);
}
  
void 
tCImage::Load(const char * name) {
  pImg->Load(name);
}

void
tCImage::DrawLine(const int x1,
		  const int y1,
		  const int x2,
		  const int y2,
		  const int color) {
  CPoint2D< int > p1(x1,y1), p2(x2,y2);
  pImg->DrawLine(p1,p2,color);
}
