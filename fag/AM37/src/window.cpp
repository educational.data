#include "window.h"
#include <gdk/gdkpixbuf.h>
#include <gdkmm/pixbuf.h>

#include <gtkmm/drawingarea.h>

//Custom drawing area with modified expose_event.
class CustomDrawingArea : public Gtk::DrawingArea
{
public:
  CustomDrawingArea() : DrawingArea() {

  }
  
  void resize_myArea(int x, int y) {
    set_size_request(x, y);
  }
  bool on_expose_event(GdkEventExpose* event) {
    Glib::RefPtr<Gdk::Pixbuf> image = Gdk::Pixbuf::create_from_file(".dump.bmp");
    image->render_to_drawable(get_window(), get_style()->get_black_gc(),
			      0, 0, 0, 0, image->get_width(), image->get_height(), // draw the whole image (from 0,0 to the full width,height) at 100,80 in the window
			      Gdk::RGB_DITHER_NONE, 0, 0);
 
    return true;
  }
};


myWin::~myWin() {
  delete m_drawing_area;
}

myWin::myWin(int w, int h) : Gtk::Window() {
  m_drawing_area = new CustomDrawingArea;
  m_drawing_area->resize_myArea(w,h);
  add(*m_drawing_area);
  show_all_children();
}
