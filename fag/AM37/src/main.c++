#ifdef HAVE_CONFIG
# include "config.h"
#endif
#include "toolbox.h"
#include <iostream>
#include <vector>
#include "window.h"
 
#include <gtkmm/main.h>


using namespace std;
typedef vector < string > vstring;
typedef vector < string >::iterator ivstring;

void doHelp(const string progName);

int main(int __argc, char **__args) {
  //examine args
  bool bHistogram = false;
  bool bBlobs = false;
  bool bNails = false;
  bool bOutputFile = false;
  string sOutputFile;
  string sInFile;
  vstring args;

  Gtk::Main kit(__argc, __args);
  
  for (int c=1;c<__argc;c++) {
    args.push_back(__args[c]);
  }
  for (int na = 0;args.size() > na;na++) {
    string arg = args[na];
    if(arg == "-o" || arg == "--output") {
      if(args.size() == na) 
	continue;
      sOutputFile = args[++na];
      bOutputFile = true;
    } 
    else if(arg == "--histogram") {
      bHistogram = true;
    } 
    else if(arg == "--help") {
      doHelp(__args[0]);
      exit(0);
    }
    else if(arg == "--blobs") {
      bBlobs = true;
    } 
    else if(arg == "--nails") {
      bNails = true;
    }
    else if(arg.substr(0,2) == "--") {
      cerr << "Error: cannot understand argument " << arg << endl;
      exit(1);
    } 
    else if(arg.substr(0,1) == "-") {
      string zf = arg.substr(1);
      if(zf.find("h") != string::npos) bHistogram = true;
      if(zf.find("b") != string::npos) bBlobs = true;
      if(zf.find("n") != string::npos) bNails = true;
      
    }
    else if(arg.substr(0,1) != "-") {
      sInFile = arg;
    }
  }
  
  if (sInFile.size() == 0) {
    cerr << "An input must be given" << endl;
    exit (1);
  }

  Toolbox box;
  tCImage img;
  img.Load(sInFile.c_str());
  if(bHistogram) {
    tCImage img2;
    box.createHistogram(img,img2);
    img = img2;
  }
  if(bBlobs)
    img = box.createBlobs(img);
  if(bNails) {
    tCImage timg(img);
    tCImage timg2(img);
    vector< nail > nails = box.findNails(img);

    for(vector< nail >::iterator it = nails.begin();it != nails.end(); it++) {
      cout << "Screw found: Angle: " << it->angle <<
	" degrees, center={" << it->center.x << "," <<
	it->center.y << "}" << endl;
    }

    nails = box.findNailsNew(timg2);

    for(vector< nail >::iterator it = nails.begin();it != nails.end(); it++) {
      cout << "Screw found: Angle: " << it->angle <<
	" degrees, center={" << it->center.x << "," <<
	it->center.y << "}" << endl;
    }
  }
  img.Save(".dump.bmp");
  
  myWin win(img.GetWidth(),img.GetHeight());
  kit.run(win);
}

void doHelp(const string progName) {
  cout << "Usage: " << progName << " [FLAGS] fil" << endl << endl;
  cout << "Analyse a picture to see if it can find any nail frame(s)" << endl << endl;
  cout << "Flags:" << endl;
  cout << "\
  -o, --output FILE   exports the result into FILE \n\
  -h, --histogram     generates an adaptive histogram \n\
  -b, --blobs         isolates and examines for blobs\n\
  --help              gives a small help (this page)" << endl << endl;
  cout << "Produced by tobibobi@mip.sdu.dk" << endl;
  
}
