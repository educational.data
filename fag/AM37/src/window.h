#ifndef __WINDOWSE_H__
#define __WINDOWSE_H__

#include <gtkmm/window.h>

class CustomDrawingArea;

class myWin : public Gtk::Window {
public:
  myWin(int w, int h);
  virtual ~myWin();
protected:
  CustomDrawingArea *m_drawing_area;
};
#endif
