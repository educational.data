/* src/config.h.  Generated automatically by configure.  */
/* src/config.h.in.  Generated automatically from configure.in by autoheader.  */

/* Name of package */
#define PACKAGE "NailsSeeker"

/* Version number of package */
#define VERSION "0.1"

/* 
 This is the version number of GTK-- found
 The version is found using gtkmm. if you are certain that you have a version
 better than this, you must make sure that pkg-config can find it.
 */
#define GTKMM_VERSION 2.0

