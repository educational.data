#ifndef __TCIMAGE_H__
#define __TCIMAGE_H__

namespace ipl {
class CImage;
};

class tCImage {
  ipl::CImage *pImg;
public:
  tCImage();
  tCImage(const tCImage &img);
  tCImage(const int w, const int h, const int dp);
  tCImage(const int w, const int h, const int dp, const int def);

  ~tCImage();

  int GetWidth() const;
  int GetHeight() const;

  const tCImage &operator=(const tCImage &img);
  
  int GetPixel(const int x, const int y) const;
  void SetPixel(const int x, int y, const int col);
  void DrawLine(const int x1, 
		const int y1, 
		const int x2, 
		const int y2, 
		const int color);

  void Save(const char *name);
  void Load(const char *name);
};
  
#endif // __TCIMAGE_H__
