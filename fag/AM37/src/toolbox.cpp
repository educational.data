// Toolbox.cpp:implementation of the Toolbox class.
//
//////////////////////////////////////////////////////////////////////

#include "toolbox.h"
//#include <math.h>
#include <iostream>


using namespace std;


//////////////////////////////////////////////////////////////////////
//Construction / Destruction
//////////////////////////////////////////////////////////////////////

Toolbox::Toolbox ()
{


}

Toolbox::~Toolbox ()
{


}

void
Toolbox::createHistogram (tCImage & img1,
			  tCImage & img2)
{

  vector < long >histBuf;
  long histCnt = 0;
  histBuf.resize (256);

  //Reset code
  for (int x = 0; x < 256; x++)
    histBuf[x] = 0;

  //calculate image
  img2 = tCImage (512, 256, 8);

  for (int y = 0; y < 256; y++) {
    for (int x = 0; x < 256; x++) {
      int index = img1.GetPixel (x, y);
      histBuf[index]++;
      if (histCnt < histBuf[index])
	histCnt = histBuf[index];
    }
  }
  int maximasize = 0;

  vector < bool > maxs = findMaxima (histBuf, maximasize);

  //generate the base histogram
  generateHistogram (img2, histBuf, histCnt, maxs, 0);

  int numRuns = 0;

  while (maximasize > 2) {
    histBuf = dampHistogram (histBuf);
    maxs = findMaxima (histBuf, maximasize);
    numRuns++;
  }
  generateHistogram (img2, histBuf, histCnt, maxs, 256);

  int b1, b2;
  b1 = b2 = 0;
  {
    bool found1 = false;
    for (int y = 0; y < 256; y++) {
      if (maxs[y] == true) {
	if (!found1) {
	  b1 = y;
	  found1 = true;
	}
	else {
	  b2 = y;
	}
      }
    }
  }
  int center = ((b1 + b2) / 2);
  for (int x = 0; x < img1.GetWidth (); x++) {
    for (int y = 0; y < img1.GetHeight (); y++) {
      int index = img1.GetPixel (x, y);
      if (index > center)
	img1.SetPixel (x, y, 0);
      else
	img1.SetPixel (x, y, 0xFFFFFF);
    }
  }
}
tPoint center(Blob &blb) {
  int mx = 0;
  int my = 0;
  int i;

  for(i=0;i<blb.size();i++)
    mx += blb[i].x;
  for(i=0;i<blb.size();i++)
    my += blb[i].y;

  mx /= blb.size();
  my /= blb.size();
  
  return tPoint(mx,my);
}

float calc_h(Blob &blb,tPoint avg,int p, int q,float phi) {
  int i;

  float cs = cos(-phi);
  float ss = sin(-phi);

  float mx = avg.x;
  float my = avg.y;

  float H = 0;
  for(i=0; i < blb.size(); i++) {
    int x = blb[i].x;
    int y = blb[i].y;

    H += pow( (x-mx)*cs - (y-my)*ss, p )*
      pow( (x-mx)*ss + (y-my)*cs, q );
  }
  return H;
}

float calc_h(Blob &blb,tPoint avg, int p, int q) {
  int i;

  float H = 0;
  for(i=0; i < blb.size(); i++) {
    int x = blb[i].x;
    int y = blb[i].y;

    H+= pow((float)(x-avg.x),p)*pow((float)(y-avg.y),q);
  }
  return H;
}

vector< nail > 
Toolbox::findNails(tCImage &img) {
  tCImage img2(img);

  vector< nail > nails;

  createBlobs(img);

  const float PI=3.141592654;
  for (int i=0;i < blobs.size();i++) {
    if(blobs[i].size() > 700 && blobs[i].size() < 1200) {
      tPoint avg = center(blobs[i]);
      float h_y = calc_h(blobs[i],avg,0,2);
      float h_x = calc_h(blobs[i],avg,2,0);
      float h_11 = calc_h(blobs[i],avg,1,1);

      float h2x=h_x*h_x;
      float h2y=h_y*h_y;
      float h211 = h_11*h_11;

      float top = 2*h_11;
      float bot = sqrt(h2x - 2*h_x*h_y + h2y + 4*h211);
      
      float ang;
      
      // calculate the angle
      if(h_x<h_y)
	ang = -0.5* asin(top / bot)+PI/2;
      else
	ang = 0.5* asin(top / bot);

      // 3'rd derived
      if(calc_h(blobs[i],avg,1,2,ang)>0)
	ang += PI;

      // only positive angels.
      if(ang<0)ang+=2*PI;

      // calculate the elongate (aflanghed)
      float level = calc_h(blobs[i],avg,2,0,ang) 
	/ calc_h(blobs[i],avg,0,2,ang);

      if(level > 100 && level < 300) {
	nail nl;
	nl.center = avg;
	nl.angle = ang*180/PI;

	nails.push_back(nl);

	/* for(int k=0;k<blobs[i].size();k++) {
	  int x = blobs[i][k].x;
	  int y = blobs[i][k].y;
	  img2.SetPixel(x,y, 0xffffff);
	  } */
	int l = 100;
	int x1 = avg.x + (int)(cos(ang));
	int y1 = avg.y + (int)(sin(ang));
	int x2 = avg.x + (int)(l*cos(ang));
	int y2 = avg.y + (int)(l*sin(ang));
	
	img2.DrawLine(x1,y1,x2,y2,0xffffff);
      }
    }
  }
  img = img2;
  return nails;
}

vector< nail > 
Toolbox::findNailsNew(tCImage &img) {
  tCImage img2(img);

  vector< nail > nails;
  cout << "--" << endl;
  blobs.resize(0);
  createBlobs(img);
  int i,b;
  const float PI=3.141592654;
  for (i=0;i < blobs.size();i++) {
    float D=0;
    tPoint maxDist;
    tPoint avg = center(blobs[i]);
    // find det punkt der er l�nkst v�k fra centrum
    for(b=0; b < blobs[i].size();b++) {
      float d = pow((float)(blobs[i][b].x - avg.x),2) + pow((float)(blobs[i][b].y- avg.y),2);
      if(d>D) {
	D=d;
	maxDist = blobs[i][b];
      }
    }

    if(D>4000 && D<4500 && blobs[i].size() > 700 && blobs[i].size < 1200) {
      // cout << D << endl;
      float ang;
      if(maxDist.x-avg.x == 0) {
	if (maxDist.y-avg.y > 0)
	  ang = PI/2;
	else
	  ang = 3*PI/2;
      } else
	ang = atan(((float)(maxDist.y-avg.y))/((float)(maxDist.x-avg.x)));

      if(maxDist.x < avg.x) ang+=PI;
      if(ang<0)ang+=2*PI;

      nail nl;
      nl.center = avg; 
      nl.angle = ang * 180/PI;
      nails.push_back(nl);

      int l = 100;
      int x1 = avg.x + (int)(cos(ang));
      int y1 = avg.y + (int)(sin(ang));
      int x2 = avg.x + (int)(l*cos(ang));
      int y2 = avg.y + (int)(l*sin(ang));
      
      img2.DrawLine(x1,y1,x2,y2,0x0000ff);
    }
  }
  img = img2;
  return nails;
}


vector < long >
Toolbox::dampHistogram (const vector < long >histBuf)
{
  vector < long >tempBuf (256);
  tempBuf[0] = (histBuf[0] + histBuf[1]) / 2;
  for (int index = 1; index < 255; index++) {
    tempBuf[index] = (histBuf[index] +
		      histBuf[index - 1] + histBuf[index + 1]) / 3;

  }
  tempBuf[255] = (histBuf[255] + histBuf[254]) / 2;
  return tempBuf;
}

vector < bool > Toolbox::findMaxima (const vector < long >&histBuf,
				     int &mSize)
{
  mSize = 0;
  vector < bool > maxs (256);
  if (histBuf[0] > histBuf[1]) {
    mSize++;
    maxs[0] = true;
  }
  for (int x = 1; x < 255; x++) {
    if (histBuf[x] >= histBuf[x - 1] && histBuf[x] > histBuf[x + 1]) {
      mSize++;
      maxs[x] = true;
    }
  }
  if (histBuf[255] > histBuf[254]) {
    mSize++;
    maxs[255] = true;
  }
  return maxs;
}

void
Toolbox::generateHistogram (tCImage & img,
			    const vector < long >&histBuf,
			    const long histCnt,
			    vector < bool > &maxs, int offset)
{

  int col = 255;

  for (int x = 0; x < 255; x++) {
    int val = (255 * histBuf[x] / histCnt);
    val *= (-1);
    val += 255;
    if (maxs[x] == true)
      col = 200;
    else
      col = 255;

    for (int y = 0; y < val; y++)
      img.SetPixel (offset + x, y, col);
  }
}

tCImage
Toolbox::createBlobs (tCImage pimg)
{
  tCImage img = adaptiveHistogram (pimg);
  using namespace std;
  int cmbs = 0;
  blbcnt = 0;
  int w = img.GetWidth ();
  int h = img.GetHeight ();
  int x, y;

  unsigned int A, B;

  unsigned int state = 0;


  try {
    tCImage blobIndexMap (w, h, 24, 0xFFFFFF);

    for (y = 1; y < img.GetHeight (); y++) {
      for (x = 1; x < img.GetWidth (); x++) {
	if (img.GetPixel (x, y) == 0xFF)
	  continue;
	state = 0;
	A = appendNorth (img, blobIndexMap, x, y);
	B = appendWest (img, blobIndexMap, x, y);
	
	if (A == B && A != 0xFFFFFF) {
	  state = 1;
	  blobIndexMap.SetPixel (x, y, A);
	  blobs.at (A).push_back (tPoint(x, y));
	}
	else if (A != 0xFFFFFF && B != 0xFFFFFF && A != B) {
	  state = 20;
	  combineBlobs (blobIndexMap, A, B);
	  blobIndexMap.SetPixel (x, y, A);
	  blobs.at (A).push_back (tPoint(x, y));
	}
	else if (B == 0xFFFFFF && A == 0xFFFFFF) {
	  state = 3;
	  int index = blobs.size ();
	  blobIndexMap.SetPixel (x, y, index);
	  Blob blob;
	  blob.push_back (tPoint(x,y));
	  blobs.push_back (blob);
	}
	else if (A != 0xFFFFFF && B == 0xFFFFFF) {
	  state = 4;
	  blobIndexMap.SetPixel (x, y, A);
	  blobs.at (A).push_back (tPoint(x,y));
	}
	else if (B != 0xFFFFFF && A == 0xFFFFFF) {
	  state = 5;
	  blobIndexMap.SetPixel (x, y, B);
	  blobs.at (B).push_back (tPoint(x,y));
	}
      }
    }
    // opret et lidt mere farverigt billede af blobs'ene
    int ci = 0;
    for (Blobs::iterator it = blobs.begin (); it != blobs.end (); it++) {
      unsigned int col = (ci++) * 0x222222;
      for (Blob::iterator itb = it->begin (); itb != it->end (); itb++) {
	blobIndexMap.SetPixel (itb->x, itb->y, col);
      }
    }
    return blobIndexMap;	// normal return
  }
  catch (exception & ex) {
    cout << "Error: " << ex.what () << endl;
    cout << "Data dump:" << endl;
    cout << "X: " << x << endl;
    cout << "Y: " << y << endl;
    cout << "A: " << A << endl;
    cout << "B: " << B << endl;
    cout << "Size of blobs: " << blobs.size () << endl;
    cout << "Current state: " << state << endl;
    cout << "-------------------------------" << endl;
    cout << flush;
    throw ex;
  }
  catch (...) {
    cout << "Unknown exception" << endl;
    cout << "Data dump:" << endl;
    cout << "X: " << x << endl;
    cout << "Y: " << y << endl;
    cout << "A: " << A << endl;
    cout << "B: " << B << endl;
    cout << "Size of blobs: " << blobs.size () << endl;
    cout << "Current state: " << state << endl;
    cout << "-------------------------------" << endl;
    cout << flush;
  }
  return tCImage ();
}

void
Toolbox::combineBlobs (tCImage & blobIndex, unsigned int &A, unsigned int B)
{
  typedef Blob::iterator vit;
  for (vit it = blobs.at (B).begin (); it < blobs.at (B).end (); it++)
    blobs.at (A).push_back (*it);

  for (vit it = blobs.at (A).begin (); it < blobs.at (A).end (); it++)
    blobIndex.SetPixel (it->x, it->y, A);

  //we must remove B
  if (B == blobs.size () - 1)
    blobs.pop_back ();
  else {
    //move the blobs from the last into B 's old position
    blobs[B] = blobs[blobs.size () - 1];

    //in the special case where A is the last blob, we 
    // must make sure to reset A to its new state.
    if (A == (blobs.size () - 1))
      A = B;
    blobs.pop_back ();
    vit vend = blobs.at (B).end ();
    for (vit it = blobs.at (B).begin (); it < vend; it++) {
      blobIndex.SetPixel (it->x, it->y, B);
    }
  }
}

int
Toolbox::appendNorth (const tCImage & img, const tCImage & blobIndex, int x,
		      int y)
{
  if (y == 0)
    return 0xFFFFFF;

  if (img.GetPixel (x, y) == img.GetPixel (x, y - 1))
    return blobIndex.GetPixel (x, y - 1);
  else
    return 0xFFFFFF;
}

int
Toolbox::appendWest (const tCImage & img, const tCImage & blobIndex, int x,
		     int y)
{
  if (x == 0)
    return 0xFFFFFF;
  if (img.GetPixel (x, y) == img.GetPixel (x - 1, y))
    return blobIndex.GetPixel (x - 1, y);
  else
    return 0xFFFFFF;
}

tCImage
Toolbox::adaptiveHistogram (tCImage img)
{
  vector < long >histBuf;
  long histCnt = 0;
  histBuf.resize (256);

  //Reset code
  for (int x = 0; x < 256; x++)
    histBuf[x] = 0;
  for (int y = 0; y < 256; y++) {
    for (int x = 0; x < 256; x++) {
      int index = img.GetPixel (x, y);
      histBuf[index]++;
      if (histCnt < histBuf[index])
	histCnt = histBuf[index];
    }
  }
  int maximasize = 0;
  vector < bool > maxs = findMaxima (histBuf, maximasize);
  while (maximasize > 2) {
    histBuf = dampHistogram (histBuf);
    maxs = findMaxima (histBuf, maximasize);
  }
  int b1, b2;
  b1 = b2 = 0;
  {
    bool found1 = false;
    for (int y = 0; y < 256; y++) {
      if (maxs[y] == true) {
	if (!found1) {
	  b1 = y;
	  found1 = true;
	}
	else {
	  b2 = y;
	}
      }
    }
  }
  tCImage retImg (img.GetWidth (), img.GetHeight (), 8);
  int center = ((b1 + b2) / 2);
  for (int x = 0; x < img.GetWidth (); x++) {
    for (int y = 0; y < img.GetHeight (); y++) {
      if (img.GetPixel (x, y) > center)
	retImg.SetPixel (x, y, 255);

      else
	retImg.SetPixel (x, y, 0);
    }
  }
  return retImg;
}
