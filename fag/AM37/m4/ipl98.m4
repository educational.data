AC_DEFUN([AC_LIB_IPL98],[
AC_PROVIDE([$0])
AC_ARG_ENABLE([ipl98],[  --with-ipl98=path       location of ipl98 (normally ~/ipl98)],
	with_ipl98="~/ipl98",
	with_ipl98="~/ipl98")
AC_MSG_CHECKING([for ipl98])
IPL98_CC=;
IPL98_LIB=;

if test ! -z "$with_ipl98"; then
  eval arg=$with_ipl98
  if test -r $arg/source/ipl98/ipl98.h ; then	
    AC_MSG_RESULT(found)
    IPL98_CC="-I$arg/source/ipl98 -I$arg/source/ipl98/cpp -I$arg/source"
    IPL98_LIB="-L$arg/lib -lipl98 -lm"
  else
    AC_MSG_RESULT([not found])
    echo "You must supply a valid path to an ipl98 srcdir. (not $with_ipl98)"
    echo "Use ./configure --with-ipl98=path..."
    exit 1
  fi
else
  AC_MSG_ERROR([Error: empty string supplied to --with-ipl98=...])
fi

AC_SUBST(IPL98_CC)
AC_SUBST(IPL98_LIB)
])