\contentsline {section}{\numberline {1}An overview}{2}
\contentsline {section}{\numberline {2}An example to get you started}{3}
\contentsline {section}{\numberline {3}The built-in problems}{8}
\contentsline {subsection}{\numberline {3.1}The `max' problem}{8}
\contentsline {subsection}{\numberline {3.2}The `dj1' problem}{9}
\contentsline {subsection}{\numberline {3.3}The `dj2' problem}{9}
\contentsline {subsection}{\numberline {3.4}The `dj3' problem}{9}
\contentsline {subsection}{\numberline {3.5}The `dj5' problem}{10}
\contentsline {subsection}{\numberline {3.6}The `f6' problem}{10}
\contentsline {subsection}{\numberline {3.7}The `himm' problem}{10}
\contentsline {subsection}{\numberline {3.8}The `mcbK' problem}{11}
\contentsline {subsection}{\numberline {3.9}The `knap' problem}{11}
\contentsline {subsection}{\numberline {3.10}The `rr' problem}{11}
\contentsline {subsection}{\numberline {3.11}The `tt' problem}{12}
\contentsline {subsection}{\numberline {3.12}The `tt:E+S' problem}{13}
\contentsline {subsection}{\numberline {3.13}The `lp' problem}{14}
\contentsline {subsection}{\numberline {3.14}The `lpK' problem}{14}
\contentsline {subsection}{\numberline {3.15}The `nk:K+Seed' problem}{14}
\contentsline {subsection}{\numberline {3.16}The `const' problem}{15}
\contentsline {subsection}{\numberline {3.17}The `rand' problem}{15}
\contentsline {subsection}{\numberline {3.18}The travelling salesman problem `tsp'}{15}
\contentsline {section}{\numberline {4}A note about the encoding of real numbers}{16}
\contentsline {section}{\numberline {5}Getting quick help}{17}
\contentsline {section}{\numberline {6}The interactive display}{17}
\contentsline {section}{\numberline {7}Keeping a log of a run}{19}
\contentsline {section}{\numberline {8}The GA choices}{20}
\contentsline {subsection}{\numberline {8.1}Multiple populations: -P and -M}{20}
\contentsline {subsection}{\numberline {8.2}Reproduction and selection}{20}
\contentsline {subsubsection}{\numberline {8.2.1}The -r{\em ReproductionMethod} option}{20}
\contentsline {subsubsection}{\numberline {8.2.2}Reproduction: replacement policy: -R{\em method}}{21}
\contentsline {subsubsection}{\numberline {8.2.3}Reproduction: elitism: the -E{\em N} option}{22}
\contentsline {subsubsection}{\numberline {8.2.4}Selection method: the -s{\em SXelectionMethod} and -b{\em N} options}{22}
\contentsline {subsubsection}{\numberline {8.2.5}Crossover and mutation: -C{\em CrossOperator}, -T{\em MutationType}, -t}{22}
\contentsline {subsubsection}{\numberline {8.2.6}Crossover and mutation probabilities: -c{\em N} and -m{\em N}}{24}
\contentsline {subsection}{\numberline {8.3}Chromosome length and representation issues: -n{\em N} and -g}{24}
\contentsline {subsection}{\numberline {8.4}File output}{25}
\contentsline {subsection}{\numberline {8.5}Seeding the random number generator: -S{\em Seed}}{25}
\contentsline {section}{\numberline {9}Non-interactive use: -NO{\em Value} and -NA{\em Value}}{26}
\contentsline {section}{\numberline {10}Simple timetabling with PGA}{26}
\contentsline {section}{\numberline {11}Some points about population analysis}{30}
\contentsline {subsection}{\numberline {11.1}Clustering}{32}
\contentsline {subsection}{\numberline {11.2}Summarising With Standard Unix Tools}{33}
\contentsline {subsection}{\numberline {11.3}What not to do}{34}
\contentsline {section}{\numberline {12}Technical points}{35}
\contentsline {section}{\numberline {13}Exercises}{37}
