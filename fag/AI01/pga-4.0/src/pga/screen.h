/*          File: screen.h
 *  Last changed: Wed Jul 19 18:28:13 2000
 *
 *  RCS: $Id: screen.h,v 1.1 2006-12-12 10:25:22 tobibobi Exp $
 *
 *  Locations for use in PGA screen layout.
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

/* Populations: */
#define POP_ROW       1
#define POP_COL       1

/* Initial oversize by */
#define OSZ_ROW       2
#define OSZ_COL       1

/* Chromosomes per population: */
#define CPP_ROW       1
#define CPP_COL       30

/* ....Chromosome length: */
#define CRL_ROW       2
#define CRL_COL       34

/* Number of evaluations: */
#define EVLIM_ROW     3
#define EVLIM_COL     1

/* ...Reporting interval: */
#define REPINT_ROW    4
#define REPINT_COL    1

/* ...Migration interval: */
#define MIG_ROW       5
#define MIG_COL       1

/* .Reproduction: */
#define REPRO_ROW     3
#define REPRO_COL     38

/* ...Crossover type: */
#define CTYPE_ROW     4
#define CTYPE_COL     34

/* ...Crossover rate: */
#define CROSS_ROW     5
#define CROSS_COL     34

/* ....Mutation type: */
#define MTYPE_ROW     6
#define MTYPE_COL     38

/* ....Mutation rate: */
#define MUT_ROW       7
#define MUT_COL       34

/* ......Replacement: */
#define REPL_ROW      8
#define REPL_COL      40

/* ....Log file name: */
#define FNAME_ROW     9
#define FNAME_COL     34

/* Eval function: */
#define EVAL_ROW      6 
#define EVAL_COL      9

/* ....Selection: */
#define SELECT_ROW    7
#define SELECT_COL    9

/* ...Selection bias: */
#define BIAS_ROW      8
#define BIAS_COL      5

/* .......Generation: */
#define GEN_ROW       10
#define GEN_COL       12

/* Evaluations so far: */
#define EVCOUNT_ROW   10
#define EVCOUNT_COL   33

/* The population table */
#define POPTABLE_ROW  11
#define POPTABLE_COL  8

