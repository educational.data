/*          File: screenpc.c
 *       Created: Fri Oct  5 2001
 *  Last changed: Fri Oct  5 15:37:48 2001
 *
 *  RCS: $Id: screenpc.c,v 1.1 2006-12-12 10:25:22 tobibobi Exp $
 *
 * This file contains the stuff that moves the cursor around on a
 * Windows machine, in an MS-DOS or bash window. It needs to supply
 * these routines that would otherwise come from the curses library
 * under Unix/Linux.
 *
 *   initscr() ..... do anything to open/init display
 *   endwin() ...... finally close display
 *   mvprintw(R, C, format_string, args...)
                     Go to row R, col C, then behave like printf.
                     It's just a combination of the next two:
 *   move(R, C) .... Go to row R, col C (top-left is R=0, C=0)
 *   printw(format_string, args...)
                     Behaves like printf
 *   clrtoeol() .... Clear from current screen location to end of line
 *   refresh() ..... Force screen update. The curses package buffers
 *                   updates, and only does them when refresh is called.
 *                   It waits for refresh() in the hope that there will
 *                   some worthwhile optimisations to be made by then.
 *   cbreak() ...... Arrange to get immediate keyboard input rather than
 *                   having the OS wait for <return> before notifying the
 *                   program.
 *   nocbreak() .... Return to default OS method of buffering input.
 *   noecho() ...... Tell OS that OS should not echo keyboard input.
 *   echo() ........ Revert to OS echoing keyboard input.
 */

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <math.h>

void initscr(void)
{}

void endwin(void)
{}

void move(int r, int c)
{
  printf("\033[%d;%dH",c,r);
}

void clrtoeol(void)
{
  printf("\033[K");
}

void refresh(void)
{}

void cbreak(void)
{}

void nocbreak(void)
{}

void echo(void)
{}

void noecho(void)
{}


void mvprintw(int r, int c, char *s, ...)
{
  va_list args;
  move(r,c);
  va_start(args, s);
  vprintf(s, args);
  va_end(args);
}

void printw(char *s, ...)
{
  va_list args;
  va_start(args, s);
  vprintf(s, args);
  va_end(args);
}

