/*          File: nkland.h
 *  Last changed: Wed Jul 12 21:28:40 2000
 *
 *  RCS: $Id: nkland.h,v 1.1 2006-12-12 10:25:21 tobibobi Exp $
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * PGA is copyright (C) 2000 by Peter Ross, Emma Hart.           *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

/* Contents of this file are (C) Terry Jones, Santa Fe Institute, Nov 94 */

#ifndef _NKLAND_H_
#define _NKLAND_H_

#define RANDOM_NEIGHBORS     0
#define NEXTDOOR_NEIGHBORS   1
#define DEF_NEIGHBORHOOD     RANDOM_NEIGHBORS

typedef struct {
    int location[2];
    double fitness[2];
#ifdef NK_STATS
    int hits[2];
#endif
} NK_JUMP;

typedef struct {
    int n;
    int k;
    int njumps;
    NK_JUMP *jump_table;
    double *loci_seeds;
    int **influencers;
    char *neighbors;
} NK_LANDSCAPE;

extern NK_LANDSCAPE *nk_create(int n, int k, int neighborhood_type, long *seed, int show_epistasis, FILE *fp);

extern double nk_fitness(GENE *point, NK_LANDSCAPE *landscape);


#ifdef NK_STATS
extern void nk_stats(NK_LANDSCAPE *landscape, FILE *fp);
#endif

#endif /* _NKLAND_H_ */
