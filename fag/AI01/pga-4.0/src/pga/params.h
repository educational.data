/*          File: params.h
 *  Last changed: Wed Sep 13 14:16:26 2000
 *
 *  RCS: $Id: params.h,v 1.1 2006-12-12 10:25:21 tobibobi Exp $
 *
 *  This file lists the parameter names and flag-characters.
 *  Command-line arguments are mostly handled  by looking for
 *  the relevant flag-character within paramlist and passing
 *  the matching constant (an `enum keynames') and argument
 *  to process(..) defined in params.c. Parameter-file name/value
 *  pairs are handled by looking up the name in paramlist and
 *  passing that matching constant and value to process(..)
 */
 
/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/
 
#ifndef _PARAMS_H
#define _PARAMS_H

enum keynames {
  EVAL,
  GRAY_CODING,
  MAX_FITNESS,
  REPRODUCTION,
  ELITE,
  REPLACEMENT,
  SELECT,
  SELECTION_BIAS,
  CHROMO_LENGTH,
  POPS,
  CHROMOS_PER_POP,
  INIT_OVERSIZE,
  MIGRATION_INT,
  CROSSOVER_TYPE,
  CROSSOVER_RATE,
  MUTATION_RATE,
  ADAPTIVE,
  MUTATION_TYPE,
  REPORT_INTERVAL,
  TWINS,
  RNG_SEED,
  INVALID
};

#define NONE '\0'

struct params 
{
  char flag;
  char *name;
  enum keynames kn;
} paramlist[] = {
  {'e',  "eval",                EVAL},
  {'g',  "gray-coding",         GRAY_CODING},
      /* You cannot specify max fitness, but you might have mistakenly */
      /* pasted it into your parameter file from a log file, so there  */
      /* is an entry for it here. It will do nothing.                  */
  {NONE, "max-fitness",         MAX_FITNESS},
  {'r',  "reproduction",        REPRODUCTION},
  {'E',  "elite",               ELITE},
  {'R',  "replacement",         REPLACEMENT},
  {'s',  "select",              SELECT},
  {'b',  "selection-bias",      SELECTION_BIAS},
  {'n',  "chromosome-length",   CHROMO_LENGTH},
  {'P',  "pops",                POPS},
  {'p',  "chromosomes-per-pop", CHROMOS_PER_POP},
  {'I',  "initial-oversize",    INIT_OVERSIZE},
  {'M',  "migration-interval",  MIGRATION_INT},
  {'C',  "crossover-type",      CROSSOVER_TYPE},
  {'c',  "crossover-rate",      CROSSOVER_RATE},
  {'m',  "mutation-rate",       MUTATION_RATE},
  {'a',  "adaptive",            ADAPTIVE},
  {'T',  "mutation-type",       MUTATION_TYPE},
  {'i',  "reporting-interval",  REPORT_INTERVAL},
  {'t',  "twins",               TWINS},
  {'S',  "random-number-seed",  RNG_SEED},
      /* The following may ONLY be set via command-line flags,    */
      /* not within a parameter file. They are separately handled */
      /* in the command-line-processing loop, and are listed here */
      /* only for completeness.                                   */
  {'F',  "data-file",           INVALID},
  {'f',  "parameter-file",      INVALID},
  {'N',  "non-interactive",     INVALID},
  {'l',  "gens-per-stage",      INVALID},
  {'h',  "help",                INVALID},
      /* This entry marks the end of the parameter list */
  {NONE, NULL,                  INVALID}
};

#endif



