/*          File: ssmap.c
 *  Last changed: Wed Jul 12 14:08:21 2000
 *
 *  RCS: $Id: ssmap.c,v 1.1 2006-12-12 10:25:22 tobibobi Exp $
 *
 *  Outputs a map of a spatially structured population, for later analysis.
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pga.h"

#define TRUE   1
#define FALSE  0


extern int numchromo;
extern int ss_xmax, ss_ymax;
extern int geno_size;

void ss_map(char *fname, int gen, CHROMOSOME *pops[], int npops, int popsize)
{
  FILE *f;
  int i;
  char mapname[256];

  sprintf(mapname, "%s.map", fname);
  if ((f = fopen(mapname, "a")) != (FILE *) NULL) {
    fprintf(f, "----------------------\ngeneration = %d\n", gen);
    for (i = 0; i < npops; i++) {
      fprintf(f, "population = %d\n", i);
      do_one_pop(f, gen, pops, i, popsize);
    }
    fclose(f);
  } else
    warn("*** Could not open map file ***\n");
}

int compare_by_fitness(const void *ct1, const void *ct2)
{
  if (((struct CHROMOTYPE *) ct2)->cptr->fitness <
      ((struct CHROMOTYPE *) ct1)->cptr->fitness)
    return (-1);
  else if (((struct CHROMOTYPE *) ct2)->cptr->fitness ==
	   ((struct CHROMOTYPE *) ct1)->cptr->fitness)
    return (0);
  else
    return (1);
}

int compare_by_typeval(const void *ct1, const void *ct2)
{
  return (((struct CHROMOTYPE *) ct1)->typeval -
	  ((struct CHROMOTYPE *) ct2)->typeval);
}

void do_one_pop(FILE * f, int g, CHROMOSOME *pops[], int thispop, int range)
{
  struct CHROMOTYPE *chromodict, *tmp, *sortdict;
  CHROMOSOME *c;
  int *chromotypes;
  int curtype = 0;
  int i, j, found;
  chromodict = (struct CHROMOTYPE *) NULL;

  chromotypes = (int *) MALLOC(numchromo * sizeof(int));

  for (i = 0; i < numchromo; i++) {
    c = &pops[thispop][i];
    tmp = chromodict;
    found = FALSE;
    while (tmp != (struct CHROMOTYPE *) NULL && !found) {
      /* Have we found this string already? */
      if (genecmp(tmp->cptr->gdp->genotype, c->gdp->genotype, geno_size)) {
	chromotypes[i] = tmp->typeval;
	tmp->instances++;
	found = TRUE;
      } else
	tmp = tmp->next;
    }
    /* If we haven't seen it already .. */
    if (!found /*tmp == (struct CHROMOTYPE *)NULL*/ ){
      tmp = (struct CHROMOTYPE *) MALLOC(sizeof(struct CHROMOTYPE));
      tmp->typeval = curtype;
      tmp->instances = 1;
      tmp->cptr = c;
      tmp->next = chromodict;
      chromodict = tmp;
      chromotypes[i] = curtype++;
    }
  }

  /* Sort out how to represent this in printout .. crude but never mind */

  /* Step 1 is to transer the linked list to an array: */
  sortdict = (struct CHROMOTYPE *) MALLOC(curtype * sizeof(struct CHROMOTYPE));
  for (i = curtype - 1, tmp = chromodict;
       tmp != (struct CHROMOTYPE *) NULL;
       i--, tmp = tmp->next) {
    sortdict[i] = *tmp;
  }

  /* This array needs to be sorted, so max fitness comes first: */
  qsort(sortdict, curtype, (sizeof(struct CHROMOTYPE)), compare_by_fitness);

  /* Now assign characters, starting from 'A', up to 'Z' at most. */
  /* The 27th and lower fitness chromosomes appear as '.'         */
  for (i = 0; i < curtype; i++)
    sortdict[i].typechar = (i < 26) ? 'A' + i : '.';

  /* Now sort that array by typeval again, so we can index into it */
  /* by the entries in the chromotypes array                       */
  qsort(sortdict, curtype, (sizeof(struct CHROMOTYPE)), compare_by_typeval);

  /* Print the rectangular map to file */
  for (i = 0; i < ss_ymax; i++) {
    for (j = 0; j < ss_xmax; j++) {
      fprintf(f, " %c", sortdict[chromotypes[i * ss_xmax + j]].typechar);
    }
    fprintf(f, "\n");
  }
  fprintf(f, "\n");

  /* Print the dictionary for this map */
  /* Print it fittest first; so must sort again */
  qsort(sortdict, curtype, (sizeof(struct CHROMOTYPE)), compare_by_fitness);
  for (i = 0; i < curtype && i < 26; i++) {
    fprintf(f, "%d %c: %4d %14.9f \n",
	    g,
	    sortdict[i].typechar,
	    sortdict[i].instances,
	    sortdict[i].cptr->fitness);

    for(j=0;j<geno_size;j++)
      fprintf(f, " %d",  sortdict[i].cptr->gdp->genotype[j].val);
    fprintf(f,"\n");        
  }
  fprintf(f, "total no of distinct chromosomes = %d\n\n", curtype);

  /* Free up everything used this time */
  tmp = chromodict;
  while (tmp != (struct CHROMOTYPE *) NULL) {
    chromodict = tmp->next;
    free(tmp);
    tmp = chromodict;
  }
  free(chromotypes);
  free(sortdict);
}









