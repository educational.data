/*          File: main.c
 *  Last changed: Wed Apr 18 11:58:25 2001
 *
 *  RCS: $Id: main.c,v 1.1 2006-12-12 10:25:20 tobibobi Exp $
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/


#include <stdlib.h>
#include <stdio.h>              /* Include the IO and maths header files. */
#include <unistd.h>             /* For getpid() */
#include <string.h>
#include <math.h>
#include <time.h>
#include <curses.h>

#include "version.h"            /* title string and version number */
#include "pga.h"                /* defines genodata and chromosome structs */
#include "nkland.h"             /* Terry Jones' neat implementation of NK */

/*
 * Note that with a compiler such as GCC, if you specify -ansi
 * explicitly then it suppresses all machine-specific defines
 * such as MSDOS or _unix_ or _vax_, the assumption being that
 * if you code were strictly ANSI it would be fully portable
 * and if it weren't then you wouldn't be porting it. Hmmm.
 * So be careful about this when trying to make the code run
 * on some new OS or machine.
 */


/* Globals used in various files, but declared here  */

int numpop = 5,             /* Number of populations */
 numchromo = 50,            /* Number of chromos in a population */
 geno_size = 32,            /* Length of a chromosome */
 gens_stage = 100,          /* How many times to cycle before asking user */
 repinterval = 10,          /* Interval between screen/file reports */
 isadaptive = FALSE,        /* Using adaptive mutation? */
 usegray = FALSE,           /* Treat bit patterns as Gray codes in decode */
 ss_task = FALSE,           /* A spatial selection run (ssgen or ssone)? */
 pause_first = FALSE,       /* Allow user to read complaints about tt file? */
 rr_blocks,                 /* RR number of low-level blocks (no default) */
 rr_blocksize = 8,          /* RR default block size */
 rr_gap = 7,                /* RR default gap */
 rr_mstar = 4,              /* RR default no of rewardable bits, low-level */
 migint = 10,               /* Interval between migrations */
 biggest_allele = 1,        /* In most problems, alleles in range 0..this */
 tournament_size = 10,      /* Default size in tournament selection */
 tt_maxevents = 0,          /* Default # of events in timetabling */
 tt_maxslots = 0,           /* Defualt # of slots in timetabling */
 tt_days = 1,               /* Default # of days in timetabling */
 tt_slots_per_day = 1,      /* Default # of slots per day in tiemtabling */
 tt_spread = 1,             /* Default definition of near-clash */
 tt_clash_type = 1,         /* Clashes don't count per-student (and gripe) */
 tt_mtn1 = 3,               /* Event tournament size in timetable mutation */
 tt_mtn2 = 3,               /* Slot tournament size in timetable mutation */
 elite = 1,                 /* How many to keep in elitism */
 hd_set_size = 10,          /* HD-based replacement: size of tournament */
 chromo_id = 1;             /* Chromosome IDs start from this value */
int randseed;               /* The random seed */
long *sticks;               /* Used in knapsack prob: array of sizes */
long target;                /* Used in knapsack prob: target */
NK_LANDSCAPE *nkl;          /* Where TJ's NK jump table gets stored */
int nk_k;                   /* Used in NK: the value of K */
long nk_seed;               /* Used in NK: seed for making table */
FILE *output = (FILE *) NULL;   /* Output file if any */
FILE *chromofile[MAX_POPS]; /* Chromo files, one per pop, if any output file */
FILE *genfile[MAX_POPS];
int interactive = TRUE;     /* Interactive? */
int finaldump = FALSE;      /* Noninteractive: do final chromodump? */
int chromoutput = FALSE;    /* Any chromosome output? */
int genoutput = FALSE;
int twins = FALSE;          /* Crossover produces pairs, or just one child? */
char *datafilename = (char *) NULL;
char *paramfilename = (char *) NULL;
char outfilename[256], chromofilename[256], genfilename[256];
double *tt_scores;          /* Used in timetabling, smart mutation */
char *geno_size_string;     /* String naming size of a chromo (hack) */
char *rr_blockarray;        /* Used in RR for higher-level block scoring */
double mute_rate = -1.0,    /* Default mutation  1/n: neg=still unset */
 cross_rate = 0.6,          /* Default crossover rate if applicable */
 bias = 1.5,                /* Default bias for rank selection */
 tt_penalty_clash = 10.0,   /* TT: penalty for a clash */
 tt_penalty_order = 3.0,    /* TT: penalty for an order violation */
 tt_penalty_exclude = 5.0,  /* TT: penalty for an exclusion violation */
 tt_penalty_consec = 1.0,   /* TT: penalty for a near-clash */
 rr_ustar = 1.0,            /* RR default reward for first block */
 rr_u = 0.3,                /* RR default reward for later blocks */
 rr_v = 0.02;               /* RR default bonus/penalty for low-level bit */
char *eval_name,            /* String giving name of problem */
 *maxfitness,               /* String giving max fitness (if known) */
 *cross_name,               /* String naming crossover type */
 *select_name,              /* String naming selection type */
 *repro_name,               /* String naming reproduction type */
 *replace_name,             /* String naming replacament policy */
 *mutate_name;              /* String naming mutation operator */
double *total_abs_fitness;  /* Used in roulette selection: size of wheel */
double lpmax;               /* Used in long-path problems */
int evals;                  /* How often eval has been called */
int ss_xmax, ss_ymax, ss_n; /* Size of grid and length of walk in ssN */
int breed_n;                /* Breeding pop size for breeder GA */
double user_threshold;      /* Non-interactive? Stop when one/all reach this */
int n_for_stop;             /* How many must reach user_threshold to stop */
double oversizing = 1.0;    /* Init pop too big by this factor: pick best*/
int permutation = FALSE;    /* Is the problem permutation based? */
int gen_total = 0;          /* Total number of generations */
int nCities;                /* Used in TSP problems */

/* Timetabling structures */

/* This will be an array where the chains get rooted: */

struct constraint **tt_event_data = (struct constraint **) NULL;

/* And this is where the preset events get noted: */

struct constraint *tt_presets = (struct constraint *) NULL;

/* This struct matches keywords to symbols, and says how many numbers */
/* ought to be picked up after the keyword is found. A value of ALL   */
/* in the args_wanted field means gather up as many numbers as can be */
/* found on the input line, rather than a fixed number.               */

#define CHUNK 10   /* The array where numbers get stashed first will    */
                   /* increase by this size whenever necessary. MUST BE */
                   /* AT LEAST 2.                                       */
#define ALL -1     /* MUST BE NEGATIVE: means "get all numbers on line" */


/* Default bindings for function pointers */

CHROMOSOME *(*initialise) (int, int) = init_pop;
CHROMOSOME(*selection) (CHROMOSOME *[], int, int) = rank_select;
CHROMOPAIR(*crossover) (CHROMOSOME, CHROMOSOME) = two_pt_cross;
CHROMOSOME(*mutate) (CHROMOSOME, double) = mutate_01;
void (*reproduction) (CHROMOSOME *[], int, int) = one_reproduction;
void (*decode) (GENE *, int, double, double, double *) = long_decode;
double (*mrate) (CHROMOSOME, CHROMOSOME) = nonadaptive;
double (*evaluate) (GENE *) = eval_max;
int (*find_bad_event) (GENE *) = find_worst_event;
int (*find_good_slot) (GENE *, int) = find_free_slot;
void (*insert) (CHROMOSOME *[], CHROMOSOME, int, int) = replace_worst;

/**********************************************/
/* Information about program flags/parameters */
/**********************************************/

#define NONE '\0'

struct params paramlist[] = {
  {'e',  "eval",                EVAL},
  {'g',  "gray-coding",         GRAY_CODING},
      /* You cannot specify max fitness, but you might have mistakenly */
      /* pasted it into your parameter file from a log file, so there  */
      /* is an entry for it here. It will do nothing.                  */
  {NONE, "max-fitness",         MAX_FITNESS},
  {'r',  "reproduction",        REPRODUCTION},
  {'E',  "elite",               ELITE},
  {'R',  "replacement",         REPLACEMENT},
  {'s',  "select",              SELECT},
  {'b',  "selection-bias",      SELECTION_BIAS},
  {'n',  "chromosome-length",   CHROMO_LENGTH},
  {'P',  "pops",                POPS},
  {'p',  "chromosomes-per-pop", CHROMOS_PER_POP},
  {'I',  "initial-oversize",    INIT_OVERSIZE},
  {'M',  "migration-interval",  MIGRATION_INT},
  {'C',  "crossover-type",      CROSSOVER_TYPE},
  {'c',  "crossover-rate",      CROSSOVER_RATE},
  {'m',  "mutation-rate",       MUTATION_RATE},
  {'a',  "adaptive",            ADAPTIVE},
  {'T',  "mutation-type",       MUTATION_TYPE},
  {'i',  "reporting-interval",  REPORT_INTERVAL},
  {'t',  "twins",               TWINS},
  {'S',  "random-number-seed",  RNG_SEED},
      /* The following may ONLY be set via command-line flags,    */
      /* not within a parameter file. They are separately handled */
      /* in the command-line-processing loop, and are listed here */
      /* only for completeness.                                   */
  {'F',  "data-file",           INVALID},
  {'f',  "parameter-file",      INVALID},
  {'N',  "non-interactive",     INVALID},
  {'l',  "gens-per-stage",      INVALID},
  {'h',  "help",                INVALID},
      /* This entry marks the end of the parameter list */
  {NONE, NULL,                  INVALID}
};


/************************************************************************/
/* The main program.                                                    */
/************************************************************************/

int main(int argc, char *argv[])
{
  int command, pop, gen, mignext, action, i, j;
  char c;
  CHROMOSOME *pops[MAX_POPS], migchromo;
  char *bigbuffer;
  int nConverged, satisfied;
  
  randseed = (unsigned) time(0) + getpid();
  
  mignext = 0;
  n_for_stop = 0;
  satisfied = FALSE;
  geno_size_string = (char *) MALLOC(20);
  sprintf(geno_size_string, "%d", geno_size);
  maxfitness = geno_size_string;
  eval_name = "max";
  cross_name = "two-point";
  select_name = "rank";
  repro_name = "one";
  mutate_name = "per-gene";
  replace_name = "worst";
  for (i=0;i<MAX_POPS;i++){
    chromofile[i] = (FILE *)NULL;
    genfile[i] = (FILE *)NULL;
  }


  /************************************************************/
  /* Deal with command-line parameters, including any request */
  /* to read parameters from a file.                          */
  /************************************************************/

  for (command = 1; command < argc; command += 1) {
    handle(argv[command]);      /* Handle any arguments. */
  }

      /* Seed RNG. User may have specified a seed */
  bsd_srandom(randseed);

  /***************************/
  /* Parameter sanity checks */
  /***************************/

  if (interactive || n_for_stop < 0)/* If interactive, stop test is that ALL */
    n_for_stop = numpop;            /* have converged; user may say sooner.. */

  if (geno_size > CRUCIAL_GENO_SIZE) {
    decode = double_decode;
    if (usegray) {
      error("PGA: chromosome length looks a bit big for Gray code\n"
            "     option - admittedly just a crude check.\n");
    }
  }

  if (evaluate == eval_tsp){
    if((crossover != pmx_cross) &&
       (crossover != ox_cross) &&
       (crossover != gox_cross) &&
       (crossover != gpx_cross)) {
      error("PGA: tsp problems must use pmx, ox, gox or gpx crossover\n");
    }
    nCities = geno_size;
    if (datafilename == (char *) NULL)
      datafilename = TSPFILE;
    if (!read_tsp_file()){
      error("PGA: error reading tsp file\n");
    }
  } else if (evaluate == eval_knap) {
    sticks = (long *) calloc(geno_size, sizeof(long));
    if (datafilename == (char *) NULL)
      datafilename = WEIGHTSFILE;
    if (!read_weights_file()){
      error("PGA: no weights file accessible. It should hold\n"
            "     the target value (an integer) and up to\n"
            "     %d further integers. The aim is to find a\n"
            "     subset with sum as close to the target as possible.\n",
            geno_size);
    }
  } else if (evaluate == eval_nk) {
    if(nk_k < 1 || nk_k > geno_size) {
        error("PGA: in nk, k should lie in range 1..n\n");
    }
    nkl = nk_create(geno_size, nk_k, NEXTDOOR_NEIGHBORS, &nk_seed, 0, stdout);
  } else if (evaluate == eval_rr) {
    int n;
    double max;
    if (datafilename == (char *) NULL)
      datafilename = RRDATAFILE;
    read_rrdata_file();
    rr_blockarray = MALLOC(rr_blocks);
    maxfitness = (char *) MALLOC(20);
    n = rr_blocks;
    max = 0.0;
    while (n > 0) {
      max += rr_ustar + (n - 1) * rr_u;
      n /= 2;
    }
    sprintf(maxfitness, "%.6f", max);
  } else if (evaluate == eval_lp || evaluate == eval_lp2) {
     if(geno_size % 2 == 0) {
        geno_size -= 1; /* force size to be odd */
        if(geno_size > 157) {
          error("PGA: for long-path problems a chromosome length\n"
                "     larger than 157 will cause FP overflow.\n");
        }
     }
     if(evaluate == eval_lp2) {
           /* lp2 has an endpoint more than 2 bits different from start */
       lpmax += 3.0*pow(2.0,(double)((geno_size-1)>>1))+(double)geno_size;
     } else {
           /* whereas lp's end is 110..0, two bits away from start */
       lpmax = 3.0*pow(2.0,(double)((geno_size-1)>>1))+(double)geno_size-2.0;
     }
     sprintf(maxfitness, "%.1f", lpmax);
  } else if (evaluate == eval_tt) {
    if (datafilename == (char *) NULL)
      datafilename = TTDATAFILE;
    if (!read_tt_datafile()){
      error("PGA: see the documentation for details of the data file\n");
    }
  }
  
  if (crossover == pmx_cross || 
      crossover == ox_cross  || 
      crossover == gox_cross ||
      crossover == gpx_cross ){
    /* order based operators: check that a permutation problem
     * is being used */
    if (!permutation){
      error("PGA: an order based crossover operator has been selected"
            " for a problem\n     that is not permutation based.\n");
    }
  }
  
  if (mutate == mutate_SBM || 
      mutate == mutate_OBM || 
      mutate == mutate_PBM){
    /* order based operators: check that a permutation problem
     * is being used */
    if (!permutation){
      error("PGA: an order based mutation operator has been selected"
            " for a problem\n     that is not permutation based.\n");
    }
  }

  if(insert == replace_parent ||
     insert == replace_by_hd ) {
    if(ss_task) {
      error("PGA: pointless to set the replacement policy in "
            "spatial reproduction (-rssone,-rssgen)\n");
    }
  }
  
  if ((reproduction == breeder_reproduction)
      && (breed_n > numchromo)) {
    error("PGA: in breeder GA, breed size %d must be no\n"
          "     larger than population size %d", breed_n, numchromo);
  }
  if ((reproduction == ss_gen_reproduction)
      || (reproduction == ss_one_reproduction)) {
    ss_task = TRUE;
    replace_name = "*spatial*";
    ss_xmax = (int) sqrt((double) numchromo);
    ss_ymax = numchromo / ss_xmax;
    numchromo = ss_xmax * ss_ymax;
    migint = 0;
  }
  if (mute_rate < 0.0) { /* user has not set it */
    mute_rate = 1.0/geno_size;
  }
  if (mutate == mutate_tt
      || mutate == mutate_tt_each) {
    tt_scores = (double *) MALLOC(geno_size * sizeof(double));
  }

  /********************************/
  /* End of sanity checking stuff */
  /********************************/

  if (pause_first) {
    warn("**** Press RETURN to continue..");
    while (getchar()!= '\n');
  }

  if (interactive) {
    prepare_display();

    prepare_input();
  }
  total_abs_fitness = (double *) MALLOC(numpop * sizeof(double));
  evals = 0;
  for(i=0; i<numpop; i++)
    pops[i] = initialise(i, numchromo);

  gen_total = 1;
  action = CONTINUE;

  if (output != NULL) {
    prepare_logfile_output(output);
  }

  nConverged = report(0, output, pops, user_threshold);
  i = 0;                        /* used to gather up any digits typed in */

  while (action != QUIT) {
    if (interactive)
      action = action_loop(pops, &nConverged, &satisfied);
    
    if (action == CONTINUE) {

      /* This is the actual GA stuff! */
      for (gen = 1; gen <= gens_stage && !satisfied; gen++, gen_total++) {
        for (pop = 0; pop < numpop; pop += 1) { /* For each generation */
          reproduction(pops, pop, numchromo);      /* (a) reproduce. */
        }
        if (numpop > 1 && migint > 0 && gen_total % migint == 0){
                                                   /* (b) perhaps migrate */
          migchromo = selection(pops, mignext, numchromo);
          for (pop = 0; pop < numpop; pop += 1) {
            if (pop != mignext) /* Don't insert to its source */
              replace_worst(pops, migchromo, pop, numchromo);
          }
          mignext = (mignext + 1) % numpop;
        }
        if (gen_total % repinterval == 0) {       /* (c) perhaps report */
          nConverged = report(gen_total, output, pops, user_threshold);
        }
        if (nConverged >= n_for_stop)
          satisfied = TRUE;
      }

      /* After gens_stage iterations, it may be time to report again */
      if ((gen_total - 1) % repinterval != 0) {
        nConverged =
            report(gen_total - 1, output, pops, user_threshold);
        if (nConverged >= n_for_stop)
          satisfied = TRUE;
      }
      if (!interactive) {
        if ((output != NULL) && finaldump)
          dump_chromos(pops, gen_total - 1);
        action = QUIT;
      }
    } /* finished another chunk of generations */
  } /* got to quitting point */

  /* All done, tidy up */
  if (interactive) {
    wrap_up_input();
    wrap_up_display();
  }

  /* Close files, free things etc */
  if (output != NULL)
    fclose(output);
  for (i=0;i<numpop;i++){
    if (chromofile[i] != NULL)
      fclose(chromofile[i]);
    if (genfile[i] != NULL)
      fclose(genfile[i]);
  } 
  for (i=0;i<numpop;i++)
    free_pops(pops[i], numchromo);
  exit(0);
}

/************************************************************************/
/* This frees whole population                                          */
/************************************************************************/

void free_pops(CHROMOSOME * whole_pop, int num)
{
  int i;

  for (i = 0; i < num; i++) {
    /* Avoid freeing anything twice; matters for some operating systems */
    if (whole_pop[i].gdp->refcount == 1) {
      free(whole_pop[i].gdp->genotype);
      free(whole_pop[i].gdp);
    } else
      whole_pop[i].gdp->refcount--;
  }
  free(whole_pop);
}

/************************************************************************/
/* This processes the command-line argument 'arg'.                      */
/************************************************************************/

void handle(char *arg)
{
  int newint;
  double newdouble;
  char tt_eventchoice[16], tt_slotchoice[16];
  struct params *pl;
  
  if (*arg == '-') {            /* Either a switch ...        */
    switch (*(arg + 1)) {
          /* The following few cases can ONLY be set by command-line flag, */
          /* not in a parameter file: */
    case 'f':   /* -f<parameter_file> */
      /* read parameters from a file */
      paramfilename = (char *) strdup(arg + 2);
      if (strlen(paramfilename) == 0) {
	error("PGA: use -fparamfile, no space between f and name\n");
      }
      else
	read_param_file(paramfilename);
      break;
      
    case 'F': /* -F<filename> sets input data file name */
      datafilename = (char *) strdup(arg + 2);
      if (strlen(datafilename) == 0) {
        error("PGA: use -Fdatafile, no space between F and name\n");
      }
      break;

    case 'h': /* -h gives help */
      pga_help();
      exit(0);
      break;
      
    case 'l': /* -l<N> sets number of generations per stage */
      if (sscanf(arg, "-l%d", &newint) == 1) {
        if (newint < 1) {
          error("PGA: Less than one generation per stage isn't sensible!\n");
        } else {
          gens_stage = newint;
        }
      }
      break;
      
    case 'N':    /* -NO<t>, -No<t>, -NA<t>, -Na<t> sets non-interactive */
      interactive = FALSE;
      if (strlen(arg) < 4) {
        error("PGA: non-interactive: must specify -NOthreshold or "
              "-NAthreshold\n     or -Nothreshold or -Nathreshold\n");
      }
      switch (*(arg + 2)) {
        case 'o':
          finaldump = TRUE;
        case 'O':
          n_for_stop = 1;
          break;
        case 'a':
          finaldump = TRUE;
        case 'A':
          n_for_stop = -1;        /* because numpop may not yet be set! */
          break;
        default:
          error("PGA: non-interactive: must say O (one) or A (all)\n"
                "     or o or a if you want a final chromosome dump too\n");
      }
      if (sscanf(arg + 3, "%lf", &newdouble) == 1) {
        user_threshold = newdouble;
      } else {
        error("PGA: non-interactive: must set stop threshold\n");
      }
      break;

    default:
      for(pl=paramlist; pl->name != NULL; pl++) {
        if(*(arg+1)==pl->flag) {
          process(pl->kn, arg+2, -1, NULL);
          break;
        }
      }
      if(pl->name == NULL) {
        error("PGA: unknown flag %s\n", arg);
      }
    }
  } else {                      /* ... or an output filename. */
    output = fopen(arg, "a");
    if (output == NULL) {
      error("PGA: can't open %s for writing.\n", arg);
    }
    strcpy(outfilename, arg);
  }
}

/****************************************************/
/* Dumps chromosomes to a file. Filename is         */
/* outfilename.N.chr, eg foo.3.chr for population 3 */
/****************************************************/


void dump_chromos(CHROMOSOME * pops[], int g)
{
  int i, j, k;

  /* save all pops in separate files */
  /* if default file is foo.chr, then save files as
   * foo.1.chr, foo.2.chr,
   */

  if (!chromoutput) {
    for (i=0;i<numpop;i++){
      sprintf(chromofilename, "%s.%d.chr", outfilename,i);
      if((chromofile[i] = fopen(chromofilename, "a")) == (FILE *)NULL) {
        error("PGA: could not open chromosome file %s to append\n",
              chromofilename);
      }
    }
  }
  for (i = 0; i < numpop; i++) {
    for (j = 0; j < numchromo; j++) {
      fprintf(chromofile[i],"%-2d %-4d %-5d %12.7f  ",i,j,g,
              pops[i][j].fitness);
      for (k=0;k<geno_size;k++)
        fprintf(chromofile[i], "%d;", pops[i][j].gdp->genotype[k].val);
      fprintf(chromofile[i], "  %5d %5d %5d\n",
	      pops[i][j].gdp->id,
	      pops[i][j].gdp->parent1,
	      pops[i][j].gdp->parent2);
    }
    fprintf(chromofile[i], "\n\n");
  }
  chromoutput = TRUE;
}

void dump_chromo_gens(CHROMOSOME * pops[], int g)
{
  int i,j,k;
  
  if (!genoutput) {
    for (i=0;i<numpop;i++){
      sprintf(genfilename, "%s.%d.gen", outfilename,i);
      genfile[i] = fopen(genfilename, "a");
      genoutput = (genfile[i] != (FILE *) NULL);
    }
  }

  if (genoutput) {
    for (i = 0; i < numpop; i++) {
      /* just print the best */
      for (j = 0; j < numchromo; j++) {
        fprintf(genfile[i],
                "%-2d %-4d %-5d %12.7f ",
                i, j, g, pops[i][j].fitness);
	for (k=0;k<geno_size;k++)
          fprintf(genfile[i], "%d:", 
                  pops[i][j].gdp->genotype[k].gen);
	fprintf(genfile[i], "  %5d %5d %5d\n",  
		pops[i][j].gdp->id,
		pops[i][j].gdp->parent1,
		pops[i][j].gdp->parent2);
      }
      fprintf(genfile[i], "\n\n");
    }
  }
}


/************************************************************************/
/* The following functions insert 'chromo' into the relevant place      */
/* within population number 'base', of size 'range'                     */
/************************************************************************/

void replace_worst(CHROMOSOME *pops[], CHROMOSOME chromo, int base, int range)
{
  int current;

  /* Can it be inserted at all? */
  if (chromo.fitness > pops[base][range - 1].fitness) {
    chromo.gdp->refcount++;
    total_abs_fitness[base] += fabs(chromo.fitness)
                            -  fabs(pops[base][range-1].fitness);
    if (pops[base][range - 1].gdp->refcount == 1) {
      free(pops[base][range - 1].gdp->genotype);
      free(pops[base][range - 1].gdp);
    } else
      pops[base][range - 1].gdp->refcount--;
    current = range - 2;	/* Start at the second last place. */
    do {			/* If 'chromo' is fitter ... */
      if (chromo.fitness > pops[base][current].fitness) {
	/* ... then push it down one. */	
	pops[base][current + 1] = pops[base][current]; 
	
      } else {
	pops[base][current + 1] = chromo;  /* ... else insert 'chromo' below */
	current = 0;		/* and finish.                    */
      }
      current -= 1;		/* Work up the array until ... */
    } while (current >= 0);	/* we get to the start   */
    if (chromo.fitness > pops[base][0].fitness) {
      pops[base][0] = chromo;	/* If chromo is the best put it in. */
    }
  } else {			/* It cannot be inserted, it is terrible */
    if (chromo.gdp->refcount == 0) {
      free(chromo.gdp->genotype);
      free(chromo.gdp);
    }
  }
}

/*******************************************************************/
/* Replace a parent by the child. Note that if two children are    */
/* being inserted, both parents will be replaced, since after the  */
/* first insertion, only the other parent exists to be found.      */
/* If a parent is the very fittest and the child is of lesser      */
/* fitness, that replacement does not happen; in all other cases   */
/* the child replaces a parent even if the parent was fitter.      */
/* NB, when migrating a chromosome to a new population, only       */
/* replace_worst is used to insert it -- see main procedure.       */
/*******************************************************************/

void replace_parent(CHROMOSOME *pops[], CHROMOSOME chromo, int base, int range)
{
  int index;

  /* find the index of the parent */
  for (index=0;index<range;index++)
    if (pops[base][index].gdp->id == chromo.gdp->parent1)
      break;
  

  /* could be that parent1 got replaced already, if we are using two children: */
  if(index == range)
      for (index=0;index<range;index++)
        if (pops[base][index].gdp->id == chromo.gdp->parent2)
          break;

  /* if we couldn't find parent2, it's because parent1 and parent2 were equal and */
  /* parent1 has already been replaced by first child. Just give up. */
  if(index == range) {
    if (chromo.gdp->refcount == 0) {
	free(chromo.gdp->genotype);
	free(chromo.gdp);
    }
    return;
  }
  
  
  /* for the moment, have elitism:
   * ie if the parent is the best in pop, and the child is worse than this
   *    then do nothing with the child
   */
  
  if (index == 0)
    if (chromo.fitness < pops[base][0].fitness){
      /* do not insert it into the pop */
      if (chromo.gdp->refcount == 0) {
	free(chromo.gdp->genotype);
	free(chromo.gdp);
      }
      return;
    }

  chromo.gdp->refcount++;
  total_abs_fitness[base] += fabs(chromo.fitness)
    -fabs(pops[base][index].fitness);
  if (pops[base][index].gdp->refcount == 1) {
    free(pops[base][index].gdp->genotype);
    free(pops[base][index].gdp);
  } else
    pops[base][index].gdp->refcount--;

  /* now add the child and re sort the population */
  pops[base][index] = chromo;

  qsort(pops[base], range, sizeof(CHROMOSOME), compchromo);
}


/*************************************************************************/
/* Replace the chromosome that is closest in Hamming terms to the child, */
/* from a set of (hd_set_size). This defaults to 10, but can be altered  */
/* by command-line flag eg -Rhd5; -Rhd leaves it at 10. Note that if the */
/* very fittest is selected for replacement, but the child is of lower   */
/* fitness, the replacement does not happen.                             */
/*************************************************************************/

void replace_by_hd(CHROMOSOME *pops[], CHROMOSOME chromo, int base, int range)
{
  
  int nearest, i,chosen,hd;
  int closestHd = 9999999;
 
  /* pick N indivuals from the population:
   * find the one whose fitness is closest to that of the child 
   */ 


  for (i=0;i<hd_set_size;i++){
    chosen = bsd_random()%numchromo;
    hd = hamming_distance(chromo, pops[base][chosen]);
    if (hd < closestHd){
      nearest = chosen;
      closestHd = hd;
    }
  }

  /* for the moment, have elitism:
   * ie if the parent is the best in pop, and the child is worse than this
   *    then do nothing with the child
   */
  
  if (nearest == 0)
    if (chromo.fitness < pops[base][0].fitness){
      /* do not insert it into the pop */
      if (chromo.gdp->refcount == 0) {
	free(chromo.gdp->genotype);
	free(chromo.gdp);
      }
      return;
    }

  chromo.gdp->refcount++;
  total_abs_fitness[base] += fabs(chromo.fitness)
    -fabs(pops[base][nearest].fitness);
  if (pops[base][nearest].gdp->refcount == 1) {
    free(pops[base][nearest].gdp->genotype);
    free(pops[base][nearest].gdp);
  } else
    pops[base][nearest].gdp->refcount--;

  /* now add the child and re sort the population */
  pops[base][nearest] = chromo;

  qsort(pops[base], range, sizeof(CHROMOSOME), compchromo);

}


/***************************************************************************
 * return hamming distance between two chromosomes
 * ie the no. of bits that DIFFER
 **************************************************************************/
int hamming_distance(CHROMOSOME first, CHROMOSOME second)
{
  int i;
  int hd = 0;

  for (i=0;i<geno_size;i++){
    if (first.gdp->genotype[i].val != second.gdp->genotype[i].val)
      hd++;
  }
    
  return(hd);
}


/************************************************************************/
/* Test whether all chromosomes in a population are equal               */
/************************************************************************/
int all_chromos_equal(CHROMOSOME * onepop)
{
  int i, j, k, equalSoFar;

  equalSoFar = TRUE;
  for (i = 0; (i < numchromo - 1) && equalSoFar; i++) {
    if (!genecmp(onepop[i].gdp->genotype,
                onepop[i + 1].gdp->genotype, geno_size)) {
      equalSoFar = FALSE;
    }
  }
  return (equalSoFar);
}

/************************************************************************/
/* Returns >0 if chromo2 is fitter than chromo1, <0 if chromo 1 is      */
/* fitter than chromo2, and 0 if they are equally fit. This is passed   */
/* to qsort as its comparison function.                                 */
/************************************************************************/

int compchromo(const void *chromo1, const void *chromo2)
{
  if ((((CHROMOSOME *) chromo2)->fitness) > (((CHROMOSOME *) chromo1)->fitness))
    return (1);
  else if ((((CHROMOSOME *) chromo2)->fitness) ==
           (((CHROMOSOME *) chromo1)->fitness))
    return (0);
  else
    return (-1);
}

/***********************************************************************/
/* Returns a random double in range 0.0 to 1.0 (never 1.0 exactly)     */
/***********************************************************************/

#define PGARAND_MAX 2147483647.01       /* 0.01+biggest result from random() */

double drandom(void)
{
  return ((double) bsd_random()/ ((double) PGARAND_MAX));
}


/***********************************************************************  
 * Functions for simplyfying the copying and comparing of genotype 
 * composed of GENE arrays
 ***********************************************************************/

int genecmp(GENE *chromo1, GENE *chromo2, int size)
{
  int i;

  for (i=0;i<size;i++)
    if (chromo1[i].val != chromo2[i].val)
      return(FALSE);

  return(TRUE);
}

void genecpy(GENE *copy, GENE *original, int size)
{
  int i,j;

    for (i=0;i<size;i++)
      copy[i].val = original[i].val;

}

/* end of pga.c */
