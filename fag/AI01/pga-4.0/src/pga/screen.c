/*          File: screen.c
 *  Last changed: Thu Mar 15 09:02:57 2001
 *
 *  RCS: $Id: screen.c,v 1.1 2006-12-12 10:25:22 tobibobi Exp $
 *
 * This file contains the stuff that handles user input and output,
 * except for error(..) and warn(...) which are in utils.c.
 *
 * This version of this file is intended for use with the UNIX curses
 * library, it employs these library routines:
 *   initscr() ..... do anything to open/init display
 *   endwin() ...... finally close display
 *   mvprintw(R, C, format_string, args...)
                     Go to row R, col C, then behave like printf.
                     It's just a combination of the next two:
 *   move(R, C) .... Go to row R, col C (top-left is R=0, C=0)
 *   printw(format_string, args...)
                     Behaves like printf
 *   clrtoeol() .... Clear from current screen location to end of line
 *   refresh() ..... Force screen update. The curses package buffers
 *                   updates, and only does them when refresh is called.
 *                   It waits for refresh() in the hope that there will
 *                   some worthwhile optimisations to be made by then.
 *   cbreak() ...... Arrange to get immediate keyboard input rather than
 *                   having the OS wait for <return> before notifying the
 *                   program. 
 *   nocbreak() .... Return to default OS method of buffering input.
 *   noecho() ...... Tell OS that OS should not echo keyboard input.
 *   echo() ........ Revert to OS echoing keyboard input.
 */
 
/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include "pga.h"
#include "screen.h"


/* These were declared in main.c, and are referred to by stuff below */
extern CHROMOSOME *(*initialise) (int, int);
extern CHROMOSOME(*selection) (CHROMOSOME *[], int, int);
extern CHROMOPAIR(*crossover) (CHROMOSOME, CHROMOSOME);
extern CHROMOSOME(*mutate) (CHROMOSOME, double);
extern void (*reproduction) (CHROMOSOME *[], int, int);
extern double (*mrate) (CHROMOSOME, CHROMOSOME);
extern double (*evaluate) (GENE *);
extern void (*insert) (CHROMOSOME *[], CHROMOSOME, int, int);
extern int numpop;
extern int numchromo;
extern int geno_size;
extern int gens_stage;
extern int gen_total;
extern int evals;
extern int repinterval;
extern int isadaptive;
extern int usegray;
extern int migint;
extern int biggest_allele;
extern int tournament_size;
extern int tt_mtn1;
extern int tt_mtn2;
extern int elite;
extern int hd_set_size;
extern int randseed;               
extern int nk_k;                   
extern int n_for_stop;
extern long nk_seed;               
extern int twins;
extern int interactive;
extern char *geno_size_string;     
extern double mute_rate;
extern double cross_rate;
extern double bias;
extern double user_threshold;
extern char *eval_name;
extern char *maxfitness;
extern char *cross_name;
extern char *select_name;
extern char *repro_name;
extern char *replace_name;
extern char *mutate_name;              
extern int ss_n;
extern int ss_task;
extern int ss_xmax;
extern int ss_ymax;
extern int breed_n;                
extern int gens_stage;
extern char outfilename[80];
extern FILE *output;
extern double oversizing;
extern int permutation;
extern struct params paramlist[];

char prompt[80];


/*****************************************************************/
/* prepare_display() does the initial setup of the display,      */
/* including display of parameter values etc                     */
/*****************************************************************/

void prepare_display(void) 
{
  initscr();
  /* ---------- */
  mvprintw(POP_ROW, POP_COL,       "          Populations: %-6d", numpop);
  /* ---------- */
  if(oversizing != 1.0)
    mvprintw(OSZ_ROW, OSZ_COL,     "  Initial oversize by: %-.3f", oversizing);
  /* ---------- */
  if ((reproduction == ss_gen_reproduction)
      || (reproduction == ss_one_reproduction))
    mvprintw(CPP_ROW, CPP_COL,     "  Chromosomes per pop: %-4d (%dx%d)",
             numchromo, ss_xmax, ss_ymax);
  else
    mvprintw(CPP_ROW, CPP_COL,     "  Chromosomes per pop: %-4d", numchromo);
  /* ---------- */
  mvprintw(EVLIM_ROW, EVLIM_COL,   "Generations per stage: %-6d", gens_stage);
  mvprintw(REPINT_ROW, REPINT_COL, "   Reporting interval: %-6d",repinterval);
  mvprintw(MIG_ROW, MIG_COL,       "   Migration interval: %-6d", migint);
  mvprintw(CTYPE_ROW, CTYPE_COL,   "   Crossover type: %s, %s", cross_name,
           (twins) ? "twins" : "one child");
  /* ---------- */
  if (!strcmp(repro_name, "one")
      || !strncmp(repro_name, "ssone", 5))
    mvprintw(CROSS_ROW, CROSS_COL, "   Crossover rate: n/a");
  else
    mvprintw(CROSS_ROW, CROSS_COL, "   Crossover rate: %3.2f", cross_rate);
  /* ---------- */
  if (!strcmp(select_name, "rank"))
    mvprintw(BIAS_ROW, BIAS_COL,   "   Selection bias: %3.2f", bias);
  else
    mvprintw(BIAS_ROW, BIAS_COL,   "   Selection bias: n/a");
  /* ---------- */
  if (isadaptive)
    mvprintw(MUT_ROW, MUT_COL,     "    Mutation rate: adaptive %.4f",
             mute_rate);
  else
    mvprintw(MUT_ROW, MUT_COL,     "    Mutation rate: %.6f", mute_rate);
  /* ---------- */
  mvprintw(CRL_ROW, CRL_COL,       "Chromosome length: %d", geno_size);

  if (output != NULL)
    mvprintw(FNAME_ROW, FNAME_COL, "    Log file name: %s", outfilename);
  /* ---------- */
  mvprintw(MTYPE_ROW, MTYPE_COL,   "Mutation type: %s", mutate_name);
  mvprintw(REPL_ROW, REPL_COL,     "Replacement: %s", replace_name);
  mvprintw(EVAL_ROW, EVAL_COL,     "Eval function: %s", eval_name);
  mvprintw(SELECT_ROW, SELECT_COL, "    Selection: %s", select_name);
  /* ---------- */
  if(reproduction == gen_reproduction
     || reproduction == breeder_reproduction)
    mvprintw(REPRO_ROW, REPRO_COL, " Reproduction: %s (elite: %d)",
             repro_name, elite);
  else
    mvprintw(REPRO_ROW, REPRO_COL, " Reproduction: %s", repro_name);
  /* ---------- */
  mvprintw(POPTABLE_ROW, POPTABLE_COL,
                                   "Pop.......Average..........Best.(max = %s)",
           maxfitness);
  mvprintw(GEN_ROW, GEN_COL,       "Generation: ");
  clrtoeol();
  mvprintw(EVCOUNT_ROW,EVCOUNT_COL,"Evaluations so far: ");
  clrtoeol();
  refresh();
}

/*****************************************************************/
/* wrap_up_display() does whatever is needed to tidy up display  */
/* processing as the program finally finishes eg clear it all    */
/*****************************************************************/

void wrap_up_display(void)
{
  refresh();
  endwin();
}

/*****************************************************************/
/* prepare_input() does whatever is needed to get the system     */
/* ready for user input, eg init the keyboard, turn off echo     */
/*****************************************************************/

void prepare_input(void)
{
  cbreak();
  noecho();
}

/*****************************************************************/
/* wrap_up_input() does whatever is needed to tidy up input      */
/* processing as the program finally finishes eg restore echoing*/
/*****************************************************************/

void wrap_up_input(void)
{
  nocbreak();
  echo();
}

/*****************************************************************/
/* prepare_logfile_output() writes the initial stuff to the log  */
/* file, namely details of parameter settings                    */
/*****************************************************************/

void prepare_logfile_output(FILE *output)
{
  fprintf(output, "\n\n===================================\n");
  fprintf(output, "               eval = %s\n", eval_name);
  if(mutate == mutate_01)
    fprintf(output, "        gray-coding = %s\n", usegray? "true" : "false");
  fprintf(output, "        max-fitness = %s\n", maxfitness);
  fprintf(output, "       reproduction = %s\n", repro_name);
  fprintf(output, "        replacement = %s\n", replace_name);
  if(reproduction == gen_reproduction
     || reproduction == breeder_reproduction)
    fprintf(output, "              elite = %d\n", elite);
  fprintf(output, "             select = %s\n", select_name);
  if (!strcmp(select_name, "fitprop"))
    fprintf(output, "     selection-bias = n/a\n");
  else
    fprintf(output, "     selection-bias = %.6f\n", bias);
  fprintf(output, "  chromosome-length = %d\n", geno_size);
  fprintf(output, "               pops = %d\n", numpop);
  if ((reproduction == ss_gen_reproduction)
      || (reproduction == ss_one_reproduction))
    fprintf(output, "chromosomes-per-pop = %d\n", numchromo);
  else
    fprintf(output, "chromosomes-per-pop = %d\n", numchromo);
  fprintf(output, "   initial-oversize = %.6f\n", oversizing);
  fprintf(output, " migration-interval = %d\n", migint);
  fprintf(output, "     crossover-type = %s\n", cross_name);
  if (!strcmp(repro_name, "one")
      || !strncmp(repro_name, "ssone", 5))
    fprintf(output, "     crossover-rate = n/a\n");
  else
    fprintf(output, "     crossover-rate = %.6f\n", cross_rate);
  fprintf(output, "           adaptive = %s\n", isadaptive? "true":"false");
  fprintf(output, "      mutation-rate = %.6f\n", mute_rate);
  fprintf(output,"      mutation-type = %s\n", mutate_name);
  fprintf(output, " reporting-interval = %d\n", repinterval);
  if (twins)
    fprintf(output, "              twins = true\n");
  else
    fprintf(output, "              twins = false\n");
  fprintf(output, " random-number-seed = %d\n", randseed);
  fprintf(output, "\n");
}

/*******************************************************************/
/* action_loop(..) finds out what the user want to do, and returns */
/* that action after doing any desirable display stuff such as     */
/* altering a parameter display or doing something to show the     */
/* that his choice has been noted.                                 */
/*******************************************************************/

int action_loop(CHROMOSOME *pops[], int *nConverged, int *satisfied)
{
  int action, i, j;
  char c;
  int mapped, saved, savedGen; /* These are booleans used to control */
                               /* what user commands are allowed, eg */
                               /* if you've just saved chromosomes   */
                               /* you may not save them again till   */
                               /* they have changed.                 */
  mapped = saved = savedGen = FALSE;
      /* When action_loop is called, changes have just happened!     */
  i = 0; /* i is used to store any number the user might type in at the prompt */
  
  do {
    setprompt(output, ss_task, mapped, saved, *satisfied);
    mvprintw(0, 1, prompt);
    clrtoeol();
    refresh();
    switch (c = getchar()){
    /* Cases are almost in alphabetic order, except digits at end and r/R which falls
     * through to case n/N
     */
    case 'c':
    case 'C':
      if (*nConverged == numpop)
        action = ASK_AGAIN;
      else {
        printw("continuing..");
        refresh();
        action = CONTINUE;
        mapped = saved = savedGen = FALSE;
        if (i > 0) {
          gens_stage = i;
          mvprintw(EVLIM_ROW, EVLIM_COL + 23,
                   "%-6d",
                   gens_stage);
          i = 0;
        }
      }
      break;
    case 'g':
    case 'G':
      if ((output != NULL) && !savedGen) {
        printw("saving chromosome gens..");
        refresh();
        dump_chromo_gens(pops, gen_total - 1);
        savedGen = TRUE;
      } else if (!savedGen) {
        strcpy(outfilename, "unnamed");
        printw("saving chromosome gens..");
        refresh();
        dump_chromo_gens(pops, gen_total - 1);
        savedGen = TRUE;
      }
      action = ASK_AGAIN;;
      i = 0;                /* reset number input */
      break;
    case 'm':
    case 'M':
      if (ss_task && !mapped) {
        if (output != NULL) {
          printw("writing grid map..");
          refresh();
        } else {
          printw("writing grid map..");
          refresh();
          strcpy(outfilename, "unnamed");
        }
        ss_map(outfilename,
               (gen_total - 1), pops, numpop, numchromo);
        mapped = TRUE;
      }
      action = ASK_AGAIN;
      i = 0;                /* reset number input */
      break;
    case 'r':
    case 'R':
      bsd_srandom(randseed);
    case 'n':
    case 'N':
      printw("starting again..");
      refresh();
      move(GEN_ROW, GEN_COL + 12);
      clrtoeol();
      mvprintw(EVCOUNT_ROW, EVCOUNT_COL, "Evaluations so far:");
      move(EVCOUNT_ROW, EVCOUNT_COL+20);
      clrtoeol();
      for(j=0; j<numpop; j++) {
        move(POPTABLE_ROW+1+j, POPTABLE_COL);
        clrtoeol();
      }
      refresh();
      for(i=0; i<numpop; i++)
        free_pops(pops[i], numchromo);
      gen_total = 1;
      evals = 0;
      for(i=0; i<numpop; i++)
        pops[i] = initialise(i, numchromo);
      if (output != NULL) {
        fprintf(output,
                "\n\n------ re-starting ------\n\n");
      }
      *nConverged = report(0, output, pops, user_threshold);
      *satisfied = (*nConverged >= n_for_stop) ? TRUE : FALSE;
      mapped = saved = savedGen = FALSE;
      action = ASK_AGAIN;
      i = 0;                /* reset number input */
      break;
    case 'q':
    case 'Q':
      action = QUIT;
      break;
    case 's':
    case 'S':
      if ((output != NULL) && !saved) {
        printw("saving chromosome values..");
        refresh();
        dump_chromos(pops, gen_total - 1);
        saved = TRUE;
      } else if (!saved) {
        strcpy(outfilename, "unnamed");
        printw("saving chromosome values..");
        refresh();
        dump_chromos(pops, gen_total - 1);
        saved = TRUE;
      }
      action = ASK_AGAIN;;
      i = 0;                /* reset number input */
      break;
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
      i = 10 * i + c - '0';
      action = ASK_AGAIN;
      break;
    default:
      action = ASK_AGAIN;
      i = 0;                /* reset number input */
      break;
    }
  } while (action == ASK_AGAIN);
  return(action);
}

/************************************************************************/
/* This updates the screen and, if necessary, the output file           */
/* If interactive, returns the number that have converged.              */
/* If NOT interactive, returns the number that have passed the supplied */
/* threshold value.                                                     */
/************************************************************************/

int report(int gen_total, FILE *output, CHROMOSOME * pops[], double threshold)
{
  int pop, chromo;
  int n_at_threshold = 0;
  char conv_char;
  double average, best, tmp;

  if (interactive) {
    mvprintw(GEN_ROW, GEN_COL + 12, "%d", gen_total);   /* necessary */
    mvprintw(EVCOUNT_ROW, EVCOUNT_COL + 20, "%d", evals);
  }
  if (output != NULL) {
    fprintf(output, "\nGenerations = %d  Evaluations-so-far = %d\n",
            gen_total, evals);
  }
  for (pop = 0; pop < numpop; pop += 1) {
    average = 0.0;
    best = -1000000.0;
    for (chromo = 0; chromo < numchromo; chromo += 1) {
      /* Note that best is usually pops[pop][numchromo] since */
      /* the population is sorted - but not sorted with -rssN */
      tmp = pops[pop][chromo].fitness;
      average += tmp;
      if (best < tmp)
        best = tmp;
    }
    average /= (double) numchromo;
    if (fabs(average - best) < 1e-9
        && all_chromos_equal(pops[pop])) {
      conv_char = '=';
      n_at_threshold++;
    } else {
      conv_char = ' ';
      if (!interactive && best >= threshold)
        n_at_threshold++;
    }
    if (interactive) {
      mvprintw(POPTABLE_ROW + 1 + pop, POPTABLE_COL,
               "%-4d%c    %12.7f     %12.7f",
               pop, conv_char,
               average,
               best);
      refresh();
    }
    if (output != NULL) {
      fprintf(output, "  %-4d    %12.7f     %12.7f\n",
              pop, average, best);
    }
  }
  if (interactive)
    refresh();
  return (n_at_threshold);
}

/************************************************************************/
/* This sets the prompt for next interaction                            */
/************************************************************************/

int stringcat(char *s1, char *s2, int n)
{
  int i, j;
  for (i = n, j = 0; s2[j] != '\0'; i++, j++) {
    s1[i] = s2[j];
  }
  s1[i] = '\0';
  return (i);
}

void setprompt(FILE *f, int sstask, int mapped, int saved, int satisfied)
{
  int i;
  for (i = 0; i < 80; i++)
    prompt[i] = '\0';

  i = stringcat(prompt, "(R)epeat, (N)ew start, (Q)uit", 0);
  if ((f != NULL) && !saved)
    i = stringcat(prompt, ", (S)ave", i);
  if (sstask && !mapped)
    i = stringcat(prompt, ", (M)ap", i);
  if (!satisfied)
    i = stringcat(prompt, ", (C)ontinue", i);
  stringcat(prompt, ": ", i);
}



