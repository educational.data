/*          File: help.c
 *  Last changed: Thu Mar 15 09:04:20 2001
 *
 *  RCS: $Id: help.c,v 1.1 2006-12-12 10:25:20 tobibobi Exp $
 *
 *  Help invoked by -h lag or by command-line foulup.
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "version.h"

/***********************************************************************/
/* Help for the user                                                   */
/***********************************************************************/
void pga_help(void)
{
  printf("%s, version %s\n", TITLE, VERSION);
  printf("Flags: defaults in (..), or in [..] if not user-nameable\n");
  printf("   -a       Adaptive mutation flag (FALSE)\n");
  printf("   -b<n>    Set selection bias. (1.5)\n");
  printf("   -c<n>    Set crossover rate, for `gen' or `breedN'. (0.6)\n");
  printf("   -e<fn>   Set evaluation function. (max)\n");
  printf("   -f<file> Read parameters from file <file>.\n");
  printf("   -g       In function optimisation, use Gray coding\n");
  printf("   -h       Display this information.\n");
  printf("   -i<n>    Set reporting interval in generations. (10)\n");
  printf("   -l<n>    Set # of generations per stage. (100)\n");
  printf("   -m<n>    Set bit mutation rate. (1/length)\n");
  printf("   -n<n>    Set chromosome length. (32)\n");
  printf("   -p<n>    Set number of chromosomes per population. (50)\n");
  printf("   -s<op>   Set selection operator. (rank)\n");
  printf("   -r<op>   Set reproduction operator. (one)\n");
  printf("   -t       Twins: crossover produces pairs (FALSE)\n");
  printf("   -C<op>   Set crossover operator. (two)\n");
  printf("   -E<n>    Set elite size, for `gen' or `breedN'. (1)\n");
  printf("   -F<file> Use problem datafile <file> instead of default.\n");
  printf("   -I<x>    Initial population is cream of <x> sets. (1.0)\n");
  printf("   -M<n>    Interval between migrations. (10)\n");
  printf("   -NA<n>   Non-interactive, stop when All reach <n>.\n");
  printf("   -Na<n>   Like -NA<n> but also save final chromosomes.\n");
  printf("   -NO<n>   Non-interactive, stop when One reaches <n>.\n");
  printf("   -No<n>   Like -NO<n> but also save final chromosomes.\n");
  printf("   -P<n>    Set number of populations. (5)\n");
  printf("   -R<op>   Set replacement policy. (worst)\n");
  printf("   -S<n>    Seed the random number generator. [from clock+PID]\n");
  printf("   -T<op>   Set mutation type. [per-gene].\n");
  printf("   <file>   Also log output in <file>.\n");
  printf("   Crossover operators ...... one, two, uniform, none,\n");
  printf("                              and for tsp: pmx, ox, gox, gpx\n");
  printf("   Selection operators ...... rank, fitprop,\n");
  printf("                              tnK (K=integer > 1),\n");
  printf("                              tmK (K=integer > 1).\n");
  printf("   Reproduction operators ... one, gen,\n");
  printf("                              breedN (N=integer > 1),\n");
  printf("                              ssoneN (N=integer > 0),\n");
  printf("                              ssgenN (N=integer > 0).\n");
  printf("   Evaluation functions ..... max, dj1, dj2, dj3,\n");
  printf("                              dj5, f6, himm, knap, tt,\n");
  printf("                              tt:E+S (E=a/e/r/w/tM; S=r/f/tN),\n");
  printf("                              mcbK (K in 1..9), rr, lp, lpK,\n");
  printf("                              tsp, nk:K+Seed, const, rand\n");
  printf("   Replacement policies ..... parent, hd, hdN (N>1 default 10),"
         " worst\n");
  printf("   Mutation types ........... s, o, p (only in tsp problems),\n");
  printf("                              default: per-gene problem-specific\n");
  printf("Made available under the terms of the GNU General Public License "
         "v2.\n");
}
