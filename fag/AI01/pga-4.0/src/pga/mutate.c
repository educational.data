/*          File: mutate.c
 *  Last changed: Thu Jul 20 17:22:00 2000
 *
 *  RCS: $Id: mutate.c,v 1.1 2006-12-12 10:25:20 tobibobi Exp $
 *
 *  Mutation. mutate() takes a CHROMOSOME as argument, returns one.
 *  The function mutate() points at the appropriate mutation procedure
 *  in this file.
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "pga.h"

extern int geno_size;
extern int chromo_id;
extern int biggest_allele;
extern double mute_rate;

extern int tt_maxslots;
extern int tt_days;
extern int tt_slots_per_day;
extern int tt_spread;
extern int tt_clash_type;
extern int tt_mtn1;
extern int tt_mtn2;
extern double tt_penalty_clash;
extern double tt_penalty_order;
extern double tt_penalty_exclude;
extern double tt_penalty_consec;
extern struct constraint **tt_event_data;
extern double *tt_scores;
extern int (*find_bad_event) (GENE *);
extern int (*find_good_slot) (GENE *, int i);


extern int gen_total;

/********************************************************************
 * swap based mutation: - used in permutation based functions 
 * One locus is picked at random, and exchanges alleles with
 * an adjacent locus
 *******************************************************************/

CHROMOSOME
mutate_SBM(CHROMOSOME original, double rate)
{
  int place,swap1,swap2;
  CHROMOSOME newchromo;
  int tmp;

  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size) * sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;


  if (drandom() < rate){
    swap1 = bsd_random()%(geno_size);

    /* swap2 is and adjacent loci, randomly choose if left or right */
    /* (being careful if we have chosen an end !) */

    if (swap1 == geno_size-1)
      swap2 = swap1 -1;
    else if (swap1 == 0)
      swap2 = swap1+1;
    else if (bsd_random()%2==0)
      swap2 = swap1+1;
    else
      swap2 = swap1-1;

    tmp = original.gdp->genotype[swap1].val;
    newchromo.gdp->genotype[swap1].val = original.gdp->genotype[swap2].val;
    newchromo.gdp->genotype[swap2].val = tmp;
    
    newchromo.gdp->genotype[swap1].gen  = gen_total; 
    newchromo.gdp->genotype[swap2].gen  = gen_total; 
  }
  
  return (newchromo);
}


/* order based mutation */
/* this operator picks 2 loci at random and exchanges their alleles */

CHROMOSOME
mutate_OBM(CHROMOSOME original, double rate)
{
  int place,swap1,swap2;
  CHROMOSOME newchromo;
  int tmp;

  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size) * sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;


  if (drandom() < rate){
    swap1 = bsd_random()%(geno_size);
    swap2 = bsd_random()%(geno_size);

    while (swap2 == swap1)
      swap2 = bsd_random()%(geno_size);
    
    tmp = original.gdp->genotype[swap1].val;
    newchromo.gdp->genotype[swap1].val = original.gdp->genotype[swap2].val;
    newchromo.gdp->genotype[swap2].val = tmp;
    
    newchromo.gdp->genotype[swap1].gen  = gen_total; 
    newchromo.gdp->genotype[swap2].gen  = gen_total; 
  }
  
  return (newchromo);
}


/* position based mutation:
 * this operator deletes a randomly picked locus and puts its
 * allele to a newly inserted locus at an arbitrary position
 */

CHROMOSOME
mutate_PBM(CHROMOSOME original, double rate)
{
  int place,swap1,swap2,i;
  CHROMOSOME newchromo;
  int tmp,savedAllele;
  GENEVAL *tmpcopy;

  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size) * sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;

  tmpcopy = (GENEVAL *)MALLOC((geno_size+1)*sizeof(GENEVAL));

  if (drandom() < rate){
    swap1 = bsd_random()%(geno_size);
    swap2 = bsd_random()%(geno_size);

    while (swap2 == swap1)
      swap2 = bsd_random()%(geno_size);
   
    /* make swap1 the locus with the lowest index to make
     * things easier:
     * locus at swap1 gets deleted, reinserted at swap 2*/

    if (swap2 < swap1){
      tmp = swap1;
      swap1 = swap2;
      swap2 = tmp;
    }
 
    /* easiest wat to do this is first copy the genotpe */
    for (i=0;i<geno_size;i++)
      tmpcopy[i] = original.gdp->genotype[i].val;

    savedAllele = tmpcopy[swap1];

    /* shuffle things backwards bewteen the swap points */
    for (i=swap1;i<swap2;i++)
      newchromo.gdp->genotype[i].val = tmpcopy[i+1];

    /* now add the deleted allele back in */
    newchromo.gdp->genotype[swap2].val = savedAllele;

    /* and fill it up */
    for (i=swap2+1;i<geno_size;i++)
      newchromo.gdp->genotype[i].val = tmpcopy[i];
    
    newchromo.gdp->genotype[swap1].gen  = gen_total; 
    newchromo.gdp->genotype[swap2].gen  = gen_total; 
  }
  free(tmpcopy);
  
  return (newchromo);
}

/************************************************************************/
/* This flips a random 0/1 bit in 'original'.                               */
/* If given chromo has non-zero refcount, then work on copy instead.    */
/* Refcounts get adjusted upon insertion, later.                        */
/************************************************************************/

CHROMOSOME
mutate_01(CHROMOSOME original, double rate)
{
  int place;
  CHROMOSOME newchromo;
  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size)*sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;

  for (place = 0; place < geno_size; place++) {
    if (drandom()<= rate) {
      if (newchromo.gdp->genotype[place].val == 0)
	newchromo.gdp->genotype[place].val = 1;
      else
	newchromo.gdp->genotype[place].val = 0;
    }
    newchromo.gdp->genotype[place].gen  = gen_total;
  }

  return (newchromo);
}

/************************************************************************/
/* This changes a random allele in the original, to lie in range 0   to */
/* biggest_allele. Forces a different value to to the current one.      */
/* If given chromo has non-zero refcount, then work on copy instead.    */
/* Refcounts get adjusted upon insertion, later.                        */
/************************************************************************/

CHROMOSOME
mutate_multi(CHROMOSOME original, double rate)
{
  int place;
  char c;
  CHROMOSOME newchromo;

  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size)*sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;

  for (place = 0; place < geno_size; place++) {
    if (drandom()<= rate) {
      c = (bsd_random()% (biggest_allele + 1));
      if (biggest_allele > 0)
	while (c == newchromo.gdp->genotype[place].val)
	  c = (bsd_random()% (biggest_allele + 1));
      newchromo.gdp->genotype[place].val = c;
      newchromo.gdp->genotype[place].gen = gen_total;
    }
  }

  return (newchromo);
}

/************************************************************************/
/* Function mrate() points at one of the next two functions, depending  */
/* on whether adaptive is chosen or note. Adaptive mutation scales the  */
/* probability of mutating each bit by the degree of similarity of the  */
/* parents.                                                             */
/************************************************************************/

/************************************************************************/
/* This function returns the bit mutation rate appropriate to the given */
/* parents - a number in the range 0..mute_rate, depending on degree of */
/* similarity of parents.                                               */
/************************************************************************/

double adaptive(CHROMOSOME parent1, CHROMOSOME parent2)
{
  int loop, count;
  double prob;

  count = 0;
  for (loop = 0; loop < geno_size; loop += 1) {
    if (parent1.gdp->genotype[loop].val == parent2.gdp->genotype[loop].val)
      count++;
  }
  /* Proportion = observed / total. */
  prob = ((double) count) / ((double) geno_size);

  return (prob * mute_rate);
}


/************************************************************************/
/* This function simply returns the fixed mutation rate.                */
/************************************************************************/
#pragma argsused
double nonadaptive(CHROMOSOME parent1, CHROMOSOME parent2)
{
  return (mute_rate);
}


/***********************************************************************/
/* Timetabling: event-freeing mutation. It finds events which are      */
/* heavily contributing to the penalty, and picks one. It changes the  */
/* slot of that event to one which cuts down the contribution a lot.   */
/***********************************************************************/

/* Work out the violation score for a given event assuming it is in a */
/* given slot in the given genotype.                                  */

double violation_score(int event, int slot, GENE *genotype)
{
  double spenalty;
  int otherslot;
  struct constraint *ctmp;

  spenalty = 0.0;
  for (ctmp = tt_event_data[event];
       ctmp != (struct constraint *) NULL;
       ctmp = ctmp->next) {
    switch (ctmp->ctype) {
    case CLASH:
    case CLASH_COPY:
      otherslot = genotype[ctmp->e2].val;
      if (slot == otherslot)
	spenalty += tt_penalty_clash;
      else if (abs(slot - otherslot) <= tt_spread
	  && (slot / tt_slots_per_day) == (otherslot / tt_slots_per_day))
	spenalty += (tt_penalty_consec / (double) abs(slot - otherslot));
      break;
    case BEFORE:
      otherslot = genotype[ctmp->e2].val;
      if (slot >= otherslot)
	spenalty += tt_penalty_order;
      break;
    case EXCLUDE:
      if (slot == ctmp->s)
	spenalty += tt_penalty_exclude;
      break;
    default:
      error("PGA: unknown constraint type found when mutating\n");
      break;
    }				/* end of switch */
  }				/* end of adding up penalties for this event */
  return (spenalty);
}

/*********************************************************************/
/* The next set of functions all find an event to be mutated. Only   */
/* one of these gets used in a run; set by command-line arg -ett:..  */
/* Choices:                                                          */
/*   find_worst_event  finds an event with bad violation score by    */
/*                     using roulette-wheel selection                */
/*   find_random_event picks any one                                 */
/*   find_event_by_tn  uses tournament selection, size tt_mtn1       */
/*********************************************************************/

int find_worst_event(GENE *geno)
{
  int i;
  double penalty, p;

  /* Look at each event (=gene locus) and consider the constraints   */
  /* on it. Add up penalties, note violation score, accumulate total */
  /* violation score (CLASH and CLASH_COPY both count in this) for   */
  /* roulette selection of a gene to mutate.                         */

  penalty = 0.0;
  for (i = 0; i < geno_size; i++) {
    p = violation_score(i, geno[i].val, geno);
    penalty += p;
    tt_scores[i] = p;
  }				/* end of looping through genotype */

  /* Now we do roulette-wheel selection to choose a gene to mutate */

  /* Randomly scale down penalty */
  penalty = drandom()* penalty;
  i = 0;
  p = 0.0;
  /* Loop to find when accumulated violation scores exceed penalty */
  while (i < geno_size) {
    p += tt_scores[i];
    if (p >= penalty)		/* >= is used here, just in case penalty = 0.0 */
      break;
    else
      i++;
  }
  return (i);
}

#pragma argsused
int find_random_event(GENE *geno)
{
  return ((int) (drandom()* (double) geno_size));
}

int find_event_by_tn(GENE *geno)
{
  int event, j, e;
  double worst, penalty;
  /* Use tournament selection to find reasonably troublesome event */

  event = (int) (drandom()* (double) geno_size);
  worst = violation_score(event, geno[event].val, geno);
  for (j = 0; j < tt_mtn1 - 1; j++) {
    e = (int) (drandom()* (double) geno_size);
    penalty = violation_score(e, geno[e].val, geno);
    if (penalty > worst) {
      worst = penalty;
      event = e;
    }
  }
  return (event);
}

/*********************************************************************/
/* The next set of functions all find a decent slot for the event.   */
/* One of these gets used in a run; set by command-line arg -ett:..  */
/* Choices:                                                          */
/*   find_free_slot    finds an unconstrained slot, else random      */
/*   find_random_slot  picks any one                                 */
/*   find_slot_by_tn   uses tournament selection, size tt_mtn2       */
/*********************************************************************/

#pragma argsused
int find_free_slot(GENE *geno, int i)
{
  int *slotflags;
  int j, slot, otherslot, okslots;
  struct constraint *ctmp;

  /* What do we mutate its slot to? Preferably a slot that's */
  /* trouble-free, being not constrained at all. Otherwise   */
  /* something random.                                       */

  /* Set up an array of flags: 1 = bad slot, 0 = good slot   */
  slotflags = (int *) MALLOC(tt_maxslots * sizeof(int));
  for (slot = 0; slot < tt_maxslots; slot++)
    slotflags[slot] = 0;
  slotflags[geno[i].val] = 1;	/* Not current one!   */

  /* Now loop through constraints on event i, and mark bad   */
  /* slots.                                                  */
  for (ctmp = tt_event_data[i];
       ctmp != (struct constraint *) NULL;
       ctmp = ctmp->next) {
    switch (ctmp->ctype) {
    case CLASH:
    case CLASH_COPY:
      slotflags[geno[ctmp->e2].val] = 1;
      break;
    case BEFORE:
      otherslot = geno[ctmp->e2].val;
      for (j = otherslot; j < tt_maxslots; j++)
	slotflags[j] = 1;
      break;
    case EXCLUDE:
      slotflags[ctmp->s] = 1;
      break;
    default:
      error("PGA: mystery constraint type when mutating\n");
      break;
    }
  }

  /* Find the number of trouble-free slots. In the loop above     */
  /* some slots might be marked several times, so it's probably   */
  /* as cheap just to run through the array once more as it would */
  /* be to try to do the counting during the above loop.          */
  okslots = 0;
  for (slot = 0; slot < tt_maxslots; slot++)
    if (slotflags[slot] == 0)
      okslots++;

  if (okslots > 0) {
    /* There are some slots where there seems to be no trouble at all */
    /* Pick one .. scale number down randomly .. */
    okslots = (int) (drandom()* (double) okslots);
    /* .. and go locate it: */
    for (slot = 0; slot < tt_maxslots; slot++) {
      if (slotflags[slot] == 0) {
	if (--okslots)
	  break;
      }
    }
    /* Now slot is the new slot for event i */
  } else {
    /* No trouble-free slots here at all, pick a random one */
    slot = (int) (drandom()* (double) tt_maxslots);
  }
  free(slotflags);
  return (slot);
}

#pragma argsused
int find_random_slot(GENE *geno, int i)
{
  return ((int) (drandom()* (double) (1 + biggest_allele)));
}

int find_slot_by_tn(GENE *geno, int i)
{
  double penalty, bestp;
  int best, j, k;

  /* What do we mutate its slot to? Try some tournament selection */
  /* to find out.                                                 */

  best = (int) (drandom()* (double) (1 + biggest_allele));
  bestp = violation_score(i, best, geno);
  for (j = 0; j < tt_mtn2 - 1; j++) {
    k = (int) (drandom()* (double) (1 + biggest_allele));
    penalty = violation_score(i, k, geno);
    if (penalty < bestp) {
      bestp = penalty;
      best = k;
    }
  }
  return (best);
}

/*******************************************************************/
/* This does the actual mutation of a timetable. The mutation rate */
/* is the probability of changing one event's slot; only one event */
/* gets moved per call, to avoid deadlock. So it's really a rate   */
/* of chromosomal mutation rather than bit mutation.               */
/*******************************************************************/

CHROMOSOME
mutate_tt(CHROMOSOME original, double rate)
{
  int i, slot;
  GENE *geno = original.gdp->genotype;
  CHROMOSOME newchromo;

  /* In timetabling, rate is taken to be the chance of making a      */
  /* single mutation of a whole chromosome; it makes less sense to   */
  /* randomly alter bits than to change a bad bit to a good one.     */
  if (drandom()> rate)
    return (original);

  /* Find event to move */
  i = find_bad_event(geno);

  /* Find slot to move it to */
  slot = find_good_slot(geno, i);

  /* Is original wanted already? If so, set up new one else use original */
  /* It is not this routine's job to decide whether the resulting        */
  /* mutated chromosome is wanted at all; after all, might be very unfit */
  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size)*sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;

  newchromo.gdp->genotype[i].val = slot;
  newchromo.gdp->genotype[i].gen = gen_total;

  return (newchromo);

}

/*******************************************************************/
/* Timetable mutation in which ALL events get considered in turn,  */
/* not just one. Invoked by -ett:a+... (`a' for all). The mutation */
/* rate should obviously be adjusted suitably in this case.        */
/*******************************************************************/

CHROMOSOME
mutate_tt_all(CHROMOSOME original, double rate)
{
  int i, slot;
  GENE *geno = original.gdp->genotype;
  CHROMOSOME newchromo;

  /* Is original wanted already? If so, set up new one else use original */
  /* It is not this routine's job to decide whether the resulting        */
  /* mutated chromosome is wanted at all; after all, might be very unfit */
  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size)*sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;

  for(i=0; i < geno_size; i++) {
    if(drandom() <= rate) {
      /* Find slot to move it to */
      slot = find_good_slot(geno, i);
      /* ..and move it. */
      newchromo.gdp->genotype[i].val = slot;
      newchromo.gdp->genotype[i].gen = gen_total;
    }
  }
  
  return (newchromo);

}

/*******************************************************************/
/* Timetable mutation in which EACH events get considered in turn, */
/* not just one. Invoked by -ett:e+... (`e' for each). This is a   */
/* variant of the above in which the rate applied to each gene is  */
/* affected by the extent to which it causes constraint violations */
/*******************************************************************/

CHROMOSOME
mutate_tt_each(CHROMOSOME original, double rate)
{
  int i, slot;
  double penalty, p;
  GENE *geno = original.gdp->genotype;
  CHROMOSOME newchromo;

  /* Is original wanted already? If so, set up new one else use original */
  /* It is not this routine's job to decide whether the resulting        */
  /* mutated chromosome is wanted at all; after all, might be very unfit */
  if (original.gdp->refcount > 0) {
    newchromo.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    newchromo.gdp->refcount = 0;
    newchromo.gdp->genotype = (GENE *) MALLOC((geno_size)*sizeof(GENE));
    newchromo.gdp->id = chromo_id++;
    newchromo.gdp->parent1 = original.gdp->parent1;
    newchromo.gdp->parent2 = original.gdp->parent2;
    genecpy(newchromo.gdp->genotype, original.gdp->genotype, geno_size);
  } else
    newchromo = original;

  penalty = 0.0;
  for (i = 0; i < geno_size; i++) {
    p = violation_score(i, geno[i].val, geno);
    penalty += p;
    tt_scores[i] = p;
  }				/* end of looping through genotype */
      /* Just in case user continues after finding a winner: */
  if(penalty == 0.0) penalty = 1.0;
  
  for(i=0; i < geno_size; i++) {
    if(drandom() <= rate*(tt_scores[i]+1.0)/penalty) {
      /* Find slot to move it to */
      slot = find_good_slot(geno, i);
      /* ..and move it. */
      newchromo.gdp->genotype[i].val = slot;
      newchromo.gdp->genotype[i].gen = gen_total;
    }
  }
  
  return (newchromo);

}



