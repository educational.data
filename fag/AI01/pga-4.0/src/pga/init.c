/*          File: init.c
 *  Last changed: Thu Jul 20 17:16:30 2000
 *
 *  RCS: $Id: init.c,v 1.1 2006-12-12 10:25:20 tobibobi Exp $
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdlib.h>
#include <stdio.h>		/* Include the IO and maths header files. */
#include <math.h>

#include "pga.h"		/* defines genodata and chromosome structs */

/* PGA-specific stuff */
extern int gen_total;
extern int chromo_id;
extern int ss_task;
extern int geno_size;
extern int biggest_allele;
extern double oversizing;
extern int mcb_range;
extern double (*evaluate)(GENE *);
extern double *total_abs_fitness;
extern int permutation; /* true if it's a permutation encoding */


/************************************************************************/
/* This initialises a chromosome of random 0s and 1s and .. up to range */
/* INCLUSIVE. So range = 1 for 0s and 1s, range=2 for 0/1/2 etc         */
/************************************************************************/

void init_normal_genotype(GENODATA * gdp, int range)
{
  int i;

  gdp->refcount = 1;

  gdp->parent1 = gdp->parent2 = 0;
  gdp->genotype = (GENE *) MALLOC((geno_size)*sizeof(GENE));
  for (i = 0; i < geno_size; i++){
    gdp->genotype[i].val = (bsd_random()%(range + 1));
    gdp->genotype[i].gen = gen_total;
    gdp->genotype[i].chromoid = chromo_id;
  }

  gdp->id = chromo_id++;
}

/************************************************************************/
/* This initialises a permutation, with values from 0 to geno_size-1    */
/* inclusive. range should be the same as geno_size!                    */
/************************************************************************/

void init_permute_genotype(GENODATA * gdp, int range)
{
  int i, j;
  GENEVAL tmp;
  
  gdp->refcount = 1;
  gdp->parent1 = gdp->parent2 = 0;
  gdp->genotype = (GENE *) MALLOC((geno_size)*sizeof(GENE));
  for(i = 0; i < geno_size; i++) {
        /* First set value of gene i = i (etc) */
    gdp->genotype[i].val = i+1;
    gdp->genotype[i].gen = gen_total;
    gdp->genotype[i].chromoid = chromo_id;
  }
  for(i = 0; i < geno_size; i++) {
    j = bsd_random()%geno_size;
            /* and now swap values of genes i and j */
    tmp = gdp->genotype[j].val;
    gdp->genotype[j].val = gdp->genotype[i].val;
    gdp->genotype[i].val = tmp;
  }

  gdp->id = chromo_id++;
}

    

/************************************************************************/
/* This creates and randomly initialises the chromosome populations     */
/************************************************************************/

CHROMOSOME * init_pop(int popN, int numchromo)
{
  int i, j, chromo, pop, numtmp;
  CHROMOSOME c;
  CHROMOSOME *the_pop;
  CHROMOSOME *poptmp;
  
      /* Set up place for oversize temporary population */
  numtmp = (int) (oversizing * (double)numchromo);
  poptmp = (CHROMOSOME *) MALLOC(numtmp * sizeof(CHROMOSOME));

      /* Set up place where the real pop will live */
  the_pop = (CHROMOSOME *) MALLOC(numchromo * sizeof(CHROMOSOME));

  total_abs_fitness[popN] = 0.0;
      /* For this pop, invent numtmp random ones: */
  for(i = 0; i < numtmp; i++) {
    poptmp[i].gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    if(permutation)
      init_permute_genotype(poptmp[i].gdp, biggest_allele);
    else
      init_normal_genotype(poptmp[i].gdp, biggest_allele);
    poptmp[i].fitness = evaluate(poptmp[i].gdp->genotype);
  }
      /* Sort the lot of them if not ssgen or ssone: */
  if(!ss_task)  qsort(poptmp, numtmp, sizeof(CHROMOSOME), compchromo);
      /* Copy the first numchromo of them: */
  for(i = 0; i < numchromo; i++) {
    the_pop[i] = poptmp[i];
    total_abs_fitness[popN] += fabs(poptmp[i].fitness);
  }

  free(poptmp);
  
  return (the_pop);
}




