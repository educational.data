/*          File: eval.c
 *  Last changed: Wed Oct 24 12:37:01 2001
 *
 *  RCS: $Id: eval.c,v 1.1 2006-12-12 10:25:20 tobibobi Exp $
 *
 *  Defines the problems: each eval_whatever takes a genotype pointer
 *  as argument, returns the fitness as a double.
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "pga.h"
#include "nkland.h"

#define TRUE 1
#define FALSE 0

TSPCITY  *tspCity;
extern int nCities;

extern int evals;
extern int geno_size;
extern int pause_first;
extern void (*decode) (GENE *, int, double, double, double *);
extern char *datafilename;
/* Random */
extern long *rng_tbl;
extern double drandom(void);
extern void bsd_srandom(unsigned);
/* Knapsack stuff: */
extern long *sticks;
extern long target;
/* NK landscape stuff, Terry Jones' implementation */
extern NK_LANDSCAPE *nkl;
/* Royal road stuff: */
extern char *rr_blockarray;
extern int rr_blocks;
extern int usegray;
extern int rr_blocksize;
extern int rr_gap;
extern int rr_mstar;
extern double rr_ustar;
extern double rr_u;
extern double rr_v;
/* Timetable stuff: */
extern int biggest_allele;
extern int tt_maxevents;
extern int tt_maxslots;
extern int tt_days;
extern int tt_slots_per_day;
extern int tt_spread;
extern int tt_clash_type;
extern double tt_penalty_clash;
extern double tt_penalty_order;
extern double tt_penalty_exclude;
extern double tt_penalty_consec;
extern struct constraint **tt_event_data;
extern struct constraint *tt_presets;
/* LP stuff */
extern double lpmax;

/************************************************************************/
/* This returns the number of bits set to one in 'genotype'. It is used */
/* to evaluate a chromosomes fitness.                                   */
/************************************************************************/

double eval_max(GENE *genotype)
{
  int value, loop;

  evals += 1;

  value = 0;
  for (loop = 0; loop < geno_size; loop++) {	/* Check each bit in turn. */
    if (genotype[loop].val == 1)
      value += 1;
  }

  return ((double) value);
}


/************************************************************************/
/* De Jong's F1. Max fitness = 100                                      */
/************************************************************************/

double eval_dj1(GENE *genotype)
{
  int i;
  double x[3];

  evals += 1;

  decode(genotype, 3, -5.12, 5.12, x);
  for(i=0;i<3;i++) x[i] = x[i]-0.053;
  
  return 100.0 - (x[0] * x[0] + x[1] * x[1] + x[2] * x[2]);
}


/************************************************************************/
/* De Jong's F2. Max fitness = 1000                                     */
/************************************************************************/

double eval_dj2(GENE *genotype)
{
  int i;
  double x[2];

  evals += 1;

  decode(genotype, 2, -2.048, 2.048, x);
  for(i=0;i<2;i++) x[i] = x[i]-0.053;

  return 1000.0 - (100.0 * (x[0] * x[0] - x[1]) * (x[0] * x[0] - x[1]) + (1 - x[0]) * (1 - x[0]));
}


/************************************************************************/
/* De Jong's F3. Max fitness = 55                                       */
/************************************************************************/

double eval_dj3(GENE *genotype)
{
  int i;
  double x[5];

  evals += 1;

  decode(genotype, 5, -5.12, 5.12, x);
  for(i=0;i<5;i++) x[i] = x[i]-0.053;

  return 25.0 - (floor(x[0]) + floor(x[1]) + floor(x[2]) + floor(x[3]) + floor(x[4]));
}


/************************************************************************/
/* De Jong's F5. Max fitness = 500                                      */
/************************************************************************/

static int a[2][25] =
{
  {
    -32, -16,  0, 16, 32, -32, -16,  0, 16, 32, -32, -16,  0, 16, 32,
    -32, -16,  0, 16, 32, -32, -16,  0, 16, 32},
  {
    -32, -32, -32, -32, -32, -16, -16, -16, -16, -16, 0, 0, 0, 0, 0,
    16, 16, 16, 16, 16, 32, 32, 32, 32, 32}
};

double eval_dj5(GENE *genotype)
{
  double x[2], total, lowtot, prod;
  int i, j, power;

  evals += 1;

  decode(genotype, 2, -65.536, 65.536, x);
  for(i=0;i<2;i++) x[i] = x[i]-0.053;

  total = 0.002;
  for (j = 0; j < 25; j += 1) {
    lowtot = 1.0 + (double) j;
    for (i = 0; i < 2; i += 1) {
      prod = 1.0;
      for (power = 0; power < 6; power += 1) {
	prod *= x[i] - a[i][j];
      }
      lowtot += prod;
    }
    total += 1.0 / lowtot;
  }

  return 500.0 - (1.0 / total);
}

double eval_f6(GENE *genotype)
{
  int i;
  double x[2], temp, temp2, temp3;

  evals += 1;

  decode(genotype, 2, -100.0, 100.0, x);
  for(i=0;i<2;i++) x[i] = x[i]-0.053;

  temp = (x[1] * x[1] + x[0] * x[0]);
  temp2 = sin(sqrt(temp));
  temp3 = 1.0 + 0.001 * temp * temp;
  
  return (0.5 - (((temp2 * temp2)-0.5) / (temp3 * temp3)));
}

double eval_himmelblau(GENE *genotype)
{
  int i;
  double x[2], t1, t2, t;
  

  evals += 1;

  decode(genotype, 2, -6.0, 6.0, x);
  for(i=0;i<2;i++) x[i] = x[i]-0.053;

  t1 = (x[0]*x[0] + x[1] - 11.0);
  t2 = (x[0] + x[1]*x[1] - 7.0);
  t = 200.0 - t1*t1 - t2*t2;
  
  return ((t > 0.0)? t : 0.0);
}

double eval_const(GENE *genotype)
{
  return(1.0);
}

/* Rand assigns a random value in 0..1 to a chromosome.      */
/* But the same chromosome should get the same random number */
/* each time                                                 */
double eval_rand(GENE *genotype)
{
  int bits[30];
  int i;
  unsigned seed;
  long savetbl[32];
  double fit;

  evals += 1;
  
  for(i=0; i<30; i++) bits[i] = 0;
      /* set up a 30-bit bit-array by cyclic XORing */
  for(i=0; i<geno_size; i++) {
    if(genotype[i].val == 1) {
      bits[i%30] = (bits[i%30]==1)? 0:1;
    }
  }
     /* Now work out the seed value from those 30 bits */
  seed = 0;
  for(i=0;i<30;i++) {
    seed = 2*seed + bits[i];
  }
     /* MUST save RNG state info, because eval gets called   */
     /* right after RNG gets called to create the chromosome */
     /* during initialisation.                               */
  for(i=0; i<32; i++)
    savetbl[i] = rng_tbl[i];
  bsd_srandom(seed);
  fit = drandom();
     /* Now restore the saved RNG info, so the RNG is not    */
     /* disturbed by that artificial seeding just above.     */
  for(i=0; i<32; i++)
    rng_tbl[i] = savetbl[i];
  return(fit);
}

double eval_mcb(GENE *genotype)
{
  int i;
  int best_score = 0;
  int score = 0;
  char c;

  evals += 1;

  for (c = genotype[0].val, i = 0; i < geno_size; i++) {
    if (genotype[i].val == c) {
      score++;
      if (score > best_score)
	best_score = score;
    } else {
      score = 1;
      c = genotype[i].val;
    }
  }

  return ((double) best_score);
}

double eval_knap(GENE *genotype)
{
  int i;
  long sum = 0;
  double fit;

  evals += 1;

  for (i = 0; i < geno_size; i++) {
    if (genotype[i].val == 1)
      sum += sticks[i];
  }
  fit = 1.0 / (1.0 + fabs((double) target - (double) sum));
  return (fit);
}

double eval_nk(GENE *genotype)
{
  evals += 1;
  return(nk_fitness(genotype, nkl));
}

/**************************************************************/
/* Long-path problem, see Horn/Goldberg/Deb paper in PPSN3    */
/* NB: geno_size will already have been coerced to be odd.    */
/**************************************************************/
#define ZERO 0
#define ONE  1
double lp_val(GENE *g, int len)
{
  int i;
  double fit = 0.0;
  if(len == 1) {
    fit = (g[0].val==ZERO? 0.0 : 1.0);
  } else if(g[0].val == g[1].val) {
    fit = lp_val(g+2,len-2);
    if(g[0].val == ONE) {
      if(fit < 0.0) {
        return(fit);
      } else {
        fit = 3.0*pow(2.0,(double)((len-1)>>1))-2.0-fit;
      }
    }
  } else if(len == 3 && g[0].val == 1
                     && g[1].val == 0
                     && g[2].val == 1) {
    fit = 2.0;
  } else if(len > 4 &&  g[0].val == 1
                     && g[1].val == 0
                     && g[2].val == 1
                     && g[3].val == 1) {
    for(i=4; i<len && g[i].val==ZERO; i++);
    if(i == len) {
      fit = 3.0*pow(2.0,(double)(((len-1)>>1)-1))-1.0;
    } else {
      fit = -1.0;
    }
  } else {
    fit = -1.0;
  }
  return(fit);
}
  
double eval_lp(GENE *genotype)
{
  int i, j;
  double fit;

  evals +=1;

  if((fit = lp_val(genotype, geno_size)) < 0.0) {
    j = 0;
    for(i=0; i < geno_size; i++) {
      if(genotype[i].val == ZERO) j++;
    }
    fit = (double)j;
  } else {
    fit = fit + (double)geno_size;
  }
  return(fit);
}

double eval_lp2(GENE *genotype)
{
  int i, j;
  double fit;

  evals +=1;

  fit = lp_val(genotype, geno_size);
  if((fit < 0.0) || (fit+(double)geno_size > lpmax)) {
    j = 0;
    for(i=0; i < geno_size; i++) {
      if(genotype[i].val == ZERO) j++;
    }
    fit = (double)j;
  } else {
    fit = fit + (double)geno_size;
  }
  return(fit);
}

/***************************************************************/
/* Get the next number from the input: put it in the location  */
/* addressed by second argument. This function returns 0 on    */
/* EOF. If stopateol is true, it returns -1 when it hits \n    */
/* (after which some other procedure has to read past the \n), */
/* otherwise it continues looking for the next number.         */
/* A number has an optional sign, perhaps followed by digits,  */
/* perhaps followed by a decimal point, perhaps followed by    */
/* more digits. There must be a digit somewhere for it to count*/
/* as a number. So it would read any of:                       */
/*  -.5                                                        */
/*  -0.5                                                       */
/*  -.5.7                                                      */
/* as minus-a-half. In the last case, it would read .7 next    */
/* time around.                                                */
/*   There doesn't seem to be a neat and reliable way to do    */
/* all this, including stopateol, using scanf?                 */
/***************************************************************/

double getdouble(FILE * file, double *valaddr, int stopateol)
{
  int c;
  int found = FALSE, indecimal = FALSE;
  int sign = +1;
  double n = 0.0, p = 1.0;

  /* First find what looks like start of a number - the first digit. */
  /* And note any sign and whether we just passed a decimal point.   */
  do {
    c = fgetc(file);
    if (c == EOF)
      return (0);
    else if (stopateol && c == '\n')
      return (-1);
    else if (c == '+' || c == '-') {
      sign = (c == '+') ? +1 : -1;
      c = fgetc(file);
      if (c == EOF)
	return (0);
      else if (stopateol && c == '\n')
	return (-1);
    }
    if (c == '.') {
      indecimal = TRUE;
      c = fgetc(file);
      if (c == EOF)
	return (0);
      else if (stopateol && c == '\n')
	return (-1);
    }
    if (c >= '0' && c <= '9') {
      found = TRUE;
    } else {
      sign = +1;
      indecimal = FALSE;
    }
  } while (!found);

  /* Now we've got digit(s) ... */
  do {
    n = 10.0 * n + c - '0';
    p = 10.0 * p;
    c = fgetc(file);

    if ((c < '0') || (c > '9')) {
      found = FALSE;
      /* We've run out. If we already saw a decimal point, return now */
      if (indecimal) {
	if (c != EOF)
	  ungetc(c, file);
	*valaddr = sign * n / p;
	return (1);
      } else
	p = 1.0;
    }
  } while (found);

  /* We ran out and we didn't see a decimal point, so is this a decimal? */
  if (c != '.') {
    /* No, give it back to caller */
    if (c != EOF)
      ungetc(c, file);
    *valaddr = sign * n;
    return (1);
  } else {
    /* It is. Step past it, carry on hoping for more digits */
    c = fgetc(file);
    while (c >= '0' && c <= '9') {
      n = 10.0 * n + c - '0';
      p = p * 10.0;
      c = fgetc(file);
    }
    /* We've run out of digits but we have a number to give */
    if (c != EOF)
      ungetc(c, file);
    *valaddr = sign * n / p;
    return (1);
  }
}

/* Use getdouble() above but convert result to int. */
int getint(FILE * f, int *valaddr, int stopateol)
{
  int r;
  double x;
  r = getdouble(f, &x, stopateol);
  *valaddr = (int) x;
  return (r);
}

/* Use getdouble above but convert result to long. */
int getlong(FILE * f, long *valaddr, int stopateol)
{
  int r;
  double x;
  r = getdouble(f, &x, stopateol);
  *valaddr = (long) x;
  return (r);
}


/* For knapsack problem, must read integers from file `weights' */
int read_weights_file(void)
{
  FILE *f;
  int i;
  long t;

  /* read target and sizes from file "weights" if it exists readable */
  if ((f = fopen(WEIGHTSFILE, "r")) != (FILE *) NULL) {
    if (!getlong(f, &target, FALSE)) {
      fclose(f);
      return (0);
    }
    i = 0;
    while (i < geno_size && (getlong(f, &t, FALSE))) {
      sticks[i++] = t;
    }
    fclose(f);
    return (1);
  } else
    return (0);
}

double eval_rr(GENE *genotype)
{
  double score = 0.0;
  int total, i, j, index, n, proceed;

  evals += 1;

  n = 0;
  /* Do lowest-level blocks */
  for (i = 0; i < rr_blocks; i++) {	/* run through each low-level block */
    total = 0;
    for (j = i * (rr_blocksize + rr_gap);
	 j < i * (rr_blocksize + rr_gap) + rr_blocksize; j++)
      if (genotype[j].val == 1)
	total++;		/* count bits in block */
    if (total > rr_mstar && total < rr_blocksize)
      score -= (total - rr_mstar) * rr_v;
    else if (total <= rr_mstar)
      score += total * rr_v;
    if (total == rr_blocksize) {
      rr_blockarray[i] = 1;
      n++;
    } else
      rr_blockarray[i] = 0;
  }

  /* Bonus for filled low-level blocks */
  if (n > 0)
    score += rr_ustar + (n - 1) * rr_u;

  /* Do higher-level  blocks */
  n = rr_blocks;		/* n counts no. of lower-level blocks */
  proceed = TRUE;		/* worth looking at next higher level? */
  while ((n > 1) && proceed) {
    proceed = FALSE;
    total = 0;
    /* there are n valid blocks in the blockarray each time */
    /* round, so n=2 is the last.                           */
    for (i = 0, index = 0; i < (n / 2) * 2; i += 2, index++) {
      if (rr_blockarray[i] == 1 && rr_blockarray[i + 1] == 1) {
	total++;
	proceed = TRUE;
	rr_blockarray[index] = 1;
      } else
	rr_blockarray[index] = 0;
    }
    if (total > 0)
      score += rr_ustar + (total - 1) * rr_u;
    n /= 2;
  }
  return (score);
}

void read_rrdata_file(void)
{
  FILE *f;

  /* read RR data from file if it exists; else use defaults */
  if ((f = fopen(datafilename, "r")) != (FILE *) NULL) {
    if (getint(f, &rr_blocksize, FALSE)
	&& getint(f, &rr_gap, FALSE)
	&& getint(f, &rr_mstar, FALSE)
	&& getdouble(f, &rr_ustar, FALSE)
	&& getdouble(f, &rr_u, FALSE)
	&& getdouble(f, &rr_v, FALSE));
  }
  rr_blocks = geno_size / (rr_blocksize + rr_gap);
}

/************************************************************************/
/* Treat bit pattern as though it were a Gray code, recover integer.    */
/* This algorithm courtesy of Achille Hui, Stanford Physics Dept.       */
/* bin2gray(b) is just b^(b>>1) as usual: the `reflected' Gray code so  */
/* widely used. Use of the `reflected' Gray code of course ensures that */
/* a number and its Gray code version occupy the same width.            */
/************************************************************************/

unsigned long gray2bin(unsigned long g)
{
  unsigned long t = g;
  t ^= t >> 1;
  t ^= t >> 2;
  t ^= t >> 4;
  t ^= t >> 8;
  t ^= t >> 16;
  return t;
}

/************************************************************************/
/* This decodes the requested number of real parameters from the given  */
/* chromosome.                                                          */
/************************************************************************/

void long_decode(GENE *genotype, int number, double lower, double upper, double *array)
{
  int loop, count, bits;
  unsigned long value;

  bits = (geno_size / number);

  for (loop = 0; loop < number; loop += 1) {
    value = 0;
    for (count = 0; count < bits; count++) {
      value = 2 * value + (int) (genotype[count + loop * bits].val);
    }

    if (usegray)
      value = gray2bin(value);

    array[loop] = ((upper - lower) *
                   ((double) value) / pow(2.0, (double) bits)) + lower;
  }
}

void double_decode(GENE *genotype, int number, double lower, double upper, double *array)
{
  int loop, count, bits;
  double value;

  bits = (geno_size / number);

  for (loop = 0; loop < number; loop += 1) {
    value = 0.0;
    for (count = 0; count < bits; count++) {
      value = 2 * value + (double) (genotype[count + loop * bits].val);
    }
    array[loop] = ((upper - lower) * (value) / pow(2.0, (double) bits)) + lower;
  }
}


/***********************************************************************/
/* Timetabling stuff. The problem is defined in file ttdata, the       */
/* chromosome represents an array of the events to be placed, the      */
/* value of an allele is the slot into which that event is placed.     */
/* Fitness is based on a penalty function (1/(1+totalPenalty)) and     */
/* the separate penalties are also given via file ttdata.              */
/*   This is NOT state-of-the-art in using GAs for timetabling. It     */
/* just happens to fit well with the PGA framework, and has worked     */
/* well (= better than humans) on real, moderate-sized problems.       */
/***********************************************************************/

void check_event_spec(void)
{
  if (geno_size == 0) {
    error("PGA: in %s: must state number of events, before first constraint\n",
	   datafilename);
  }
}

/* This procedure reads the timetable data file and records the details */

#define MAXSLOTS 78
struct tt_params tt_paramlist[]=
{
  {"events", DATA_EVENTS, 1},
  {"slots", DATA_SLOTS, 1},
  {"days", DATA_DAY, 1},
  {"near-clash", DATA_SPREAD, 1},
  {"clash-type", DATA_CLASHTYPE, 1},
  {"p-clash", PENALTY_CLASH, 1},
  {"p-order", PENALTY_ORDER, 1},
  {"p-exclude", PENALTY_EXCLUDE, 1},
  {"p-near", PENALTY_NEARCLASH, 1},
  {"separate", CONSTRAIN_CLASH, ALL},
  {"order", CONSTRAIN_BEFORE, ALL},
  {"exclude", CONSTRAIN_EXCLUDE, ALL},
  {"preset", CONSTRAIN_PRESET, 2},
  {NULL, END_LIST, 0}
};

int read_tt_datafile(void)
{
  FILE *f;
  char buffer[1024];
  double *data;
  struct constraint *ctmp;
  struct tt_params *paramp;
  int i, j, k, n, d0, d1, data_max;

  if ((f = fopen(datafilename, "r")) == (FILE *) NULL) {
    warn("PGA: could not open timetable data file %s\n",
	    datafilename);
    return (0);
  }
  data_max = CHUNK;
  data = (double *) MALLOC(sizeof(double) * data_max);

  geno_size = 0;		/* Must change this when reading data! 0 is a flag */

  while (fscanf(f, " %s", buffer) != EOF) {
    /* We have a non-whitespace string, do we know it? */
    for (paramp = tt_paramlist;
	 (paramp->param_type != END_LIST)
	 && strncmp(paramp->param, buffer, strlen(buffer));
	 paramp++);

    if (paramp->param_type == END_LIST) {	/* If we don't.. */
      fscanf(f, "%*[^\n]");	/* skip to end of line */
    } else {			/* Else we know it .. */
      if (paramp->args_wanted != ALL) {	/* Gather the numbers */
	if (paramp->args_wanted > data_max) {
	  data_max = paramp->args_wanted;
	  if ((data = (double *) realloc(data, sizeof(double) * data_max))
	      == (double *) NULL) {
	    error("PGA: could not realloc reading %s\n", datafilename);
	  }
	}
	for (n = 0; n < paramp->args_wanted; n++) {	/* read the numbers wanted */
	  if (!getdouble(f, &data[n], FALSE)) {
	    warn("PGA: unexpected EOF in timetable data\n");
	    return (0);
	  }
	}
      } else {			/* Gather ALL the numbers on this line */
	n = 0;
	while ((j = getdouble(f, &data[n], TRUE)) > 0) {
	  /* Now increment n .. error to do it in above line, since */
	  /* getdouble will be called once too often (at EOL)       */
	  n++;
	  /* Will we need more space? */
	  if (n == data_max) {
	    data_max += CHUNK;
	    if ((data = (double *) realloc(data, sizeof(double) * data_max))
		== (double *) NULL) {
	      error("PGA: failed to realloc reading %s\n",
		    datafilename);
	    }
	  }
	}
      }				/* End of number gathering */
      /* NB: next scan for a keyword using fscanf above will go past */
      /* any further newlines, even if we did getdouble(..TRUE)      */
      /* At this point, n contains the number of numbers read in for */
      /* the current item.                                           */

      /* d0 and d1 are used purely to make the code a little easier to read */
      d0 = (int) data[0];
      d1 = (int) data[1];

      /* Store the data according to what it is: */
      switch (paramp->param_type) {
      case DATA_EVENTS:
	geno_size = tt_maxevents = d0;
	/* Now we know how many events, set up the array where the */
	/* constraint chains will be rooted */
	tt_event_data =
	    (struct constraint **) MALLOC(geno_size * sizeof(struct constraint *));
	/* and initialise each to be empty chain */
	for (i = 0; i < geno_size; i++)
	  tt_event_data[i] = (struct constraint *) NULL;
	break;
      case DATA_SLOTS:
	tt_maxslots = d0;
	/* Now we know how many timetable slots there are, we know */
	/* how big alleles can be: */
	biggest_allele = d0 - 1;
	if (tt_maxslots > MAXSLOTS) {
	  warn("PGA: not allowed more than %d slots\n",MAXSLOTS);
	  return (0);
	}
	break;
      case DATA_DAY:
	tt_days = d0;
	break;
      case DATA_SPREAD:
	tt_spread = d0;
	break;
      case DATA_CLASHTYPE:
	if (d0 < 1 || d0 > 3) {
	  warn("PGA: %s must be 1, 2 or 3\n", paramp->param);
	  return (0);
	}
	tt_clash_type = d0;
	break;
      case PENALTY_CLASH:
	tt_penalty_clash = data[0];
	break;
      case PENALTY_ORDER:
	tt_penalty_order = data[0];
	break;
      case PENALTY_EXCLUDE:
	tt_penalty_exclude = data[0];
	break;
      case PENALTY_NEARCLASH:
	tt_penalty_consec = data[0];
	break;
      case CONSTRAIN_CLASH:
	check_event_spec();
/*	if (n < 2) {
	  warn("PGA: expected at least 2 numbers in a `%s' constraint\n",
		paramp->param);
	  return (0);
          } */
	for (i = 0; i < n - 1; i++) {
	  for (j = i + 1; j < n; j++) {
	    d0 = data[i];
	    d1 = data[j];
	    if (d0 == d1) {
	      warn("PGA: cannot separate event %d from itself\n",d0);
	      pause_first = TRUE;	/* Not a fatal mistake */
	      break;		/* Give up on this constraint */
	    } else if (d0 > d1) {	/* Store clash with lower-numbered event */
	      k = d0;
	      d0 = d1;
	      d1 = k;
	    }
	    /* Check to see if we have it already */
	    for (ctmp = tt_event_data[d0];
		 ctmp != (struct constraint *) NULL
		 && !(ctmp->ctype == CLASH
		      && ctmp->e1 == d0
		      && ctmp->e2 == d1);
		 ctmp = ctmp->next);
	    /* If we got through that loop to ctmp==NULL, it's new */
	    if (ctmp != (struct constraint *) NULL) {
	      if (tt_clash_type == 1) {
		warn("PGA: separate events %d and %d: already noted\n",d0,d1);
		pause_first = TRUE;	/* Not a fatal mistake */
	      } else if (tt_clash_type == 3)
		ctmp->cpenalty += tt_penalty_clash;
	    } else {		/* New constraint */
	      ctmp = (struct constraint *) MALLOC(sizeof(struct constraint));
	      if (d0 < geno_size)
		ctmp->e1 = d0;
	      else {
		warn("PGA: clash constraint: no such event as %d\n",d0);
		return (0);
	      }
	      if (d1 < geno_size)
		ctmp->e2 = d1;
	      else {
		warn("PGA: clash constraint: no such event as %d\n",d1);
		return (0);
	      }
	      ctmp->ctype = CLASH;
	      ctmp->cpenalty = tt_penalty_clash;
	      ctmp->next = tt_event_data[d0];
	      tt_event_data[d0] = ctmp;
	      /* Keep a copy with other event: ignore copy when computing   */
	      /* penalties. The copy is useful when checking for duplicates */
	      /* or if implementing `smart' mutation.                       */
	      ctmp = (struct constraint *) MALLOC(sizeof(struct constraint));
	      ctmp->e1 = d1;
	      ctmp->e2 = d0;
	      ctmp->ctype = CLASH_COPY;
	      ctmp->cpenalty = tt_penalty_clash;
	      ctmp->next = tt_event_data[d1];
	      tt_event_data[d1] = ctmp;
	    }			/* end of installing new constraint */
	  }			/* end of j loop */
	}			/* end of i loop */
	break;
      case CONSTRAIN_BEFORE:
	check_event_spec();
/*	if (n < 2) {
	  warn("PGA: expected at least 2 numbers in a `%s' constraint\n",
		paramp->param);
	  return (0);
          } */
	for (i = 1; i < n; i++) {
	  d1 = data[i];
	  /* Check to see if we have it already */
	  for (ctmp = tt_event_data[d0];
	       ctmp != (struct constraint *) NULL
	       && !(ctmp->ctype == BEFORE
		    && ctmp->e1 == d0
		    && ctmp->e2 == d1);
	       ctmp = ctmp->next);
	  /* If we got through that loop to ctmp==NULL, it's new */
	  if (ctmp != (struct constraint *) NULL) {
	    warn("PGA: order event %d before %d: already noted\n",d0,d1);
	    pause_first = TRUE;	/* Not a fatal mistake */
	    continue;		/* Give up on this constraint */
	  }
	  ctmp = (struct constraint *) MALLOC(sizeof(struct constraint));
	  if (d0 < geno_size)
	    ctmp->e1 = d0;
	  else {
	    warn("PGA: before constraint: no such event as %d\n",d0);
	    return (0);
	  }
	  if (d1 < geno_size)
	    ctmp->e2 = d1;
	  else {
	    warn("PGA: before constraint: no such event as %d\n",d1);
	    return (0);
	  }
	  ctmp->ctype = BEFORE;
	  ctmp->cpenalty = tt_penalty_order;
	  ctmp->next = tt_event_data[d0];
	  tt_event_data[d0] = ctmp;
	}
	break;
      case CONSTRAIN_EXCLUDE:
	check_event_spec();
/*	if (n < 2) {
	  error("PGA: expected at least 2 numbers in a `%s' constraint\n",
		paramp->param);
          } */
	for (i = 1; i < n; i++) {
	  d1 = data[i];
	  /* Check to see if we have it already */
	  for (ctmp = tt_event_data[d0];
	       ctmp != (struct constraint *) NULL
	       && !(ctmp->ctype == EXCLUDE
		    && ctmp->e1 == d0
		    && ctmp->s == d1);
	       ctmp = ctmp->next);
	  /* If we got through that loop to ctmp==NULL, it's new */
	  if (ctmp != (struct constraint *) NULL) {
	    warn("PGA: exclude event %d from slot %d: already noted\n",d0,d1);
	    pause_first = TRUE;	/* Not a fatal mistake */
	    continue;		/* Give up on this constraint */
	  }
	  ctmp = (struct constraint *) MALLOC(sizeof(struct constraint));
	  if (d0 < geno_size)
	    ctmp->e1 = d0;
	  else {
	    warn("PGA: exclude constraint: no such event as %d\n",d0);
	    return (0);
	  }
	  if (d1 < tt_maxslots)
	    ctmp->s = d1;
	  else {
	    warn("PGA: exclude constraint: no such slot as %d\n",d1);
	    return (0);
	  }
	  ctmp->ctype = EXCLUDE;
	  ctmp->cpenalty = tt_penalty_exclude;
	  ctmp->next = tt_event_data[d0];
	  tt_event_data[d0] = ctmp;
	}
	break;
      case CONSTRAIN_PRESET:
	check_event_spec();
	ctmp = (struct constraint *) MALLOC(sizeof(struct constraint));
	if (d0 < geno_size)
	  ctmp->e1 = d0;
	else {
	  warn("PGA: preset constraint: no such event as %d\n",d0);
	  return (0);
	}
	if (d1 < tt_maxslots)
	  ctmp->s = d1;
	else {
	  warn("PGA: preset constraint: no such slot as %d\n",d1);
	  return (0);
	}
	ctmp->ctype = PRESET;
	ctmp->next = tt_presets;
	tt_presets = ctmp;
	break;
      default:
	warn("PGA: uncatered-for type in timetable data\n");
	return (0);
      }				/* end of param switch */
    }				/* end of dealing with one param */
  }				/* end of reading file */

  fclose(f);
  free(data);

  if (tt_days == 0) {
    error("PGA: cannot have 0 days in timetable\n");
  } else {
    tt_slots_per_day = (tt_maxslots + tt_days - 1) / tt_days;
    if (tt_slots_per_day * (tt_days - 1) >= tt_maxslots) {
      warn("NOTE: %d slots over %d days means %d slots per day\n",
	    tt_maxslots, tt_days, tt_slots_per_day);
      warn("      so there are only %d days (%d %s on the last)\n",
	    (tt_maxslots + tt_slots_per_day - 1) / tt_slots_per_day,
	    tt_maxslots % tt_slots_per_day,
	    ((tt_maxslots % tt_slots_per_day) == 1 ? "slot" : "slots"));
      pause_first = TRUE;
    }
  }
  return (1);
}

/* In the timetabling problem, a chromosome represents an array */
/* of the events that must be in the timetable. The value for   */
/* a given event (ie the allele for a given locus) is the slot  */
/* in the timetable when that event happens.                    */
/*   To evaluate a chromosome, scan it: for each event E, look  */
/* at the constraints in the chain rooted at tt_event_data[E]   */
/* and for those which are violated, increase the penalty as    */
/* appropriate. Final fitness is 1/(1+penalty)                  */

double eval_tt(GENE *genotype)
{
  int i, slot, otherslot;
  double penalty = 0.0;
  struct constraint *ctmp;

  evals += 1;

  /* Force the preset slots to have the right value : */
  for (ctmp = tt_presets;
       ctmp != (struct constraint *) NULL;
       ctmp = ctmp->next)
    genotype[ctmp->e1].val = ctmp->s;

  /* Now look at each event, and consider the constraints that apply. */
  /* Constraints affecting two events are attached to just one of them. */
  for (i = 0; i < geno_size; i++) {
    slot = genotype[i].val;
    for (ctmp = tt_event_data[i];
	 ctmp != (struct constraint *) NULL;
	 ctmp = ctmp->next) {
      switch (ctmp->ctype) {
      case CLASH:
	otherslot = genotype[ctmp->e2].val;
	if (slot == otherslot)
	  penalty += ctmp->cpenalty;
	/* Penalise a near-clash too, by an amount depending on how near */
	else if (abs(slot - otherslot) <= tt_spread
	  && (slot / tt_slots_per_day) == (otherslot / tt_slots_per_day))
	  penalty += (tt_penalty_consec / (double) abs(slot - otherslot));
	break;
      case CLASH_COPY:
	/* Ignore copy; penalty gets added when original is processed */
	break;
      case BEFORE:
	otherslot = genotype[ctmp->e2].val;
	if (slot >= otherslot)
	  penalty += ctmp->cpenalty;
	break;
      case EXCLUDE:
	if (slot == ctmp->s)
	  penalty += ctmp->cpenalty;
	break;
      default:
	error("PGA: unknown constraint type found in evaluation\n");
	break;
      }				/* end of switch */
    }				/* end of this constraint chain */
  }				/* end of genotype */

  return (1.0 / (1.0 + penalty));
}

/***********************************************************/
/* TSP problem, using standard-format TSP files.           */
/* Note that if a chromosome of length N is initialised    */
/* for a permutation problem, the set is 1..N              */
/* and cities in a TSP file are also numbered 1..N         */
/***********************************************************/

double eval_tsp(GENE *genotype)
{
  double dist = 0.0;
  int i,city1,city2;
  double x,y;

  evals += 1;

  /* chromosome represents order in which cities are traversed */ 
  /* calculate Euclidean distance */

  for (i=0;i<geno_size;i++){
    city1 = genotype[i].val - 1;
    if (i  == (geno_size-1))
      city2 = genotype[0].val - 1;
    else
      city2 = genotype[i+1].val - 1;
    
    x = (double)(tspCity[city1].x - tspCity[city2].x);
    y = (double)(tspCity[city1].y - tspCity[city2].y);

    /* TSPLIB and others want distances to be integers */
    dist = dist + rint(sqrt(x*x + y*y));
  }
    
  return(1/dist);
}



int read_tsp_file()
{
  FILE *f;
  int i,j,a;
  float b,c;
  char buf[4096];
  char word[100];
  int bufsize = 4096;

  if ((f = fopen(datafilename, "r")) == (FILE *) NULL) {
    warn("PGA: could not open TSP data file %s\n",
	  datafilename);
    return (0);
  }

  /* skip over the first two lines */
  for (i=0;i<3;i++)
    fgets(buf, bufsize, f);
  
  
  fscanf(f, "%[A-Z] : %d", word, &nCities);
  if (strncmp(word, "DIMENSION", 9) != 0){
    warn("PGA: incorrect format for TSP data file %s\n",datafilename);
    return (0);
  }

  fgets(buf, bufsize, f); /* skips to end of line */
  geno_size = nCities;
  biggest_allele = nCities;

  /* If genes are chars, check here that nCities is < 128, otherwise
   * you will get into sign trouble when chars get converted to integers
   * at various points elsewhere in the code.
   */

  /* check geometry is OK */
  fscanf(f, "%[^:]: %s", buf, word);
  if (strncmp(word, "EUC_2D", 6) != 0){
    warn("PGA: incorrect edge weight type %s\n",datafilename);
    return (0);
  }

  /* now skip to end of line and skip the next too */
  for (i=0;i<2;i++)  
    fgets(buf, bufsize, f);

  /* now read the array */
  tspCity = (TSPCITY *)MALLOC(sizeof(TSPCITY) * (nCities));
  for (j=0;j<nCities;j++){
    fscanf(f, "%d%f%f", &a,&b,&c);
    if(a != j+1) error("===OUCH: j=%d a=%d\n",j,a);
    tspCity[a-1].x = b;
    tspCity[a-1].y = c;
  }

  fclose(f);
  return(1);
}

