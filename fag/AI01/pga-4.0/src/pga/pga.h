/*          File: pga.h
 *  Last changed: Mon Sep 18 18:02:50 2000
 *
 *  RCS: $Id: pga.h,v 1.1 2006-12-12 10:25:21 tobibobi Exp $
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#ifndef _PGA_H_
#define _PGA_H_

/* Genes are ints by default now. They used to be chars, in earlier
 * versions, but these days we can afford the memory and we'd like
 * to be able to handle fair-sized permutation problems.
 */
typedef int GENEVAL;
 
typedef struct gene
{
  GENEVAL val;   /* Gene value */
  int gen;       /* The generation the value appeared at this postion in */
  int chromoid;  /* The id of the chromosome  it came from ? */
  int geneid;    /* Unique identifier for this gene */
} GENE;

typedef struct genodata
{                          
  GENE *genotype;          /* The actual array of genes               */
  int refcount;            /* How many chromosomes refer to this?     */
  int id;                  /* Unique ID                               */
  int parent1, parent2;    /* ID of parents: 0 if non-existent parent */
} GENODATA;

/* Chromosome data structure. Why not just combine GENODATA and CHROMOSOME,
 * you may ask? The reason I've done it this way is so that several
 * CHROMOSOMEs may share the one set of genes, by having each point at
 * that one set (viz, a GENODATA). This can save some memory, and perhaps
 * some time that might otherwise be spent on copying a GENODATA. If you
 * were to have huge populations, this might save you some system paging
 * activity. If you happened to have very long chromosomes (rarely a good idea)
 * you might save some serious run-time.
 */
typedef struct chromosome
{
  GENODATA *gdp;           /* The chromosome's genes: two chromosomes */
  double fitness;          /* may share one! And its fitness.         */
} CHROMOSOME;

typedef struct chromopair
{                          /* Returned by crossover, contains one or */
  CHROMOSOME child1;       /* two children depending on -t flag      */
  CHROMOSOME child2;
} CHROMOPAIR;


/* The CHROMOTYPE structure is only used in spatially-structured
 * populations, and only for preparing output that shows the 2-d
 * map of the population and a list of how many occurrences of each
 * chromosome there are.
 */
struct CHROMOTYPE {        /* Cell for recording the type of a chromo */
  struct CHROMOTYPE *next; /* Chain pointer */
  int typeval;             /* Unique ID for this chromo string */
  int instances;           /* How many times it appears */
  char typechar;           /* What char it will be printed as */
  CHROMOSOME *cptr;        /* Pointer to actual chromosome */
};


/* Struct for TSP problems */
typedef struct tspcity {
  float x;
  float y;
} TSPCITY;                                                                      


/* If a genotype is longer than this (set by -n) then use
   a double in the decode routine, because a long will overflow.
   A long is 32 bits on most systems, so a value of 60 assumes
   that every call of decode() will be trying to split the
   genotype into at least two numbers, so won't be collecting
   up more than 30 bits at one go. */

#define CRUCIAL_GENO_SIZE 60

/* User actions possible */

#define QUIT      1
#define CONTINUE  2
#define RESTART   3
#define ASK_AGAIN 4

/* data input files */

#define WEIGHTSFILE  "weights"  /* name of file for knapsack data */
#define RRDATAFILE   "rrdata"   /* name of file for Royal Road data */
#define TTDATAFILE   "ttdata"   /* Name of file for timetable data */
#define TSPFILE      "tspdata"  /* Name of file for TSP data */

/* macro for use in spatial stuff */

#define LOCATION(x,y) ((y)*ss_xmax+(x))

#ifndef TRUE
#define TRUE  1
#define FALSE 0
#endif

/******************************/
/* Timetabling-specific stuff */
/******************************/

/* The constraint types: */

enum constraint_type
{
  CLASH,                   /* E1 and E2 must not clash: blame E1 */
  CLASH_COPY,              /* .. but keep a copy for later info with E2 */
  BEFORE,                  /* Event E must precede another: but blame E */
  EXCLUDE,                 /* Event E must not be in slot S: blame E */
  PRESET                   /* Event E must be in slot S: just make it so! */
};

struct constraint
{                          /* used for timetable constraints of     */
  struct constraint *next; /* various sorts: e1 != e2, e1 not in s, */
  int e1;                  /* e1 must be in s, e1 must precede e2   */
  int e2;
  int s;
  double cpenalty;         /* what this constraint costs to break   */
  enum constraint_type ctype;   /* what type of constraint this is */
};

/* The following enum defines symbols for each type of entry in */
/* the timetable data file. The struct following it associates  */
/* keywords with each type, thus making it easy to add other    */
/* keywords just by adding extra lines to the struct - code     */
/* should NOT need changing.                                    */

enum entry_type
{
  END_LIST,                /* (marks end of struct array below)          */
  DATA_EVENTS,             /* Number of events is #1                     */
  DATA_SLOTS,              /* Number of slots (NOT per day) is #1        */
  DATA_DAY,                /* Total number of days is #1                 */
  DATA_SPREAD,             /* Near-clashes if <= #1 apart                */
  DATA_CLASHTYPE,          /* Clash penalties are per-student? type=1..3 */
  PENALTY_CLASH,           /* Penalty for clash is #1                    */
  PENALTY_ORDER,           /* Penalty for order violation is #1          */
  PENALTY_EXCLUDE,         /* Penalty for event in bad slot for it is #1 */
  PENALTY_NEARCLASH,       /* Penalty for near-clash is #1               */
  CONSTRAIN_CLASH,         /* Event #1 and event #2 in different slots   */
  CONSTRAIN_BEFORE,        /* Event #1 must precede event #2             */
  CONSTRAIN_EXCLUDE,       /* Event #1 must not be in slot #2            */
  CONSTRAIN_PRESET         /* Event #1 must be in slot #2                */
};

#define MAX_POPS   100     /* Maximum number of populations allowed      */
#define CHUNK 10           /* The array where numbers get stashed first  */
                           /* will increase by this size when necessary. */
                           /* MUST BE AT LEAST 2                         */
#define ALL -1             /* MUST BE NEGATIVE: means "get all numbers   */
                           /* on the line"                               */

struct tt_params
{
  char *param;
  enum entry_type param_type;
  int args_wanted;
};

enum keynames {
  EVAL,
  GRAY_CODING,
  MAX_FITNESS,
  REPRODUCTION,
  ELITE,
  REPLACEMENT,
  SELECT,
  SELECTION_BIAS,
  CHROMO_LENGTH,
  POPS,
  CHROMOS_PER_POP,
  INIT_OVERSIZE,
  MIGRATION_INT,
  CROSSOVER_TYPE,
  CROSSOVER_RATE,
  MUTATION_RATE,
  ADAPTIVE,
  MUTATION_TYPE,
  REPORT_INTERVAL,
  TWINS,
  RNG_SEED,
  INVALID
};
 
#define NONE '\0'
 
struct params
{
  char flag;
  char *name;
  enum keynames kn;
};


/* Function prototypes */

/* cross.c */
CHROMOPAIR one_pt_cross(CHROMOSOME parent1, CHROMOSOME parent2);
CHROMOPAIR two_pt_cross(CHROMOSOME parent1, CHROMOSOME parent2);
CHROMOPAIR pmx_cross(CHROMOSOME parent1, CHROMOSOME parent2);
CHROMOPAIR ox_cross(CHROMOSOME parent1, CHROMOSOME parent2);
CHROMOPAIR gox_cross(CHROMOSOME parent1, CHROMOSOME parent2);
CHROMOPAIR gpx_cross(CHROMOSOME parent1, CHROMOSOME parent2);
int        rand_bit(void);
CHROMOPAIR uniform_cross(CHROMOSOME parent1, CHROMOSOME parent2);
CHROMOPAIR no_cross(CHROMOSOME parent1, CHROMOSOME parent2);

/* eval.c */
double eval_max(GENE *genotype);
double eval_tsp(GENE *genotype);
double eval_dj1(GENE *genotype);
double eval_dj2(GENE *genotype);
double eval_dj3(GENE *genotype);
double eval_dj5(GENE *genotype);
double eval_f6(GENE *genotype);
double eval_himmelblau(GENE *genotype);
double eval_mcb(GENE *genotype);
double eval_const(GENE *genotype);
double eval_rand(GENE *genotype);
double eval_knap(GENE *genotype);
double eval_nk(GENE *genotype);
double eval_lp(GENE *genotype);
double eval_lp2(GENE *genotype);
double getdouble(FILE *file, double *valaddr, int stopateol);
int    getint(FILE *f, int *valaddr, int stopateol);
int    getlong(FILE *f, long *valaddr, int stopateol);
int    read_weights_file(void);
double eval_rr(GENE *genotype);
void   read_rrdata_file(void);
unsigned long gray2bin(unsigned long g);
void   long_decode(GENE *genotype, int number,
                   double lower, double upper, double *array);
void   double_decode(GENE *genotype, int number,
                     double lower, double upper, double *array);
void   check_event_spec(void);
int    read_tt_datafile(void);
double eval_tt(GENE *genotype);

/* help.c */
void pga_help(void);

/* init.c */
void        init_genotype(GENODATA * gdp, int range);
void        init_permute_genotype(GENODATA * gdp, int range);
CHROMOSOME *init_pop(int numpops, int numchromo);

/* main.c */
int    main(int argc, char *argv[]);
void   free_pops(CHROMOSOME * whole_pop, int num);
void   handle(char *arg);
void   check_pops(CHROMOSOME *pops[]);
void   dump_chromos(CHROMOSOME *pops[], int g);
void   dump_chromo_gens(CHROMOSOME *pops[], int g);
void   replace_worst(CHROMOSOME *pops[], CHROMOSOME chromo,
                     int base, int range);
void   replace_parent(CHROMOSOME *pops[],CHROMOSOME chromo,
                      int base, int range);
void   replace_by_hd(CHROMOSOME *pops[], CHROMOSOME chromo,
                     int base, int range);
int    all_chromos_equal(CHROMOSOME *onepop);
int    compchromo(const void * chromo1, const void * chromo2);
double drandom(void);
int    genecmp(GENE *chromo1, GENE *chromo2, int size);
void   genecpy(GENE *copy, GENE *original, int size);

/* mutate.c */
CHROMOSOME mutate_OBM(CHROMOSOME original, double rate);
CHROMOSOME mutate_PBM(CHROMOSOME original, double rate);
CHROMOSOME mutate_SBM(CHROMOSOME original, double rate);
CHROMOSOME mutate_01(CHROMOSOME original, double rate);
CHROMOSOME mutate_multi(CHROMOSOME original, double rate);
double     adaptive(CHROMOSOME parent1, CHROMOSOME parent2);
double     nonadaptive(CHROMOSOME parent1, CHROMOSOME parent2);
double     violation_score(int event, int slot, GENE *genotype);
int        find_worst_event(GENE *geno);
int        find_random_event(GENE *geno);
int        find_event_by_tn(GENE *geno);
int        find_free_slot(GENE *geno, int i);
int        find_random_slot(GENE *geno, int i);
int        find_slot_by_tn(GENE *geno, int i);
CHROMOSOME mutate_tt(CHROMOSOME original, double rate);
CHROMOSOME mutate_tt_all(CHROMOSOME original, double rate);
CHROMOSOME mutate_tt_each(CHROMOSOME original, double rate);

/* reprod.c */
void       one_reproduction(CHROMOSOME *pops[], int base, int range);
void       gen_reproduction(CHROMOSOME *pops[], int base, int range);
void       breeder_reproduction(CHROMOSOME *pops[], int base, int range);
int        rand_dir(void);
CHROMOSOME ss_select(CHROMOSOME *pops[], int base, int x, int y);
void       ss_gen_reproduction(CHROMOSOME *pops[], int base, int range);
void       ss_one_reproduction(CHROMOSOME *pops[], int base, int range);

/* screen.c */
void   prepare_display(void);
void   wrap_up_display(void);
void   prepare_input(void);
void   wrap_up_input(void);
void   prepare_logfile_output(FILE *output);
int    action_loop(CHROMOSOME *pops[], int *nConverged, int *satisfied);
int    report(int gen_total, FILE *output, CHROMOSOME * pops[],
              double threshold);
int    stringcat(char *s1, char *s2, int n);
void   setprompt(FILE *f, int sstask, int mapped, int saved, int satisfied);

/* select.c */
CHROMOSOME rank_select(CHROMOSOME *pops[], int base, int range);
CHROMOSOME fitprop_select(CHROMOSOME *pops[], int base, int range);
CHROMOSOME tm_select(CHROMOSOME *pops[], int base, int range);
CHROMOSOME tn_select(CHROMOSOME *pops[], int base, int range);

/* ssmap.c */
void ss_map(char *fname, int gen, CHROMOSOME *pops[], int npops, int popsize);
int  compare_by_fitness(const void *ct1, const void *ct2);
int  compare_by_typeval(const void *ct1, const void *ct2);
void do_one_pop(FILE *f, int g, CHROMOSOME *pops[], int thispop, int range);

/* random.c */
void bsd_srandom(unsigned x);
long bsd_random(void);

/* utils.c */
void error(char *s, ...);
void warn(char *s, ...);
void *mmalloc(size_t n, int line, char *file);
#define MALLOC(n) mmalloc((n), __LINE__, __FILE__)

/* params.c */
void read_param_file(char *filename);
void process(enum keynames kn, char *arg, int line, char *filename);

#endif /* _PGA_H_ */


