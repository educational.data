/*          File: nkland.c
 *  Last changed: Thu Jul 20 17:25:05 2000
 *
 *  RCS: $Id: nkland.c,v 1.1 2006-12-12 10:25:20 tobibobi Exp $
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * PGA is copyright (C) 2000 by Peter Ross, Emma Hart.           *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

/* Contents of this file are (c) Terry Jones, Santa Fe, Nov 1994 */

#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/time.h>
#include <malloc.h>
#include "pga.h"
#include "nkland.h"

/*extern char *sys_errlist[]; */
extern int sys_nerr;
extern int errno;

#define nk_uniform(n) ((int) (nk_random() * (double)(n)))
#define WHY errno >= sys_nerr ? "Reason unknown" : sys_errlist[errno]
#define JUMP_MULTIPLIER 128

static double nk_random();
static long nk_seed();
/* PMR: Removed TJ's Malloc and `static void error()' declarations */

NK_LANDSCAPE *
nk_create(int n, int k, int neighborhood_type, long *seed, int show_epistasis, FILE *fp)
{
    int **get_influencers();
    register NK_LANDSCAPE *new;
    register int locus; 
    register int i; 
    int njumps;

    if (n <= 0){
	fprintf(stderr,"nk_create() called with n <= 0 (n=%d)", n);
        exit(1);
    }
    
    if (k >= n){
	fprintf(stderr,"nk_create() called with k >= n (k=%d, n=%d)", k, n);
        exit(1);
    }
    
    new = (NK_LANDSCAPE *) MALLOC(sizeof(NK_LANDSCAPE));

    new->n = n;
    new->k = k;
    njumps = k * JUMP_MULTIPLIER;
    
    if (njumps < n){
	njumps = n;
    }

    new->njumps = njumps;

    *seed = nk_seed(*seed);
    
    new->loci_seeds = (double *) MALLOC(n * sizeof(double));

    for (locus = 0; locus < n; locus++){
	new->loci_seeds[locus] = nk_random();
    } 
    
    new->jump_table = (NK_JUMP *) MALLOC(njumps * sizeof(NK_JUMP));

    for (i = 0; i < njumps; i++){
#ifdef NK_STATS
	new->jump_table[i].hits[0] = new->jump_table[i].hits[1] = 0;
#endif
	new->jump_table[i].location[0] = nk_uniform(njumps);
	new->jump_table[i].location[1] = nk_uniform(njumps);
	new->jump_table[i].fitness[0] = nk_random();
	new->jump_table[i].fitness[1] = nk_random();
    }
    
    new->influencers = get_influencers(n, k, neighborhood_type);
    
    if (show_epistasis){
	
	fprintf(fp, "The landscape's epistatic interactions are as follows:\n");
	
	for (locus = 0; locus < n; locus++){
	    fprintf(fp, "Locus %d: ", locus);
	    
	    for (i = 0; i < k; i++){
		fprintf(fp, "%d ", new->influencers[locus][i]);
	    } 
	    
	    putc('\n', fp);
	} 
    }

    new->neighbors = MALLOC(k + 2);
    new->neighbors[k + 1] = '\0';
    
    return new;
}

double
nk_fitness(GENE *point, NK_LANDSCAPE *landscape)
{
    double fitness = 0.0;
    register int locus;
    register int n = landscape->n;
    register int k = landscape->k;
    register char *neighbors = landscape->neighbors;
    register int *influencers_this_locus;
    
    for (locus = 0; locus < n; locus++){

	register int i;
	double nk_locus_fitness();
	
	influencers_this_locus = landscape->influencers[locus];
	
	neighbors[0] = point[locus].val;

	for (i = 0; i < k; i++){
	    neighbors[i + 1] = point[influencers_this_locus[i]].val;
	} 
    
	fitness += nk_locus_fitness(landscape, locus, neighbors);
    } 
    
    return fitness / (double) n;
}

double
nk_locus_fitness(NK_LANDSCAPE *landscape, int locus, char *neighbors)
{
    double fitness = landscape->loci_seeds[locus];
    register int i;
    register int k = landscape->k;
    register NK_JUMP *jumps = landscape->jump_table;

    for (i = 0; i <= k; i++){
	register int index = neighbors[i];
	fitness += jumps[locus].fitness[index];
	locus = jumps[locus].location[index];
#ifdef NK_STATS
	jumps[locus].hits[index]++;
#endif
    } 
 
    return fitness - floor(fitness);
}

#define MBIG                 1000000000
#define MSEED                161803398
#define FAC                  (1.0 / MBIG)

static int inext;
static int inextp;
static long ma[56];

static double
nk_random(void)
{
    long mj;
    
    if (++inext == 56){
	inext = 1;
    }
    
    if (++inextp == 56){
	inextp = 1;
    }

    mj = ma[inext] - ma[inextp];
    
    if (mj < 0L){
	mj += MBIG;
    }

    ma[inext] = mj;
    return mj * FAC;
}

static long
nk_seed(long seed)
{
    long mj;
    long mk;
    register int i;
    register int k;

    if (seed < 0){
	extern int gettimeofday();

	struct timeval tp;
	if (gettimeofday(&tp, (struct timezone *)0) == -1){
	    fprintf(stderr,"Could not gettimeofday in nk_seed().");
            exit(1);
	}
	
	seed = tp.tv_sec + getpid();
    }
    
    if (seed >= MSEED){
      seed = seed % MSEED;
    }
    if (seed < 0) seed = -seed;
    
    ma[55] = mj = seed;
    mk = 1;
    
    for (i = 1; i <= 54; i++){
	register int ii = (21 * i) % 55;
	ma[ii] = mk;
	mk = mj - mk;
	if (mk < 0){
	    mk += MBIG;
	}
	mj = ma[ii];
    }
    
    for (k = 0; k < 4; k++){
	for (i = 1; i <= 55; i++){
	    ma[i] -= ma[1 + (i + 30) % 55];
	    if (ma[i] < 0){
		ma[i] += MBIG;
	    }
	}
    }
    
    inext = 0;
    inextp = 31;
    
    return seed;
}

int **
get_influencers(int n, int k, int neighborhood_type)
{
    register int locus;
    void pick_influencers();

    if (k){
	int **influencers = (int **) MALLOC(n * sizeof(int *));

	for (locus = 0; locus < n; locus++){
	    /* Get space for k others. */
	    influencers[locus] = (int *)MALLOC(k * sizeof(int)); 
	    
	    /* Choose the k influencing, put into influencers[locus]. */
	    pick_influencers(locus, n, k, neighborhood_type, influencers[locus]);
	}

	return influencers;
    }
    else {
	return (int **) 0;
    }
}

void
pick_influencers(int locus, int n, int k, int neighborhood_type, int *influencers)
{
    static char *picked = (char *)0;
    register int i;
    register int picked_index = 0;
    
    /* Small optimization if we know that all loci except locus must be influencers. */
    if (k == n - 1){
	for (i = 0; i < n; i++){
	    if (i != locus){
		influencers[picked_index] = i;
		picked_index++;
	    }
	}
	
	return;
    }

    switch (neighborhood_type){
	case RANDOM_NEIGHBORS:{
	    /*
	     * Knuth gives a better algorithm for this.
	     * it's simple, linear, and doesn't need malloc...
	     * I didn't want to put it in now, since I don't
	     * have time to check it properly for you at the
	     * moment. This works, but is slower than need be,
	     * especially as k approaches n - 1.
	     */
	     
	    int npicked = 0;
	    
	    if (picked == (char *)0){
		/* This is never freed, but that's no big deal. */
		picked = MALLOC(n);
	    }
	    
	    for (i = 0; i < n; i++){
		picked[i] = 0;
	    }
	    
	    while (npicked < k) {
		int new = nk_uniform(n);
		
		if (new != locus && !picked[new]){
		    picked[new] = 1;
		    npicked++;
		    influencers[picked_index] = new;
		    picked_index++;
		}
	    }
	    
	    break;
	}

	case NEXTDOOR_NEIGHBORS:{
	    register int nleft;
	    register int nright;

	    nleft = nright = k >> 1;
	    
	    /* Put the extra one left or right, at random. */
	    if (k % 2){
		if (!nk_uniform(2)){
		    nright++;
		}
		else {
		    nleft++;
		}
	    }

	    /* Get the loci to the left, watch for the edge of the world. */
	    if (locus >= nleft){
		register int i; 
		for (i = locus - nleft; i < locus; i++){
		    influencers[picked_index] = i;
		    picked_index++;
		} 
	    }
	    else {
		for (i = 0; i < locus; i++){
		    influencers[picked_index] = i;
		    picked_index++;
		} 
		
		for (i = n - (nleft - locus); i < n; i++){
		    influencers[picked_index] = i;
		    picked_index++;
		}
	    }
	    
	    /* Get the loci to the right, watch for the edge of the world. */
	    if (locus + nright < n){
		for (i = locus + 1; i <= locus + nright; i++){
		    influencers[picked_index] = i;
		    picked_index++;
		}
	    }
	    else {
		for (i = locus + 1; i < n; i++){
		    influencers[picked_index] = i;
		    picked_index++;
		}
		
		for (i = 0; i <= locus + nright - n; i++){
		    influencers[picked_index] = i;
		    picked_index++;
		}
	    }

	    break;
	}

	default :{
	    fprintf(stderr,"unknown neighborhood_type found in pick_influencers.");
            exit(1);
	}
    }
    
    return;
}


#ifdef NK_STATS
void
nk_stats(NK_LANDSCAPE *landscape, FILE *fp)
{
    register int i;
    register int njumps = landscape->njumps;
    int width = 1 + (int) log10((double) njumps);
    
    printf("Jump table statistics:\n");
    
    for (i = 0; i < njumps; i++){
	fprintf(fp, "Location %*d [%*d, %f, %3d] [%*d, %f, %3d]\n", width, i, 
		width, landscape->jump_table[i].location[0], landscape->jump_table[i].fitness[0], landscape->jump_table[i].hits[0],
		width, landscape->jump_table[i].location[1], landscape->jump_table[i].fitness[1], landscape->jump_table[i].hits[1]);
    } 
    
    return;
}
#endif






