Travelling Salesman Problems
----------------------------

Last update: 21 July 2000 by PMR

PGA handles small travelling salesman problems (TSPs) of a particular
sort:
 - the points to visit all lie in a two-dimensional plane;
 - the distance between two points is the normal, Euclidean distance
   rounded to the nearest integer:
    (int)(0.5 + sqrt(dx*dx + dy*dy))
 - distances are symmetric, that is, the didtance from A to B is
   always the same as the distance from B to A.
The file format is that used by TSPLIB, at
  http://www.iwr.uni-heidelberg.de/iwr/comopt/software/TSPLIB95/  
but PGA only reads the symmetric 2-d examples there.

TSPLIB files typically start like this:
    ...........
    NAME : washington
    COMMENT : electoral district problem, by Bill Clinton
    TYPE : TSP
    DIMENSION : 194
    EDGE_WEIGHT_TYPE : EUC_2D
    NODE_COORD_SECTION               
    1 2314 5457
    2 6606 1324
    etc
    ...........
PGA only pays attention to the fourth line, from which it picks up
the number of points (194 in this case) and then the seventh and
subsequent lines, reading as many lines of point data as were
specified by the DIMENSION. So you may simply edit that line if you
want PGA to read fewer points. It does also check that the fifth line
specifies EUC_2D, in case you picked up the wrong kind of file by
mistake.

TSPLIB provides optimal tour lengths, as found by laborious
branch-and-cut or branch-and-bound methods, or (in a few tough cases)
lower and upper limits on the optimal tour length. Note that lengths
are always integers!   

The subdirectory TSP/ contains four samples from TSPLIB, and two
trivial problems to let you check that the right things seem to be
happening:

pr226.tsp  .... optimum 80369
rat195.tsp .... optimum 2323
st70.tsp ...... optimum 675
ts225.tsp ..... optimum 126643

These are all pretty small by today's standards. The two trivial
problems are:

ts12.tsp ...... 12 points in a straight line, optimum 11000. There
                are many optimal tours: start at one point, proceed
                to one end visiting any/all points passed on the way,
                then return to the other end visiting some unvisited
                cities, return to the start visiting whatever has not
                yet been visited. For example,
                   4 6 8 9 11 12 10 7 5 3 1 2
                   ------------- ------ --- -
                        |           |    |  + on way back to start (4)
                        |           |    |
                        |           |    + on way from 4 to other end (1)
                        |           |
                        |           +those not yet visited between 12 and 4
                        |
                        outward from start (4) to one end (12)

ts14.tsp ...... This adds two more points, not on the straight line,
                so that now there is essentially a minimal number of
                tours. Each possible tour is a rectangle, the four
                corners are points 1, 2, 13, 14.

PGA uses a straightforward permutation encoding of the points -- if
there are N points, they are numbered 1..N inclusive. There are
four crossover types, PMX, OX, GOX and GPX, and three mutation types,
swap (one adjacent pair), order (one swap, of any pair) and position
(pick up one, inject it somewhere).

Experiments should persuade you that it is not easy to solve any but
the trivial problems. In general, permutation-based encodings using
special permutation-preserving operators have not been outstandingly
good ways to tackle interesting TSPs.
