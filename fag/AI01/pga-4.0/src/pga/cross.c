/*          File: cross.c
 *  Last changed: Wed Apr 18 12:06:34 2001
 *
 *  RCS: $Id: cross.c,v 1.1 2006-12-12 10:25:19 tobibobi Exp $
 *
 *  Crossover choices. 
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include "pga.h"

extern int geno_size;
extern int chromo_id;
extern int twins;
extern int biggest_allele;
extern FILE *output;
/************************************************************************/
/* This performs one point crossover on 'parent1' and 'parent2'.        */
/* Child is new (even though there may be identical genotypes elsewhere */
/* in the system) so has refcount 0; set to 1 only upon insertion       */
/************************************************************************/

CHROMOPAIR
one_pt_cross(CHROMOSOME parent1, CHROMOSOME parent2)
{
  int chosen, i;
  CHROMOPAIR children;

  /* Generate crossover point randomly. */
  chosen = (int) (drandom()* geno_size);

  /* Create the child. */
  children.child1.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
  children.child1.gdp->refcount = 0;
  children.child1.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
  children.child1.gdp->id = chromo_id++;
  children.child1.gdp->parent1 = parent1.gdp->id;
  children.child1.gdp->parent2 = parent2.gdp->id;

  if (twins) {
    children.child2.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    children.child2.gdp->refcount = 0;
    children.child2.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
    children.child2.gdp->id = chromo_id++;
    children.child2.gdp->parent1 = parent2.gdp->id;
    children.child2.gdp->parent2 = parent1.gdp->id;
  }
  /* Do the crossover, keeping track of gens, etc. */
  for (i = 0; i < chosen; i++) {
    children.child1.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
    children.child1.gdp->genotype[i].gen = parent2.gdp->genotype[i].gen;
    if (twins){
      children.child2.gdp->genotype[i].gen = parent1.gdp->genotype[i].gen;
      children.child2.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
    } 
 }
  for (i = chosen; i < geno_size; i++) {
    children.child1.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
    children.child1.gdp->genotype[i].gen = parent1.gdp->genotype[i].gen;
    if (twins){
      children.child2.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
      children.child2.gdp->genotype[i].gen = parent2.gdp->genotype[i].gen;
    }
  }
  return (children);
}

/************************************************************************/
/* This performs two point crossover on 'parent1' and 'parent2'.        */
/* Child is new (even though there may be identical genotypes elsewhere */
/* in the system) so has refcount 0; set to 1 only upon insertion       */
/************************************************************************/

CHROMOPAIR
two_pt_cross(CHROMOSOME parent1, CHROMOSOME parent2)
{
  int first, last, i, swap;
  CHROMOPAIR children;

  /* Generate the two points randomly. */
  first = (int) (drandom()* geno_size);
  last = (int) (drandom()* geno_size);

  if (first > last) {		/* Make sure that they are in the */
    swap = first;		/* right order.                   */
    first = last;
    last = swap;
  }
  /* Create the child. */
  children.child1.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
  children.child1.gdp->refcount = 0;
  children.child1.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
  children.child1.gdp->id = chromo_id++;
  children.child1.gdp->parent1 = parent1.gdp->id;
  children.child1.gdp->parent2 = parent2.gdp->id;

  if (twins) {
    children.child2.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    children.child2.gdp->refcount = 0;
    children.child2.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
    children.child2.gdp->id = chromo_id++;
    children.child2.gdp->parent1 = parent2.gdp->id;
    children.child2.gdp->parent2 = parent1.gdp->id;
  }

  /* Do the crossover */
  for (i = 0; i < first; i++) {
    children.child1.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
    children.child1.gdp->genotype[i].gen = parent2.gdp->genotype[i].gen;
    if (twins){
      children.child2.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
      children.child2.gdp->genotype[i].gen = parent1.gdp->genotype[i].gen;
    }
  }
  for (i = first; i < last; i++) {
    children.child1.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
    children.child1.gdp->genotype[i].gen = parent1.gdp->genotype[i].gen;
    if (twins){
      children.child2.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
      children.child2.gdp->genotype[i].gen = parent2.gdp->genotype[i].gen;
    }
  }
  for (i = last; i < geno_size; i++) {
    children.child1.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
    children.child1.gdp->genotype[i].gen = parent2.gdp->genotype[i].gen;
    if (twins){
      children.child2.gdp->genotype[i].gen = parent1.gdp->genotype[i].gen;
      children.child2.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
    }
  }
  
  return (children);
}


/************************************************************************/
/* This performs uniform crossover on 'parent1' and 'parent2'.          */
/* Uses rand_bit() to cut down on expensive random number generation.   */
/* Child is new (even though there may be identical genotypes elsewhere */
/* in the system) so has refcount 0; set to 1 only upon insertion       */
/************************************************************************/

int rand_bit_src;
int rand_bit_bits = 0;
int rand_bit(void)
{
  if (rand_bit_bits == 0) {
    rand_bit_bits = 16;
    rand_bit_src = (int) (drandom()* 131072.0); /* Get 16+1 more random bits */
  }
  rand_bit_src = rand_bit_src >> 1;     /* Lose 1 bit */
  rand_bit_bits--;              /* Number left after this call */
  return (rand_bit_src & 1);
}

CHROMOPAIR
uniform_cross(CHROMOSOME parent1, CHROMOSOME parent2)
{
  int i;
  CHROMOPAIR children;

  children.child1.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
  children.child1.gdp->refcount = 0;
  children.child1.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
  children.child1.gdp->id = chromo_id++;
  children.child1.gdp->parent1 = parent1.gdp->id;
  children.child1.gdp->parent2 = parent2.gdp->id;

  if (twins) {
    children.child2.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    children.child2.gdp->refcount = 0;
    children.child2.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
    children.child2.gdp->id = chromo_id++;
    children.child2.gdp->parent1 = parent2.gdp->id;
    children.child2.gdp->parent2 = parent1.gdp->id;
  }
  
  /* Do the crossover */
  for (i = 0; i < geno_size; i++)
    if (rand_bit()){
      children.child1.gdp->genotype[i].gen = parent2.gdp->genotype[i].gen;
      children.child1.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
      if (twins){
	children.child2.gdp->genotype[i].gen = parent1.gdp->genotype[i].gen;
        children.child2.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
      }
    } else {
      children.child1.gdp->genotype[i].gen = parent1.gdp->genotype[i].gen;
      children.child1.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
      if (twins){
	children.child2.gdp->genotype[i].gen = parent2.gdp->genotype[i].gen;
        children.child2.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
      }
    }
  return (children);
}

/***************************************************************/
/* No crossover; exists for the sake of investigating the null */
/* hypothesis in GA experiments.                               */
/***************************************************************/

CHROMOPAIR
no_cross(CHROMOSOME parent1, CHROMOSOME parent2)
{
  CHROMOPAIR children;

  children.child1 = parent1;
  if (twins)
    children.child2 = parent2;
  return (children);
}



/* Permutation based crossover operators */

/*************************************************************************/
/* PMX crossover (implemented as described in Michalewicz, page 172)     */
/* This operator was proposed by Goldberg and Lingle: it builds an       */
/* offspring by choosing a subsequence of a permutation from one         */
/* parent, and preserving the order and position of as many 'cities' as  */
/* possible from the other parent. A subsequence of a permutation is     */
/* selected by choosing two random cut points which serve as boundaries  */
/* for swapping operations                                               */
/*************************************************************************/

CHROMOPAIR
pmx_cross(CHROMOSOME parent1, CHROMOSOME parent2)
{
  int i,first,last,swap;
  CHROMOPAIR children;
  int *map1, *map2, *conflict1, *conflict2;
  int city;

  map1 = (int *)MALLOC((geno_size+1)*sizeof(int));
  conflict1  = (int *)MALLOC((geno_size+1)*sizeof(int));
  map2 = (int *)MALLOC((geno_size+1)*sizeof(int));
  conflict2  = (int *)MALLOC((geno_size+1)*sizeof(int));
  for(i=0; i< geno_size+1; i++) {
    map1[i] = map2[i] = conflict1[i] = conflict2[i] = 0;
  }
  
  children.child1.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
  children.child1.gdp->refcount = 0;
  children.child1.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
  children.child1.gdp->id = chromo_id++;
  children.child1.gdp->parent1 = parent1.gdp->id;
  children.child1.gdp->parent2 = parent2.gdp->id;
  
  if (twins) {
    children.child2.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    children.child2.gdp->refcount = 0;
    children.child2.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
    children.child2.gdp->id = chromo_id++;
    children.child2.gdp->parent1 = parent2.gdp->id;
    children.child2.gdp->parent2 = parent1.gdp->id;
  }
  
  /* First choose two random cross points */
  /* Generate the two points randomly. */
  first = (int) (drandom()* geno_size);
  do {
    last = (int) (drandom()* geno_size);
  } while (last == first);
  
  if (first > last) {		/* Make sure that they are in the */
    swap = first;		/* right order.                   */
    first = last;
    last = swap;
  }

  /* first the segments between the bounds are swapped */
  for (i=first;i<last;i++){
    children.child1.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
    if (twins)
      children.child2.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
    /* and define the mappings */
    map1[parent2.gdp->genotype[i].val] = parent1.gdp->genotype[i].val; 
    conflict1[parent2.gdp->genotype[i].val] = 1;
    map2[parent1.gdp->genotype[i].val] = parent2.gdp->genotype[i].val; 
    conflict2[parent1.gdp->genotype[i].val] = 1;
  }

  /* now read along the parent chromo: if there is a conflict ,
   * replace with the mapping, if not, copy the gene from parent1
   * to the chromo  */

  for (i=0;i<first;i++){
    if (conflict1[parent1.gdp->genotype[i].val] == 1){
      city = map1[parent1.gdp->genotype[i].val];
      while (conflict1[city])
	city = map1[city];
      children.child1.gdp->genotype[i].val = city;
    }
    else
      children.child1.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
  }
  
  for (i=last;i<geno_size;i++){
    if (conflict1[parent1.gdp->genotype[i].val] == 1){
      city = map1[parent1.gdp->genotype[i].val];
      while (conflict1[city])
	city = map1[city];
      children.child1.gdp->genotype[i].val = city;
    }
    else
      children.child1.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
  }
  if(twins) {
    for (i=0;i<first;i++){
      if (conflict2[parent2.gdp->genotype[i].val] == 1){
        city = map2[parent2.gdp->genotype[i].val];
        while (conflict2[city])
	  city = map2[city];
        children.child2.gdp->genotype[i].val = city;
      }
      else
        children.child2.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
    }
  
    for (i=last;i<geno_size;i++){
      if (conflict2[parent2.gdp->genotype[i].val] == 1){
        city = map2[parent2.gdp->genotype[i].val];
        while (conflict2[city])
	  city = map2[city];
        children.child2.gdp->genotype[i].val = city;
      }
      else
        children.child2.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
    }
  }
  
  free(map1);
  free(conflict1);
  free(map2);
  free(conflict2);
  
  return(children);
}

/*********************************************************************/
/* OX crossover (see Michalewicz, page 173) Proposed by Davis, this  */
/* operator builds offspring by choosing a subsequence of a          */
/* permutation from one parent, and preserving the relative order of */
/* 'cities' in the other parent.                                     */
/*********************************************************************/

CHROMOPAIR ox_cross(CHROMOSOME parent1, CHROMOSOME parent2) 
{
 int i,first,last,swap,segLength,j,k,place; 
 CHROMOPAIR children; 
 int *segment1, *segment2, *in_substring1, *in_substring2;

  children.child1.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
  children.child1.gdp->refcount = 0;
  children.child1.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
  children.child1.gdp->id = chromo_id++;
  children.child1.gdp->parent1 = parent1.gdp->id;
  children.child1.gdp->parent2 = parent2.gdp->id;
  
  if (twins) {
    children.child2.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
    children.child2.gdp->refcount = 0;
    children.child2.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
    children.child2.gdp->id = chromo_id++;
    children.child2.gdp->parent1 = parent2.gdp->id;
    children.child2.gdp->parent2 = parent1.gdp->id;
  }
  

  /* First choose two random cross points */
  /* Generate the two points randomly. */
  first = (int) (drandom()* geno_size);
  do {
    last = (int) (drandom()* geno_size);
  } while (last == first);
  
  if (first > last) {		/* Make sure that they are in the */
    swap = first;		/* right order.                   */
    first = last;
    last = swap;
  }

  
  /* Calculate the length of the segment that still needs to 
   * be allocated, ie the number of genes not included in the substring
   * defined between the cut points */
  segLength = geno_size-(last-first);
  
  segment1 = (int *)MALLOC(sizeof(int)*(segLength+1));
  segment2 = (int *)MALLOC(sizeof(int)*(segLength+1));
  in_substring1 = (int *)calloc(biggest_allele+1, sizeof(int));
  in_substring2 = (int *)calloc(biggest_allele+1, sizeof(int));

  /* First we can copy the substrings defined by the cut points  
   * straight into the children
   * and mark these alleles as out of the equation
   */
  
  for (i=first;i<last;i++){
    children.child1.gdp->genotype[i].val = parent1.gdp->genotype[i].val;
    in_substring1[parent1.gdp->genotype[i].val]  = 1;
    if (twins){
      in_substring2[parent2.gdp->genotype[i].val]  = 1;
      children.child2.gdp->genotype[i].val = parent2.gdp->genotype[i].val;
    }
  }
  
  /* Now, starting from the second cut point of one parent, the alleles
   * from the other parent are copied in the same order, omitting alleles
   * already present
   */
  j = 0;
  k = 0;
  for (i=0;i<geno_size;i++){
    place = last+i;
    if (place >= geno_size)
      place = place-geno_size;
    if (!in_substring1[parent2.gdp->genotype[place].val])
      segment1[j++] = parent2.gdp->genotype[place].val;
    if (twins)
      if (!in_substring2[parent1.gdp->genotype[place].val])
	segment2[k++] = parent1.gdp->genotype[place].val;
  }
  
  /* Now we have the new segments, fill up the child genotypes,
   * starting at the 2nd cut point again
   */
  for (i=0;i<segLength;i++){
    place = last+i;
    if (place >= geno_size)
      place = place-geno_size;
    children.child1.gdp->genotype[place].val = segment1[i];
      
    if (twins)
      children.child2.gdp->genotype[place].val = segment2[i];
  }

  /* Free the arrays */
  free(in_substring2);
  free(in_substring1);
  free(segment2);
  free(segment1);

  return (children);
}

/********************************************************************/
/* GOX generalized order crossover                                  */
/* 1) select a substring from parent1                               */
/* 2) all operations of the substring are deleted with respect      */
/*    to their index in the receiving chromosome                    */
/* 3) the donators substring is implanted into the receiver at the  */   
/*    position where the first operation of the substring has       */
/*    occurred.                                                     */
/*                                                                  */
/* Taken from Evolutionary Search and the Job Shop, Dirk C.Matfield */
/* page 72                                                          */ 
/********************************************************************/

CHROMOPAIR gox_cross(CHROMOSOME parent1, CHROMOSOME parent2) 
{
 int i,first,last,swap,segLength,j,k,place; 
 CHROMOPAIR children; 
 int *dummy, *substring;
 int firstjob, firstindex,found ,pos, p2position, job,index;
 dummy = (int *)MALLOC((geno_size+1)*sizeof(int));

 children.child1.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
 children.child1.gdp->refcount = 0;
 children.child1.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
 children.child1.gdp->id = chromo_id++;
 children.child1.gdp->parent1 = parent1.gdp->id;
 children.child1.gdp->parent2 = parent2.gdp->id;
 
 if (twins) {
   children.child2.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
   children.child2.gdp->refcount = 0;
   children.child2.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
   children.child2.gdp->id = chromo_id++;
   children.child2.gdp->parent1 = parent2.gdp->id;
   children.child2.gdp->parent2 = parent1.gdp->id;
 }
 
 /* First choose two random cross points */
 /* Generate the two points randomly. */
 first = (int) (drandom()* geno_size);
 do {
   last = (int) (drandom()* geno_size);
 } while (last == first);
 
 if (first > last) {		/* Make sure that they are in the */
   swap = first;		/* right order.                   */
   first = last;
   last = swap;
 }

 segLength = last-first;
 /* Select a substring defined between first and last from parent1 */
 substring = (int *)MALLOC(sizeof(int)*(segLength+1));
 
 /* Make a copy */
 for (i=0;i<geno_size;i++)
   dummy[i] = parent2.gdp->genotype[i].val;

 
 /* Now for each gene in segment, delete that job-index from
  * the child */

 for (i=0;i<segLength;i++){
   /* The allele gives the job we are interested in:
    * find the INDEX of this job in parent1 first
    */
    
   index = 0;
   job = parent1.gdp->genotype[first+i].val;
   for (j=0;j<first+i;j++){
     if (parent1.gdp->genotype[j].val == job)
       index++;
   }
   
   /* So the job is the index'th + 1 */
   index++;
   /* if this is the first one, record the index */
   if (i ==0){
     firstindex = index;
     firstjob = job;
   }

   /* Now we have the index: delete the index'th occurence of JOB
    * from the dummy */
   found = 0;
   for (j=0;j<geno_size;j++){
     if (parent2.gdp->genotype[j].val == job)
       found++;
     if (found == index){
       /* got it ! delete it ! */
       dummy[j] = -1;
       break;
     }
   }
 }

 /* Now find out where the first index'th occurrence of the firstjob is
  * in parent 2 - we are going to copy upto here,
  */
 found = 0;
 for (i=0;i<geno_size;i++){
   if (parent2.gdp->genotype[i].val == firstjob)
     found++;
   if (found == firstindex)
     break;
 }

 p2position = i;

 /* Now dummy[] contains -1's and real values
  * start with first real value and copy into the child until we reach
  * the firstindex'th occurrence of the firstjob */
 pos = 0;
 for (i=0;i<p2position;i++)
   if (dummy[i] != -1)
     children.child1.gdp->genotype[pos++].val = dummy[i];

 /* Now insert substring copied from 1st parent*/
 for (i=0;i<segLength;i++)
   children.child1.gdp->genotype[pos++].val =
     parent1.gdp->genotype[first+i].val;

 /* Now fill it up */
 for (i=p2position;i<geno_size;i++)
   if (dummy[i] != -1)
     children.child1.gdp->genotype[pos++].val = dummy[i];

 free(substring);
 free(dummy);
 
 return(children);
}

/********************************************/
/* GPX generalized position based crossover */
/* page 97 D.Mattfeld                       */
/********************************************/

CHROMOPAIR gpx_cross(CHROMOSOME parent1, CHROMOSOME parent2) 
{
 int i,first,last,swap,segLength,j,k,place; 
 CHROMOPAIR children; 
 int *dummy, *substring;
 int firstjob, firstindex,found ,pos, p2position, job,index;
 dummy = (int *)MALLOC((geno_size+1)*sizeof(int));

 children.child1.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
 children.child1.gdp->refcount = 0;
 children.child1.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
 children.child1.gdp->id = chromo_id++;
 children.child1.gdp->parent1 = parent1.gdp->id;
 children.child1.gdp->parent2 = parent2.gdp->id;
 
 if (twins) {
   children.child2.gdp = (GENODATA *) MALLOC(sizeof(GENODATA));
   children.child2.gdp->refcount = 0;
   children.child2.gdp->genotype = (GENE *)MALLOC((geno_size)*sizeof(GENE));
   children.child2.gdp->id = chromo_id++;
   children.child2.gdp->parent1 = parent2.gdp->id;
   children.child2.gdp->parent2 = parent1.gdp->id;
 }
  
 /* First choose two random cross points */
 /* Generate the two points randomly. */
 first = (int) (drandom()* geno_size);
 do {
   last = (int) (drandom()* geno_size);
 } while ( last == first );

 if (first > last) {		/* Make sure that they are in the */
   swap = first;		/* right order.                   */
   first = last;
   last = swap;
 }

 segLength = last-first;
 /* Select a substring defined between first and last from parent1 */
 substring = (int *)MALLOC(sizeof(int)*(segLength+1));
 
 /* Make a copy */
 for (i=0;i<geno_size;i++)
   dummy[i] = parent2.gdp->genotype[i].val;

 /* Now for each gene in segment, delete that job-index from
  * the child */

 for (i=0;i<segLength;i++){
   /* The allele gives the job we are interested in:
    * find the INDEX of this job in parent1 first
    */
   index = 0;
   job = parent1.gdp->genotype[first+i].val;
   for (j=0;j<first+i;j++){
     if (parent1.gdp->genotype[j].val == job)
       index++;
   }
   
   /* So the job is the index'th + 1 */
   index++;
   /* If this is the first one, record the index */
   if (i ==0){
     firstindex = index;
     firstjob = job;
   }

   /* Now we have the index: delete the index'th occurence of JOB
    * from the dummy */
   found = 0;
   for (j=0;j<geno_size;j++){
     if (parent2.gdp->genotype[j].val == job)
       found++;
     if (found == index){
       /* got it ! delete it ! */
       dummy[j] = -1;
       break;
     }
   }
 }

 /* We are going to implant the substring at the same position in
  * the child as it came from in the donor parent
  * we know  this is position given by first
  *
  * so using the dummy array start to fill up the child
  * until we have (first-1) values in it */
 pos = 0;
 i = 0;
 while(pos < first){
   if (dummy[i] != -1)
     children.child1.gdp->genotype[pos++].val = dummy[i];
   i++;
 }

 p2position = i;

 /* Now insert substring copied from 1st parent*/
 for (i=0;i<segLength;i++)
   children.child1.gdp->genotype[pos++].val =
     parent1.gdp->genotype[first+i].val;

 /* Now fill it up */
 for (i=p2position;i<geno_size;i++)
   if (dummy[i] != -1)
     children.child1.gdp->genotype[pos++].val = dummy[i];

 free(substring);
 free(dummy);
 
 return(children);
}





