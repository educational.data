/*          File: params.c
 *  Last changed: Thu Sep 14 00:24:46 2000
 *
 *  RCS: $Id: params.c,v 1.1 2006-12-12 10:25:21 tobibobi Exp $
 *
 *  Handles parameters. The function process(name,value,line,file) 
 *  does the main work. The function read_param_file(filename) reads
 *  parameter names and values from a file and processes them.
 *  A parameter file can contain comments (from a # to end of line),
 *  blank lines and lines of the form
 *      name = value
 *  spaced as you like on one line, with optional comment, eg
 *       eval    =  dj6   # one of de Jong's test functions
 *  However, neither name nor value may contain spaces. The stuff of 
 *  this sort that appears at the start of any log file is OK to copy 
 *  into your parameter files. Lines not of the above form (eg other 
 *  lines in a log file) will cause a fatal error
 */
 
/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "pga.h"

/* These were declared in main.c, and are referred to by process(..) below */
extern CHROMOSOME(*selection) (CHROMOSOME *[], int, int);
extern CHROMOPAIR(*crossover) (CHROMOSOME, CHROMOSOME);
extern CHROMOSOME(*mutate) (CHROMOSOME, double);
extern void (*reproduction) (CHROMOSOME *[], int, int);
extern double (*mrate) (CHROMOSOME, CHROMOSOME);
extern double (*evaluate) (GENE *);
extern int (*find_bad_event) (GENE *);
extern int (*find_good_slot) (GENE *, int);
extern void (*insert) (CHROMOSOME *[], CHROMOSOME, int, int);
extern int numpop;
extern int numchromo;
extern int geno_size;
extern int repinterval;
extern int isadaptive;
extern int usegray;
extern int migint;
extern int biggest_allele;
extern int tournament_size;
extern int tt_mtn1;
extern int tt_mtn2;
extern int elite;
extern int hd_set_size;
extern int randseed;               
extern int nk_k;                   
extern long nk_seed;               
extern int twins;
extern char *geno_size_string;     
extern double mute_rate;
extern double cross_rate;
extern double bias;
extern char *eval_name;
extern char *maxfitness;
extern char *cross_name;
extern char *select_name;
extern char *repro_name;
extern char *replace_name;
extern char *mutate_name;              
extern double lpmax;               
extern int ss_n; 
extern int breed_n;                
extern double oversizing;
extern int permutation;
extern struct params paramlist[];


/*****************************************************************/
/* boring_when_tidied(buffer) removes any comment, from a # to   */
/* end of line, and returns TRUE if what's left is empty or just */
/* white space. Else returns FALSE (ie not boring)               */
/*****************************************************************/
int boring_when_tidied(char *buffer)
{
  int j, l;

  l = strlen(buffer);
  if(l==0)
    return(TRUE);
  else if(buffer[--l]=='\n')
    buffer[l] = '\0';
      /* Find first '#', end the line there */
  for(j=0; j<=l; j++) {
    if(buffer[j] == '#') {
      buffer[j] = '\0';
      break;
    }
  }

  l = strlen(buffer);
  for(j=0; j<l; j++) {
    if( !isspace(buffer[j]) )
      return(FALSE);
  }
  return(TRUE);
}

/*****************************************************************/
/* tidy(buffer) returns a pointer to the buffer with leading and */
/* trailing space removed. Returns NULL if what's left is empty  */
/*****************************************************************/
char *tidy(char *buffer, int line, char *filename) 
{
  int i, l;
  char *p;

      /* Go look for a non-space char, return NULL if not found */
  for(p=buffer; *p !='\0'; p++) {
    if(!isspace(*p))
      break;
  }
  if(*p=='\0')
    return(NULL);

      /* Starting from the non-space char, insert a \0 at the first space */
  l = strlen(p);
  for(i=0; i<l; i++) {
    if(isspace(p[i])) {
      p[i] = '\0';
      break;
    }
  }
      /* Check the rest, was there more text after a space? */
  if(i<l) {
    i++;
    for(;i<l;i++) {
      if(!isspace(p[i])) {
        error("PGA: not allowed spaces within name or value on line %d"
              " of file %s\n", line, filename);
      }
    }
  }

  if(strlen(p) == 0)
    return(NULL);
  else
    return(p);
}

#define MAXLINELENGTH 256

/**************************************************************************/
/* read_param_file(filename) reads a parameter file and processes         */
/* each line in turn, immediately. Parameter files must be explicitly     */
/* set using -f<filename> on the command line, there is no default,       */
/* and command-line arguments, including -f, are processed left-to-right. */
/* You can therefore override a value set in a file.                      */  
/**************************************************************************/
void read_param_file(char *filename)
{
  FILE *f;
  char buffer[MAXLINELENGTH+1];
  char *name, *value;
  int i, j, k, line = 0;
  struct params *pl;
  
  if((f = fopen(filename, "r")) == NULL) {
    error("PGA: could not open parameter file %s\n", filename);
  }

  while( fgets(buffer, MAXLINELENGTH, f) != NULL ) {
    line++;
        /* Get rid of any comment. If what's left is empty */
        /* or white space, proceed to next input line */
    if( boring_when_tidied(buffer) )
      continue;

        /* If we reach here, the only legitimate possibility */
        /* is of the form   name = value  maybe with spaces. */
    k = strlen(buffer);
    j = 0; /* count the number of = in the string */
    name = buffer;
    for(i=0; i<k; i++) {
      if(buffer[i] == '=') {
        value = buffer+i+1;
        buffer[i] = '\0';
        j++;
      }
    }
    if(j != 1) {
      error("PGA: line %d of file %s is not of the form   name = value\n",
            line, filename);
    } 

        /* Tidy the name and value strings to get rid of unwanted */
        /* leading and trailing spaces */
    if((name = tidy(name, line, filename)) == NULL) {
      error("PGA: something wrong before '=' in line %d of file %s\n",
            line, filename);
    }
    if((value = tidy(value, line, filename)) == NULL) {
      error("PGA: something wrong after '=' in line %d of file %s\n",
            line, filename);
    }

    for(pl = paramlist; pl->name != NULL; pl++) {
      if(strcmp(name, pl->name) == 0) {
        process(pl->kn, value, line, filename);
        break;
      }
    }
    if(pl->name == NULL) {
      error("PGA: unrecognised parameter name %s at line %d of %f\n",
            name, line, filename);
    }
  }
}

/************************************************************************/
/* This processes info from name/value pairs and command-line arguments */
/************************************************************************/
void process(enum keynames kn, char *arg, int line, char *filename)
{
  int newint;
  double newdouble;
  char tt_eventchoice[16], tt_slotchoice[16]; 

  switch(kn) {
    case ADAPTIVE: /* -a sets adaptive mutation */
      if(strcmp(arg,"true")==0)
        isadaptive = TRUE;
      else if(strcmp(arg,"false")==0)
        isadaptive = FALSE;
      else
        isadaptive = !isadaptive;
      if (isadaptive)
        mrate = adaptive;
      else
        mrate = nonadaptive;
      break;
      
    case SELECTION_BIAS: /* -b<R> sets bias in rank selection */
      if(strcmp(arg,"n/a")==0)
        break;
      if (sscanf(arg, "%lf", &newdouble) == 1) {
        if (newdouble < 1.0 || newdouble > 2.0) {
          error("PGA: the rank bias must be strictly between 1 and 2.\n");
        } else {
          bias = newdouble;
        }
      }
      break;
      
    case CROSSOVER_RATE: /* -c<R> sets crossover rate */
      if (sscanf(arg, "%lf", &newdouble) == 1) {
        if (newdouble < 0 || newdouble > 1) {
          error("PGA: the crossover rate must be between 0 and 1.\n");
        } else {
          cross_rate = newdouble;
        }
      }
      break;
      
    case CROSSOVER_TYPE: /* -C<crossover> sets crossover operator */
      if (!strncmp(arg, "one", 3)) {
        cross_name = "one-point";
        crossover = one_pt_cross;
      } else if (!strncmp(arg, "two", 3)) {
        cross_name = "two-point";
        crossover = two_pt_cross;
      } else if (!strcmp(arg, "uniform")) {
        cross_name = "uniform";
        crossover = uniform_cross;
      } else if (!strcmp(arg, "none")) {
        cross_name = "none";
        crossover = no_cross;
      } else if (!strcmp(arg, "pmx")) {
	cross_name = "pmx";
	crossover = pmx_cross;
      } else if (!strcmp(arg, "ox")){
	cross_name = "ox";
	crossover = ox_cross;
      }
      else if (!strcmp(arg, "gox")){
	cross_name = "gox";
	crossover = gox_cross;
      }
      else if (!strcmp(arg, "gpx")){
	cross_name = "gpx";
	crossover = gpx_cross;
      }
      break;
      
    case EVAL: /* -e<problem> sets the problem */
      eval_name = (char *) strdup(arg);
      if (!strcmp(arg, "max")) {
        evaluate = eval_max;
        sprintf(geno_size_string, "%d", geno_size);
        maxfitness = geno_size_string;
      } else if (!strcmp(arg, "tsp")){
	evaluate = eval_tsp;
	permutation = TRUE;
        maxfitness = "unknown";
        mutate = mutate_SBM;
        mutate_name = "swap-based";
            /* set crossover to something credible if necessary */
        if(crossover == two_pt_cross) {
          crossover = pmx_cross;
          cross_name = "pmx";
        }
      } else if (!strcmp(arg, "dj1")) {
        evaluate = eval_dj1;
        maxfitness = "100";
      } else if (!strcmp(arg, "dj2")) {
        evaluate = eval_dj2;
        maxfitness = "1000";
      } else if (!strcmp(arg, "dj3")) {
        evaluate = eval_dj3;
        maxfitness = "55";
      } else if (!strcmp(arg, "dj5")) {
        evaluate = eval_dj5;
        maxfitness = "approx 499.001997";
      } else if (!strcmp(arg, "f6")) {
        evaluate = eval_f6;
        maxfitness = "1.0 (>0.999)";
      } else if (!strcmp(arg, "himm")) {
        evaluate = eval_himmelblau;
        maxfitness = "200";
      } else if (!strcmp(arg, "const")) {
        evaluate = eval_const;
        maxfitness = "constant 1.0";
      } else if (!strcmp(arg, "rand")) {
        evaluate = eval_rand;
        maxfitness = "1.0";
      } else if (!strcmp(arg, "knap")) {
        evaluate = eval_knap;
        maxfitness = "1.0";
      } else if (!strncmp(arg, "nk:",3)) {
        evaluate = eval_nk;
        maxfitness = "unknown, in 0..1";
        if(sscanf(arg+3,"%d+%d", &nk_k, &nk_seed) != 2) {
          error("PGA: mangled nk argument\n");
        }
      } else if (!strncmp(arg, "lp",2)) {
        evaluate = eval_lp;
        if(strlen(arg) > 2) {
          int k;
          k = atoi(arg+2);
          evaluate = eval_lp2;
          if(k < 3) {
            error("PGA: lpK requires K > 2\n");
          } else {
            lpmax = (double)(-k);
          }
        }
      } else if (!strncmp(arg, "tt", 2)) {
        evaluate = eval_tt;
        mutate = mutate_multi;
        mutate_name = "per-gene";
        if (strlen(arg) > 2 && *(arg + 2) == ':') {
          mutate = mutate_tt;
          mutate_name = "one-slot";
          if (sscanf(arg + 3, "%[^+]+%[^+]",
                     tt_eventchoice, tt_slotchoice) != 2) {
            error("PGA: mangled tt argument %s\n", arg);
          } else {
            switch (tt_eventchoice[0]) {
          case 'a':
              mutate = mutate_tt_all;
              mutate_name = "each-slot";
              break;
          case 'e':
              mutate = mutate_tt_each;
              mutate_name = "each-slot-by-score";
              break;
          case 'r':           /* random */
              find_bad_event = find_random_event;
              break;
          case 'w':
              find_bad_event = find_worst_event;
              break;
          case 't':
              find_bad_event = find_event_by_tn;
              tt_mtn1 = atoi(tt_eventchoice + 1);
              if (tt_mtn1 < 1) {
                error("PGA: tt:tN+.. has silly N\n");
              }
              break;
            default:
              error("PGA: tt:.. unrecognised event chooser\n");
            }
            switch (tt_slotchoice[0]) {
          case 'r':           /* random */
              find_good_slot = find_random_slot;
              break;
          case 'f':
              find_good_slot = find_free_slot;
              break;
          case 't':
              find_good_slot = find_slot_by_tn;
              tt_mtn2 = atoi(tt_slotchoice + 1);
              if (tt_mtn2 < 1) {
                error("PGA: tt:..+tN has silly N\n");
              }
              break;
          default:
              error("PGA: tt:.. unrecognised slot chooser\n");
            }
          }
        }
        maxfitness = "1.0";
      } else if (!strcmp(arg, "rr")) {
        evaluate = eval_rr;
        /* NB, maxfitness set after arg processing, once params known */
      } else if (!strncmp(arg, "mcb", 3)) {
        evaluate = eval_mcb;
        maxfitness = geno_size_string;
        mutate = mutate_multi;
        mutate_name = "per-gene";
        biggest_allele = atoi(arg + 3);
        if (biggest_allele < 1 || biggest_allele > 79) {
          warn("PGA: mcbK: K should be in [1..79], defaults to 2\n");
          biggest_allele = 2;
        }
      } else {
        error("PGA: unknown evaluation function: %s\n", arg);
      }
      break;
      
    case ELITE: /* -E<N> sets size of elite group */
      elite = atoi(arg);
      if(elite < 0) {
        error("PGA: elite size cannot be negative (%d)\n", elite);
      }
      break;
      
    case GRAY_CODING: /* -g sets Gray coding in binary-encoded problems */
      if(strcmp(arg,"true")==0)
        usegray = TRUE;
      else if(strcmp(arg,"false")==0)
        usegray = FALSE;
      else
        usegray = !usegray;
      break;
      
    case REPORT_INTERVAL: /* -i<N> sets reporting every N generations */
      if (sscanf(arg, "%d", &newint) == 1) {
        if (newint < 1) {
          error("PGA: A report every %d generations isn't sensible!\n",
                newint);
        } else {
          repinterval = newint;
        }
      }
      break;
      
    case INIT_OVERSIZE: /* -I<R> sets initial population oversize by factor R */
      if (sscanf(arg, "%lf", &oversizing) == 1) {
        if (oversizing < 1.0) {
          error("PGA: initial population oversized by %f < 1.0 is crazy!\n",
                oversizing);
        }
      }
      break;
      
    case MUTATION_RATE: /* -m<R> sets mutation rate */
      if (sscanf(arg, "%lf", &newdouble) == 1) {
        if (newdouble < 0 || newdouble > 1) {
          error("PGA: the mutation rate must be between 0 and 1.\n");
        } else {
          mute_rate = newdouble;
        }
      }
      break;

    case MIGRATION_INT: /* -M<N> sets migration interval */
      if ((reproduction == ss_gen_reproduction)
          || (reproduction == ss_one_reproduction)) {
        error("PGA: cannot use -M or -s with -rssoneN or -rssgenN\n");
      }
      if (sscanf(arg, "%d", &newint) == 1) {
        if (newint < 0) {
          error("PGA: A migration interval of %d makes no sense.\n", newint);
        } else {
          migint = newint;
        }
      }
      break;
      
    case CHROMO_LENGTH:    /* -n<N> sets chromosome length, if possible */
      if (sscanf(arg, "%d", &newint) == 1) {
        if (newint < 12) {
          error("PGA: Chromosome length must be at least 12.\n");
        } else {
          geno_size = newint;
          sprintf(geno_size_string, "%d", geno_size);
        }
      }
      break;
      
    case CHROMOS_PER_POP:    /* -p<N> sets population size */
      if (sscanf(arg, "%d", &newint) == 1) {
        if (newint < 2) {
          error("PGA: You must have at least 2 chromosomes per population.\n");
        } else {
          numchromo = newint;
        }
      }
      break;
      
    case POPS:    /* -P<N> sets N populations */
      if (sscanf(arg, "%d", &newint) == 1) {
        if (newint < 1) {
          error("PGA: You must have at least 1 population.\n");
        } else {
          numpop = newint;
        }
      }
      break;
      
    case REPRODUCTION: /* -r<reproduction> sets reproduction operator */
      if (!strcmp(arg, "one")) {
        reproduction = one_reproduction;
        repro_name = "one";
      } else if (!strcmp(arg, "gen")) {
        reproduction = gen_reproduction;
        repro_name = "gen";
      } else if (!strncmp(arg, "breed", 5)) {
        reproduction = breeder_reproduction;
        repro_name = (char *) strdup(arg);
        select_name = "*breeder*";
        breed_n = atoi(arg+5);
        if(breed_n < 2) {
          error("PGA: for breeder GA, breeding size must be > 1\n");
        }
      } else if (!strncmp(arg, "ssgen", 5)) {
        reproduction = ss_gen_reproduction;
        repro_name = (char *) strdup(arg);
        select_name = "*spatial*";
        ss_n = atoi(arg + 5);
        if (ss_n < 1) {
          warn("PGA: ss random walk must be of length > 0, set to 5\n");
          ss_n = 5;
        }
      } else if (!strncmp(arg, "ssone", 5)) {
        reproduction = ss_one_reproduction;
        repro_name = (char *) strdup(arg);
        select_name = "*spatial*";
        ss_n = atoi(arg + 5);
        if (ss_n < 1) {
          warn("PGA: ss random walk must be of length > 0, set to 5\n");
          ss_n = 5;
        }
      } else {
        error("PGA: don't know about %s reproduction.\n", arg);
      }
      break;
      
    case REPLACEMENT:
      if (!strncmp(arg,"parent",6)) {
        insert = replace_parent;
        replace_name = "parent";
      } else if (!strcmp(arg,"worst")) {
        insert = replace_worst;
        replace_name = "worst";
      } else if (!strncmp(arg,"hd",2)) {
        insert = replace_by_hd;
        if(strlen(arg)>2) {
          hd_set_size = atoi(arg+2);
          if(hd_set_size < 2) {
            warn("PGA: -RhdN requires N > 1; N set to 10\n");
            hd_set_size = 10;
          }
          replace_name = (char *)strdup(arg);
        } else replace_name = "hd10";
      }
      break;
        
    case SELECT: /* -s<select> sets selection operator */
      if ((reproduction == ss_gen_reproduction)
          || (reproduction == ss_one_reproduction)) {
        error("PGA: cannot use -s or -M with -rssN\n");
      }
      if (!strcmp(arg, "rank")) {
        selection = rank_select;
        select_name = "rank";
      } else if (!strcmp(arg, "fitprop")) {
        selection = fitprop_select;
        select_name = "fitprop";
      } else if (!strncmp(arg, "tm", 2)) {
        selection = tm_select;
        select_name = (char *) strdup(arg);
        tournament_size = atoi(arg + 2);
        if (tournament_size < 1) {
          warn("PGA: tm selection requires arg > 1; set to 10\n");
          tournament_size = 10;
        }
      } else if (!strncmp(arg, "tn", 2)) {
        selection = tn_select;
        select_name = (char *) strdup(arg);
        tournament_size = atoi(arg + 2);
        if (tournament_size < 1) {
          warn("PGA: tn selection requires arg > 1; set to 10\n");
          tournament_size = 10;
        }
      } else {
        error("PGA: I don't know about %s selection.\n", arg);
      }
      break;
      
    case RNG_SEED: /* -S<N> sets random seed */
      if (sscanf(arg, "%d", &newint) == 1) {
        if (newint < 0) {
          warn("PGA: random seed must be non-negative, has been set to 1.\n");
          newint = 1;
        }
        randseed = newint;
      }
      break;
      
    case TWINS: /* -t toggles twins flag */
      if(strcmp(arg,"true")==0)
        twins = TRUE;
      else if(strcmp(arg,"false")==0)
        twins = FALSE;
      else
        twins = !twins;
      break;

    case MUTATION_TYPE: /* -T<type> sets mutation type */
      if(!strcmp(arg,"s")) {
        mutate = mutate_SBM;
        mutate_name = "swap-based";
      } else if (!strcmp(arg,"o")) {
        mutate = mutate_OBM;
        mutate_name = "order-based";
      } else if (!strcmp(arg,"p")) {
        mutate = mutate_PBM;
        mutate_name = "position-based";
      } else if(!strcmp(arg,"per-gene")) {
            /* Don't set mutate in this branch! It will be either  */
            /* mutate_01 or mutate_multi or a mutate_tt_* but that */
            /* will be determined by the problem chosen.           */
        mutate_name = "per-gene";
      } else error("PGA: unknown type of mutation %s\n",arg);
      break;

    case MAX_FITNESS:
      break;
      
    default:
      error("PGA: code problem: some param/flag type is not handled properly"
            " in source file params.c\n");
    }
}

