/*          File: select.c
 *  Last changed: Wed Jul 12 13:57:14 2000
 *
 *  RCS: $Id: select.c,v 1.1 2006-12-12 10:25:22 tobibobi Exp $
 *
 *  Selection choices, Each takes a CHROMOSOME * pointing to array of all
 *  pops, and two integers giving base index and size of target pop; returns
 *  the selected CHROMOSOME.
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

/* select.c
 * Copyright (C) 1993, 1994 Peter Ross and Geoff Ballinger.
 * This is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License, see the file COPYING.
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "pga.h"

extern double bias;
extern double *total_abs_fitness;
extern int numchromo;
extern int tournament_size;

/************************************************************************/
/* This function performs rank based selection and is lifted from       */
/* Whitley's paper on GENITOR in icga 89. It selects from the specified */
/* subpopulation within 'pops'.                                         */
/************************************************************************/

CHROMOSOME
rank_select(CHROMOSOME *pops[], int base, int range)
{
  double value;

  if (bias < 1.05)
    value = drandom()/ bias;
  else {
    value = (bias - sqrt(bias * bias - 4.0 * (bias - 1.0) * drandom()))/ (2.0 * (bias - 1.0));
  }
  value = value * (double) range;

  return (pops[base][(int) value]);
}


/************************************************************************/
/* This function implements fitness proportionate selection. It treats  */
/* the fitness of the population as a "roulette wheel", rolls a ball on */
/* the wheel, and picks the chromosome within whose sector the ball     */
/* stops.                                                               */
/************************************************************************/

#pragma argsused
CHROMOSOME
fitprop_select(CHROMOSOME *pops[], int base, int range)
{
  int loop;
  double where;

  where = drandom()* total_abs_fitness[base];	/* Random float in 0..total */

  for (loop = 0; where > fabs(pops[base][loop].fitness); loop += 1)
    where -= fabs(pops[base][loop].fitness);

  return (pops[base][loop]);
}

/************************************************************************/
/* This fitness function performs a form of tournament selection as in  */
/* the classic marriage problem in dynamic programming. Select one at   */
/* uniform random, then continue for up to tournament_size tries making */
/* uniform random selections: choose the first that exceeds the fitness */
/* of that first-chosen. After tournament_size failures, choose first.  */
/************************************************************************/

#define ANY ((int)(drandom() * (double)range))

CHROMOSOME
tm_select(CHROMOSOME *pops[], int base, int range)
{
  int i, choice;
  double f;
  int reject = ANY;

  f = pops[base][reject].fitness;
  for (i = 0; i < tournament_size; i++) {
    choice = ANY;
    if (pops[base][choice].fitness > f)
      return (pops[base][choice]);
  }
  return (pops[base][reject]);
}

/************************************************************************/
/* This fitness function performs straight tournament selection as in   */
/* Brindle, Alberta TR 81-2. Pick tournament_size at uniform random,    */
/* return fittest of those.                                             */
/************************************************************************/

CHROMOSOME
tn_select(CHROMOSOME *pops[], int base, int range)
{
  int i, choice;
  double f;
  int one = ANY;

  f = pops[base][one].fitness;
  for (i = 0; i < tournament_size - 1; i++) {
    choice = ANY;
    if (pops[base][choice].fitness > f) {
      one = choice;
      f = pops[base][one].fitness;
    }
  }
  return (pops[base][one]);
}


