/*          File: version.h
 *  Last changed: Wed Sep 13 22:20:56 2000
 *
 *  RCS: $Id: version.h,v 1.1 2006-12-12 10:25:23 tobibobi Exp $
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/



#define TITLE    "PGA: pseudo-parallel genetic algorithm testbed"
#define VERSION  "4.0"

/*
 * TO DO: Change the interactive GUI from curses-based to X-based, but using
 *   the FLTK library available from http://www.fltk.org. This has the
 *   huge advantage that the same source code should compile for Windows
 *   95/98/2000/NT, and the FLTK library stuff is also GPL and available
 *   for download at no charge. xpga, derived from 2.9, used XForms for
 *   which the source code was not readily available and which was not
 *   Microsoft-able.
 *
 * Revision 4.0 2000/09/14 pmr
 * Added in the extras by EH which had been part of xpga, which
 *   was built from 2.9 and therefore missing the 3.0 and 3.1 extensions.
 *   Includes choice of replacement policy, and permutation-based stuff
 *   for TSP etc.
 * Parameters can be read from file first, overidden by command-line
 *   flags.
 * Much tidying of the code. Minor bug-fixes relating to some extreme cases.
 *
 * --- version jumps to 4.0, to mark major code tidying and additions
 *
 * Revision 3.1 97/12/20 pmr
 * Added -ett:a+... to make mutation apply to each timetable event,
 *   rather than just a single one. The `a' is for `all'.
 * Added -ett:e+... to make mutations apply to each timetable event,
 *   but with probability scaled by the penalty that the event causes.
 *   In both a+.. and e+.. cases, the basic per-event rate is set by the
 *   -m option.
 * Added Himmelblau function, -ehimm
 * **** FIXED A HORRIFIC PROBLEM WITH BERKELEY'S RANDOM NUMBER GENERATOR.
 *      It turned out that although a sequence of calls of bsd_random()&01
 *      would generate a sequence of random-looking bits, according to the
 *      standard sorts of test, a bug in the code meant that you could only
 *      ever get four such sequences! And (bsd_random()>>k)&01 could produce
 *      only 2^(k+2) possible sequences.
 *
 * Revision 3.0 96/11/21 pmr
 * Default mutation rate now 1/length.
 * DJ functions all have origin offset by 0.053 on each axis -- maximum
 *   is otherwise located at origin, with too distinctive a bit pattern
 *   for complete credibility.
 * Fixed silly bug in dj5, thanks to Dae Gyu Kim
 * Added -I<n> option: initialise by making a pop of size
 *   popsize*n, then take popsize best. n is a real.
 * Added NK landscapes, random eval, constant eval. These are
 *   useful for studies of selection pressure, comparisons
 *   of different GAs etc. The NK landscape stuff uses Terry Jones'
 *   implementation, by FTP from Santa Fe in /pub/terry/nk/.
 *   The random eval gives a random fitness in 0..1 to any chromosome,
 *   but equal chromosomes get equal fitness. The constant eval
 *   gives fitness 1 to all chromosomes.
 *
 * Revision 2.9 96/3/18 pmr
 * Added (R)estart option to begin again with same seed as at first,
 *   and (A)gain became (N)ew start
 * Added long-path problem, geno size coerced to be odd, including
 *   variant lpK whose path end-point is not a mere two bits
 *   away from path start. This is done by truncating the path
 *   by (K-2) steps, so that if K <= geno size the end of the
 *   path is K bits different from the start.
 * Added breeder GA code -rbreedN, where N is breeding group size.
 * Tidied some definitions to cater for porting problems; now
 *   work OK with GCC on MSDOS.
 * Can write a map of a spatially-structured problem even if
 *   no output file specified; goes to unnamed.map.
 * Convergence criterion fixed. Used to be that (average fitness
 *   = best fitness) in each population, but as Dave Corne pointed
 *   out this fails rwadily for some cases, eg mcb-type problems.
 *   So now the test is (average=best) && (all chromos equal), so
 *   perfomance impact is negligible.
 * Two bugs, fixed thanks to Juergen Branke and class at Karlsruhe:
 *   - problem if migration interval smaller than reporting interval
 *   - fitprop calculation bug.
 *
 * Revision 2.8  95/3/28 pmr
 * Convert to ANSI C.
 * Pga and decode sources to separate directories.
 * Fixed bug in mutation, was unable to get largest allele value.
 * Eval count now includes calls during initialisation.
 * Interesting bug in Gnu CC on HP-9000 discovered: non_adaptive()
 *   was being optimised out of existence, since it merely returned the
 *   value of mutation rate, but the compiler fouled up the return value
 *   as seen by the reproduction code. Thus reproduction was being told
 *   that the mutation probability was much larger than 1 .. causing
 *   all bits of a bit-string to invert!
 *
 * Revision 2.7  94/6/27 pmr
 * Extended timetabling a bit, so mutation can be `clever'. It selects
 *   an event to move and a slot to move it to, using user-specified
 *   methods: -ett:E+S where E sets event selection:
 *       r   random
 *       w   roulette wheel choice of bad offender
 *       tM  use tournament selection size M to pick bad offender
 *   and where S sets slot selection:
 *       r   random
 *       f   free slot (ie unconstrained by this event) else random
 *       tN  use tournament selection size N to pick good slot  
 * Extended non-interactive options: -No<n> is like -NO<n> and -Na<n>
 *   is like -NA<n>, except they also save the chromosomes at the end.
 *   Also, a converged population counts, since it won't ever reach the
 *   threshold <n>.
 * Can specify a different datafile (rather that weights/rrdata/ttdata)
 *   by -Fdatafile option.
 * Can omit crossover, by -Cnone; good for testing null hypotheses.
 * Yet more code-shuffling.
 * 
 * Revision 2.6  94/6/8 pmr
 * Added simple timetabling (`tt') by penalty function approach. Problem 
 *   described in separate file ttdata lets you specify:
 *   - how many slots, events and days
 *   - the various penalties
 *   - which events should not clash
 *   - which events should come before others
 *   - which slot(s) an event must/must not be in
 *   - how to handle near-clashes: close can count as bad
 *   - whether clash constraints are per-student or should be unique.
 *   Two real problems included: send me yours!
 * Added `mcbK' problem, has (K+1) non-overlapping maxima: handy
 *   for looking at multiple-solution issues. Suggested by Dave Corne.
 * One or two bugs fixed (found by Dave Corne, dave@aifh.ed.ac.uk).
 * Gray code option (-g) for function optimisation problems.
 * Can save a 2-d map showing where the best chromosomes are
 *   in spatial selection. The top 26 distinct chrosmomes get shown.
 * More informative command-line prompting.
 * Can change generations per stage interactively, for more
 *   delicate investigations when things get interesting.
 * Save random number seed too in report file, so runs can be replicated.
 * Can always save chromosomes, even if you didn't name a file on command line.
 * A `stage' (-l flag) ends prematurely if all populations appear to
 *   have converged; cannot continue beyond convergence.
 * Non-interactive flag (-N) suppresses screen output, so you can run
 *   batch jobs and consume all file space. Program then ends when
 *   -l limit is reached or when fittest in one or all populations 
 *   have passed a threshold, if that happens first (use -NO<n> or -NA<n>)
 * Some type conversion warnings and a bug in screen update pointed
 *   out by Rick Olson (rolson@edu.luc.it.orion)
 *
 * Revision 2.5  93/9/17 pmr
 * Split into several C files for convenience.
 * Chromo dump also numbers chromos within population, to make 3-d
 *   plots easier to construct from a dump. Also makes it easier
 *   to summarise a complete population using a (g)awk script.
 * Marriage tournament selection (-stmN): choose one at random, have
 *   up to N tries to find one fitter, stop on first fitter. If none,
 *   return that initial choice. Based on `marriage' problem, classic
 *   of the dynamic programing literature.
 * Original tournament selection (-stnN): choose N at random, return
 *   fittest.
 * Allow seeding of random number generator (-SN).
 * Proper Royal Road function now (-err); parameter data is read from a file
 *   called rrdata, or else uses Holland's defaults.
 * Changed number reading: now when reading weights or rrdata file, all
 *   non-numeric text is just ignored. And no keyboard echo now.
 * Can choose whether crossover produces one child or two (-t)
 * Spatially organised reproduction (-rssN) using random walk to find
 *   parents, modelled on Wright's shifting balance model of evolution.
 *   Requires specific selection routine, and migration disallowed.
 * Bug fix thanks to Willem Vermin: free_pops no longer frees anything
 *   twice, as some OSs object fatally to this.
 * Bug fix thanks to Willem Vermin: insert should also deduct fitness
 *   of least fit member from total_abs_fitness when that member gets
 *   removed
 * Blunder pointed out by Dave Corne: default crossover was 1-pt, but
 *   shown as 2-pt.
 *
 * Revision 2.4  93/6/29 pmr
 * Added `Royal road' problem, user-selectable block size (-errK, K int)
 * Fixed minor problem in fitprop_select(), copes with tiny/negative
 *   fitnesses (negative..??)
 * Fitprop_select() doesn't recalculate total fitness every time now;
 *   just updated on insertion and in gen reproduction.
 * If saving to file, can save chromosomes too (in "file.chr")
 * Track chromosome parentage.
 *
 * Revision 2.3  93/6/18 pmr
 * Added one-point and uniform crossovers, (-C option)
 * First public (non-Edinburgh) release.
 *
 * Revision 2.2  93/6/2  pmr
 * Added 1-d knapsack problem, info from file `weights'
 * Modified screen layout a little
 *
 * Revision 2.1  93/3/18 pmr
 * Switch to char array for chromosomes, user-selectable length
 * Some code tidying
 *
 * Revision 2.0  92/2/27 pmr
 * Basic curses output
 * Added problem akin to binary F6 of Schaffer et al
 * Fixed problem with gen reproduction
 * Output to file too (append) if named
 * Fixed problem with sorting of chromosomes
 * GHB's original code has all been displaced, by this point.
 *
 * Revision 1.9  92/2/27 pmr
 * Some code and output tidying
 * Fixed bias problems
 *
 * Revision 1.8  90/11/23  10:24:33  ghb
 * Fitness is now a double.
 * Added De Jong's functions 1,2,3 and 5.
 * No longer display the best genotype.
 * Limit by evaluations, not generations.
 * Fixed bugs in generation based reproduction.
 * Adaptive mutation not allowed in generation based reproduction.
 * Fixed F5 using Genesis source as guide.
 * 
 * Revision 1.7  90/11/18  00:04:00  ghb
 * Deal in generations between migrations, not migrations per generation.
 * Fixed output bug.
 * Revision number in help screen.
 * 
 * Revision 1.6  90/11/14  11:51:49  ghb
 * Wrote replacement for drand48.
 * Adaptive mutation flag now works!
 * Added fitprop selection and gen reproduction.
 * Added user defined migration size.
 * 
 * Revision 1.5  90/11/13  10:16:32  ghb
 * Re-worked command line handling.
 * Allow definable output file, operators, etc.
 * Removed "PGA: " from normal outputs.
 * Added help option.
 * 
 * Revision 1.4  90/11/12  08:56:48  ghb
 * Comments added.
 * 
 * Revision 1.3  90/11/09  13:30:42  ghb
 * Fixed Insertion to insert new best member.
 * It Works!
 * 
 * Revision 1.2  90/11/09  10:13:09  ghb
 * Shortened some variable names to make things more readable.
 * Limit number of generations, not number of evaluations.
 * Insertion, Selection, Reproduction, Crossover and Mutation!
 * 
 * Revision 1.1  90/11/07  21:27:02  ghb
 * Initial revision
 * 
*/


