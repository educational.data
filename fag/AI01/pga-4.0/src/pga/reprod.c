/*          File: reprod.c
 *  Last changed: Thu Apr 19 15:25:52 2001
 *
 *  RCS: $Id: reprod.c,v 1.1 2006-12-12 10:25:22 tobibobi Exp $
 *
 *  Reproduction choices. Each takes a CHROMOSOME * pointing to the array
 *  of populations, and two ints giving base index and size of target pop
 *  in that array. Returns nothing.
 */


/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/

#define TRUE 1
#define FALSE 0

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "pga.h"

extern CHROMOSOME(*selection) (CHROMOSOME *[], int, int);
extern CHROMOPAIR(*crossover) (CHROMOSOME, CHROMOSOME);
extern CHROMOSOME(*mutate) (CHROMOSOME, double);
extern double (*mrate) (CHROMOSOME, CHROMOSOME);
extern double (*evaluate) (GENE *);
extern void (*insert) (CHROMOSOME *[], CHROMOSOME, int, int); 
extern double mute_rate;
extern double cross_rate;
extern double *total_abs_fitness;
extern int numchromo;
extern int twins;
extern int elite;
extern int ss_xmax, ss_ymax, ss_n;
extern int breed_n;

/************************************************************************/
/* This function performs one-at-a-time reproduction on the specified   */
/* subpopulation. Adaptive mutation is used if activated.               */
/************************************************************************/

void one_reproduction(CHROMOSOME *pops[], int base, int range)
{
  CHROMOSOME parent1, parent2, child;
  CHROMOPAIR children;

  parent1 = selection(pops, base, range);	 /* Select two parents. */
  parent2 = selection(pops, base, range);

  children = crossover(parent1, parent2);	          /* Cross them */
  child = mutate(children.child1, mrate(parent1, parent2));  /*  mutate */
  child.fitness = evaluate(child.gdp->genotype); /* Evaluate the child. */
  insert(pops, child, base, range);	       /* Insert the child into */
                                                     /* the population. */
  if (twins) {
    child = mutate(children.child2, mrate(parent1, parent2)); /* mutate */
    child.fitness = evaluate(child.gdp->genotype);   /* Evaluate child. */
    insert(pops, child, base, range);	       /* Insert the child into */
                                                     /* the population. */
  }
}

/************************************************************************/
/* This function performs generation based reproduction. A new          */
/* population is built by selecting members of the old one, possibly    */
/* crossing them with another, and possibly mutating them. This new     */
/* population is sorted and then used to replace the old population.    */
/************************************************************************/

void gen_reproduction(CHROMOSOME *pops[], int base, int range)
{
  CHROMOSOME *newpop, parent1, parent2;
  CHROMOPAIR children;
  int loop, reeval;

  newpop = (CHROMOSOME *) MALLOC(sizeof(CHROMOSOME) * range);

  /* Keep top dog of old population for the moment, ie elitism */
  for(loop=0; loop < elite; loop++) {
    newpop[loop] = pops[base][0];
    newpop[loop].gdp->refcount++;
  }

  for (; loop < range;) {
    reeval = FALSE;
    parent1 = selection(pops, base, range);
    parent2 = selection(pops, base, range);
    if (drandom()<= cross_rate) {
      reeval = TRUE;
      children = crossover(parent1, parent2);
      children.child1 = mutate(children.child1, mrate(parent1, parent2));
      if (twins)
	children.child2 = mutate(children.child2, mrate(parent1, parent2));
    } else {
      children.child1 = parent1;
      if (twins)
	children.child2 = parent2;
    }

    /* By this point, child may have refcount 0 if product of crossover */
    /* and/or of mutation; or may have refcount > 0 otherwise.          */
    /* Increment child's refcount, so it is 1 if completely new, >1     */
    /* if in old population too.                                        */
    children.child1.gdp->refcount++;
    if (twins)
      children.child2.gdp->refcount++;

    if (reeval) {
      children.child1.fitness = evaluate(children.child1.gdp->genotype);
      if (twins)
	children.child2.fitness = evaluate(children.child2.gdp->genotype);
    }
    newpop[loop++] = children.child1;
    if (twins && loop < range)
      newpop[loop++] = children.child2;
  }

  qsort((char *) newpop, range, sizeof(CHROMOSOME), compchromo);

  total_abs_fitness[base] = 0.0;
  for (loop = 0; loop < range; loop++) {
    /* If old member is not wanted in new population or elsewhere, its */
    /* refcount will be 1. Decrement ref counts in old pop, ditch the  */
    /* unwanted. */
    if (pops[base][loop].gdp->refcount == 1) {
      free(pops[base][loop].gdp->genotype);
      free(pops[base][loop].gdp);
    } else
      pops[base][loop].gdp->refcount--;

    pops[base][loop] = newpop[loop];
    total_abs_fitness[base] += fabs(newpop[loop].fitness);
  }

  free(newpop);
}

/**********************************************************************/
/* This function performs breeder-GA reproduction. A new population   */
/* population is built by selecting members at random from within the */
/* top breed_n of the current population, and possibly crossing and   */
/* mutating them according to cross-rate, until a new population is   */
/* built. See Muehlenbein and Schlierkamp-Voosen paper in ICGA-93     */
/**********************************************************************/

void breeder_reproduction(CHROMOSOME *pops[], int base, int range)
{
  CHROMOSOME *newpop, parent1, parent2;
  CHROMOPAIR children;
  int loop, reeval;
  double value, value2;

  newpop = (CHROMOSOME *) MALLOC(sizeof(CHROMOSOME) * range);

  /* Keep top dog of old population for the moment, ie elitism */
  for(loop=0; loop < elite; loop++) {
    newpop[loop] = pops[base][0];
    newpop[loop].gdp->refcount++;
  }

  /* Fill out rest of new population: */
  for (; loop < range;) {
    reeval = FALSE;
    value = drandom()*breed_n;
    parent1 = pops[base][(int)value];
    value2 = drandom()*breed_n;
    while(floor(value2) == floor(value)) /* choose DIFFERENT value2 */
      value2 = drandom()*breed_n;
    parent2 = pops[base][(int)value2];
    if (drandom()<= cross_rate) {
      reeval = TRUE;
      children = crossover(parent1, parent2);
      children.child1 = mutate(children.child1, mrate(parent1, parent2));
      if (twins)
	children.child2 = mutate(children.child2, mrate(parent1, parent2));
    } else {
      children.child1 = parent1;
      if (twins)
	children.child2 = parent2;
    }

    /* By this point, child may have refcount 0 if product of crossover */
    /* and/or of mutation; or may have refcount > 0 otherwise.          */
    /* Increment child's refcount, so it is 1 if completely new, >1     */
    /* if in old population too.                                        */
    children.child1.gdp->refcount++;
    if (twins)
      children.child2.gdp->refcount++;

    if (reeval) {
      children.child1.fitness = evaluate(children.child1.gdp->genotype);
      if (twins)
	children.child2.fitness = evaluate(children.child2.gdp->genotype);
    }
    newpop[loop++] = children.child1;
    if (twins && loop < range)
      newpop[loop++] = children.child2;
  }

  qsort((char *) newpop, range, sizeof(CHROMOSOME), compchromo);

  total_abs_fitness[base] = 0.0;
  for (loop = 0; loop < range; loop++) {
    /* If old member is not wanted in new population or elsewhere, its */
    /* refcount will be 1. Decrement ref counts in old pop, ditch the  */
    /* unwanted. */
    if (pops[base][loop].gdp->refcount == 1) {
      free(pops[base][loop].gdp->genotype);
      free(pops[base][loop].gdp);
    } else
      pops[base][loop].gdp->refcount--;

    pops[base][loop] = newpop[loop];
    total_abs_fitness[base] += fabs(newpop[loop].fitness);
  }

  free(newpop);
}

/* This does `spatial selection' reproduction, distantly based on Wright's
 * shifting-balance model of evolution. The much more common panmictic model
 * is Fisher's in which the entire population competes in selection - eg
 * in roulette-wheel or rank-based selection, the fitness of all members
 * of the population is implicitly used in each selection. In Wright's
 * model, selection need only consider chromosomes in some (2-D) neighbourhood
 * each time. It seems to make sense to use a 2-D grid rather than 3-D or
 * higher; given sufficiently high dimension, every point would be an
 * immediate neighbour of every other so it would be a form of panmictic
 * evolution again.
 *
 * In this implementation, the chromosomes are regarded as being arranged
 * in a 2-D toroidal grid; so first the population size is first automatically
 * adjusted so that the grid can be rectangular and roughly square.
 *
 * Reproduction is generational and proceeds as follows. Each chromosome
 * is replaced by the result of crossover and mutation applied to two parents
 * chosen as the fittest met on a random walk of length ss_n starting from
 * the place where the child will be installed. If you use the -t (twins)
 * flag location (x,y) and location (x,y+1) (if it exists) get used to
 * store the children.
 *
 * This spatial selection reproduction utilises specific selection and
 * insertion algorithms, so you cannot use the -s option with it.
 * /


/*******************************************************************/
/* This chooses a location in the neighbourhood of given x,y using */
/* a random walk of length ss_n. Choose the fittest found, ties    */
/* are broken by choosing last.                                    */
/*******************************************************************/

/* The helps cut down on the number of drandom() calls. */
/* Returns random integer in range [0,7].               */
int rand_dir_bits = 0;
int rand_dir_store = 0;
int rand_dir(void)
{
  int direction;

  if (rand_dir_bits == 0) {
    rand_dir_store = (int) (drandom()* 16777216.0);
    rand_dir_bits = 24;
  }
  direction = rand_dir_store & 0x07;
  rand_dir_store = rand_dir_store >> 3;
  rand_dir_bits -= 3;
  return (direction);
}

CHROMOSOME
ss_select(CHROMOSOME *pops[], int base, int x, int y)
{
  int i;
  CHROMOSOME chosen;
  double f;

  chosen = pops[base][LOCATION(x, y)];
  f = chosen.fitness;
  for (i = 0; i < ss_n; i++) {
    switch (rand_dir()){
    case 0:
      y -= 1;
      break;
    case 1:
      x += 1;
      y -= 1;
      break;
    case 2:
      x += 1;
      break;
    case 3:
      x += 1;
      y += 1;
      break;
    case 4:
      y += 1;
      break;
    case 5:
      x -= 1;
      y += 1;
      break;
    case 6:
      x -= 1;
      break;
    case 7:
      x -= 1;
      y -= 1;
      break;
    }

    /* Note that it is probably cheaper to do the next two lines */
    /* than to use conditional tests of whether x or y is out of */
    /* bounds.                                                   */
    x = (x + ss_xmax) % ss_xmax;
    y = (y + ss_ymax) % ss_ymax;

    if (pops[base][LOCATION(x, y)].fitness >= f) {
      chosen = pops[base][LOCATION(x, y)];
      f = chosen.fitness;
    }
  }
  return (chosen);
}

/*******************************************************************/
/* Spatial selection. Uses specific selection and insertion, so    */
/* you cannot set them independently. Generational version.        */
/*******************************************************************/

void ss_gen_reproduction(CHROMOSOME *pops[], int base, int range)
{
  int x, y;
  CHROMOSOME parent1, parent2;
  CHROMOPAIR children;
  CHROMOSOME *newpop;

  /* Somewhere to keep the new population, so that all selection is */
  /* from the old population.                                       */
  newpop = (CHROMOSOME *) MALLOC(range * sizeof(CHROMOSOME));

  /* Scan the grid, doing selection and insertion */
  for (x = 0; x < ss_xmax; x++) {
    for (y = 0; y < ss_ymax;) {
      parent1 = ss_select(pops, base, x, y);
      parent2 = ss_select(pops, base, x, y);
      children = crossover(parent1, parent2);
      children.child1 = mutate(children.child1, mrate(parent1, parent2));
      children.child1.fitness = evaluate(children.child1.gdp->genotype);
      children.child1.gdp->refcount++;
      newpop[y * ss_xmax + x] = children.child1;
      y++;
      /* If twins, put the second chromosome just below the first if */
      /* there is a place to put it. Don't wrap; top is already new. */
      if (twins && y < ss_ymax) {
	children.child2 = mutate(children.child2, mrate(parent1, parent2));
	children.child2.fitness = evaluate(children.child2.gdp->genotype);
	children.child2.gdp->refcount++;
	newpop[y * ss_xmax + x] = children.child2;
	y++;
      }
    }
  }

  /* Copy new that is better into old, freeing any unwanted old stuff. */
  for (x = 0; x < numchromo; x++) {
    if (pops[base][x].fitness < newpop[x].fitness) {
      if (pops[base][x].gdp->refcount == 1) {
	free(pops[base][x].gdp->genotype);
	free(pops[base][x].gdp);
      } else
	pops[base][x].gdp->refcount--;
      pops[base][x] = newpop[x];
    } else {
      free(newpop[x].gdp->genotype);
      free(newpop[x].gdp);
    }
  }

  free(newpop);
}

/*******************************************************************/
/* Spatial selection. Uses specific selection and insertion, so    */
/* you cannot set them independently. One (Genitor-ish) version.   */
/*******************************************************************/

#pragma argsused
void ss_one_reproduction(CHROMOSOME *pops[], int base, int range)
{
  int x, y;
  CHROMOSOME parent1, parent2;
  CHROMOPAIR children;

  x = (int) (drandom()* (double) ss_xmax);
  y = (int) (drandom()* (double) ss_ymax);

  if (twins) {			/* Twins occupy adjacent sites vertically */
    y = (y >> 1) << 1;
  }
  parent1 = ss_select(pops, base, x, y);
  parent2 = ss_select(pops, base, x, y);
  children = crossover(parent1, parent2);
  children.child1 = mutate(children.child1, mrate(parent1, parent2));
  children.child1.fitness = evaluate(children.child1.gdp->genotype);
  if (pops[base][LOCATION(x, y)].fitness < children.child1.fitness) {
    children.child1.gdp->refcount++;
    if (pops[base][LOCATION(x, y)].gdp->refcount == 1) {
      free(pops[base][LOCATION(x, y)].gdp->genotype);
      free(pops[base][LOCATION(x, y)].gdp);
    } else
      pops[base][LOCATION(x, y)].gdp->refcount--;
    pops[base][LOCATION(x, y)] = children.child1;
  } else {
    free(children.child1.gdp->genotype);
    free(children.child1.gdp);
  }

  /* If twins, put the second chromosome just below the first. But don't */
  /* wrap, top may be valuable and unrelated.                            */
  y++;
  if (twins && y < ss_ymax) {
    if (pops[base][LOCATION(x, y)].fitness < children.child2.fitness) {
      children.child2.gdp->refcount++;
      if (pops[base][LOCATION(x, y)].gdp->refcount == 1) {
	free(pops[base][LOCATION(x, y)].gdp->genotype);
	free(pops[base][LOCATION(x, y)].gdp);
      } else
	pops[base][LOCATION(x, y)].gdp->refcount--;
      pops[base][LOCATION(x, y)] = children.child2;
    } else {
      free(children.child2.gdp->genotype);
      free(children.child2.gdp);
    }
  }
}


