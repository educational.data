/*          File: utils.c
 *  Last changed: Tue Jul 18 11:07:14 2000
 *
 *  RCS: $Id: utils.c,v 1.1 2006-12-12 10:25:23 tobibobi Exp $
 *
 *  Utility functions.
 */

/*****************************************************************
 *       PGA - Pseudo-Parallel Genetic Algorithm Testbed         *
 *                                                               *
 *           By Peter Ross (peter@dcs.napier.ac.uk),             *
 *              Emma Hart (emmah@dcs.napier.ac.uk)               *
 *                                                               *
 * Based on an older program by Geoff Ballinger (geoff@ed.ac.uk) *
 *****************************************************************/
/*****************************************************************
 * Copyright (C) 2000 by Peter Ross, Emma Hart.                  *
 *                                                               *
 *   This program is free software; you can redistribute it      *
 * and/or modify it under the terms of the GNU General Public    *
 * License as published by the Free Software Foundation; either  *
 * version 2 of the License, or (at your option) any later       *
 * version.                                                      *
 *   This program is distributed in the hope that it will be     *
 * useful, but WITHOUT ANY WARRANTY; without even the implied    *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR       *
 * PURPOSE. See the GNU General Public License for more details. *
 *   You should have received a copy of the GNU General Public   *
 * License along with this program; if not, write to the Free    *
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, *
 * USA.                                                          *
 *   [GNU GPL should be in the file named COPYING].              *
 *****************************************************************/


#include <stdio.h>
#include <stdarg.h>

/***********************************************************/
/* error(format_string, args)                              */
/* behaves like fprintf(stderr,...) and then exits         */
/***********************************************************/
void error(char *s, ...)
{
  va_list args;
  va_start(args, s);
  vfprintf(stderr, s, args);
  va_end(args);
  exit(1);
}

/***********************************************************/
/* warn(format_string, args)                               */
/* behaves like fprintf(stderr,...) but does not exit      */
/***********************************************************/
void warn(char *s, ...)
{
  va_list args;
  va_start(args, s);
  vfprintf(stderr, s, args);
  va_end(args);
}

/***********************************************************/
/* mmalloc(bytes,message)                                  */
/* behaves like malloc(bytes) but writes a message and     */
/* exits if there is not enough memory available.          */
/*   This is mainly used via a macro such as               */
/* #define MALLOC(n) mmalloc((n),__LINE__,__FILE__)        */
/***********************************************************/
void *mmalloc(size_t n, int line, char *file)
{
  void *p;
  if((p = (void *)malloc(n)) == NULL) {
    fprintf(stderr, "malloc could not get %d bytes: see line %d of file %s\n",
            n, line, file);
    exit(1);
  }
  return(p);
}



