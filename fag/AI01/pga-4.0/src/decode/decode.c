/* decode.c
 *
 * Copyright (C) 1994 Peter Ross
 * This is free software; you can redistribute it and/or modify it under the
 * terms of the GNU General Public License, see the file COPYING.
 *
 * Stand-alone filter to turn chromosome strings back into
 * sets of numbers, or into timetables.
 *  Usage:  decode [-g] n lower upper
 *          decode -t [-Fdatafile]
 *          decode -h
 * Use -g for Gray (de)code. n is number of numbers per chromosome,
 *   lower and upper give range to scale to.
 * Use -t for timetabling, to recover (stylised) timetable. It will
 *   read the file ttdata, or the file you specify.
 *   The report tells you about the number of constraints of each type,
 *   and then, for each chromosome, it tells you which constraints are
 *   specifically violated, it tells you the total penalty for the
 *   chromosome and finally it prints a human-readable version of the
 *   timetable in order of slots. Within each slot the events assigned
 *   to that slot are reported in order of event number.
 * Use -h for brief help.
 */

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include "pga.h"
#include "proto.h"

#define TRUE 1
#define FALSE 0

int geno_size;
int biggest_allele = 1;
int tt_maxevents = 0;
int tt_maxslots = 0;
int tt_days = 1;
int tt_slots_per_day;
int tt_spread = 1;
int tt_clash_type = 1;
char *datafilename = TTDATAFILE;
double tt_penalty_clash = 10.0;
double tt_penalty_order = 3.0;
double tt_penalty_exclude = 5.0;
double tt_penalty_consec = 1.0;


/************************************************************************/
/* Treat bit pattern as though it were a Gray code, recover integer.    */
/* This algorithm courtesy of Achille Hui, Stanford Physics Dept.       */
/* bin2g(b) is just b^(b>>1) as usual. This method is standard, and     */
/* of course ensures that a number and its Gray code version occupy the */
/* same width.                                                          */
/************************************************************************/

unsigned long 
g2bin (unsigned long g)
{
  unsigned long t = g;
  t ^= t>>1;
  t ^= t>>2;
  t ^= t>>4;
  t ^= t>>8;
  t ^= t>>16;
  return(t);
}

/************************************************************************/
/* This decodes the requested number of real parameters from the given  */
/* chromosome.                                                          */
/************************************************************************/

void 
ldecode (char *genotype, int number, double lower, double upper, int usegray)
{
  int loop,count,bits,geno_size;
  unsigned long value;

  geno_size = strlen(genotype);
  bits=(geno_size/number);

  for(loop=0; loop<number; loop+=1) {
    value=0;
    for(count=0; count<bits; count++) {
      value = 2*value + (int)(genotype[count+loop*bits] - '0');
    }
    if(usegray)
      value = g2bin(value);
    printf(" %14.9f",
           ((upper-lower)*((double)value)/pow(2.0,(double)bits))+lower);
  }
  printf("\n");
}  


/***************************************************************/
/* Get the next number from the input: put it in the location  */
/* addressed by second argument. This function returns 0 on    */
/* EOF. If stopateol is true, it returns -1 when it hits \n    */
/* (after which some other procedure has to read past the \n), */
/* otherwise it continues looking for the next number.         */
/* A number has an optional sign, perhaps followed by digits,  */
/* perhaps followed by a decimal point, perhaps followed by    */
/* more digits. There must be a digit somewhere for it to count*/
/* as a number. So it would read any of:                       */
/*  -.5                                                        */
/*  -0.5                                                       */
/*  -.5.7                                                      */
/* as minus-a-half. In the last case, it would read .7 next    */
/* time around.                                                */
/*   There doesn't seem to be a neat and reliable way to do    */
/* all this, including stopateol, using scanf?                 */
/***************************************************************/

double 
getdouble (FILE *file, double *valaddr, int stopateol)
{
  int c;
  int found = FALSE, indecimal = FALSE;
  int sign = +1;
  double n = 0.0, p = 1.0;

  /* First find what looks like start of a number - the first digit. */
  /* And note any sign and whether we just passed a decimal point.   */  
  do {
    c = fgetc(file);
    if(c == EOF) return (0);
    else if(stopateol && c =='\n') return(-1);
    else if(c == '+' || c == '-') {
      sign = (c == '+')? +1 : -1;
      c = fgetc(file);
      if(c == EOF) return (0);
      else if(stopateol && c =='\n') return(-1);
    }
    if(c == '.') {
      indecimal = TRUE;
      c = fgetc(file);
      if(c == EOF) return (0);
      else if(stopateol && c =='\n') return(-1);
    }
    if(c >= '0' && c <= '9') {
      found = TRUE;
    } else {
      sign = +1;
      indecimal =  FALSE;
    }
  } while(!found);

  /* Now we've got digit(s) ... */
  do {
    n = 10.0*n + c - '0';
    p = 10.0*p;
    c = fgetc(file);

    if((c < '0') || (c > '9')) {
      found = FALSE;
      /* We've run out. If we already saw a decimal point, return now */
      if(indecimal) {
      	if(c != EOF) ungetc(c,file);
        *valaddr =  sign * n/p;
      	return(1);
      } else p = 1.0;
    }
  } while(found);

  /* We ran out and we didn't see a decimal point, so is this a decimal? */
  if(c != '.') {
    /* No, give it back to caller */
    if(c != EOF) ungetc(c,file);
    *valaddr = sign * n;
    return(1);
  } else {
    /* It is. Step past it, carry on hoping for more digits */
    c = fgetc(file);
    while(c >= '0' && c <= '9') {
      n = 10.0*n + c - '0';
      p = p*10.0;
      c =  fgetc(file);
    }
    /* We've run out of digits but we have a number to give */
    if(c != EOF) ungetc(c,file);
    *valaddr = sign * n/p;
    return(1);
  }
}

/* Use getdouble() above but convert result to int. */
int 
getint (FILE *f, int *valaddr, int stopateol)
{
  int r;
  double x;
  r = getdouble(f,&x,stopateol);
  *valaddr = (int)x;
  return(r);
}

/* Use getdouble above but convert result to long. */
int 
getlong (FILE *f, long *valaddr, int stopateol)
{
  int r;
  double x;
  r = getdouble(f,&x,stopateol);
  *valaddr = (long)x;
  return(r);
}



/***********************************************************************/
/* Timetabling stuff. The problem is defined in file ttdata, the       */
/* chromosome represents an array of the events to be placed, the      */
/* value of an allele is the slot into which that event is placed.     */
/* Fitness is based on a penalty function (1/(1+totalPenalty)) and     */
/* the separate penalties are also given via file ttdata.              */
/*   This is NOT state-of-the-art in using GAs for timetabling. It     */
/* just happens to fit well with the PGA framework, and has worked     */
/* well (= better than humans) on real, moderate-sized problems.       */
/***********************************************************************/

/* Constraints are stored in chains, one chain per event. The            */
/* constraints in any chain refer to a specific event. Constraints       */
/* which refer to two (or more?) can be associated with one but not      */
/* more than one. The idea is that, to evaluate a chromosome, one        */
/* steps down the chromosome and the array of chains simultaneously.     */
/* At each step the constraints associated with that event are checked.  */
/*                                                                       */
/* As you can see, this is less than efficient if there are many events  */
/* but few constraints, or many unconstrained events. However, such      */
/* problems are not hard timetabling problems! Doing it this way at      */
/* least makes it easy to see which constraints are associated with each */
/* event - eg in case you want to be clever about mutating an event?     */

/* This will be an array where the chains get rooted: */

struct constraint **tt_event_data = (struct constraint **)NULL;

/* And this is where the preset events get noted: */

struct constraint *tt_presets = (struct constraint *)NULL;


struct tt_params tt_paramlist[] = {
  {"events",       DATA_EVENTS,         1  },
  {"slots",        DATA_SLOTS,          1  },
  {"days",         DATA_DAY,            1  },
  {"near-clash",   DATA_SPREAD,         1  },
  {"clash-type",   DATA_CLASHTYPE,      1  },
  {"p-clash",      PENALTY_CLASH,       1  },
  {"p-order",      PENALTY_ORDER,       1  },
  {"p-exclude",    PENALTY_EXCLUDE,     1  },
  {"p-near",       PENALTY_NEARCLASH,   1  },
  {"separate",     CONSTRAIN_CLASH,     ALL},
  {"order",        CONSTRAIN_BEFORE,    ALL},
  {"exclude",      CONSTRAIN_EXCLUDE,   ALL},
  {"preset",       CONSTRAIN_PRESET,    2  },
  {NULL, END_LIST, 0  }
};

void 
check_event_spec (void)
{
  if(geno_size == 0) {
    fprintf(stderr,
       "ttreport: in %s: must state number of events, before first constraint\n",
       datafilename);
  exit(1);
  }
}

/* This procedure reads the timetable data file and records the details */

#define MAXSLOTS 150

int 
read_tt_datafile (void)
{
  FILE *f;
  char buffer[256];
  double *data;
  struct constraint *ctmp;
  struct tt_params *paramp;
  int i, j, k, n, d0, d1, data_max;
  int n_clash;  /* Count number of constraints, by type */
  int n_exclude;
  int n_before;
  int n_preset;

  n_clash = n_exclude = n_preset = n_before = 0;
  
  if((f=fopen(datafilename,"r")) == (FILE *)NULL) {
    fprintf(stderr, "ttreport: could not open timetable data file %s\n",
                    datafilename);
    return(0);
  }

  data_max = CHUNK;
  data = (double *)malloc(sizeof(double)*data_max);

  geno_size = 0;  /* Must change this when reading data! 0 is a flag */
  
  while(fscanf(f, " %s", buffer) != EOF) {
    /* We have a non-whitespace string, do we know it? */
    for(paramp = tt_paramlist;
        (paramp->param_type != END_LIST)
        && strncmp(paramp->param,buffer,strlen(buffer));
        paramp++);
        
    if(paramp->param_type == END_LIST) {  /* If we don't.. */
      fscanf(f,"%*[^\n]");                /* skip to end of line */
    } else {                              /* Else we know it .. */
      if(paramp->args_wanted != ALL) {    /* Gather the numbers */
        if(paramp->args_wanted > data_max) {
          data_max = paramp->args_wanted;
          if((data = (double *)realloc(data,sizeof(double)*data_max)) 
               == (double *)NULL) {
            fprintf(stderr,"ttreport: could not realloc reading %s\n",
                           datafilename);
            exit(1);
	  }
	}
        for(n=0;n<paramp->args_wanted;n++) {/* read the numbers wanted */
          if(!getdouble(f,&data[n],FALSE)) {
            fprintf(stderr, "ttreport: unexpected EOF in timetable data\n");
            return(0);
          }
        }
      } else { /* Gather ALL the numbers on this line */
        n = 0;
        while((j=getdouble(f,&data[n],TRUE)) > 0) {
          /* Now increment n .. error to do it in above line, since */
          /* getdouble will be called once too often (at EOL)       */
          n++;
          /* Will we need more space? */
          if(n == data_max) {
            data_max += CHUNK;
            if((data = (double *)realloc(data,sizeof(double)*data_max))
               == (double *)NULL) {
              fprintf(stderr,"ttreport: failed to realloc reading %s\n",
                             datafilename);
              exit(1);
	    }
	  }
	}
      } /* End of number gathering */
      /* NB: next scan for a keyword using fscanf above will go past */
      /* any further newlines, even if we did getdouble(..TRUE)      */
      /* At this point, n contains the number of numbers read in for */
      /* the current item.                                           */

      /* d0 and d1 are used purely to make the code a little easier to read */
      d0 = (int)data[0];
      d1 = (int)data[1];

      /* Store the data according to what it is: */
      switch(paramp->param_type) {
      	case DATA_EVENTS:
      	  geno_size = tt_maxevents = d0;
          /* Now we know how many events, set up the array where the */
          /* constraint chains will be rooted */
          tt_event_data =
           (struct constraint **)malloc(geno_size*sizeof(struct constraint *));
          /* and initialise each to be empty chain */
          for(i=0;i<geno_size;i++)
            tt_event_data[i] = (struct constraint *)NULL;
	  break;
      	case DATA_SLOTS:
      	  tt_maxslots = d0;
          /* Now we know how many timetable slots there are, we know */
          /* how big alleles can be: */
          biggest_allele = d0 - 1;
          if(tt_maxslots > MAXSLOTS) {
            fprintf(stderr,"ttreport: not allowed more than %d slots\n",
                           MAXSLOTS);
            return(0);
          }
          break;
        case DATA_DAY:
          tt_days = d0;
          break;
        case DATA_SPREAD:
          tt_spread = d0;
          break;
        case DATA_CLASHTYPE:
          if(d0 < 1 || d0 > 3) {
            fprintf(stderr,"ttreport: %s must be 1, 2 or 3\n", paramp->param);
            return(0);
	  }
          tt_clash_type = d0;
          break;
        case PENALTY_CLASH:
          tt_penalty_clash = data[0];
          break;
        case PENALTY_ORDER:
          tt_penalty_order = data[0];
          break;
        case PENALTY_EXCLUDE:
          tt_penalty_exclude = data[0];
          break;
        case PENALTY_NEARCLASH:
          tt_penalty_consec = data[0];
          break;  
        case CONSTRAIN_CLASH:
          check_event_spec();
          if(n < 2) {
            fprintf(stderr,
              "ttreport: expected at least 2 numbers in a `%s' constraint\n",
              paramp->param);
            return(0);
	  }
          for(i=0;i<n-1;i++) {
            for(j=i+1;j<n;j++) {
              d0 = data[i];
	      d1 = data[j];
              if(d0 == d1) {
                fprintf(stderr,
			"ttreport: cannot separate event %d from itself\n",
                        d0);
                break;  /* Give up on this constraint */
	      } else if(d0 > d1) { /* Store clash with lower-numbered event */
                k = d0;
                d0 = d1;
                d1 = k;
	      }
              /* Check to see if we have it already */
              for(ctmp = tt_event_data[d0];
                  ctmp != (struct constraint *)NULL
                  && !(ctmp->ctype == CLASH
                       && ctmp->e1 == d0
                       && ctmp->e2 == d1);
                  ctmp = ctmp->next);
              /* If we got through that loop to ctmp==NULL, it's new */
              if(ctmp != (struct constraint *)NULL) {
                if(tt_clash_type == 1) {
                printf("ttreport: separate events %d and %d: already noted\n",
                     d0, d1);
                } else if(tt_clash_type == 3)
                  ctmp->cpenalty += tt_penalty_clash;
              } else { /* Add a new constraint */
                ctmp = (struct constraint *)malloc(sizeof(struct constraint));
                if(d0 < geno_size)
                  ctmp->e1 = d0;
                else {
                  fprintf(stderr,
                          "ttreport: clash constraint: no such event as %d\n",
                          d0);
                  return(0);
                }
                if(d1 < geno_size) 
                  ctmp->e2 = d1;
                else {
                  fprintf(stderr,
                          "ttreport: clash constraint: no such event as %d\n",
                          d1);
                  return(0);
                }
                n_clash++;
                ctmp->ctype = CLASH;
                ctmp->cpenalty = tt_penalty_clash;
                ctmp->next = tt_event_data[d0];
                tt_event_data[d0] = ctmp;
                /* Keep a copy with other event: ignore copy when computing   */
                /* penalties. The copy is useful when checking for duplicates */
                /* or if implementing `smart' mutation.                       */
                ctmp = (struct constraint *)malloc(sizeof(struct constraint));
                ctmp->e1 = d1;
                ctmp->e2 = d0;
                ctmp->ctype = CLASH_COPY;
                ctmp->cpenalty = tt_penalty_clash;
                ctmp->next = tt_event_data[d1];
                tt_event_data[d1] = ctmp;
	      } /* end of adding new constraint */
            } /* end of j loop */
          } /* end of i loop */
          break;
        case CONSTRAIN_BEFORE:
          check_event_spec();
          if(n < 2) {
            fprintf(stderr,
              "ttreport: expected at least 2 numbers in a `%s' constraint\n",
              paramp->param);
            return(0);
          }
          for(i=1;i<n;i++) {
            d1 = data[i];
            /* Check to see if we have it already */
            for(ctmp = tt_event_data[d0];
                ctmp != (struct constraint *)NULL
                && !(ctmp->ctype == BEFORE
                     && ctmp->e1 == d0
                     && ctmp->e2 == d1);
                ctmp = ctmp->next);
            /* If we got through that loop to ctmp==NULL, it's new */
            if(ctmp != (struct constraint *)NULL) {
              fprintf(stderr,
                      "ttreport: order event %d before %d: already noted\n",
                             d0, d1);
              continue;  /* Give up on this constraint */
            }
            ctmp = (struct constraint *)malloc(sizeof(struct constraint));
            if(d0 < geno_size)
              ctmp->e1 = d0;
            else {
              fprintf(stderr,
                      "ttreport: before constraint: no such event as %d\n",
                      d0);
              return(0);
            }
            if(d1 < geno_size) 
              ctmp->e2 = d1;
            else {
              fprintf(stderr,
                      "ttreport: before constraint: no such event as %d\n",
                      d1);
              return(0);
            }
            n_before++;
            ctmp->ctype = BEFORE;
            ctmp->cpenalty = tt_penalty_order;
            ctmp->next = tt_event_data[d0];
            tt_event_data[d0] = ctmp;
          }
          break;
        case CONSTRAIN_EXCLUDE:
          check_event_spec();
          if(n < 2) {
            fprintf(stderr,
               "ttreport: expected at least 2 numbers in a `%s' constraint\n",
               paramp->param);
            exit(1);
          }
          for(i=1;i<n;i++) {
            d1 = data[i];
            /* Check to see if we have it already */
            for(ctmp = tt_event_data[d0];
                ctmp != (struct constraint *)NULL
                && !(ctmp->ctype == EXCLUDE
                     && ctmp->e1 == d0
                     && ctmp->s  == d1);
                ctmp = ctmp->next);
            /* If we got through that loop to ctmp==NULL, it's new */
            if(ctmp != (struct constraint *)NULL) {
              fprintf(stderr,
                   "ttreport: exclude event %d from slot %d: already noted\n",
                   d0, d1);
              continue;  /* Give up on this constraint */
            }
            ctmp = (struct constraint *)malloc(sizeof(struct constraint));
            if(d0 < geno_size)
              ctmp->e1 = d0;
            else {
              fprintf(stderr,
                      "ttreport: exclude constraint: no such event as %d\n",
                      d0);
              return(0);
            }
            if(d1 < tt_maxslots) 
              ctmp->s = d1;
            else {
              fprintf(stderr,
                      "ttreport: exclude constraint: no such slot as %d\n",
                      d1);
              return(0);
            }
            n_exclude++;
            ctmp->ctype = EXCLUDE;
            ctmp->cpenalty = tt_penalty_exclude;
            ctmp->next = tt_event_data[d0];
            tt_event_data[d0] = ctmp;
          }
          break;
        case CONSTRAIN_PRESET:
          check_event_spec();
          ctmp = (struct constraint *)malloc(sizeof(struct constraint));
          if(d0 < geno_size)
            ctmp->e1 = d0;
          else {
            fprintf(stderr,
                    "ttreport: preset constraint: no such event as %d\n",
                    d0);
            return(0);
          }
          if(d1 < tt_maxslots) 
            ctmp->s = d1;
          else {
            fprintf(stderr,"ttreport: preset constraint: no such slot as %d\n",
                           d1);
            return(0);
          }
          n_preset++;
          ctmp->ctype = PRESET;
          ctmp->next = tt_presets;
          tt_presets = ctmp;
          break;
        default:
          fprintf(stderr,"ttreport: uncatered-for type in timetable data\n");
          return(0);
          break;
      } /* end of param switch */
    } /* end of dealing with one param */
  } /* end of reading file */

  fclose(f);
  free(data);

  if(tt_days == 0) {
    fprintf(stderr, "ttreport: cannot have 0 days\n");
    exit(1);
  } else {
    tt_slots_per_day = (tt_days + tt_maxslots - 1)/tt_days;
    if(tt_slots_per_day*(tt_days-1) >= tt_maxslots) {
      fprintf(stderr,"NOTE: %d slots over %d days means %d slots per day\n",
                     tt_maxslots, tt_days, tt_slots_per_day);
      fprintf(stderr,"      so there are only %d days (%d %s on the last)\n",
                     (tt_maxslots+tt_slots_per_day-1)/tt_slots_per_day,
                     tt_maxslots % tt_slots_per_day,
                     ((tt_maxslots % tt_slots_per_day)==1? "slot":"slots"));
    }
  }
  printf("Number of clash   constraints: %d\n", n_clash);
  printf("Number of exclude constraints: %d\n", n_exclude);
  printf("Number of before  constraints: %d\n", n_before);
  printf("Number of preset  constraints: %d\n", n_preset);
  return(1);
}

/* In the timetabling problem, a chromosome represents an array */
/* of the events that must be in the timetable. The value for   */
/* a given event (ie the allele for a given locus) is the slot  */
/* in the timetable when that event happens.                    */
/*   To evaluate a chromosome, scan it: for each event E, look  */
/* at the constraints in the chain rooted at tt_event_data[E]   */
/* and for those which are violated, increase the penalty as    */
/* appropriate. Final fitness is 1/(1+penalty)                  */

void 
show_tt_penalty (char *genotype)
{
  int i, slot, otherslot;
  double d, penalty = 0.0;
  struct constraint *ctmp;

  /* Force the preset slots to have the right value : */
  for(ctmp=tt_presets;
      ctmp != (struct constraint *)NULL;
      ctmp = ctmp->next) {
    if(genotype[ctmp->e1] != ('0' + ctmp->s)) {
       genotype[ctmp->e1] = ('0' + ctmp->s);
       printf("preset: event %d shifted into slot %d\n",
               ctmp->e1, ctmp->s);
    }
  }

  /* Now look at each event, and consider the constraints that apply. */
  /* Constraints affecting two events are attached to just one of them. */
  for(i=0; i < geno_size; i++) {
    slot = SLOTVAL(genotype[i]);
    for(ctmp = tt_event_data[i];
        ctmp != (struct constraint *)NULL;
        ctmp = ctmp->next) {
      switch(ctmp->ctype) {
        case CLASH:
          otherslot = SLOTVAL(genotype[ctmp->e2]);
          if(slot == otherslot) {
            if(tt_clash_type == 3)
              printf("     clash: events %d and %d in slot %d  (%f): %d\n",
                      ctmp->e1, ctmp->e2, slot, ctmp->cpenalty,
                      (int)(ctmp->cpenalty/tt_penalty_clash));
            else
              printf("     clash: events %d and %d in slot %d  (%f)\n",
                      ctmp->e1, ctmp->e2, slot, ctmp->cpenalty);
            penalty += ctmp->cpenalty;
          /* Penalise a near-clash too, by an amount depending on how near */
          } else if(abs(slot - otherslot) <= tt_spread
                  && (slot/tt_slots_per_day) == (otherslot/tt_slots_per_day)) {
            d = (tt_penalty_consec/(double)abs(slot - otherslot));
            printf("near-clash: event %d in slot %d",
                    ctmp->e1, slot);
            if(tt_clash_type == 3)
              printf(", event %d in slot %d, gap %d  (%f): %d\n",
                      ctmp->e2, otherslot, abs(slot-otherslot)-1, d,
                      (int)(ctmp->cpenalty/tt_penalty_clash));
            else
              printf(", event %d in slot %d, gap %d  (%f)\n",
                      ctmp->e2, otherslot, abs(slot-otherslot)-1, d);
            penalty += d;
          }
          break;
        case CLASH_COPY:
          /* Ignore copy; penalty gets added when original is processed */
          break;
        case BEFORE:
          otherslot = SLOTVAL(genotype[ctmp->e2]);
          if(slot >= otherslot) {
            printf("     order: event %d in slot %d not before slot %d  (%f)\n",
                   ctmp->e1, slot, otherslot, ctmp->cpenalty);
            penalty += ctmp->cpenalty;
          }
          break;
        case EXCLUDE:
          if(slot == ctmp->s) {
            printf("   exclude: event %d should not be in slot %d  (%f)\n",
                   ctmp->e1, ctmp->s, ctmp->cpenalty);
            penalty += ctmp->cpenalty;
          }
          break;
        default:
          fprintf(stderr,
                  "ttreport: unknown constraint type found in evaluation\n");
          exit(1);
          break;
      } /* end of switch */
    } /* end of this constraint chain */
  } /* end of genotype */

  printf("Total penalty is %f\n", penalty);
}



/************************************************************************/
/* Timetable report : very simplistic                                   */
/************************************************************************/

struct eventrec {
  struct eventrec *next;
  int event;
};

void 
ttreport (char *genotype)
{
  int i, s, maxslots, gsize;
  struct eventrec **slots;
  struct eventrec *er, *tmp;
  
  gsize = strlen(genotype);
  maxslots = tt_maxslots;

  slots = (struct eventrec **)malloc(maxslots*sizeof(struct eventrec *));
  for(i=0;i<maxslots;i++) {
    slots[i] = (struct eventrec *)NULL;
  }

  for(i=gsize-1;i >= 0;i--) {
    s = SLOTVAL(genotype[i]);
    er = (struct eventrec *)malloc(sizeof(struct eventrec));
    er->event = i;
    er->next = slots[s];
    slots[s] = er; 
  }

  printf("\n----------------------------------------\n");
  show_tt_penalty(genotype);
  printf("\n");
  for(i=0;i<maxslots;i++) {
    printf("slot %2d: ", i);
    er = slots[i];
    while(er != (struct eventrec *)NULL) {
      printf(" %d", er->event);
      tmp = er;
      er = er->next;
      free(tmp);
    }
    printf("\n");
  }
  printf("----------------------------------------\n");
  free(slots);
  
}

int 
main (int argc, char **argv)
{
  int i, c, n, usegray, ttproblem;
  double lower, upper;
  char buffer[4096];

  usegray = FALSE;
  ttproblem = FALSE;

  argc--; argv++;
  while(argc > 0 && argv[0][0] == '-') {
    switch(argv[0][1]) {
    case 'g':
     usegray = TRUE;
     break;
    case 't':
     ttproblem = TRUE;
     break;
    case 'F':
     datafilename=(char *)strdup((argv[0]+2));
     break;
    case 'h':
     printf("Usage: decode [-g] n l u\n");
     printf("       decode -t [-Fdatafilename]\n");
     printf("  This filter decodes the chromosome bitstrings\n");
     printf("  used in pga's function optimisation tasks, or\n");
     printf("  the chromosomes used in the timetabling task.\n");
     printf("  It expects chromosomes, one per line, on standard input:\n");
     printf("   eg  UNIX:  cat  mytest.chr | decode 2 -100 100\n");
     printf("     MS-DOS:  type mytest.chr | decode 2 -100 100\n\n");
     printf("  For function optimisation:\n");
     printf("    Args:  n   number of real values that a chromosome\n");
     printf("               represents.\n");
     printf("           l   lower limit of range of values\n");
     printf("           u   upper limit of range of values\n");
     printf("    Use -g if chromosomes were Gray-coded (pga -g).\n\n");
     printf("  For timetables: use -t flag by itself. Use -F to\n");
     printf("    override default filename which is ttdata.\n");
     exit(1);
   default:
     printf( "unrecognised flag %s\n", argv[0]);
     exit(1);
   }
   argc--; argv++;
  }

  i = 0;
  if(!ttproblem) {
    if(argc != 3) {
      fprintf(stderr, "Usage: decode [-g] n lower upper\n");
      fprintf(stderr, "       decode -t [-Fdatafile]\n\n");
      fprintf(stderr, "Use    decode -h   for brief help\n");
      exit(1);
    }
    n = atoi(argv[0]);
    lower = atof(argv[1]);
    upper = atof(argv[2]);
    while((c=getchar()) != EOF) {
      switch(c) {
      case '\n':
        buffer[i]='\0';
        if(i>0) ldecode(buffer,n,lower,upper,usegray);
        i=0;
        break;
      case '0':
      case '1':
        buffer[i++] = c;
        break;
      default:
        break;
      }
    }
  } else {
   if(!read_tt_datafile()) {
     exit(1);
   }
   while((c=getchar()) != EOF) {
      if(c == '\n') {
        buffer[i]='\0';
        if(i>0) ttreport(buffer);
        i = 0;
      } else if(c >= '0') {
        buffer[i++] = c;
      }
    }
 }
 exit(0);
}

