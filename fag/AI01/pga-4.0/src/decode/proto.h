/* decode.c */
unsigned long g2bin(unsigned long g);
void ldecode(char *genotype, int number, double lower, double upper, int usegray);
double getdouble(FILE *file, double *valaddr, int stopateol);
int getint(FILE *f, int *valaddr, int stopateol);
int getlong(FILE *f, long *valaddr, int stopateol);
void check_event_spec(void);
int read_tt_datafile(void);
void show_tt_penalty(char *genotype);
void ttreport(char *genotype);
int main(int argc, char **argv);
