# This file is an example of a parameter file; it shows all the
# parameters you can set in such a file, even though it doesn't
# strictly make sense to set some of these at the same time, eg in
# steady-state (one) reproduction there is no point in specifying an
# elite group size. This file also shows the default values for
# everything except the random-number-seed.

               eval = max       # max, dj1, dj2, dj3, dj5, bf6,
                                #   himm, knap, tt, tt:E+S (E=a/e/r/w/tM;
                                #   S=r/f/tN), mcbK (K in 1..9), rr,
                                #   lp, lpK, tsp, nk:K+Seed, const, rand
        gray-coding = false     # true, false
        max-fitness = 32        # a dummy, whatever you wish
       reproduction = one       # one, gen, breedN (N>1),
                                #   ssoneN (N>0), ssgenN (N>0)
              elite = 1         # non-negative integer
        replacement = worst     # worst, parent, hd, hdN
                                #   (N>1, default N=10)
             select = rank      # rank, fitprop, tnK (K>1), tmK (K>1)
     selection-bias = 1.500000  # real between 1.0 and 2.0
  chromosome-length = 32        # integer, at least 12
               pops = 5         # positive integer
chromosomes-per-pop = 50        # integer, at least 2
   initial-oversize = 1.000000  # real, at least 1.0
 migration-interval = 10        # non-negtive integer (0 = none)
     crossover-type = two-point # one (or one-point), two (or two-point),
                                #   uniform, none, pmx, ox, gox, gpx
     crossover-rate = n/a       # n/a, or real in 0.0 to 1.0
           adaptive = false     # true, false
      mutation-rate = 0.031250  # real in 0.0 to 1.0 (default 1/length)
      mutation-type = per-gene  # per-gene, s, o, p (= swap, order, permute: TSP)
 reporting-interval = 10        # positive integer
              twins = false     # true, false
 random-number-seed = 968876255 # non-negative integer (default clock+pid)

# You can include blank lines and use spaces as you wish in files like
# this. Note that there are a few parameters that *cannot* be set in
# a parameter file, notably the number of generations per stage (ie
# before the user gets asked again) and those to do with requesting
# non-interactive runs (since the user might be very puzzled why
# nothing seemd to be happening, if he had set such a parameter by
# mistake within a parameter file).

