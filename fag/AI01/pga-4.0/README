
PGA, the Parallel Genetic Algorithms testbed -- version 4.0
-----------------------------------------------------------

By Peter Ross, peter@dcs.napier.ac.uk 
and contributions by Emma Hart, emmah@dcs.napier.ac.uk

Current version developed/maintained by Peter Ross, peter@dcs.napier.ac.uk
Original version by Geoffrey H. Ballinger, geoff@ed.ac.uk

NEW SINCE 3.1

Briefly:
         - TSP stuff (thanks to EH) with permutatioon-specific
           crossover and mutation operators
         - parameter file: you can specify parameters in a file and
           then override them on the command-line
         - rewrote much of the code for tidiness, speed etc
         - replacement policies: parent, worst, or by Hamming distance

NEW SINCE 3.0

Briefly:
         - In simple exam timetabling, you can now mutate all events,
           either at a fixed (-m) rate or at that rate scaled down
           by the penalty attributable to the event.
         - Added Himmleblau function
         - Fixed NASTY RNG bug, that is also present in commercial
           versions in Solaris OS, in HPUX, in Linux etc -- all due
           to Berkeley's 1988 version.

NEW SINCE 2.9

Briefly:
         - Added NK landscapes, const and rand evaluation functions.
         - Added non-random (over-productive) initialisation.
         - Default mutation now 1/length
         - Fixed silly bug in DJ5, thanks to Dae Gyu Kim.

NEW SINCE 2.8

Briefly:
         - Added elitism, -EN in generational GA causes top N to
           be copied to next generation.
         - Added long-path problems -elp and -elpK. The latter omits
           the last (K-2) steps so end of path is K bits different from
           start.
         - Added breeder GA code -rbreedN, where N is breeding group size.
         - Tidied some definitions to cater for porting problems; now
             work OK with BC++ 4.5 and GCC on MSDOS.
         - Can write a map of a spatially-structured problem even if
              no output file specified; goes to unnamed.map.
         - Convergence criterion fixed. Used to be that (average fitness
             = best fitness) in each population, but as Dave Corne pointed
             out this fails rwadily for some cases, eg mcb-type problems.
             So now the test is (average=best) && (all chromos equal), so
             perfomance impact is negligible.
         - Two bugs, fixed thanks to Juergen Branke and class at Karlsruhe:
             - problem if migration interval smaller than reporting interval
             - fitprop calculation bug.

NEW SINCE 2.7

Briefly:
         - now in ANSI C: compiles using gcc on various Unix
           workstations, and even for PCs (DOS)
         - the cost of evaluating the initial population is now
           included in the evaluations display.
         - includes a random number generator rather than relying on
           system one.
         - fixed minor bug in mutation code, which prevented mutation
           from producing the largest allele in certain cases.

NEW SINCE 2.6

Briefly:
         - better timetabling; you can choose various methods of 
           mutating a timetable, by choosing the event to move
           and the slot to move it to according to various algorithms.
           In particular, -ett:r+t5 is good - random choice of event
           and use tournment selection size 5 to find good slot to move
           it to. Use this with -m1.0 -a for decent results.
         - -No<n> and -Na<n> are like -NO<n> and -NA<n> but also save
           the chromosomes at the end.
         - when non-interactive, convergence also counts when deciding
           whether to stop.
         - can specify data file other than weights/rrdata/ttdata by
           using -Fdatafile flag
         - fixed a silly bug in timetabling which meant that the `days'
           was really `slots per day'
         - added pga-batch, a simple shell script to let you do lots of
           runs non-interactively and summarise the results
         - can set crossover type to "none", so no crossover happens.
           This lets you investigate the benefit of crossover a bit
           more.
         - even more code tidying.

NEW SINCE 2.5

Briefly: 
         - simple timetabling, using a penalty function: problem is
           specified in a separate file using a reasonably flexible
           language to specify constraints etc. This can solve real
           non-trivial problems.
         - can be run noninteractively; stop when limit (set by -l)
           reached or when the fittest in one or all populations passes
           a given threshold
         - pauses when all populations appear to have converged.
         - another new problem, mcbK: alleles lie in range 0..K, fitness is
           length of maximum contiguous block of identical alleles. This
           means there are K+1 distinct maxima to be found. Good for
           basic explorations with spatial selection.
         - in the spatial selection options, you can dump a rectangular
           map showing where the fittest chromosomes are, to *.map. Up to
           26 distinct highly-fit chromosomes are mapped explicitly. Thus
           you can explore how fit chromosomes take over territory and
           shape each others' territory.
         - Gray code option for the function optimisation tasks
         - stand-alone decode program. In the function optimisation
           tasks a chromosome is a (0/1-encoded) sequence of real
           numbers. decode lets you reover those real numbers, for use
           with external plotting programs so you can see how the
           solutions move in Cartesian space.
         - interactive control of number of generations between prompts
           (should have had this years ago, blush) .
         - can always save the chromosomes, even if you didn't mention
           a file.
         - report file also gives the random number seed, whether from
           user or clock, so runs can be replicated precisely.
         - more informative command-line prompts.
         - some minor bugs fixed.
         - minor code tidying and shuffling between files.

NEW SINCE 2.4

Briefly: - proper Royal Road (as in Holland's challenge); 
         - two kinds of tournament selection;
         - spatially-structured reproduction, on a 2-D toroidal grid,
           either generational or one at a time;
         - mutation is now per-bit, meaning of adaptive has changed;
         - can seed the random number generator;
         - minor mod to format of *.chr file;
         - crossover can produce two children or just one (default is one);
         - source split into several files.

WHAT IT IS

PGA is a simple testbed for basic explorations in genetic algorithms.
Command line arguments control a range of parameters, there are a
number of built-in problems for the GA to solve. The current set
consists of:
  - maximise the number of bits set in a chromosome
  - De Jong's functions DJ1, DJ2, DJ3, DJ5
  - binary F6, used by Schaffer et al
  - maximise the length of a sequence of equal alleles; alleles
    drawn from a set of user-specified size (set sizes 2..10)
  - a crude 1-d knapsack problem; you specify a target and a set of
    numbers in an external file, GA tries to find a subset that sums
    as closely as possible to the target
  - the `royal road' function(s), as defined in Holland's challenge.
    The various parameters can be specified in an external file.
  - that max-contiguous-block problem: the user specifies how many
    distinct values an allele can have, and fitness is the size
    of the longest sequence of equal-valued alleles. So the maxima
    are non-overlapping.
  - simple timetabling, in terms of (fixed-size) slots and events.
    Constraints are specified in a separate file, and describe
    which events must not clash, event orderings, which events are
    to be fixed in place, which slots an event must not occupy,
    which events should be kept apart, and the penalties for violations.
  - travelling salesman problems, with data read from file
  - Himmelblau function
  - NK landscapes
  - constant and random fitness problems
and it's easy to add your own problems (see below). Chromosomes are
represented as character arrays, so you are not (quite) stuck with
bit-string problem encodings.

PGA allows, among other things:
  - multiple populations, with/without periodic migration (the `island' model)
  - various reproduction strategies
     - generational
     - GENITOR-like
     - spatially-structured generational (the `cellular' model)
     - spatially-structured GENITOR-like ( " )
  - various selection strategies:
     - rank-based as in GENITOR
     - roulette-wheel (fitprop)
     - tournament, with control of tournament size
     - `marriage tournament', with control of tournament size
  - one-point, two-point and uniform crossover
  - choice of whether crossover produces one or two children
  - control of population size, chromosome length (independently
    of the problem!), the usual rate parameters.

The command-line options are summarised by the `-h' flag:

PGA: pseudo-parallel genetic algorithm testbed, version 4.0
Flags: defaults in (..), or in [..] if not user-nameable
   -a       Adaptive mutation flag (FALSE)
   -b<n>    Set selection bias. (1.5)
   -c<n>    Set crossover rate, for `gen' or `breedN'. (0.6)
   -e<fn>   Set evaluation function. (max)
   -f<file> Read parameters from file <file>.
   -g       In function optimisation, use Gray coding
   -h       Display this information.
   -i<n>    Set reporting interval in generations. (10)
   -l<n>    Set # of generations per stage. (100)
   -m<n>    Set bit mutation rate. (1/length)
   -n<n>    Set chromosome length. (32)
   -p<n>    Set number of chromosomes per population. (50)
   -s<op>   Set selection operator. (rank)
   -r<op>   Set reproduction operator. (one)
   -t       Twins: crossover produces pairs (FALSE)
   -C<op>   Set crossover operator. (two)
   -E<n>    Set elite size, for `gen' or `breedN'. (1)
   -F<file> Use problem datafile <file> instead of default.
   -I<x>    Initial population is cream of <x> sets. (1.0)
   -M<n>    Interval between migrations. (10)
   -NA<n>   Non-interactive, stop when All reach <n>.
   -Na<n>   Like -NA<n> but also save final chromosomes.
   -NO<n>   Non-interactive, stop when One reaches <n>.
   -No<n>   Like -NO<n> but also save final chromosomes.
   -P<n>    Set number of populations. (5)
   -R<op>   Set replacement policy. (worst)
   -S<n>    Seed the random number generator. [from clock+PID]
   -T<op>   Set mutation type. [per-gene].
   <file>   Also log output in <file>.
   Crossover operators ...... one, two, uniform, none,
                              and for tsp: pmx, ox, gox, gpx
   Selection operators ...... rank, fitprop,
                              tnK (K=integer > 1),
                              tmK (K=integer > 1).
   Reproduction operators ... one, gen,
                              breedN (N=integer > 1),
                              ssoneN (N=integer > 0),
                              ssgenN (N=integer > 0).
   Evaluation functions ..... max, dj1, dj2, dj3,
                              dj5, bf6, himm, knap, tt,
                              tt:E+S (E=a/e/r/w/tM; S=r/f/tN),
                              mcbK (K in 1..9), rr, lp, lpK,
                              tsp, nk:K+Seed, const, rand
   Replacement policies ..... parent, hd, hdN (N>1 default 10), worst
   Mutation types ........... s, o, p (only in tsp problems),
                              default: per-gene problem-specific
Made available under the terms of the GNU General Public License v2.




The output is curses-based, with optional output to file for later
plotting or analysis using a tool such as (g)awk. The screen layout
looks like this:

.................................................................
 (R)epeat, (N)ew start, (Q)uit, (C)ontinue:
           Populations: 5       Chromosomes per pop: 50
                                  Chromosome length: 32
 Generations per stage: 100            Reproduction: one
    Reporting interval: 10           Crossover type: two-point, one child
    Migration interval: 10           Crossover rate: n/a
         Eval function: dj5           Mutation type: per-gene
             Selection: rank          Mutation rate: 0.031250
        Selection bias: 1.50            Replacement: worst
 
            Generation: 0        Evaluations so far: 250
        Pop.......Average..........Best.(max = approx 499.001997)
        0          20.8874139      400.8359415
        1          11.6627009      478.6331572
        2          15.5316153      470.1711265
        3          36.4764306      467.1020082
        4          36.0776251      479.8166322
.................................................................

The `N' option restarts with new randomly-chosen chromosomes. The `R'
option restarts with the same initial population as when the program was
first started. The `C' option continues for a further number of
generations, as determined by the `-l' flag (`generations per stage' in
the above display). The `=' opposite populations 0 and 2 show that they
appear to have converged, because the average fitness and best fitness
are equal. If you have specified output to file too, then you also get
the option of saving the chromosomes to a file called filename.chr. If
you've chosen a spatial selection option, you can save a map of the grid
of chromosomes to a file called filename.map.

INSTALLING IT

There is a very simple Makefile, which doesn't even install it for
you!  Source consists of various C and header files, using ANSI C. The
code was developed on a Sun-4 and, later, on Linux. The curses usage
is pretty simple, so it should be easy to adapt it to your own
system. I don't have access to a range of machines, so I haven't
provided lots of system-dependent switches, although it works on
various SunOS, SOLARIS and HP/UX, and PC (DOS). 

See the doc/ directory for documentation, the src/pga directory for
the main source code, the src/decode directory for the decode program
that decodes chromosomes for the timetabling and the numeric
optimisation problems, and the TSP/ directory for some TSP problems.
In the top-level directory there are also these files:

    COPYING ........ the GNU General Public License
    README ......... this file
    graph1.awk ..... an example of how to plot graphs from the output
    graph2.awk ..... another such example
    graph3.awk ..... yet another such example
    param.tst ...... an example of a parameter file
    pgabatch ....... a script to run PGA many times, off-line
    rrdata ......... a sample datafile for the Royal Road problem
    tspdata ........ a sample datafile for the travelling salesman problem
    ttdata ......... a sample datafile for timetabling
    ttdata.93 ...... another timetabling problem
    ttdata.94 ...... and another one
    ttdata.ael ..... same as ttdata, basically
    weights ........ a sample datafile for a knapsack problem




ADDING NEW PROBLEMS

To add a new problem to pga:
  - create a new eval_whatever function, alongside the others in eval.c;
    put any problem-specific auxiliary functions there too (eg
    read_weights_file is used in knpasack problem, is beside eval_knap).
    Remember that eval_whatever has to increment evals each time it is
    called.
  - declare eval_whatever extern at top of file main.c
  - declare any problem-specific globals and other data-reading
    functions at top of file main.c
  - go to procedure handle(), case 'e', add the branch which sets
    eval to be eval_whatever, and sets maxfitness (a string) and
    eval_name (a string) appropriately
  - in main.c main(), just after handle() is called, you may need to add any
    problem-specific setup stuff that cannot be done until all arguments
    are processed (eg see the bit which says if(eval == eval_knap).. )




PROBLEMS, QUESTIONS

PGA has been used for teaching for several years now, and has been
used as a starting point by a fair number of people for their own
projects. So it's reasonably reliable. However, if you find bugs, or have
useful contributions to make, Tell Me!

Peter Ross
School of Computing
Napier University
10 Colinton Road
Edinburgh EH10 5DT

peter@dcs.napier.ac.uk




