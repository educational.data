AI01 Assessible Work:  Task 2 (of 2).
-------------------------------------

The aim of this exercise is to give you some experience of
experimenting with genetic algorithms using pga.  It consists of three
straightforward experiments investigating the behaviour and properties
of and parameter choices involved in running a GA.

Experiment 1:

  Investigate the phenomenon of premature convergence/genetic drift:

  - take the max-1s problem, and chromosome lengths of 32, 50 and 100.
    Leave all parameters as default except migration (if you use
    several populations) --- turn it off with -M0 --- and set the
    mutation rate to 0.  Run the GA and note the behaviour (which you
    have already seen, in class).

  - explain what is going on: in particular, what effect does
    chromosome length have and why?

  - now try the same tests but with, in addition, larger populations.
    Try at least -p100 and -p200.  Note what difference this makes.

  - explain, again, what is going on now: in particular, why does
    population size have the effect it does?  

  - given your understanding of the GA's behaviour, choose one more
    parameter or option you think is relevant to this phenomenon
    (leave the problem choice, migration, and mutation as they are:
    choose something else).  Explain why you chose that one, and
    experiment to determine what effect it has.

  - report the experiment you did with your chosen parameter or
    option, including a summary of the results, and your conclusions.

  - finally, summarize what you now understand about the phenomenon of
    genetic drift or premature convergence and how it depends on the
    GA parameters.

Experiment 2:

  Investigate whether fitness proportional or rank selection gives
  better performance with dj3, dj5 and bf6:

  - use a single population of size 100 and leave other settings at
    default.  Thus pga -P1 -p100 -ePPP -sXXX where PPP is one of dj3,
    dj5 or f6 and XXX is rank (the default) or fitprop.

  - you need to decide what you mean by `performance' and how you will
    measure it, and also how to control for the effect of the random
    initial population on your result.  Describe and explain your
    choices in your report.

  - summarize your results and conclusion of the best choice for each
    problem.

Experiment 3:

  Investigate whether the crossover and mutation defaults are best for
  the three problems dj3, dj5 and bf6.

  - given the best choice for selection from Expt 2, devise and carry
    out an experiment to determine whether crossover is doing useful
    work in each problem.  Describe your experiment, explain any
    choices you made, summarize the resulys and conclusions in the
    report.

  - finally, given again the best choice from Expt 2 (but keep
    crossover turned on), investigate whether the pga default mutation
    parameter is the best choice for each problem.  As for the
    previous part, describe your experiment and its outcome in your
    report.

Reporting
---------

   AI01 is assessed by an INDIVIDUAL REPORT, on the 13 scale with
external censor, and this task forms the first half of the assessment.
There will be a similar task based on genetic algorithms to complete
the assessment.

   Your report should be arouind 10 pages total and certainly not more
than 12 pages -- covering BOTH tasks.  That means you have around 5-6
pages for task 2.  You may structure your report as you wish, but your
goal is to provide a convincing account of your experimentation, so
this means:

  - you should provide enough information that someone else can
    replicate what you have done and be confident they have done the
    same as you did.

  - to help you with the running of tests (if you have a Un*x
    environment handy) the pgabatch script in the Utils directory for
    the task will run a batch of experiments for you.  The pgalog.awk
    script, used with the awk (or gawk or mawk) program, will analyse
    the output from pgabatch even further for you.  Use them, if you
    wish to do a more exhaustive experiment, once you have a clear
    idea what you are testing.  You can get good answers to the
    questions, though, using only interactive runs, if you are
    systematic.

  - you should explain your design choices, e.g. what runs did you
    choose to make, what (non-default) parameters did you set, what
    was your measurement criterion, and why did you make these
    choices.

  - make sure you answer the questions!  This time they are not marked
    (a), (b) etc.  but they are there nevertheless.  In particular,
    for Expt 3 you need to describe your methods and why you chose
    that way to do things, as well as the results and the conclusions
    you draw from them.  Structure your report so that it is clear to
    the reader which sections/paragraphs address which question(s).

  - don't waste space explaining genetic algoorithms or other standard
    material unless it is directly relevant to an answer; concentrate
    on what you did and why, what the results were and what you think
    you can conclude from them.  It is possible to make a very good
    account of this task in very few pages.

  - aim to use good experimental methods.  Look back over the lectures
    for comments on ways to do good experiments with such networks.

   Your report should be INDIVIDUAL, that is, written by you and
describing your own work and analysis.  However, you may wish to share
experimental data or discuss the exercise with others in the class:
that's fine -- but if you use someone else's data, you must
acknowledge that with a citation.
