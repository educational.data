# This gawk script summarises a collection of pga runs.
#
# Usage:
#    gawk -f pgalog.awk logfile
#    gawk -f pgalog.awk -v output=1 logfile
#
# The first form just prints the header information of each run
# and the number of runs, some statistics about the fitness of the best
# in each run, and the mean and sample standard deviation of the number
# of evaluations used.
#
# The second form also outputs the number of evaluations and the
# maximum fitness (together on one line) from each run.
#
# Peter Ross, July 94

function summarise()
{ 
  if(n > 0) {
    av1 = sum1/n
    av2 = sum2/n
    printf("%% %s\n%% %s\n%% %s\n", line1, line2, line3)
    printf("%%                n = %d\n",n)
    printf("%%   max of fittest = %12.7f (x %d)\n",max, maxcount)
    printf("%%  mean of fittest = %12.7f\n",av2)
    printf("%%   min of fittest = %12.7f\n",min)
    printf("%%       mean evals = %10.2f\n",av1)
    if(n > 1) {
      sd1 = sqrt((ssq - (sum1*sum1/n))/(n-1))
      printf("%%        std evals = %10.2f\n",sd1)
    }
    printf("%% --------------------------------------\n\n")
  }
  n = 0
  sum1 = 0.0
  sum2 = 0.0
  ssq  = 0.0
  max  = 0.0
  min  = 1000000.0
  maxcount = 0
}

BEGIN         { n = 0
                sum1 = 0.0
                ssq  = 0.0
                sum2 = 0.0
                max  = 0.0
                min  = 1000000.0
                maxcount = 0
              }
              
/Batch/       { summarise()
                line1 = $0
                getline
                line2 = substr($0,6)
              }

/started/     { line3 = $0
              }

/Generat/     { if(output == 1) {
                  printf(" %8d",$6)
                }
                sum1 += $6
                ssq += $6 * $6
                getline
                if(output==1) {
                  printf(" %10.7f\n",$3)
		}
                sum2 += $3
                if($3 > max || n==0) {
                  max = $3 
		  maxcount = 1
                } else if ($3 == max) {
                  maxcount++
                }
                if($3 < min || n==0) { 
		  min = $3 
                }
                n++
	      }

END           { summarise()
              }
