/* ****************************************************** */
/* file ibp.h: contains definitions for programs that use */
/*             16-bit integer weights                     */
/*                                                        */
/* Copyright (c) 1993 by Donald R. Tveter                 */
/*                                                        */
/* ****************************************************** */

#include <stdio.h>
#include <signal.h>
#include <setjmp.h>
#include <math.h>
#include <string.h>

#ifdef DOS16
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define INT32 long
#define MAXINT 32767
#define WRITEBIN "wb"
#define READBIN "rb"
#define HOTKEYS
#endif

#ifdef DOS32
#include <stdlib.h>
#include <time.h>
#include <conio.h>
#define INT32 int
#define MAXINT 2147483647
#define HOTKEYS
#endif

#ifdef UNIX
#ifdef BSD
#include <termios.h>
#elif defined NEXT
#include <malloc.h>
#include <sgtty.h>
#else /* SysV */
#include <malloc.h>
#include <termio.h>
#endif
#define INT32 int
#define SIGINT 2
#define MAXINT 2147483647
#define CLOCKS_PER_SEC 1000000.0
#define HOTKEYS
extern long clock();
#endif

#define RXSTRSIZE 257     /* max length of string to store rx files */
#define FILENAMESIZE 15   /* maximum length of a file name */
#define MAXFILES 10       /* maximum number of files on file stack */
#define MAXFORMAT 21      /* maximum number of format breaks */
#define BUFFSIZE 257      /* maximum size of an input line */
#define OUTSTRSIZE 400    /* max size of output string */
#define WTTYPE short      /* a 16-bit integer */
#define WTSIZE 2          /* shorts are two bytes */
#define MAXSHORT 32767    /* largest short */
#define MINSHORT -32768   /* smallest short */
#define HCODE -32768      /* code number for a layer h (2) unit */
#define ICODE -32767      /* code number for a layer i (3) unit */
#define OCODE -32766      /* code number for a layer j (4) unit */
#define CAPHCODE -32764   /* code for a string of h layer units */
#define CAPXCODE -32763   /* code for a string of unknown values */
#define SKIPCODE -32762   /* temporal difference code to skip pattern */
#define DIFFCODE -32761   /* temporal difference code to use diff */
#define GT 0              /* a symbol meaning > */
#define GE 1              /* a symbol meaning >= */
#define TRAIN 0           /* selects training patterns */
#define TEST 1            /* selects testing patterns */
#define TOL 0             /* selects stats based on tolerance */
#define MAX 1             /* selects stats based on max value */
#define LOOKUP 1          /* produces lookup table for s function */

#ifdef FLOAT
#define REAL float
#else
#define REAL double
#endif

typedef struct seednode
   {
     unsigned val;            /* a seed value */
     struct seednode *next;   /* pointer to next node */
   } SEEDNODE;

typedef struct patnode
   {
    WTTYPE *addr;             /* address to get the value from */
    WTTYPE val;               /* a normal value */
    short layer;              /* layer of the net to take value from */
    short unitno;             /* unit of the layer to take value from */
   } PATNODE;

typedef struct patlist
   {
     PATNODE *pats;           /* the list of patterns */
     struct patlist *next;    /* pointer to the next pattern */
   } PATLIST;

typedef struct unit
   {
     int inuse;               /* flags unit in use */
     short layernumber;       /* layer number of the unit */
     short unitnumber;        /* position within layer */
     INT32 error;             /* to sum error factors */
     WTTYPE oj;               /* state of activation of node */
     WTTYPE tj;               /* a temporary variable */
     WTTYPE translate;        /* the amount to translate an input by */
     WTTYPE userscale;        /* the amount to scale an input by */
     struct wtnode *wtlist;   /* the list of weights */
     struct unit *next;       /* link to next unit in this layer */
   } UNIT;

typedef struct wtnode
   {
#ifdef SYMMETRIC
     WTTYPE *weight;          /* ptr to weight */
     WTTYPE *olddw;           /* ptr to delta wji */
     WTTYPE *eta;             /* ptr to eta for the DBD method */
     INT32 *total;            /* ptr to total of weight changes */
     WTTYPE *slope;           /* previous slope */
#else
     WTTYPE weight;           /* weight from here to backunit */
     WTTYPE olddw;            /* delta wji from previous iteration */
     WTTYPE eta;              /* the eta for the DBD method */
     INT32 total;             /* total weight changes for batch mode */
     WTTYPE slope;            /* previous slope */
#endif
     short inuse;             /* 0 if unused, > 0 otherwise */
     struct wtnode *next;     /* link to next node */
     UNIT *backunit;          /* ptr to unit the weight comes from */
   } WTNODE;

typedef struct layer
   {
     char activation;         /* activation function for this layer */
     WTTYPE D;                /* gain for this layer */
     WTTYPE biasact;          /* bias act. value for this layer */
     int unitcount;           /* number of units in this layer now */
     int initialcount;        /* number of units to start with */
     struct layer *backlayer; /* pointer to previous layer */
     struct layer *next;      /* pointer to next layer */
     UNIT *units;             /* start of list of units in this layer */
     PATLIST *patstart[2];    /* to the list of patterns */
     PATLIST *currentpat[2];  /* the current pattern */
   } LAYER;

typedef struct data
   {
    int uncertain;            /* flags values created by continuous */
                              /* updates in the forward pass */
    int off;                  /* -1 for off by 1, 0 otherwise */
    int npats;                /* number of patterns */
    INT32 iterno;             /* iteration number when data was found */
    int right;                /* number of patterns right */
    int wrong;                /* number of patterns wrong */
    float avgerr;             /* average abs error per unit */
    float pctright;           /* percentage right */
   } DATA;
