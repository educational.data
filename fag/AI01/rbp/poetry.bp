* In this problem a recurrent network is used to memorize two lines of
* poetry and in effect form a linked list.  It comes from William
* James' book, _Psychology_ in the chapter on Assoication.  There are
* two lines of poetry from a poem called Locksley Hall that need to be
*  memorized:
*
*   I the heir of all the ages in the foremost files of time
*
*   For I doubt not through all the ages ones increasing purpose runs
*
* The problem here is that there are two instances of "the ages" in
* these two lines.  How does a person manage to keep on track in each
* instance and produce different output after each "the ages"?
* Obviously its all the words preceeding "the ages" plus "the ages" that
* contribute associations.  Its easy to devise a recurrent backprop
* network to do this.  For simplicity, I changed the lines to:
*
*   i the heir of all the ages in the frmst files of
*
*   time for i doubt not thru the ages one incre purpo runs
*
* so every word is shortened to 5 characters.  Then I converted each
* letter to its ASCII code and drop the first 3 bits.  In training you
* input "i" and this is supposed to produce "the" on the output.  Then
* when you input "the" this should give "heir" on the output units and
* so on.  When the network memorizes the data it too has to keep a
* short-term memory of what comes before "the ages" so it can output the
* right words after each "the ages".
*
m 25+20 20 25
a as dd uq
qp e 0.025
s99
ci 0.5
f ic oc
t 0.2
*e 0.25
e 0.5 0.05
a 0
rt {
X                         * (nothing) -> i
0100100000000000000000000
0100100000000000000000000 H  * i -> the
1010001000001010000000000
1010001000001010000000000 H  * the -> heir
0100000101010011001000000
0100000101010011001000000 H  * heir -> of
0111100110000000000000000
0111100110000000000000000 H  * of -> all
0000101100011000000000000
0000101100011000000000000 H  * all -> the
1010001000001010000000000
1010001000001010000000000 H  * the -> ages
0000100111001011001100000
0000100111001011001100000 H  * ages -> in
0100101110000000000000000
0100101110000000000000000 H  * in -> the
1010001000001010000000000
1010001000001010000000000 H  * the -> frmst
0011010010011011001110100
0011010010011011001110100 H  * frmst -> files
0011001001011000010110011
0011001001011000010110011 H  * files -> of
0111100110000000000000000
0111100110000000000000000 H  * of -> time
1010001001011010010100000
1010001001011010010100000 H  * time -> for
0011001111100100000000000
0011001111100100000000000 H  * for -> i
0100100000000000000000000
0100100000000000000000000 H  * i -> doubt
0010001111101010001010100
0010001111101010001010100 H  * doubt -> not
0111001111101000000000000
0111001111101000000000000 H  * not -> thru
1010001000100101010100000
1010001000100101010100000 H  * thru -> the
1010001000001010000000000
1010001000001010000000000 H  * the -> ages
0000100111001011001100000
0000100111001011001100000 H  * ages -> one
0111101110001010000000000
0111101110001010000000000 H  * one -> incre
0100101110000111001000101
0100101110000111001000101 H  * incre -> purpo
1000010101100101000001111
1000010101100101000001111 H  * purpo -> runs
1001010101011101001100000
}
