* This data set is designed to show how associative memory works.
* The characteristics of a number of people and their names are stored
* into the network.
*
* #1  a person from Chicago
* #2  is a Cubs fan
* #3  a person from New York
* #4  is a Mets fan
* #5  a Democrat
* #6  a Republican
* #7  likes lemonade
* #8-20 are allocated to names, each position is a unique name
*
* The possible outputs are:
*
* #1  is a Sox fan
* #2  is a Bears fan
* #3  likes tennis
* #4  is a Yankees fan
* #5  is a Jets fan
*
* The idea is that you can train these patterns into a backprop net
* and then test the net with certain inputs.  For instance, input
*
* 1010 010 0000000000000
*
* that is, nameless Republican Cub fans from Chicago and you'll get an
* estimate of how the person feels about the Sox, Bears, tennis, Yankees
* and Jets.  Thus, the associative memory does what other people call
* fuzzy reasoning but without having to write the fuzzy rules.  Actually
* the names field in this example could be skipped altogether however
* using names has a use in the Hopfield/Boltzman machine so that's how
* they got there.
*
m 20 5
s7
ci 0.25
f ic

rt {
1010   101   1000000000000     01000
1010   010   0100000000000     11000
0101   101   0010000000000     00001
0100   010   0001000000000     00100
1000   101   0000100000000     00100
0101   011   0000010000000     00001
1010   011   0000001000000     11000
0101   010   0000000100000     00011
1000   000   0000000010000     00100
0100   000   0000000001000     00100
1010   100   0000000000100     01000
0101   100   0000000000010     00001
}
e 0.25
a 0.0
