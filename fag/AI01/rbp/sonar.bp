* This program runs the aspect-angle dependent data from Gorman and
* Sejnowski's article: "Analysis of Hidden Units in a Layered Network
* Trained to Classify Sonar Targets", in Neural Networks, volume 1,
* number 1, 1988.  The data has been modified so that class 0 is now
* class 1 and what was class 1 is now class 2.  In addition, G&J gave
* training data where all the class 1 patterns came first and all the
* class 2 patterns came second.  With the online backprop method this
* approach gives rather poor results unless the patterns are selected in
* a random order (not implemented in this version) or you intermix
* patterns from the two classes.  The file sonarmix.tra contains
* patterns that were mixed and the file sonar.tra contains the original
* unmixed patterns.  You might want to try it both ways.
*
* The periodic update setting given below is close to the one Gorman
* and Sejnowski report using.  Note that when using the periodic update
* settings the number of patterns you get correct will be much higher
* than it really is because the tests are made as the program does each
* pattern.  To get the real performance measure you must do an
* independent test of the whole pattern set after each complete pass
* thru the data.  To do this use: "f u+".
*
* The quickprop setting gives its best results by around 300 iterations
* however the performance on the test set is worse than the periodic
* update method.  Perhaps quickprop doesn't generalize as well as the
* conventional algorithm?
*
m 60 6 2
a dd uq
qp e 0.01
e 2
a 0
t 0.49
f ir pc
*rt sonar.tra
rt sonarmix.tra
tf sonar.tes
s 0
ci 0.3
