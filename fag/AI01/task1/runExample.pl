#!/usr/bin/perl
# $Author: tobibobi $
# $Revision: 1.2 $
#
# $Log: runExample.pl,v $
# Revision 1.2  2006-12-11 22:49:45  tobibobi
# Can now run evaluation of data and analyse the output.
#
# Revision 1.1  2006/12/09 15:23:00  tobibobi
# Moved all data to a new directory
#

use warnings;

sub findOutputEval {
    open RESULT, "copy";
    my @lines = <RESULT>;
    close RESULT;

    $_ = $lines[@lines-1];
    
    s/^\s*//;

    @vars = split /\s+/;
    my %res = ( perr => $vars[2], eerr => $vars[8]);

    return \%res;
}

sub findOutput {
    open(RESULT,"copy");
    @lines = <RESULT>;

    my $maxPSuccess = 0;
    my $maxErr = 1;
    my $bestIttr = -1;
    my $maxItr = 0;
    for(my $i = 4;$i < @lines;$i++) {
	$_ = $lines[$i];
	s/^\s*//;
	my @vals = split (/\s+/, $_);
	my $itt = $vals[0];
	my $err = $vals[6];
	my $perr = $vals[4];

	if($err < $maxErr) {
	    $bestIttr = $itt;
	    $maxErr = $err;
	    $maxPSuccess = $perr;
	}
	$maxItr = $itt;
	#print "$itt $perr % $err\n";
    }
    #print "Best iter: $bestIttr, Max error: $maxErr, ";
    #print "Max Percentage: $maxPSuccess\n";
    close(RESULT);
    
    my %res = 
	(
	 iteration => $bestIttr,
	 maxerror  => $maxErr,
	 percentageSuccess => $maxPSuccess,
	 totaliterations => $maxItr
	 );
    return \%res;
}

sub ExecuteBP() {
    my $count = 0;
    my $len = 10;
    my $meanPErr = 0;
    my $meanErr = 0;
    my $meanItr = 0;
    my $meanMaxItr = 0;
    my $meanEvalErr = 0;
    my $meanEvalPErr = 0;

    for (my $start = 0; $start<=30-$len;$start+=1) {
	open(RESOURCE,"whitecaps-res");
	my @total=();
	my $index=0;
	my $traindata = "";
	my $testdata = "";
	my $evaldata = "";
	@lines=<RESOURCE>;
	foreach (@lines) {
	    @data = split;
	    $data[3]*=1/10;
	    #$data[3]/=2;
	    $data[3]/=2;
	    $data[3]+=0.5;

	    if($index >= $start && $index < $start+$len/2) {
		$testdata .= "@data\n";
	    } elsif ($index >= $start+$len/2 && $index <= $start+$len) {
		$evaldata .= "@data\n";
	    } else {
		$traindata .= "@data\n";
	    }
	    $index++;
	}
	close(RESOURCE);

	open (OUTPUT,">wcap.tst");
	print OUTPUT $testdata;
	close (OUTPUT);

	open (OUTPUT,">wcap.trn");
	print OUTPUT $traindata;
	close (OUTPUT);

	open (OUTPUT,">wcap.eva");
	print OUTPUT $evaldata;
	close (OUTPUT);

	system "../rbp/bp sample-enew>/dev/null";

	my $res = findOutput();	


	$meanErr += $res->{'maxerror'};
	$meanPErr += $res->{'percentageSuccess'};
	$meanItr += $res->{'iteration'};
	$meanMaxItr += $res->{'totaliterations'};

	SetSampleEvaluate(numberofiterations => $res->{'maxerror'});

	system "../rbp/bp sample-eval>/dev/null";

	$res = findOutputEval();

	$meanEvalErr += $res->{'eerr'};
	$meanEvalPErr += $res->{'perr'};

	$count++;
    }
    

    printf "Avg: %02.3f %% %1.5f NI: %4.1f MNI: %4.1f - Eval: %3.2f %% %1.5f\n", 
      $meanPErr / $count,
      $meanErr / $count, 
      $meanItr / $count,
      $meanMaxItr / $count,
      $meanEvalPErr / $count,
      $meanEvalErr / $count;

    my %res = 
	(
	 meanPErr => $meanPErr / $count,
	 meadErr =>  $meanErr / $count, 
	 meanItr =>  $meanItr / $count,
	 meanMaxItr => $meanMaxItr / $count,
	 );

    return \%res;
}
my %currSample = ( );
sub SetSample {
    my %params = 
	(
	 tolerance  => 0.1,
	 eta        => 0.1,
	 momentum   => 0.5,
	 seed       => 7,
	 weights    => 1,
	 maxruns    => 4000,
	 network    => "3 7 1",
	 weightfile => 'weights',
	 @_
	 );
    %currSample = %params;

    open SAMPLE, ">sample-enew";
    print SAMPLE "m 3 7 1\n";
    print SAMPLE "m $params{'network'}\n";
    print SAMPLE "s $params{'seed'}\n";
    print SAMPLE "ci $params{'weights'}\n";
    print SAMPLE "f ir c+\n";
    print SAMPLE "e $params{'eta'}\n";
    print SAMPLE "a $params{'momentum'}\n";

    print SAMPLE "rt wcap.trn\ntf wcap.tst\n";
    print SAMPLE "t $params{'tolerance'}\n";
    print SAMPLE "f P0\n";
    print SAMPLE "r $params{'maxruns'} 10\n";
    print SAMPLE "sw $params{'weightfile'}\n";
    print SAMPLE "q\n";
    close SAMPLE;
}


sub SetSampleEvaluate {
    my %params = 
	(
	 %currSample,
	 @_
	 );

    open SAMPLE, ">sample-eval";
    print SAMPLE "m $params{'network'}\n";
    print SAMPLE "s $params{'seed'}\n";
    print SAMPLE "ci $params{'weights'}\n";
    print SAMPLE "f ir c+\n";
    print SAMPLE "e $params{'eta'}\n";
    print SAMPLE "a $params{'momentum'}\n";
    print SAMPLE "rt wcap.trn\ntf wcap.tst\n";
    print SAMPLE "t $params{'tolerance'}\n";
    print SAMPLE "f P0\n";
    print SAMPLE "r $params{'maxruns'} 1000\n";
#    print SAMPLE "sw $params{'weightfile'}\n";
    print SAMPLE "tf wcap.eva\n";
    print SAMPLE "t\n";
    print SAMPLE "q\n";
    close SAMPLE;
}

sub perform {
    my %parms = @_;
    SetSample ( %parms);
    printf "A: %.1f n: %1.1f ", $parms{'momentum'}, $parms{'eta'};

    
    my $res = ExecuteBP;
}

sub main() {
    my $e = 1;
    my $m = 0;
    my $t = 10;
    print "A: moment, n: eta, NI: number of iterations before best error\n";
    print "MNI: The number of iterations before training converged, Eval: Evaluation test.\n";

#      for(my $e = 1 ;$e<10;$e++) {
#  	perform (eta => $e/10,  momentum => 0, tolerance => 0.1, weightfile => 'w' . $e . 'm0');
#  	perform (eta => $e/10,  momentum => 0.1, tolerance => 0.1 , weightfile => 'w' . $e . 'm1');
#  	perform (eta => $e/10,  momentum => 0.5, tolerance => 0.1, weightfile => 'w' . $e . 'm5');
#  	perform (eta => $e/10,  momentum => 0.9, tolerance => 0.1, weightfile => 'w' . $e . 'm9');
#  	print "\n";
#      }
#     print "Tolerance\n";
#     perform (eta => 0.1, momentum => 0.1, tolerance => 0.1);
#     perform (eta => 0.1, momentum => 0.1, tolerance => 0.09);
#     perform (eta => 0.1, momentum => 0.1, tolerance => 0.08);
#     perform (eta => 0.1, momentum => 0.1, tolerance => 0.07);
#     perform (eta => 0.1, momentum => 0.1, tolerance => 0.06);
#     perform (eta => 0.1, momentum => 0.1, tolerance => 0.05);
    perform (eta => 0.1, momentum => 0.1, network => "3 7 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 8 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 9 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 6 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 5 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 4 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 3 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 2 1");
    perform (eta => 0.1, momentum => 0.1, network => "3 7 1 x");
}


main;

#SetSample $%params;
# ExecuteBP;

