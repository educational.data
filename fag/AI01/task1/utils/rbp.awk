# This script extracts information from the `copy' file written
# by rbp.  Script written by Peter Ross, 1995.

# One or more lines, maybe including the last if it failed to converge,
# show the number of iterations; pick up that number (1st field):
/iterations/  { iter=$1 }

# If it converged OK, there will be a `patterns learned ..' line, so
# pick up the final iteration count from there instead (8th field):
/patterns/    { iter=$8 }

# A test file will produce four lines; the first two show the percentage
# right based on tolerance:
/tolerance/   { getline           # Want the NEXT line
                sub(/,/, "", $1)  # Eliminate the comma from 1st field
                tolright=$1 }     # Pick up the percentage figure

# .. and the next two show percentage right based on maximum value:
/maximum/     { getline           # Want the NEXT line
                sub(/,/, "", $1)  # Eliminate the comma from 1st field
                maxright=$1}      # Pick up the percentage right

# Finally, output the wanted information:
#   e   a   iterations-taken   test-performance-tol  test-performance-max
END           { printf("%.2f %.2f  %-4d    %7s %7s\n",
                         e,a,iter,tolright,maxright) }
