#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "matrix.h"

Matrix::Matrix(const int i, const int j) : matrix(i) {
  mi=i;
  mj=j;

  for(int p=0;p<i;p++ ) {
    
    matrix[p] = d_vector(j);
    
    for (int v=0;v<j;v++) 
      if(v==p)
	matrix[p][v] = 1;
      else
	matrix[p][v] = 0;
  }
}

Matrix::Matrix(const int i, const int j, const tBase val) : matrix(j) {
  mi=i;
  mj=j;

  for(int p=0;p<i;p++ ) {

    matrix[p] = d_vector(j);

    for (int v=0;v<j;v++) 
      matrix[p][v] = val;
  }
}
Matrix::Matrix(const int i, const int j, tBase *mat) : matrix(j) {
  mi=i;
  mj=j;

  for(int p=0;p<i;p++ ) {
    matrix[p] = d_vector(j);
    for (int v=0;v<j;v++) 
      matrix[p][v] = *mat++;
  }
}


Matrix::Matrix(const Matrix &mat) {
  matrix = mat.matrix;
  
  mi = mat.width();
  mj = mat.height();
}

Matrix::d_vector &Matrix::operator[](const int index) {
  return matrix[index];
}

int Matrix::width() const {
  return mi;
}

int Matrix::height() const {
  return mj;
}

Matrix Matrix::add(const Matrix &mat) {
  Matrix mnew(mat.width(),mat.height());
  for(int i=0;i<mat.width();i++) {
    for(int j=0;j<mat.height();j++) {
      mnew[i][j] = matrix[i][j] + mat.matrix[i][j];
    }
  }
  return mnew;
}
