#ifndef HEADER_MATRIX_H
# define HEADER_MATRIX_H

#include <vector>
using namespace std;

class Matrix {
  typedef double tBase;
  typedef vector<tBase> d_vector;
  typedef vector<d_vector> d_matrix;

  d_matrix matrix;
  int mi,mj;
public:
  Matrix(const int l, const int i);                  /* Initialize identity matrix */
  Matrix(const Matrix &mat);                         /* Copy contructor */
  Matrix(const int i, const int j, const tBase val); /* Initialize to fixed Value */
  Matrix(const int i, const int j, tBase *mat);      /* Initialise to C-style matrix */
  d_vector &operator[](const int index);

  Matrix add(const Matrix &mat);

  int width() const;
  int height() const;
};

#endif // HEADER_MATRIX_H
