#include <math.h>
#include <vector>
#include <iostream>
#include <iomanip>
#include <exception>

class out_of_bounds : public std::exception { };
class matrixsize_not_equal : public std::exception { };


typedef double dp;

class Vector {
  unsigned int rows;
  vector< dp > data;
public:
  Vector(const unsigned int size);
  Vector(const Vector &vec);
  Vector(const unsigned int size, const dp* vecdata);
  unsigned int nrows() const;
  dp &operator[](const unsigned int index);
  dp operator[](const unsigned int index) const;
  dp &at(const unsigned int index);
  dp at(const unsigned int index) const;
  void dump() const;
};






/// support class for matrix,
/// used to help making arguments.
class Matrix {
  vector < vector < dp > > data;
  unsigned int cols,rows;
public:
  Matrix(int rows, int cols);
  Matrix(const Matrix &mat);
  Matrix(const int col, const int row, const double *mat);
  dp rowsize(unsigned int row);
  void dump();
  void dump(std::vector<unsigned int> v);      
  dp &at(unsigned int row, unsigned int col);  
  dp at(unsigned int row, unsigned int col) const;
  vector< dp > &operator[](unsigned int row);
  const vector< dp > &operator[](unsigned int row) const;
  Matrix &operator=(const Matrix &mat);
  Matrix &operator+=(const Matrix &mat);
  const unsigned int nrows() const;
  const unsigned int ncols() const;
};

Matrix operator-(const Matrix &mat);
Vector operator-(const Vector &vec);
Vector operator+(const Vector &v1, const Vector &v2);
Matrix IMatrix(const unsigned int size);
Matrix operator+(const Matrix &a, const Matrix &b);
