#include <iostream>
#include <iomanip>
#include "linear.h"

using namespace std;

vector < int >
luDecomp (Matrix & a)
{
  vector < int >r (a.nrows ());
  for (unsigned int i = 0; i < a.nrows (); i++)
    r[i] = i;
  for (unsigned int k = 0; k < a.nrows () - 1; k++) {
    int big = k;
    unsigned int m;
    if (a[r[k]][k] == 0) {
      for (m = k; m < a.ncols (); m++) {
        double dum = a.rowsize (r[m]);
        if (dum > 0 && dum > a.rowsize (r[big]))
          big = m;
      }
    }
    swap (r[big], r[k]);
  }
  for (unsigned int k = 0; k < a.ncols () - 1; k++) {
    for (unsigned int i = k + 1; i < a.ncols (); i++) {
      double V = fabs (a[r[i]][k] / a[r[k]][k]);

      a[r[i]][k] = V;

      for (unsigned int j = k + 1; j < a.ncols (); j++)
        a[r[i]][j] = a[r[i]][j] - V * a[r[k]][j];
    }
  }
  return r;
}

void
forwardSub (const Matrix & a, Vector & b, vector < int >&r)
{
  for (unsigned int k = 0; k < a.nrows () - 1; k++)
    for (unsigned int i = k + 1; i < a.nrows (); i++)
      b[i] = b[i] - a[r[i]][k] * b[k];
}

Vector
backSub (const Matrix & a, const Vector & b, const vector < int >&r)
{
  Vector x (a.nrows ());
  x[a.nrows () - 1] =
    b[a.nrows () - 1] / a[r[a.nrows () - 1]][a.nrows () - 1];
  for (int i = (int)a.nrows () - 2; i >= 0; i--) {
    double sum = 0;

    for (unsigned int j = i + 1; j < a.nrows (); j++)
      sum += a[r[i]][j] * x[j];
    x[i] = (b[i] - sum) / a[r[i]][i];

  }
  return x;
}

Vector
Solve (Matrix & a, Vector & b)
{
  vector < int >r = luDecomp (a);
  forwardSub (a, b, r);
  Vector result = backSub (a, b, r);
  return result;
}
 
Vector
userMinusF (const Vector & a)
{
  Vector F (5);
  double k1 = 0.24;
  double k2 = 0.31;
  double k3 = 1.0;
  double k4 = 1E8;
  double k5 = 1E-5;
  double k6 = 0.1;
  double k7 = 1E8;
  double k8 = 1E-4;
  
  double A  = a[0];
  double B  = a[1];
  double C  = a[2];
  double D  = a[3];
  double H  = a[4];

  double dA = -(k1+k8)*A + k2*B                     + k7*H*D;
  double dB = k1*A       - (k2+k3)*B  + k4*H*C;
  double dC =              k3*B       - (k4+k5)*H*C + k6*H*D;
  double dD = A+B+C+D - 1;
  double dH = C+D+1E-5 - H;
  F[0] = -dA;
  F[1] = -dB;
  F[2] = -dC;
  F[3] = -dD;
  F[4] = -dH;
  return F;
}

Matrix
userDF (const Vector & a)
{
  double k1 = 0.24;
  double k2 = 0.31;
  double k3 = 1.0;
  double k4 = 1E8;
  double k5 = 1E-5;
  double k6 = 0.1;
  double k7 = 1E8;
  double k8 = 1E-4;

  
  double A  = a[0];
  double B  = a[1];
  double C  = a[2];
  double D  = a[3]; 
  double H  = a[4];

  double m[] = {
    -k1-k8,     k2,       0,          k7*H,     k7*D,
    k1,         -k2-k3,   k4*H,       0,        k4*C,
    0,          k3,       -k4-k5*H,   k6*H,     -(k4+k5)*C + D*k6,
    1,          1,        1,          1,        0,
    0,          0,        1,          1,        -1
  };
  return Matrix (5, 5, m);
}

Vector
NewtonIteration (const Vector & a)
{
  Vector temp = a;
  Matrix DF = userDF (a);
  Vector mF = userMinusF (a);
  return temp + Solve(DF, mF);
}

Matrix
NewtonSolve (const Vector & startguess, const unsigned int steps)
{
  Vector a = startguess;
  Matrix dump (startguess.nrows()*3, steps + 1);

  for (unsigned int c = 0; c < startguess.nrows (); c++)
    dump[0][c] = startguess[c];

  for (unsigned int k = 1; k <= steps; k++) {
    Vector temp = NewtonIteration(a);
    a = temp;
    for (unsigned int r = 0; r < startguess.nrows (); r++)
      dump[k][r] = a[r];
  }

  for (unsigned int k = 1; k <= steps; k++)
    for (unsigned int c = 0; c < startguess.nrows (); c++)
      dump[k][startguess.nrows() + c] = dump[k][c] - dump[k - 1][c];

  for (unsigned int k = 2; k <= steps; k++)
    for (unsigned int r = 0; r < startguess.nrows (); r++)
      dump[k][startguess.nrows()*2 + r] = dump[k][startguess.nrows() + r] / pow (dump[k - 1][startguess.nrows() + r], 2);

  return dump;
}

void
dumpItter (const Matrix & mat)
{
  cout << setprecision (16);
  cout << "k";
  for(int c = 0;c<mat.ncols()/3;c++)
    cout << "\talpha_" << c+1 << "\t";
  cout << endl <<
    "-------------------------------------------------------------------------"
    << endl;
  for (unsigned int k = 0; k < mat.nrows (); k++) {
    cout << k << "\t";

    for (unsigned int col = 0; col < mat.ncols () /3; col++)
      cout << mat[k][col] << "\t";

    cout << endl;
  }

  cout << endl;
  cout << setprecision (5);
  cout << "k";
  for(int c = 0;c<mat.ncols()/3;c++)
    cout << "\td_" << c+1 << "\t";

  cout << endl <<
    "---------------------------------------------------" << endl;

  for (unsigned int k = 1; k < mat.nrows (); k++) {
    cout << k << "\t";

    for (unsigned int col = mat.ncols () / 3; col < 2 * mat.ncols () / 3; col++)
      cout << mat[k][col] << "    \t";

    cout << endl;
  }

  cout << endl;
  cout << setprecision (5);
  cout << "k";
  for(int c = 0;c<mat.ncols()/3;c++)
    cout << "\tC__" << c+1 << "\t";

  cout << endl << "---------------------------------------------------" << endl;

  for (unsigned int k = 2; k < mat.nrows (); k++) {
    cout << k << "\t";

    for (unsigned int col = 2 * mat.ncols () / 3; col < mat.ncols (); col++)
      cout << mat[k][col] << "    \t";

    cout << endl;
  }
}

int
main ()
{
  try {
    // double alpha[] = { 2, 2, 2};
    // Vector vAlpha(3,alpha);
    double alpha[] = { 1, 0, 0, 0, 1E-7 };
    Vector vAlpha (5, alpha);
    Matrix result = NewtonSolve (vAlpha, 20);

    dumpItter(result);
  } 
  catch (exception & ex) {
    cerr << "Exception thrown: ";
    cerr << ex.what () << endl;
    exit (1);
  }
  return 0;
}

