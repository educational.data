#include "linear.h"
#include <iostream>

inline void swap(dp &i, dp &j) {
  dp tm = i;
  i = j;
  j = tm;
}

Vector::Vector(const unsigned int size) {
  rows = size;
  data.resize(size);
  for(unsigned int i=0;i<size;i++)
    data[i] = 0.0;
}
Vector::Vector(const Vector &vec) {
  rows = vec.nrows();
  data.resize(rows);
  data = vec.data;//.assign(vec.begin(),vec.end());
}
Vector::Vector(const unsigned int size, const dp* vecdata) {
  rows = size;
  data.resize(rows);
  for (int pos = 0;pos < size;pos++)
    data[pos] = vecdata[pos];
}

unsigned int Vector::nrows() const {
  return rows;
}
dp &Vector::operator[](const unsigned int index) {
  if(index < 0 || index>=rows) throw out_of_bounds();
  return data[index];
}
dp Vector::operator[](const unsigned int index) const {
  if(index < 0 || index>=rows) throw out_of_bounds();
  return data[index];
}
dp &Vector::at(const unsigned int index) {
  if(index < 0 || index>=rows) throw out_of_bounds();
  return data[index];
}
dp Vector::at(const unsigned int index) const {
  if(index < 0 || index>=rows) throw out_of_bounds();
  return data[index];
}
void Vector::dump() const {
  using namespace std;
  cout << setprecision(10);
  cout << "Vector [" << rows << "] = {" << endl;
  for(unsigned int i=0;i<rows;i++) {
    cout << "\t";
    if(at(i) != at(i)) 
      cout << "Not a Number" << endl;
    else
      cout << at(i) << endl;
  }
  cout << "}" << endl;
}






/// support class for matrix,
/// used to help making arguments.

Matrix::Matrix(int cols, int rows) {
  data.resize(rows);

  for (int c=0;c<rows;c++)
    data[c].resize(cols);

  this->rows = rows;
  this->cols = cols;
}
Matrix::Matrix(const Matrix &mat) {
  data = mat.data;
  rows = mat.nrows();
  cols = mat.ncols();
}
Matrix::Matrix(const int col, const int row, const double *mat) {
  rows=row;
  cols=col;
  data.resize(rows);
  int pos = 0;
  for (int c=0;c<rows;c++) {
    data[c].resize(cols);
    for(int r=0;r<cols;r++) 
      data[c][r] = mat[pos++];
  }
}
    
dp 
Matrix::rowsize(unsigned int row) {
  if(row >= nrows())
    throw out_of_bounds();

  dp retval = 0.0;
  for(unsigned int c=0;c<cols;c++)
    retval += fabs(data[row][c]);
  return retval;
}
void 
Matrix::dump() {
  using namespace std;
  cout << setprecision(10);
  cout << "Matrix [" << rows << "," << cols << "] = {" << endl;
  for(unsigned int i = 0;i<rows;i++) {
    cout << "\t";
    for(unsigned int j = 0;j<cols;j++) {
      cout << data[i][j] << "\t";
    }
    cout << endl;
  }
  cout << "}" << endl;
}
void 
Matrix::dump(std::vector<unsigned int> v) {
  using namespace std;
  if(v.size() != rows) {
    cerr << "matrix::dump - v is not a representative sized vector:" << __LINE__ << endl;
    return;
  }
  cout << setprecision(10);
  cout << "Matrix[" << rows << ":" << cols << "] = {" << endl;
  for(unsigned int i = 0;i<cols;i++) {
    cout << "\t";
    for(unsigned int j = 0;j<rows;j++) {
      cout << data[v[i]][j] << "\t";
    }
    cout << endl;
  }
  cout << "}" << endl;
}
      
dp &
Matrix::at(unsigned int row, unsigned int col) {
  if(row >= nrows())
    throw out_of_bounds();
  if(col >= ncols())
    throw out_of_bounds();
  return data[row][col];
}
  
dp 
Matrix::at(unsigned int row, unsigned int col) const {
  if(row < 0 && row >= nrows())
    throw out_of_bounds();
  if(col < 0 && col >= ncols())
    throw out_of_bounds();
  return data[row][col];
}
  
vector< dp > &
Matrix::operator[](unsigned int row) {
  if(row < 0 && row >= nrows())
    throw out_of_bounds();
  return data[row];
}
const vector< dp > &
Matrix::operator[](unsigned int row) const{
  if(row < 0 && row >= nrows())
    throw out_of_bounds();
  return data[row];
}
Matrix &
Matrix::operator=(const Matrix &mat) {
  data = mat.data;
  rows = mat.nrows();
  cols = mat.ncols();
  return *this;
}
Matrix &
Matrix::operator+=(const Matrix &mat) {
  if(mat.ncols() != ncols() ||
     mat.nrows() != nrows()) 
    throw matrixsize_not_equal();
    
  for(unsigned int c=0;c<ncols();c++) 
    for(unsigned int r=0;r<nrows();r++)
      data[c][r] = data[c][r] + mat.data[c][r];
  return *this;
}
const unsigned int 
Matrix::nrows() const {
  return rows;
}
const unsigned int 
Matrix::ncols() const {
  return cols;
}

Matrix operator-(const Matrix &mat) {
  Matrix mat2 = mat;
  for(unsigned int col=0;col<mat.ncols();col++)
    for(unsigned int row=0;row<mat.nrows();row++)
      mat2[col][row] = -(mat2[col][row]);
  return mat2;
}

Vector operator-(const Vector &vec) {
  Vector vec2 = vec;
  for (unsigned int row=0;row<vec2.nrows();row++) 
    vec2[row] = -(vec[row]);
  return vec2;
}

Vector operator+(const Vector &v1, const Vector &v2) {
  if(v1.nrows() != v2.nrows()) 
    throw matrixsize_not_equal();

  Vector res = v1.nrows();
  for(unsigned int r=0;r<res.nrows();r++) {
    res[r] = v1[r] + v2[r];
  }
  return res;
}  

Matrix IMatrix(const unsigned int size) {
  Matrix mat(size,size);

  for(unsigned int i=0;i<size;i++) 
    mat[i][i] = 1.0;

  return mat;
}

Matrix operator+(const Matrix &a, const Matrix &b) {
  if(a.ncols() != b.ncols() ||
     a.nrows() != b.nrows()) throw matrixsize_not_equal();

  Matrix ret(a.ncols(),a.nrows());
  for(unsigned int c=0;c<a.ncols();c++) 
    for(unsigned int r=0;r<a.nrows();r++)
      ret[c][r] = a[c][r] + b[c][r];

  return ret;
}

