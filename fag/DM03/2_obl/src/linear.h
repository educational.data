#include <math.h>
#include <vector>
#include <iostream>
#include <exception>

class out_of_bounds : public std::exception { };
class matrixsize_not_equal : public std::exception { };


typedef double dp;

inline dp swap(dp &i, dp &j) {
  dp tm = i;
  i = j;
  j = tm;
}

class Vector {
  dp *data;
  int rows;
public:
  Vector(const int size) {
    rows = size;
    data = new dp[size];
    for(int i=0;i<size;i++)
      data[i] = 0.0;
  }
  Vector(const Vector &vec) {
    rows = vec.nrows();
    data = new dp[rows];
    memcpy(data,vec.data,rows*sizeof(dp));
  }
  Vector(const int size, const dp* vecdata) {
    rows = size;
    data = new dp[rows];
    memcpy(data,vecdata,rows*sizeof(dp));
  }
  ~Vector() {
    delete[] data;
  }
  int nrows() const {
    return rows;
  }
  dp &operator[](int index) {
    if(index < 0 || index>=rows) throw out_of_bounds();
    return data[index];
  }
  dp operator[](int index) const {
    if(index < 0 || index>=rows) throw out_of_bounds();
    return data[index];
  }
  dp &at(const int index) {
    if(index < 0 || index>=rows) throw out_of_bounds();
    return data[index];
  }
  dp at(const int index) const {
    if(index < 0 || index>=rows) throw out_of_bounds();
    return data[index];
  }
  void dump() const {
    using namespace std;
    cout << setprecision(10);
    cout << "Vector [" << rows << "] = {" << endl;
    for(int i=0;i<rows;i++) {
      cout << "\t";
      if(at(i) != at(i)) 
	cout << "Not a Number" << endl;
      else
	cout << at(i) << endl;
    }
    cout << "}" << endl;
  }
};






/// support class for matrix,
/// used to help making arguments.
class Matrix {
  dp *data;
  int cols,rows;
public:
  Matrix(int cols, int rows) {
    data = new dp[rows*cols];
    this->rows = rows;
    this->cols = cols;
  }
  Matrix(const Matrix &mat) {
    data = new dp[mat.nrows() * mat.ncols()];
    memcpy(data,mat.data,mat.nrows() * mat.ncols() * sizeof(dp));
    rows = mat.nrows();
    cols = mat.ncols();
  }
  Matrix(const int col, const int row, const double *mat) {
    rows=row;
    cols=col;
    data = new dp[rows*cols];
    memcpy(data,mat,rows*cols*sizeof(double));
  }
    
  ~Matrix() {
    delete[] data;
  }
  dp rowsize(int row) {
    if(row >= nrows())
      throw out_of_bounds();

    dp retval = 0.0;
    for(int c=0;c<rows;c++)
      retval += fabs(at(row,c));
    return retval;
  }
  void dump() {
    using namespace std;
    cout << setprecision(10);
    cout << "Matrix [" << rows << "," << cols << "] = {" << endl;
    for(int i = 0;i<cols;i++) {
      cout << "\t";
      for(int j = 0;j<rows;j++) {
	cout << at(i,j) << "\t";
      }
      cout << endl;
    }
    cout << "}" << endl;
  }
  void dump(std::vector<int> v) {
    using namespace std;
    if(v.size() != rows) {
      cerr << "matrix::dump - v is not a representative sized vector:" << __LINE__ << endl;
      return;
    }
    cout << setprecision(10);
    cout << "Matrix[" << rows << ":" << cols << "] = {" << endl;
    for(int i = 0;i<cols;i++) {
      cout << "\t";
      for(int j = 0;j<rows;j++) {
	cout << at(v[i],j) << "\t";
      }
      cout << endl;
    }
    cout << "}" << endl;
  }
      
  dp &at(int row, int col) {
    if(row < 0 && row >= nrows())
      throw out_of_bounds();
    if(col < 0 && col >= ncols())
      throw out_of_bounds();
    return data[row*cols+col];
  }
  
  dp at(int row, int col) const {
    if(row < 0 && row >= nrows())
      throw out_of_bounds();
    if(col < 0 && col >= ncols())
      throw out_of_bounds();
    return data[row*cols+col];
  }
  
  dp *operator[](int row) {
    if(row < 0 && row >= nrows())
      throw out_of_bounds();
    return &(data[row*cols]);
  }
  dp *operator[](int row) const{
    if(row < 0 && row >= nrows())
      throw out_of_bounds();
    return &(data[row*cols]);
  }
  Matrix &operator=(const Matrix &mat) {
    delete[] data;

    data = new dp[mat.nrows() * mat.ncols()];
    memcpy(data,mat.data,mat.nrows() * mat.ncols() * sizeof(dp));
    rows = mat.nrows();
    cols = mat.ncols();

    return *this;
  }
  Matrix &operator+=(const Matrix &mat) {
    if(mat.ncols() != ncols() ||
       mat.nrows() != nrows()) 
      throw matrixsize_not_equal();
    
    for(int c=0;c<ncols();c++) 
      for(int r=0;r<nrows();r++)
	at(c,r) = at(c,r) + mat[c][r];
    return *this;
  }
  const int nrows() const {
    return rows;
  }
  const int ncols() const {
    return cols;
  }
};

inline Matrix operator-(const Matrix &mat) {
  Matrix mat2 = mat;
  for(int col=0;col<mat.ncols();col++)
    for(int row=0;row<mat.nrows();row++)
      mat2[col][row] = -(mat2[col][row]);
  return mat2;
}

inline Vector operator-(const Vector &vec) {
  Vector vec2 = vec;
  for (int row=0;row<vec2.nrows();row++) 
    vec2[row] = -(vec[row]);
  return vec2;
}

inline Vector operator+(const Vector &v1, const Vector &v2) {
  if(v1.nrows() != v2.nrows()) 
    throw matrixsize_not_equal();

  Vector res = v1.nrows();
  for(int r=0;r<res.nrows();r++) {
    res[r] = v1[r] + v2[r];
  }
  return res;
}  

inline Matrix IMatrix(const int size) {
  Matrix mat(size,size);

  for(int i=0;i<size;i++) 
    mat[i][i] = 1.0;

  return mat;
}

Matrix operator+(const Matrix &a, const Matrix &b) {
  if(a.ncols() != b.ncols() ||
     a.nrows() != b.nrows()) throw matrixsize_not_equal();

  Matrix ret(a.ncols(),a.nrows());
  for(int c=0;c<a.ncols();c++) 
    for(int r=0;r<a.nrows();r++)
      ret[c][r] = a[c][r] + b[c][r];

  return ret;
}
