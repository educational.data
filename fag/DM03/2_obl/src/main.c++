#include <iostream>
#include <iomanip>
#include "linear.h"

using namespace std;

vector < int >
luDecomp (Matrix & a)
{
  vector < int >r (a.nrows ());
  for (int i = 0; i < a.nrows (); i++)
    r[i] = i;

  for (int k = 0; k < a.nrows () - 1; k++) {
    int big = k;
    int m;
    if (a[r[k]][k] == 0) {
      for (m = k; m < a.nrows (); m++) {
        double dum = a.rowsize (r[m]);
        if (dum > 0 && dum > a.rowsize (r[big]))
          big = m;
      }
    }
    swap (r[big], r[k]);
  }
  for (int k = 0; k < a.nrows () - 1; k++) {
    for (int i = k + 1; i < a.nrows (); i++) {
      double V = fabs (a[r[i]][k] / a[r[k]][k]);

      a[r[i]][k] = V;

      for (int j = k + 1; j < a.ncols (); j++)
        a[r[i]][j] = a[r[i]][j] - V * a[r[k]][j];
    }
  }
  return r;
}

void
forwardSub (const Matrix & a, Vector & b, vector < int >&r)
{
  for (int k = 0; k < a.nrows () - 1; k++)
    for (int i = k + 1; i < a.nrows (); i++)
      b[i] = b[i] - a[r[i]][k] * b[k];
}

Vector
backSub (const Matrix & a, const Vector & b, const vector < int >&r)
{
  Vector x (a.nrows ());
  x[a.nrows () - 1] =
    b[a.nrows () - 1] / a[r[a.nrows () - 1]][a.nrows () - 1];
  for (int i = a.nrows () - 2; i >= 0; i--) {

    double sum = 0;

    for (int j = i + 1; j < a.nrows (); j++)
      sum += a[r[i]][j] * x[j];

    x[i] = (b[i] - sum) / a[r[i]][i];
  }
  return x;
}

Vector
Solve (Matrix & a, Vector & b)
{
  vector < int >r = luDecomp (a);
  forwardSub (a, b, r);
  return backSub (a, b, r);
}

Vector
userMinusF (const Vector & a)
{
  Vector F (3);
  double K1 = 14.0;
  double K2 = 6.0;
  double K3 = 35.0;

  double K12 = -6.0;
  double K13 = -20.0;
  double K23 = 12.0;

  double AB_2 = 64.0;
  double AC_2 = 144.0;
  double BC_2 = 36.0;

  F[0] =
    -(K1 * a[0] * a[0] + K2 * a[1] * a[1] - 2 * K12 * a[0] * a[1] - AB_2);
  F[1] =
    -(K1 * a[0] * a[0] + K3 * a[2] * a[2] - 2 * K13 * a[0] * a[2] - AC_2);
  F[2] =
    -(K2 * a[1] * a[1] + K3 * a[2] * a[2] - 2 * K23 * a[1] * a[2] - BC_2);
  return F;
}

Matrix
userDF (const Vector & a)
{
  double m[] = {
    28 * a[0] + 12 * a[1], 12 * a[0] + 12 * a[1], 0,
    28 * a[0] + 40 * a[2], 0, 40 * a[0] + 70 * a[2],
    0, 12 * a[1] - 24 * a[2], 70 * a[2] - 24 * a[1]
  };
  return Matrix (3, 3, m);
}

Vector
NewtonIteration (const Vector & a)
{
  Vector temp = a;
  Matrix DF = userDF (a);
  Vector mF = userMinusF (a);
  return temp + Solve (DF, mF);
}

Matrix
NewtonSolve (const Vector & startguess, const int steps)
{
  Vector a = startguess;
  Matrix dump (9, steps + 1);

  for (int r = 0; r < startguess.nrows (); r++)
    dump[r][0] = startguess[r];

  for (int k = 1; k <= steps; k++) {
    Vector temp = a;
    Matrix DF = userDF (a);
    Vector mF = userMinusF (a);
    a = temp + Solve (DF, mF);
    for (int r = 0; r < startguess.nrows (); r++)
      dump[r][k] = a[r];
  }

  for (int k = 1; k <= steps; k++)
    for (int r = 0; r < startguess.nrows (); r++)
      dump[3 + r][k] = dump[r][k] - dump[r][k - 1];


  for (int k = 2; k <= steps; k++)
    for (int r = 0; r < startguess.nrows (); r++)
      dump[6 + r][k] = dump[3 + r][k] / pow (dump[3 + r][k - 1], 2);

  return dump;
}

void
dumpItter (const Matrix & mat)
{
  cout << setprecision (16);
  cout << "k\talpha_1\t\t\talpha_2\t\t\talpha_3" << endl;
  cout <<
    "-------------------------------------------------------------------------"
    << endl;
  for (int k = 0; k < mat.nrows (); k++) {
    cout << k << "\t";

    for (int col = 0; col < mat.ncols () / 3; col++)
      cout << mat[col][k] << "\t";

    cout << endl;
  }

  cout << endl;
  cout << setprecision (5);
  cout << "k\td_1\t\td_2\t\td_3" << endl;
  cout << "---------------------------------------------------" << endl;

  for (int k = 1; k < mat.nrows (); k++) {
    cout << k << "\t";

    for (int col = mat.ncols () / 3; col < 2 * mat.ncols () / 3; col++)
      cout << mat[col][k] << "    \t";

    cout << endl;
  }

  cout << endl;
  cout << setprecision (5);
  cout << "k\tC_1\t\tC_2\t\tC_3" << endl;
  cout << "---------------------------------------------------" << endl;

  for (int k = 2; k < mat.nrows (); k++) {
    cout << k << "\t";

    for (int col = 2 * mat.ncols () / 3; col < mat.ncols (); col++)
      cout << mat[col][k] << "    \t";

    cout << endl;
  }
}

int
main ()
{
  try {
    double alpha[] = { 2, 2, 2 };
    Vector vAlpha (3, alpha);
    Matrix result = NewtonSolve (vAlpha, 4);
    dumpItter (result);
  } 
  catch (exception & ex) {
    cerr << "Exception thrown: ";
    cerr << ex.what () << endl;
    exit (1);
  }
  return 0;
}
