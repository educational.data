dnl -*-sh-*-
dnl Copyright (C) 2007 Tobias Nielsen <tobibobi@gmail.com>
dnl 
dnl This program is free software; you can redistribute it and/or modify
dnl it under the terms of the GNU General Public License as published by
dnl the Free Software Foundation; either version 2 of the License, or
dnl (at your option) any later version.
dnl 
dnl This program is distributed in the hope that it will be useful,
dnl but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl GNU General Public License for more details.
dnl 
dnl You should have received a copy of the GNU General Public License
dnl along with this program; if not, write to the Free Software
dnl Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
dnl 
dnl $Revision: 1.12 $
dnl $Author: tobibobi $

AC_PREREQ(2.59)
AC_INIT(odin_framework,0.1.210,[tobibobi@gmail.com])
AC_REVISION([$Revision: 1.12 $])
AC_CONFIG_AUX_DIR(config)

AC_ARG_WITH([arm-elf],
    AC_HELP_STRING([--with-arm-elf=[location]],
        [Where to look for arm-elf-* based tools. 
(only valid together with "--with-ecos=...")]),
    [ac_cv_arm_elf=$withval],
    [ac_cv_arm_elf=none])

if ( test x$ac_cv_arm_elf == xyes ) || ( test x$ac_cv_arm_elf == xno ); then
    AC_MSG_ERROR([You need to specify a location for --with-arm-elf=...])
fi

AC_ARG_WITH([ecos],
    AC_HELP_STRING([--with-ecos=[location]],
        [Specify location of ecos for compilation with ecos]),
    [ac_cv_ecos_home=$withval],
    [ac_cv_ecos_home=none])


AM_INIT_AUTOMAKE
AC_CONFIG_SRCDIR([src/config.h.in])
AC_CONFIG_HEADER([src/config.h])



# AM_MAINTAINER_MODE

dnl Test for debugging

default_debug=yes
default_documentation=yes


AC_PROG_CC
AC_PROG_CXX
AC_PROG_CPP

ARM_CC=${CC}
ARM_CXX=${CXX}
ARM_CPP=${CPP}
ARM_AR=ar
ARM_AS=as
ARM_RANLIB=ranlib

if test x$cross_compiling == xyes; then
  if test x$host_alias != "xarm-elf"; then
    AC_MSG_FAILURE([Cross compile for $host_alias not supported.])
  fi 
fi

CFLAGS="${CFLAGS=}"


if test x$ac_cv_ecos_home != xnone; then
    we_are_building_for_odin_modules=yes
    if test x$ac_cv_ecos_home == xyes; then
        AC_MSG_ERROR([You need to give a statement for --with-ecos-home=[location] in order for this statement to be usefull])
        exit -1
    fi

    ECOS_HOME=$ac_cv_ecos_home

    #### INCLUDE
    AC_MSG_CHECKING([for include directory for eCos])
    if test -d ${ECOS_HOME}/include; then
        ECOS_INCLUDE=$( cd ${ECOS_HOME}/include;pwd )
    elif test -d ${ECOS_HOME}/install/include; then
        ECOS_INCLUDE=$( cd ${ECOS_HOME}/install/include;pwd )
    fi
    AC_MSG_RESULT([$ECOS_INCLUDE])
    if test -z "$ECOS_INCLUDE"; then 
        AC_MSG_ERROR([Not found a valid include location for the eCos])
        exit -1
    fi

    #### LIB
    AC_MSG_CHECKING([for lib directory for eCos])
    
    if test -d ${ECOS_HOME}/lib
    then
        ECOS_LIB=$( cd ${ECOS_HOME}/lib;pwd )
    elif test -d ${ECOS_HOME}/install/lib
    then
        ECOS_LIB=$( cd ${ECOS_HOME}/install/lib;pwd )
    fi
    AC_MSG_RESULT([$ECOS_LIB])
    if test -z "$ECOS_LIB"
    then 
        AC_MSG_ERROR([Not found a valid library location for the eCos])
        exit -1
    fi
    ECOS_PKGCONFIG=$ECOS_INCLUDE/pkgconf
    if test ! -f $ECOS_HOME/ecos.ecc && test ! -d $ECOS_PKGCONFIG
    then
        AC_MSG_ERROR([It doesnt appear that the directory is an eCos directory])
        exit -1
    fi
    AC_SUBST([ECOS_PKGCONFIG])
    AC_SUBST([ECOS_INCLUDE])
    AC_SUBST([ECOS_LIB])
    AC_SUBST([ECOS_HOME])

    default_debug=false

    AC_DEFINE([ON_MODULE],[],[Indicates if the code is for the module])
    MODULE_CFLAGS="-mcpu=arm7tdmi -I${ECOS_INCLUDE} -Wall -Wpointer-arith -Wstrict-prototypes -Winline -Wundef -g -O2 -ffunction-sections -fdata-sections"
    MODULE_CXXFLAGS="-mcpu=arm7tdmi -I${ECOS_INCLUDE} -fno-exceptions -ffunction-sections -fdata-sections -fno-rtti"
    MODULE_LDFLAGS="-nostartfiles -L${ECOS_LIB} -Ttarget.ld -mcpu=arm7tdmi -Wl,--gc-sections -Wl,-static -Wl,-fatal-warnings -g -nostdlib"
    BUILD_TARGET=MODULE
    
    AC_CHECK_PROG([PROG_GCC],[arm-elf-gcc],[yes],[no],[$ac_cv_arm_elf:$PATH:${HOME}/ecos/gnutools/arm-elf/bin])
    if test x$PROG_GCC == xyes; then
        ARM_CC=$as_dir/arm-elf-gcc
        ARM_CPP="$as_dir/arm-elf-gcc -E"
    else 
        AC_MSG_ERROR([Cannot find arm-elf tools])
    fi
    
    AC_CHECK_PROG([PROG_GXX],[arm-elf-g++],[yes],[no],[$ac_cv_arm_elf:$PATH:${HOME}/ecos/gnutools/arm-elf/bin])
    if test x$PROG_GXX == xyes; then
        ARM_CXX=$as_dir/arm-elf-g++
    else 
        AC_MSG_ERROR([Cannot find arm-elf tools])
    fi

    AC_CHECK_PROG([PROG_AR],[arm-elf-ar],[yes],[no],[$ac_cv_arm_elf:$PATH:${HOME}/ecos/gnutools/arm-elf/bin])
    if test x$PROG_AR == xyes; then
        ARM_AR=$as_dir/arm-elf-ar
    else 
        AC_MSG_ERROR([Cannot find arm-elf tools])
    fi

    AC_CHECK_PROG([PROG_AS],[arm-elf-as],[yes],[no],[$ac_cv_arm_elf:$PATH:${HOME}/ecos/gnutools/arm-elf/bin])
    if test x$PROG_AS == xyes; then
        ARM_AS=$as_dir/arm-elf-as
    else 
        AC_MSG_ERROR([Cannot find arm-elf tools])
    fi

    AC_CHECK_PROG([PROG_RANLIB],[arm-elf-ranlib],[yes],[no],[$ac_cv_arm_elf:$PATH:${HOME}/ecos/gnutools/arm-elf/bin])
    if test x$PROG_RANLIB == xyes; then
        ARM_RANLIB=$as_dir/arm-elf-ranlib
    else 
        AC_MSG_ERROR([Cannot find arm-elf tools])
    fi
    default_documentation=no
else
    BUILD_TARGET=SIMULATION
    AC_DEFINE([ON_BUILD_PLATFORM],[],[We are not on the platform (inverse of ON_MODULE)])
fi
AM_CONDITIONAL([BUILD_FOR_MODULES],[test x$ac_cv_ecos_home != xnone])

AC_SUBST([MODULE_CFLAGS])
AC_SUBST([MODULE_CXXFLAGS])
AC_SUBST([MODULE_LDFLAGS])
AC_SUBST([ARM_CC])
AC_SUBST([ARM_CXX])
AC_SUBST([ARM_CPP])
AC_SUBST([ARM_AR])
AC_SUBST([ARM_AS])
AC_SUBST([ARM_RANLIB])
AC_SUBST([BUILD_TARGET])

AC_CACHE_SAVE

AC_ARG_ENABLE([debug],
        AC_HELP_STRING([--disable-debug],
                [remove debug symbols and debug tests]),
        [ac_cv_debug=$enableval],
        [ac_cv_debug=$default_debug])

CFLAGS=`echo "$CFLAGS"|sed "s/-O.\|-g//g"` 
CXXFLAGS=`echo "$CXXFLAGS"|sed "s/-O.\|-g//g"` 
if test x$ac_cv_debug == xno; then
        CFLAGS="$CFLAGS -DNDEBUG"
	CXXFLAGS="$CXXFLAGS -DNDEBUG"
else
        CFLAGS="$CFLAGS -g"
	CXXFLAGS="$CXXFLAGS -g"
fi
AM_CONDITIONAL([BUILD_DEBUG],[test x$ac_cv_debug == xyes])

CFLAGS="$CFLAGS -Wall -Werror"

dnl Test for documentation

AC_ARG_ENABLE([documentation],
    AC_HELP_STRING([--disable-documentation],
        [use doxygen to build documentation]),
    [ac_cv_documentation=$enableval],
    [ac_cv_documentation=$default_documentation])


if test x$ac_cv_documentation == xyes; then
    AC_CHECK_PROGS(doxygen,[doxygen],no)
    export doxygen;
    if test x$doxygen == xno; then
        AC_MSG_ERROR([doxygen is needed in order to support the documentation of this project.])
    fi
    AC_SUBST([doxygen])
    
    AC_CHECK_PROGS(dot,[dot],no)
    if test x$dot == xno; then
        AC_MSG_ERROR([doxygen needs the dot tool from the graphviz package])
    fi
else
    echo "Documentation is not enabled"
fi
AM_CONDITIONAL([BUILD_DOCUMENTATION],[test x$ac_cv_documentation == xyes])
AC_SUBST(CFLAGS)


dnl AC_GNU_SOURCE

AC_LANG([C++])
if test x$we_are_building_for_odin_modules != xyes; then
dnl AC_CHECK_HEADERS([boost/utility.hpp boost/integer.hpp boost/thread/condition.hpp boost/thread/exceptions.hpp boost/operators.hpp],,
dnl	AC_MSG_ERROR([Missing boost libraries!!]))
echo hello world
else
	dnl test to see if we can locate ecos.
	AC_PATH_PROG([ECOSCONFIG],[ecosconfig],[],[$PATH:/opt/ecos/])
fi

AC_CHECK_HEADERS([stdexcept])

AC_CACHE_SAVE

AC_LANG([C])
AC_PROG_SED

AC_CACHE_SAVE


dnl AC_C_CONST
AC_C_STRINGIZE
AC_C_PROTOTYPES
AC_C_VOLATILE

if test x$we_are_building_for_odin_modules != xyes; then
AC_CHECK_LIB(pthread,pthread_create)
fi

AC_CACHE_SAVE

AM_SYS_POSIX_TERMIOS

# 
# Here all the makefiles and other generated files are added.
#

AC_CONFIG_FILES([
        Makefile
        src/Makefile
	src/libs/Makefile
        src/images/Makefile
        src/images/com_test/Makefile
        src/images/xbee_test/Makefile
        src/images/02V2_test_002/Makefile
	src/images/linearactuator/Makefile
	src/images/battery/Makefile
	src/images/xbee/Makefile
	src/utils/Makefile
        src/utils/xbee_utils/Makefile
	dox/doxyfile
        dox/Makefile
        makefiles/Makefile
        makefiles/openocd.cfg
        makefiles/openocdscript
])

AC_OUTPUT
echo
echo "Status after $0:"
echo 
echo "Building for $BUILD_TARGET"
echo 
echo " documentation:  $ac_cv_documentation"
echo " eCos:           $ac_cv_ecos_home"
echo " eCos-include:   $ECOS_INCLUDE"
echo " eCos-lib:       $ECOS_LIB"
echo " eCos-config:    $ECOS_PKGCONFIG"
echo
echo " module CC:      $ARM_CC"
echo " module CPP:     $ARM_CPP"
echo " module CXX:     $ARM_CXX"
echo " module AS:      $ARM_AS"
echo " module AR:      $ARM_AR"
echo " module RANLIB:  $ARM_RANLIB"
echo ""
echo "Build flags:"
echo " CFLAGS:         $MODULE_CFLAGS"
echo " CXXFLAGS:       $MODULE_CXXFLAGS"
echo " LDFLAGS:        $MODULE_LDFLAGS"
echo
echo "run make to compile now"
