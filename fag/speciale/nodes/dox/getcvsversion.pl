#!/usr/bin/perl
# -*- perl -*-

my $input = $ARGV[0];

my $path = $input;
my $file = $input;

$path =~ s/(.*)\/[^\/]+/$1/;
$file =~ s/.*\/([^\/]+)/$1/;
my $foundData = 0;
open ENTRIES,"<$path/CVS/Entries";
while(<ENTRIES>) {
    chomp;
    if(/$file/) {
	my $revision = $_;
	$revision =~ s/^\/[^\/]+\/([^\/]+)\/.*$/$1/;
	print $revision;
	print "\n";
        $foundData = 1;
    }
}

if ($foundData == 0) {
    @data="`svn info`";
    for(@data) {
        chomp;
        if(s/Revision: //) {
            print "$_\n";
            $foundData = 1;
        }
    }
}

if ($foundData == 0) {
    print "CHECKOUT\n";
}
