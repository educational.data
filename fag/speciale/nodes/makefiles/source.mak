# -*- makefile -*-

ifeq ($(BUILD_TARGET),MODULE)
include $(ECOS_PKGCONFIG)/ecos.mak
#AM_CFLAGS=$(ECOS_GLOBAL_CFLAGS)
#AM_LDFLAGS=$(ECOS_GLOBAL_LDFLAGS)

OPENOCD_CFG   = $(abs_top_builddir)/makefiles/openocd.cfg
OPENOCD_SCRIPT= $(abs_top_builddir)/makefiles/openocdscript
OPENOCD       = openocd

program: 
	$(MAKE) -C $(abs_top_builddir)/src/libs all
	$(MAKE) -C $(abs_top_builddir)/$(BUILDDIR) program-local; \

program-local:all
	rm -f .target.elf
	cp ${PROGRAM_TARGET} .target.elf
	@ echo ".start programming with openocd" 
	$(OPENOCD) -f $(OPENOCD_CFG)
	@ echo ".flash programming finished"
else
program:
	@echo "Programming is not supported when compiling for simulation."
	exit 1

endif