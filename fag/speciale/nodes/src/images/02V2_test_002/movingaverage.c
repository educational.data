 /*
 *  movingaverage.c
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

/* INCLUDES */
#include <malloc.h>
#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */
#include <cyg/hal/hal_arch.h>
#include "movingaverage.h"

/* DEFINES */

/* STATICS */

/* GLOBAL VARIABLES */

movingaverage_t *create_movingaverage(char size) {
  movingaverage_t *ma = malloc(sizeof(movingaverage_t));
  char cp;
  ma->ptr = 0;
  ma->size = size;
  ma->sum = 0;
  ma->data = malloc(sizeof(cyg_uint16) * size);
  
  for(cp=0;cp<size;cp++)
    *(ma->data + cp) = (cyg_uint16)0;
  
  return ma;
}

void release_movingaverage(movingaverage_t *ma) {
  free(ma->data);
  free(ma);
}

void insert_movingaverage(movingaverage_t *ma, cyg_uint16 val) {
  ma->sum = ma->sum - *(ma->data + ma->ptr) + val;
  *(ma->data + ma->ptr) = val;
  ma->ptr++;
  if (ma->ptr == ma->size) ma->ptr = 0;
}

int get_movingaverage(movingaverage_t *ma) {
  return ma->sum/ma->size;
}

