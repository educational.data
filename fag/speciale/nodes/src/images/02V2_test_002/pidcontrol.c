 /*
 *  pidcontrol.c
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

/* INCLUDES */
#include <malloc.h>
#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */
#include <cyg/hal/hal_arch.h>
#include "pidcontrol.h"

/* DEFINES */

/* STATICS */

/* GLOBAL VARIABLES */

pidcontrol_t *create_pidcontrol(double pGain, double iGain, double dGain, double iMax, double iMin) {
  pidcontrol_t *pid = malloc(sizeof(pidcontrol_t));
	
  pid->dState = 0;
  pid->iState = 0;
  pid->iMax = iMax;
  pid->iMin = iMin;
  pid->pGain = pGain;
  pid->iGain = iGain;
  pid->dGain = dGain;
  
  return pid; 
}

void release_pidcontrol(pidcontrol_t *pid) {
  free(pid);
}

double update_pidcontrol(pidcontrol_t * pid, double error, double position) {
  double pTerm,dTerm=0,iTerm=0;

  pTerm = pid->pGain * error;
  
  if (pid->iGain != 0) {
    pid->iState += error;
    if (pid->iState > pid->iMax) pid->iState = pid->iMax;
    else if (pid->iState < pid->iMin) pid->iState = pid->iMin;
    iTerm = pid->iGain * pid->iState;
  }
  
  if (pid->dGain != 0) {
    dTerm = pid->dGain * (position - pid->dState);
    pid->dState = position;
  }

  return pTerm + iTerm + dTerm;
}

