 /*
 *  g01V2.c
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

/* INCLUDES */
#include "AT91SAM7S256.h"
#include "Board.h"

/* DEFINES */

/* STATICS */

/* GLOBAL VARIABLES */

/* Set System LED (SYSLED) */
void sysled_set(char b) {
    volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
	
	if (b) pPIO->PIO_CODR = SYSLED;
	else pPIO->PIO_SODR = SYSLED;
}

/* Toggle System LED (SYSLED) */
void sysled_toggle(void) {
	volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
	
	if  ((pPIO->PIO_ODSR & SYSLED) == SYSLED)		// read previous state of SYSLED
		pPIO->PIO_CODR = SYSLED;					// turn SYSLED (DS1) on	
	else
		pPIO->PIO_SODR = SYSLED;
}

/* Set external power - 3.3V provided through connector */
void specificpwr_set(char b) {
	volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
	
	if (b) pPIO->PIO_CODR = PWRCTRL;
	else pPIO->PIO_SODR = PWRCTRL;
}

