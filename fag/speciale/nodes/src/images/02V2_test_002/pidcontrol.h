/*
 *  pidcontrol.h
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef pidcontrol_h
#define pidcontrol_h

#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */

typedef struct {
  double dState;
  double iState;
  double iMax, iMin;
  double pGain, iGain, dGain; 
} pidcontrol_t;

pidcontrol_t *create_pidcontrol(double, double, double, double, double);
void release_pidcontrol(pidcontrol_t *);
double update_pidcontrol(pidcontrol_t *, double, double);

#endif   // pidcontrol
