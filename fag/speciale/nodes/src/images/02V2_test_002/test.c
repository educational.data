/*
 *  test.c
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

/* CONFIGURATION CHEKCS */
#include <pkgconf/system.h>     /* which packages are enabled/disabled */
#ifdef CYGPKG_KERNEL
# include <pkgconf/kernel.h>
#endif
#ifdef CYGPKG_LIBC
# include <pkgconf/libc.h>
#endif

/* INCLUDES */
#include <stdio.h>                      /* printf */
#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */
#include <cyg/hal/hal_arch.h>

#include "AT91SAM7S256.h"
#include "g01V2.h"
#include "Board.h"
#include "s02V2.h"

/* Defines */
#define NTHREADS 2
#define STACKSIZE ( CYGNUM_HAL_STACK_SIZE_TYPICAL + 4096 )

/* STATICS */

static cyg_handle_t thread[NTHREADS];
static cyg_thread thread_obj[NTHREADS];
static char stack[NTHREADS][STACKSIZE];

static void debug_thread(CYG_ADDRESS data) {
  char command[8];
  printf("\nDebug");
  while(true) {
    printf("\n>");
    fflush(stdout);
    gets(command);
    if (strcmp("time",command) == 0) 
      printf("%u",(unsigned)cyg_current_time);
    else if(strcmp("s02V2",command) == 0) {
      s02V2_printDebug();
      fflush(stdout);
    } else if(strcmp("start",command) == 0) cyg_thread_resume(thread[1]);
    else if(strcmp("cal",command) == 0) s02V2_calibrate();
    else printf("Unknown command");
    cyg_thread_delay(1);
  }
}

static void main_thread(CYG_ADDRESS data) {
  
  while(true) {
    s02V2_setPosition(0);
    s02V2_setPosition(60);
    //cyg_thread_delay(100);
  }
}

void cyg_user_start(void) {  
  /* Welcome message */
  printf("Odin\n");

  /* Set up system LED (SYSLED) and Power Control pin (PWRCTRL) */
  AT91C_BASE_PIOA->PIO_PER = (SYSLED|PWRCTRL);
  AT91C_BASE_PIOA->PIO_OER = (SYSLED|PWRCTRL);
  AT91C_BASE_PIOA->PIO_SODR = (SYSLED|PWRCTRL);

  /* Set up system LED (SYSLED) and Power Control pin (PWRCTRL) */
  sysled_set(true);
  specificpwr_set(true);

  if (s02V2_initialize() == 0) printf("\ns02V2 initialized");
  else printf("\ns02V2 initialization failed");
  fflush(stdout);
  
  /* Set up threads */
  cyg_thread_create(12, debug_thread, (cyg_addrword_t) 0, "Debug thread", (void *)stack[0], STACKSIZE, &thread[0], &thread_obj[0]);
  cyg_thread_resume(thread[0]);

  /* Set up threads */
  cyg_thread_create(10, main_thread, (cyg_addrword_t) 0, "Main thread", (void *)stack[1], STACKSIZE, &thread[1], &thread_obj[1]);

}

