#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "common.h"
#include "hal/uart.h"
#include "nodetime.h"
#include "stdio.h"
#include "linear_actuator/s02V2.h"

struct leg_t *leg;

extern "C" void
_impure_ptr (void)
{
}


extern "C" void recieve_data0(void *ptr) {
  struct uart_t *uart = (struct uart_t *)ptr;
  byte buffer[10];
  uart_recieve_data(uart, &buffer, 10);
  printf("0 - %x,%x,%x,%x,%x-%x,%x,%x,%x,%x\n",
         buffer[0],
         buffer[1],
         buffer[2],
         buffer[3],
         buffer[4],
         buffer[5],
         buffer[6],
         buffer[7],
         buffer[8],
         buffer[9]);
}

extern "C" void recieve_data1(void *ptr) {
  struct uart_t *uart = (struct uart_t *)ptr;
  byte buffer[10];
  uart_recieve_data(uart, &buffer, 10);
  printf("1 - %x,%x,%x,%x,%x-%x,%x,%x,%x,%x\n",
         buffer[0],
         buffer[1],
         buffer[2],
         buffer[3],
         buffer[4],
         buffer[5],
         buffer[6],
         buffer[7],
         buffer[8],
         buffer[9]);
}

extern "C" void
cyg_user_start (void)
{
  struct uart_t *uart0 = create_uart (0);
  struct uart_t *uart1 = create_uart (1);
  s02V2_initialize();

  uart_tie (uart0, uart1);

  uart_register_recieve_callback(uart0,recieve_data0,uart0,10);
  uart_register_recieve_callback(uart1,recieve_data1,uart1,10);
}


