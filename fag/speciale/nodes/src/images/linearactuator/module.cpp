#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "common.h"
#include "hal/uart.h"
#include "nodetime.h"
#include "stdio.h"
#include "linear_actuator/s02V2.h"
#include "hal/AT91SAM7S256.h"
#include "linear_actuator/board.h"
#include "linear_actuator/s02V2.h"

struct leg_t *leg;

/* Defines */
#define TYPE_POSE 1
#define TYPE_ID 2

#define NTHREADS 2
#define STACKSIZE ( CYGNUM_HAL_STACK_SIZE_TYPICAL + 4096 )

/* STATICS */

static cyg_handle_t thread[NTHREADS];
static cyg_thread thread_obj[NTHREADS];
static char stack[NTHREADS][STACKSIZE];

bool new_command = false;
volatile int position = 0;

extern "C" void
_impure_ptr (void)
{
}

#if (MODULE_ID == -1)
# error MODULE_ID not defined
#endif

extern "C" void recieve_data0(void *ptr) {
  struct uart_t *uart = (struct uart_t *)ptr;
  byte buffer[28];
  static int pose_id = MODULE_ID+3;

  uart_recieve_data(uart, &buffer, 28);

  if(buffer[2] == TYPE_ID) 
    {
      printf("Requested redefinition of gait ID\n");
      for (int i=3;i<28;i++) 
	{
	  if(buffer[i] == MODULE_ID) 
	    {
	      pose_id = i;
	      printf("Recieved column id: %d\n",pose_id-3);
	    }
	}
    }

  if(buffer[2] == TYPE_POSE) 
    {
      position = buffer[pose_id];
      printf("Recieved pose %d\n",position);
      if ((position > 0 && position < 62) || position == 87)
	new_command = true;
    } 

  buffer[0] &= 127; // set bit 0 to 0
  //buffer[1] = MODULE_ID;
  //uart_send_message(uart,buffer,28);
}

static void main_thread(CYG_ADDRESS data) {
  //printf("Here\n");
  //static bool is_calibrated=false;
  while(true) 
    {
      if (new_command) {
	new_command = false;
	if(position != 87) 
	  {
	    //if(is_calibrated)
	      s02V2_setPosition(position-1);
	    //else
	      //printf("Not allowed - missing calibration.\n");
	  }
	else 
	  {
	    s02V2_calibrate();
	    //is_calibrated = true;
	  }
      }
      cyg_thread_delay(10);
    }  
}

extern "C" void
cyg_user_start (void)
{
  struct uart_t *uart0 = create_uart (0);
  struct uart_t *uart1 = create_uart (1);

  /* Set up system LED (SYSLED) and Power Control pin (PWRCTRL) */
  AT91C_BASE_PIOA->PIO_PER = (SYSLED|PWRCTRL);
  AT91C_BASE_PIOA->PIO_OER = (SYSLED|PWRCTRL);
  AT91C_BASE_PIOA->PIO_SODR = (SYSLED|PWRCTRL);

  s02V2_initialize();

  uart_tie (uart0, uart1);

  uart_register_recieve_callback(uart0,recieve_data0,uart0,1);
  uart_register_recieve_callback(uart1,recieve_data0,uart1,1);

  printf("EXT: %X\n",MODULE_ID);

  /* Set up threads */
  cyg_thread_create(13, main_thread, (cyg_addrword_t) 0, "Main thread", (void *)stack[1], STACKSIZE, &thread[1], &thread_obj[1]);
  cyg_thread_resume(thread[1]);
}
