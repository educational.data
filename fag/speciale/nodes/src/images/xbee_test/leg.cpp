#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include "common.h"
#include "hal/uart.h"
#include "nodetime.h"
#include "stdio.h"

struct leg_t *leg;

extern "C" void
_impure_ptr (void)
{
}

struct uart_t *uart;
struct uart_t *xbee;

extern "C" void recieve_data(void *ptr) {
  byte buffer[10];
  uart_recieve_data(xbee, &buffer, 10);
  printf("%x,%x,%x,%x,%x-%x,%x,%x,%x,%x\n",
         buffer[0],
         buffer[1],
         buffer[2],
         buffer[3],
         buffer[4],
         buffer[5],
         buffer[6],
         buffer[7],
         buffer[8],
         buffer[9]);
}

void connect_system() {
  uart = create_uart (0);
  xbee = create_xbee_uart();
  
  uart_tie (uart, xbee);
  uart_register_recieve_callback(xbee,recieve_data,NULL,10);
}

extern "C" void recieve_data2(void *ptr) {
  byte buffer[10];
  uart_recieve_data(xbee, &buffer, 10);
}

void connect_other() {
  xbee = create_xbee_uart();
  uart_register_recieve_callback(xbee,recieve_data2,NULL,10);
}

extern "C" void
cyg_user_start (void)
{
  connect_system();
}


