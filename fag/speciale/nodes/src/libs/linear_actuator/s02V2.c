 /*
 *  s02V2.c
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

/* INCLUDES */
#include <stdio.h>                      /* printf */
#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */

#include "AT91SAM7S256.h"
#include "Board.h"
#include "g01V2.h"
#include "movingaverage.h"
#include "pidcontrol.h"
#include "cubicinterpolation.h"
#include "linear_actuator/s02V2.h"
#include "utils/lthread.h"
#include "utils/lmemory.h"

/* DEFINES */

/* STATICS */
static cyg_sem_t sem_s02V2;

static cyg_interrupt tacho_interrupt;
static cyg_handle_t tacho_interrupt_handle;
static cyg_interrupt tacho_timer_interrupt;
static cyg_handle_t tacho_timer_interrupt_handle;

static cyg_interrupt adc_interrupt;
static cyg_handle_t adc_interrupt_handle; 

static movingaverage_t *currentfilter = NULL;
static movingaverage_t *positionfilter = NULL;

static pidcontrol_t *currentpid = NULL;
static pidcontrol_t *speedpid = NULL;
static pidcontrol_t *positionpid = NULL;

struct calibration_data {
  unsigned int min;
  unsigned int max;
};
#define PAGE 500

/*static cyg_handle_t thread[2];
static cyg_thread thread_obj[2];
static char stack[2][ CYGNUM_HAL_STACK_SIZE_TYPICAL ];*/

/* GLOBAL VARIABLES */
bool initialized = false;
unsigned int tacho_count = 0;
unsigned int tacho_timer = 0;

//unsigned int current_desired = CURRENT_OFFSET;
//unsigned int speed_desired = 0;
//unsigned int position_desired = 0;

unsigned int position0 = 400;
unsigned int position60 = 600;

void s02V2_startADC(void);

/* INTERRUPT ROUTINES */
/* Tachometer interrupt service routine */
cyg_uint32 tacho_interrupt_isr( cyg_vector_t vector, cyg_addrword_t data ) {
  cyg_interrupt_mask( CYGNUM_HAL_INTERRUPT_TC2 | CYGNUM_HAL_INTERRUPT_IRQ0 ); // Prevent interrupt from occuring again
  cyg_interrupt_acknowledge( vector ); // Tell the kernel that we have recieved the interrupt

  if (vector == CYGNUM_HAL_INTERRUPT_IRQ0) // If interrupt was caused by tachometer
    tacho_count++; // Increment tacho counter
  
  if ((AT91C_BASE_TC2->TC_SR & (1<<0)) == (1<<0)) // If timer overflow has occured
    tacho_timer = 0xFFFF; // Set to maximum value
  else
    tacho_timer = AT91C_BASE_TC2->TC_CV; // Read timer register

  //printf("\n%u", tacho_timer);

  s02V2_startADC();

  AT91C_BASE_TC2->TC_CCR = ( AT91C_TC_CLKEN | AT91C_TC_SWTRG ); // Reset timer
  
  cyg_interrupt_unmask ( CYGNUM_HAL_INTERRUPT_TC2 | CYGNUM_HAL_INTERRUPT_IRQ0 ); // Allow interrupt to occur again
  return CYG_ISR_HANDLED; // Tell the kernel that the interrupt has been processed.
}

/* Read ADC */
cyg_uint32 adc_interrupt_isr( cyg_vector_t vector, cyg_addrword_t data ) {
    cyg_interrupt_mask( vector ); // Prevent interrupt from occuring again
    cyg_interrupt_acknowledge ( vector );
    return ( CYG_ISR_HANDLED | CYG_ISR_CALL_DSR );
}

void adc_interrupt_dsr(cyg_vector_t vector, cyg_ucount32 count, cyg_addrword_t data) {
  int status = AT91C_BASE_ADC->ADC_SR;

  //printf("adc");
  //fflush(stdout);

  if ((status & AT91C_ADC_EOC0) == AT91C_ADC_EOC0)
    insert_movingaverage(currentfilter,AT91C_BASE_ADC->ADC_CDR0);
  if ((status & AT91C_ADC_EOC2) == AT91C_ADC_EOC2)
    insert_movingaverage(positionfilter,AT91C_BASE_ADC->ADC_CDR2);    

  cyg_interrupt_unmask( vector ); // Allow interrupt to occur again
}

/* LOCAL FUNCTIONS */

void s02V2_enable(bool enable) {
  volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
  if (enable) pPIO->PIO_SODR = S02V2_ENABLE;
  else pPIO->PIO_CODR = S02V2_ENABLE;
}

void s02V2_brake(bool enable) {
  volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
  if (enable) pPIO->PIO_SODR = S02V2_BRAKE;
  else pPIO->PIO_CODR = S02V2_BRAKE;
}

void s02V2_setDirection(bool direction) { // true = contract, false = expand
  volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
  if (direction) pPIO->PIO_SODR = S02V2_DIRECTION;
  else pPIO->PIO_CODR = S02V2_DIRECTION;
}

signed int s02V2_getCurrent(void) {
  signed int current = get_movingaverage(currentfilter);
  return current;
}

double s02V2_getSpeed(void) {
  double speed = 0.0;
  if (tacho_timer < 0xFFFF)
    speed = (double)246402000.0/((double)tacho_timer*2752.0);
  return speed;
}

double s02V2_getPosition(void) {
  double position = ((double)60*((double)get_movingaverage(positionfilter)-position0))/(double)(position60-position0);
  return position;
}

void s02V2_setDutyCycle(unsigned int dutycycle) {
  if (dutycycle < 2) AT91C_BASE_PWMC_CH0->PWMC_CUPDR = 0x0002;
  else if  (dutycycle > MAX_DUTYCYCLE) AT91C_BASE_PWMC_CH0->PWMC_CUPDR = MAX_DUTYCYCLE;
  else AT91C_BASE_PWMC_CH0->PWMC_CUPDR = dutycycle;
}

unsigned int s02V2_getDutyCycle(void) {
  return AT91C_BASE_PWMC_CH0->PWMC_CDTYR;
}

void s02V2_startADC(void) {
  AT91C_BASE_ADC->ADC_CR = AT91C_ADC_START;
}

/* THREADS */
/* Position->Speed->Current->Motor controller */
double s02V2_control(signed int position_desired, signed int speed_desired, signed int current_desired) {
  double input = 0, output = 0, error = 0, position = 0;

  /* Position Controller */
  position = s02V2_getPosition();
  if (position_desired > -1) {
    error = (double)position_desired - position;
    output = update_pidcontrol(positionpid,error,position);
    
    if (output < 0) {
      output = -output;
      s02V2_setDirection(true);
    } else {
      s02V2_setDirection(false);
    }
    if (output > 50) output = 50;
  }

  /* Speed Controller */
  if (speed_desired > -1) {
    input = s02V2_getSpeed();
    error = output + speed_desired - input;
    output = update_pidcontrol(speedpid,error,input);
  
    if (output < 0) output = 0;
    else if (output > 700) output = 700;
  }

  /* Current Controller */
  input = (double)s02V2_getCurrent();
  if (current_desired < CURRENT_OFFSET) error = output + (double)CURRENT_OFFSET - input;
  else error = output + current_desired - input;
  output = update_pidcontrol(currentpid,error,input);
  
  if (output < 0) s02V2_setDutyCycle(0);
  else s02V2_setDutyCycle(output);
    
  return position;
}

void s02V2_controlReset(void) {
  currentpid->iGain = 0;
  currentpid->dGain = 0;
  speedpid->iGain = 0;
  speedpid->dGain = 0;
  positionpid->iGain = 0;
  positionpid->dGain = 0;
}

/* GLOBAL FUNCTIONS */
/* Initialize linear actuator
 *   - Returns true if succeeded or false if not */
int s02V2_initialize(void) { 
  int status = 0;
  struct calibration_data *calibration = (struct calibration_data *)(AT91C_IFLASH + AT91C_IFLASH_PAGE_SIZE*PAGE);
  
  if (!initialized) {
    if(calibration->min != 0xFFFFFFFF && calibration->max != 0xFFFFFFFF) {
      position0 = calibration->min;
      position60 = calibration->max;
      printf("Initialise linear controller with min: %d max: %d\n",
	     position0,position60);
    }
    
    cyg_semaphore_init( &sem_s02V2, 0 ); // Initialize the linear actuator semaphore

    /* Set up control pins */
    volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
    pPIO->PIO_PER = S02V2_CONTROL_MASK;          // PIO Pin Enable
    pPIO->PIO_OER = S02V2_CONTROL_MASK;          // PIO Output control Enable
    pPIO->PIO_PPUDR = S02V2_CONTROL_MASK;        // PIO Pull-Up Resistor Disable
    pPIO->PIO_MDDR = S02V2_CONTROL_MASK;         // PIO Multi-Drive Disable
    pPIO->PIO_SODR = ( S02V2_ENABLE | S02V2_BRAKE ); // Enable motorcontroller, brake motor, set direction
    pPIO->PIO_CODR = S02V2_DIRECTION;            // Set direction to expand

    /* Set up interrupt for analog feedback */
    cyg_interrupt_configure( CYGNUM_HAL_INTERRUPT_ADC, true, true );
    cyg_interrupt_create( CYGNUM_HAL_INTERRUPT_ADC, CYGNUM_HAL_PRI_3, 0, &adc_interrupt_isr, &adc_interrupt_dsr, &adc_interrupt_handle, &adc_interrupt );
    cyg_interrupt_attach ( adc_interrupt_handle );
    cyg_interrupt_unmask ( CYGNUM_HAL_INTERRUPT_ADC );

    /* Set up analog feedback pins */
    volatile AT91PS_ADC pADC = AT91C_BASE_ADC;
    AT91C_BASE_PMC->PMC_PCER = 1<<AT91C_ID_ADC;                // Enable ADC clock
    
    // Software trigger, 10 Bit resolution, Normal mode, No ADC clock prescaler, Shortest startup time, Shortest sample and hold timer
    pADC->ADC_MR = ( AT91C_ADC_TRGEN_DIS | 
                     AT91C_ADC_LOWRES_10_BIT | 
                     AT91C_ADC_SLEEP_NORMAL_MODE | 
                     (1<<8) | 
                     (AT91C_ADC_STARTUP & (0x0<<16)) | 
                     (3<<24)); 
    
    pADC->ADC_IER = ( AT91C_ADC_GOVRE );
    pADC->ADC_CHER = ( AT91C_ADC_CH0 | AT91C_ADC_CH1 | AT91C_ADC_CH2 ); // Enable channel 0,1,2 - Current,Voltage,Gradient
    pADC->ADC_CR = AT91C_ADC_START;                              // Start conversion
    
    /* Set up counter for counting time between tachometer interrupts */
    AT91C_BASE_PMC->PMC_PCER = 1<<AT91C_ID_TC2; // Enable Timer1 clock
    volatile AT91PS_TC pTachoTimer = AT91C_BASE_TC2;
    pTachoTimer->TC_CCR = ( AT91C_TC_CLKEN | AT91C_TC_SWTRG ); // Enable and reset timer 1
    pTachoTimer->TC_CMR = ( AT91C_TC_CLKS_TIMER_DIV1_CLOCK ); // Set Timer Clock to MCK/2
    pTachoTimer->TC_RC = 0xFFFF; // Count to maximum value;
    pTachoTimer->TC_IER = 1<<0;  // Enable overflow interrupt
    pTachoTimer->TC_IDR = 0xFE;  // Disable all other interrupts

    /* Set up tachometer interrupt */
    cyg_interrupt_configure( CYGNUM_HAL_INTERRUPT_IRQ0, false, true ); // IRQ0 interrupt edge-triggered on rising edge
    cyg_interrupt_create( CYGNUM_HAL_INTERRUPT_IRQ0, CYGNUM_HAL_PRI_6, 0, &tacho_interrupt_isr, NULL, &tacho_interrupt_handle, &tacho_interrupt); // Create interrupt on IRQ0 with priority 6
    cyg_interrupt_attach( tacho_interrupt_handle );    // Attach interrupt to IRQ0
    cyg_interrupt_configure( CYGNUM_HAL_INTERRUPT_TC2, true, true ); // Timer1 interrupt level trigered on high
    cyg_interrupt_create( CYGNUM_HAL_INTERRUPT_TC2, CYGNUM_HAL_PRI_5, 0, &tacho_interrupt_isr, NULL, &tacho_timer_interrupt_handle, &tacho_timer_interrupt); // Create interrupt on IRQ0 with priority 5
    cyg_interrupt_attach( tacho_timer_interrupt_handle );

    /* Set up PWM Channel 0 */
    volatile AT91PS_PWMC_CH pVoltage = AT91C_BASE_PWMC_CH0;
    AT91C_BASE_PMC->PMC_PCER = 1<<AT91C_ID_PWMC; // Enable PWM clock

    pVoltage->PWMC_CMR = ( (AT91C_PWMC_CPD & (0<<10)) | (AT91C_PWMC_CPOL & (1<<9)) | (AT91C_PWMC_CALG & (0<<8)) | AT91C_PWMC_CPRE_MCK ); // Update dutycycle at next period event, output waveform starts at high level, period is left aligned, connect to MCK
    pVoltage->PWMC_CPRDR = 0x0300;//18432000/S02V2_PWM_HZ; // Set period

    pVoltage->PWMC_CDTYR = 0x002; // Set dutycycle to lowest;
    
    /* Set up PWM pin */
    pPIO->PIO_PDR = S02V2_PWM; // PIO Pin Disable 
    pPIO->PIO_ODR = S02V2_PWM; // PIO Output control Disable
    pPIO->PIO_PPUDR = S02V2_PWM; // PIO Pull-Up Resistor Disable
    pPIO->PIO_MDDR = S02V2_PWM; // PIO Mult-Drive Disable
    pPIO->PIO_BSR = S02V2_PWM; // Connect Pin to Periphiral B / PWM Channel 0

    /* Set up PWM Controller */
    volatile AT91PS_PWMC pPWM = AT91C_BASE_PWMC;
    pPWM->PWMC_MR = ( AT91C_PWMC_DIVA & ((0<<0) | AT91C_PWMC_DIVB) & ((0<<16) | AT91C_PWMC_PREA_MCK | AT91C_PWMC_PREB_MCK )); // Turn off CLKA and CLKB, Set prescalers to MCK
    pPWM->PWMC_ENA = AT91C_PWMC_CHID0; // Enable Channel 0
    
    /* Initialize moving average filters */
    currentfilter = create_movingaverage(100);
    positionfilter = create_movingaverage(100);

    /* Initialize pid controllers */
    currentpid = create_pidcontrol(0.5,0.125,0.0,768.0/0.125,-768.0/0.125);
    speedpid = create_pidcontrol(35.0,1.0,0.0,1000.0,-1000.0);
    positionpid = create_pidcontrol(2.0,0.0,0.0,0.0,0.0);

    /* TO BE REMOVED */
    cyg_interrupt_unmask ( CYGNUM_HAL_INTERRUPT_IRQ0 ); // Unmask tachometer interrupt
    cyg_interrupt_unmask ( CYGNUM_HAL_INTERRUPT_TC2 );
    /* END TO BE REMOVED */

    status = 0;                    // Initialization succeeded
    initialized = true;               // Initialization completed
    cyg_semaphore_post( &sem_s02V2 ); // Increment semaphore count
  }
  return status;
}

int s02V2_reset(void) { // Reset linear actuator - Return status
  int status = 0;
  if(initialized) {
    cyg_semaphore_wait( &sem_s02V2 ); // Wait for semaphore to be released
  
    /* Disable clocks */
    AT91C_BASE_PMC->PMC_PCDR = 1<<AT91C_ID_ADC; // Disable ADC clock
    AT91C_BASE_PMC->PMC_PCDR = 1<<AT91C_ID_PWMC; // Disable PWM clock

    initialized = false;
    status = true;
    cyg_semaphore_destroy( &sem_s02V2 ); // Increment semaphore count
  }
  return status;
}
void *s02V2_positionControl(void *ptr);

typedef struct {
  int endpos;
  lthread_t theThread;
} pos_control_threading_t;

pos_control_threading_t *thread_data = NULL;


static void writeCalibration(void) 
{
  cyg_uint32 old_settings = *AT91C_MC_FMR;

  unsigned c=0;
  *AT91C_MC_FMR = (AT91C_MC_FMCN & (28 << 16)) | 
    (0 << 8) | AT91C_MC_FRDY | 
    AT91C_MC_LOCKE |AT91C_MC_PROGE;
  
  while((*AT91C_MC_FSR & AT91C_MC_FRDY) == 0) {
    ++c;
    cyg_thread_yield();
  }
  
  // tell to write page 1000 and start writing right away.
  *AT91C_MC_FCR = 0x1 | (PAGE << 8) | (AT91C_MC_KEY & (0x5a << 24));
  while((*AT91C_MC_FSR & AT91C_MC_FRDY) == 0) {
    ++c;
    cyg_thread_yield();
  }
  
  if(*AT91C_MC_FSR & AT91C_MC_LOCKE)
    {
      printf("LOCKE\n");
      return;
    }

  if(*AT91C_MC_FSR & AT91C_MC_PROGE)
    {
      printf("PROGE\n");
      return;
    }
  *AT91C_MC_FMR = old_settings;
}

int s02V2_calibrate(void) { //Calibrate linear actuator - Return status
  int status = 0;
  struct calibration_data *calibration = (struct calibration_data *)(AT91C_IFLASH + AT91C_IFLASH_PAGE_SIZE*PAGE);
  if(initialized) {
    //cyg_semaphore_wait( &sem_s02V2 ); // Wait for semaphore to be released  
    /*if(hasThread == TRUE) {
      lthread_suspend(&theThread);
      }*/
    s02V2_setDirection(true);
    printf("Calibrating...\nMin Position=");
    fflush(stdout);
    s02V2_brake(false);
    cyg_thread_delay(100);
    //s02V2_controlReset();
    while(s02V2_getCurrent()<300) {
      s02V2_control(-1,5,0);
      //s02V2_printDebug();
      cyg_thread_delay(1);
    }
    position0 = get_movingaverage(positionfilter)+5;
    printf("%i\nMax position",position0);

    
    fflush(stdout);
    tacho_count = 0;
    s02V2_setDirection(false);
    //s02V2_controlReset();
    while((tacho_count < 6116) && (s02V2_getCurrent() < 400)) {
      s02V2_control(-1,5,0);
      cyg_thread_delay(1);
    }
    //s02V2_brake(true);
    position60 = get_movingaverage(positionfilter);
    printf("%i\nCalibration done.",position60);

    

    printf("Tacho_count=%i\n",tacho_count);
    if (tacho_count < 6116) {
      status = -1;
      printf("Overcurrent error\n");
    }
    s02V2_setDutyCycle(0);
    //cyg_semaphore_post( &sem_s02V2 ); // Increment semaphore count

    calibration->min = position0;
    calibration->max = position60;
    writeCalibration();
  }
  return status;
}

int s02V2_setPositionTime(double endpos,double time) { //Go to position in mm within i given time in mm/sec - Return status
  int status = 0;

  cyg_semaphore_wait( &sem_s02V2 ); // Wait for semaphore to be released



  cyg_semaphore_post( &sem_s02V2 ); // Increment semaphore count

  return status;
}



int s02V2_setPosition(signed int endpos) { //Go to position in mm - Return status
  //int status = 0, reached;
  //double position = 0;
  
  
  printf("Someone called me\n");
  //cyg_semaphore_wait( &sem_s02V2 ); // Wait for semaphore to be released
  
  //cyg_semaphore_post( &sem_s02V2 ); // Increment semaphore count
  if(thread_data == NULL) 
    {
      thread_data = lmalloc(sizeof(pos_control_threading_t));
      thread_data->endpos = endpos;
      
      printf("Starting thread\n");
      lthread_create(&thread_data->theThread,s02V2_positionControl,thread_data);
    }
  else
    { 
      thread_data->endpos = endpos;
      printf("Resuming thread for %d\n", thread_data->endpos);
      lthread_resume(&thread_data->theThread);
    }

  //s02V2_brake(false);
  //reached = cyg_current_time() + 25;
  //while(reached > cyg_current_time()) {
    //s02V2_printDebug();printf("\t%i",reached);
  //  position = s02V2_control(endpos,0,0);
    //printf("\t%f",position);
  //  if ((position > endpos+1) || (position < endpos-1))
  //   reached = cyg_current_time()+25;
  //  cyg_thread_delay(1);
  //}
  //s02V2_brake(true);
  
  return thread_data->endpos;
}

void *s02V2_positionControl(void *ptr) {
  pos_control_threading_t *data = (pos_control_threading_t *)ptr;
  int position = 10;
  s02V2_brake(false);
  printf("Called for %d\n", data->endpos);
  for(;;) 
    {
      //cyg_semaphore_wait( &sem_s02V2 ); // Wait for semaphore to be released
      
      //cyg_semaphore_post( &sem_s02V2 );
      printf("Reg %d\n",data->endpos);
      if ((position < (data->endpos - 1)) || (position > (data->endpos + 1))) 
	{
	  printf("Reg %d\n",data->endpos);
	  position = s02V2_control(data->endpos,0,0);
	  
	  cyg_thread_delay(1);
	} 
      else
	{
	  s02V2_brake(true);
	  printf("Stopped\n");
	  fflush(stdout);
	  lthread_suspend(&data->theThread);
	  printf("Called for %d\n", data->endpos);
	  //printf("Started\n");
	  s02V2_brake(false);
	}
    }
}

void s02V2_printDebug(void) { // Print Debug parameters on debug port
  printf("\n%u",(unsigned)cyg_current_time());
  printf("\t%i",s02V2_getCurrent());
  printf("\t%f",s02V2_getSpeed());
  printf("\t%f",s02V2_getPosition());
  printf("\t%u",s02V2_getDutyCycle());
}
