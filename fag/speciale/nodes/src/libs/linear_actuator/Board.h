//----------------------------------------------------------------------------------------------------
//         ATMEL Microcontroller Software Support  -  ROUSSET  -
//----------------------------------------------------------------------------------------------------
// The software is delivered "AS IS" without warranty or condition of any
// kind, either express, implied or statutory. This includes without
// limitation any warranty or condition with respect to merchantability or
// fitness for any particular purpose, or against the infringements of
// intellectual property rights of others.
//----------------------------------------------------------------------------------------------------
// File Name: 	Board.h
// Object: 		AT91SAM7S Evaluation Board Features Definition File.
//
// Creation:	JPP   16/June/2004
//----------------------------------------------------------------------------------------------------   
#ifndef Board_h
#define Board_h

#include "AT91SAM7S256.h"
#define __inline inline

//#define true	1
#define false	0

//----------------------------------------------- 
// SAM7Board Memories Definition   
//-----------------------------------------------  
// The AT91SAM7S2564 embeds a 64-Kbyte SRAM bank, and 256 K-Byte Flash

#define  INT_SRAM			0x00200000
#define  INT_SRAM_REMAP		0x00000000

#define  INT_FLASH			0x00000000
#define  INT_FLASH_REMAP	0x01000000

#define  FLASH_PAGE_NB		512
#define  FLASH_PAGE_SIZE	128


/*********************/
/* System Definition */
/*********************/
#define EXT_OC		18432000						// Exetrnal ocilator MAINCK
#define MCK			18432000
#define MCKKHz		(MCK/1000)     					//

/*                                 PIO          & PA    PB     PIN */
#define SYSLED			(1<<00) /* PA0	/		& PWM0  TIOA0  48  */
#define PWRCTRL			(1<<01) /* PA1	/		& PWM1  TIOB0  47  */

/******************/
/* Debug UART     */
/******************/
/*                                 PIO          & PA    PB     PIN */
#define DRXD			(1<<9)   /* PA9  /      & DRXD  NPCS1  30  */
#define DTXD			(1<<10)  /* PA10 /      & DTXD  NPCS2  29  */

#define DEBUG_MASK		(DTXD|DRXD)

#define DEBUG_BAUDRATE	115200

/********************/
/* USART Definition */
/********************/
/*                                  PIO         &  PA    PB    PIN */
#define RF0				 (1<<7)  /* PA7  /      &  RTS0  PWM3  32  */
#define RX0              (1<<5)  /* PA5  /      &  RXD0  NPCS3 35  */
#define TX0              (1<<6)  /* PA6  /      &  TXD0  PCK0  34  */

#define RF1				 (1<<24) /* PA24 /		&  RTS1  PWM1  23  */
#define RX1				 (1<<21) /* PA21 /      &  RXD1  PCK1  11  */
#define TX1				 (1<<22) /* PA22 /      &  TXD1  NPCS3 14  */

#define USART0_MASK		 (RF0|RX0|TX0)
#define USART1_MASK		 (RF1|RX1|TX1)

#define BAUDRATE		115200

/*****************************/
/* Testing Board Definitions */
/*****************************/
/*                                 PIO          &	PA		PB		PIN	*/
#define TB_LED0			(1<<11) /* PA11			&	NPCS0	PWM0	28	*/
#define TB_LED1			(1<<12)	/* PA12			&	MISO	PWM1	27	*/
#define TB_LED2			(1<<13) /* PA13			&	MOSI	PWM2	22	*/
#define TB_LED3			(1<<14) /* PA14			&	SPCK	PWM3	21	*/
#define TB_LED4			(1<<17) /* PA17 / AD0	&	TD		PCK1	9	*/
#define TB_LED5			(1<<18) /* PA18 / AD1	&	RD		PCK2	10	*/
#define TB_LED6			(1<<19)	/* PA19 / AD2	&	RK		FIQ		13	*/
#define TB_LED7			(1<<20) /* PA20 / AD3	&	RF		IRQ0	16	*/ 

#define TB_LED_MASK (LED0|LED1|LED2|LED3|LED4|LED5|LED6|LED7)



#endif   // Board_h     
