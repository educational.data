/*
 *  s02V2.h
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef s02V2_h
#define s02V2_h

#include "common.h"
#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */


/* DEFINES */
#define CYGNUM_HAL_PRI_0	0
#define CYGNUM_HAL_PRI_1	1
#define CYGNUM_HAL_PRI_2	2
#define CYGNUM_HAL_PRI_3	3
#define CYGNUM_HAL_PRI_4	4
#define CYGNUM_HAL_PRI_5	5
#define CYGNUM_HAL_PRI_6	6
#define CYGNUM_HAL_PRI_7	7

/*                                 PIO         &  PA     PB    PIN */
#define S02V2_ENABLE    (1<<12)	/* PA12	       &  MISO	 PWM1  27  */
#define S02V2_DIRECTION (1<<13) /* PA13	       &  MOSI	 PWM2  22  */
#define S02V2_PWM       (1<<11) /* PA11	       &  NPCS0  PWM0  28  */
#define S02V2_BRAKE     (1<<14) /* PA14	       &  SPCK	 PWM3  21  */
#define S02V2_TACHO     (1<<20) /* PA20 / AD3  &  RF	 IRQ0  16  */ 
#define S02V2_CURRENT   (1<<17) /* PA17 / AD0  &  TD	 PCK1  9   */
#define S02V2_VOLTAGE   (1<<18) /* PA18 / AD1  &  RD	 PCK2  10  */
#define S02V2_GRADIENT	(1<<19)	/* PA19 / AD2  &  RK	 FIQ   13  */
#define S02V2_CONTROL_MASK (S02V2_ENABLE|S02V2_DIRECTION|S02V2_BRAKE)
#define S02V2_FEEDBACK_MASK (S02V2_CURRENT|S02V2_VOLTAGE|S02V2_GRADIENT)

#define S02V2_PWM_HZ		24000

#define CURRENT_OFFSET          7
#define MAX_DUTYCYCLE           767

BEGIN_CSPEC 

/* INTERFACE */
int s02V2_initialize(void); //Initialize linear actuator - Return status
int s02V2_reset(void); //Reset linear actuator - Return status
int s02V2_calibrate(void); //Calibrate linear actuator - Return status
int s02V2_setPosition(signed int); //Go to position in mm - Return status
int s02V2_setPositionTime(double,double); //Go to position in mm within i given time in mm/sec - Return status
double s02V2_getPosition(void); // Returns position in mm
void s02V2_printDebug(void); // Print Debug parameters on debug port

END_CSPEC
#endif   // s02V2_h
