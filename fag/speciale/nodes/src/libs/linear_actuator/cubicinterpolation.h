/*
 *  movingaverage.h
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef cubicinterpolation_h
#define cubicinterpolation_h

#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */
#include <cyg/hal/hal_arch.h>

typedef struct {
  double a0,a1,a2,a3;
  double position,speed;
} cubicinterpolation_t;

cubicinterpolation_t *create_cubicinterpolation(int, int, double);
void release_cubicinterpolation(cubicinterpolation_t *);
void calculate_cubicinterpolation(cubicinterpolation_t *, double);

#endif   // cubicinterpolation_h
