 /*
 *  cubicinterpolation.c
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */

/* INCLUDES */
#include <stdio.h>
#include <malloc.h>
#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */
#include <cyg/hal/hal_arch.h>
#include "cubicinterpolation.h"

/* DEFINES */

/* STATICS */

/* GLOBAL VARIABLES */

cubicinterpolation_t *create_cubicinterpolation(int start, int end, double time) {

  cubicinterpolation_t *ci = malloc(sizeof(cubicinterpolation_t));

  ci->a0 = (double)start;
  ci->a1 = 0.0;
  ci->a2 = (double)(3*(double)(end - start))/(time*time);
  ci->a3 = (double)-(2*(double)(end - start))/(time*time*time);

  ci->position = 0.0;
  ci->speed = 0.0;

  //printf("a0=%f,a1=%f,a2=%f,a3=%f",ci->a0,ci->a1,ci->a2,ci->a3);
  //fflush(stdout);

  return ci;
}

void release_cubicinterpolation(cubicinterpolation_t *ci) {
  free(ci);
}

void calculate_cubicinterpolation(cubicinterpolation_t *ci, double time) {
  double time2 = time*time;
  double time3 = time*time*time;

  ci->position = ci->a0 + (ci->a1 * time) + (ci->a2 * time2) + (ci->a3 * time3);
  ci->speed = ci->a1 + 2 * ci->a2 * time + 3 * ci->a3 * time2;

  //printf("\n%f\t%f\t%f",time,ci->position,ci->speed);
  //fflush(stdout);
}



