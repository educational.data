/*
 *  movingaverage.h
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef movingaverage_h
#define movingaverage_h

#include <cyg/kernel/kapi.h>            /* All the kernel specific stuff */
#include <cyg/hal/hal_arch.h>

typedef struct {
  char ptr;
  char size;
  signed int sum;
  cyg_uint16 *data;
} movingaverage_t;

movingaverage_t *create_movingaverage(char);
void release_movingaverage(movingaverage_t *);
void insert_movingaverage(movingaverage_t *, cyg_uint16); 
int get_movingaverage(movingaverage_t *);

#endif   // movingaverage_h
