/*
 *  g01V2.h
 *  
 *
 *  Created by Andreas Lyder on 31/03/08.
 *  Copyright 2008 __MyCompanyName__. All rights reserved.
 *
 */
#ifndef g01V2_h
#define g01V2_h

void sysled_set(char);
void sysled_toggle(void);
void specificpwr_set(char);

#endif   // g01V2_h
