#if ! defined (COMMON_H_INCLUDED)
# define COMMON_H_INCLUDED

/**
 * @file common.h
 * @brief Globally defined informations
 *
 * This file will include specific macro definitions for use by the
 * individual parts of the program 
 */

# if defined(ON_BUILD_PLATFORM)
#  include <inttypes.h>
# else
typedef unsigned int   uint32_t;
typedef unsigned short uint16_t;
typedef unsigned char  uint8_t;
typedef int            int32_t;
typedef short          int16_t;
typedef signed char    int8_t;

typedef uint32_t ulong;
typedef uint8_t uchar;
typedef uint8_t byte;
# endif

# if defined(ON_BUILD_PLATFORM)
#  define BUILD_ON_PC(a) a
# else
#  define BUILD_ON_PC(a) 
# endif

# if defined(ON_MODULE)
#  define BUILD_ON_MODULE(a) a
# else
#  define BUILD_ON_MODULE(a) 
# endif



# ifdef __cplusplus
#  define CSPEC extern "C"
#  define BEGIN_CSPEC extern "C" {
#  define END_CSPEC }
# else
#  define CSPEC
#  define BEGIN_CSPEC
#  define END_CSPEC
# endif

# if ! defined(NULL)
#  define NULL 0L
# endif


//typedef unsigned int uint;
typedef int BOOL;
typedef BOOL bool_t;
# define TRUE ((bool_t)1)
# define FALSE ((bool_t)0)

typedef void * object_t;

#define EOK 0

/** 
 * @mainpage ODIN communication simulation
 *
 * This project is mainly supposed to aid in testing general
 * communcations setup for the individual <b>odin</b> modules in the
 * http://odin.mmmi.sdu.dk project.
 *
 * Two of the main things that are being tested in this little project
 * is the ability to set up global communications in a network with a
 * high connection probability; and set up a hybrid communication
 * sort.
 *
 * The project is sought implemented in an object oriented fashion and
 * this means that there is certain supporting functionalities in the
 * project. The object oriented methologies is contained within an
 * <tt>LOBJECT</tt> that is inspired by the Linux kernels
 * <tt>KOBJECT</tt>. If this is used correctly, one should not have to
 * worry about using memory management.
 *
 * @see lobject.h
 *
 * I have done a great deal of work in order to support
 * abstraction. This means that certain functions are implemented in
 * wrapper functions that then is used extensively throughout the
 * project.
 *
 * @see array.h
 * @see queue.h
 * @see lmemory.h
 *
 * @author Tobias Nielsen
 */

#endif
