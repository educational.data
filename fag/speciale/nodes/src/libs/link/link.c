#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#if defined(ON_BUILD_PLATFORM)
# include <assert.h>
# include <stdio.h>
#else
# error THIS FILE IS DISALLOWED ON THE MODULE!!!
#endif

#include "common.h"
#include "link.h"
#include "hal/uart.h"
#include "utils/lrandom.h"
#include "utils/lobject.h"
#include "utils/lmemory.h"
#define LOG_DUMP
#include "log.hpp"


/* static BOOL */
/* eatPackage (void) */
/* { */
/*   return (lrandom () % 100) <= 5; */
/* } */

void
destroy_link (link_t *link)
{
  assert(link != NULL);
  larray_destroy (&link->uarts);
}

link_t *create_link ()
{
  link_t *link = lobject_create(link_t);
  lobject_set_destructor(link, (lobject_destructor)&destroy_link);
  larray_create (&link->uarts);
  link->is_recieving = FALSE;
  return link;
}

void uart_receive_byte(uart_t *uart, uint8_t data);

void link_write_byte(link_t *link, struct uart *sender, int8_t data) {
  int i = 0;
  int sending_legs = 0;
  int cLeg;

  assert(link != NULL);
  down(link);

  if(link->is_recieving == TRUE) 
    {
      up(link);
      return;
    }
  
  link->is_recieving = TRUE;
  
  cLeg = larray_size (&link->uarts);
  
  assert(cLeg != 0);
  
  for (i = 0; i < cLeg; i++)
    {
      uart_t *uart =
	(uart_t *) larray_get (&link->uarts, i);
      
      if(uart->is_transmitting_package == TRUE)
        sending_legs++;
    }

  if(sending_legs>1)
    {
      printf("*");
      fflush(stdout);
    }

  for (i = 0; i < cLeg; i++)
    {
      uart_t *uart =
	(uart_t *) larray_get (&link->uarts, i);
      if(uart != sender) 
        uart_receive_byte(uart, data);
    }
  link->is_recieving = FALSE;
  up(link);
}

void
link_add_uart (link_t *link,
               uart_t *uart)
{
  assert(link != NULL);
  assert(uart != NULL);
  down(link);
  down(uart);
  larray_insert (&link->uarts, uart);
  uart->link = link;
  up(uart);
  up(link);
}

void
link_remove_uart (link_t *link,
                  uart_t *uart)
{
  assert(link != NULL);
  assert(uart != NULL);
  down(link);
  down(uart);
  uart->link = NULL;
  larray_remove (&link->uarts, uart);
  up(uart);
  up(link);
}
