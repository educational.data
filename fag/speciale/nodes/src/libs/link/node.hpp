#ifndef __NODE_HPP__
#define __NODE_HPP__

#if ! defined(COMMON_H_INCLUDED)
# error Needs to have common.h included
#endif

#include <string>
#include "utils/lobject.h"
// forward declarations
struct message;
class Leg;
struct link;

/**
 * @brief Centralized connector node.
 *
 * A node is the part that all the different legs is attached to in
 * order to perform communication.
 */
class Node {
private:
  struct link *impl;

#if defined(ON_BUILD_PLATFORM)
  /// Arbitrary name for this node
  std::string name;
#endif

public:
  /// constructor type that accepts a special node name
  explicit Node(const std::string &name);

  /**
   * @brief The same as the arbitrary node but can also handle a number.
   *
   * The number is automaticly concatinated as a string to the name.
   */
  Node(const std::string &name, const int number);

  /// destructor
  ~Node();

  /**
   * @brief Insert a leg in one of the sockets of the node.
   */
  Node &addLeg(Leg &leg, int end);

  /**
   * @brief Insert a leg in one of the sockets of the node.
   */
  Node &addLeg(Leg *leg, int end);

  /**
   * @brief Send data to all the nodes in this net.
   *
   * The former version of this actually used to prevent sending
   * messages to themselves. But the new version of the algorithm
   * cannot rely on such a luxery and therefore the end coordinator
   * must decide when a message was sent to itself.
   */
  void sendData ( void *data, const int size );


  /**
   * @brief Return the name of this leg.
   *
   * This is only an convinience function.
   */
  const std::string &getName();
};

#endif
