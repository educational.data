#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "common.h"

#if defined(ON_MODULE)
# error This file is not supposed to be compiled on the module
#endif

#include "node.hpp"
#include "link.h"
//#include "leg/endcoordinator.hpp"
#include "leg/cleg.hpp"
#include "leg/endcoord.h"

#include "log.hpp"
#include "leg/leg.h"
#include "utils/lmemory.h"

#include <sstream>
#include <iostream>

using namespace std;


Node & Node::addLeg (Leg & leg, int end)
{
  return addLeg(&leg,end);
}

Node & Node::addLeg (Leg * leg, int end)
{
  /*  if(leg->hasNode(end)) {
    LOG_ERROR("warning: leg " << leg->getName() 
              << " is attached to node " 
              << leg->getEndCoordinator(end).getNode().getName() 
              << " and is now being reattached to node " 
              << name << " on interface " << end);

  }
  addEndcoordinator(leg->getEndCoordinator(end));
  */
  assert(leg != NULL);
  assert(impl != NULL);
  assert(leg->leg != NULL);
  assert(leg->leg->uart[end] != NULL);
  link_add_uart(impl,leg->leg->uart[end]);
  return *this;
}

/* Node &
Node::addEndcoordinator(lobject_ptr<struct endcoord_t> endcoord) {
  link_add_uart(impl,endcoord->uart);
  } */

/*
Node & Node::addEndcoordinator (EndCoordinator &end)
{
  link_add_uart(impl,end.impl->uart);
  return *this;
}
*/
 /* Node & 
Node::remEndcoordinator(lobject_ptr<struct endcoord_t> endcoord)
{
  link_remove_uart(impl,endcoord->uart);
  return *this;
  } */

Node::Node (const std::string & name)
{
  impl = create_link();
  this->name = name;
}

Node::Node (const std::string & name, const int number) {
  stringstream sstrm;
  sstrm << name << number;
  this->name = sstrm.str();
  impl = create_link();
}

const std::string &
Node::getName() {
  return name;
}

Node::~Node() {
  up(impl);
}
