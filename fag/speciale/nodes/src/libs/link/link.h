#ifndef __LINK_H__
#define __LINK_H__

#if defined(ON_MODULE)
# error This file is not supposed to be compiled on the module
#endif

#if ! defined(COMMON_H_INCLUDED)
# error Needs to have common.h included
#endif

#include "utils/array.h"
#include "utils/lobject.h"

/** @file link.h
 * @brief representing a link in simulation scenario
 *
 * In this case where we are running simulated, we 
 * must have something to represent a single node.
 * This file will handle that.
 */


BEGIN_CSPEC

struct uart;
/**
 * @brief The implementation of the core of the link code.
 *
 * @see lobject.h
 */
typedef struct link {
  LOBJECT;

  /// There exists two uarts in this module and they are stored
  /// directly in this array.
  larray_t uarts;

  bool_t is_recieving;
} link_t;

/**
 * @brief create a link module.
 *
 * @todo make a "full" implementation that includes all the link
 * modules.
 */
link_t * create_link(void);
void link_write_byte(link_t *link, struct uart *sender, int8_t data);
void link_add_uart(link_t *link, struct uart *uart);
void link_remove_uart(link_t *link, struct uart *uart);

END_CSPEC

#endif
