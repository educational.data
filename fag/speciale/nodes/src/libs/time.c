#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "common.h"

#if defined(ON_BUILD_PLATFORM)
# include <sys/times.h>
//# include <string>
//# include <sstream>
//# include <iostream>
#endif

#if defined(ON_MODULE)
#include <cyg/kernel/kapi.h>
#endif

#include "nodetime.h"

static unsigned timInit = 0;
static BOOL isInit = FALSE;

//using namespace std;

nodetime_t 
nodetime_time(void) 
{
#if defined(ON_MODULE)
  return cyg_current_time();
#endif

#if defined(ON_BUILD_PLATFORM)
  if(isInit == TRUE) {
    struct tms buf;
    clock_t tBeg = times(&buf)/3;

    return tBeg-timInit;
  } else {
    return timInit;
  }
#endif
}

void 
nodetime_initTime(void) {
#if defined(ON_BUILD_PLATFORM)
  struct tms buf;
  timInit = times(&buf)/3;
  //timInit = clock();
#endif
#if defined(ON_MODULE)
  timInit = cyg_current_time();
#endif
  isInit = TRUE;
    
}

void 
nodetime_initTime_static(void) {
  isInit = FALSE;
  timInit = 0;
}

void 
nodetime_tick(void) {
#if defined(ON_BUILD_PLATFORM)
  timInit++;
#endif
}

nodetime_t 
nodetime_timeplus(const nodetime_t alpha) 
{
  return nodetime_time() + alpha;
}
  
BOOL 
nodetime_timeElapsedA(const nodetime_t deadTime) 
{
  nodetime_t t = nodetime_time();
  if(t < deadTime)
    return FALSE;
  
  if(t == deadTime)
    return TRUE;
  if(t+2 > deadTime)
    return TRUE;
  
  return FALSE;
}


BOOL 
nodetime_timeElapsedB(const nodetime_t oldTime, const nodetime_t allowedTime) 
{
  int opa = oldTime + allowedTime;
  int ct = nodetime_time();
  
  if (opa <= ct)
    return TRUE;
  else
    return FALSE;
}
