#ifndef __LOG_HPP__
# define __LOG_HPP__

# include "common.h"

# if defined (__cplusplus)

#  if defined(ON_BUILD_PLATFORM)
#   include <ostream>
#   include <iomanip>
#  endif

namespace log 
{
  void init();

  const int file = 1;
  const int stdout = 2;
  const int stderr = 4;
  const int timestamp = 8;
  const int error = 16;

#  if defined(ON_BUILD_PLATFORM)
  std::ostream &elog(const char *filename);
  std::ostream &olog(const char *filename);  
  std::ostream &hlog(const char *filename);  
  std::ostream &flog(const char *filename);  
  std::ostream &log(const char *filename, const int type = file | timestamp);
#  endif
};

#  if defined(ON_BUILD_PLATFORM)
#   define LOG_FILE(M) log::log(__FILE__,log::file | log::timestamp) << M << std::endl
#   define LOG_OUT(M) log::log(__FILE__,log::stdout | log::timestamp) << M << std::endl
#   define LOG_ERROR(M) log::elog(__FILE__) << M << std::endl
#  else
#   define LOG_FILE(M) 
#   define LOG_OUT(M) 
#   define LOG_ERROR(M)
#  endif
# endif
BEGIN_CSPEC

#if defined (ON_BUILD_PLATFORM)
# if defined (LOG_DUMP)
#  define log_file(...) _log_file(__VA_ARGS__)
#  define log_error(...) _log_error(__VA_ARGS__)
#  define log_out(...) _log_out(__VA_ARGS__)
# else
#  define log_file(...) _log_nolog(__VA_ARGS__)
#  define log_error(...) _log_nolog(__VA_ARGS__)
#  define log_out(...) _log_nolog(__VA_ARGS__)
# endif
void _log_file(char * format, ...);
void _log_error(char * format, ...);
void _log_out(char * format, ...);
void _log_nolog(char * format, ...);
#else
# define log_file(...) do { } while(0)
# define log_error(...) do { } while(0)
# define log_out(...) do { } while(0)
#endif

END_CSPEC
#endif
