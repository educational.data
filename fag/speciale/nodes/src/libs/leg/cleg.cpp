#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#if defined(ON_BUILD_PLATFORM)
# include <iostream>
# include <string.h>
# include <exception>
# include <stdexcept>
# include <sstream>
# include <fstream>

using namespace std;
#endif

#include <assert.h>

#include "common.h"
#include "cleg.hpp"
#include "leg.h"
#include "leg/endcoord.h"
#include "leg/global_handler.h"

#include "utils/lthread.h"


#if defined(ON_BUILD_PLATFORM)
# include "link/node.hpp"
# include "nodetime.h"
#endif

#include "log.hpp"
#include "uart.hpp"

/**
 * @file leg.cpp
 * @brief This is the actual implementation file for the leg.
 *
 * Internally the functionality of the leg has been split out 
 * in to another file: msgstate{.h,.cpp}
 *
 * @version $Revision: 1.5 $
 * @author Tobias Nielsen email:tobibobi@gmail.com
 * @see msgstate.cpp
 * @see msgstate.h
 */



#if defined (ON_BUILD_PLATFORM)
Leg::Leg(const std::string &title) {
  leg = create_leg_with_name(const_cast<char *>(title.c_str()));
  initLeg();

  
}

Leg::Leg(const std::string &title, int number) {
  leg = create_leg_with_name_and_number(const_cast<char *>(title.c_str()),number);
  initLeg();
}
#endif

void
Leg::initLeg() {
  leg_add_link_protocol(leg,create_endcoord(leg,0));
  leg_add_link_protocol(leg,create_endcoord(leg,1));

  leg_add_network_protocol(leg,create_global_handler(leg));
}

Leg::Leg()
{
  leg = create_leg();
  initLeg();
}

Leg::~Leg ()
{
  up(leg);
}
