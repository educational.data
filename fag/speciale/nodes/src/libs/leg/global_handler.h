#ifndef __GLOBAL_HANDLER_H__INCLUDED__
#define __GLOBAL_HANDLER_H__INCLUDED__

#include "common.h"
#include "utils/lobject.h"
#include "nodetime.h"
#include "leg/leg.h"
#include "utils/lsocket.h"
#include "leg/protocol_abstraction.h"

//struct msg_handler_t;

BEGIN_CSPEC 
typedef struct global_handler global_handler_t;

struct global_handler {
  leg_t *leg;
  lsocket_provider_t *provider;

  volatile bool_t isChecked;
  volatile int current_reply_interface;
  volatile int current_global_interface;
  volatile int num_non_globals;
  volatile bool_t is_global;
  volatile nodetime_t idle_count_down;
  volatile bool_t has_executed_idle;
  
  volatile bool_t has_said_i_was_not_global;
  volatile int current_state;

  volatile int competeValue;
  volatile int biggestForeignCompeteValue;

  volatile int thread_count;
  volatile bool_t is_threaded;

  lthread_t thread_idle;
  lthread_t thread_reciever_1;
  lthread_t thread_reciever_2;
  lmutex_t mutex;

  lsocket_t *socket[2];
  message_t current_msg;

};

protocol_handler_t *create_global_handler(leg_t *m);
END_CSPEC
#endif // __GLOBAL_HANDLER_H__INCLUDED__
