#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "utils/lsocket.h"
#include "leg/protocol_abstraction.h"
#include "leg/leg.h"

#include "utils/lthread.h"
#include "utils/lmemory.h"

#include "hal/uart.h"

#include "nodetime.h"
#include "endcoord.h"
#include "global_handler.h"

#define LOG_DUMP
#include "log.hpp"

/**
 * @file leg.cpp
 * @brief This is the actual implementation file for the leg.
 *
 * Internally the functionality of the leg has been split out 
 * in to another file: msgstate{.h,.cpp}
 *
 * @version $Revision: 1.8 $
 * @author Tobias Nielsen email:tobibobi@gmail.com
 * @see msgstate.cpp
 * @see msgstate.h
 */


//static void __handleIdleState (struct leg_t *);
static leg_t *__create_leg (void);
//static void __parsedata (struct leg_t *);

/* void */
/* leg_buildHybridNet (leg_t *leg, int net) */
/* { */
/*   message_t msg; */
/*   msg.type = BUILD_POTENTIAL; */
/*   msg.potentialData.net_id = net; */
/*   msg.potentialData.potential = 5; */
  
/* } */

static void
__destroy_leg (void *obj)
{
  //int i;
  leg_t *leg = (leg_t *) obj;
#if defined(ON_BUILD_PLATFORM)
  uart_release_connection (leg->uart[0]);
  uart_release_connection (leg->uart[1]);
#endif
  //assert(leg->is_running == TRUE);
  leg->tear_down = TRUE;

  lthread_join (&(leg->thread));

  while (leg->is_running == TRUE)
    lthread_yield ();

  assert (leg->is_running == FALSE);

  lthread_mutex_destroy (&(leg->mutex1));
  lthread_mutex_destroy (&(leg->mutex2));
  lthread_destroy (&(leg->thread));

  
  /*
  for (i = 0; i < larray_size (&(leg->handlers)); ++i)
    {
      msg_handler_t *handler = larray_get (&(leg->handlers), i);
      up (handler->object);
    }
  larray_destroy (&(leg->handlers));
  
  up (leg->end_coordinator[0]);
  up (leg->end_coordinator[1]);
  */
  up (leg->uart[0]);
  up (leg->uart[1]);
}


static void
__execute_timestep (leg_t *leg)
{

  /*
    endcoord_t *end0 = leg->end_coordinator[0];
  endcoord_t *end1 = leg->end_coordinator[1];

  endcoord_idle_handler (end0);
  endcoord_idle_handler (end1);

  if (endcoord_hasMessage (end0))
    {
      log_file ("%s: got a message", leg->name);
      down (leg);
      leg->buffer = endcoord_getMessage (end0);

      leg->forward_id = 1;
      leg->backwards_id = 0;

      __parsedata (leg);
      lfree (leg->buffer);
      up (leg);
    }

  if (endcoord_hasMessage (end1))
    {
      log_file ("%s: got a message", leg->name);
      down (leg);
      leg->buffer = endcoord_getMessage (end1);

      leg->forward_id = 0;
      leg->backwards_id = 1;

      __parsedata (leg);
      lfree (leg->buffer);
      up (leg);
    }

  __handleIdleState (leg);
  */
}


static void *
leg_run (void *ptr)
{
  leg_t *leg = (leg_t *) ptr;

  leg->is_running = TRUE;

  do
    {
      __execute_timestep (leg);

      lthread_yield ();
    }
  while (leg->tear_down != TRUE);

  leg->is_running = FALSE;

  return NULL;
}

#if defined (ON_BUILD_PLATFORM)

link_t *
leg_getLink (leg_t *leg, int id)
{
  assert (id >= 0 && id <= 1);
  return leg->uart[id]->link;
}

BOOL
leg_hasNode (leg_t * leg, int id)
{
  assert (id >= 0 && id <= 1);
  return leg->uart[id]->link != NULL;
}

leg_t *
create_leg_with_name (char *name)
{
  leg_t *leg = __create_leg ();
  sprintf (leg->name, "%s", name);
  lthread_create (&leg->thread, &leg_run, leg);
  return leg;
}

leg_t *
create_leg_with_name_and_number (const char *name, int number)
{
  leg_t *leg = __create_leg ();
  sprintf (leg->name, "%s%d", name, number);
  lthread_create (&leg->thread, &leg_run, leg);
  return leg;
}

#endif

leg_t *
create_leg ()
{
  leg_t *leg = __create_leg ();
  lthread_create (&leg->thread, &leg_run, leg);
  return leg;
}

static leg_t *
__create_leg ()
{
  leg_t *leg = lobject_create (struct leg);
  lobject_set_destructor (leg, &__destroy_leg);

  leg->uart[0] = create_uart (0);
  leg->uart[1] = create_uart (1);

  leg->idle_count_down = 0;

  leg->is_running = FALSE;

  leg->is_recieving_1 = FALSE;
  leg->is_recieving_2 = FALSE;
  leg->tear_down = FALSE;
  leg->is_forwarding = FALSE;
  leg->has_data_1 = FALSE;
  leg->has_data_2 = FALSE;

  lthread_mutex_init (&(leg->mutex1));
  lthread_mutex_init (&(leg->mutex2));

  larray_create (&(leg->protocols));
  larray_create (&(leg->sockets));
  return leg;
}



void leg_add_network_protocol(leg_t *leg, 
                              protocol_handler_t *handler) 
{
  assert(leg != NULL);
  assert(handler != NULL);
  //assert(handler->provider != NULL);

  handler->leg = leg;

  if(handler->start != NULL)
    handler->start(handler);

  larray_insert(& leg->protocols, handler);
}
void leg_add_link_protocol(leg_t *leg, 
                           protocol_handler_t *handler) 
{
  assert(leg != NULL);
  down(leg);

  assert(handler != NULL);
  down (handler);

  assert(handler->provider != NULL);
  assert(handler->provider->functions != NULL);

  handler->leg = leg;

  if(handler->start != NULL)
    handler->start(handler);

  larray_insert(&leg->protocols, handler);

  up(leg);
}

lsocket_t *leg_seek_socket(leg_t *leg, uint8_t protocol_id) 
{
  int i;
  int tProt = protocol_id;
  assert(leg != NULL);
  down(leg);

  // grap uart requests first!
  if(protocol_id < 2 ) 
    {
      lsocket_provider_t *provider  = leg->uart[protocol_id]->socket_provider;
      lsocket_provider_funcs_t *functions = provider->functions;

      assert(functions != NULL);
    
      if(functions->provides_protocol(provider, provider->ptr, protocol_id))
        {
          lsocket_t *socket = lsocket_create(0, protocol_id);
        
          if(functions->create_socket == NULL || functions->create_socket(provider, protocol_id, provider->ptr, NULL)) 
            {
              lsocket_provider_connect_socket(provider, socket);
              up(leg);
              return socket;
            } 
          else 
            {
              up(socket);
            }
        }      
    }

  // then ordinary protocols
  for(i=0;i< larray_size(&leg->protocols);++i) 
    {
      protocol_handler_t *handler = (protocol_handler_t *)
        larray_get(&leg->protocols,i);

      lsocket_provider_t *provider  = handler->provider;
      if(provider != NULL)
        {
          lsocket_provider_funcs_t *functions = provider->functions;
          
          assert(functions != NULL);
          
          if(functions->provides_protocol(provider, provider->ptr, protocol_id))
            {
              if(protocol_id >= 20 && protocol_id < 30)
                {
                  tProt = protocol_id-10;
                }
              
              lsocket_t *socket = lsocket_create(0, tProt);
              if(functions->create_socket == NULL || functions->create_socket(provider, tProt, provider->ptr, NULL)) 
                {
                  lsocket_provider_connect_socket(provider, socket);
                  up(leg);
                  return socket;
                } 
              else 
                {
                  up(socket);
                }
            }      
        }
    }
  up(leg);
  return NULL;  
}
