/*
 * $Id: global_handler.c,v 1.5 2008-02-23 16:32:43 tobibobi Exp $
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


#include "common.h"
#define LOG_DUMP

#include "global_handler.h"
//#include "leg.hpp"
#include <stdlib.h>
#include "message.h"
#include "utils/lrandom.h"
#include "utils/lobject.h"
#include "utils/lmemory.h"
#include "log.hpp"
#include "leg.h"


int
max (const int a, const int b)
{
  return a > b ? a : b;
}

#include "nodetime.h"


#define IDLE_COUNT_DOWN 20

#define STATE_NOTHING  0
#define STATE_IS_EVOLVING_TREE 1
#define STATE_IS_COMPETING_VALUES 2
#define STATE_IS_WAITING_FOR_POTENTIALS 3

static void 
__send_interface (global_handler_t *handler,
                  int id, message_t *msg)
{

}

static void 
__send_forward (global_handler_t *handler,
                message_t *msg)
{

}

static void 
__send_backward (global_handler_t *handler,
                 message_t *msg)
{

}

static void
handleMsgGrowTreeRequest (global_handler_t *handler,
			  message_t *msg)
{
  message_t newmsg;
  down (handler);
  //
  // i am being asked if i can grow my tree..
  // ignore if i am already forwarding..
  //
  if (handler->leg->is_forwarding
      || handler->isChecked || handler->current_state != STATE_NOTHING)
    return;

  log_file ("MsgGT(%s): Requested by %s to ask if neighbours is connected.",
	    handler->leg->name,msg->sender);

  //
  // send forward request on all the neighbours to see if they 
  // are globally connected.
  //
  handler->is_global = TRUE;
  handler->current_global_interface = handler->leg->backwards_id;
  handler->current_reply_interface = handler->leg->forward_id;

  newmsg.type = ASK_HAS_GLOBAL_REQUEST;
  handler->isChecked = TRUE;
  handler->current_state = STATE_IS_EVOLVING_TREE;
  handler->idle_count_down = nodetime_timeplus (IDLE_COUNT_DOWN);
  handler->has_executed_idle = FALSE;
  handler->num_non_globals = 0;

  __send_forward (handler, &newmsg);
  up (handler);
}

static void
handleMsgAskHasGlobalReply (global_handler_t *handler,
			    message_t *msg)
{
  message_t newmsg;
  down (handler);
  //
  // We try to catch the situations where we are already being
  // processing a tree.
  //
  if (handler->current_state == STATE_IS_EVOLVING_TREE
      && handler->current_global_interface != handler->leg->backwards_id)
    {
      log_file ("MsgAHGR(%s): Was asking the the others - interrupted by %s",
		handler->leg->name, msg->sender);

      //
      // initiate the competition
      //
      handler->current_state = STATE_IS_COMPETING_VALUES;
      handler->idle_count_down = nodetime_timeplus (IDLE_COUNT_DOWN);
      handler->has_executed_idle = FALSE;
      handler->competeValue = lrandom () % 63 + 1;

      //
      // construct a message interrupt
      // and include the current competition value at the same time
      //
      newmsg.type = INTERRUPT_TREE_GROWTH;
      newmsg.competeData.competeValue = handler->competeValue;
      newmsg.competeData.numOfUnConnectedNeighbours =
	handler->num_non_globals;

      //
      // send the message back on the same interface that
      // original message appered on.
      //
      __send_backward (handler, &newmsg);
    }
  else
    {
      //
      // Test to see if im not global, and i actually can say that i was
      // not global.
      //
      if (!handler->is_global)
	{
	  log_file
	    ("MsgAHGR(%s): Is not globally connected and replies it to %s",
	     handler->leg->name, msg->sender);
	  //
	  // I can assume that now i will be globally connected to the
	  // rest of the global network, and therefore i allready now
	  // can set my global state to true.
	  //
	  handler->is_global = TRUE;


	  newmsg.type = IS_NOT_GLOBALLY_CONNECTED_REPLY;

	  __send_backward (handler, &newmsg);
	}
      else
	{

	  //
	  // I do not report that im already global.
	  //
	  log_file ("MsgAHGR(%s): Already globally connected",
		    handler->leg->name);
	}
    }
  up (handler);
}



static void
handleMsgIsNotGlobalConnectedMessage (global_handler_t *handler,
				      message_t *msg)
{
  down (handler);
  //
  // If the global interface is reporting that there is something that
  // is not globally connected, it must be an error and therefore i
  // just ignore this message.
  //
  if (handler->current_global_interface == handler->leg->backwards_id)
    {
      log_error ("MsgINGC(%s): recieved not global from %s on global IF.",
		 handler->leg->name, msg->sender);
      up (handler);
      return;
    }

  if (handler->current_state == STATE_NOTHING)
    {
      //
      // just ignore this reply, we cannot use it for anything since
      // we are in a state where it gives no meaning to us.
      //
      up (handler);
      return;
    }

  //
  // We register that there is someone that is not globally
  // connected. 
  //
  handler->num_non_globals++;

  log_file ("MsgINGC(%s): %s is not connected.", handler->leg->name, msg->sender);

  up (handler);

}



static void
handleMsgInteruptTreeGrowth (global_handler_t *handler,
			     message_t *msg)
{
  message_t newmsg;

  down (handler);

  handler->num_non_globals =
    max (handler->num_non_globals,
	 (int) msg->competeData.numOfUnConnectedNeighbours);

  if (handler->competeValue == 0
      && handler->current_global_interface != handler->leg->backwards_id
      && handler->current_state == STATE_IS_EVOLVING_TREE)
    {
      log_file ("MsgInterrupt(%s): Interrupted by %s.", handler->leg->name, msg->sender);

      handler->current_state = STATE_IS_COMPETING_VALUES;
      handler->competeValue = lrandom () % 63 + 1;
      handler->idle_count_down = nodetime_timeplus (IDLE_COUNT_DOWN);
      handler->has_executed_idle = FALSE;

      newmsg.type = INTERRUPT_TREE_GROWTH;
      newmsg.competeData.competeValue = handler->competeValue;
      newmsg.competeData.numOfUnConnectedNeighbours =
	handler->num_non_globals;

      handler->biggestForeignCompeteValue = msg->competeData.competeValue;
      __send_backward (handler, &newmsg);
    }
  else if (handler->current_state == STATE_IS_COMPETING_VALUES
	   && handler->current_global_interface != handler->leg->backwards_id)
    {
      handler->biggestForeignCompeteValue =
	max (handler->biggestForeignCompeteValue,
	     (int) msg->competeData.competeValue);

      log_file ("MsgInterrupt(%s): Updated from %s val: %d - TTD: %d",
		handler->leg->name, msg->sender,
		(int) newmsg.competeData.competeValue,
		handler->idle_count_down);
    }
  up (handler);
}



static void
handleStateIsEvolvingTree (global_handler_t *handler)
{
  message_t msg;
  down (handler);
  handler->current_state = STATE_NOTHING;
  if (handler->num_non_globals > 0)
    {
      log_file ("StateIET(%s): %d unconnected neighbours. Forwards data now.",
		handler->leg->name, handler->num_non_globals);

      msg.type = GROW_TREE_REQUEST;
      __send_interface (handler, handler->current_reply_interface,
			  &msg);
      handler->leg->is_forwarding = TRUE;
    }
  else
    {
      log_file ("StateIET(%s): No isolated children.",
		handler->leg->name);
    }
  up (handler);
}

static void
handleStateIsCompetingValues (global_handler_t *handler)
{
  message_t msg;
  down (handler);
  handler->current_state = STATE_NOTHING;

  if (handler->competeValue == handler->biggestForeignCompeteValue)
    {
      log_file ("idle: damn, %s eqaul in in competion with value %d",
		handler->leg->name,
		(int) handler->biggestForeignCompeteValue);
    }

  else if (handler->competeValue > handler->biggestForeignCompeteValue)
    {

      log_file ("idle: %s won, send a second request command.",
		handler->leg->name);

      handler->current_state = STATE_IS_EVOLVING_TREE;
      handler->idle_count_down = IDLE_COUNT_DOWN;
      handler->competeValue = 0;
      handler->biggestForeignCompeteValue = 0;
      msg.type = ASK_HAS_GLOBAL_REQUEST;

      __send_interface (handler, handler->current_reply_interface,
			  &msg);
    }
  else if (handler->competeValue < handler->biggestForeignCompeteValue)
    {
      log_file ("IDLE: %s lost!", handler->leg->name);
    }
  up (handler);
}

static int
handleMessage (global_handler_t *handler, message_t *msg)
{
  down (handler);
  switch (msg->type)
    {
    case GROW_TREE_REQUEST:
      handleMsgGrowTreeRequest (handler, msg);
      break;

    case ASK_HAS_GLOBAL_REQUEST:
      handleMsgAskHasGlobalReply (handler, msg);
      break;

    case IS_NOT_GLOBALLY_CONNECTED_REPLY:
      handleMsgIsNotGlobalConnectedMessage (handler, msg);
      break;

    case INTERRUPT_TREE_GROWTH:
      handleMsgInteruptTreeGrowth (handler, msg);
      break;
    default:
      up (handler);
      return 0;
    }
  up (handler);
  return 1;
}

static void
handleIdle (global_handler_t *handler)
{
  down (handler);
  /* if (handler->idle_count_down > 0)
    handler->idle_count_down--;
  */
  if (handler->idle_count_down == nodetime_time ()
      && handler->current_state != STATE_NOTHING
      && handler->has_executed_idle == FALSE)
    {
      handler->has_executed_idle = TRUE;

      switch (handler->current_state)
	{
	case STATE_IS_EVOLVING_TREE:
	  handleStateIsEvolvingTree (handler);
	  break;

	case STATE_IS_COMPETING_VALUES:
	  handleStateIsCompetingValues (handler);
	  break;

	default:
	  break;
	}
    }
  up (handler);
}


static void 
__handle_socket_reception(lsocket_t *sock, global_handler_t *handler) 
{
  socket_waitfor(sock);
  lthread_mutex_lock(&handler->mutex);
  socket_read(sock,&handler->current_msg, sizeof(struct message));
  handleMessage(handler,&handler->current_msg);
  lthread_mutex_unlock(&handler->mutex);
}

static void
__dec_thread_count(global_handler_t *handler) {
  lthread_mutex_lock(&handler->mutex);
  handler->thread_count--;
  lthread_mutex_unlock(&handler->mutex);
}

static void
__inc_thread_count(global_handler_t *handler) {
  lthread_mutex_lock(&handler->mutex);
  handler->thread_count++;
  lthread_mutex_unlock(&handler->mutex);
}

static void *
__run_reciever_1(void *ptr) 
{
  global_handler_t *handler = ptr;
  __inc_thread_count(handler);
  while(handler->is_threaded == TRUE) 
    {
      __handle_socket_reception(handler->socket[0],handler);
    }
  __dec_thread_count(handler);

  lthread_exit(0);
  return 0;
}

static void *
__run_reciever_2(void *ptr) 
{
  global_handler_t *handler = ptr;
  __inc_thread_count(handler);
  while(handler->is_threaded == TRUE) 
    {
      __handle_socket_reception(handler->socket[1],handler);
    }
  __dec_thread_count(handler);
  lthread_exit(0);
  return 0;
}

static void *
__run_idle(void *ptr) 
{
  global_handler_t *handler = ptr;
  /// use handleIdle
  __inc_thread_count(handler);
  while(handler->is_threaded == TRUE) {
    lthread_mutex_lock(&handler->mutex);
    handleIdle(handler);
    lthread_mutex_unlock(&handler->mutex);

    lthread_msleep(1);
  }
  __dec_thread_count(handler);
  lthread_exit(0);
  return 0;
}


static void 
__start_global_protocol(struct protocol_handler *hand) 
{
  global_handler_t *handler = hand->object;
  lthread_create(&handler->thread_idle,__run_idle,handler);
  lthread_create(&handler->thread_reciever_1,__run_reciever_1,handler);
  lthread_create(&handler->thread_reciever_2,__run_reciever_2,handler);
}

static void
__destroy_handler (struct protocol_handler *hand)
{
  global_handler_t *handler = hand->object;
  handler->is_threaded = FALSE;
  up(handler->socket[0]);
  up(handler->socket[1]);
  while(handler->thread_count > 0)
    lthread_yield();

  lfree(handler);
}

protocol_handler_t *
create_global_handler (leg_t *leg)
{
  protocol_handler_t *phand = lobject_create (struct protocol_handler);
  global_handler_t *handler = lmalloc(sizeof(struct global_handler));
  phand->object = handler;
  phand->start = __start_global_protocol;
  lobject_set_destructor (phand, &__destroy_handler);
  // this is a back handler which has cyclic references and therefore
  // it shall not use any reference counting
  handler->leg = leg;
  handler->isChecked = FALSE;
  handler->is_global = FALSE;
  handler->idle_count_down = nodetime_time ();
  handler->has_executed_idle = TRUE;
  handler->current_state = STATE_NOTHING;

  handler->competeValue = 0;
  handler->biggestForeignCompeteValue = 0;

  handler->num_non_globals = 0;
  handler->has_said_i_was_not_global = FALSE;
  handler->current_global_interface = -1;

  //struct msg_handler_t *msgHandler = lobject_create (struct msg_handler_t);

  //msgHandler->handleIdle = &handleIdle;
  //msgHandler->handleMessage = &handleMessage;
  //msgHandler->object = handler;
  //handler->msg_handler = msgHandler;

  //leg_addMessageHandler (handler->leg, msgHandler);
  return phand;
}
