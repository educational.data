#ifndef __UART_HPP__
#define __UART_HPP__



typedef void (*serial_callback_function)(void *ptr, int size);

class node;

/**
 * @brief The serial communicator (UART) for the ODIN Module.
 *
 * @todo Decide interface.
 * @todo implement this interface.
 * $Revision: 1.5 $
 */
class SerialCommunicator {
private:
  serial_callback_function recieve_callback;
  void *recieve_callback_ptr;

  /**
   * @brief Hidden copy constructor.
   */
  SerialCommunicator(const SerialCommunicator &com) {}

  /**
   * @brief Executes the attached callback function.
   */
  bool call_callback(const int size);

public:

  /**
   * @brief the actual implementation of the uart.
   */
  struct uart_impl_t *impl;

  /**
   * @brief initialise the Communicator for one end connector.
   */
  SerialCommunicator(int id);
  ~SerialCommunicator();

  
  /**
   * @brief sends out a bit of communication on the framework that is there.
   * @arg ptr location in memory that holds the data to send.
   * @arg size length in bytes of the data to send.
   */
  void sendData(void *ptr, const int size);


  /**
   * @brief Function to tell system to start receiving data.
   *
   * The function is blocking until it has recieved the amount of data
   * that is said in the size flag.
   *
   * @arg ptr pointer to a buffer that is able to hold the recieved
   * data.  
   * 
   * @arg size indicates the amount of size that the buffer is
   * allowed to write.
   **/
  void recieveData(void *ptr, int size);


  /**
   * @brief Register a callback function for when data is ready to be
   * recieved.
   *
   * @arg callback Pointer to the function that is supposed to be
   * called is in this form: 
   * <pre> void function(void *ptr,int * size);</pre> 
   * The <i>size</i> is indicating the size of data available.
   *
   * @arg ptr any internal data that is supposed to be passed along
   * when the callback is being called.
   */
  void register_recieve_callback(serial_callback_function callback, void *ptr);


  /**
   * @brief Deregister the recieve callback function.
   *
   * It will simply set the callback to null.
   */
  void clear_recieve_callback();

#if defined (ON_BUILD_PLATFORM)
  /**
   * @brief provides a handle for the node to be able to connect the
   * leg to the node.
   */
  void attachNode(node & nod);
  
  node *getNode();

#endif
};

#endif 
