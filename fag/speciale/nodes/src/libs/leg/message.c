#if defined(HAVE_CONFIG_H)
# include "config.h"
#endif

#include "common.h"
#include "message.h"
#include <string.h>


struct message 
createGlobalConnectionMessage(void) 
{
  struct message msg;
  msg.type = GROW_TREE_REQUEST;
#if defined (ON_BUILD_PLATFORM)
  strcpy ((char *)msg.sender, "main");
#endif
  return msg;
}



struct message 
createTearDownMessage(void) 
{
  struct message msg;
  msg.type = NODE_TEAR_DOWN;
#if defined (ON_BUILD_PLATFORM)
  strcpy ((char *)msg.sender, "main");
#endif
  return msg;
}


  
struct message 
createAskWhoIsForwardingMessage(void) 
{
  struct message msg;
  msg.type = REPORT_FORWARDING_STATUS;
#if defined (ON_BUILD_PLATFORM)
  strcpy ((char *)msg.sender, "main");
#endif
  return msg;
}
