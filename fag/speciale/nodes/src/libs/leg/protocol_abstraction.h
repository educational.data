#ifndef __MSGHANDLER_H__INCLUDED__
#define __MSGHANDLER_H__INCLUDED__

#include "utils/lobject.h"

BEGIN_CSPEC

// forward declarations
typedef struct protocol_handler protocol_handler_t;

struct leg;
struct lsocket_provider;
struct protocol_handler;

/**
 * @brief message handler for the leg
 *
 * In order to handle Messages for the leg, a MsgHandler must be
 * implemented.
 *
 * @author Tobias G. Nielsen
 */
struct protocol_handler {
  LOBJECT;
  void *object;
  void (*start)(struct protocol_handler *handler);
  struct leg *leg;
  struct lsocket_provider *provider;
};

END_CSPEC

#endif
