#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "common.h"
#include "endcoord.h"


#if defined(ON_BUILD_PLATFORM)
# include <string.h>
# include <stdio.h>
#endif

#include <assert.h>

#include "utils/crc32.h"
#include "utils/lobject.h"
#include "utils/lrandom.h"
#include "utils/lthread.h"
#include "utils/lmemory.h"
#include "utils/lsocket.h"
#include "message.h"

//#define LOG_DUMP
#include "log.hpp"



#include "nodetime.h"
//static int id = 0;

CSPEC BOOL
endcoord_deliver_message (endcoord_t *e)
{
  lobject_ptr<endcoord_t> i(e);

  i->out_packet->size = sizeof (struct DataPacket);

  /// setting the crc val
  i->out_packet->crc = 0;
  i->out_packet->randomNumber = lrandom ();

  unsigned int crc =
    calc_CRC32 ((uint8_t *) i->out_packet, sizeof (DataPacket_t));

  i->out_packet->crc = crc;
  i->send_checksum = crc;
  /// done crc

  i->isSendingData = TRUE;

  i->last_sent = nodetime_time ();

  i->has_data_to_send = FALSE;
  
  socket_write(i->socket, i->out_packet, sizeof(struct DataPacket));

  i->isSendingData = FALSE;

  //
  // mark collision is set when the endcoordinator
  // recieves a looping message.
  //
  if (i->markCollision == TRUE)
    {
      i->markCollision = FALSE;
      return TRUE;
    }
  return FALSE;
}

static void
__handleTokensAddition (endcoord_t *e, int id)
{
  lobject_ptr<endcoord_t> i = e;
  
  if (!larray_contains (&i->tokens, (void *) id))
    larray_insert (&i->tokens, (void *) id);
}

static void
__parseInBuffer (endcoord_t *e)
{
  lobject_ptr<endcoord_t> i = e;
  i->last_recieved = nodetime_time ();

  int flags = i->in_packet->flags;
  int protocol = i->in_packet->protocol;
  if ((flags & MSG_PING) && !(flags & MSG_PING_REPLY))
    {
      log_out("%s Got PING",i->leg->name);

      // send ping reply
      i->out_packet->flags = (MSG_PING_REPLY | MSG_PING);
      i->has_data_to_send = TRUE;
      i->current_state = STATE_INITIAL;
    }

  if (flags & MSG_ID)
    {
      log_out("%s (%d) Got id msg (%d)", i->leg->name, i->my_token, i->in_packet->id);
      __handleTokensAddition (i, (int) i->in_packet->id);
    }

  if ((flags & MSG_TOKEN) && (i->in_packet->new_id == i->my_token))
    i->has_token = TRUE;

  if (flags & MSG_DATA)
    {
      if(lsocket_provider_has_protocol(i->provider,protocol) == TRUE)
        {
          msg_data_t *msg = static_cast<msg_data_t*>(lmalloc (sizeof(struct msg_data)));
          lmemcpy (msg->data, &i->in_packet->data, 100);
          msg->protocol = protocol;
          log_out("Message has arrived for protocol %d", protocol);
          lqueue_push (&i->in_messages, msg);
          lsocket_provider_post(i->provider,protocol);
        }
      else
        {
          log_out("Message dropped for protocol %d", protocol);
        }
    }
}

///////////////////////////////////////////////


/**
 * @brief Called when there is a piece of data in the inbuffer.
 *
 * Currently this function is called directly from the
 * link.c#link_send_message() function.
 */
static void
__endcoord_recieve_data (void *iPtr)
{
  lobject_ptr<endcoord_t> i = (endcoord_t *) iPtr;
  uint8_t size = 0;
  void *data = i->inBuffer;
  assert (data != NULL);

  size = socket_read(i->socket,i->inBuffer, sizeof(struct DataPacket)+10);

  assert (size >= sizeof (struct DataPacket));

  lthread_mutex_lock (&i->recieve_mutex);

  //
  // we avoid looping if we are sending data.
  //
  if (i->isSendingData == TRUE)
    {
      //
      // tell that we have a package collision.
      //
      i->markCollision = TRUE;
    }


  //
  // verify the crc of the data.
  //
  unsigned int indicated_crc = i->in_packet->crc;
  i->in_packet->crc = 0;

  unsigned int crc =
    calc_CRC32 ((uint8_t *) i->in_packet, sizeof (struct DataPacket));

  if (crc != indicated_crc)
    {
      //
      // data corrupted.
      //
      lthread_mutex_unlock (&i->recieve_mutex);
      log_out("Data is corrupted");
      return;
    }

  if (i->markCollision == TRUE)
    {
      //
      // test to see if the collision is from the last
      // send
      //
      if (indicated_crc == i->send_checksum)
	i->markCollision = FALSE;

      lthread_mutex_unlock (&i->recieve_mutex);
      log_out("collision");
      return;
    }

  __parseInBuffer (i);
  lthread_mutex_unlock (&i->recieve_mutex);
}

// defined in endcoord_statemachine.c
CSPEC void endcoord_state_idle_handler(endcoord_t *i);

CSPEC bool_t
endcoord_idle_handler (endcoord_t * e)
{
  bool_t res;
  lobject_ptr<endcoord_t> i = e;

  endcoord_state_idle_handler (i);
  res = i->isThreaded;
  return res;
}



CSPEC bool_t 
endcoord_is_connected (endcoord_t * i) 
{
  lobject_ptr<endcoord_t> e = i;

  return e->current_state != STATE_NC;
}


static void *
__threadIdle (void *ptr)
{
  endcoord_t *i = (endcoord_t *) ptr;
  while ((endcoord_idle_handler (i)) == TRUE) 
    {
      lthread_msleep(1);
    }

  lthread_exit (0);
  return NULL;
}



static void *
__threadReceptor (void *ptr)
{
  endcoord_t *i = (endcoord_t *) ptr;

  while (i->isThreaded == TRUE)
    {
      if(socket_waitfor(i->socket) == EOK)
        __endcoord_recieve_data (i);
      lthread_yield();
    }
  lthread_exit (0);
  return NULL;
}


static void __start(protocol_handler_t *prot) 
{
  assert(prot != NULL);
  lobject_ptr<endcoord_t> i = *static_cast<endcoord_t *>(prot->object);

  i->socket = leg_seek_socket(i->leg, i->end);
  assert (i->isThreaded == FALSE);
  i->isThreaded = TRUE;
  lthread_create (&(i->thread), __threadIdle, (void *) i);
  lthread_create (&(i->thread), __threadReceptor, (void *) i);
}


static void
__destroy_endcoord (endcoord_t *impl)
{
  assert(impl != NULL);
  if (impl->isThreaded == TRUE)
    {
      impl->isThreaded = FALSE;
      lthread_join (&(impl->thread));
    }
  up (impl->socket);
  lqueue_destroy (&impl->out_messages);
  lqueue_destroy (&impl->in_messages);
  larray_destroy (&impl->tokens);
  lthread_mutex_destroy (&impl->recieve_mutex);
}


static int
__endcoord_write_to_socket (lsocket_provider_t *p, 
                 uint8_t protocol, 
                 object_t ptr,
		 object_t buffer, 
                 uint8_t size)
{
  lobject_ptr<endcoord_t> ec = (endcoord_t*)ptr;
  if(lqueue_empty(&ec->in_messages) == TRUE)
    return 0;

  msg_data_t *msg = static_cast<msg_data_t *>(lqueue_pop(&ec->in_messages));

  if(100 < size)
    size = 100;

  lmemcpy(buffer,msg->data,size);
  lfree (msg);

  return size;
}


static int
__endcoord_read_from_socket (lsocket_provider_t *p, 
                  uint8_t protocol, 
                  object_t ptr,
		  object_t buffer, 
                  uint8_t size)
{
  lobject_ptr<endcoord_t> ec = (endcoord_t*)ptr;
 
  msg_data_t *msg = static_cast<msg_data_t *>(lmalloc(sizeof(struct msg_data)));

  if(size > 100)
    size = 100;

  msg->protocol = protocol;
  lmemcpy(msg->data, buffer, size);
  
  lqueue_push(&ec->out_messages,msg);
  return size;
}

static bool_t 
__endcoord_socket_poll (lsocket_provider_t *provider, 
                        uint8_t protocol, object_t ptr)
{
  lobject_ptr<endcoord_t> ec = (endcoord_t*)ptr;
  
  if(! lqueue_empty(&ec->in_messages))
    {
      msg_data_t *msg = static_cast<msg_data *>(lqueue_peek(&ec->in_messages));
      if(msg->protocol == protocol || msg->protocol == 0 || protocol == 0)
          return TRUE;
    }
  return FALSE;
}


// we have the section from 10 -> 15 and 20 -> 25
static bool_t 
__endcoord_socket_has_id(lsocket_provider_t *provider, 
                         object_t ptr, 
                         uint8_t protocol_id)
{
  lobject_ptr<endcoord_t> ec = (endcoord_t*)ptr;

  if((ENDCOORD_BASE_ID + ec->end*10 <= protocol_id) && (ENDCOORD_BASE_ID + ec->end*10 + 5  > protocol_id))
    return TRUE;
  else
    return FALSE;
}




CSPEC protocol_handler_t *
create_endcoord (leg_t *leg, int end)
{
  protocol_handler_t *prot = lobject_create (struct protocol_handler);
  lobject_set_destructor (prot,
			  (lobject_destructor) & __destroy_endcoord);

  
  prot->start = __start;

  endcoord_t *i = static_cast<endcoord_t*>(lmalloc(sizeof (endcoord_t)));
  assert(i != NULL);
  prot->object = i;
  
  i->isSendingData = FALSE;
  i->markCollision = FALSE;
  i->funcs.write_to_socket = __endcoord_write_to_socket;
  i->funcs.read_from_socket = __endcoord_read_from_socket;
  i->funcs.poll = __endcoord_socket_poll;
  i->funcs.provides_protocol = __endcoord_socket_has_id;
  prot->provider = lsocket_create_provider(&i->funcs);
  prot->provider->ptr = i;
  i->provider = prot->provider;

  i->out_packet = (DataPacket_t *) (i->outBuffer);
  i->in_packet = (DataPacket_t *) (i->inBuffer);
  lmemset ((void *) i->outBuffer, 0, ENDCOORD_BUFFER_SIZE);
  lmemset ((void *) i->inBuffer, 0, ENDCOORD_BUFFER_SIZE);

  lqueue_create (&i->out_messages);
  lqueue_create (&i->in_messages);
  larray_create (&i->tokens);
  lthread_mutex_init (&i->recieve_mutex);

  i->leg = leg;
  i->end = end;
  i->last_recieved = 1600;
  i->last_sent = 1600;
  i->last_run = 0;
  i->current_state = STATE_NC;
  i->has_token = FALSE;
  i->isThreaded = FALSE;
  i->has_data_to_send = FALSE;
  return prot;
}
