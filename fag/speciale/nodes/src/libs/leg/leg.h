#ifndef __LEG_H__INCLUDED__
#define __LEG_H__INCLUDED__

#include "utils/lobject.h"
#include "utils/array.h"
#include "utils/lsocket.h"
#include "leg/protocol_abstraction.h"

#if defined(ON_BUILD_PLATFORM)
# include "link/link.h"
#endif
#include "message.h"
#include "msghandler.h"

BEGIN_CSPEC
/**
 * @brief Representation of a generic leg.
 *
 * It is only a generic interface that encapsulates the actual
 * interface for a true leg type. Internally (private) you should take
 * a much closer look at the class leg_msgState_t to get a full view of
 * what is truely going on.
 *
 * @see leg_msgState_t
 */
typedef struct leg {
  LOBJECT;
  /** 
   * @brief Actual private implementation class (pImpl ideom)
   */
  BOOL is_recieving_1;
  BOOL is_recieving_2;
  BOOL volatile has_data_1;
  BOOL volatile has_data_2;

  BOOL volatile is_running;
  BOOL volatile tear_down;
  unsigned int forward_id;
  unsigned int backwards_id;

#if defined(ON_BUILD_PLATFORM)
  //string p_name;
  char name[20];
#endif
  lthread_t thread;
  lmutex_t mutex1;
  lmutex_t mutex2;

  larray_t sockets;
  larray_t protocols;

  int idle_count_down;
  //int whished_net;
  //BOOL has_said_i_was_not_global;
 
  BOOL is_forwarding;

  larray_t handlers;
  // std::vector<IMsgHandler *> handlers;
  // typedef std::vector<IMsgHandler *>::iterator itvechandle;
  struct uart *uart[2];
} leg_t;


leg_t *create_leg(void);
leg_t *create_leg_with_name(char *name);
leg_t *create_leg_with_name_and_number (const char *name, int number);

void leg_add_network_protocol(leg_t *leg, 
                              protocol_handler_t *handler);
                              

void leg_add_link_protocol(leg_t *leg, protocol_handler_t *handler);

/**
 * @brief find the optimal socket for the needed id.
 *
 * Id is currently just an Protocol ID. So if there is no socket that
 * provides this protocol id, there will not be any socket available.
 */                           
lsocket_t *leg_seek_socket(leg_t *leg, uint8_t id);

/**
 * @brief get the forwarding state of this leg.
 */
BOOL leg_getIsForwarding(leg_t *leg);
void leg_setIsForwarding (leg_t *leg, BOOL state);

#if defined (ON_BUILD_PLATFORM)
/**
 * @brief attach a node to a side of the leg.
 *
 * @arg nod the node that needs to be attached to the node function.
 * @arg id must be either 0 or 1 to indicate a side that the node is
 *      attached to.
 */
void leg_setLink(leg_t *leg, link_t *nod, int id);


/**
 * @brief resolves the attached nodes for this leg.
 *
 * @arg id is the end that the node is attached to.
 */
link_t *leg_getNode(leg_t *leg, int id);

/**
 * @brief Requests if the node is present in this nodeend.
 *
 * @arg id is the end that the node is attached to.
 */
bool_t leg_hasNode(leg_t *leg, int id);

/**
 * @brief the nodes name
 *
 * This is offcourse only meant for testing since it will not be
 * included in the implementation of the system. But in some near
 * future i will expect that this will be modified so it will only
 * be included in the compile scripts for the simulator.
 */
//const std::string &leg_getName() const;
#endif

/**
 * @brief reports the current global state for this leg
 *
 * This functionality is currently only used inside the actual leg
 * code and therefore is not necisarry to export as a public
 * function. Therefore this function is currently deprecated
 * @deprecated
 */
bool_t leg_getIsGlobal(leg_t *leg);

END_CSPEC

#endif
