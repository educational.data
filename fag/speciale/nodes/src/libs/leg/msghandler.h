#ifndef __MSGHANDLER_H__INCLUDED__
#define __MSGHANDLER_H__INCLUDED__

#include "utils/lobject.h"


// forward declarations
struct leg;
struct lsocket_provider;

BEGIN_CSPEC
/**
 * @brief message handler for the leg
 *
 * In order to handle Messages for the leg, a MsgHandler must be
 * implemented.
 *
 * @author Tobias G. Nielsen
 */
typedef struct msg_handler {
  LOBJECT;
  object_t object;
  struct leg *leg;
  struct lsocket_provider *provider;
} msg_handler_t;

END_CSPEC

#endif
