#ifndef __MESSAGE_HPP__
#define __MESSAGE_HPP__

#include "common.h"

////////// MESSAGES 
#define GROW_TREE_REQUEST 1
#define ASK_HAS_GLOBAL_REQUEST 2
#define IS_NOT_GLOBALLY_CONNECTED_REPLY 3
#define INTERRUPT_TREE_GROWTH 4

#define BUILD_POTENTIAL 5
#define SEEK_POTENTIAL 6
#define HAS_POTENTIAL 7

#define REPORT_FORWARDING_STATUS 8
#define NODE_TEAR_DOWN 9



/**
 * @brief The structure actually defining a package. 
 *
 * The package is supposed to be as small as possible and therefore
 * uses the gcc extension __packed__
 *
 * @Version $Revision: 1.3 $
 */
BEGIN_CSPEC

struct message {
  /// List of all the different package states that we can use.
  uint8_t type;

  /// Identifier so there is no chance of recieving the same message twice.
  uint8_t messageId;
#if defined(ON_BUILD_PLATFORM)
  uint8_t sender[20];
#endif
  union {
    /// Competing data that represenst speciall data when the system
    /// is waiting for competing information.
    struct {
      uint8_t competeValue;
      uint8_t numOfUnConnectedNeighbours;
    } competeData;
    struct {
      int8_t net_id;
      int8_t potential;
    } potentialData;
  };
} __attribute__((__packed__));

typedef struct message message_t;

#if defined(ON_BUILD_PLATFORM)
message_t createGlobalConnectionMessage(void);
message_t createTearDownMessage(void);
message_t createAskWhoIsForwardingMessage(void);
#endif
END_CSPEC

#endif
