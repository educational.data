#ifndef __ENDCOORD_H__
#define __ENDCOORD_H__

#include "message.h"
#include "nodetime.h"

#include "utils/queue.h"
#include "utils/array.h"
#include "utils/lobject.h"
#include "utils/lthread.h"
#include "leg.h"
#include "leg/protocol_abstraction.h"

#define MSG_PING (1 << 0)
#define MSG_PING_REPLY (1 << 1)
#define MSG_DATA (1 << 2)
#define MSG_ID (1 << 3)
#define MSG_ID_CONFLICT (1 << 4)
#define MSG_TOKEN (1 << 5)



BEGIN_CSPEC

#define ENDCOORD_BASE_ID 10
#define ENDCOORD_BUFFER_SIZE 255

#define STATE_NC 0
#define STATE_INITIAL 1
#define STATE_AWAITS_ID 2
#define STATE_CONFLICTING_ID 3
#define STATE_USE_TOKEN 4
#define STATE_TOKEN_SLAVE 5

typedef struct endcoord endcoord_t;
typedef struct DataPacket DataPacket_t;

typedef struct msg_data {
  uint8_t protocol;
  uint8_t data[100];
} msg_data_t;

/** 
 * @brief low-level data-link packet 
 *
 * The data packet is an implementation to encompass a data link layer
 * protocol.
 *
 */
struct DataPacket
{
  /** 
   * @brief Will be representing a valid size.
   *
   * For now it can only represent one size - namely the size of the
   * baseheader + a message.  But in the future it will be optional to
   * include the message.
   *
   * The connector tries to load this amount of data before marking
   * the end of a recieve/transmit period.
   * @see endcoordinator.c
   */
  uint8_t size;

  /// Standard CRC marker. (Should it be moved to the end of the package?)
  uint32_t crc;

  /** 
   * @brief TODO: Find out why it is that i need to include a random
   * number every time.
   */
  uint16_t randomNumber;

  /// Indicates what this message is used for.
  uint8_t flags;

  /** 
   * @brief The sender id of the endconnector that has sent this
   * message. Should this be changed?
   */
  uint8_t id;

  /** 
   * @brief When a competition occours, this will be used for
   *        selecting another id.
   */
  uint8_t new_id;

  /**
   * @brief in case of a package arrival, which protocol has 
   *        just recieved a message.
   */

  uint8_t protocol;

  uint8_t data[100];
} __attribute__ ((__packed__));

/**
 * @brief implementation for the endcoordinator.
 * 
 * This data structure is using the lobject method and therefore one
 * should read and understand the usage described with lobject before
 * using this structure.
 *
 * @see lobject.h
 * @see endcoord.h
 * @see EndCoordinator
 */
struct endcoord
{
  volatile BOOL isThreaded;

  BOOL isSendingData;
  volatile BOOL markCollision;

  volatile int current_state;
  //volatile int cid;
  nodetime_t last_recieved;
  nodetime_t last_sent;
  nodetime_t last_run;
  nodetime_t last_state_time;
  int has_data_to_send;

  struct leg *leg;
  lsocket_t *socket;

  volatile BOOL has_token;

  int my_token;
  int next_token;
  int largest_token;

  uint8_t inBuffer[ENDCOORD_BUFFER_SIZE];		/// The actual inbuffer.
  uint8_t outBuffer[ENDCOORD_BUFFER_SIZE];		/// The actual outbuffer
  DataPacket_t *out_packet;	/// wraps the in_buffer in a DataPacket_t structure
  DataPacket_t *in_packet;	/// wraps the in_buffer in a DataPacket_t structure

  unsigned int send_checksum;

  /**
   * @brief queue line that holds a stack of outgoing messages.
   *
   * The messages is to be handled for now with normal queue
   * functions. If possible, you should prefer to use the
   * implementation that exists in EndCoordinator::sendMessage(...)
   *
   * But should it be necisarry, one can offcourse use ordinary
   * functionality like with the #lqueue_push(...) function: 
   * 
   * <pre>
   *   message msg;
   *   // do what is needed with msg
   *   lqueue_push(&out_messages, &msg);
   * </pre>
   */
  lqueue out_messages;

  /** 
   * @brief queue line that holds a stack of incomming messages
   *
   * The messages is to be handled for now with normal queue functions
   * as described in utils/lqueue.h
   */
  lqueue in_messages;
  larray_t tokens;

  lthread_t thread;
  lmutex_t recieve_mutex;
  lsocket_provider_funcs_t funcs;
  lsocket_provider_t *provider;
  uint8_t end;
};


/**
 * @brief Creates and initialises the fields in the endcoord_impl
 *        structure. 
 */
protocol_handler_t *create_endcoord (leg_t *leg, int end);
//void endcoord_recieve_data (endcoord_impl * i);
// BOOL endcoord_idle_handler (endcoord_t * i);
// BOOL endcoord_hasMessage( endcoord_t * i);
// void endcoord_sendMessage( endcoord_t * i,  message_t *msg);
// struct message *endcoord_getMessage( endcoord_t * i);
// BOOL endcoord_is_connected ( endcoord_t * i);
#if ! defined(ON_MODULE)
//void endcoord_start_own_thread ( endcoord_t * i);
#endif

END_CSPEC
#endif
