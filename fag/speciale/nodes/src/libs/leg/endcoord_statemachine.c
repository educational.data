

#if HAVE_CONFIG_H
# include "config.h"
#else 
# error ....
#endif


//#define LOG_DUMP
#include "log.hpp"


#include "common.h"
#include "endcoord.h"

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "nodetime.h"
#include "utils/lrandom.h"
#include "utils/lmemory.h"

BOOL endcoord_deliver_message (endcoord_t *i);

static void 
change_state(endcoord_t *i, int state) {
  //int last_state = i->current_state;
  i->current_state = state;

  i->last_state_time = nodetime_time();
}

static void 
handle_use_token (endcoord_t *this);

/**
 * @brief handle unconnected state
 *
 * Continuesly sends ping requests out on the interface to
 * notify an eventual network of its pressence.
 * If it is less than three timesteps since there 
 * was communication,
 * it will assume that we are still connected.
 */
static void
handle_nc (endcoord_t *i)
{
  assert (i != NULL);
  if (i->last_sent == 1600 || nodetime_timeElapsedB (i->last_sent, 3))
    {				// initial
      i->out_packet->flags = MSG_PING;
      log_out ("%s PING",i->leg->name);
      endcoord_deliver_message (i);
    }
  else if (nodetime_timeElapsedB (i->last_recieved, 3) == FALSE
	   && i->last_recieved != 1600)
    {
      log_out ("Timely responce - connected.");
      change_state(i,STATE_INITIAL);
    }
}

static void
handle_con (endcoord_t *i)
{
  assert (i != NULL);
  //log_out("Got connection");
  if (nodetime_timeElapsedB (i->last_recieved, 3) &&
      nodetime_timeElapsedB (i->last_sent, 3))
    {
      // Have we lost something, lets try to send a ping message.
      i->out_packet->flags = MSG_PING;
      endcoord_deliver_message (i);
      return;
    }

  if (nodetime_timeElapsedB (i->last_recieved, 6) == TRUE)
    {
      change_state(i,STATE_NC);
      log_out ("Dead communication, return to not connected.");
      return;
    }
  //else printf ("S\n");
}


static void
handle_initial (endcoord_t *this)
{
  assert (this != NULL);
  this->out_packet->flags = MSG_ID;
  this->out_packet->id = (lrandom () % 63) + 1;

  this->my_token = this->out_packet->id;
  this->next_token = 0;
  this->largest_token = 0;
  log_out("%s Transmit my id: %d", this->leg->name,this->my_token);

  change_state(this,STATE_AWAITS_ID);

  endcoord_deliver_message (this);
}

#define imax(a, b) a > b ? a : b

static void
handle_wait_for_id (endcoord_t *this)
{
  int rid;
  if (nodetime_timeElapsedB (this->last_recieved, 3) &&
      larray_contains (&this->tokens, (void *) this->my_token))
    {
      change_state(this,STATE_CONFLICTING_ID);
      //this->out_packet->flags = MSG_ID_CONFLICT;
      return;
    }

  if (nodetime_timeElapsedB (this->last_recieved, 3) &&
      nodetime_timeElapsedB (this->last_sent, 3))
    {
      this->largest_token = 0;
      this->next_token = 0;
      larray_insert (&this->tokens, (void *) this->my_token);
      // it seems that we are ready to go to token state.
      for (rid = 0; rid < larray_size (&this->tokens); rid++)
	{
	  int id = (int) larray_get (&this->tokens, rid);
	  this->largest_token = imax (this->largest_token, id);
	  if(id < this->my_token)
	    this->next_token = imax(id,this->next_token);
	}

      if(this->next_token == 0)
	this->next_token = this->largest_token;
      
      larray_clear (&this->tokens);
      if (this->largest_token == this->my_token)
	{
	  log_out ("%d Owns the token.", this->my_token);
          change_state(this, STATE_USE_TOKEN);
	}
      else
	{
	  log_out ("%d Change to slave", this->my_token);
	  change_state(this,STATE_TOKEN_SLAVE);
	}
      return;
    }

  if (nodetime_timeElapsedB (this->last_recieved, 12))
    {
      log_out ("Connection lost");
      change_state(this,STATE_NC);
    }
}

static void
handle_conflicting_id (endcoord_t *this)
{
  int rid;
  change_state(this,STATE_AWAITS_ID);
  log_out("CONFLICTING ID! %d", this->my_token);
  larray_remove (&this->tokens, (void *) this->my_token);
  this->out_packet->id = this->my_token;
  this->my_token = 0;
  while (this->my_token == 0)
    {
      rid = (lrandom () % 63) + 1;
      if (larray_contains (&this->tokens, (void *) rid))
	this->my_token = rid;
    }
  this->out_packet->new_id = this->my_token;
  endcoord_deliver_message (this);
}



static void
handle_use_token (endcoord_t *this)
{
   //printf("%d run with the token.\n",this->cid);
  if (lqueue_empty (&(this->out_messages)) == TRUE)
    {
      // relingwish token
      log_out ("relingwish token from %d to %d", this->my_token, this->next_token);
      
      change_state(this,STATE_TOKEN_SLAVE);
      this->out_packet->flags  = MSG_TOKEN;
      this->out_packet->new_id = this->next_token;
      this->out_packet->id     = this->my_token;
      this->has_token          = FALSE;
      endcoord_deliver_message (this);
    }
  else 
    {
      msg_data_t *msg = lqueue_pop(&this->out_messages);
      lmemcpy (&this->out_packet->data, msg->data, 100);

      this->out_packet->protocol = msg->protocol;
      
      this->out_packet->flags = MSG_DATA;
      this->out_packet->id    = this->my_token;
      
      if (lqueue_empty (&this->out_messages))
	{ // We have no more data to send and therefore continues.
          //log_out ("sending and relingwish token");
	  this->out_packet->flags |= MSG_TOKEN;
	  this->out_packet->new_id = this->next_token;
          this->has_token          = FALSE;

          change_state(this,STATE_TOKEN_SLAVE);
	}
      else 
        {
          // we are actually already in this state, so this is just added 
          // for clarity.
          change_state(this,STATE_USE_TOKEN);
        }
      endcoord_deliver_message (this);
    }
}



static void
handle_token_slave (endcoord_t *this)
{
  if(this->has_token == TRUE) {
    //log_out ("Got token");
    change_state(this,STATE_USE_TOKEN);
    return;
  }
  // do noting really, just watch out for lost messages.
  if (nodetime_timeElapsedB (this->last_recieved, 12) &&
      nodetime_timeElapsedB (this->last_sent, 12))
    {
      // seems that either we are lost or 
      // someone else that was supposed to 
      // have the token was lost.
      log_out ("I guess we have lost the token");
      change_state(this,STATE_NC);
    }
}

/**
 * This should really be updated a lot. It is not supporting reentrant
 * code at all.
 */
void
endcoord_state_idle_handler (endcoord_t *this)
{
  //log_out("!!!");
  assert (this != NULL);
  if (this->has_data_to_send == TRUE)
    {
      this->has_data_to_send = FALSE;
      //printf("\n");
      endcoord_deliver_message (this);
      return;
    }

  //printf("Time: %d\n",nodetime_time());
  
  if ( ! nodetime_timeElapsedB (this->last_run,1) )
     return;

  /* log_file("\33[3%d;1m R %2d S %2d state %d ",
           this->cid+1,
           nodetime_time() - this->last_recieved, 
           nodetime_time() - this->last_sent, 
           this->current_state);*/

  this->last_run = nodetime_time();
  switch (this->current_state)
    {
    case STATE_NC:
      //log_out ("%d NC\n", i->cid);
      handle_nc (this);
      break;
    case STATE_INITIAL:
      //printf ("%d CN\n", i->cid);
      handle_con (this);
      handle_initial (this);
      break;

    case STATE_AWAITS_ID:
      handle_wait_for_id (this);
      break;

    case STATE_CONFLICTING_ID:
      handle_conflicting_id (this);
      break;

    case STATE_USE_TOKEN:
      handle_use_token (this);
      break;

    case STATE_TOKEN_SLAVE:
      handle_token_slave (this);
      break;

    default:
      log_file("(%d) Enterred unknown state %d\n",
               nodetime_time(), this->current_state);
      break;
    }
}
