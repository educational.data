#ifndef __LEG_HPP__
#define __LEG_HPP__

#if defined(ON_BUILD_PLATFORM)
# include <string>
# include <boost/shared_ptr.hpp>

using namespace std;

#endif

class Node;
class EndCoordinator;
typedef Node * pnode;

typedef struct message message_t;
typedef struct leg leg_t;



/**
 * @brief Representation of a generic leg.
 *
 * It is only a generic interface that encapsulates the actual
 * interface for a true leg type. Internally (private) you should take
 * a much closer look at the class leg_msgState_t to get a full view of
 * what is truely going on.
 *
 * @see leg_msgState_t
 */
class Leg {
  void initLeg();

public:
  /** 
   * @brief Actual private implementation class (pImpl ideom)
   */
  leg_t *leg;

# if defined(ON_BUILD_PLATFORM)
  Leg(const string &title);
  Leg(const string &title, int number);
# endif

  Leg();        
  ~Leg();

  /**
   * @brief get the forwarding state of this leg.
   */
  bool getIsForwarding() const;
  void setIsForwarding (const bool state);


# if defined (ON_BUILD_PLATFORM)
  /**
   * @brief attach a node to a side of the leg.
   *
   * @arg nod the node that needs to be attached to the node function.
   * @arg id must be either 0 or 1 to indicate a side that the node is
   *      attached to.
   */
  void setNode(Node &nod, const int id);

  /**
   * @brief attach a node to a side of the leg.
   *
   * @arg nod the node that needs to be attached to the node function.
   * @arg id must be either 0 or 1 to indicate a side that the node is
   *      attached to.
   */
  void setNode(Node *nod, const int id);

  /**
   * @brief resolves the attached nodes for this leg.
   *
   * @arg id is the end that the node is attached to.
   */
  Node &getNode(int id) const;

  /**
   * @brief Requests if the node is present in this nodeend.
   *
   * @arg id is the end that the node is attached to.
   */
  bool hasNode(int id) const;
  
  /**
   * @brief the nodes name
   *
   * This is offcourse only meant for testing since it will not be
   * included in the implementation of the system. But in some near
   * future i will expect that this will be modified so it will only
   * be included in the compile scripts for the simulator.
   */
  const string &getName() const;
# endif
};

BUILD_ON_PC(typedef boost::shared_ptr<Leg> leg_tp;)

#endif
