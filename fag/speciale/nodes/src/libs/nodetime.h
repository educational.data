#ifndef __NODES_TIME_HPP__
#define __NODES_TIME_HPP__

#include "common.h"

BEGIN_CSPEC

typedef int nodetime_t;

void nodetime_initTime(void);
void nodetime_initTime_static(void);
void nodetime_tick(void);
nodetime_t nodetime_time(void);
nodetime_t nodetime_timeplus(const nodetime_t alpha);
//BOOL timeElapsed(const time_t deadTime);
BOOL nodetime_timeElapsedB(const nodetime_t oldTime, const nodetime_t allowedTime);

END_CSPEC
#endif

