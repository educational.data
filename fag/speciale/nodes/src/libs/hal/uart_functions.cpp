/**
 *
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "common.h"

#include "utils/lobject.h"
#include "utils/lmemory.h"
//#include <dirent.h>
#include "uart.h"
#include "stdio.h"

//#define LOG_DUMP
#include "log.hpp"

CSPEC void call_rcv_callback (uart_t *uart);
CSPEC void uart_send_startSequence (uart_t *uart);
CSPEC void uart_send_endSequence (uart_t *uart);
CSPEC void uart_read_data_direct (uart_t *uart, void *buffer,
				  size_t * uiCommandLength);
CSPEC int uart_send_data (uart_t *uart, void *data, size_t size);

static int
__uart_func_escape_date (uart_t *uart, uint8_t * inBuffer,
		       uint8_t * outBuffer, uint16_t * size)
{
  uint16_t nSize = 0;
  uint8_t bufData;

  for (uint16_t i = 0; i < *size; ++i)
    {
      bufData = inBuffer[i];
      if (bufData == 0x13 || bufData == 0x31 || bufData == 0x33)
	{
	  bufData = ~bufData;
	  outBuffer[nSize++] = 0x33;
	}
      outBuffer[nSize++] = bufData;
    }
  *size = nSize;
  return nSize;
}


bool
uart_tie_test_insert_id (uart_ids_t & u, uint16_t id)
{
  lthread_mutex_lock (&u.test_mutex);

  for (int i = 0; i < 16; i++)
    {
      if (id == u.id[i])
	{
	  lthread_mutex_unlock (&u.test_mutex);
	  return false;
	}
    }
  u.id[u.cp++] = id;
  if (u.cp >= 16)
    u.cp = 0;

  lthread_mutex_unlock (&u.test_mutex);
  return true;
}


/**
 * @brief Send a message and do checking of the output at the same
 *        time.
 *
 * The method applied here is by sending a single message and then
 * testing if the message appear as input right away for the reader.
 * If the reader do not read the message right away, it is assumeably
 * colision affected and therefore the system can right away skip and
 * return.
 *
 * @return NULL if message was sent succesfully, or EAGAIN if
 *         collision.
 */

CSPEC int uart_send_message (uart_t *uart, void *data, uint8_t size)
{
  uint8_t buffer[2*size];
  uint16_t siz = size;
  
  down (uart);
  uart->is_transmitting_package = TRUE;

  uart_send_startSequence (uart);

  __uart_func_escape_date (uart, (uint8_t *)data, buffer, &siz);

  uart_send_data (uart, buffer,siz);

  uart_send_endSequence (uart);
  //lthread_yield();
  uart->is_transmitting_package = FALSE;
  up (uart);
  return size;
}


static int
readData (uart_t *uart)
{
  uint32_t uiCommandLength = 1;
  unsigned char buffer[1];

readData_retry:
  uiCommandLength = 1;
  uart_read_data_direct (uart, (void *) buffer, &uiCommandLength);

  if (uart->is_transmitting_package == TRUE) 
      goto readData_retry;

  if (buffer[0] == 0x31)
    {
      uart->curr_buffer_ptr = 0;
      uart->curr_written_buffer_ptr = 0;
      uart->is_recieving_package = TRUE;
      goto readData_retry;
    }

  if (buffer[0] == 0x13)
    {
      uart->is_recieving_package = FALSE;
      return 0;
    }

  if (uart->is_recieving_package != TRUE)
    goto readData_retry;

  //printf("%d - buffer: 0x%X\n",uiCommandLength,buffer[0]);

  if (buffer[0] == 0x33)
    {
      uiCommandLength = 1;
      uart_read_data_direct (uart, (void *) buffer, &uiCommandLength);
      buffer[0] = ~buffer[0];
    }

  uart->inBuffer[uart->curr_buffer_ptr++] = buffer[0];
  return 1;
}

static void *
reader_routine (void *ptr)
{
  uart_t *uart = (uart_t *) ptr;
  //unsigned char buffer[1];

  //cyg_uint32 uiCommandLength;
  for (;;)
    {
      int res = readData (uart);
      //fflush (stdout);
      // send currently lacking data..
      if (uart->is_forwarding == TRUE &&
	  uart->curr_buffer_ptr > sizeof (simplePackageHdr_t) &&
	  uart->other_uart != NULL &&
	  res != 0 &&
	  (uart->curr_buffer_ptr - uart->curr_written_buffer_ptr) > 0)
	{
	  unsigned clen =
	    uart->curr_buffer_ptr - uart->curr_written_buffer_ptr;

	  uart_send_data (uart->other_uart,
			  &(uart->inBuffer[uart->curr_written_buffer_ptr]),
			  clen);

	  uart->curr_written_buffer_ptr = uart->curr_buffer_ptr;
	}

      if (res == 0)
	{
	  if (uart->is_ignored == FALSE)
	    {
              lmemcpy(uart->safeBuffer,uart->inBuffer,uart->curr_buffer_ptr);
              lsocket_provider_post(uart->socket_provider,uart->end);
              uart->has_data_in_buffer = TRUE;
	      //call_rcv_callback (uart);
	      //printf("\n");
	    }
	  uart->curr_buffer_ptr = 0;
          lthread_yield();          
	}

      if (uart->curr_buffer_ptr < uart->curr_written_buffer_ptr)
	{
	  uart->curr_written_buffer_ptr = 0;
	}

      if (res == 0 && uart->other_uart != NULL && uart->is_forwarding == TRUE)
	{
	  uart_send_endSequence (uart->other_uart);
	  //printf("Termination\n");
	}


      // determine if the data is forward able data.
      if (uart->curr_written_buffer_ptr == 0 &&
	  uart->curr_buffer_ptr == sizeof (simplePackageHdr_t) &&
          uart->pIds != NULL)
	{
	  struct simplePackageHdr_t *bf =
	    (struct simplePackageHdr_t *) uart->inBuffer;

	  if (!uart_tie_test_insert_id (*(uart->pIds), bf->id))
	    {
	      uart->is_forwarding = FALSE;
	      uart->is_ignored = TRUE;
	    }
	  else
	    {
	      uart->is_ignored = FALSE;
	      uart->is_forwarding = TRUE;
	      if (uart->other_uart != NULL)
		uart_send_startSequence (uart->other_uart);
	    }
	}
      if (uart->curr_buffer_ptr >= sizeof (uart->inBuffer))
	{
	  uart->curr_buffer_ptr = 0;
	  uart->is_forwarding = FALSE;
	  uart->is_ignored = TRUE;
	  uart->is_recieving_package = FALSE;
	  printf ("ERROR: Buffer overrun!!\n");
	}
    }

  return NULL;
}

static int
__write_to_socket (lsocket_provider_t *p, 
                   uint8_t protocol, 
                   object_t ptr,
                   object_t buffer, 
                   uint8_t size)
{
  uart_t *uart = static_cast<uart_t *>(ptr);
  if(uart->has_data_in_buffer == FALSE)
    return 0;

  uart->has_data_in_buffer = FALSE;
  assert(size < UART_BUFFER_SIZE);
  lmemcpy(buffer,uart->safeBuffer,size);
  return size;
}

static int
__read_from_socket (lsocket_provider_t *p, 
                    uint8_t protocol, 
                    object_t ptr,
                    object_t buffer, 
                    uint8_t size)
{
  uart_t *uart = static_cast<uart_t *>(ptr);
  assert(size < UART_BUFFER_SIZE);
  uart_send_message(uart,buffer,size);
  return size;
}

static bool_t 
__socket_poll (lsocket_provider_t *, 
                           uint8_t protocol, 
                           object_t ptr)
{
  uart_t *uart = static_cast<uart_t *>(ptr);
  return uart->has_data_in_buffer && protocol == uart->end;
}

static bool_t 
__socket_has_id(lsocket_provider_t *, 
                object_t ptr, 
                uint8_t protocol_id)
{
  uart_t *u = static_cast<uart_t *>(ptr);
  if(u->end == protocol_id)
    return TRUE;
  else
    return FALSE;
}


void
uart_initialize (uart_t *uart, uint8_t end)
{
  uart->socket_funcs.write_to_socket = __write_to_socket;
  uart->socket_funcs.read_from_socket = __read_from_socket;
  uart->socket_funcs.poll = __socket_poll;
  uart->socket_funcs.provides_protocol = __socket_has_id;

  uart->socket_provider = lsocket_create_provider (&uart->socket_funcs);
  uart->socket_provider->ptr = uart;

  uart->end = end;
  uart->curr_buffer_ptr = 0;
  uart->other_uart = NULL;
  uart->pIds = NULL;
  uart->is_forwarding = FALSE;
  uart->is_ignored = FALSE;
  uart->is_recieving_package = FALSE;
  uart->is_transmitting_package = FALSE;

  lthread_mutex_init(&uart->mutex);
  lthread_cond_init(&uart->condition);
  lthread_create (&uart->reader_thread, reader_routine, uart);
  
}
