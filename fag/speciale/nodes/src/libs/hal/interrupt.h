#ifndef __HAL_INTERRUPT_H_INCLUDED__
#define __HAL_INTERRUPT_H_INCLUDED__
/** @file interrupt.h
 * @brief Describes the main interrupt for the leg.
 *
 * Here the individual interrupts for the leg is described. So far it
 * will include a timer interrupt only since the normal serial
 * communication is performed through the ecos hal.
 */
#endif
