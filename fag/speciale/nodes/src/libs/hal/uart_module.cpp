/**
 * @file uart_module.c
 * @brief Handles the hardware implementation of the uarts.
 *
 * This includes handling of the special collision back off scheme
 * that is necesarry in order to prevent packages overflow the system.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <pkgconf/system.h>	/* which packages are enabled/disabled */
#ifdef CYGPKG_KERNEL
# include <pkgconf/kernel.h>
#endif

#ifdef CYGPKG_LIBC
# include <pkgconf/libc.h>
#endif

#ifdef CYGPKG_IO_SERIAL
# include <pkgconf/io_serial.h>
#endif

#ifndef CYGFUN_KERNEL_API_C
# error Kernel API must be enabled to build this file
#endif

#ifndef CYGPKG_LIBC_STDIO
# error C library standard I/O must be enabled to build this file
#endif

#ifndef CYGPKG_IO_SERIAL_HALDIAG
# error I/O HALDIAG pseudo-device driver must be enabled to build this file
#endif

#include "common.h"

#include "uart.h"
#include "utils/lobject.h"
#include "utils/lmemory.h"
//#include <pkgconf/io_serial.h>
#include <cyg/kernel/kapi.h>	/* All the kernel specific stuff */
#include <cyg/io/io.h>		/* I/O functions */
#include <cyg/hal/hal_arch.h>	/* CYGNUM_HAL_STACK_SIZE_TYPICAL */
#include <cyg/hal/plf_io.h>
#include <cyg/io/serialio.h>
#include <cyg/io/config_keys.h>
#include <sys/types.h>
#include <dirent.h>

#include "AT91SAM7S256.h"

#undef LOG_DUMP
#include "log.hpp"

#include <assert.h>

#if defined(ON_MODULE)

bool uart_tie_test_insert_id(uart_ids_t &u, cyg_uint16 id);

int
uart_send_data (struct uart_t *uart, void *data, unsigned size)
{
  uint8_t bufData1;

  uint32_t siz;
  uint8_t *cDP = static_cast<uint8_t *>(data);
  down (uart);

  for (uint16_t i = 0; i < size; i++)
    {
      bufData1 = *(cDP++);
      /*
      // remember to escape the data.
      if(bufData1 == 0x13 || bufData1 == 0x33 || bufData1 == 0x31) 
        {
          siz = 1;                
          unsigned char escape = 0x33;
          cyg_io_write (uart->handle, static_cast<void *> (&escape), &siz);
          bufData1 = ~bufData1;
          }
      */
      siz = 1;        
      cyg_io_write (uart->handle, static_cast<void *> (&bufData1), &siz);
    }
  up (uart);
  return 0;
}


CSPEC void uart_send_startSequence(struct uart_t *uart) 
{
  volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
  char bufData1 = 0x31;
  unsigned siz = 1;
  if(uart->end == 0) 
    {
      pPIO->PIO_SODR = 1<<7;
    } 
  else 
    {
      pPIO->PIO_SODR = 1<<24;
    }
  cyg_io_write (uart->handle, static_cast<void *> (&bufData1), &siz);
}

#define SEQ ((1 << 4) | (1 << 11) | (1 << 9))

void uart_send_endSequence(struct uart_t *uart) 
{
  volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;
  char bufData1 = 0x13;
  unsigned siz = 1;
  
  cyg_io_write (uart->handle, static_cast<void *> (&bufData1), &siz);
  //cyg_thread_delay(1);
  if(uart->end == 0) 
    {
      while((AT91C_BASE_US0->US_CSR & SEQ) != SEQ ) 
        {
          // do nothing
        }
      pPIO->PIO_CODR = 1<<7;
    } 
  else 
    {
      while((AT91C_BASE_US1->US_CSR & SEQ) != SEQ ) 
        {
          // do nothing
        }
      pPIO->PIO_CODR = 1<<24;
  }
}

void 
uart_read_data_direct(uart_t *handle,void *buffer, size_t *size) 
{
    cyg_io_read (uart->handle, (void *) buffer, &uiCommandLength);
}

int 
uart_readData(uart_t *uart) {
  cyg_uint32 uiCommandLength = 1;
  unsigned char buffer[1];
  //printf("Entry\n");
 readData_retry:
  uart_read_data_direct(uart, (void *) buffer, &uiCommandLength);
  //printf("$%X ",buffer[0]);
  //fflush(stdout);
  if(uart->is_transmitting_package == TRUE)
    goto readData_retry;

  if(buffer[0] == 0x31) {
    uart->curr_buffer_ptr = 0;
    uart->curr_written_buffer_ptr = 0;
    uart->is_recieving_package = TRUE;
    goto readData_retry;
  }

  if(buffer[0] == 0x13) 
    {
      uart->is_recieving_package = FALSE;
      return 0;
    }
  
  if(uart->is_recieving_package != TRUE)
    goto readData_retry;

  //printf("%d - buffer: 0x%X\n",uiCommandLength,buffer[0]);

  if(buffer[0] == 0x33) 
    {
      cyg_io_read (uart->handle, (void *) buffer, &uiCommandLength);
      buffer[0] = ~buffer[0];
    }  

  uart->inBuffer[uart->curr_buffer_ptr++] = buffer[0];
  return 1;
}

static void *
reader_routine (void *ptr)
{
  uart_t *uart = (struct uart_t *) ptr;
  //unsigned char buffer[1];

  //cyg_uint32 uiCommandLength;
  for (;;)
    {
      int res = readData(uart);
      printf("%d",res);
      fflush(stdout);
      // send currently lacking data..
      if(uart->is_forwarding == TRUE && 
         uart->curr_buffer_ptr > sizeof(simplePackageHdr_t) &&
         uart->other_uart != NULL &&
         res != 0 &&
         (uart->curr_buffer_ptr - uart->curr_written_buffer_ptr) > 0) 
        {
          unsigned clen = uart->curr_buffer_ptr - uart->curr_written_buffer_ptr;
          //printf("clen= %d, cp = %d, cw = %d\n",clen, uart->curr_buffer_ptr, uart->curr_written_buffer_ptr);
          uart_send_data(uart->other_uart,
                         &(uart->inBuffer[uart->curr_written_buffer_ptr]),
                         clen);

          uart->curr_written_buffer_ptr = uart->curr_buffer_ptr;
        }
      // if the data is to be locally connected and not ignored
      // call callback
//       if(uart->curr_buffer_ptr == uart->callback_size && uart->callback_size != 0) 
//         {

//         }

      if (res == 0) {
	if(uart->is_ignored == FALSE) {
	  call_rcv_callback(uart);
	  //printf("\n");
	}
	uart->curr_buffer_ptr = 0;
      }

      if(uart->curr_buffer_ptr < uart->curr_written_buffer_ptr)
        {
          uart->curr_written_buffer_ptr = 0;
        }

      if(res == 0 && 
         uart->other_uart != NULL && 
         uart->is_forwarding == TRUE) 
        {
          send_endSequence(uart->other_uart);
	  //printf("Termination\n");
        }


      // determine if the data is forward able data.
      if(uart->curr_written_buffer_ptr == 0 && 
         uart->curr_buffer_ptr == sizeof(simplePackageHdr_t)) 
        {
          struct simplePackageHdr_t *bf = (struct simplePackageHdr_t *)uart->inBuffer;
          /*if(bf->id & IS_GLOBAL)
            uart->is_forwarding = TRUE;
          else
            uart->is_forwarding = FALSE; */
          if(! test_insert_id(*(uart->pIds),bf->id)) 
	    {
	      uart->is_forwarding = FALSE;
	      uart->is_ignored = TRUE;
	    } 
	  else 
	    {
	      uart->is_ignored = FALSE;
	      uart->is_forwarding = TRUE;
	      if(uart->other_uart != NULL)
		send_startSequence(uart->other_uart);
	    }
        }
      if(uart->curr_buffer_ptr >= sizeof(uart->inBuffer)) 
        {
          uart->curr_buffer_ptr = 0;
          uart->is_forwarding = FALSE;
          uart->is_ignored = TRUE;
          uart->is_recieving_package = FALSE;
          printf("ERROR: Buffer overrun!!\n");
        }
    }

  return NULL;
}

CSPEC void uart_tie(struct uart_t *u1, struct uart_t *u2) 
{
  u1->other_uart = u2;
  u2->other_uart = u1;
  if(u1->pIds == NULL) {
    struct uart_ids_t *ids = (uart_ids_t *)lmalloc(sizeof(struct uart_ids_t));;

    lthread_mutex_init(&ids->test_mutex);
    ids->cp=0;
    ids->busy=0;

    for(int i=0;i<16;i++)
      ids->id[i] = 0xffff;
        
    u1->pIds = ids;
    u2->pIds = ids;
  }  
}

void uart_initialize(struct uart_t *u, uint8_t end);

/**
 * @brief Creates a UART object and connects to one of the uarts.
 *
 * The uart connection is being made to either /dev/ser0 or
 * /dev/ser1. For reference, the /dev/ser0 is the connection located
 * at the general board, and the /dev/ser1 is either the xbee module
 * or just the opposite (actuator) board.
 * <p>
 * The function creates a reader thread that is responsible for waking
 * all reader aware member functions. These should use the
 * #uart_register_recieve_callback() function.
 *
 * @see lobject.h
 */
CSPEC struct uart_t *
create_uart (const int end)
{
  uart_t *uart = lobject_create (struct uart_t);

  assert (end == 0 || end == 1);
  assert (uart != NULL);

  

  volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;

  if(end == 0) 
    {
      pPIO->PIO_PER = 1<<7;
      pPIO->PIO_OER = 1<<7;
    } 
  else 
    {
      pPIO->PIO_PER = 1<<24 | 1<<1;
      pPIO->PIO_OER = 1<<24 | 1<<1;
    }

  Cyg_ErrNo err;
  cyg_serial_info_t inf;
  cyg_uint32 len = sizeof (cyg_serial_info_t);

  if (end == 0)
    err = cyg_io_lookup ("/dev/ser0", &uart->handle);
  else
    err = cyg_io_lookup ("/dev/ser1", &uart->handle);

  if (ENOERR != err)
    {
      printf ("serial error for device %d\n", end);
      up (uart);
      return NULL;
    }
  cyg_io_get_config (uart->handle, CYG_IO_GET_CONFIG_SERIAL_INFO, &inf, &len);

  inf.word_length = CYGNUM_SERIAL_WORD_LENGTH_8;
  inf.baud = CYGNUM_SERIAL_BAUD_38400;
  inf.stop = CYGNUM_SERIAL_STOP_2;
  inf.parity = CYGNUM_SERIAL_PARITY_NONE;
  inf.flags  = 0x0000;

  len = sizeof (cyg_serial_info_t);
  err =
    cyg_io_set_config (uart->handle, CYG_IO_SET_CONFIG_SERIAL_INFO, &inf,
		       &len);

  // start a blocking reader thread that will stuff data in to the buffer.

  uart_initialize(uart, end);

  return uart;
}

/**
 * @brief Creates a XBee UART object and connects to one of the uarts.
 *
 * The uart connection is being made to either /dev/ser0 or
 * /dev/ser1. For reference, the /dev/ser0 is the connection located
 * at the general board, and the /dev/ser1 is either the xbee module
 * or just the opposite (actuator) board.
 * <p>
 * The function creates a reader thread that is responsible for waking
 * all reader aware member functions. These should use the
 * #uart_register_recieve_callback() function.
 *
 * @see lobject.h
 */
CSPEC struct uart_t *
create_xbee_uart ()
{
  uart_t *uart = lobject_create (struct uart_t);

  assert (uart != NULL);

  volatile AT91PS_PIO pPIO = AT91C_BASE_PIOA;

  pPIO->PIO_PER  = 1<<24 | 1<<1;
  pPIO->PIO_OER  = 1<<24 | 1<<1;
  pPIO->PIO_CODR = 1<<24 | 1<<1;

  Cyg_ErrNo err;
  cyg_serial_info_t inf;
  cyg_uint32 len = sizeof (cyg_serial_info_t);

  err = cyg_io_lookup ("/dev/ser1", &uart->handle);

  if (ENOERR != err)
    {
      printf ("Serial error for xbee device\n");
      up (uart);
      return NULL;
    }

  cyg_io_get_config (uart->handle, CYG_IO_GET_CONFIG_SERIAL_INFO, &inf, &len);

  inf.word_length = CYGNUM_SERIAL_WORD_LENGTH_8;
  inf.baud        = CYGNUM_SERIAL_BAUD_9600;
  inf.stop        = CYGNUM_SERIAL_STOP_1;
  inf.parity = CYGNUM_SERIAL_PARITY_NONE;
  inf.flags  = 0x0000;

  len = sizeof (cyg_serial_info_t);
  err =
    cyg_io_set_config (uart->handle, CYG_IO_SET_CONFIG_SERIAL_INFO, &inf,
		       &len);

  // start a blocking reader thread that will stuff data in to the buffer.

  uart_initialize(uart,1);

  return uart;
}


 /**
  * @brief Do nothing, just a leftover from uart_pc - but needed by leg.c
  *
  * @deprecated
  */
CSPEC void
uart_release_connection (struct uart_t *uart)
{
  // do nothing, just a leftover from uart_pc - but needed by leg.c
}

