/**
 * @file uart.h
 * @brief The function and structure overview for the implemtation of the uart.
 */
#ifndef __UARTIMPL_H_INCLUDED__
#define __UARTIMPL_H_INCLUDED__

#include "utils/lobject.h"
#include "utils/lsocket.h"

#if defined(ON_MODULE)
# include <cyg/kernel/kapi.h>	/* All the kernel specific stuff */
# include <cyg/io/io.h>		/* I/O functions */
# include <cyg/hal/hal_arch.h>	/* CYGNUM_HAL_STACK_SIZE_TYPICAL */
# include <cyg/io/serialio.h>
# include <cyg/io/config_keys.h>

# include <sys/types.h>
# include <dirent.h>
# include "utils/lthread.h"
# include "utils/lsocket.h"
#endif
#if defined(ON_BUILD_PLATFORM)
# include <stdlib.h>
#endif

#define UART_BUFFER_SIZE 255

BEGIN_CSPEC 

struct link;

typedef void (*uart_function_t) (void *ptr);

#define IS_GLOBAL (1<<0)
#define IS_XBEE (1<<1)

struct simplePackageHdr_t {
  uint16_t id;
} __attribute__ ((__packed__));

struct uart_ids_t {
  uint16_t id[16];
  uint8_t cp;
  lmutex_t test_mutex;
};

/**
 * @brief Direct representation of a single uart.
 * 
 * Holds the information for the uart. On the pc, it holds info for
 * connecting links to the node. For the module, it holds handles and
 * thread info for uart control.
 */
typedef struct uart
{
  LOBJECT;			/** see lobject for how to use this. */
  char safeBuffer[UART_BUFFER_SIZE];
  char inBuffer[UART_BUFFER_SIZE];
  //void *callback_ptr;
  //uart_function_t callback_fn;

  //unsigned callback_size;
  unsigned curr_buffer_ptr;
  unsigned curr_written_buffer_ptr;
  struct uart *other_uart;
  uint8_t end;			/** Indicates which connector this
                                    represents */

  lsocket_provider_t *socket_provider;
  lsocket_provider_funcs_t socket_funcs;

  struct uart_ids_t *pIds;

  volatile BOOL is_recieving_package;
  volatile BOOL is_transmitting_package;
  volatile bool_t has_just_sent;
  volatile BOOL is_forwarding;
  volatile BOOL is_ignored;
  volatile BOOL has_data_in_buffer;

  lthread_t reader_thread;	/** the thread that performs sleeping
                                    wait for a incomming data.  */

  lmutex_t mutex;
  lcondition_t condition;

#if defined (ON_BUILD_PLATFORM)
  struct link *link;
  void *node;
  uint8_t _buffer[UART_BUFFER_SIZE];
  int _buf_size;
  int _buf_read_ptr;
  int _buf_write_ptr;
#endif
#if defined (ON_MODULE)
  cyg_io_handle_t handle;	/** the actual handle to the serial
                                    port. */
  cyg_serial_info_t inf;	/** is this field actually needed? */  
#endif
} uart_t;


/** @brief Constructor for implementation of uart controller. */
uart_t *create_uart (int end);
uart_t *create_xbee_uart (void);

void uart_tie(uart_t *u1, uart_t *u2);

/** @brief Stuffs a piece of data into the out queue. */
int uart_send_message (uart_t *uart, object_t data, uint8_t size);

/** @brief copies data from the inbuffer to the location in "buffer". */
void uart_recieve_data (uart_t *uart, object_t data, uint8_t size);

void uart_release_connection (uart_t *uart);

void uart_signal_rcv (uart_t *uart);
END_CSPEC
#endif
