/**
 * @file uart_pc.c
 *
 * @brief implementation of simulated uarts.
 *
 * This approach is used when the system is being executed as part of
 * the modular framework on the build platform.
 *
 * @see uart.h
 */
#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "common.h"

#include "link/link.h"

#include "uart.h"
#include "utils/lobject.h"
#include "utils/lmemory.h"

#include <stdio.h>

#include <assert.h>


void uart_initialize(uart_t *uart, uint8_t end);
CSPEC void call_rcv_callback(uart_t* uart);

#ifdef ON_BUILD_PLATFORM

CSPEC int
uart_send_data(uart_t *uart, void *data, size_t size)
{
  //fprintf(stderr,"function %s Not implemented\n",__FUNCTION__);
  if(uart->link == NULL)
    return size;

  uint8_t bufData1;

  //uint32_t siz;
  uint8_t *cDP = static_cast<uint8_t *>(data);
  down (uart);

  for (uint16_t i = 0; i < size; i++)
    {
      bufData1 = *(cDP++);

      link_write_byte(uart->link, uart, bufData1);


      /** send one byte at a time */
    }
  up (uart);
  return size;
}


CSPEC void 
uart_send_startSequence(uart_t *uart) 
{
  char bufData1 = 0x31;
  unsigned siz = 1;

  uart_send_data (uart, static_cast<void *> (&bufData1), siz);
}

CSPEC void 
uart_send_endSequence(uart_t *uart) 
{
  char bufData1 = 0x13;
  unsigned siz = 1;

  uart_send_data (uart, static_cast<void *> (&bufData1), siz);
}


CSPEC void
uart_receive_byte(uart_t *uart, uint8_t data) {
  uart->_buffer[uart->_buf_write_ptr++] = data;
  uart->_buf_size++;

  if(uart->_buf_write_ptr == UART_BUFFER_SIZE)
    uart->_buf_write_ptr = 0;

  lthread_cond_signal(& uart->condition);
  //lthread_resume(& uart->reader_thread);
}

CSPEC void 
uart_read_data_direct(uart_t *uart, object_t buffer, size_t *uiCommandLength)
{
  lthread_mutex_lock(&uart->mutex);
  while(uart->_buf_size == 0)
    lthread_cond_wait(&uart->condition, &uart->mutex);
  
  lthread_mutex_unlock(&uart->mutex);

  if(*uiCommandLength == 1) 
    {
      uint8_t *data = (uint8_t *)buffer;
      *data = uart->_buffer[uart->_buf_read_ptr++];
      if(uart->_buf_read_ptr == UART_BUFFER_SIZE) 
        uart->_buf_read_ptr = 0;

      uart->_buf_size--;
    }
  else 
    {
      uint8_t *data = (uint8_t *)buffer;
      size_t siz = 1;
      for(uint16_t i = 0;i< *uiCommandLength;++i) 
        {
          uart_read_data_direct(uart,data++,&siz);
        }
    }
}

/**
 * @brief Creates a uart object for simulated run.
 *
 * The uart object is simply created and nothing more is done.
 *
 * @see lobject.h
 */
CSPEC uart_t *
create_uart (int end)
{
  uart_t *uart = lobject_create (struct uart);

  assert (end == 0 || end == 1);
  assert (uart != NULL);

  uart->_buf_read_ptr = 0;
  uart->_buf_size = 0;
  uart->_buf_write_ptr = 0;

  uart->node = NULL;
  uart_initialize(uart, end);
  return uart;
}


CSPEC void uart_release_connection(uart_t *uart) {
  down(uart);
  uart->node = NULL;
  up(uart);
}

CSPEC void 
uart_signal_rcv(uart_t *uart) 
{
  if(lobject_valid(uart) == FALSE)
    return;


  down(uart);
  
  // call_rcv_callback(uart);
  
  up(uart);
} 

#endif
