#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#if defined(ON_MODULE)
# error This file is not supposed to be implemented on the module
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/times.h>

#include <fstream>
#include <sstream>
#include <iostream>

#include "common.h"


#include "grid.hpp"
#include "nodetime.h"
#include "log.hpp"

#include "leg/cleg.hpp"
#include "link/node.hpp"

#include "leg/message.h"



#ifndef NULL
# define NULL 0L
#endif

using namespace std;


static void 
performHybridTest() {
  /* nodetime_initTime();
  grid::grid gr = grid::createTestGrid();

  LOG_FILE("start");

  gr.l[12]->setNetMember(10);
  gr.l[10]->buildHybridNet(10);
  usleep(1000000);
  LOG_FILE("stop"); */
}

int
main (int argc, char *argv[])
{

  log::init();
  performHybridTest ();
  return 0;

}
