#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "common.h"

#include "link/link.h"
#include "leg/leg.h"
#include "leg/endcoord.h"


#include "nodetime.h"
#include <iostream>
#include <assert.h>
#include <pthread.h>
#include <vector>
#include "log.hpp"

using namespace std;


volatile bool p = false;
volatile bool b = false;

void *test1(void *t) {
  char datan[] = {'a', 'b', 'c', 'd'};
  char data[] = {0,0,0,0};
  leg_t * leg1 = create_leg();
  leg_t * leg2 = create_leg();
  leg_t * leg3 = create_leg();
  leg_t * leg4 = create_leg();
  link_t * link = create_link();

  link_add_uart(link,leg1->uart[0]);
  link_add_uart(link,leg2->uart[1]);
  link_add_uart(link,leg3->uart[0]);
  link_add_uart(link,leg4->uart[0]);

  lsocket_t *socket1 = leg_seek_socket(leg1,0);
  lsocket_t *socket2 = leg_seek_socket(leg2,1);
  assert(socket1 != NULL);
  assert(socket2 != NULL);
  sleep(1);
  socket_write(socket1,datan,4);
  socket_waitfor(socket2);
  int res = socket_read(socket2,data,4);

  assert(res == 4);
  assert(data[0] == 'a');
  assert(data[1] == 'b');
  assert(data[2] == 'c');
  assert(data[3] == 'd');

  up(leg1);
  up(leg2);
  up(link);
  p = true;
  return NULL;
}

void *test2(void *t) {
  char datan[] = {'a', 'b', 'c', 'd'};
  char data[] = {0,0,0,0};
  leg_t * leg1 = create_leg_with_name("a1");
  leg_t * leg2 = create_leg_with_name("a2");
  leg_t * leg3 = create_leg_with_name("a3");
  leg_t * leg4 = create_leg_with_name("a4");
  link_t * link = create_link();

  link_add_uart(link,leg1->uart[0]);
  link_add_uart(link,leg2->uart[1]);
  link_add_uart(link,leg3->uart[0]);
  link_add_uart(link,leg4->uart[0]);

  leg_add_link_protocol(leg1,create_endcoord(leg1,0));
  leg_add_link_protocol(leg1,create_endcoord(leg1,1));

  leg_add_link_protocol(leg2,create_endcoord(leg2,0));
  leg_add_link_protocol(leg2,create_endcoord(leg2,1));

  leg_add_link_protocol(leg3,create_endcoord(leg3,0));
  leg_add_link_protocol(leg4,create_endcoord(leg4,0));

  lsocket_t *socket1 = leg_seek_socket(leg1,10);
  lsocket_t *socket2 = leg_seek_socket(leg2,20);
  assert(socket1 != NULL);
  assert(socket2 != NULL);
  lthread_msleep(100);
  socket_write(socket1,datan,4);
  assert(socket_waitfor(socket2) == 0);
  int res = socket_read(socket2,data,4);

  assert(res == 4);
  assert(data[0] == 'a');
  assert(data[1] == 'b');
  assert(data[2] == 'c');
  assert(data[3] == 'd');
  up(leg1);
  up(leg2);
  up(leg3);
  up(leg4);
  up(link);

  b = true;
  return NULL;
}

int main(int argc, char *argv[]) 
{
  /*
    nodetime_initTime();
  //lthread_t thread1;
  //lthread_create(&thread1, test1, NULL);

  lthread_t thread2;
  lthread_create(&thread2, test2, NULL);

  while(p | b == false)
    lthread_yield();
  */
  return 0;
}
