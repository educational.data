#include "common.h"
#include "leg/leg.h"

#include <cyg/kernel/kapi.h>
#include "hal/interrupt.h"
#include "nodetime.h"

struct leg_t *leg;

extern "C" void _impure_ptr() { }

extern "C"
void cyg_user_start(void) {
  nodetime_initTime();
  leg = create_leg();
}
