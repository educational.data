#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#if defined(ON_MODULE)
# error This file cannot be included on the module
#endif

#include "common.h"

#include "grid.hpp"
#include "leg/cleg.hpp"
#include "link/node.hpp"
#include <iostream>


namespace grid {
  using namespace std;

  grid createTestGrid() {
    vector < Node_tp >n;
    vector < Leg_tp >l;

    for (int i = 0; i <= 33; i++)
      {
        Leg_tp spLeg (new Leg ("lg", i));
        l.push_back (spLeg);
      }

    for (int i = 0; i <= 17; i++)
      {
        Node_tp spNode (new Node ("nd", i));
        n.push_back (spNode);
      }

    n[1]->addLeg (*l[1], 0).addLeg (*l[2], 0);
  
    n[2]->addLeg (*l[1], 1).addLeg (*l[3], 0);
  
    n[3]->addLeg (*l[2], 1).addLeg (*l[3], 1).
      addLeg (*l[4], 0).addLeg (*l[5], 0);

    n[4]->addLeg (*l[4], 1).addLeg (*l[9], 0).
      addLeg (*l[6], 0);
  
    n[5]->addLeg (*l[5], 1).addLeg (*l[6], 1).
      addLeg (*l[10], 0).addLeg (*l[11], 0).
      addLeg (*l[7], 0);

    n[6]->addLeg (*l[9], 1).addLeg (*l[19], 0).
      addLeg (*l[20], 0).addLeg (*l[16], 0).
      addLeg (*l[10], 1);

    n[7]->addLeg (*l[29], 0).addLeg (*l[26], 0).
      addLeg (*l[19], 1);

    n[8]->addLeg (*l[7], 1).addLeg (*l[12], 0).
      addLeg (*l[13], 0).addLeg (*l[8], 0);

    n[9]->addLeg (*l[11], 1).addLeg (*l[16], 1).
      addLeg (*l[21], 0).addLeg (*l[22], 0).
      addLeg (*l[17], 0).addLeg (*l[12], 1);

    n[10]->addLeg (*l[20], 1).addLeg (*l[26], 1).
      addLeg (*l[30], 0).addLeg (*l[31], 0).
      addLeg (*l[27], 0).addLeg (*l[21], 1);

    n[11]->addLeg (*l[29], 1).addLeg (*l[33], 0).
      addLeg (*l[30], 1);

    n[12]->addLeg (*l[8], 1).addLeg (*l[14], 0).
      addLeg (*l[15], 0);

    n[13]->addLeg (*l[13], 1).addLeg (*l[17], 1).
      addLeg (*l[23], 0).addLeg (*l[24], 0).
      addLeg (*l[18], 0).addLeg (*l[14], 1);

    n[14]->addLeg (*l[22], 1).addLeg (*l[27], 1).
      addLeg (*l[32], 0).
      addLeg (*l[28], 0).addLeg (*l[23], 1);

    n[15]->addLeg (*l[31], 1).addLeg (*l[33], 1).
      addLeg (*l[32], 1);

    n[16]->addLeg (*l[15], 1).addLeg (*l[18], 1).
      addLeg (*l[25], 0);

    n[17]->addLeg (*l[24], 1).addLeg (*l[28], 1).
      addLeg (*l[25], 1);

    grid gr;
    gr.n = n;
    gr.l = l;
    return gr;
  }

  grid createSimpleTestGrid() {
    vector < Node_tp >n;
    vector < Leg_tp >l;

    for (int i = 0; i <= 33; i++)
      {
        Leg_tp spLeg (new Leg ("lg", i));
        l.push_back (spLeg);
      }

    for (int i = 0; i <= 17; i++)
      {
        Node_tp spNode (new Node ("nd", i));
        n.push_back (spNode);
      }

    n[1]->addLeg (*l[1], 0).addLeg (*l[2], 0);
    n[2]->addLeg (*l[1], 1).addLeg (*l[3], 0);
    n[3]->addLeg (*l[2], 1).addLeg (*l[3], 1);

    grid gr;
    gr.n = n;
    gr.l = l;
    return gr;
  }
}
