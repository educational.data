#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#if defined(ON_MODULE)
# error This file is not supposed to be implemented on the module
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/times.h>

#include "common.h"

#include <fstream>
#include <sstream>
#include <iostream>

#include "grid.hpp"

#include "nodetime.h"
#define LOG_DUMP
#include "log.hpp"

#include "leg/cleg.hpp"
#include "link/node.hpp"
#include "hal/uart.h"

#include "leg/message.h"
#include "leg/endcoord.h"

#ifndef NULL
# define NULL 0L
#endif

using namespace std;
/*
static void
performGlobalTest ()
{
  nodetime_initTime();
  log::hlog(__FILE__) 
    << "*** preparing to create grid" << endl;

  grid::grid gr = grid::createTestGrid();
  //usleep(100000);
  
  lobject_ptr<struct uart_t> uart = create_uart(0);
  lobject_ptr<struct endcoord_t> ec = create_endcoord(uart);

  //SerialCommunicator sc(0);
  //EndCoordinator ec(sc);
  
  endcoord_start_own_thread(ec);
  gr.n[1]->addEndcoordinator(ec);
    
  log::hlog(__FILE__) 
    << "*** Requesting the network to create a network" << endl;

  message msg = createGlobalConnectionMessage();
  endcoord_sendMessage(ec,&msg);

  usleep(2000000);

  log::hlog(__FILE__) 
    << "*** Asking who is performing forwarding data" << endl;

  msg = createAskWhoIsForwardingMessage();
  endcoord_sendMessage(ec,&msg);
  usleep(200000);

  log::hlog(__FILE__) 
    << "*** Sending teardown" << endl;


  msg = createTearDownMessage();
  endcoord_sendMessage(ec,&msg);
  usleep(100000);

  log::hlog(__FILE__)  
    << "*** Is there anymore..." << endl;
}

static void
performSimpleGlobalTest ()
{
  nodetime_initTime();
  log::hlog(__FILE__) 
    << "*** preparing to create grid" << endl;

  grid::grid gr = grid::createTestGrid();
  //usleep(100000);
  
  lobject_ptr<struct uart_t> uart = create_uart(0);
  lobject_ptr<struct endcoord_t> ec = create_endcoord(uart);

  //SerialCommunicator sc(0);
  //EndCoordinator ec(sc);
  
  gr.n[1]->addEndcoordinator(ec);
    
  log::hlog(__FILE__) 
    << "*** Requesting the network to create a network" << endl;

  message msg = createGlobalConnectionMessage();
  endcoord_sendMessage(ec,&msg);

  usleep(100000);

  log::hlog(__FILE__) 
    << "*** Asking who is performing forwarding data" << endl;

  msg = createAskWhoIsForwardingMessage();
  endcoord_sendMessage(ec,&msg);
  usleep(100000);

  log::hlog(__FILE__) 
    << "*** Sending teardown" << endl;


  msg = createTearDownMessage();
  endcoord_sendMessage(ec,&msg);
  usleep(100000);

  log::hlog(__FILE__)  
    << "*** Is there anymore..." << endl;
}
*/
void quickAndDirty() {

  nodetime_initTime();

  Node n1("1"), n2("2"), n3("3");
  Leg l1("l1"), l2("l2"), l3("l3");
  n1.addLeg(l1,0).addLeg(l2,0);
  n2.addLeg(l1,1).addLeg(l3,0);    
  n3.addLeg(l3,1).addLeg(l2,1);    

  /*lobject_ptr<struct uart_t> uart = create_uart(0);
  lobject_ptr<struct endcoord_t> ec = create_endcoord(uart);
  endcoord_start_own_thread(ec);*/

  //SerialCommunicator sc(0);
  //EndCoordinator ec(sc);
  sleep(1);
}

int
main (int argc, char *argv[])
{
  // log::init();

  //performGlobalTest ();
  //performSimpleGlobalTest ();
  //quickAndDirty();


  return 0;

}
