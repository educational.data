#if ! defined(__LTHREAD_H__INCLUDED__)
#define __LTHREAD_H__INCLUDED__

#if defined(ON_BUILD_PLATFORM)
# include <pthread.h>
#endif

#if defined(ON_MODULE)
# include <cyg/hal/hal_arch.h>
# include <cyg/kernel/kapi.h>
# include <stdlib.h>
#endif



BEGIN_CSPEC

struct _lthread {
#if defined(ON_BUILD_PLATFORM)
  pthread_t thread;
  volatile uint8_t lockstate;
#endif
#if defined(ON_MODULE)
  int isSuspended;
  cyg_handle_t handle;
  cyg_thread thread;
  void *stack;
#endif
};

struct _lmutex {
#if defined(ON_BUILD_PLATFORM)
  pthread_mutex_t mutex;
#endif
#if defined(ON_MODULE)
  cyg_mutex_t mutex;
#endif
};

struct _lcondition {
#if defined(ON_BUILD_PLATFORM)
  pthread_cond_t condition;
#endif
#if defined(OM_MODULE)
  
#endif
};

typedef struct _lcondition lcondition_t;
typedef struct _lthread lthread_t;
typedef struct _lmutex lmutex_t;

typedef void *(*lroutine)(void*);

void lthread_yield(void);
void lthread_msleep(uint32_t miliseconds);
void lthread_create(lthread_t *thread, lroutine routine, void *arg);
void lthread_destroy(lthread_t *thread);
void lthread_exit(void * arg);
void lthread_join(lthread_t *thread);
// void lthread_suspend(lthread_t *thread);
// void lthread_resume(lthread_t *thread);

void lthread_mutex_init(lmutex_t *mutex);
void lthread_mutex_destroy(lmutex_t *mutex);
int lthread_mutex_lock(lmutex_t *mutex);
int lthread_mutex_unlock(lmutex_t *mutex);
lthread_t *lthread_current(void);;

void lthread_cond_init(lcondition_t *cond);
void lthread_cond_destroy(lcondition_t *cond);
void lthread_cond_wait(lcondition_t *cond,lmutex_t *mutex);
void lthread_cond_signal(lcondition_t *cond);
void lthread_cond_broadcast(lcondition_t *cond);



END_CSPEC

# if defined(__cplusplus)
class lmutex_lock {
private:
  lmutex_t *_mutex;
public:
  lmutex_lock(lmutex_t &mutex) {
    _mutex = &mutex;
    lthread_mutex_lock(_mutex);
  }
  ~lmutex_lock() {
    lthread_mutex_unlock(_mutex);
  }    
};
# endif

#endif
