#ifdef HAVE_CONFIG_H
# include "config.h"
#endif


#include "common.h"
#include "lobject.h"
#include "lmemory.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

#define TEST_LOBJ if (lobj->is_deleted == TRUE) { \
    fprintf(stderr,"%s: object of type %s has been deleted!\n",__FUNCTION__,lobj->t_name); \
    abort(); }

void 
__lobject_copy( lobject *lobj) {
  
}

BOOL 
__lobject_valid(struct __lobject_holder_t *obj) {
  if(obj == NULL)
    return FALSE;

  if(obj->lobj.is_deleted == TRUE)
    return FALSE;

  return TRUE;
}

void *
__create_lobject (unsigned size, char *type)
{
  struct __lobject_holder_t *obj = 
    (struct __lobject_holder_t *)lmalloc(size);

  struct lobject_t *lobj = &obj->lobj;

  lobj->size = size;
  lobj->l_created = TRUE;
  lobj->destructor = NULL;
  lobj->is_deleted = FALSE;
  lobj->ref_cnt = 0;
  lobj->object = obj;
  lobj->t_name = (char *)lmalloc(100);

  assert(lobj->t_name != NULL);
  strncpy(lobj->t_name,type,100);
  lobj->lobj = lobj;

  lobj->t_name[99] = 0;

  lthread_mutex_init(&lobj->mutex);

  __down_lobject(lobj);
  return obj;
}


void
__init_lobject (void *ptr, lobject *lobj, unsigned size, char *type)
{
  assert(ptr != NULL);
  assert(lobj != NULL);

  lobj->size = size;
  lobj->l_created = FALSE;
  lobj->destructor = NULL;
  lobj->is_deleted = FALSE;
  lobj->ref_cnt = 0;
  lobj->object = ptr;
  lobj->t_name = (char *)lmalloc(100);

  assert(lobj->t_name != NULL);
  strncpy(lobj->t_name,type,100);
  lobj->lobj = lobj;

  lthread_mutex_init(&lobj->mutex);

  __down_lobject(lobj);
}

void __lobject_set_destructor(lobject *lobj, lobject_destructor destructor) 
{
  assert(lobj != NULL);
  //TEST_LOBJ
  __down_lobject(lobj);
  lobj->destructor = destructor;
  __up_lobject(lobj);
}

char *deadMsg="Dead msg";

void
__up_lobject (lobject *lobj)
{

  assert (lobj != NULL);
  //TEST_LOBJ
    
  lthread_mutex_lock(&lobj->mutex);
  //
  // This test makes sure that functions that are using threads
  // will be able to let their thread functions complete
  // and destroy their data AFTER their destructor is called
  // but the destructor must not return before all resources is
  // released.
  //
  if (--(lobj->ref_cnt) == 0 && lobj->is_deleted == FALSE)
    {
      lobj->is_deleted = TRUE;
      lthread_mutex_unlock(&lobj->mutex);
    
      //printf("Calling destructor for a \"%s\" object\n", lobj->t_name);

      if (lobj->destructor != NULL)
	lobj->destructor (lobj->object);

      //printf("Now freeing a \"%s\" objects memory\n", lobj->t_name);

      lobj->t_name = deadMsg;
      lobj->ref_cnt = -10;
      //lfree (lobj->t_name);
      //lfree (lobj->object);
    }
  else
    lthread_mutex_unlock(&lobj->mutex);
}

void
__down_lobject (lobject *lobj)
{
  assert (lobj != NULL);
  assert (lobj->ref_cnt != -10);
  //TEST_LOBJ

  //lthread_mutex_lock(&lobj->mutex);
  ++(lobj->ref_cnt);
  //lthread_mutex_unlock(&lobj->mutex);
}


