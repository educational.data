#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "common.h"

#include "queue.h"
#include "lmemory.h"
#if defined(ON_BUILD_PLATFORM)
# include <assert.h>
#endif
//#include <deque>

//using namespace std;

struct queue {
private:
  void **ptr;
  unsigned head;
  unsigned tail;
  unsigned buffer_len;
  unsigned n_elements;
  
public:
  queue(const unsigned initial_size=10) {
    ptr = (void **)lmalloc(initial_size*sizeof(void *));
    buffer_len = initial_size;
    tail = 0;
    head = 0;
    n_elements = 0;
  }
  ~queue() {
    lfree(ptr);
  }
  void push(void *element) {
    if(buffer_len == n_elements+1)
      pop();

    *(ptr + (tail * sizeof(void *))) = element;
    if(tail == buffer_len-1) 
      tail = 0;
    else
      ++tail;

    n_elements++;
  }

  void *pop() {
#if defined(ON_BUILD_PLATFORM)
    assert(!empty());
#endif
    if(empty())
      return NULL;

    void *p = *(ptr + (head * sizeof(void *)));
    if(head == buffer_len-1)
      head = 0;
    else
      ++head;

    n_elements--;

    return p;
  }

  void *peek() {
#if defined(ON_BUILD_PLATFORM)
    assert(!empty());
#endif
    if(empty())
      return NULL;

    void *p = *(ptr + (head * sizeof(void *)));
    
    return p;
  }

  bool empty() const {
    return n_elements == 0;
  }
};

void
lqueue_create (lqueue *q)
{
  q->queue = new queue;
}

void
lqueue_destroy (lqueue *q)
{
  delete q->queue;
}

void *
lqueue_pop (lqueue *q)
{
  if (q->queue->empty ())
    return NULL;
  /*
  void *ptr = _q.back ();
  _q.pop_back ();
  */
  return q->queue->pop();
}

void *
lqueue_peek (lqueue *q)
{
  if (q->queue->empty ())
    return NULL;
  /*
  void *ptr = _q.back ();
  _q.pop_back ();
  */
  return q->queue->peek();
}

void
lqueue_push (lqueue *q, void *obj)
{
  q->queue->push (obj);

  //_q.push_front ( obj );
}

BOOL
lqueue_empty (lqueue *q)
{
  return q->queue->empty () ? TRUE : FALSE;
}
