#ifndef __LSOCKET_H_DEFINED__
#define __LSOCKET_H_DEFINED__

#include "common.h"
#include "lobject.h"
#include "array.h"
#include "lthread.h"

BEGIN_CSPEC 

typedef struct lsocket lsocket_t;
typedef struct lsocket_provider lsocket_provider_t;

typedef int (*read_write_fn) (lsocket_provider_t *, uint8_t protocol, object_t ptr, object_t buffer, uint8_t size);
typedef bool_t (*poll_fn) (lsocket_provider_t *, uint8_t protocol, object_t ptr);
typedef bool_t (*prov_prot_id_fn) (lsocket_provider_t *, object_t ptr, uint8_t protocol_id);
typedef bool_t (*create_socket_fn) (lsocket_provider_t *, uint8_t protocol, object_t ptr, void *args);
typedef void (*close_socket_fn) (lsocket_provider_t *, uint8_t protocol, object_t ptr);

/* 
 * @brief Interface for provider functionality
 *
 * Users of the lsocket_provider, must implement this structure and
 * provide it to the lsocket_provider_create function.
 */
typedef struct lsocket_provider_funcs
{
  /// called when the provider should write something TO the socket.
  read_write_fn write_to_socket;

  /// called when the provider should read some something FROM the socket.
  read_write_fn read_from_socket;

  /// if poll return false, it means there is no data available.
  poll_fn poll;

  /**
   * @brief request if protocol is available in this provider.
   *
   * Each provider may provider data to more than one protocol.
   * This indicates if the protocol can provide that information.
   */
  prov_prot_id_fn provides_protocol;

  /**
   * @brief Called when a client creates a new socket instance (NOT USED YET)
   *
   * It may also be called if the socket is copied.
   */
  create_socket_fn create_socket;

  /// Called when a client close connection to a socket instance (NOT USED YET)
  close_socket_fn close_socket;

} lsocket_provider_funcs_t;

/**
 * @brief Provider for all that provides data to other protocols/layers.
 *
 * The socket provider must be implemented if a protocol has data to
 * send to other layers. <p>
 *
 * The ptr should be set to any local data and will be used in every
 * provider functions.<p>
 *
 * If functions is not set, the creation is asserted.<p>
 * 
 * sockets is internal and should not be touched.<p>
 *
 * @see leg.h
 */
struct lsocket_provider
{
  LOBJECT;
  void *ptr;
  lsocket_provider_funcs_t *functions;

  larray_t sockets;
};


/**
 * @brief socket interface.
 * 
 * All data is internal and should not be touched.
 * In order to create this structure, use lsocket_create, 
 * or look at leg.h to use leg.h::Leg_seek_socket (preferred).
 */
struct lsocket
{
  LOBJECT;
  lsocket_provider_t *provider;
  struct lsocket *lower_socket;
  struct lsocket *higher_socket;

  lcondition_t condition;
  lmutex_t mutex;
  bool_t paused;

  uint8_t protocol;
  int errcode;
  uint16_t flags;
  uint32_t reserved;
};



///////////////////////////////////////////
//      Functions only for provider.
///////////////////////////////////////////

lsocket_provider_t *lsocket_create_provider (lsocket_provider_funcs_t *funcs);
void lsocket_close_provider (lsocket_provider_t *provider);
void lsocket_provider_connect_socket (lsocket_provider_t *provider, lsocket_t *socket);
lsocket_t *lsocket_create(uint16_t flags, uint8_t protocol);
bool_t lsocket_provider_post (lsocket_provider_t *provider, uint8_t protocol);
bool_t lsocket_provider_post_specific (lsocket_t *socket, uint8_t protocol, int err);
bool_t lsocket_provider_request_protocol_id(lsocket_provider_t *provider, int id);
bool_t lsocket_provider_has_protocol(lsocket_provider_t *provider, uint8_t protocol);



//////////////////////////////////////////
//      "normal" socket functions..
//////////////////////////////////////////

/// Sleep until a state change in the protocol is available.
int socket_waitfor (lsocket_t *socket);

/// Examine if there is any data available
int socket_poll (lsocket_t *socket);

/// Write data to socket. Sleeps while doing it.
int socket_write (lsocket_t *socket, object_t data, uint8_t size);

/// Read data from socket. Sleeps while doing it.
int socket_read (lsocket_t *socket, object_t data, uint8_t size);

END_CSPEC

#if defined (__cplusplus)
/**
 * @brief wrapper c++ functionality for lsocket.
 */
class LSocket {
  lsocket_t *_socket;
public:
  LSocket(lsocket *socket) 
  {
    _socket = socket;
    down(socket);
  }
  // cpy ctor.
  LSocket(const LSocket &socket) 
  {
    _socket = socket._socket;
    down(_socket);
  }
  // final class, no need to virtualize dtor.
  ~LSocket() {
    up(_socket);
  }
  
  int waitfor() {
    return socket_waitfor(_socket);
  }

  int poll() {
    return socket_poll(_socket);
  }
  
  int write(object_t data, const uint8_t size) 
  {
    return socket_write(_socket, data, size);
  }
  
  int read(object_t data, const uint8_t size) 
  {
    return socket_read(_socket, data, size);
  }
};
#endif

#endif
