/** 
 * @file lobject.h 
 * @brief object memory handling.
 * 
 * <h2>Description</h2>
 * In order to support some handling of objects in this framework, i
 * have defined this centric object. The object is inspired by the
 * object handling functionality known from the linux kernel. In this
 * implementation the lobject functionality handles reference
 * counting. Such counting gives the posibility to automaticly destroy
 * an object when the object is no longer in use and thereby hopefully
 * reduce the risk of memory leaks.
 *
 * <h3>Usage</h3> 
 * In order to let an object be a lobject type, the very first
 * statement in any structure *MUST* be an LOBJECT eg.
 *
 * <pre>
 *   struct thing {
 *      LOBJECT;
 *      ... /// own stuff
 *   };
 * </pre>
 *
 * Then when the first object is being initialised, it must use the
 * <tt>lobject_init(obj,type)</tt> function where obj is the object to
 * be initialised and type is the accourding type. When this is done,
 * registers are being reset and a default destructor is being added
 * to the structure.
 * 
 * Example:
 *
 * <pre>
 *   struct thing *theThing = lmalloc(sizeof(struct thing));
 *   lobject_init(theThing,struct thing);
 *   ... /// other initializations.
 * </pre>
 * 
 * 
 */
#ifndef __LOBJECT_H__
#define __LOBJECT_H__


# ifndef COMMON_H_INCLUDED
#  error common.h must be included prior to including this file
# endif

#include "lthread.h"
#include <assert.h>

BEGIN_CSPEC
struct lobject_t
{
  struct lobject_t *lobj; /// self pointer.
  char *t_name;
  unsigned size;
  volatile int ref_cnt;
  void (*destructor) (void *);
  BOOL l_created;
  BOOL is_deleted;
  void *object;
  lmutex_t mutex;
};

typedef struct lobject_t lobject;

#define LOBJECT lobject lobj;

struct __lobject_holder_t {
  LOBJECT;
};  


// #define lobject_create_new(type) 
//  (struct type*)__alloc_lobject_new(sizeof(struct type))

//#define lobject_create_new_with_destructor(type, destructor) 
//  (struct type*)__alloc_lobject_new_with_destructor(sizeof(struct type),destructor)

#define lobject_init(obj,type) \
  __init_lobject((void *)obj,&((obj)->lobj), sizeof(type), (#type))

#define lobject_create(type) \
  (type *)__create_lobject(sizeof(type), (char *)(#type))

// #define lobject_create_with_destructor(obj, type, destructor)
//  __create_lobject_with_destructor((void *)obj, sizeof(struct type), destructor)

#define lobject_copy_to

#define lobject_valid(obj) __lobject_valid((struct __lobject_holder_t *)obj)

#define down(object) \
  assert(object != NULL);\
  __down_lobject(&((struct __lobject_holder_t*)object)->lobj)

#define up(object) \
  assert(object != NULL);\
  __up_lobject(&((struct __lobject_holder_t*)object)->lobj)

#define up_lobject(object) \
  __up_lobject(object)

#define get_object(type,lobj) \
  ((type *)(((struct lobject_t *)lobj)->object))

#define get_lobject(obj) \
  (&(obj->lobj))

#define lobject_set_destructor(object, destructor) \
  __lobject_set_destructor(&object->lobj,(lobject_destructor)destructor)


struct lobject_t;
typedef void (*lobject_destructor) (void *);

// private versions of the programs - should not be used directly
//void *__lobject_copy (void *obj);
void __lobject_copy_to (void *dst, lobject *src);

void __lobject_set_destructor(lobject *lobj, lobject_destructor destructor);
void * __create_lobject (unsigned size, char *type);
void __init_lobject (void *obj, lobject *lobj, unsigned size, char *type);

void __down_lobject (lobject *lobj);
void __up_lobject (lobject *object);

BOOL __lobject_valid(struct __lobject_holder_t *obj);

END_CSPEC

# if defined(__cplusplus)
template <typename T>
class lobject_ptr {
private:
  T* _p;
public:
  typedef T* T_ptr;
  typedef T& T_ref;

  lobject_ptr() {
    _p = NULL;
  }
  // we asume that this ptr is already down by one.
  lobject_ptr(T* ptr) {
    assert(ptr != NULL);
    down(ptr);
    _p = ptr;
  }
  lobject_ptr(const lobject_ptr<T> &obj) {    
    _p = obj._p;
    if(_p != NULL)
      down(_p);
  }
  lobject_ptr(T& obj) {    
    _p = &obj;
    if(_p != NULL)
      down(_p);
  }
  ~lobject_ptr() {
    if(_p != NULL)
      up(_p);
  }
  T_ptr operator->() {
    return _p;
  }
  lobject_ptr<T> &operator=(const lobject_ptr<T> &ref) {
    if(_p != NULL)
      up(_p);

    _p = ref._p;
    if(_p != NULL)
      down(_p);
    return *this;
  }

  lobject_ptr<T> &operator=(T* ref) {
    if(_p != NULL)
      up(_p);

    _p = ref;
    if(_p != NULL)
      down(_p);
    return *this;
  }
  T_ptr pointer() {
    return _p;
  }
  operator T_ptr() {
    return _p;
  }

  T& ref() {
    return *_p;
  }
};
# endif

#endif
