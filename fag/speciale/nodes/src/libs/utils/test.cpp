#ifdef HAVE_CONFIG_H
# include "config.h"
#else
# error I depend on config.h
#endif

#include "common.h"

#include "crc32.h"
#include "array.h"
#include "queue.h"
#include "lrandom.h"
#include "stdio.h"
#include <assert.h>
#include "lobject.h"
#include "lmemory.h"
#include "lsocket.h"

void test_array() {
  larray_t array;

  char message1[] = "Buuh";
  char message2[] = "Baa";

  larray_create (&array);
  assert(larray_buffersize(&array) == 1);
  larray_insert (&array, message1);
  larray_insert (&array, message2);
  assert (larray_get (&array, 0) == message1);
  assert (larray_get (&array, 1) == message2);

  assert (larray_contains (&array, message1) == TRUE);
  assert (larray_contains (&array, message2) == TRUE);
  assert (larray_size (&array) == 2);
  assert(larray_buffersize(&array) == 11);

  assert (larray_contains(&array, message1) == TRUE);
  larray_remove (&array, message1);
  assert (larray_contains(&array, message1) == FALSE);
  assert (larray_get (&array, 0) == message2);
  assert (larray_size(&array) == 1);
  larray_clear(&array);
  assert (larray_size(&array) == 0);
  larray_destroy (&array);
}

void test_queue() {
  char message1[] = "Buuh";
  char message2[] = "Baa";

  lqueue q;

  lqueue_create (&q);
  lqueue_push (&q, message1);
  lqueue_push (&q, message2);

  assert (lqueue_empty(&q) == FALSE);
  assert (lqueue_pop (&q) == message1);
  assert (lqueue_pop (&q) == message2);
  assert (lqueue_empty(&q) == TRUE);
  assert (lqueue_pop (&q) == NULL);
  lqueue_destroy (&q);
}

void test_rand() 
{
  int i;
  for(i = 0;i< 20;i++) {
    printf("%d ",(int)(lrandom() % 100));
  }
  printf("\n");
}


struct test_object_t {
  LOBJECT;
  int i;
};

int test_val = 0;
void destroy_test_object(struct test_object_t *obj) {
  test_val = 1;
  lfree(obj);
}

void test_lobject() 
{
  struct test_object_t *obj = lobject_create(struct test_object_t);

  test_val=0;
  lobject_set_destructor(obj, (lobject_destructor)&destroy_test_object);
  

  assert(test_val == 0);
  up(obj);
  assert(test_val == 1);
}

BOOL retval = FALSE;
int wc = 0;
int rc = 0;
int pc = 0;

int 
socket_writer(lsocket_provider_t *p, 
              uint8_t protocol, 
              object_t ptr, 
              object_t buf, 
              uint8_t size) 
{
  wc++;
  return 0;
}

int 
socket_reader(lsocket_provider_t *p, 
              uint8_t protocol, 
              object_t ptr, 
              object_t buf, 
              uint8_t size) 
{
  rc++;
  return 0;
}

BOOL poll(lsocket_provider_t *p, 
          uint8_t protocol, 
          object_t ptr) 
{
  pc++;
  return FALSE;
}

struct lsocket_provider_funcs funcs;

void test_socket() 
{
  assert(wc == 0);
  assert(rc == 0);
  assert(pc == 0);
  struct lsocket_provider *provider =
    lsocket_create_provider(NULL);
  assert(provider == NULL);

  funcs.write_to_socket = socket_writer;
  funcs.read_from_socket = socket_reader;
  funcs.poll = poll;  
  
  provider = lsocket_create_provider(&funcs);
  provider->ptr = NULL;

  retval = FALSE;
  struct lsocket *sock = lsocket_create(0,0);
  assert(sock != NULL);

  assert(socket_write(sock,NULL,0) < 0);
  assert(rc == 0);
  assert(socket_read(sock,NULL,0) < 0);
  assert(wc == 0);

  lsocket_provider_connect_socket(provider,sock);

  assert(socket_write(sock,NULL,0) == 0);
  assert(rc == 1);
  assert(socket_read(sock,NULL,0) == 0);
  assert(wc == 1);

  up(sock);
  
  up(provider);
}

int
main (int argc, char *argv)
{
  test_array();
  test_queue();
  //test_rand();
  

  test_lobject();

  test_socket();

  return 0;
}
