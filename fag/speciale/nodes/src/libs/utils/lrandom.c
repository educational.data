#ifdef HAVE_CONFIG_H
# include "config.h"
#else
# error Cannot compile without config.h
#endif

#include "common.h"
#include "utils/lrandom.h"


static unsigned long idum = 0;

unsigned long 
lrandom(void ) 
{
  idum = 1664525L*idum + 1013904223L;
  return idum;
}

void 
lseed(unsigned long seed)
{
  idum = seed;
}
