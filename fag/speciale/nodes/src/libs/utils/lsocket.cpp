#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "lsocket.h"
#include <errno.h>
#include "lmemory.h"
#include "stdio.h"

#define LOG_DUMP
#include "log.hpp"

static void
close_provider (lsocket_provider_t *p)
{
  int c = larray_size (&p->sockets);
  for (int i = 0; i < c; ++i)
    {
      lsocket_t *sock = (struct lsocket *) larray_get (&p->sockets, i);

      // close any pending operations
      lsocket_provider_post_specific (sock, 0, -ENETRESET);
      sock->provider = NULL;
    }
  larray_destroy (&p->sockets);

  lfree (p);
}

CSPEC lsocket_provider_t *
lsocket_create_provider (lsocket_provider_funcs_t *funcs)
{
  if (funcs == NULL)
    return NULL;
  lsocket_provider_t *p = lobject_create (lsocket_provider_t);
  p->functions = funcs;
  larray_create (&p->sockets);

  lobject_set_destructor (p, (lobject_destructor) close_provider);

  return p;
}

static void 
lsocket_provider_disconnect_socket(lsocket_t *socket) {
  down(socket);

  larray_remove(&socket->provider->sockets, socket);
  socket->provider = NULL;
  up(socket);

  up(socket);
}

static void 
lsocket_disconnect(lsocket_t *socket) 
{
  lsocket_t *lower = socket->lower_socket;
  socket->lower_socket->higher_socket = NULL;
  socket->lower_socket = NULL;

  up(lower);
  up(socket);  
}

CSPEC void 
lsocket_connect(lsocket_t *lower, lsocket_t *upper) 
{
  down(lower);
  down(upper);
  upper->lower_socket = lower;
  lower->higher_socket = upper;
  if(upper->provider != NULL)
    lsocket_provider_disconnect_socket(upper);
}


CSPEC void
lsocket_provider_connect_socket (lsocket_provider_t *provider,
                                 lsocket_t *socket)
{
  down(provider);
  down(socket);
  if(socket->provider != NULL)
    lsocket_provider_disconnect_socket(socket);

  if(socket->lower_socket != NULL)
    lsocket_disconnect(socket);

  down(socket);
  socket->provider = provider;
  larray_insert (&provider->sockets, socket);
  up(socket);
  up(provider);
}

CSPEC bool_t
lsocket_provider_post_specific (lsocket_t *socket, uint8_t protocol, int val)
{
  down(socket);

  if (socket->paused == true && 
      (socket->protocol == protocol || protocol == 0))
    {
      socket->errcode = val;
      lthread_cond_signal (&socket->condition);
      up(socket);
      return TRUE;
    }
  else
    {
      up(socket);
      return FALSE;
    }
}

CSPEC bool_t
lsocket_provider_has_protocol(lsocket_provider_t *provider, 
                              uint8_t protocol) 
{
  if(protocol == 0)
    return TRUE;
  down(provider);
  for(int i = 0;i< larray_size(&provider->sockets);i++) 
    {
      lsocket_t *sock = (lsocket_t *)larray_get(&provider->sockets,i);
      if(sock->protocol == protocol)
        {
          up(provider);
          return TRUE;
        }
    }
  down(provider);
  return FALSE;
}

CSPEC bool_t
lsocket_provider_post (lsocket_provider_t *provider, uint8_t protocol)
{
  down(provider);
  for (int i = 0; i < larray_size (&provider->sockets); ++i)
    {
      lsocket_t *s =
	(lsocket_t *) larray_get (&provider->sockets, i);
      if(lsocket_provider_post_specific (s, protocol, 0))
        {
          up(provider);
          return TRUE;
        }
    }
  up(provider);
  return FALSE;
}

/// Shall we enter a sleep state?
/// we can only do that if the protocol implement a poll function.
static bool
__test_waitfor (lsocket_t *socket)
{
  if (socket->provider->functions->poll != NULL)
    {
      if (socket->provider->functions->
	  poll (socket->provider, socket->protocol,
		socket->provider->ptr) == TRUE)
	return false;
      else
	return true;
    }
  else
    return false;
}

/// Are there more data to be read?
/// needs to have the poll function implemented, otherwise it will say no.
static bool
__test_moredata (lsocket_t *socket)
{
  if (socket->provider->functions->poll != NULL)
    {
      if (socket->provider->functions->
	  poll (socket->provider, socket->protocol,
		socket->provider->ptr) == TRUE)
        {
          return true;
        }
      else
        {
          return false;
        }
    }
  else
    {
      return false;
    }
}



CSPEC int
socket_waitfor (lsocket_t *socket)
{
  down(socket);
  lthread_mutex_lock(&socket->mutex);
  if (socket->provider == NULL)
    {
      errno = ECONNABORTED;
      up(socket);
      return -ECONNABORTED;
    }

  while (__test_waitfor (socket))
    {
      // we must go into sleep state
      socket->paused = TRUE;
      lthread_cond_wait(&socket->condition,&socket->mutex);
      socket->paused = FALSE;

      // we come back here when we are awoken by
      // lsocket_provider_post_specific
    }

  if (socket->errcode != 0)
    {
      int res = socket->errcode;
      socket->errcode = 0;
      errno = res;
      lthread_mutex_unlock(&socket->mutex);
      up(socket);
      return -res;
    }
  lthread_mutex_unlock(&socket->mutex);
  up(socket);
  return 0;
}

CSPEC int
socket_write (lsocket_t *socket, object_t data, uint8_t size)
{
  down(socket);
  if (socket->provider == NULL)
    {
      errno = ECONNABORTED;
      up(socket);
      return -ECONNABORTED;
    }
  errno = 0;

  if (socket->provider->functions->read_from_socket == NULL)
    {
      errno = EPROTO;
      up(socket);
      return -EPROTO;
    }
  up(socket);
  read_write_fn read = socket->provider->functions->read_from_socket;
  return
    read (socket->provider,
          socket->protocol,
          socket->provider->ptr,
          data, size);
}


CSPEC int
socket_read (lsocket_t *socket, object_t data, uint8_t size)
{
  down(socket);
  if (socket->provider == NULL)
    {
      errno = ECONNABORTED;
      up(socket);
      return -ECONNABORTED;
    }
  errno = 0;

  if (socket->provider->functions->write_to_socket == NULL)
    {
      errno = EPROTO;
      up(socket);
      return -EPROTO;
    }
  read_write_fn write_to_socket = socket->provider->functions->write_to_socket;

  int res = write_to_socket (socket->provider,
                             socket->protocol,
                             socket->provider->ptr,
                             data,
                             size);

  if (__test_moredata (socket))
    {
      lsocket_provider_post (socket->provider, socket->protocol);
    }
  up(socket);
  return res;
}

static void
socket_close (lsocket_t *socket)
{
  socket->provider = NULL;
  lsocket_provider_post_specific (socket, 0, -ENETRESET);
  lthread_mutex_destroy(&socket->mutex);
  lthread_cond_destroy(&socket->condition);
}

CSPEC lsocket_t * 
lsocket_create(uint16_t flags, uint8_t protocol) 
{
  lsocket_t *socket = lobject_create(lsocket_t);
  lobject_set_destructor(socket,(lobject_destructor)socket_close);
  lthread_mutex_init (&socket->mutex);
  lthread_cond_init(&socket->condition);  
  socket->flags = flags;
  socket->protocol = protocol;
  socket->paused = FALSE;
  return socket;
}


