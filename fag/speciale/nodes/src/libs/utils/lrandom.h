#ifndef __LRANDOM_H__
#define __LRANDOM_H__

BEGIN_CSPEC

void lseed(unsigned long seed);
unsigned long lrandom(void);

END_CSPEC
#endif
