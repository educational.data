#ifndef __ARRAY_H__
# define __ARRAY_H__

# if ! defined(COMMON_H_INCLUDED)
#  error common.h must be included prior to including this file
# endif

/** @file array.h
 * @brief A simple array function.
 *
 * The array is a random acces growable array implementation. 
 * that means that there must be added a series of objects before
 * refering to an id in the array.
 * But this is off course on thing that will be reconsidered in a
 * later version.
 *
 * The array is quarenteed to have equal access time, but may take
 * longer for adding since there may be a need for growing the array.
 *
 * The user should be aware that he is solely responsible for freing
 * the objects that are contained within this array. The
 * #array_remove() function only removes the object reference and
 * decreases the size of the array.
 */

# if defined(__cplusplus)
extern "C" {
# endif
  
  typedef struct larray {
    void *reserved;
  } larray_t;
  
  void     larray_create( larray_t *array );
  void     larray_destroy (larray_t *array );
  
  void     larray_insert( larray_t *array, object_t object );
  int      larray_size( larray_t *array );
  int      larray_remove( larray_t *array, object_t object );
  object_t larray_get( larray_t *array, uint16_t id );
  bool_t   larray_contains( larray_t *array, object_t object );
  void     larray_clear ( larray_t *array );
  unsigned larray_buffersize( larray_t *array );
  
# if defined(__cplusplus)
}
# endif

#endif
