#if defined(HAVE_CONFIG_H)
# include "config.h"
#endif

#include "common.h"
#include "array.h"
#include "lmemory.h"
#include "lthread.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

// #if defined (ON_BUILD_PLATFORM)
// #include <vector>
// #include <algorithm>
// using namespace std;

// #define VEC_DECL vector<void *> &vec = *((vector<void *>*)array->reserved)

// CSPEC void
// array_create (struct array_t *array)
// {
//   //array->reserved = new array_private_t;
//   array->reserved = new vector < void *>();
// }

// CSPEC void
// array_destroy (struct array_t *array)
// {
//   delete (vector < void *>*) (array->reserved);
// }

// CSPEC void
// array_insert (struct array_t *array, void *val)
// {
//   VEC_DECL;
//   vec.push_back (val);
// }

// CSPEC int
// array_size (struct array_t *array)
// {
//   VEC_DECL;
//   return vec.size ();
// }

// CSPEC void
// array_clear (struct array_t *array)
// {
//   VEC_DECL;
//   vec.clear();
// }



// CSPEC int
// array_remove (struct array_t *array, void *obj)
// {
//   VEC_DECL;
//   for (vector < void *>::iterator it = vec.begin (); it != vec.end (); it++)
//     if (*it == obj)
//       {
// 	vec.erase (it);
// 	return TRUE;
//       }
//   return FALSE;
// }

// CSPEC
// BOOL array_contains(struct array_t *array, void *data) 
// {
//   VEC_DECL;

//   return find(vec.begin(),vec.end(),data) != vec.end() ? TRUE : FALSE;
// }

// CSPEC void *
// array_get (struct array_t *array, unsigned int id)
// {
//   VEC_DECL;

//   if (id >= vec.size ())
//     return NULL;

//   return vec[id];
// }



// #elif defined(ON_MODULE)

class arrayBuffer {
  volatile unsigned n_elements;
  volatile unsigned _size;
  void **ptr;
  lmutex_t mutex;

  volatile void grow(unsigned buf_size = 10) {
    lthread_mutex_lock(&mutex);
    void **np = (void **)lmalloc(((_size + buf_size) * sizeof(void *)));
    assert(np != NULL);
    lmemcpy((void *)np,(void *)ptr,_size);
    lfree(ptr);
    ptr = np;
    _size += buf_size;
    lthread_mutex_unlock(&mutex);
  } 

public:
  arrayBuffer(unsigned initialSize=1) {
    ptr = (void **)lmalloc(sizeof(void*) * initialSize);
    n_elements = 0;
    _size = initialSize;
    lthread_mutex_init(&mutex);
  }
  ~arrayBuffer() {
    lthread_mutex_lock(&mutex);
    lfree(ptr);
    lthread_mutex_unlock(&mutex);
    lthread_mutex_destroy(&mutex);
  }
  void clear() {
    lthread_mutex_lock(&mutex);
    n_elements = 0;
    lthread_mutex_unlock(&mutex);
  }

  volatile void remove(unsigned id) {
    lthread_mutex_lock(&mutex);
    --n_elements;
    assert(id <= n_elements);      

    //printf("Trying to remove %d\n", id);
    
    for(unsigned i = id;i<n_elements;++i) 
      {
	void **ref1 = (void **)ptr+i;
	void **ref2 = (void **)ptr+(i+1);
	*ref1 = *ref2;
      }
    lthread_mutex_unlock(&mutex);
  }

  unsigned buffersize() {
    return _size;
  }

  bool remove(void *element) {
    for(unsigned i =0; i < n_elements;++i)
      {
	assert(i < _size);
        if(*(ptr+i) == element)
          {
            remove(i);
            return true;
          }
      }
    return false;
  }

  volatile bool contains(void *element) {
    lthread_mutex_lock(&mutex);
    for(unsigned i =0; i < n_elements;i++)
      {
	void ** ref = (void **)(ptr + i);
	//printf("testing %lx - value: %lx\n", ref,*ref);
	assert(ref != NULL);
	//assert(*ref != NULL);
	
        if(*ref == element)
	  {
	    lthread_mutex_unlock(&mutex);
	    return true;
	  }
      }
    lthread_mutex_unlock(&mutex);
    return false;
  }

  void test_elements() {
    for(unsigned i =0; i < n_elements;i++)
      {
	//printf("I=%d\n",i);
	//assert(*(ptr+i) != NULL);
      }
  }

  volatile void push_back(void *element) {
    if(_size == n_elements + 1)
      grow();

    assert(element != NULL);
    
    lthread_mutex_lock(&mutex);
    void **ref = (void **)(ptr+n_elements);
    //printf("set %lx val %lx size=%d,elements=%d\n",ref,element,_size,n_elements);
    *ref = element;
    //void ** ref2 = (void **)(ptr + n_elements);
    //printf("retrieving: from %lx value %lx\n",ref2, *ref2);
    n_elements++;
    //lthread_mutex_unlock(&mutex);

    //lthread_mutex_lock(&mutex);
    /* assert(*(ptr + (n_elements-1)*sizeof(void *)) == element);
       assert(*(ptr + (n_elements-1)*sizeof(void *)) != NULL); */
    test_elements();
    lthread_mutex_unlock(&mutex);
    
  }
  void * operator[](const unsigned id) {
    if(id >= n_elements)
      return NULL;
    else
      return *(ptr+id);
  }

  unsigned size() {
    return n_elements;
  }
};

#define VEC_DECL arrayBuffer &vec = *((arrayBuffer*)array->reserved)


CSPEC void
larray_create (larray_t *array)
{
  array->reserved = new arrayBuffer();
}

CSPEC void
larray_destroy (larray_t *array)
{
  delete (arrayBuffer *) (array->reserved);
}
  
CSPEC void
larray_insert (larray_t *array, object_t val)
{
  VEC_DECL;
  vec.push_back (val);
}

CSPEC unsigned
larray_buffersize (larray_t *array)
{
  VEC_DECL;
  return vec.buffersize();
}
  
CSPEC int
larray_size (larray_t *array)
{
  VEC_DECL;
  return vec.size ();
}
  
CSPEC void
larray_clear (larray_t *array)
{
  VEC_DECL;
  vec.clear();
}
  
  
  
CSPEC int
larray_remove (larray_t *array, object_t obj)
{
  VEC_DECL;
    
  return vec.remove(obj) ? TRUE : FALSE;
}
  
CSPEC BOOL 
larray_contains(larray_t *array, object_t data) 
{
  VEC_DECL;
    
  return vec.contains(data) ? TRUE : FALSE;
}
  
CSPEC object_t
larray_get (larray_t *array, uint16_t id)
{
  VEC_DECL;
    
  if (id >= vec.size ())
    return NULL;
    
  return vec[id];
}

// #else
// #error WHAT AM I COMPILING FOR??? I DO NOT KNOW
// #endif
