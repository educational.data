#if defined (HAVE_CONFIG_H)
# include "config.h"
#endif

#include "common.h"
#include "lthread.h"
#include "lmemory.h"
#include "array.h"

#include "stdio.h"
#include <assert.h>

static larray_t __array_threads;
static bool_t __threads_has_init = FALSE;

static void __init_thread_stack(void) {
  if(__threads_has_init == FALSE) 
    {
      larray_create(&__array_threads);
      __threads_has_init = TRUE;
    }
}

#if defined(ON_BUILD_PLATFORM)

#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void pthread_yield(void);

void lthread_yield(void) 
{
  pthread_yield();
}

void lthread_msleep(uint32_t miliseconds) {
  usleep(1000 * miliseconds);
}

void lthread_create(lthread_t *thread, lroutine routine, void *arg)
{

  __init_thread_stack();

  larray_insert(&__array_threads,thread);

  //pthread_t *pthread;
  // thread->pthread = lmalloc(sizeof(pthread_t));
  //pthread = (pthread_t *)(thread->reserved);
  memset(& thread->thread,0,sizeof(pthread_t));
  pthread_create(& thread->thread, NULL,routine,arg);
}

void lthread_exit(void * arg) 
{
  pthread_exit(arg);
}

void lthread_join(lthread_t *thread)
{
  pthread_join(thread->thread,NULL);
}

void lthread_destroy(lthread_t *thread) 
{
  //lfree(thread->reserved);
}

void lthread_suspend(lthread_t *thread) {
  assert(thread != NULL);
  thread->lockstate = 1;
  while(thread->lockstate != 0)
    lthread_yield();
}

void lthread_resume(lthread_t *thread) {
  thread->lockstate = 0;
}

lthread_t *lthread_current(void) 
{
  pthread_t st = pthread_self();
  int i;
  for(i=0;i<larray_size(&__array_threads);++i) 
    {
      lthread_t *thr = (lthread_t *)larray_get(&__array_threads,i);
      assert(thr != NULL);
      pthread_t ct = thr->thread;
      if(pthread_equal(ct, st) != 0)
        return thr;
    }
  return NULL;
}

void lthread_cond_init(lcondition_t *cond) 
{
  pthread_cond_init(&cond->condition, NULL);
}
void lthread_cond_destroy(lcondition_t *cond) 
{
  pthread_cond_destroy(&cond->condition);
}
void lthread_cond_wait(lcondition_t *cond,lmutex_t *mutex) 
{
  pthread_cond_wait(&cond->condition,&mutex->mutex);
}
void lthread_cond_signal(lcondition_t *cond) 
{
  pthread_cond_signal(&cond->condition);
}
void lthread_cond_broadcast(lcondition_t *cond) 
{
  pthread_cond_broadcast(&cond->condition);
}

void lthread_mutex_init(lmutex_t *mutex)
{
  //mutex->pthread = lmalloc(sizeof(pthread_mutex_t));
  pthread_mutexattr_t mattr;
  pthread_mutexattr_init (&mattr);
  //pthread_mutexattr_settype (&mattr, PTHREAD_MUTEX_RECURSIVE);
  pthread_mutex_init (& mutex->mutex, &mattr);
  pthread_mutexattr_destroy (&mattr);
}

void lthread_mutex_destroy(lmutex_t *mutex)
{
  pthread_mutex_destroy(& mutex->mutex);
  //lfree(mutex->mutex);
}

int lthread_mutex_lock(lmutex_t *mutex)
{
  return pthread_mutex_lock(& mutex->mutex);
}

int lthread_mutex_unlock(lmutex_t *mutex)
{
  return pthread_mutex_unlock(& mutex->mutex);
}

#endif

#if defined (ON_MODULE)

# include <cyg/kernel/kapi.h>

void lthread_yield(void) 
{
  cyg_thread_yield();
}

void lthread_msleep(uint32_t miliseconds) {
  cyg_sleep(miliseconds);
}

#define STACK_SIZE 4096

void lthread_create(lthread_t *thread, lroutine routine, void *arg)
{
  thread->stack = lmalloc(STACK_SIZE);
  cyg_thread_create((cyg_addrword_t)10,
                    (cyg_thread_entry_t*)routine,
                    (cyg_addrword_t)arg,
                    "runner",
                    thread->stack,
                    STACK_SIZE,
                    &thread->handle,
                    &thread->thread);

  larray_insert(&__array_threads,thread);

  thread->isSuspended = FALSE;

  cyg_thread_resume(thread->handle);
}

void lthread_destroy(lthread_t *thread) 
{
  
}

void lthread_exit(void * arg) 
{

}
void lthread_join(lthread_t *thread)
{

}

#define TRUE 1
#define FALSE 0

void lthread_suspend(lthread_t *thread) {
  if(thread->isSuspended == FALSE) {
    thread->isSuspended = TRUE;
    //printf("stopping (%d)\n",thread->isSuspended);
    cyg_thread_suspend(thread->handle);
  }
}

void lthread_resume(lthread_t *thread) {
  //printf("Trying to restart for data: (%d)\n",thread->isSuspended);
  if(thread->isSuspended != FALSE) {
    thread->isSuspended = FALSE;
    //printf("starting\n");
    cyg_thread_resume(thread->handle);
  }
}

void lthread_mutex_init(lmutex_t *mutex) 
{
  cyg_mutex_init(&mutex->mutex);
}

void lthread_mutex_destroy(lmutex_t *mutex)
{
  cyg_mutex_destroy(&mutex->mutex);
}

int lthread_mutex_lock(lmutex_t *mutex)
{
  return cyg_mutex_lock(&mutex->mutex);
}

int lthread_mutex_unlock(lmutex_t *mutex)
{
  cyg_mutex_unlock(&mutex->mutex);;
  return 0;
}

#endif
