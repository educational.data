#if ! defined(__LMEMORY_H__INCLUDED__)
#define __LMEMORY_H__INCLUDED__

BEGIN_CSPEC

void *lmalloc(unsigned size);
void lfree(void *member);
void *lmemset(void *s, int c, unsigned n);
void *lmemcpy(void *dest, const void *src, unsigned n);

END_CSPEC

#endif
