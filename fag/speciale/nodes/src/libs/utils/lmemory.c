#if defined(HAVE_CONFIG_H)
# include "config.h"
#endif

#include "common.h"
#include "stdlib.h"
#include "lmemory.h"


#include <string.h>

void *lmalloc(unsigned size) {
  return malloc(size);
}

void lfree(void *ptr) {
  free(ptr);
}

void *lmemset(void *s, int c, unsigned n) {
  return memset(s,c,n);
}

void *lmemcpy(void *dest, const void *src, unsigned n) {
  return memcpy(dest, src,n);
}
