#ifndef __LQUEUE_H__
#define __LQUEUE_H__

#if ! defined(COMMON_H_INCLUDED)
# error common.h needs to be included prior to using this file.
#endif

/** @file queue.h
 * @brief Defines a simple implementation of a queue.
 *
 * The queue is basicly just a circular queue that is unable to grow
 * in size as of right now. I off course will change that if i find
 * that a normal implentation like that will not suffice.
 *
 */

#if defined(__cplusplus)
extern "C" {
#endif

  /**
   * @brief internaly queue.
   *
   * This queue implementation will quite likely be changed when i am
   * certain that the queue implementation will not utilize any stl
   * code.
   *
   */
  struct queue;

  /**
   * @brief Wrapper for internal queue.
   *
   * Be aware that even though the present version will only include a
   * single pointer, it may in the future be replaced with more
   * correct code.
   */
  struct lqueue_t {
    struct queue *queue;
  };
  
  typedef struct lqueue_t lqueue;
  
  /**
   * @brief initialize a lqueue object.
   *
   * This function is mandatory to call before using as it will create
   * internal obejcts.
   */
  void lqueue_create(lqueue *q);

  /**
   * @brief destroys internal object for queue.
   *
   * The internal representation is destroyed, but all the objects
   * that are present on the queue is not touched and therefore there
   * should be a proper destruction done of those prior to destroying
   * the object as it will yield memorory leaks if the object is
   * prematurely destroyed.
   */
  void lqueue_destroy(lqueue *q);
  
  /**
   * @pops a single object from the queue line.
   *
   * If there is no object on the queue, a NULL will be returned.
   *
   * Beware that such functionality might change if this is
   * reimplemented with mailboxes.
   */
  void *lqueue_pop(lqueue *q);

  /**
   * @returns the top object from the queue line.
   *
   * If there is no object on the queue, a NULL will be returned.
   * <p/>
   * furthermore the object in the head of the queue is not removed.
   *
   * Beware that such functionality might change if this is
   * reimplemented with mailboxes.
   */
  void *lqueue_peek(lqueue *q);
  
  /**
   * @brief Adds an object to the queue stack.
   *
   * When you add an object to a queue, you should be aware that an
   * object may in this implementation fall over the end of this qeueu
   * since we do not support in this implementatio to dynamicly
   * enlarge the queue if its filling up.
   *
   * beware that future implementation may implement lobject and
   * therefore it will be less likely that the object will be lost
   * even though its lost at the end of the queue.
   */
  void lqueue_push(lqueue *q, void *obj);

  /**
   * @brief Detect if there is an object in the queue.
   *
   * One should use this method to test if there is an object in the
   * 
   */
  BOOL lqueue_empty(lqueue *q);
  
#if defined(__cplusplus)
}
#endif

#endif
