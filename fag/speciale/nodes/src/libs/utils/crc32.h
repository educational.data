#ifndef __CRC_H__INCLUDED__
#define __CRC_H__INCLUDED__

#include "common.h"
/* crc32.h

   header file for crc32 checksum
*/

#define CRC32_XINIT 0xFFFFFFFFL		/* initial value */
#define CRC32_XOROT 0xFFFFFFFFL		/* final xor value */

#define MINIMUM_CHECKSUM_LEN	 8
#define MAXIMUM_CHECKSUM_LEN	99

/* NAACCR 6.0 Specifications */
//#define NAACCR_60_CHECKSUM_POS	942
//#define NAACCR_60_CHECKSUM_LEN	10
BEGIN_CSPEC
/* function prototypes */
unsigned long calc_CRC32(unsigned char *p, unsigned long reclen);
int AssignCRC32(unsigned char *, unsigned long, unsigned long, unsigned long);
int CompareCRC32(unsigned char *, unsigned long, unsigned long, unsigned long);
END_CSPEC
#endif
