#ifndef __GRID_HPP__
#define __GRID_HPP__

#if defined(ON_MODULE)
# error This file must not be compiled on the module
#endif

#include <vector>
#include <boost/shared_ptr.hpp>

class Node;
class Leg;

namespace grid {
  typedef boost::shared_ptr<Node> Node_tp;
  typedef boost::shared_ptr<Leg> Leg_tp;

  struct grid {
    std::vector < Node_tp > n;
    std::vector < Leg_tp > l;
  };

  grid createTestGrid();
}

#endif
