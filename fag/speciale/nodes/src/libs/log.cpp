#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "log.hpp"
#include "nodetime.h"
#include <stdio.h>
#include <stdarg.h>

#if defined (ON_BUILD_PLATFORM)
# include <iostream>
# include <sstream>
# include <iomanip>
# include <fstream>
# include <stdexcept>
#endif

using namespace std;

const static char *blue="\33[34m";
const static char *red="\33[31m";
const static char *green="\33[32m";
const static char *reset="\33[0m";
const static char *power="\33[1m";

namespace log {
#if ! defined (ON_MODULE)
  
  static ofstream ost;
  static ostream *stream;

  void init() {
    ost.open("test.log");
  }

  ostream &elog(const char *filename) {
    stringstream strs;
    strs << blue << power << " [" << reset 
         << power << red << setw(4) 
         << nodetime_time() << blue << "] " << reset;
    cerr << strs.str();
    return cerr;
  }

  ostream &olog(const char *filename) {
    return log(filename,stdout | timestamp);
  }

  ostream &hlog(const char *filename) {
    return log(filename,file);
  }

  ostream &log(const char *filename, const int type) {
    if(type == 0)
      throw invalid_argument("You must specify an argument to log");

    if(type & (stdout|stderr|file) == 0)
      throw invalid_argument("You must specify an output");

    if (type & file)
      stream = &ost;
    
    if (type & stdout)
      stream = &cout;

    if (type & stderr)
      stream = &cerr;

    if((type & timestamp) != 0) {
      stringstream strs;
      if(type & error)
        strs << blue << power << " [" << reset << power << red << setw(4) << nodetime_time() << blue << "] " << reset;
      else
        strs << blue << power << " [" << reset << power << green << setw(4) << nodetime_time() << blue << "] " << reset;

      (*stream) << strs.str();
    }
    
    
    return *stream;
  }

  ostream &flog(const char *filename) {
    return log(filename, file | timestamp);
  }
#endif
}

BEGIN_CSPEC

void _log_file(char * format, ...) 
{
  char buffer[256];
  va_list args;
  va_start (args, format);
  vsprintf (buffer,format, args);
  va_end (args);
  LOG_FILE(buffer);
}

void _log_out(char * format, ...) 
{
  char buffer[256];
  va_list args;
  va_start (args, format);
  vsprintf (buffer,format, args);
  va_end (args);
  LOG_OUT(buffer);
}

void _log_error(char * format, ...)
{
  char buffer[256];
  va_list args;
  va_start (args, format);
  vsprintf (buffer,format, args);
  va_end (args);
  LOG_ERROR(buffer);
}

void _log_nolog(char * format, ...)
{ }

END_CSPEC
