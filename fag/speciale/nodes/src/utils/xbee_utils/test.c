#include "xbee.h"
#include <assert.h>
#include <stdio.h>

unsigned char buffer[] ={ 0x31,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0x10,0x13 };

int main(int arcc, char *arcv[]) 
{
  assert(arcc == 2);
  xbee_communicator_t com;
  xbee_configuration_t conf;
  conf.channel=1;
  conf.idl=1;
  conf.idu=1;
  conf.serialport = arcv[1];
  xbee_initialize(&com,conf);

  unsigned size=12;
  
  xbee_writePackage(&com, (void *)buffer,&size);

  printf("Managed to transmit %d bytes.\n",size);
  return 0;
}
