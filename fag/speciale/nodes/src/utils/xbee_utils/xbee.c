#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "xbee.h"




void openConnection(xbee_communicator_t *com, xbee_configuration_t *conf) {
  //int res;
  struct termios tty;      // will be used for new port settings
  //char buf[255];      // buffer used to store received characters
  //serial_index = 0;
  
  // set up serial port
  com->fd_serial = open(conf->serialport, O_WRONLY | O_NONBLOCK | O_NOCTTY | O_NDELAY);
  if (com->fd_serial < 0) {
    char string[255];
    sprintf(string,"unable to connect to serial port %s", conf->serialport);
    perror(string);
    exit(-1);
  } else {
    printf("Opened %s\n",conf->serialport);
  }
  tcgetattr(com->fd_serial, &com->oldtty);      // save current port settings
  tcgetattr(com->fd_serial, &tty);      // save current port settings
  //bzero(&com->tty, sizeof(com->tty));      // InitServoialize the port settings structure to all zeros
  // then set the baud rate, handshaking and a few other settings
  tty.c_iflag = IGNPAR;
  tty.c_lflag = 0;      // set input mode (non-canonical, no echo,.)
  tty.c_oflag = 0;
  tty.c_cflag = B9600 | CS8 | CLOCAL;
  tty.c_cc[VTIME] = 0;      // inter-character timer unused
  tty.c_cc[VMIN] = 1;      // blocking read until first char received
  
  if(tcflush(com->fd_serial, TCIOFLUSH) != 0) {
    perror("Cannot flush data");
    exit(-1);
  }
 
  if(tcsetattr(com->fd_serial, TCSANOW, &com->tty) != 0) {
    perror("Cannot set attribs");
    exit(-1);
  }
}

void xbee_write(xbee_communicator_t *com, void *buffer, unsigned int *len) {

  printf("Trying to send %d bytes\n",*len);
  *len = write(com->fd_serial,buffer,*len);
  if(errno != 0)
    perror("Cannot write data");

}

void xbee_writeString(xbee_communicator_t *com, char *string) {
  unsigned int len = strlen(string);
  xbee_write(com, string, &len);
}

void xbee_writePackage(xbee_communicator_t *com, void *string, unsigned int *size) {
  xbee_write(com, string, size);
}

void enterCommandMode(xbee_communicator_t *com) {
  sleep(1);
  xbee_writeString(com,"+++");
  sleep(1);
}

void xbee_initialize(xbee_communicator_t *com, xbee_configuration_t conf) {
  openConnection(com, &conf);
  //enterCommandMode(com);
}
