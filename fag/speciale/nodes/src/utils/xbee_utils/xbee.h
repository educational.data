#ifndef __XBEE_H__INCLUDED__
#define __XBEE_H__INCLUDED__

//#include <cyg/io/io.h>

#include <termios.h>

typedef struct {
  int fd_serial;
  struct termios tty;
  struct termios oldtty;
} xbee_communicator_t;

typedef struct {
  unsigned int channel;
  unsigned int idu;
  unsigned int idl;
  char *serialport;
  
} xbee_configuration_t;

void xbee_initialize(xbee_communicator_t *com, xbee_configuration_t conf);
void xbee_write(xbee_communicator_t *com, void *buffer, unsigned int *len);
void xbee_writeString(xbee_communicator_t *com, char *string);

void xbee_writePackage(xbee_communicator_t *com, void *string, unsigned int *size);

#endif
