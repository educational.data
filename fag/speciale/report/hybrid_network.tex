% -*- latex -*-
\section{Hybrid communication}\label{sec:hybrid}
Previously I have described the \emph{Global communication}, where
the idea is that all participants should be connected to one
single big net that covers all modules (not necessarily all
nodes) in the graph. But the channel forwarding mechanism is
also useable to create a hybrid net, where a connected net is
created as needed between two modules that have a need for some
fast or massive communication link between the participants.

I have, as a proof of concept, created an algorithm that is
based on gradients that will establish the shortest possible
path between two or more nodes which have a need for fast
communication.

The initiator of this algorithm is some higher level unknown
algorithm, or perhaps even the user of the robot. But I will
later discuss some possible approaches where it is the amount of
communication that decides whenever a channeled network is to
appear.

\begin{Figure}[H]
\setRuled
\resizebox{\textwidth}{!}{\epsfig{file=fig/gradients}}
\caption[Resolving an optimal route]{Resolving of optimal route
  between the two modules \emph{S} and \emph{E} using
  gradients. The module \emph{S} sends out messages that leaves
  a inverted \emph{distance to module} value in each module.}
\label{fig:gradients}
\end{Figure}

\subsection{The current algorithm}
The algorithm I have designed up to this point is as mentioned,
based on the ideas of gradients.  

If we look at the network in figure \ref{fig:gradients}, the
algorithm basically works by sending out a message from $S$ that
leaves knowledge of the number of modules that the message has
passed so far. When the message ultimately arrives at the end
module ($E$), the module returns a message that opens up the
actual path to the starting module ($S$). The returning path is
build up by following a gradient toward the starting module
$S$. This mean that the process is divided over three major
states that are described as:
\begin{description}
\item [Forward discovery] build a graph with weighted edges.

\item [Reverse building] use the weighted edges to open up a
  channel back towards $S$ by following the gradient backwards.

\item [Complete] The path is complete and running.
\end{description}

In order to avoid that the algorithm should overcrowd the
network with messages which for some reason seems to loop
forever, the algorithm is implemented by setting the initial
count to a number value representing the maximum steps that the
algorithm should run. For every step, the variable is
decremented until it reach zero. Here the message stop and the
algorithm terminates locally.

\subsection{Implementation}
It is difficult to visualize how the algorithm work for the
individual modules since each module, as long as it is not part
of the net, is just forwarding the messages as they appear
reflecting the gradients. The end module who intend to share a
network with the start module, is being triggered when it
receive a gradient message, to send out a ``build network''
message. But if we separate the functionality of these two
module types, we have a module that is able to forward messages
and alter the gradient as the messages are being transferred. It
can reverse the process and decide, between all the other
modules on the same node, if it is the module with the highest
gradient.

The second type can choose to stop the message forwarding if it
discovers that it is part of the same network, and then reverse
the mechanism so that a net is being build based on the resolved
gradients.

This second functionality is of course trivial assumed that
someone has decided for the module that is are part of the same
network. But the first functionality needs to perform a decision
between several modules on who owns the best gradient and is
therefore not as straight forward.

\subsubsection*{Messages}
To support this we have three messages:
\begin{description}
\item[BUILD] Ask the modules to try to build their gradient
  based on the included gradient - 1. If a module already
  has a gradient that is lower than the supplied gradient
  (meaning that we already have a shorter path to the source
  net), we just ignores this message.

\item[SEEK] Ask all modules, connected to one node, to report
  their current potential to each other. 

\item[HAS] Reply with the gradient from the individual modules.
  (only if they have previously obtained a gradient through a
  BUILD reply)

\end{description}

In figure \ref{fig:hybrid_statemachine}, I have tried to give a
description of the internals of the hybrid algorithm. The three
messages are the driving force of the algorithms, as the machine
is in its \emph{idle} state while the gradients is being
built. But as a SEEK message appears on a terminator, the module
replies on the same terminator with its gradient value with a
HAS message.  At the same time it enters its \emph{wait} state
where it waits for all other modules to reply with their
gradients. When the terminator has not received any HAS messages
for $10 \tau$, the received potentials is compared to the
modules own potential. If the module owns the best potential, it
sets its state to open (\emph{resolve}) and restarts the search
process on the opposite terminal by sending a SEEK message.

\begin{Figure}[ht]
\setRuled
\begin{pspicture}(11.4,7)
\psset{linecolor=black,nodesep=2pt}
%\psframe(11.4,7){}
\rput(0.2,1){
\rput(0,4){\cnode*{0.15}{root}}
\rput(1.5,4){\circlenode{idle}{\small\bf idle}}
\rput(4.5,4){\circlenode{seek}{\small\bf seek}}
\rput(4.5,1){\circlenode{wait}{\small\bf wait}}
\rput(7.5,1){\circlenode{open}{\small\bf resolve}}
\pnode(11,1){next}
}
\nccircle[linecolor=transistioncolor,linewidth=2pt,fillstyle=none,angleA=0]{->}{idle}{0.5}
\nbput*{\small $R^{\leftarrow}_{BUILD}$}
\ncline[linecolor=transistioncolor,linewidth=2pt]{->}{root}{idle}
\ncline[linecolor=transistioncolor,linewidth=2pt]{->}{idle}{seek}
\ncput*{\small $R^{\leftarrow}_{SEEK}$}
\ncline[linecolor=transistioncolor,linewidth=2pt]{->}{seek}{wait}
\ncput*{\small $T^{\leftarrow}_{HAS}$}
\nccircle[linecolor=transistioncolor,linewidth=2pt,fillstyle=none,angleA=180]{->}{wait}{0.5}
\nbput*{\small $R^{\leftarrow}_{HAS}$}
\ncline[linecolor=transistioncolor,linewidth=2pt]{->}{wait}{idle}
\ncput*{\small not best}
\ncline[linecolor=transistioncolor,linewidth=2pt]{->}{wait}{open}
\ncput*{\small best}
\ncline[linecolor=transistioncolor,linewidth=2pt,linestyle=dotted]{->}{open}{next}
\ncput*{\small $T^{\rightarrow}_{SEEK}$}
\end{pspicture}
\caption{State machine describing the hybrid algorithm.}\label{fig:hybrid_statemachine}
\end{Figure}

\subsection{Conclusion}
The hybrid network algorithm is an initial attempt to use
gradients to build a hybrid network. The implementation is in
itself rather simple, but performs remarkably well in simulation
environments with two different modules establishing a hybrid
net. But the hybrid approach needs to be reconsidered since it
has not been thought through how it may coexists with normal
communication. Another thing that has to be thought through is
how to alter the method so it will work well with more than two
modules in the same net.
