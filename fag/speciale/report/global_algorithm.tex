% -*- latex -*-
\chapter{Higher level communication}
In the previous chapter I have covered how communication is
taking place at the low level between the individual
modules. This kind of communication is very local and has no
functionality to carry the information across many different
modules. It can actually be said that the communication at node
level, has absolutely no knowledge of its neighbouring cells.

The higher levels of the communication system provides
communication on a more wide level since I now cover the gap
that exists between these nodes.

This level of communication is highly interesting since there
exist many possible ways of communicating across the graph
structure of the modules, all dependant on the individual
application chosen for the Odin module. At this time, its very
difficult to say anything about what these applications should
be (at least at a very concrete level) so a more basic approach
is attempted.

Another thing that I need to re-mention is the fact that there
are two versions of the modules available. The first one
includes the physical forwarding mechanism while the next
(present) one does \emph{not} include this scheme. Even though
this has great impact on how the system is able to perform, I
still believe that the communication schemes are useful when we
want to perform forwarding of data between modules.

I present two algorithms that I believe to be essential for
modular robots like the Odin modules. The first algorithm,
presented in section \ref{sec:global_algorithm}, builds a
\emph{global net} where all modules are connected and therefore
hear all that is being said. The second algorithm, presented in
\ref{sec:hybrid}, creates a \emph{hybrid net} where the
algorithm connects two modules that have a certain need to be in
direct communicating with each other.

\section{Global communication algorithm}\label{sec:global_algorithm}
This section describes the global algorithm as I have chosen to
design it. First i describe the reasoning for the global
algorithm by stating the goal of the global algorithm. Next, I
will try to give a general description of the method for
establishing such a network based on the knowledge learnt from
previous sections. As a consequence of this i describe the
design as i have made it. %Finally, I analyze the outcome based
%on several continues runs of the algorithm in a simulation.

\subsection{The goal of a global algorithm}
When we say that a message is global, we mean that the message
has interest to the entire scope of modules. So we should be
able to send a message guaranteed to be delivered to all members
of the present grid. In order to do this, each module in the
present version, is able to forward from one end to the other on
a physical basis as described by
\cite{hybrid_communication}. This opens the possibility of 
creating a network that will span several different modules at
the same time - simply based on whatever the modules have closed
their connection between their individual terminals or not.

But within this option there also exist some issues that the
algorithm should be able to take care of.

The modules are able to be connected in a large grid that can
yield some loops in the communication structure (see figure
\ref{fig:global-loop}). Therefore the algorithm must be able to
avoid these loops. Furthermore, there is no guaranteed delivery
of packages based on a series of possible reasons. The messages
may simply be lost based on collisions or noise or maybe the
modules may fail or simply be removed based on
reconfiguration. Another likely thing could be that a module may
be removed and placed in a different position in the network.

Some of these things of course are the responsibility of other
parts of the system to detect, but it is important to know that
the impact of these problems may ultimately cause the global
network to fail, meaning that the goal of the algorithm has
failed.

So we can point out the individual goals that the algorithm
should take care of.

\begin{itemize}
\item Be able to establish a network that is guaranteed to reach
  all modules in the grid.
\item Solve loop issues so that messages will not be looping
  forever.
\item Be able to handle continuous reconfigurations due to
  removal of modules or simple failure of individual modules.
\item Be robust enough to create reliable networks in noisy
  environments.
\end{itemize}
\subsection{The proposed solution}
\begin{Figure}
\setRuled
\epsfig{file=fig/global,width=\linewidth}
\caption[Loop scenario in the global scheme.]{Loop scenario in
  the global scheme. It can be seen that if a node is met from
  both sides at the same time, there may appear a
  problem.}\label{fig:global-loop}
\end{Figure}

As the entire network is requested to go in to a global state,
the process is of course always started at a single leg. This
single leg starts the process by sending a request out on the
first node asking who are not already connected to the global
network. When any modules which replies they are not part of the
global network, the first module detects this as a need for
establishing a connection to this module and therefore connect
the two terminals internally.

Of course, if no other modules are present or if the ones
that are present at the node, already are connected, there will be
no answer and therefore the algorithm will terminate locally.

If the starting module has detected that it was necessary to
create a forwarding connection to the node, it will prior to
doing this, send a message to the modules connected to this node
indicating that they should try to grow the network one step
further. This will then reiterate the algorithm at all the
attached modules.

\subsubsection{Problems with the proposed solution}
In the case that the requesting algorithm splits and later
connects again at some node further out in the structure, a
complexity might occur. 

Figure \ref{fig:global-loop} displays such a complexity. In sub
figure (a), the first module receives a request to connects its
connected first node. Therefore it asks if any modules on the
first node is connected to the global network they reply and say
they are not connected and therefore the first module decides to
setup the forwarding. 

In sub figure (b), the next two modules perform the same task
and in both situations the modules are not connected to the
global network and therefore continues.

In sub figure (c) the two modules ask \emph{at the same time} if
any of the modules connected to the last node, are connected to
the local graph. Since they both arrive at the same time, they
also both receive the same answer: The last module is \emph{not}
connected. So since both the asking modules in sub figure (c)
decide that they need to provide forwarding to the last module,
we get the scenario we see in sub figure (d), where a loop
appears.


This problem is a result of a race condition that is truly
specific to the distributed nature of the algorithm. There is a
very good chance that the requests will appear at the end node
at the same time. A sequential algorithm would not suffer from
this since a module would have guaranteed single access on the
node until the computation is complete.


\begin{sidefig}
\begin{pspicture}(4.5,3.5)
  %\psframe*[linecolor=blueback](4.5,3.5){}
  \rput(2.25,1){
    \emptynode(-2,0){A}
    \emptynode(-1.5,1.5){B}
    \emptynode(0,2){C}
    \emptynode(1.5,1.5){D}
    \emptynode(2,0){E}
    \odinnode(0,0){M}
    \askingline{A}{M}
    \ncput*[fillcolor=white,linecolor=black]{1}
    \askingline{B}{M}
    \ncput*[fillcolor=white,linecolor=black]{2}
    \askingline{C}{M}
    \ncput*[fillcolor=white,linecolor=black]{3}
    \askedline{D}{M}
    \askedline{E}{M}
}
\end{pspicture}
\caption[Requests meeting at the same node]{Several requests
  arrive at the same node at the same time. A competition
  decides who has the responsibility to connect this node to the
  resulting network.}\label{fig:competition}
\end{sidefig}

\subsubsection{Proposed solution to the race problem}
A special case is when two or more requests arrive at the same
node at approximately the same time from the modules \emph{1,2}
and \emph{3}, as shown in figure \ref{fig:competition}. It is
evident that all the modules that have sent out a request on that
node, will also receive the request from other modules that
also have sent out a request.

In this situation a special scenario appear where the
process must be stopped since they cannot all make the request
and hope to succeed. Therefore all requesting modules must
signal each other to stop their current request task and instead
solve who should have the sole responsibility for requesting the
modules for their global net state.

The proposed solution is to make a competition between the
conflicting modules. Each module generate a random number and
sends the number to each other. Then the module that has sent
out the highest number will know that it has won the competition
when it has seen the values from all the other competing
modules. This module will now have won the responsibility of
handling the three build process and therefore repeat the
requesting for global state while all other modules act as
normal connected or not connected modules.

\section{State machine}
In order to handle this functionality, the algorithm use the
following messages:

\begin{description}
\item[GROW\_TREE:] \emph{(GT)} Message telling the module that
  it should try to grow the three one level further for the next
  node and its directly attached modules.

\item[ASK\_HAS\_GLOBAL:] \emph{(AHG)} Message used to ask the
  other modules if they already have connection to the global
  network.

\item[IS\_NOT\_GLOBALLY\_CONNECTED:] \emph{(INGC)} Message
  replied from modules that has been asked with the
  ASK\_HAS\_GLOBAL message indicating that the modules are
  \emph{not} connected to the global network.

\item[INTERRUPT\_THREE\_GROWTH:] \emph{(ITG)} Message telling
  that there is a conflict where more than one module is
  actively trying to ask if the modules are globally connected.
\end{description}

\paragraph{States and transitions}
In figure \ref{fig:state_global}, I have tried to describe
\emph{all} the messages that causes state changes in the
diagram. The resulting figure is therefore rather complex and I
have therefore numbered the individual transitions for
reference.

\begin{description}
\item[noth:] The beginning state or rather the default
  state. This state represent the module when it is not
  responsible for conducting any three building. A message
  ASK\_HAS\_GLOBAL will ask the module if it is currently a part
  of the global net or not. It will therefore reply accordingly
  ($T_5$).

  It is only if the module receive a GROW\_TREE message on its
  back interface that the system will start the process by
  sending out a ASK\_HAS\_GLOBAL message on its forward
  interface ($T_1$). But if they \emph{already has global
    connection}, the request is ignored.
\end{description}

\begin{Figure}
\setRuled
\global\def\figside{l}
\begin{pspicture}(9,11)
  %\psframe(9,11){}
  \psset{linecolor=black}
  \rput(1.5,8.5){\cnode*{0.15}{root}}
  \rput[cm](4,8.5){\circlenode{A}{\small\bf noth}}
  \rput[cm](4,6){\circlenode{B}{\small\bf eval}}
  \rput[cm](6,1.5){\circlenode{C}{\small\bf comp}}
  \psset{linecolor=transistioncolor,linewidth=2pt,fillstyle=none,nodesep=2pt}

  \ncarc{->}{A}{B}
  \naput*{\small $\mathbf{T_1}$:$R^{\rightarrow}_{(GT)}$}
  \ncarc{->}{B}{A}
  \naput*{\small $\mathbf{T_8}$}

  \ncarc[arcangleB=-90,arcangleA=-45]{->}{B}{C}
  \ncput*{\parbox{1.1cm}{\small $\mathbf{T_3}$:\\$R^{\rightarrow}_{(ITG)}$\\$T^{\rightarrow}_{(ITG)}$}}
  \ncline{->}{B}{C}
  \ncput*{\parbox{1.1cm}{\small $\mathbf{T_2}$:\\$R^{\rightarrow}_{(AHG)}$\\$T^{\rightarrow}_{(ITG)}$}}

  \ncarc[arcangle=-65]{->}{C}{B}
  \ncput*{\parbox{2cm}{\small $\mathbf{T_6}$:\\$v>v_{foreign}$\\$T^{\leftarrow}_{(AHG)}$}}
  \ncarc[arcangle=-90,arcangleA=-110,nodesep=5pt]{->}{C}{A}
  \ncput*{\small $\mathbf{T_7}$:$v<v_{foreign}$}

  \nccircle{->}{A}{0.5}
  \nbput*{\parbox{1.1cm}{\small $\mathbf{T_5}$:\\$R^{\leftarrow}_{(AHG)}$\\$T^{\leftarrow}_{(ING)}$}}
  \nccircle[angleA=90]{->}{B}{0.5}
  \nbput*{\small $\mathbf{T_4}$:$R^{\rightarrow}_{(INGC)}$}

  \ncline{->}{root}{A}
\end{pspicture}
\caption[State machine for the global algorithm]{State machine
  for the global algorithm. This state diagram only consists of
  3 states. The individual transitions are highly complex and
  therefore I recommend reading the text on the next page.}
\label{fig:state_global} 
\end{Figure}

\begin{description}
\item[eval:] When the graph has started the three growth
  mechanism, it await until the network stop sending any
  replies. If any in the network, during this period, has
  replied that they are \emph{not} part of the global network
  ($T_4$), the initiating module decides that it needs to create
  a forwarding to this net. If this is the case, the module
  sends a GROW\_THREE message on its forwarding interface to let
  the members try to grow their net before the internal
  forwarding is finally connected.

  Should the module receive a ASK\_HAS\_GLOBAL message, it know
  that there are other modules on the same node that is trying
  to build a net. It therefore inhibits the process by sending
  out a INTERRUPT\_THREE\_GROWTH message including a competition
  value ($T_2$).

  In the case it receive an INTERRUPT\_THREE\_GROWTH message, it
  replies with a new INTERRUPT\_THREE\_GROWTH message including
  a new competition value ($T_3$).
  
\item[comp:] The system is being blocked by multiple contestants
  and now wait until all competitors has delivered their
  competition values. If the module's own competition value is
  the highest, the module return to the evaluation state
  restarting the evaluation process ($T_6$). If the module's own
  competition value is lower than the highest foreign value, the
  module just returns to the ordinary sleeping state (noth)
  where it just acts as a normal connected module ($T_8$)
  
  Should it happen that the modules competition value is the
  same as the largest foreign seen value, a new
  INTERRUPT\_THREE\_GROWTH message is sent out with a new
  competition value (not shown) and the competition process is
  repeated.
\end{description}

