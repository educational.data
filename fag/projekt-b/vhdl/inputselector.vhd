library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity InputSelector is
port(
	IO_In_port : in std_logic_vector(7 downto 0);
	IO_Read_strobe : out std_logic;

	BUS_In_port : in std_logic_vector(7 downto 0);
	BUS_Read_strobe : out std_logic;

	CPU_In_port : out std_logic_vector(7 downto 0);
	CPU_Read_strobe : in std_logic;

	CPU_Port_id : in std_logic_vector(7 downto 0)
);
end InputSelector;

architecture Behavioral of InputSelector is
	constant IO_portid : std_logic_vector(7 downto 0) := x"FF";
begin
	process(CPU_Port_id, CPU_Read_strobe, IO_In_port, BUS_In_port)
	begin
		case CPU_Port_id is
			when IO_portid =>
				CPU_In_port <= IO_In_port;
				IO_Read_strobe <= CPU_Read_strobe;
				BUS_Read_strobe <= '0';
			when others =>
				CPU_In_port <= BUS_In_port;
				BUS_Read_strobe <= CPU_Read_strobe;
				IO_Read_strobe <= '0';
		end case;
	end process;
end Behavioral;
