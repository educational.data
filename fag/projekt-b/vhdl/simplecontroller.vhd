library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Simple_Controller is
	Port (	pClk				: in		std_logic;  -- System clock
				pReset 			: in		std_logic;

				pUp				: in	std_logic;
				pDown 			: in	std_logic;

				pPosOut			: out	std_logic_vector (7 downto 0); -- To CPU
				pPosReadEnable	: in	std_logic);
end Simple_Controller;

architecture Behavioral of Simple_Controller is
	signal sPosRegister	: std_logic_vector (7 downto 0);		-- bit8 = overflow
begin

	process(pClk, pReset)
	begin
		if pReset = '1' then
			sPosRegister <= (others => '0');
		elsif pClk'event and pClk = '1' then
			if pDown = '1' and pUp = '0' then
				sPosRegister <= "00001111";
			elsif pUp = '1' and pDown = '0' then
			   sPosRegister <= "11110000";
			else
			   sPosRegister <= (others => '0');
			end if;
		end if;
	end process;

	pPosOut <= sPosRegister;

end Behavioral;