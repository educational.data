library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity RAM is
port (
	DIA    : in STD_LOGIC_VECTOR (7 downto 0);
	DIB    : in STD_LOGIC_VECTOR (7 downto 0);
	ENA    : in STD_logic;
	ENB    : in STD_logic;
	WEA    : in STD_logic;
	WEB    : in STD_logic;
	RSTA   : in STD_logic;
	RSTB   : in STD_logic;
	CLKA   : in STD_logic;
	CLKB   : in STD_logic;
	ADDRA  : in STD_LOGIC_VECTOR (7 downto 0);
	ADDRB  : in STD_LOGIC_VECTOR (7 downto 0);
	DOA    : out STD_LOGIC_VECTOR (7 downto 0);
	DOB    : out STD_LOGIC_VECTOR (7 downto 0)
);
end RAM;

architecture Behavioral of RAM is

	component RAMB4_S8_S8	
	generic (
		INIT_00 : bit_vector := X"3333333333333333333333333333333333333333333333333333333333333333";
		INIT_01 : bit_vector := X"3333333333333333333333333333333333333333333333333333333333333333"
	);
	--
	port (
		DIA    : in STD_LOGIC_VECTOR (7 downto 0);
		DIB    : in STD_LOGIC_VECTOR (7 downto 0);
		ENA    : in STD_logic;
		ENB    : in STD_logic;
		WEA    : in STD_logic;
		WEB    : in STD_logic;
		RSTA   : in STD_logic;
		RSTB   : in STD_logic;
		CLKA   : in STD_logic;
		CLKB   : in STD_logic;
		ADDRA  : in STD_LOGIC_VECTOR (8 downto 0);
		ADDRB  : in STD_LOGIC_VECTOR (8 downto 0);
		DOA    : out STD_LOGIC_VECTOR (7 downto 0);
		DOB    : out STD_LOGIC_VECTOR (7 downto 0)
	); 
	end component;

begin

	BRAM : RAMB4_S8_S8
	port map (
		DIA    => DIA,
		DIB    => DIB,
		ENA    => ENA,
		ENB    => ENB,
		WEA    => WEA,
		WEB    => WEB,
		RSTA   => RSTA,
		RSTB   => RSTB,
		CLKA   => CLKA,
		CLKB   => CLKB,
		ADDRA(7 downto 0) => ADDRA(7 downto 0),
		ADDRA(8) => '0',
		ADDRB(7 downto 0) => ADDRB(7 downto 0),
		ADDRB(8) => '0',
		DOA    => DOA,
		DOB    => DOB
	);

end Behavioral;
