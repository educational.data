library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Mouse_Controller is
	Port (
		--sBufferCount_dbg		: out std_logic_vector (5 downto 0);
		--sMousePackets_dbg 	: out std_logic_vector (32 downto 0);
		--sState_dbg				: out std_logic_vector (2 downto 0);
		--sPosRegister_dbg		: out std_logic_vector (8 downto 0);
		--sStartUp_dbg			: out std_logic;
		--sStartUpState_dbg		: out std_logic_vector (4 downto 0);
		--sStartUpClkCount_dbg	: out std_logic_vector (11 downto 0);
				
		D4			: out		std_logic;  -- Debug signal
		D5			: out		std_logic;  -- Debug signal
		D6			: out		std_logic;  -- Debug signal

		pClk			: in		std_logic;  -- System clock
		pReset		: in		std_logic;  -- Global reset
		pMouseClk		: inout	std_logic;  -- Mouse driven clock
		pData 		: inout	std_logic;  -- Data signal: Controller <-> Mouse
		pPosOut		: out		std_logic_vector (7 downto 0);  -- To CPU
		pPosReadEnable	: in		std_logic   -- From CPU
		);
end Mouse_Controller;

architecture Behavioral of Mouse_Controller is
	signal sPosRegister	: std_logic_vector (8 downto 0);		-- bit8 = overflow
	signal sMousePackets	: std_logic_vector (32 downto 0);
	signal sBufferCount	: std_logic_vector (5 downto 0);
	signal sState		: std_logic_vector (2 downto 0);
	
	signal sStartUp		: std_logic;
	signal sStartUpClkCount : std_logic_vector (11 downto 0);
	signal sStartUpState	: std_logic_vector (4 downto 0);
	signal sStartUpMouseClk_Last : std_logic;

	signal D4_new		: std_logic;
	signal D5_new		: std_logic;
	signal D6_new		: std_logic;
	
begin
	--sMousePackets_dbg <= sMousePackets;
	--sBufferCount_dbg <= sBufferCount;
	--sState_dbg <= sState;
	--sPosRegister_dbg <= sPosRegister;
	--sStartUp_dbg <= sStartUp;
	--sStartUpState_dbg <= sStartUpState;
	--sStartUpClkCount_dbg <= sStartUpClkCount;

	process (pReset, pClk, sStartUp)
	begin
		if (pReset = '1') then
			sStartUpState <= "00000";
			sStartUp <= '1';
			sStartUpClkCount <= "000000000000";
			-- sStartUpClkCount <= "110000000000";		-- Til simulering i modelsim

			sStartUpMouseClk_Last <= '1';
			pMouseClk <= 'Z';
			pData <= 'Z';

			D5 <= '1';
			D5_new <= '0';

		elsif (sStartUp = '1') then
			if (pClk'event and pClk = '1') then
				case sStartUpState is
				when "00000" => -- Tr�k MouseClk lav
					pMouseClk	<= '0';
					sStartUpState <= sStartUpState + 1;
				when "00001" => -- Vent 0.1 ms
					if (sStartUpClkCount = "110011100100") then
						sStartUpState <= sStartUpState + 1;
					else
						sStartUpClkCount <= sStartUpClkCount + 1;
						pMouseClk	<= '0';
					end if;
				when "00010" => -- Tr�k pData lav
					pData <= '0';
					sStartUpState <= sStartUpState + 1;
				when "00011" => -- Frig�r MouseClk
					pMouseClk <= 'Z';
					sStartUpState <= sStartUpState + 1;
					-- Musen skal nu starte med at drive MouseClk

				when "01111" =>	pData <= 'Z';			-- Release	data line
					sStartUp <= '0';					-- Initialisering f�rdig
					sStartUpState <= "11111";

				when others =>
					null;
				end case;

				if (pMouseClk = '0') then
					-- The host changes the Data line only when the Clock line is low,
					-- and data is read by the device when Clock is high
					if (sStartUpMouseClk_Last = '1') then -- kunstig event
					case sStartUpState is
					when "00100" =>	pData <= '0';			-- start bit
						sStartUpState <= "00101";
						D5 <= D5_new;
						D5_new <= not D5_new;
					when "00101" =>	pData <= '0';
						sStartUpState <= "00110";	-- Clock in 8 bit + stop bit - MSB
					when "00110" =>	pData <= '0';	-- LSB f�rst
						sStartUpState <= "00111";	-- Enable = 11110100	= F4h
					when "00111" =>	pData <= 'Z';
						sStartUpState <= "01000";
					when "01000" =>	pData <= '0';
						sStartUpState <= "01001";
					when "01001" =>	pData <= 'Z';
						sStartUpState <= "01010";
					when "01010" =>	pData <= 'Z';
						sStartUpState <= "01011";
					when "01011" =>	pData <= 'Z';
						sStartUpState <= "01100";
					when "01100" =>	pData <= 'Z';	-- MSB
						sStartUpState <= "01101";
					when "01101" =>	pData <= '0';	-- Paritets bit = 0
						sStartUpState <= "01110";
					when "01110" =>	pData <= 'Z';
						sStartUpState <= "01111";	-- Stop bit 
					when others =>
						null;
					end case;
					end if;
					sStartUpMouseClk_Last <= '0';
				else
					sStartUpMouseClk_Last <= '1';
				end if;
			end if;	-- pClk
		end if;
	end process;	

	process (pReset, pClk, pMouseClk, sState, sStartUp, sBufferCount)   -- Fill data register
	begin
		if (pReset = '1') then
			sMousePackets <= (others => '0');
			sBufferCount <= "000000";

			D4 <= '1';
			D4_new <= '0';

		elsif (sStartUp = '0') then
			if (sState = "101") then
				sMousePackets <= (others => '0');
				sBufferCount <= "000000";
			elsif (pMouseClk'event and pMouseClk = '1' and sState = "000") then
				-- sample the data on the positive clock edge and store the necessary bits. 
				-- data bits, mindst betydende bit f�rst
				sMousePackets <= sMousePackets (31 downto 0) & pData;
				sBufferCount <= sBufferCount + '1';

				D4 <= D4_new;
				D4_new <= not D4_new;
			end if;
		end if;
	end process;

	process (pReset, pClk, sMousePackets, sState)
	begin
		if (pReset = '1') then
			sPosRegister <= (others => '0');
			sState <= "000";
			D6 <= '1';
			D6_new <= '0';
		elsif (pClk'event and pClk = '1') then
			if (sBufferCount = "100001") then -- 33 = 34 bit
				sState <= "001";
				D6 <= D6_new;
				D6_new <= not D6_new;
			end if;

			case sState is
				when "001" =>
					sState <= "010";
 				when "010" =>  -- sClkCount = 33 = Data ready
					--pMouseClk	<= '0'; -- Data = high, Clock = low:  Communication Inhibited
					--pData			<= 'Z';

					if (sMousePackets(24) = '1') then -- Overflow bit for y == bit 8 i stream
						sPosRegister <= (8 => '0', others => '1');
					else
						if (sMousePackets(26) = '1') then -- Y sign bit
							sPosRegister <= ('0' & sPosRegister(7 downto 0)) - sMousePackets (9 downto 2);
						else
							sPosRegister <= ('0' & sPosRegister(7 downto 0)) + sMousePackets (9 downto 2);
							-- sPosRegister <= ('0' & sPosRegister(7 downto 0)) + sMousePackets (23 to 30);
							-- sPosRegister <= ('0' & sMousePackets (30 downto 23));
						end if;
					end if;

					sState <= "011";
				when "011" =>
					if (sPosRegister(8) = '1' and sMousePackets(26) = '1') then	 -- signbit
						sPosRegister <= (others => '0'); 				-- Negativ overflow
					elsif (sPosRegister(8) = '1' and sMousePackets(26) = '0') then
						sPosRegister <= (8 => '0', others => '1'); 	-- Positiv overflow
					end if;

					sState <= "100";
				when "100" =>
					--pMouseClk	<= 'Z'; -- Data = high, Clock = high:  Idle state.
					--pData			<= 'Z';
					sState <= "101";
				when "101" =>		-- Reset sBufferCount && sMousePackets
					sState <= "111";
				when "111" =>		-- F�rdig med at evaluere MousePackets
					sState <= "000";
				when others	=>
					-- null;
					sState <= sState + 1;
				end case;
			end if;
	end process;

	process (pReset, pClk, pPosReadEnable)
	begin
		if (pReset = '1') then
			pPosOut <= (others => '0');
		elsif (pClk'event and pClk = '1') then
			if (pPosReadEnable = '1') then  -- pull high to read register
				pPosOut <= sPosRegister (7 downto 0);
			end if;
		end if;
	end process;
end Behavioral;

-- Som de ligger i sMousePackets:
-- 32            26    24      21                      10                   0
-- ST L R 0 1 XS YS XY YY P SP ST X X X X X X X X P SP ST Y Y Y Y Y Y Y Y P SP
-- 0  0 0 0 1 0  0  0  0  0 1  0  0 0 1 1 0 1 1	0 1 1  0  0 1 0 1 1 1 0	1 0 1

-- Start Up Process:
--  After reset pull data to zero to get mouse to drive the clock.
--  Send enable command (F4h).
--  The mouse sends acknowledge (FAh) back.

-- Initialiserings fase:
-- Tr�k pMouseClk lav i mindst 0.1 ms:
-- 33.000.000 Hz / 10000 = 3300 Clks
-- Tr�k pData lav
-- Send enable (F4h)
-- Vent p� at mus sender acknowledge tilbage