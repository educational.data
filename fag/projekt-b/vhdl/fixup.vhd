library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fixup is
    Port ( A : in std_logic_vector(1 downto 0);
           B : out std_logic_vector(7 downto 0));
end fixup;

architecture Behavioral of fixup is
begin
	B <= (0 => A(0), 1 => A(1), others => '0');
end Behavioral;
