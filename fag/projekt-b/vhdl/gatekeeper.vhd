library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity GateKeeper is
port (
	inst : in std_logic_vector(15 downto 0);

	BUSRequest : out std_logic;
	BUSGrant : in std_logic;
	
	Hold : out std_logic; 
	 
	CPU_Port_id : in std_logic_vector(7 downto 0);
	CPU_Out_port : in std_logic_vector(7 downto 0);
	CPU_In_port : out std_logic_vector(7 downto 0);

	CPU_Read_strobe : in std_logic;
	CPU_Write_strobe : in std_logic;
			 
	BUS_Port_id : out std_logic_vector(7 downto 0);
	BUS_Out_port : out std_logic_vector(7 downto 0);
	BUS_In_port : in std_logic_vector(7 downto 0);

	BUS_Read_strobe : out std_logic;
	BUS_Write_strobe : out std_logic		 
);
end GateKeeper;

architecture Behavioral of GateKeeper is
	signal tristate : std_logic;
	signal request : std_logic;
begin
	-- Sammens�t request signalet via
	-- analyse af instruction s�ttet.
	request <= (inst(15) and (not inst(14)) and inst(13)) or (inst(15) and (inst(14)) and inst(13));

	hold <= (request and not BUSGrant);

	-- tristate f�lger samme m�nster som hold
	tristate <= not (request and BUSGrant);
	BusRequest <= request;

	BUS_Port_id <= CPU_Port_id when tristate = '0' else (others => 'Z');
	BUS_Out_port <= CPU_Out_port when tristate = '0' else (others => 'Z');

	-- alt inddata ryger direkte igennem bufferen
	CPU_in_port <= BUS_in_port;

	BUS_Read_strobe <= CPU_Read_strobe when tristate = '0' else 'Z';
	BUS_Write_strobe <= CPU_Write_strobe when tristate = '0' else 'Z';
end Behavioral;


