library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VComponents.all;

entity VGATiming is
port (
	clk   : in std_logic;
	reset : in std_logic;
	hsync : out std_logic;
	vsync : out std_logic;
	blank : out std_logic;
	row   : out std_logic_vector(9 downto 0);
	col   : out std_logic_vector(9 downto 0)
);
end VGATiming;

architecture lowlevel of VGATiming is

	-- define horizontal and vertical resolution
	--
	constant h_res : natural := 640;
	constant v_res : natural := 480;

	-- define pixel clock (in MHz)
	--
	constant pixel_clock : natural := 30;

	-- define horizontal timings (in multiple of pixel clock)
	--
	constant h_front_porch : natural := 940*pixel_clock/1000;  -- 0.94 us
	constant h_sync_time   : natural := 3770*pixel_clock/1000; -- 3.77 us
	constant h_back_porch  : natural := 1890*pixel_clock/1000; -- 1.89 us

	--	define start/stop h. sync timings
	--
	constant h_sync_start : natural := h_res + h_front_porch;
	constant h_sync_stop  : natural := h_sync_start + h_sync_time;

	-- define length of a horizontal cycle
	--
	constant h_cycle : natural := h_sync_stop + h_back_porch;

	-- define horizontal frequency (in kHz)												 
	--
	constant h_freq : natural := 1000 * pixel_clock / h_cycle;

	-- define vertical timings	(in multiple of horizontal frequency)
	--
	constant v_front_porch : natural := 450*h_freq/1000;  -- 0.45 ms
	constant v_sync_time   : natural := 64*h_freq/1000;	-- 0.064 ms
	constant v_back_porch  : natural := 1002*h_freq/1000; -- 1.02 ms	

	-- define start/stop v. sync timings
	--
	constant v_sync_start : natural := v_res + v_front_porch;
	constant v_sync_stop  : natural := v_sync_start + v_sync_time;	

	--	define length of a vertical cycle
	--
	constant v_cycle : natural := v_sync_stop + v_back_porch;

	-- define two counters
	--
	signal horz : std_logic_vector(9 downto 0); 
	signal vert : std_logic_vector(9 downto 0);

begin
	
	-- counter
	--	
	process(clk, reset)
	begin
		if reset = '1' then
			-- reset counters
			--
			horz <= (others => '0');
			vert <= (others => '0');			

		elsif clk'event and clk = '1' then
			horz <= horz + '1';
			if horz > h_cycle then
				horz <= (others => '0');				
				vert <= vert + '1';
				if vert > v_cycle then
					vert <= (others => '0');
				end if;					
			end if;
									
		end if;						
	end process;

	-- output current pixel position (x,y)
	--
	row <= vert;
	col <= horz;

	-- synchronization generator
	--
	process(horz, vert, reset)
	begin
		if reset = '1' then
			-- reset sync signals
			--
			hsync <= '1';
			vsync <= '1';
		else
			-- assert horizontal synchronization, active low
			--
			if (horz > h_sync_start and horz < h_sync_stop) then				
				hsync <= '0';
			else
				hsync <= '1';
			end if;

			-- assert vertical synchronization, active low
			--
			if (vert > v_sync_start and vert < v_sync_stop) then
				vsync <= '0';
			else
				vsync <= '1';
			end if;
				
		end if;
	end process;

	-- blanking generator
	--
	process(clk, reset)
	begin
		if reset = '1' then
			blank <= '1';
		elsif clk'event and clk = '1' then
			if horz >= h_res or vert >= v_res then
				blank <= '1';
			else
				blank <= '0';
			end if;
		end if;
	end process;

end lowlevel;
