library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

--  Uncomment the following lines to use the declarations that are
--  provided for instantiating Xilinx primitive components.
library UNISIM;
use UNISIM.VComponents.all;

entity Arbiter_PH74F786 is
port(
	BR1 : in    std_logic; 
	BR2 : in    std_logic; 
	BR3 : in    std_logic; 
	BR4 : in    std_logic;
	BG1 : out    std_logic; 
	BG2 : out    std_logic; 
	BG3 : out    std_logic; 
	BG4 : out    std_logic
);
end Arbiter_PH74F786;

architecture Behavioral of Arbiter_PH74F786 is
 -- vi anvender her en arbitrerings logic fra
 -- 74F786 kredsen.
 -- Den er specificeret i app notes:
	component arbitration_section
	port (
		BR1 : in    std_logic; 
		BR2 : in    std_logic; 
		BR3 : in    std_logic; 
		BR4 : in    std_logic; 
		A1  : out   std_logic; 
		A2  : out   std_logic; 
		A3  : out   std_logic; 
		A4  : out   std_logic; 
		A5  : out   std_logic; 
		A6  : out   std_logic; 
		B1  : out   std_logic; 
		B2  : out   std_logic; 
		B3  : out   std_logic; 
		B4  : out   std_logic; 
		B5  : out   std_logic; 
		B6  : out   std_logic
	) ;
	end component ;

	signal A1, B1, A2, B2, A3, B3, A4, B4, A5, B5, A6, B6 : std_logic;
	signal BR1i, BR2i, BR3i, BR4i : std_logic;

begin

	BR1i <= not BR1;
	BR2i <= not BR2;
	BR3i <= not BR3;
	BR4i <= not BR4;

	-- arbitration section
	arb_sec: arbitration_section
	port map (
		BR1 => BR1i,
		BR2 => BR2i,
		BR3 => BR3i,
		BR4 => BR4i,
		A1  => A1,
		A2  => A2,
		A3  => A3,
		A4  => A4,
		A5  => A5,
		A6  => A6,
		B1  => B1,
		B2  => B2,
		B3  => B3,
		B4  => B4,
		B5  => B5,
		B6  => B6
	);

	-- decode/output section
	BG1 <= (A1 and A2 and A3) or (A1 and A2 and A6) or (A1 and A3 and B6);
	BG2 <= (B1 and A4 and A5) or (B1 and A4 and A6) or (B1 and A5 and B6);
	BG3 <= (B2 and B4 and A6) or (A1 and B2 and A6) or (B1 and B4 and A6);
	BG4 <= (B3 and B5 and B6) or (A1 and B3 and B6) or (B1 and B5 and B6);

end Behavioral;
