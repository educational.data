library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
library UNISIM;
use UNISIM.VComponents.all;
						 
entity Pong is
port (
   clk   : in std_logic;
	reset : in std_logic;
	data  : in std_logic_vector(7 downto 0);
	addr  : out std_logic_vector(1 downto 0);
	int   : out std_logic;
	frame : in std_logic;
	blank : in std_logic;
	row   : in std_logic_vector(9 downto 0);
	col   : in std_logic_vector(9 downto 0);
	rgb   : out std_logic_vector(5 downto 0)
);
end Pong;

architecture lowlevel of Pong is

	signal pixel_data : std_logic_vector(5 downto 0);

	constant player_w : natural := 4;
	constant player_h : natural := 20;
	constant p1x_pos : natural := 45;
	constant p2x_pos  : natural := 586;
	constant ball_size : natural := 3;

	constant border_width : natural := 2;

	signal p1y_pos : std_logic_vector(9 downto 0);
	signal p2y_pos : std_logic_vector(9 downto 0);
	signal ballx_pos : std_logic_vector(9 downto 0);
	signal bally_pos : std_logic_vector(9 downto 0);
	
	-- row/col hit
	--
	signal p1_hit : std_logic;
	signal p2_hit : std_logic;
	signal ball_hit : std_logic;
	signal border_hit : std_logic;

	-- state machine
	--
	type state_type is (IDLE, LOAD_P1_DATA, LOAD_P2_DATA, LOAD_BALLX_DATA, LOAD_BALLY_DATA, DONE);
	signal state, next_state: state_type;	

begin
	--	render pixel (row,col)
	-- 
	rgb <= pixel_data when blank = '0' else (others => '0');

	-- calculate pixel color for a given object
	--
	process(border_hit, ball_hit, p1_hit, p2_hit)
	begin
		-- background
		--
		pixel_data <= "010101";

		--	border
		-- 
		if border_hit = '1' then
			pixel_data <= "111111";
		end if;

		-- ball  and both paddles
		if ball_hit = '1' or p1_hit = '1' or p2_hit = '1' then
			pixel_data <= "101010";
		end if;

	end process;

	-- calculate hits
	--
	process(row, col, reset, ballx_pos, bally_pos, p1y_pos, p2y_pos)
	begin
		if reset = '1' then			
			border_hit <= '0';
			ball_hit <= '0';
			p1_hit <= '0';
			p2_hit <= '0';													 
		else

			if (row <= conv_std_logic_vector(0, 10) + border_width) or (row >= conv_std_logic_vector(479, 10) - border_width) or
		   	(col <= conv_std_logic_vector(0, 10) + border_width) or (col >= conv_std_logic_vector(639, 10) - border_width) or 
				(col = conv_std_logic_vector(319, 10)) then
				border_hit <= '1';
			else
				border_hit <= '0';
			end if;

			if ('0' & ballx_pos <= col + conv_std_logic_vector(ball_size, 10)) and (ballx_pos + conv_std_logic_vector(ball_size, 10) >= '0' & col) and
				('0' & bally_pos <= row + conv_std_logic_vector(ball_size, 10)) and (bally_pos + conv_std_logic_vector(ball_size, 10) >= '0' & row) then
				ball_hit <= '1';
			else
				ball_hit <= '0';
			end if;
			
			if ('0' & conv_std_logic_vector(p1x_pos, 10) <= col + conv_std_logic_vector(player_w, 10)) and (conv_std_logic_vector(p1x_pos, 10) + conv_std_logic_vector(player_w, 10) >= '0' & col) and
				('0' & p1y_pos <= row + conv_std_logic_vector(player_h, 10)) and (p1y_pos + conv_std_logic_vector(player_h, 10) >= '0' & row) then
				p1_hit <= '1';
			else
				p1_hit <= '0';
			end if;

			if ('0' & conv_std_logic_vector(p2x_pos, 10) <= col + conv_std_logic_vector(player_w, 10)) and (conv_std_logic_vector(p2x_pos, 10) + conv_std_logic_vector(player_w, 10) >= '0' & col) and
				('0' & p2y_pos <= row + conv_std_logic_vector(player_h, 10)) and (p2y_pos + conv_std_logic_vector(player_h, 10) >= '0' & row) then
				p2_hit <= '1';
			else
				p2_hit <= '0';
			end if;		

		end if;		
	end process;

	--
	-- HER UNDER !!! TILSTANDSMASKINE TIL AT L�SE NOGET FRA HUKOMMELSE...
	--

	-- next state process
	--
	process(state, frame)
	begin
		case state is

			when IDLE =>
				if frame = '0' then
					next_state <= LOAD_P1_DATA;
				else
					next_state <= IDLE;			 
				end if;

			when LOAD_P1_DATA => next_state <= LOAD_P2_DATA;
			when LOAD_P2_DATA => next_state <= LOAD_BALLX_DATA;
			when LOAD_BALLX_DATA => next_state <= LOAD_BALLY_DATA;
			when LOAD_BALLY_DATA => next_state <= DONE;

			when DONE =>
				if frame = '1' then
					next_state <= IDLE;
				else
					next_state <= DONE;			 
				end if;

			when others => next_state <= IDLE;

		end case;
	end process;

	-- clocked process
	--
	process(clk, reset)
	begin
		if reset = '1' then
			state <= IDLE;
		elsif clk'event and clk = '1' then
			state <= next_state;
		end if;
	end process;

	--	input/output process
	--
	process(state, data)
	begin
		case state is

			when IDLE =>
				addr <= "00";
				int <= '0';

			when LOAD_P1_DATA =>
				p1y_pos <= shl("00" & data, "10");
				addr <= "01";
				int <= '0';

			when LOAD_P2_DATA =>
				p2y_pos <= shl("00" & data, "10");
				addr <= "10";
				int <= '0';

			when LOAD_BALLX_DATA =>
				ballx_pos <= shl("00" & data, "10");	
				addr <= "11";
				int <= '1';

			when LOAD_BALLY_DATA =>
				bally_pos <= shl("00" & data, "10");
				addr <= "00";
				int <= '1';

			when DONE =>				
				addr <= "00";
				int <= '0';

			when others =>
				addr <= "00";
				int <= '0';

		end case;
	end process;

end lowlevel;
