OUTDIR=.build
texfiles:=${wildcard *.tex}
clsfiles:=${wildcard *.cls}
clsnfiles:=${clsfiles:%=${outdir}/%}
figfiles:=${wildcard fig/*.fig}
epsfiles:=${figfiles:fig/%.fig=${OUTDIR}/%.eps}
latex:=latex #-interaction=batchmode
VPATH=.:${OUTDIR}:fig
SHELL=/bin/bash

help :
	@echo "Error: Missing command"
	@echo 
	@echo "Possible make commands"
	@echo "  xdvi  - produces a DVI file and displays it"
	@echo "  xps   - produces a PS file and displays it"
	@echo "  html  - produces a HTML version."
	@echo "  dvi   - generates a DVI file"
	@echo "  ps    - generates a PS file"
	@echo "  print - uses rlpr to print to std. printer"
	@echo "  clean - cleans all output folders and backup files"
	@echo "  help  - this little print"
	@echo
	@echo "Have fun - tobibobi@mip.sdu.dk"



xdvi : dvi
	@xdvi ${OUTDIR}/index.dvi &

xps : ps 
	gv $(OUTDIR)/index.ps &

html : ${OUTDIR}/index.html

dvi : ${OUTDIR}/index.dvi

ps : ${OUTDIR}/index.ps


print : ${OUTDIR}/index.ps
	rlpr ${OUTDIR}/index.ps

${OUTDIR}/index.html : ${texfiles} makefile
	rm -rf ${OUTDIR}
	mkdir ${OUTDIR}
	latex2html -dir ${OUTDIR} -split +0 -no_navigation index.tex

${OUTDIR}/index.dvi : ${texfiles} ${epsfiles} makefile ${OUTDIR} index.bib ${clsfiles}
	cp index.bib ${OUTDIR}/index.bib
	cp ${texfiles} ${OUTDIR}

	cp tb_report.cls ${OUTDIR}

	cd ${OUTDIR}; \
	${latex} index.tex; \
	bibtex index; \
	${latex} index.tex; \
	${latex} index.tex;

${OUTDIR}/%.eps : %.fig ${OUTDIR}
	fig2dev -L eps $< $@

${OUTDIR}/%.cls : %.cls ${OUTDIR}
	cp $< $@

${OUTDIR}/index.ps : ${OUTDIR}/index.dvi
	cd $(OUTDIR);dvips index.dvi -o index.ps

${OUTDIR} :
	@mkdir ${OUTDIR}

clean : 
	@rm -rf ${OUTDIR}
	@rm -rf *~
	@rm -f \#*
	@find . -name *.bak -exec rm -f {} \;