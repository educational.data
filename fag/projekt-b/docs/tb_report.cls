\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{STY-FILES/tb_report}[2003/9/19 LaTeX2e extended class]
%\LoadClass[a4paper,dvi]{report}
\LoadClass[dvi]{report}
% \usepackage{STY-FILES/tb_report}

\usepackage[danish]{babel}
\usepackage[latin1]{inputenc}
\usepackage[dvips]{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{epsfig}
%\usepackage{a4wide}
\usepackage{array}
\usepackage{vpage}
\usepackage{color}
\usepackage{letterspace}

\usepackage{listings} 
\usepackage{fancyvrb}

\selectlanguage{danish}

\newcommand{\HRule}{\rule{\linewidth}{1mm}}
\newcommand\@place{}


\newcommand\@coverimage{\vspace{\stretch{2}}}
\newcommand\coverimage[1]{\renewcommand\@coverimage{ %
\vspace*{\stretch{1.5}} %
{\center \epsfig{file=#1,height=5cm}  \par}
  \vspace*{\stretch{2}}}}

\newcommand\@subtitle{}
\newcommand\subtitle[1]{\renewcommand\@subtitle{\\#1}}
\newcommand\place[1]{\renewcommand\@place{#1}}
\newcommand\signature[1]{\begin{minipage}[b]{7cm}
      \begin{center}
        \vskip 1.5cm%
        \rule{5cm}{1sp}\\[0mm]
        \emph{#1}
      \end{center}
    \end{minipage} }



\renewcommand\maketitle{\begin{titlepage}%
  \let\footnotesize\small
  \let\footnoterule\relax
  \let \footnote \thanks
  %%\null\vfil
  %%\vskip 60\p@
  \begin{center}
      {\Large \bf{\@place }} 
  \end{center}
  \@coverimage
  \setlength{\parindent}{0mm}
  \setlength{\parskip}{0mm}
  \HRule%
  \begin{flushright}
    \setlength\lineskip{5mm}
    \noindent {\Huge {\bf \@title} \@subtitle}  %
  \end{flushright}
  \HRule%

  %%\vskip 3em%
  \vspace*{\stretch{1}}%
  \begin{center}%
    {\LARGE
     \@author
     }%
      \vskip 0.5em%
  \end{center}\par
  \@thanks
  \vspace*{\stretch{2}}%
  {\center \Large \@date \par}%       % Set date in \large size.
  %\vfil\null
  \end{titlepage}%
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}

\renewcommand\chaptermark[1]{
  \begin{minipage}[t]{15cm}
    \begin{minipage}[t]{11cm}
      \vskip 1 mm
      \bf \large #1 \\
      \hrule
    \end{minipage}
    \hskip 1cm
    \parbox[t]{2.5cm} {
      \colorbox{blue}{
        \begin{minipage}[t]{2.5cm} %
          \begin{center} %
            \vskip 5mm 
            \color{white}{ { 
                \LARGE \bf
                \@chapapp 
            } } \\ \vskip 5mm 
          \color{white}{ {
              \Huge \bf 
              \thechapter  
          }  }  \vskip 3mm %
          \end{center} %
        \end{minipage}  %
      }
    }
  \end{minipage}
  \vskip 3mm
}

\renewcommand\@makechapterhead{}


\newcommand\conclusion{\chapter*{Konklusion}}

\def\@chapter[#1]#2{\ifnum \c@secnumdepth >\m@ne
  \refstepcounter{chapter}%
  \typeout{\@chapapp\space\thechapter.}%
  \addcontentsline{toc}{chapter}%
                  {\protect\numberline{\thechapter}#1}%
                  \else
                  \addcontentsline{toc}{chapter}{#1}%
                  \fi
                  \chaptermark{#1}%
                    \addtocontents{lof}{\protect\addvspace{10\p@}}%
                    \addtocontents{lot}{\protect\addvspace{10\p@}}%
                    \if@twocolumn
                    \@topnewpage%
                    \else
                    \@afterheading
                    \fi}
\newenvironment{Example}[1]
{
  \setlength\parindent{0cm}
  \vskip 7mm
  \rule{\linewidth}{0.5pt}\\
  {\large \bf{Eksempel :} #1}\\[0cm]
  \rule{\linewidth}{0.5pt}
  \begin{center}
  \begin{minipage}[b]{0.9\linewidth}}  
  {\end{minipage}\end{center}\rule{\linewidth}{0.5pt} 
}


\newenvironment{Program}[1]
{
  \rule{\linewidth}{0.5pt}\\
  {\large \bf{Program :} #1}\\[0cm]
  \rule{\linewidth}{0.5pt}
  \begin{verbatim}}  
  {\end{verbatim}\rule{\linewidth}{0.5pt} 
}



\endinput
